-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: lovedb
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `language` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'2017-06-09 14:17:39','2017-06-09 14:17:39','EN','Home','home','templates/home.html'),(2,'2017-06-09 14:19:26','2017-06-09 14:19:26','EN','Login','login','templates/login.html'),(3,'2017-06-09 14:21:25','2017-06-09 14:21:25','EN','Forgot Password','forgot-password','templates/rest.html'),(4,'2017-06-09 14:23:17','2017-06-09 14:23:17','EN','Prime','prime',NULL),(5,'2017-06-09 14:23:17','2017-06-09 14:23:17','EN','Lite','lite',NULL),(6,'2017-06-09 14:23:17','2017-06-09 14:23:17','EN','Features','features','templates/features.html'),(7,'2017-06-09 14:24:53','2017-06-09 14:24:53','EN','Mobile','mobiles',''),(8,'2017-06-09 14:24:53','2017-06-09 14:24:53','EN','About Us','about-us','templates/about-us.html'),(9,'2017-06-09 14:24:53','2017-06-09 14:24:53','EN','Contact Us','contact-us','templates/contact-us.html'),(10,'2017-06-09 19:23:26','2017-06-09 19:23:26','En','Plans & Pricing','plans-pricing',NULL);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-10 22:41:09
--
-- Table structure for table `text_widget`
--

DROP TABLE IF EXISTS `text_widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_widget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_widget`
--

LOCK TABLES `text_widget` WRITE;
/*!40000 ALTER TABLE `text_widget` DISABLE KEYS */;
INSERT INTO `text_widget` VALUES (1,'2017-06-10 09:57:34','2017-06-10 09:57:34','homepage_header_h1'),(2,'2017-06-10 12:44:38','2017-06-10 12:44:38','homepage_header_h5'),(3,'2017-06-10 13:55:50','2017-06-10 13:55:50','homepage_about_text_h1'),(4,'2017-06-10 13:56:39','2017-06-10 13:56:39','homepage_about_text_p'),(5,'2017-06-10 14:10:24','2017-06-10 14:10:24','homepage_featured_first_blog'),(6,'2017-06-10 14:10:24','2017-06-10 14:10:24','homepage_featured_second_blog'),(7,'2017-06-10 14:10:24','2017-06-10 14:10:24','homepage_featured_third_blog'),(8,'2017-06-10 14:10:24','2017-06-10 14:10:24','homepage_featured_fourth_blog'),(9,'2017-06-10 14:10:24','2017-06-10 14:10:24','homepage_featured_fifth_blog'),(10,'2017-06-10 14:10:24','2017-06-10 14:10:24','homepage_featured_sixth_blog'),(11,'2017-06-10 15:55:44','2017-06-10 15:55:44','about_us_description'),(12,'2017-06-10 15:59:16','2017-06-10 15:59:16','about_us_header'),(13,'2017-06-10 16:27:50','2017-06-10 16:27:50','contact_us_address'),(14,'2017-06-10 16:27:50','2017-06-10 16:27:50','contact_us_number'),(15,'2017-06-10 16:27:50','2017-06-10 16:27:50','contact_us_email'),(16,'2017-06-10 17:02:18','2017-06-10 17:02:18','mobile_header_text'),(17,'2017-06-10 17:04:14','2017-06-10 17:04:14','mobile_description_text'),(18,'2017-06-10 17:10:00','2017-06-10 17:10:00','record_voice_description'),(19,'2017-06-10 17:13:00','2017-06-10 17:13:00','send_song_description'),(20,'2017-06-10 17:13:00','2017-06-10 17:13:00','send_gift_description'),(21,'2017-06-10 18:46:23','2017-06-10 18:46:23','forgot_password_description');
/*!40000 ALTER TABLE `text_widget` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

--
-- Table structure for table `text_translations`
--

DROP TABLE IF EXISTS `text_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` text,
  `language` varchar(255) DEFAULT NULL,
  `text_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgjkiweyyutste40q01ew7bjbi` (`text_id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_translations`
--

LOCK TABLES `text_translations` WRITE;
/*!40000 ALTER TABLE `text_translations` DISABLE KEYS */;
INSERT INTO `text_translations` VALUES (1,'2017-06-10 09:58:05','2017-06-10 18:56:32','dating without distractions','EN',1),(2,'2017-06-10 12:45:38','2017-06-10 12:45:38','We believe that a voice is for life...lovappy it.','EN',2),(3,'2017-06-10 13:58:56','2017-06-10 13:58:56','The LovAppy Corporation loves love.','EN',3),(4,'2017-06-10 13:59:15','2017-06-10 18:55:58','      We believe that the world is complex and full of distractions. It is our goal to continually improve                 our technology to assist you in finding the one and only important part of life. Start with our free                 app and begin a good , old-fashioned courting process. We will not let you fall into the traps                 that are everywhere in today’s world. With LovAppy Lite, your courting forces you into looking                 deeper into potential mates. Not efficient enough for you? Consider our coming soon,                 American-style arranged marriage algorithm.','EN',4),(5,'2017-06-10 14:11:02','2017-06-10 14:11:02','LovBlog','EN',5),(6,'2017-06-10 14:12:19','2017-06-10 14:12:19','Lorem Impsum','EN',6),(7,'2017-06-10 14:12:19','2017-06-10 14:12:19','Lorem Impsum','EN',7),(8,'2017-06-10 14:12:19','2017-06-10 14:12:19','Lorem Impsum','EN',8),(9,'2017-06-10 14:12:19','2017-06-10 14:12:19','Lorem Impsum','EN',9),(10,'2017-06-10 14:12:19','2017-06-10 14:12:19','Lorem Impsum','EN',10),(11,'2017-06-10 15:58:03','2017-06-10 15:58:03','We believe that the world is complex and full of distratctions. It is our goal to continually improve our technology to assist you in finding the one and only important part of life, Start with our free app and begin a good, old-fashioned courting process, We will not let you fall into the traps that are everywhere in today\'s world. With LovAppy Lite, your courting process forcecs you into looking deeper into potential mates. Not efficient enough for you? Consider our coming soon, American-style arranged marriage algorithm.','EN',11),(12,'2017-06-10 15:59:51','2017-06-10 15:59:51','The LovAppy Corporation loves love.','EN',12),(13,'2017-06-10 16:30:14','2017-06-10 16:30:14','1510 Corlies Avenue Neptune, NJ 07753','EN',13),(14,'2017-06-10 16:30:14','2017-06-10 16:30:14','(800) 574-7802','EN',14),(15,'2017-06-10 16:30:14','2017-06-10 16:30:14','info@lovappy.com','EN',15),(16,'2017-06-10 17:03:14','2017-06-10 17:03:14','Download our Mobile App','EN',16),(17,'2017-06-10 17:07:16','2017-06-10 17:07:16','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco','EN',17),(18,'2017-06-10 17:13:50','2017-06-10 17:13:50','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco','EN',18),(19,'2017-06-10 17:13:50','2017-06-10 17:13:50','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco','EN',19),(20,'2017-06-10 17:13:50','2017-06-10 17:13:50','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco','EN',20),(21,'2017-06-10 18:46:52','2017-06-10 18:46:52','Enter your email address into the text box and click ','EN',21);
/*!40000 ALTER TABLE `text_translations` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `image_widget`
--

DROP TABLE IF EXISTS `image_widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image_widget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image_widget`
--

LOCK TABLES `image_widget` WRITE;
/*!40000 ALTER TABLE `image_widget` DISABLE KEYS */;
INSERT INTO `image_widget` VALUES (1,'2017-06-10 09:54:09','2017-06-10 18:24:29','home_banner1','banner','/images/home-banner4.png'),(2,'2017-06-10 09:54:23','2017-06-10 12:41:24','home_banner2','banner','/images/home-banner.png'),(3,'2017-06-10 10:52:30','2017-06-10 12:41:34','home_banner3','banner','/images/home-banner2.png'),(4,'2017-06-10 10:55:07','2017-06-10 12:41:41','home_banner4','banner','/images/home-banner3.png'),(5,'2017-06-10 14:50:18','2017-06-10 14:50:18','main_login_bg','background','/images/new-login-bg.png'),(6,'2017-06-10 15:31:48','2017-06-10 15:31:48','homepage_featured_first_bg','background','/images/lips.png'),(7,'2017-06-10 15:31:48','2017-06-10 15:31:48','homepage_featured_second_bg','background','/images/wed_pic.png'),(8,'2017-06-10 15:31:48','2017-06-10 15:31:48','homepage_featured_third_bg','background','/images/rings.png'),(9,'2017-06-10 15:31:48','2017-06-10 15:31:48','homepage_featured_fourth_bg','background','/images/heart.png'),(10,'2017-06-10 15:31:48','2017-06-10 15:31:48','homepage_featured_fifth_bg','background','/images/lips.png'),(11,'2017-06-10 15:31:48','2017-06-10 15:31:48','homepage_featured_sixth_bg','background','/images/stand.png'),(12,'2017-06-10 16:03:42','2017-06-10 16:03:42','about_us_bg','background','/images/about-header.png'),(13,'2017-06-10 17:00:07','2017-06-10 17:00:07','left_mobile_page_image','default','/images/left-mobile-page.png');
/*!40000 ALTER TABLE `image_widget` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;


--
-- Table structure for table `pages_images`
--

DROP TABLE IF EXISTS `pages_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_images` (
  `page_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  PRIMARY KEY (`page_id`,`image_id`),
  KEY `FK8e4v7d30ndc0rakycxqejij8u` (`image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_images`
--

LOCK TABLES `pages_images` WRITE;
/*!40000 ALTER TABLE `pages_images` DISABLE KEYS */;
INSERT INTO `pages_images` VALUES (1,1),(1,2),(1,3),(1,4),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(2,5),(7,13),(8,12);
/*!40000 ALTER TABLE `pages_images` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

--
-- Table structure for table `pages_texts`
--

DROP TABLE IF EXISTS `pages_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_texts` (
  `page_id` int(11) NOT NULL,
  `text_id` int(11) NOT NULL,
  PRIMARY KEY (`page_id`,`text_id`),
  KEY `FKt4u4wddyod95kfq6xlwr5w10u` (`text_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_texts`
--

LOCK TABLES `pages_texts` WRITE;
/*!40000 ALTER TABLE `pages_texts` DISABLE KEYS */;
INSERT INTO `pages_texts` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(3,21),(6,18),(6,19),(6,20),(7,16),(7,17),(8,11),(8,12),(9,13),(9,14),(9,15);
/*!40000 ALTER TABLE `pages_texts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;