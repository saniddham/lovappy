/**
 * Created by Darrel Rayen on 10/20/17.
 */
$(document).ready(function () {

    var dealSubmit = $('#deal-submit');
    var updateDeal = $('#update-deal-btn');
    var dealId = $('#deal-id').val();
    dealSubmit.css('cursor', 'pointer');

    dealSubmit.on('click', function (e) {
        e.preventDefault();
        dealSubmit.addClass("loading");
        dealSubmit.prop('disabled', true);
        $.ajax({
            url: baseUrl + 'lox/dating/places/deals/save',
            type: "POST",
            data: $('#add-deal-form').serialize(),

            success: function (data) {
                dealSubmit.removeClass("loading");
                dealSubmit.prop('disabled', false);
                window.location.href = baseUrl + 'lox/dating/places/deals';

            },
            error: function (jqXHR, textStatus, errorThrown) {
                dealSubmit.removeClass("loading");
                dealSubmit.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401) {
                    //window.location.href = baseUrl + "login";
                }
                console.log("unsuccessfull");

            }
        });
    });

    updateDeal.on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: baseUrl + 'lox/dating/places/deals/update',
            type: "POST",
            data: $('#deal-update-form').serialize(),

            success: function (data) {
                window.location.href = baseUrl + 'lox/dating/places/deals/' + dealId;

            },
            error: function (jqXHR, textStatus, errorThrown) {
                updateDeal.removeClass("loading");
                updateDeal.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401) {
                    //window.location.href = baseUrl + "login";
                }
                console.log("unsuccessful");

            }
        });
    });
});
function hideErrorMessages() {
    $('.error-msg').each(function () {
        $(this).text('');
        $(this).hide();
    });
}