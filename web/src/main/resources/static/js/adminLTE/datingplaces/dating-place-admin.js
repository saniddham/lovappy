/**
 * Created by Darrel Rayen on 10/20/17.
 */
$(document).ready(function () {

    var updateDatingLocationSubmit = $('#update-dating-place-btn');
    updateDatingLocationSubmit.css('cursor', 'pointer');

    updateDatingLocationSubmit.on('click', function (e) {
        e.preventDefault();
        updateDatingLocationSubmit.addClass("loading");
        updateDatingLocationSubmit.prop('disabled', true);
        $.ajax({
            url: baseUrl + 'lox/dating/places/update',
            type: "POST",
            data: $('#dating-place-edit-form').serialize(),

            success: function (data) {
                console.log("success");
                updateDatingLocationSubmit.removeClass("loading");
                updateDatingLocationSubmit.prop('disabled', false);
                $("#popup-message-title").text("Done");
                $("#popup-message-text").text("Dating Place has been updated successfully.");
                $('#message-popup').modal({
                    closable: false,
                    onApprove: function () {
                        //window.location.href = baseUrl + 'dating/places';
                    }
                }).modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                updateDatingLocationSubmit.removeClass("loading");
                updateDatingLocationSubmit.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401) {
                    //window.location.href = baseUrl + "login";
                }
                console.log("unsuccessfull");

            }
        });
    });

    var uploadImageContainer = $("#image-upload-container").find("span");
    var loadingHtml = "<span class=\"ui button loading\"></span>";


    $("#dating-location-photo").on('change', function () {
        uploadImageContainer.html(loadingHtml);

        var formData = new FormData();
        formData.append('dating_place_picture', this.files[0]);

        $.ajax({
            url: baseUrl + "dating/places/create/upload_dating-place-picture",
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (data) {
                uploadImageContainer.html("<img src=\"" + data.url + "\"/>");
                $("#dating-place-picture").val(data.id);
                $("#dating-place-cover-picture").val(data.id);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 400)
                    showNotifyErrorMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";

            }
        });
    });

    function getCountries(event) {
        var countryList = [];
        $.ajax({
            type: 'GET',
            url: 'https://restcountries.eu/rest/v1/all'
        }).done(function (response) {
            $('#country').empty();
            $('#country').append('<option value="default"> -- Select Country -- </option>');

            $.each(response, function (index, item) {
                if ($.inArray(item.name, countryList) == -1 && item.name.length >= 1) {
                    countryList.push("<option value='" + item.name + "'>" + item.name + "</option>");
                    /*$('#country').append('<option value="' + item.name + '">' + item.name + '</option>');*/
                }
            });
            $('#country').append(countryList.join(""));
        });

    }

    getCountries(onload);

});
function hideErrorMessages() {
    $('.error-msg').each(function () {
        $(this).text('');
        $(this).hide();
    });
}