/**
 * Created by Darrel Rayen on 10/20/17.
 */
$(document).ready(function () {

    var submitQuestion = $('#submit-question');
    var today = new Date();
    $('#question-date').val(today.getTime());
    submitQuestion.on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: baseUrl + 'lox/questions',
            type: "POST",
            data: $('#add-question-form').serialize(),

            success: function (data) {
                window.location.href = baseUrl + "lox/questions";
            },
            error: function (jqXHR, textStatus, errorThrown) {
                hideErrorMessages();
                if (jqXHR.status === 400) {
                    var messages = jqXHR.responseJSON;

                    if (messages !== null && typeof messages !== 'undefined') {
                        for (var i = 0; i < messages.length; i++) {
                            var item = messages[i];
                            toastr.error(item.message);
                        }
                    }
                } else if (jqXHR.status === 401) {
                    window.location.href = baseUrl + "lox/login";
                }
            }
        });
    });

});


function updateQuestion(id) {
    var questionId = $('#questionId' + id).val();
    $.ajax({
        url: baseUrl + 'lox/questions/' + questionId,
        type: "PUT",
        data: {question: $('#questionString' + id).val()},
        success: function (data) {
            window.location.href = baseUrl + "lox/questions/";
        },
        error: function (jqXHR, textStatus, errorThrown) {
            hideErrorMessages();
            if (jqXHR.status === 400)
                showValidationMessages(jqXHR.responseJSON);
            else if (jqXHR.status === 401) {
                window.location.href = baseUrl + "lox/login";
            }
        }
    });
}

function approveResponse(id) {
    var responseId = $('#responseId' + id).val();
    var questionId = $('#question-id').val();
    $.ajax({
        url: baseUrl + 'lox/questions/response/approve/' + responseId,
        type: "POST",
        success: function (data) {
            window.location.href = baseUrl + 'lox/questions/' + questionId;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            hideErrorMessages();
            if (jqXHR.status === 400)
                showValidationMessages(jqXHR.responseJSON);
            else if (jqXHR.status === 401) {
                window.location.href = baseUrl + "lox/login";
            }
        }
    });
}

function rejectResponse(id) {
    var responseId = $('#responseId' + id).val();
    var questionId = $('#question-id').val();
    $.ajax({
        url: baseUrl + 'lox/questions/response/reject/' + responseId,
        type: "POST",
        success: function (data) {
            window.location.href = baseUrl + 'lox/questions/' + questionId;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            hideErrorMessages();
            if (jqXHR.status === 400)
                showValidationMessages(jqXHR.responseJSON);
            else if (jqXHR.status === 401) {
                window.location.href = baseUrl + "lox/login";
            }
        }
    });
}

function hideErrorMessages() {
    $('.error-msg').each(function () {
        $(this).text('');
        $(this).hide();
    });
}