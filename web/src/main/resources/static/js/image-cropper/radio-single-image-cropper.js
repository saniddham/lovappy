$(function () {

    'use strict';

    var console = window.console || {
        log: function () {
        }
    };
    var URL = window.URL || window.webkitURL;
    var $singleImage = $('#single-image');
    var $download = $('#download');
    var originalImageURL = $singleImage.attr('src');
    var uploadedImageType = 'image/jpeg';
    var uploadedImageURL;

    var options = {
        minCropBoxWidth: 200,
        minCropBoxHeight: 198,
        imageSmoothingEnabled: false,
        imageSmoothingQuality: 'high',
        aspectRatio: 200 / 198,
        preview: $('#imgView')
    };

    // Tooltip
    $('[data-toggle="tooltip"]').tooltip();


    // Cropper
    $singleImage.on({
        ready: function (e) {
        },
        cropstart: function (e) {
        },
        cropmove: function (e) {
        },
        cropend: function (e) {
        },
        crop: function (e) {
        },
        zoom: function (e) {
        }
    }).cropper(options);


    // Buttons
    if (!$.isFunction(document.createElement('canvas').getContext)) {
        $('button[data-method="getCroppedCanvas"]').prop('disabled', true);
    }

    if (typeof document.createElement('cropper').style.transition === 'undefined') {
        $('button[data-method="rotate"]').prop('disabled', true);
        $('button[data-method="scale"]').prop('disabled', true);
    }


    // Options
    $('.docs-toggles').on('change', 'input', function () {
        var $this = $(this);
        var name = $this.attr('name');
        var type = $this.prop('type');
        var cropBoxData;
        var canvasData;

        if (!$singleImage.data('cropper')) {
            return;
        }

        if (type === 'checkbox') {
            options[name] = $this.prop('checked');
            cropBoxData = $singleImage.cropper('getCropBoxData');
            canvasData = $singleImage.cropper('getCanvasData');

            options.ready = function () {
                $singleImage.cropper('setCropBoxData', cropBoxData);
                $singleImage.cropper('setCanvasData', canvasData);
            };
        } else if (type === 'radio') {
            options[name] = $this.val();
        }

        $singleImage.cropper('destroy').cropper(options);
    });


    // Methods
    $('.docs_buttons').on('click', '[data-method]', function () {
        var $this = $(this);
        var data = $this.data();
        var cropper = $singleImage.data('cropper');
        var cropped;
        var $target;
        var resultSg;

        if ($this.prop('disabled') || $this.hasClass('disabled')) {
            return;
        }

        if (cropper && data.method) {
            data = $.extend({}, data); // Clone a new one

            if (typeof data.target !== 'undefined') {
                $target = $(data.target);

                if (typeof data.option === 'undefined') {
                    try {
                        data.option = JSON.parse($target.val());
                    } catch (e) {
                        console.log(e.message);
                    }
                }
            }

            cropped = cropper.cropped;

            switch (data.method) {
                case 'rotate':
                    if (cropped && options.viewMode > 0) {
                        $singleImage.cropper('clear');
                    }

                    break;

                case 'getCroppedCanvas':
                    if (uploadedImageType === 'image/jpeg') {
                        if (!data.option) {
                            data.option = {};
                        }

                        data.option.fillColor = '#fff';
                    }

                    break;
            }

            resultSg = $singleImage.cropper(data.method, data.option, data.secondOption);

            switch (data.method) {
                case 'rotate':
                    if (cropped && options.viewMode > 0) {
                        $singleImage.cropper('crop');
                    }

                    break;

                case 'scaleX':
                case 'scaleY':
                    $(this).data('option', -data.option);
                    break;

                case 'getCroppedCanvas':
                    if (resultSg) {
                        // Bootstrap's Modal
                        $('#getCroppedCanvasModal').modal().find('.modal-body').html(resultSg);

                        if (!$download.hasClass('disabled')) {
                            $download.attr('href', resultSg.toDataURL(uploadedImageType));
                        }
                    }

                    break;

                case 'destroy':
                    if (uploadedImageURL) {
                        URL.revokeObjectURL(uploadedImageURL);
                        uploadedImageURL = '';
                        $singleImage.attr('src', originalImageURL);
                    }

                    break;
            }

            if ($.isPlainObject(resultSg) && $target) {
                try {
                    $target.val(JSON.stringify(resultSg));
                } catch (e) {
                    console.log(e.message);
                }
            }

        }
    });


    // Keyboard
    $(document.body).on('keydown', function (e) {

        if (!$singleImage.data('cropper') || this.scrollTop > 300) {
            return;
        }

        switch (e.which) {
            case 37:
                e.preventDefault();
                $singleImage.cropper('move', -1, 0);
                break;

            case 38:
                e.preventDefault();
                $singleImage.cropper('move', 0, -1);
                break;

            case 39:
                e.preventDefault();
                $singleImage.cropper('move', 1, 0);
                break;

            case 40:
                e.preventDefault();
                $singleImage.cropper('move', 0, 1);
                break;
        }

    });


    // Import image
    var $inputImage = $('#inputSingleImage');

    if (URL) {
        $inputImage.change(function () {
            var files = this.files;
            var file;

            if (!$singleImage.data('cropper')) {
                return;
            }

            if (files && files.length) {
                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    uploadedImageType = file.type;

                    if (uploadedImageURL) {
                        URL.revokeObjectURL(uploadedImageURL);
                    }

                    uploadedImageURL = URL.createObjectURL(file);
                    $singleImage.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
                    $inputImage.val('');
                } else {
                    window.alert('Please choose an image file.');
                }
            }
        });
    } else {
        $inputImage.prop('disabled', true).parent().addClass('disabled');
    }

});
