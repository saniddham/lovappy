var map;
var googleMapLoaded = false;
// Called in event-create.html
function setGoogleMapLoaded() {
    googleMapLoaded = true;
}

function distanceCal(lat1, lon1, lat2, lon2) {
    var R = 6371; // Earth's radius in Km
    return Math.acos(Math.sin(lat1) * Math.sin(lat2) +
            Math.cos(lat1) * Math.cos(lat2) *
            Math.cos(lon2 - lon1)) * R;
}
var latitude1 = null;
var longitude1 = null;
$(document).ready(function () {
    // Prevent submit on press of Enter key
    $(window).keydown(function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            return false;
        }
    });

    function getCoordinates() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                $('#search-latitude').val(position.coords.latitude);
                $('#search-longitude').val(position.coords.longitude);
                $('#name-latitude').val(position.coords.latitude);
                $('#name-longitude').val(position.coords.longitude);
                $('#filter-latitude').val(position.coords.latitude);
                $('#filter-longitude').val(position.coords.longitude);
                $('#filtered').val(true);
                var needReload = false;

                var backendLatitude = $("#latitude2").val();
                var backendLongitude = $("#longitude2").val();

                latitude1 = (backendLatitude != null) ? backendLatitude : localStorage.getItem("latitude");
                longitude1 = (backendLongitude != null) ? backendLongitude : localStorage.getItem("longitude");

                var distance = distanceCal(position.coords.latitude, position.coords.longitude, latitude1, longitude1);
                var firstLoad = (!latitude1 && !longitude1);

                if (firstLoad || (distance > 1)) {
                    localStorage.setItem("latitude", position.coords.latitude);
                    localStorage.setItem("longitude", position.coords.longitude);
                    needReload = true;
                    latitude1 = localStorage.getItem("latitude");
                    longitude1 = localStorage.getItem("longitude");
                }
                $('#latitude').val(latitude1);
                $('#longitude').val(longitude1);

                if (needReload) {
                    $('#location-form').submit();
                } else {
                    localStorage.setItem("latitude", null);
                    localStorage.setItem("longitude", null);

                }

            });
        }
    }

    getCoordinates();

    function degreesToRadians(degrees) {
        return degrees * Math.PI / 180;
    }

    function distanceInKmBetweenEarthCoordinates(lat1, lon1, lat2, lon2) {
        var earthRadiusKm = 6371;

        var dLat = degreesToRadians(lat2 - lat1);
        var dLon = degreesToRadians(lon2 - lon1);

        lat1 = degreesToRadians(lat1);
        lat2 = degreesToRadians(lat2);

        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return earthRadiusKm * c;
    }
});