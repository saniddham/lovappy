$(document).ready(function () {


    $('.dropdown').dropdown();

    var submitBtn = $("#save-btn");

    $('#upload-1').on('click', function (e) {
        e.preventDefault();
        $('#image-upload-id').val('1');
        $('#upload-form').trigger("reset");
        $('#radio-1').prop("checked", true);
        $('#radio-2').prop("checked", false);
        $('#upload-modal').modal('show');
    });
    $('#upload-2').on('click', function (e) {
        e.preventDefault();
        $('#image-upload-id').val('2');
        $('#upload-form').trigger("reset");
        $('#radio-1').prop("checked", true);
        $('#radio-2').prop("checked", false);
        $('#upload-modal').modal('show');
    });
    $('#upload-3').on('click', function (e) {
        e.preventDefault();
        $('#image-upload-id').val('3');
        $('#upload-form').trigger("reset");
        $('#radio-1').prop("checked", true);
        $('#radio-2').prop("checked", false);
        $('#upload-modal').modal('show');
    });

    $('#image-file-input').click(function () {
        $('#radio-1').prop("checked", true);
        $('#radio-2').prop("checked", false);
    });

    $('#image-url-input').on('click', function (e) {
        $('#radio-1').prop("checked", false);
        $('#radio-2').prop("checked", true);
    });

    $('#cancel-btn').on('click', function (e) {
        $('#upload-modal').modal('hide');
    });

    $('#import-btn').on('click', function (e) {
        e.preventDefault();
        if ($('input[name="radio"]:checked').val() == "file") {

            var formData = new FormData();
            var imageFile = $("#image-file-input")[0].files[0];

            if (imageFile !== undefined) {
                formData.append('image', imageFile);
                $.ajax({
                    url: baseUrl + "/vendor/product/image_upload",
                    type: "POST",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: formData,
                    success: function (data) {
                        var selection = $('#image-upload-id').val();
                        if (selection == '1') {
                            $("#image1").attr('src', data.url);
                            $("#photo-id1").val(data.id);
                        } else if (selection == '2') {
                            $("#image2").attr('src', data.url);
                            $("#photo-id2").val(data.id);
                        } else if (selection == '3') {
                            $("#image3").attr('src', data.url);
                            $("#photo-id3").val(data.id);
                        }

                        $('#upload-modal').modal('hide');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 400)
                            showNotifyErrorMessages(jqXHR.responseJSON);
                        else if (jqXHR.status === 401)
                            window.location.href = baseUrl + "login";

                    }
                });
            } else {
                toastr.error('Invalid image!!');
            }

        } else {
            if ($('#image-url-input').validate()) {
                $.ajax({
                    type: "GET",
                    url: $('#image-url-input').val(),
                    success: function (message, text, response) {
                        if (response.getResponseHeader('Content-Type').indexOf("image") != -1) {

                            var formData = new FormData();
                            formData.append('image', $('#image-url-input').val());

                            $.ajax({
                                url: baseUrl + "/vendor/product/image_upload_url",
                                type: "POST",
                                processData: false,
                                contentType: false,
                                cache: false,
                                data: formData,
                                success: function (data) {
                                    var selection = $('#image-upload-id').val();
                                    if (selection == '1') {
                                        $("#image1").attr('src', data.url);
                                        $("#photo-id1").val(data.id);
                                    } else if (selection == '2') {
                                        $("#image2").attr('src', data.url);
                                        $("#photo-id2").val(data.id);
                                    } else if (selection == '3') {
                                        $("#image3").attr('src', data.url);
                                        $("#photo-id3").val(data.id);
                                    }
                                    $('#upload-modal').modal('hide');
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    if (jqXHR.status === 400) {
                                        showNotifyErrorMessages(jqXHR.responseJSON);
                                    }else if (jqXHR.status === 401) {
                                        window.location.href = baseUrl + "login";
                                    }else {
                                        toastr.error('Invalid image url!!');
                                    }
                                }
                            });

                        } else {
                            toastr.error('Invalid image url!!');
                        }
                    },
                    error: function () {
                        toastr.error('Invalid image url!!');
                    }
                });
            } else {
                toastr.error('Image url required!!');
            }
        }
    });

    submitBtn.on('click', function (e) {
        e.preventDefault();
        submitBtn.addClass("disabled");
        submitBtn.addClass("loading");

        var x = $('#code-input').validate()
            & $('#countryInput').validateCountry()
            & $('#productTypeInput').validateProductType()
            & $('#brandInput').validateBrand()
            & $('#name-input').validate()
            & $('#ean-input').validate()
            & $('#price-input').validate()
            & $('#photo-id1').validateImage1()
            & $('#description-input').validate();


        if (x) {
            var url = "/api/v1/product/add";
            $.ajax({
                url: baseUrl + url,
                type: "POST",
                data: $('#product-add-form').serialize(),
                success: function (data) {
                    if (containsValidationErrors(data)) {
                        submitBtn.removeClass("disabled");
                        submitBtn.removeClass("loading");
                    } else {
                        toastr.success('Successfully saved !!');
                        $('#product-add-form').trigger("reset");
                        submitBtn.removeClass("disabled");
                        submitBtn.removeClass("loading");
                        location.reload();
                    }
                },
                error: function (data) {
                    submitBtn.removeClass("disabled");
                    submitBtn.removeClass("loading");
                }
            });
        } else {
            submitBtn.removeClass("disabled");
            submitBtn.removeClass("loading");
            toastr.error($('#fill-all-fields').val());
        }

    });

    $('#save-exit-btn').on('click', function (e) {
        e.preventDefault();
        $('#save-exit-btn').addClass("disabled");
        $('#save-exit-btn').addClass("loading");

        var x = $('#code-input').validate()
            & $('#countryInput').validateCountry()
            & $('#productTypeInput').validateProductType()
            & $('#brandInput').validateBrand()
            & $('#name-input').validate()
            & $('#ean-input').validate()
            & $('#price-input').validate()
            & $('#photo-id1').validateImage1()
            & $('#description-input').validate();


        if (x) {
            var url = "/api/v1/product/add";
            $.ajax({
                url: baseUrl + url,
                type: "POST",
                data: $('#product-add-form').serialize(),
                success: function (data) {
                    if (containsValidationErrors(data)) {
                        $('#save-exit-btn').removeClass("disabled");
                        $('#save-exit-btn').removeClass("loading");
                    } else {
                        window.location.href = baseUrl + "vendor";
                    }
                },
                error: function (data) {
                    $('#save-exit-btn').removeClass("disabled");
                    $('#save-exit-btn').removeClass("loading");
                }
            });
        } else {
            $('#save-exit-btn').removeClass("disabled");
            $('#save-exit-btn').removeClass("loading");
            toastr.error($('#fill-all-fields').val());
        }

    });

    $('#update-exit-btn').on('click', function (e) {
        e.preventDefault();
        $('#update-exit-btn').addClass("disabled");
        $('#update-exit-btn').addClass("loading");

        var x = $('#code-input').validate()
            & $('#countryInput').validateCountry()
            & $('#productTypeInput').validateProductType()
            & $('#brandInput').validateBrand()
            & $('#name-input').validate()
            & $('#ean-input').validate()
            & $('#price-input').validate()
            & $('#photo-id1').validateImage1()
            & $('#description-input').validate();


        if (x) {
            var url = "/api/v1/product/update";
            $.ajax({
                url: baseUrl + url,
                type: "POST",
                data: $('#product-add-form').serialize(),
                success: function (data) {
                    if (containsValidationErrors(data)) {
                        $('#update-exit-btn').removeClass("disabled");
                        $('#update-exit-btn').removeClass("loading");
                    } else {
                        window.location.href = baseUrl + "vendor/products";
                    }
                },
                error: function (data) {
                    $('#update-exit-btn').removeClass("disabled");
                    $('#update-exit-btn').removeClass("loading");
                }
            });
        } else {
            $('#update-exit-btn').removeClass("disabled");
            $('#update-exit-btn').removeClass("loading");
            toastr.error($('#fill-all-fields').val());
        }

    });

    $('#add-brand').on('click', function (e) {
        e.preventDefault();
        $('#add-brand-modal').modal('show');
    });

    $('#add-product-type').on('click', function (e) {
        e.preventDefault();
        $('#add-product-type-modal').modal('show');
    });

    $('#add-location').on('click', function (e) {
        e.preventDefault();
        $('#add-product-location-modal').modal('show');
    });

    $('#add-brand-button').on('click', function (e) {
        e.preventDefault();
        if ($('#brandName').validate()) {
            var url = "/api/v1/brand/add";
            $.ajax({
                url: baseUrl + url,
                type: "POST",
                data: $('#brand-form').serialize(),
                success: function (data) {
                    $('#add-brand-modal').modal('hide');
                    var obj = {};
                    obj[data.brandName] = data.id;
                    setDinamicOptions('#brand-select', obj);
                },
                error: function (response) {
                    if (response.status == '302') {
                        toastr.error("Duplicate entry !");
                    } else {
                        toastr.error("Failed to save !");
                    }
                }
            });
        } else {
            toastr.error("Brand name required !");
        }
    });

    $('#add-product-type-button').on('click', function (e) {
        e.preventDefault();

        if ($('#typeName').validate()) {
            var url = "/api/v1/product-type/add";
            $.ajax({
                url: baseUrl + url,
                type: "POST",
                data: $('#product-type-form').serialize(),
                success: function (data) {
                    $('#add-product-type-modal').modal('hide');
                    var obj = {};
                    obj[data.typeName] = data.id;
                    setDinamicOptions('#product-type-select', obj);
                },
                error: function (response) {
                    if (response.status == '302') {
                        toastr.error("Duplicate entry !");
                    } else {
                        toastr.error("Failed to save !");
                    }
                }
            });
        } else {
            toastr.error("Name required !");
        }
    });

    $('#add-product-category-button').on('click', function (e) {
        e.preventDefault();

        if ($('#categoryName').validate()) {
            var url = "/api/v1/product-category/add";
            $.ajax({
                url: baseUrl + url,
                type: "POST",
                data: $('#product-category-form').serialize(),
                success: function (data) {
                    $('#add-category-modal').modal('hide');
                    var obj = {};
                    obj[data.categoryName] = data.id;
                    setDinamicOptions('#category-select', obj);
                },
                error: function (response) {
                    if (response.status == '302') {
                        toastr.error("Duplicate entry !");
                    } else {
                        toastr.error("Failed to save !");
                    }
                }
            });
        } else {
            toastr.error("Name required !");
        }
    });

    $('#add-product-location-button').on('click', function (e) {
        e.preventDefault();


        var flag = $('#store-id-input').validate()
            & $('#l_countryInput').validatePLCountry()
            & $('#company-name-input').validate()
            & $('#manager-input').validate()
            & $('#manager-contact-input').validate()
            & $('#assistant-input').validate()
            & $('#assistant-contact-input').validate()
            & $('#address-line-1').validate()
            & $('#city').validate()
            & $('#zipcode').validate()
            & $('#state').validate()
            & $('#location-contact').validate()
            & $('#open-from').validate()
            & $('#open-to').validate();

        if (flag) {
            var url = "/api/v1/vendor/add/location";
            $.ajax({
                url: baseUrl + url,
                type: "POST",
                data: $('#product-location-form').serialize(),
                success: function (data) {
                    $('#add-product-location-modal').modal('hide');
                    var obj = {};
                    obj[data.companyName] = data.id;
                    setDinamicOptions('#country', obj);
                },
                error: function (response) {
                    if (response.status == '302') {
                        toastr.error("Duplicate entry !");
                    } else {
                        toastr.error("Failed to save !");
                    }
                }
            });
        } else {
            toastr.error("Name required !");
        }
    });

    function setDinamicOptions(selector, options) {
        var att = "data-dinamic-opt";
        $(selector).find('[' + att + ']').remove();
        var html = $(selector + '-menu').html();
        for (key in options) {
            html += '<div class="item" data-value="' + options[key] + '" ' + att + '>' + key + '</div>';
        }
        $(selector + '-menu').html(html);
        $(selector).dropdown();
    }

    (function ($) {
        $.fn.validate = function () {
            var success = true;
            if ($(this).val() == '') {
                $(this).css('border', '1px solid red');
                success = false;
            } else
                $(this).css('border', '1px solid #898989');

            return success;
        }

    }(jQuery));

    (function ($) {
        $.fn.validateCountry = function () {
            var success = true;
            if ($(this).val() == '') {
                $('#country').css('border', '1px solid red');
                success = false;
            } else
                $('#country').css('border', '1px solid #898989');

            return success;
        }

    }(jQuery));


    (function ($) {
        $.fn.validatePLCountry = function () {
            var success = true;
            if ($(this).val() == '') {
                $('#l_country').css('border', '1px solid red');
                success = false;
            } else
                $('#l_country').css('border', '1px solid #898989');

            return success;
        }

    }(jQuery));

    (function ($) {
        $.fn.validateProductType = function () {
            var success = true;
            if ($(this).val() == '') {
                $('#product-type-select').css('border', '1px solid red');
                success = false;
            } else
                $('#product-type-select').css('border', '1px solid #898989');

            return success;
        }

    }(jQuery));

    (function ($) {
        $.fn.validateBrand = function () {
            var success = true;
            if ($(this).val() == '') {
                $('#brand-select').css('border', '1px solid red');
                success = false;
            } else
                $('#brand-select').css('border', '1px solid #898989');

            return success;
        }

    }(jQuery));

    (function ($) {
        $.fn.validateImage1 = function () {
            var success = true;
            if ($(this).val() == '') {
                $('#image-upload-1').css('border', '1px solid red');
                success = false;
            } else
                $('#image-upload-1').css('border', '1px solid #898989');

            return success;
        }

    }(jQuery));

    (function ($) {
        $.fn.validateImage2 = function () {
            var success = true;
            if ($(this).val() == '') {
                $('#image-upload-2').css('border', '1px solid red');
                success = false;
            } else
                $('#image-upload-2').css('border', '1px solid #898989');

            return success;
        }

    }(jQuery));

    (function ($) {
        $.fn.validateImage3 = function () {
            var success = true;
            if ($(this).val() == '') {
                $('#image-upload-3').css('border', '1px solid red');
                success = false;
            } else
                $('#image-upload-3').css('border', '1px solid #898989');

            return success;
        }

    }(jQuery));

});


function containsValidationErrors(response) {
    $('.error-msg').each(function () {
        $(this).text('');
        $(this).hide();
    });

    if (response !== null && response.errorMessageList !== null && typeof response.errorMessageList !== 'undefined') {
        var errorMessages;
        for (var i = 0; i < response.errorMessageList.length; i++) {

            if (!!errorMessages) {
                errorMessages += response.errorMessageList[i].message + "</br>";
            } else {
                errorMessages = response.errorMessageList[i].message + "</br>";
            }
            toastr.error(response.errorMessageList[i].message);

        }
        if (!!errorMessages) {
            $('#confirmTerms').html(errorMessages);
            $('#confirmTerms').show();
        }
        return true;
    }
    return false;
}


$(document).ready(function () {
    $('.terms-modal').on('click', function (e) {
        e.preventDefault();
        $('#musician-terms').modal('show');
    });
    $('.ambassador-calendar .col-6:last-child label').on('click', function (e) {
        e.preventDefault();
        $('.ambassador-calendar .col-6:last-child label').removeClass('active');
        $(this).addClass('active');
    });

    $('#upload-photo-id').on('click', function (e) {
        e.preventDefault();
        $('#upload-modal').modal('show');
    });

});

var options = {
    minCropBoxWidth: 198,
    minCropBoxHeight: 200,
    imageSmoothingEnabled: false,
    imageSmoothingQuality: 'high',
    aspectRatio: 198 / 200,
    preview: $('#img2')
};

$('#upload-button').click(function () {
    $('#inputImage').click();
});

function uploadFile() {
    var uploadImageContainer = $("#id-upload-container").find("span");
    var url = baseUrl + "/user/registration/photo";

    $('#image').cropper('getCroppedCanvas', {fillColor: '#FFFFFF'}).toBlob(function (blob) {
        var formData = new FormData();

        formData.append('photo-id', blob);

        $.ajax(url, {
            method: "POST",
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function (data) {
                uploadImageContainer.find('img').attr('src', data.url);
                $("#photo-id").val(data.id);

                $('#upload-modal').modal('hide');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 400)
                    showNotifyErrorMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";

            }
        });
    });
}

$("#description-input").htmlarea({
    css: baseUrl + "css/jHtmlArea/jHtmlArea.Editor.css",
    toolbar: ["bold", "italic", "underline", "|", "orderedlist", "unorderedlist", "|", "indent", "outdent", "|",
        "justifyleft", "justifycenter", "justifyright",
        "p", "h1", "h2", "h3", "h4", "h5", "h6"
    ]
});