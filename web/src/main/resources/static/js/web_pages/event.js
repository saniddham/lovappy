/**
 * Created by Tejaswi Venupalli on 9/13/2017
 */

$(document).ready(function () {

    var eventRegister = $('#user-event-attend');
    var eventRegisterLoggedOut = $('#user-event-attend-logged-out');
    var deleteLocationImage = $('#delete-image-button');
    var deleteLocationImageModal = $('#event-location-image-delete');
    $('#event-descripionTextArea').hide();
    $('#confirm-edit-event').hide();
    $('#event-attend-confirm').hide();
    $('#admission-cost-edit-field').hide();
    $('#edit-cost-confirm').hide();
    $('#attendees-edit-field').hide();
    $('#edit-attendees-confirm').hide();
    $("#upload-photos-link").hide();
    deleteLocationImageModal.hide();

    var $eventTitleTxt = $("#event-title-txt");
    var $eventTitleEditBtn = $("#edit-event-title-img");
    var $eventTitleEditTxt = $("#event-title-edit-txt");
    var $eventTitleEditBtnConfirm = $("#edit-event-title-img-confirm");

    $eventTitleEditTxt.hide();
    $eventTitleEditBtnConfirm.hide();

    eventRegisterLoggedOut.on('click', function () {
        event.preventDefault();
        $('#events-loggedout-attend-modal').modal('show');
    });

    eventRegister.on('click', function (e) {
        event.preventDefault();
        var eventId = $('#event_id-register').val();
        calculatePrice(getCalcPriceData());
        $('#square-payment-form').modal('show');
        /*        $('#event-attend-confirm').show();
         $("#proceed-event-payment").click(function () {
         calculatePrice(getCalcPriceData());
         $('#event-attend-confirm').hide();
         $('#square-payment-form').modal('show');
         });
         $("#payment-not-now-btn").click(function () {
         location.reload();
         });*/
    });

    deleteLocationImage.on('click', function () {
        event.preventDefault();
        deleteLocationImageModal.show();
        var imageId = $('#location-image-id').val();
        var eventId = $('#eventIdForPictures').val();

        $('#delete-confirm').on('click', function () {
            deleteLocationImageModal.hide();
            $.ajax({
                url: baseUrl + "/api/v1/events/location-pictures/delete/" + eventId,
                type: "POST",
                success: function (data) {
                    $("#popup-message-title").text("Done");
                    $("#popup-message-text").text("Image Cancelled Successfully");
                    $('#message-popup').modal({
                        closable: false,
                        onApprove: function () {
                            location.reload();
                        }
                    }).modal('show');
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });

        $('#delete-cancel').on('click', function () {
            location.reload();
        });
    });


    $('#events-checkuser').click(function () {
        var userId = $('#loggedInuserId').val();
        console.log(userId);
        if (userId === '' || jQuery.type(userId) === null) {
            $('#events-loggedout-modal').modal('show');
        } else {
            window.location.href = baseUrl + 'ambassador/signup';
        }
    });

    $('#upload-photos-link').on('click', function (e) {
        e.preventDefault();
        $('#upload-event-photos').trigger('click');

    });

    // $("#edit-location-pictures-id").on('click', function (e) {
    //     e.preventDefault();
    //     $('#edit-location-pictures').trigger('click');
    // });

    $('#edit-location-pictures').on('change', function (e) {
        var formData = new FormData();
        formData.append('event_location_picture', this.files[0]);
        var eventId = $('#eventIdForPictures').val();
        $.ajax({
            url: baseUrl + "/api/v1/events/update/upload_location-pictures/" + eventId,
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (data) {
                console.log("Test");
                $("#popup-message-title").text("Done");
                $("#popup-message-text").text("Event Location Picture updated successfully");
                $('#message-popup').modal({
                    closable: false,
                    onApprove: function () {
                        location.reload();
                    }
                }).modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 400)
                    showNotifyErrorMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";

            }
        });
    });

    $('#upload-event-photos').on('change', function (e) {

        var formData = new FormData();
        console.log($(this).get(0).files.length);
        for (var i = 0; i < $(this).get(0).files.length; i++) {
            formData.append('files', $(this).get(0).files[i]);
        }
        var eventId = $('#eventIdForPictures').val();

        $.ajax({
            url: baseUrl + "/api/v1/events/upload/upload-event-pictures/" + eventId,
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (data) {
                console.log("success");
                $('#upload-success').show();
                $('#picture-count').text(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });

    });

    $('#update-gallery').on('click', function (e) {
        e.preventDefault();
        var eventId = $('#eventIdForUpdate').val();
        window.location.href = baseUrl + '/events/gallery/details/' + eventId;
    });

    <!--region Open and Close modals-->
    $('.edit-btn, .events-content-detail a img').click(function () {
        var selectedArea = $(this).data("edit");
        loadDatepickerEventDate();
        loadDatepickerStartTime();
        loadDatepickerFinishTime();
        $(".modal[data-edit='" + selectedArea + "']").modal({
            autofocus: false,
        }).modal('show');

    });

    /*Reload a specifiv div content*/
    function reloadDiv(divName) {
        var container = document.getElementById(divName);
        var content = container.innerHTML;
        container.innerHTML = content;
    }

    /*Set date field value according to the selected Date*/
    $("#datetimepicker").change(function () {
        var bla = $('#datetimepicker').val();
        $('.date-value').html(bla);
    });
    $("#datetimepicker-two").change(function () {
        var bla2 = $('#datetimepicker-two').val();
        $('.date-value-two').html(bla2);
    });
    $("#datetimepicker-event").change(function () {
        var bla3 = $('#datetimepicker-event').val();
        $('.date-value-event').html(bla3);
    });


    //Edit evet date/time
    var $profileFormLoaderDateTime = $('#btn-save-edited-datetime i');
    $("#btn-save-edited-datetime").click(function () {
        $profileFormLoaderDateTime.show();
        $.ajax({
            url: baseUrl + "/api/v1/events/update",
            type: "POST",
            data: $('#event-datetime-form').serialize(),
            success: function (data) {
                if (containsValidationErrors(data)) {
                    $profileFormLoaderDateTime.hide();
                } else {
                    $profileFormLoaderDateTime.hide();
                    $("#popup-message-title").text("Done");
                    $("#popup-message-text").text("Event Date & time updated successfully.");
                    $('#message-popup').modal({
                        closable: false,
                        onApprove: function () {
                            location.reload();
                        }
                    }).modal('show');
                }
            },
            error: function (data) {
                $profileFormLoader.hide();
                console.log(data);
            }
        });
    });

    //Edit Event Location
    var $profileFormLoaderLoader = $('#btn-save-edited-location i');
    $("#btn-save-edited-location").click(function () {
        $profileFormLoaderLoader.show();
        console.log($('#event-location-form').serialize());
        $.ajax({
            url: baseUrl + "/api/v1/events/update",
            type: "POST",
            data: $('#event-location-form').serialize(),
            success: function (data) {
                if (containsValidationErrors(data)) {
                    $profileFormLoaderLoader.hide();
                } else {
                    $profileFormLoaderLoader.hide();
                    $("#popup-message-title").text("Done");
                    $("#popup-message-text").text("Event Location updated successfully.");
                    $('#message-popup').modal({
                        closable: false,
                        onApprove: function () {
                            location.reload();
                        }
                    }).modal('show');
                }
            },
            error: function (data) {
                $profileFormLoaderLoader.hide();
                console.log(data);
            }
        });
    });

    //Replace div with textbox to update description
    $('#edit-event').click(function () {

        $('#event-descripionTextArea').show();
        $('#event-descripionText').hide();
        $('#edit-event').hide();
        $('#confirm-edit-event').show();
    });

    //Edit the Event Description
    $('#confirm-edit-event').click(function () {
        console.log($('#eventDescriptionForm').serialize());
        $.ajax({
            url: baseUrl + "/api/v1/events/update",
            type: "POST",
            data: $('#eventDescriptionForm').serialize(),
            success: function (data) {
                if (containsValidationErrors(data)) {
                    console.log("Unable to update");
                } else {
                    $("#popup-message-title").text("Done");
                    $("#popup-message-text").text("Event Description updated successfully.");
                    $('#message-popup').modal({
                        closable: false,
                        onApprove: function () {
                            location.reload();
                        }
                    }).modal('show');
                }
            },
            error: function (data) {
                console.log(data);
            }
        });

    });

    //Edit the Event Title
    $eventTitleEditBtn.click(function () {
        $eventTitleTxt.hide();
        $eventTitleEditTxt.show();
        $eventTitleEditBtn.hide();
        $eventTitleEditBtnConfirm.show();
    });
    $eventTitleEditBtnConfirm.click(function () {
        console.log($('#eventTitleForm').serialize());
        $.ajax({
            url: baseUrl + "/api/v1/events/update",
            type: "POST",
            data: $('#eventTitleForm').serialize(),
            success: function (data) {
                if (containsValidationErrors(data)) {
                    console.log("Unable to update");
                } else {
                    $("#popup-message-title").text("Done");
                    $("#popup-message-text").text("Event Title updated successfully.");
                    $('#message-popup').modal({
                        closable: false,
                        onApprove: function () {
                            location.reload();
                        }
                    }).modal('show');
                }
            },
            error: function (data) {
                console.log(data);
            }
        });

    });

    $('#edit-cost').click(function () {
        $('#admission-cost-display-field').hide();
        $('#edit-cost').hide();
        $('#admission-cost-edit-field').show();
        $('#edit-cost-confirm').show();
    });

    //Edit event Cost
    $('#edit-cost-confirm').click(function () {
        //console.log($('#edit-admissioncost-attendees-form').serialize());
        $.ajax({
            url: baseUrl + "/api/v1/events/update",
            type: "POST",
            data: $('#edit-admissioncost-attendees-form').serialize(),
            success: function (data) {
                if (containsValidationErrors(data)) {
                    console.log("Cost update fail");
                    $('#cost-upload-success').show();
                    $('#content-message').text("Admission cost update Failed");
                } else {
                    console.log("Cost update success");
                    $("#popup-message-title").text("Done");
                    $("#popup-message-text").text("Admission Cost updated successfully.");
                    $('#message-popup').modal({
                        closable: false,
                        onApprove: function () {
                            location.reload();
                        }
                    }).modal('show');
                }
            },
            error: function (data) {
                console.log(data);
            }
        });

    });

    $('#edit-attendees').click(function () {
        $('#attendees-edit-field').show();
        $('#edit-attendees-confirm').show();
        $('#attendees-display-field').hide();
        $('#edit-attendees').hide();
    });

    //Event event attendees limit
    $('#edit-attendees-confirm').click(function () {
        //console.log($('#edit-admissioncost-attendees-form').serialize());
        $.ajax({
            url: baseUrl + "/api/v1/events/update",
            type: "POST",
            data: $('#edit-admissioncost-attendees-form').serialize(),
            success: function (data) {
                if (containsValidationErrors(data)) {
                    console.log("Attendees update fail");
                    $('#cost-upload-success').show();
                    $('#content-message').text("Attendees update Failed");
                } else {
                    console.log("Cost/Attendees update success");
                    $("#popup-message-title").text("Done");
                    $("#popup-message-text").text("Attendees updated successfully.");
                    $('#message-popup').modal({
                        closable: false,
                        onApprove: function () {
                            location.reload();
                        }
                    }).modal('show');
                }
            },
            error: function (data) {
                console.log(data);
            }
        });

    });

    //Select date from date picker
    /*    $('#calendar-demo').datepicker({

     onSelect: function (dateText) {
     console.log("input's current value: " + this.value);
     localStorage.setItem("selected_date", this.value);
     $.ajax({
     url: baseUrl + "/events?pickedDate=" + this.value,
     cache: false,
     success: function (html) {
     var resultHtml = document.open("text/html", "replace");
     resultHtml.write(html);
     resultHtml.close();
     console.log("check here "+localStorage.getItem("selected_date"));
     $("#calendar-demo").datepicker( "setDate", new Date('2014-12-12') );
     }

     });
     }
     });*/
    // display calendar
    /*Select date on date changed*/

    var pickedDate = $("#picked-date-value").val();
    var date = new Date(pickedDate ? pickedDate : new Date());

    $('#calendar-demo').dcalendar({selected: date});

    $('#calendar-demo').dcalendarpicker({
        dateFormat: 'dd-mm-yyyy'
    }).on('selectdate', function (e) {

        console.log(e.date);
        localStorage.setItem("selected_date", e.date);
        $.ajax({
            url: baseUrl + "/events/browse?pickedDate=" + e.date,
            cache: false,
            success: function (html) {
                var resultHtml = document.open("text/html", "replace");
                resultHtml.write(html);
                resultHtml.close();
                console.log(localStorage.getItem("selected_date"));
            }

        });
    });

    $('#share-btn-facebook').click(function () {
        console.log("facebookig");
        share('facebook');
    });

    $('#share-btn-twitter').click(function () {
        console.log("tweeting");
        share('twitter');
    });
    $('#share-btn-google').click(function () {
        console.log("google")
        share('google');
    });
    $('#share-btn-linkedin').click(function () {
        console.log("linkeninnin");
        share('linkedin');
    });
    $('#share-btn-pinterest').click(function () {
        console.log("pinteresssst");
        share('pinterest');
    });

    //share Section
    $('#share-email-article').click(function () {
        $(".modal[data-edit='share-email-article']").modal("show");
    });

    //Share Event social media
    function share(socialType) {
        console.log("share " + postId);
        $.ajax({
            url: baseUrl + "/api/v1/events/" + postId + "/share/" + socialType,
            type: "POST",
            data: {},
            processData: false,

            success: function () {
                console.log("Shared succcesfully");
            },
            error: function () {
            }
        });

    }

    /*

     $('#showLocation i').on('click', function (e) {
     console.log("show location");
     $('#showLocationMap').show();

     var google_key = $('#googleKey').val();
     var google_url = "https://maps.googleapis.com/maps/api/js?callback=setGoogleMapLoaded&key=" + google_key;

     (function () {
     var a = document.createElement('script');
     a.type = 'text/javascript';
     a.async = true;
     a.defer = true;
     a.src = google_url;
     var s = document.getElementsByTagName('script')[0];
     s.parentNode.insertBefore(a, s);
     })();

     }); */

    //Triggered when an select option is changed
    $(function () {
        $("#speed").selectmenu({
            change: function (event, ui) {

                if (navigator.geolocation) {

                    navigator.geolocation.getCurrentPosition(function (position) {

                        var $radius = parseInt(ui.item.value);
                        var $latitude = position.coords.latitude;
                        var $longtitude = position.coords.longitude;

                        console.log(ui.item.value);
                        console.log($latitude);
                        console.log($longtitude);

                        $('#radius-search').val($radius);
                        $('#latitude-search').val($latitude);
                        $('#longitude-search').val($longtitude);

                        $('#search-radius-form').submit();

                    });
                }
            }
        });
    });

    // changing nav elements to show pop up for EVENT_AMBASSADOR only role
    var isAmbassadorOnly = $('#isAmbassadorOnly').val();

    if (isAmbassadorOnly === '1') {
        $('.main-nav ul li').each(function () {
            var field = $(this).find('a').text();
            if (field.toLowerCase() === 'likes' || field.toLowerCase() === 'my map') {
                $(this).find('a').attr('class', 'ambassadorOnlyPopup');
            }
        });
        $('.responsive-nav ul li').each(function () {
            var field = $(this).find('a').text();
            if (field.toLowerCase() === 'likes' || field.toLowerCase() === 'my map') {
                $(this).find('a').attr('class', 'ambassadorOnlyPopup');
            }
        });
    }

    $('.ambassadorOnlyPopup').on('click', function (e) {
        e.preventDefault();
        $('#ambassador-modal').modal('show');
    });


    var date = new Date();
    var eventDate = new Date($('#eventDateForUpload').val());
    var start = new Date(eventDate.getFullYear() + '/' + (eventDate.getMonth() + 1) + '/' + eventDate.getDate() + ' ' + $('#eventStartTime').val());
    //var finish = new Date(eventDate.getFullYear() + '/' + (eventDate.getMonth() + 1) + '/' + eventDate.getDate() + ' ' + $('#eventFinishTime').val());
    if (date >= start) {
        $('#upload-photos-link').show();
    }

    $("#eventDescription").htmlarea({
        toolbar: ["bold", "italic", "underline", "|", "orderedlist", "unorderedlist", "|", "indent", "outdent", "|",
            "justifyleft", "justifycenter", "justifyright"
        ]
    });

});

function processPaymentForm(nonce) {
    $.ajax({
        url: baseUrl + 'api/v1/events/user/attend',
        type: "POST",
        data: {
            event_id: eventId,
            nonce: nonce,
            coupon: $("#coupon-code").val()
        },
        success: function (noOfAttendees) {
            $("#popup-message-title").text("Done");
            $("#popup-message-text").text("Payment Made Successfully");
            $('#message-popup').modal({
                closable: false,
                onApprove: function () {
                    $('#event-confirmed').show();
                    $('#event-attend').hide();
                    $('#event-confirmed').find('label').text(noOfAttendees + ' attending');
                }
            }).modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 400)
                showValidationMessages(jqXHR.responseJSON);
            else if (jqXHR.status === 401)
                window.location.href = baseUrl + "login";
            else if (jqXHR.status === 500)
                $.notify("Order not executed", {
                    globalPosition: 'bottom right',
                    className: "error",
                    autoHide: true
                });
        },
        complete: function (jqXHR, textStatus) {
            $requestNonceBtn.removeClass("loading disabled");
        }
    });
}

//Cancel Event by event id
function toEventDetail(eventId) {
  window.location.href = baseUrl + "events/" + eventId;
}
//Cancel Event by event id
function cancelEvent(eventIdCancel) {

    var $profileFormLoaderCancel = $('#btn-event-cancel i');
    $('#cancel-event-confirmation').modal({
        autofocus: false,

    }).modal('show');

    $('#btn-event-cancel').click(function () {
        $profileFormLoaderCancel.show();
        console.log("The id is " + eventIdCancel);
        $.ajax({
            url: baseUrl + "/api/v1/events/cancel/" + eventIdCancel,
            type: "POST",
            success: function (data) {
                if (containsValidationErrors(data)) {
                    $profileFormLoaderCancel.hide();
                    $("#btn-event-cancel").unbind('click');
                } else {
                    $profileFormLoaderCancel.hide();
                    $("#popup-message-title").text("Done");
                    $("#popup-message-text").text("Event Cancelled Successfully");
                    $('#message-popup').modal({
                        closable: false,
                        onApprove: function () {
                            location.reload();
                        }
                    }).modal('show');
                }
            },
            error: function (data) {
                $profileFormLoaderCancel.hide();
                console.log(data);
            }
        });

    });

    $('#btn-event-not-cancel').click(function () {
        eventIdCancel = null;
    });

}

function loadDatepickerEventDate() {
    $('#datetimepicker-event').datetimepicker({

        format: 'm/d/Y',
        timepicker: false
    })
}

function loadDatepickerStartTime() {
    $('#datetimepicker').datetimepicker({
        datepicker: false,
        format: 'h:i A'
    })
}

function loadDatepickerFinishTime() {
    $('#datetimepicker-two').datetimepicker({
        datepicker: false,
        format: 'h:i A'
    })
}


function containsValidationErrors(response) {
    $('.error-msg').each(function () {
        $(this).text('');
        $(this).hide();
    });

    if (response !== null && response.errorMessageList !== null && typeof response.errorMessageList !== 'undefined') {
        for (var i = 0; i < response.errorMessageList.length; i++) {
            var item = response.errorMessageList[i];
            var $controlGroup = $('#' + item.fieldName + 'FormGroup');
            var $errorMsg = $controlGroup.find('.error-msg');
            $errorMsg.text(item.message);
            $errorMsg.show();
        }
        return true;
    }
    return false;
}


function getCalcPriceData() {
    return {
        event_id: $('#event_id-register').val(),
        coupon: $("#coupon-code").val()
    };
}