/**
 * Created by Darrel Rayen on 10/20/17.
 */
$(document).ready(function () {
    var verificationFormDiv = $('#verification-form');
    var codeVerificationDiv = $('#verification-code');
    var submitPlaceDeal = $('#submit-place-deal-btn');
    var backButton = $('#back-btn-change-number');
    var placeID = $('#dating-place-id').val();
    var messageId;

    $('#place-id').val(placeID);
    $('#place-id1').val($('#datin-place-id').val());
    codeVerificationDiv.hide();

    submitPlaceDeal.on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: baseUrl + 'dating/places/deals/save',
            type: "POST",
            data: $('#add-place-deal-form').serialize(),
            success: function (data) {
                console.log("successful");
                $("#popup-message-title").text("Done");
                $("#popup-message-text").text("Thank you for submitting a Deal. You will be notified by email when it is approved.");
                $('#message-popup').modal({
                    closable: false,
                    onApprove: function () {
                        window.location.href = baseUrl + 'dating/places/' + placeID;
                    }
                }).modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                submitPlaceDeal.removeClass("loading");
                submitPlaceDeal.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401) {
                    //window.location.href = baseUrl + "login";
                }
                console.log("unsuccessful");

            }
        });
    });

    var submitPlaceClaim = $('#verify-owner-btn');
    var place = $('#place').val();
    var user = $('#user').val();
    $('#place-test').val(place);
    $('#user-test').val(user);
    submitPlaceClaim.on('click', function (e) {
        var phoneNumber = $('#phone').intlTelInput("getNumber");
        $('#phone').val(phoneNumber);
        e.preventDefault();
        $.ajax({
            url: baseUrl + 'api/v1/dating/places/claim/save',
            type: "POST",
            data: $('#owner-claim-form').serialize(),
            success: function (data) {
                console.log("successful");
                messageId = data.result;
                $('#message-id').val(data.result);
                $('#owner-phone-number').val($('#phone').val());
                verificationFormDiv.hide();
                codeVerificationDiv.show();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                submitPlaceDeal.removeClass("loading");
                submitPlaceDeal.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401) {
                    window.location.href = baseUrl + "login";
                }
                console.log("unsuccessful");
            }
        });
    });

    backButton.on('click', function (e) {
        verificationFormDiv.show();
        codeVerificationDiv.hide();
    });

    var verifyCodeButton = $('#verify-code-btn');
    $('#place-verify').val(place);
    $('#user-verify').val(user);

    verifyCodeButton.on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: baseUrl + 'api/v1/dating/places/claim/verify',
            type: "POST",
            data: $('#verification-code-form').serialize(),
            success: function (data) {
                console.log("owner verified");
                $("#popup-message-title").text("Done");
                $("#popup-message-text").text("Congratulations. You have successfully claimed your Dating Place");
                $('#message-popup').modal({
                    closable: false,
                    onApprove: function () {
                        if (data.result) {
                            if (!isNaN(data.result)) {
                                console.log("the id is " + data.result);
                                window.location.href = baseUrl + 'api/v1/dating/places/claim/google/' + data.result;
                            }
                        } else {
                            window.location.href = baseUrl + 'dating/places/' + place;
                        }
                    }
                }).modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                verifyCodeButton.removeClass("loading");
                verifyCodeButton.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401) {
                    window.location.href = baseUrl + "login";
                } else if (jqXHR.status === 409) {
                    $("#popup-message-title").text("Done");
                    $("#popup-message-text").text("Sorry.This place already been claimed. Please contact support");
                    $('#message-popup').modal({
                        closable: false,
                        onApprove: function () {
                            window.location.href = baseUrl + 'dating/places/' + place;
                        }
                    }).modal('show');
                }
                console.log("unsuccessful");
            }
        });
    });

    var submitPlaceBtn = $('#complete-owner-dating-place-btn');
    submitPlaceBtn.on('click', function (e) {
        e.preventDefault();
        submitPlaceBtn.addClass("loading");
        submitPlaceBtn.prop('disabled', true);
        $.ajax({
            url: baseUrl + 'api/v1/dating/places/google/complete',
            type: "POST",
            data: $('#dating-place-form').serialize(),

            success: function (data) {
                console.log("success");
                submitPlaceBtn.removeClass("loading");
                submitPlaceBtn.prop('disabled', false);
                $("#popup-message-title").text("Done");
                $("#popup-message-text").text("Your changes have been submitted. You will be notified by email when it is approved.");
                $('#message-popup').modal({
                    closable: false,
                    onApprove: function () {
                        window.location.href = baseUrl + 'dating/places/';
                    }
                }).modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                submitPlaceBtn.removeClass("loading");
                submitPlaceBtn.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401) {
                    window.location.href = baseUrl + "login";
                }
                console.log("unsuccessful");
            }
        });
    });

    var resendVerificationBtn = $('#resend-verification');
    resendVerificationBtn.on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: baseUrl + 'api/v1/dating/places/claim/resend/' + messageId,
            type: "POST",
            success: function (data) {
                console.log("success");
                $("#popup-message-title").text("Done");
                $("#popup-message-text").text("The verification code has been resent.Please check now.Thanks");
                $('#message-popup').modal({
                    closable: false,
                    onApprove: function () {
                        //window.location.href = baseUrl + 'dating/places/';
                    }
                }).modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401) {
                    window.location.href = baseUrl + "login";
                }
                console.log("unsuccessful");
            }
        });
    });


});
function hideErrorMessages() {
    $('.error-msg').each(function () {
        $(this).text('');
        $(this).hide();
    });
}