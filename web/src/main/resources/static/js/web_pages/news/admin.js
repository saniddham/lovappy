

$(document).ready( function () {
    $('#news_table').DataTable();
} );
$.validate({
    lang: 'en'
});
function getAllCategories(){
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: baseUrl+'/lox/news/admin/category/all',
        dataType: 'json',
        success: function(result) {
            console.log(result);
            var tbody="";
            $.each(result,function(index,obj){
                var status="<button  class='btn btn-success categoryStatusDeactive' id='statusBtn"+obj.categoryId+"' type='button'>Active</button>";

                if(obj.status == 0){
                    status= "<button class='btn btn-danger categoryStatusActive ' id='statusBtn"+obj.categoryId+"' type='button'>Inactive</button>";
                }
                tbody+="<tr>" +
                    "<td>"+obj.categoryId+"</td>" +
                    "<td>"+obj.categoryName+"</td>" +
                    "<td>"+obj.categoryDescription+"</td>" +
                    "<td>"+status+"</td>" +
                    "</tr>";


            });
            $("#categoryTableBody").html(tbody);

        }
    });
}

var elt = $('keywordPhrasestag');
elt.tagsinput({
    itemValue: 'value',
    itemText: 'text',
    typeaheadjs: {
        name: 'cities',
        displayKey: 'text',
        source: [{"value":1,"text":"Amsterdam","continent":"Europe"},{"value":4,"text":"Washington","continent":"America"},{"value":7,"text":"Sydney","continent":"Australia"},{"value":10,"text":"Beijing","continent":"Asia"},{"value":13,"text":"Cairo","continent":"Africa"}]
    }
});



$('.add_new').click(function () {
    $('.addnew').modal('show');
    getDropdowns();
});
$('.cancel-btn').click(function () {
    $('.addnew').modal('hide');
});

$('.manage_category').click(function () {
    $('.category_modal').modal('show');
    getAllCategories();

});
$('.cancel-btn-catgeory').click(function () {
    $('.category_modal').modal('hide');
});


function searchNews() {
    $.ajax({
        type: "get",
        contentType: "application/json",
        url: baseUrl+"/lox/news/admin/asynchronous/search?txtSearch="+$("#txtSearch").val(),
        dataType: 'json',
        timeout: 600000,
        cache: false,
        success: function (data) {

            $("#news_table tbody").destroy();
            $.each(data.newsStories, function( key, value ) {
                var statusString = "";
                if(value.newsStoryPosted == null) {
                    statusString = "<td><button class='pending' value='pending'>PENDING</button></td>";
                } else if(value.newsStoryPosted == true) {
                    statusString = "<td><button class='approved' value='approved'>APPROVED</button></td>";
                } else if(value.newsStoryPosted == false) {
                    statusString = "<td><button class='denied' value='denied'>DENIED</button></td>";
                }
                $("#news_table tbody").append("<tr><td>"+value.newsStoryId+"</td><td"+value.newsStoryTitle+"</td><td>"+value.categoryName+"</td>"+
                    "<td>"+value.newsStorySubmitDate+"</td>"+statusString+"<td><a class='detail detail-btn'"+
                    "href='http://localhost:8080/lovappy-web/news/newsGet?id='"+value.newsStoryId+">Detail</a></td></tr>");
            });
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
    });

}



    function newsStorySubmit() {
        var newsStoryDto = {};

        newsStoryDto = {
            newsStoryTitle : $("#title").val(),
            categoryId : $("#category").val(),
            //  categoryId : $("#categoryList").find(":selected").text(),
            newsStoryContain : $("#newsStoryContain").val(),

            imageDescription : $("#imageDescription").val(),
            imagekeyword : $("#keywords").val(),

            advancedOptionSeoTitle : $("#seoTitle").val(),
            advancedOptionMetaDescription : $("#metaDescription").val(),
            advancedOptionKeywordPhrases : $("#keywordPhrasestag").val(),
        }

            var formData = new FormData();

            formData.append("videoUploadFile", $("#uploadFileVideo")[0].files[0]);
            formData.append("newsPicture", $("#uploadNewsPicture")[0].files[0]);
            formData.append("newsAuthorPicture", $("#uploadFile")[0].files[0]);
            formData.append('dto', JSON.stringify(newsStoryDto));
            // formData.append('dto', new Blob([JSON.stringify(newsStoryDto)], { type: "application/json"}));
            //    var formData = new FormData($("#addNewNewsForm")[0]);
            //   JSON.stringify(newsStoryDto);
            $("#addNewNewsBtn").attr("disabled","disabled");
            $('body').pleaseWait();
            $.ajax({
                url: baseUrl+'/lox/news/admin/asynchronous/new',
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    console.log(response);
                    alert("Saved Successfully!");
                    getAllNews();
                    $('.addnew').modal('hide');
                    $("#addNewNewsForm")[0].reset();
                    $("#addNewNewsBtn").removeAttr("disabled");
                    $('body').pleaseWait('stop');

                },
                error:function(err){
                    alert("Something went wrong!");
                }
            });




    }

$("#addNewNewsForm").submit(function(e){
    e.preventDefault();
    newsStorySubmit();
});

function getDropdowns(){
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: baseUrl+'/lox/news/admin//asynchronous/drop-downs',
        dataType: 'json',
        success: function(result) {
            //console.log(result.newsCategories);
            var options="<option value=''>-select category-</option>";
            $.each(result.newsCategories,function(index,obj){
                options +="<option value='"+obj.categoryId+"'>"+ obj.categoryName+"</option>";
            });
            $("#category").html(options);

            var keywords="";
            $.each(result.newsStoryPhrases,function(index,obj){
                keywords +="<option value='"+obj.keywardId+"'>"+ obj.keywardPhrase+"</option>";
                // keywords.push(obj.keywardPhrase);
            });
            //$("#keywordPhrasestag").html(keywords);
        }
    });
}

function getAllNews(){
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: baseUrl+'/lox/news/admin/allNews',
        dataType: 'json',
        success: function(result) {
            console.log(result);
            var tbody="";
            $.each(result,function(index,obj){
                //console.log(obj.id);
                var status="<label class='btn btn-success'>Approved</label>";

                if(obj.posted == null){
                    status= "<button class='btn btn-warning active pending-btn' id='approvalBtn"+obj.id+"'>Pending</button>";
                }else if(obj.posted ==false){
                    status= "<label class='btn btn-danger'>Denied</label>";

                }
                tbody+="<tr>" +
                    "<td>"+obj.id+"</td><td>"+obj.title+"</td>" +
                    "<td>"+obj.newsCategory.categoryName+"</td>" +
                    "<td>"+formatDate(obj.submitDate)+"</td>" +
                    "<td>"+status+"</td>" +
                    "<td><button id='viewBtn"+obj.id+"' class='btn btn-primary active detail-btn ' type='button'>Detail</button></td>" +
                    "</tr>";

            });
            $("#newsTableBody").html(tbody);

        }
    });
}
getAllNews();

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

var vid = document.getElementById("myVideo");

function playVid() {

    vid.play();
}

function pauseVid() {
    vid.pause();
}
$('body').on('click','.detail-btn',function (e) {
    e.preventDefault();
    var id=$(this).attr('id');
    var strId= id.replace("viewBtn","");
    // $("#downloadVideo").attr("onclick","window.location='/lovappy-web/news/video/"+strId+"'");
    // alert(strId);
    //  alert(id);
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: baseUrl+'/lox/news/admin/get/'+strId,
        dataType: 'json',
        success: function(result) {
            $("#newsStoryTitleView").val(result.newsStoryTitle);
            $("#advancedOptionKeywordPhrasesView").val(result.advancedOptionKeywordPhrases);
            $("#advancedOptionMetaDescriptionView").val(result.advancedOptionMetaDescription);
            $("#advancedOptionSeoTitleView").val(result.advancedOptionSeoTitle);
            $("#categoryNameView").val(result.categoryName);
            $("#imageDescriptionView").val(result.imageDescription);
            $("#imagekeywordView").val(result.imagekeyword);
            $("#newsStoryTitleView").val(result.newsStoryTitle);
            $("#newsStoryContainView").val(result.newsStoryContain);
            // $("#newsImageView").val(result.image.url);
            // $("#newsImageAuthorView").val(result.authorImage.url);
            // $("#newsVideoView").val(result.authorImage.url);

console.log(result.image.url);
            $("#newsImageView").attr("src",result.image.url);
            $("#newsImageAuthorView").attr("src",result.authorImage.url);
            $("#myVideo > source").attr("src",result.video.url);
            $("#newsVideoDownload").attr("href",result.video.url);
            vid.load();

        }
    });
    $('.story_detail').modal('show');

});

$('.cancel-btn').click(function () {
    $('.story_detail').modal('hide');
});
//Approval for news publish
var newsStoryId=0;
$('body').on('click','.pending-btn',function () {
    var id=$(this).attr("id");
    newsStoryId= id.replace("approvalBtn","");
    $('.approval').modal('show');
});

$("#approvedBtn").click(function(){
    $.ajax({
        type: "GET",
        url: baseUrl+'/lox/news/admin/approval/'+newsStoryId+'/true',
        success: function(result) {
            setTimeout( getAllNews(),500);
            $('.approval').modal('hide');


        }
    });
});
$("#deniedBtn").click(function(){
    $.ajax({
        type: "GET",
        url: baseUrl+'/lox/news/admin/approval/'+newsStoryId+'/false',
        success: function(result) {

            setTimeout(getAllNews(),500);
            $('.approval').modal('hide');

        }
    });
});

// Manage Catergory
$("body").on("submit","#addCategoryForm",function(e){

    e.preventDefault();
    var categoryData=
        {
            "categoryName":$("#categoryName").val(),
            "categoryDescription":$("#categoryDescription").val()
        }

    $.ajax({
        contentType: "application/json",
        url: baseUrl+'/lox/news/admin/category/save', // url where to submit the request
        type : "POST", // type of action POST || GET
        dataType : 'json', // data type
        data : JSON.stringify(categoryData), // post data || get data
        success : function(result) {
            // you can see the result from the console
            // tab of the developer tools
            if(result !=""){
                alert("Successfully Added!");
                getAllCategories();
            }

        },
        error: function(xhr, resp, text) {
            console.log(xhr, resp, text);
        }
    })
});
$("body").on("keyup","#categoryName",function(){

    $.ajax({
        url: baseUrl+'/lox/news/admin/category/check/'+$("#categoryName").val(), // url where to submit the request
        type : "GET", // type of action POST || GET
        success : function(result) {
            // you can see the result from the console
            // tab of the developer tools
            //console.log(result);
            if(result != ""){
                $("#categoryExistMsg").show();
                $("#addNewCategoryBtn").attr("disabled",true);
            }else{
                $("#categoryExistMsg").hide();
                $("#addNewCategoryBtn").attr("disabled",false);
            }
        },
        error: function(xhr, resp, text) {
            console.log(xhr, resp, text);
        }
    });
});

$("body").on("click",".categoryStatusActive",function(){
    var strId=$(this).attr("id");
    var id=strId.replace("statusBtn","");

    $.ajax({
        url: baseUrl+'/lox/news/admin/category/status/'+id+'/'+1, // url where to submit the request
        type : "GET", // type of action POST || GET
        success : function(result) {
            // you can see the result from the console
            // tab of the developer tools
            //console.log(result);
            if(result != ""){
                getAllCategories();
                alert("Ativated Successfully");

            }
        },
        error: function(xhr, resp, text) {
            console.log(xhr, resp, text);
        }
    });
});
$("body").on("click",".categoryStatusDeactive",function(){
    var strId=$(this).attr("id");
    var id=strId.replace("statusBtn","");

    $.ajax({
        url: baseUrl+'/lox/news/admin/category/status/'+id+'/'+0, // url where to submit the request
        type : "GET", // type of action POST || GET
        success : function(result) {
            // you can see the result from the console
            // tab of the developer tools
            //console.log(result);
            if(result != ""){
                getAllCategories();
                alert("Deativated Successfully");

            }
        },
        error: function(xhr, resp, text) {
            console.log(xhr, resp, text);
        }
    });
});


$("body").on("keyup","#title",function(){

    $.ajax({
        url: baseUrl+'/lox/news/admin/title/check/'+$("#title").val(), // url where to submit the request
        type : "GET", // type of action POST || GET
        success : function(result) {
            // you can see the result from the console
            // tab of the developer tools
            //console.log(result);
            if(result == true){
                $("#titleExistMsg").show();
                $("#addNewNewsBtn").attr("disabled",true);
            }else{
                $("#titleExistMsg").hide();
                $("#addNewNewsBtn").attr("disabled",false);
            }
        },
        error: function(xhr, resp, text) {
            console.log(xhr, resp, text);
        }
    });
});