/**
 * Created by Manoj Janaka on 17/02/2018
 */

$(document).ready(function () {

    var submitBtn = $("#register-vendor");

    $("input:checkbox").change(function () {
        var ischecked = $(this).is(':checked');
        if (!ischecked) {
            submitBtn.prop('disabled', true);
            submitBtn.css('cursor', 'not-allowed');
        }
        else {
            submitBtn.prop('disabled', false);
            submitBtn.css('cursor', 'pointer');
        }

    });

    submitBtn.on('click', function (e) {
        e.preventDefault();
        submitBtn.addClass("disabled");
        submitBtn.addClass("loading");

        var x = $('#vendor-name').validate()
            & $('#company-name').validate()
            & $('#contact-phone').validate()
            & $('#vendor-email').validate()
            & $('#alternative-contact-person').validate()
            & $('#alternative-contact-email').validate()
            & validatePassword()
            & $('#vendor-confirm-password').validate()
            & $('#vendor-password').validate();


        var z = $('#confirm-form').validateConfirmForm();

        if (x && z) {
            var url = "/vendor/signup";
            $.ajax({
                url: baseUrl + url,
                type: "POST",
                data: $('#reg-form').serialize(),
                success: function (data) {
                    if (containsValidationErrors(data)) {
                        submitBtn.removeClass("disabled");
                        submitBtn.removeClass("loading");
                    } else {
                        $('#confirm-form').prop('checked', false);
                        $('#vendor-modal').show();
                        window.setTimeout(function () {
                            window.location.href = baseUrl + "vendor/mobile/verification" + data.result;
                        }, 1000);
                        submitBtn.removeClass("disabled");
                        submitBtn.removeClass("loading");
                    }
                },
                error: function (data) {
                    submitBtn.removeClass("disabled");
                    submitBtn.removeClass("loading");
                    console.log(data);
                }
            });
        } else {
            submitBtn.removeClass("disabled");
            submitBtn.removeClass("loading");
            toastr.error($('#fill-all-fields').val());
        }

    });

    $('#events-checkuser').click(function () {
        var userId = $('#loggedInuserId').val();
        console.log(userId);
        if (userId === '' || jQuery.type(userId) === null) {
            $('#events-loggedout-modal').modal('show');
        } else {
            window.location.href = baseUrl + '/vendor/signup';
        }
    });

    (function ($) {
        $.fn.validate = function () {
            var success = true;
            if ($(this).val() == '') {
                $(this).css('border', '1px solid red');
                success = false;
            } else
                $(this).css('border', '1px solid #898989');

            return success;
        }

    }(jQuery));

    (function ($) {
        $.fn.validatePhone = function () {
            var success = true;
            if ($(this).val() == '') {
                $('#phoneNumber').css({
                    'display': 'inline-block',
                    'background-color': '#fff',
                    'margin-top': '-9px'
                }).html('Please provide your phone number');
                success = false;
            } else
                $('#phoneNumber').html('');

            return success;
        }

    }(jQuery));

    (function ($) {
        $.fn.validateConfirmForm = function () {
            var success = true;
            if (!$(this).is(':checked')) {
                $('#confirmTerms').css({
                    'display': 'inline-block',
                    'background-color': '#fff'
                }).html('Please agree to proceed');
                success = false;
            } else
                $('#confirmTerms').html('');

            return success;
        }

    }(jQuery));

    function validatePassword() {
        var password = $("#vendor-password").val();
        var confirmPassword = $("#vendor-confirm-password").val();
        if (password != confirmPassword) {
            $("#vendor-password").css('border', '1px solid red');
            $("#vendor-confirm-password").css('border', '1px solid red');
            toastr.error($('#password-not-equal').val());
            return false;
        }
        $("#vendor-password").css('border', '1px solid #898989');
        $("#vendor-confirm-password").css('border', '1px solid #898989');
        return true;
    }

});


function containsValidationErrors(response) {
    $('.error-msg').each(function () {
        $(this).text('');
        $(this).hide();
    });

    if (response !== null && response.errorMessageList !== null && typeof response.errorMessageList !== 'undefined') {
        var errorMessages;
        for (var i = 0; i < response.errorMessageList.length; i++) {

            if (!!errorMessages) {
                errorMessages += response.errorMessageList[i].message + "</br>";
            } else {
                errorMessages = response.errorMessageList[i].message + "</br>";
            }
            toastr.error(response.errorMessageList[i].message);

        }
        if (!!errorMessages) {
            $('#confirmTerms').html(errorMessages);
            $('#confirmTerms').show();
        }
        return true;
    }
    return false;
}


$(document).ready(function () {
    $('.terms-modal').on('click', function (e) {
        e.preventDefault();
        $('#musician-terms').modal('show');
    });
    $('.ambassador-calendar .col-6:last-child label').on('click', function (e) {
        e.preventDefault();
        $('.ambassador-calendar .col-6:last-child label').removeClass('active');
        $(this).addClass('active');
    });

    $('#upload-photo-id').on('click', function (e) {
        e.preventDefault();
        $('#upload-modal').modal('show');
    });

});

var options = {
    minCropBoxWidth: 198,
    minCropBoxHeight: 200,
    imageSmoothingEnabled: false,
    imageSmoothingQuality: 'high',
    aspectRatio: 198 / 200,
    preview: $('#img2')
};

$('#upload-button').click(function () {
    $('#inputImage').click();
});

function uploadFile() {
    var uploadImageContainer = $("#id-upload-container").find("span");
    var url = baseUrl + "/user/registration/photo";

    $('#image').cropper('getCroppedCanvas', {fillColor: '#FFFFFF'}).toBlob(function (blob) {
        var formData = new FormData();

        formData.append('photo-id', blob);

        $.ajax(url, {
            method: "POST",
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function (data) {
                uploadImageContainer.find('img').attr('src', data.url);
                $("#photo-id").val(data.id);

                $('#upload-modal').modal('hide');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 400)
                    showNotifyErrorMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";

            }
        });
    });
}
