
    $(document).ready(function () {

        var $registerForm = $('#register-form');
        var $registerLoader = $('#submit-homepage-reg').siblings('i');

        $registerForm.on('submit', function(e) {
            e.preventDefault();
            $registerLoader.show();

            $.ajax({
                url: baseUrl + "register",
                type: "POST",
                data: $registerForm.serialize(),
                success: function (data) {
                    if(containsValidationErrors(data)) {
                        $registerLoader.hide();
                    } else {
                        if(data.result === "no_love") {
                            window.location.href = baseUrl + 'registration/gender';
                        }
                        else {
                            window.location.href = baseUrl + 'registration/couples';
                        }
                        /*$("#verification-email-popup").modal('show');
                        $registerForm.find("input[type=text], input[type=password]").val("");
                        $registerLoader.hide();*/
                    }
                },
                error: function (data) {
                    $registerLoader.hide();
                    console.log(data);
                }
            });
        });

        $("#btn-ok").click(function () {
            $("#verification-email-popup").modal('hide');
        })


        $(".social-login").on('click', function (e) {
         e.preventDefault();

        var inLove =$('#coupleRadio').is(':checked');
        var provider = $(this).attr("provider");
        if(inLove) {
        socialLoginHack();
        window.location.href = baseUrl + 'login/' +provider;
        } else {
        window.location.href = baseUrl + 'login/' +provider;
        }


        });

        function socialLoginHack() {
            $.ajax({
                url: baseUrl + "register/social",
                type: "POST",
                data: {},
                async: false,
                success: function () {

                      return true;
                },
                error: function (e) {
                return false;
                }
            });
        }
    });


    function containsValidationErrors(response) {
        $('.error-msg').each(function () {
            $(this).text('');
        });

        if (response !== null && response.errorMessageList !== null && typeof response.errorMessageList !== 'undefined') {
            for (var i = 0; i < response.errorMessageList.length; i++) {
                var item = response.errorMessageList[i];
                //var $controlGroup = $('#' + item.fieldName + 'FormGroup');
                //form.find($controlGroup).addClass('has-error');
                //form.find($controlGroup).find('.error-msg').text(item.message);
                var $control = document.getElementsByName(item.fieldName);
                $($control).siblings('.error-msg').text(item.message);
            }
            return true;
        }
        return false;
    }