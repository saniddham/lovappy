$(document).ready(function () {
    var creditPrice = 1.99;
    jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
    jQuery('.quantity').each(function () {
        var spinner = jQuery(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min'),
            max = input.attr('max');

        btnUp.click(function () {
            var oldValue = parseFloat(input.val());
            var newVal = oldValue;
            if (oldValue >= max) {
                newVal = oldValue;
            } else {
                newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
            var totalPrice = newVal * creditPrice;
            $("#totalPrice").text(totalPrice);
        });

        btnDown.click(function () {
            var oldValue = parseFloat(input.val());
            var newVal = oldValue;
            if (oldValue <= min) {
                newVal = oldValue;
            } else {
                newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
            var totalPrice = newVal * creditPrice;
            $("#totalPrice").text(totalPrice);
        });

    });

    $("#buy-btn").click(function () {
        if (!isLoggedIn) {
            window.location.href = baseUrl + "login";
            return;
        }

        calculatePrice(getCalcPriceData($("#quantity-input").val()));
        $('#square-payment-form').modal('show');
    });

    $("#send-btn").click(function () {
        if (!isLoggedIn) {
            window.location.href = baseUrl + "login";
            return;
        }


        var quantityToSend = $("#quantity-input").val();
        var balance = parseInt($("#balance").text());
        if (balance - quantityToSend >= 0) {
            sendCreditsToUser(userId);
        } else {
            $("#popup-message-title").text("Done");
            $("#popup-message-text").text("You don't have enough credits to send. Do you want to buy?");
            $('#message-popup').modal({
                closable: false,
                onApprove: function () {
                    calculatePrice(getCalcPriceData(quantityToSend - balance));
                    $('#square-payment-form').modal('show');
                }

            }).modal('show');

        }
    });


});

function getCalcPriceData(quantity) {
    return {
        coupon: $("#coupon-code").val(),
        quantity: quantity
    };
}

function getBalanceToBuy() {
    if (sendCredit) {
        var quantityToSend = $("#quantity-input").val();
        var balance = parseInt($("#balance").text());
        return quantityToSend - balance;
    }
    return $("#quantity-input").val();
}
function processPaymentForm(nonce) {
    $.ajax({
        url: baseUrl + "credits/order",
        type: "POST",
        data: {
            nonce: nonce,
            quantity: getBalanceToBuy(),
            coupon: $("#coupon-code").val()
        },
        success: function (data) {

            if (sendCredit) {
                sendCreditsToUser(userId);
            } else {
                $("#popup-message-title").text("Done");
                $("#popup-message-text").text("Payment Made Successfully");
                $('#message-popup').modal({
                    closable: false,
                    onApprove: function () {
                        location.reload();
                    }
                }).modal('show');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 400)
                showValidationMessages(jqXHR.responseJSON);
            else if (jqXHR.status === 401)
                window.location.href = baseUrl + "login";
            else if (jqXHR.status === 500)
                $.notify("Order not executed", {
                    globalPosition: 'bottom right',
                    className: "error",
                    autoHide: true
                });
        },
        complete: function (jqXHR, textStatus) {
            $requestNonceBtn.removeClass("loading disabled");
        }
    });
}


function sendCreditsToUser(userId) {
    $.ajax({
        url: baseUrl + "/api/v1/credits/" + userId,
        type: "POST",
        data: {
            balance: $("#quantity-input").val(),
        },
        success: function (data) {

            $("#popup-message-title").text("Done");
            $("#popup-message-text").text("Credits were sent Successfully");
            $('#message-popup').modal({
                closable: false,
                onApprove: function () {
                    location.reload();
                }
            }).modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 400)
                showValidationMessages(jqXHR.responseJSON);
            else if (jqXHR.status === 401)
                window.location.href = baseUrl + "login";
            else if (jqXHR.status === 500)
                $.notify("Order not executed", {
                    globalPosition: 'bottom right',
                    className: "error",
                    autoHide: true
                });
        },
        complete: function (jqXHR, textStatus) {
            $requestNonceBtn.removeClass("loading disabled");
        }
    });
}