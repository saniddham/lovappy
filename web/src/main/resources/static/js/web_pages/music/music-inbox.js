var soundId;
var sound;
var prevUrl;
$(document).ready(function () {



    //download music
    $(".downloadMusic").click(function () {

        var musicID = $(this).attr("data-music-id");
        downloadMusicExchange(musicID);
    });

    //play music
    var songPlayImage = "/images/icons/lovdrops_play_icons/btn_play.png";
    var songPauseImage = "/images/icons/lovdrops_play_icons/btn_pause.png";
    var playBtn = $(".playBtn");
    var playLovDropButton;

    playBtn.click(function () {
        var url = $(this).attr("data-url");
        var userId = $(this).attr("data-id");


        playLovDropButton = $("#" + userId);

        play(url, userId);
    });

    function play(url, userId) {


        if (sound) {
            if (sound.playing()) {


                sound.stop(soundId);

                if (prevUrl === url) {
                    return;
                }
            } else {

                if (prevUrl === url) {

                    sound.play(soundId);
                    return;
                }
            }

        }
        prevUrl = url;
        sound = new Howl({
            src: [url],
            html5: true, // Force to HTML5 so that the audio can stream in (best for large files).
            onplay: function () {
                playLovDropButton.attr("src", baseUrl + songPauseImage);

                //                $("#listenerAddress").atrr("text", user.)
            },
            onload: function () {
                playLovDropButton.attr("src", baseUrl + songPlayImage);


            },
            onend: function () {
                playLovDropButton.attr("src", baseUrl + songPlayImage);

            },
            onpause: function () {
                playLovDropButton.attr("src", baseUrl + songPlayImage);

            },
            onstop: function () {
                playLovDropButton.attr("src", baseUrl + songPlayImage);

            }
        });

        if (sound.playing()) {

            soundId = sound.stop();
        } else {

            soundId = sound.play();

        }
    }

});