$(document).ready(function () {

    $('.terms-modal').on('click', function (e) {
        e.preventDefault();
        $('#vendor-terms').modal('show');
    });
    $('#btnClose').on('click', function (e) {
        e.preventDefault();
        $('#vendor-terms').modal('hide');
    });

    $("input").prop('required', true);

    // Initialize form validation on the  form.
    // It has the name attribute
    $("#music-submit-form").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
            coverPhoto: "required",
            file: "required",
            text: {
                required: true,
                // Specify that email should be validated
                // by the built-in "email" rule
                text: true
            },
            radio: {
                required: true
            }
        },
        // Specify validation error messages
        messages: {
            coverPhoto: "Please add cover photo",
            file: "Please upload a song",
            text: {
                required: "field is required"
            },
            radio: "field is required"
        }
//        // Make sure the form is submitted to the destination defined
//        // in the "action" attribute of the form when valid
//        submitHandler: function(form) {
//            form.submit();
//        }
    });
    var loadingHtml = "Submit <span class=\"ui button loading\"></span>";
    var saveBtn = $("#musicSubmitBtn");
    saveBtn.on('click', function (e) {
        // e.preventDefault();


        saveBtn.prop("disabled", true);
        saveBtn.find("i").show();
        $("#music-submit-form").submit();
    });

    var bar = $('.bar');
    var percent = $('#percent');
    var progress = $('#progress');

    $("#music-submit-form").submit(function (e) {

        var percentage = 0;
        progress.show();
        function doStuff() {
            if (percentage < 80) {
                var percentVal = percentage + '%';
                bar.width(percentVal);
                percent.html(percentVal);
                percentage = percentage + Math.floor(Math.random() * 11);
            }
        }

        setInterval(doStuff, 5000);

        $(this).ajaxSubmit({
            type: 'POST',
            crossDomain: true,
            headers: {
                Accept: "application/json; charset=utf-8"
            },

            url: baseUrl + "api/v1/music",
            success: function (data) {

                var percentVal = '100%';
                bar.width(percentVal);
                percent.html(percentVal);

                saveBtn.prop("disabled", false);
                saveBtn.find("i").hide();
                $("#popup-message-title").text("Successfully Submitted");
                $("#popup-message-text").text("Your Song is pending approval. You will receive a notification by email.");
                $('#message-popup').modal({
                    closable: false,
                    onApprove: function () {
                        window.location.href = baseUrl + "music";
                    }
                }).modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                saveBtn.prop("disabled", false);
                saveBtn.find("i").hide();
                progress.hide();
                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });

    });

    $("#upload-music-cover").on('change', function () {
        readURL(this, $('#coverPhoto'));
    });


});