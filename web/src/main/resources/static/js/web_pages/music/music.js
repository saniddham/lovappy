$(document).ready(function () {


    $("input[name='filter']").change(function () {
        $("#searchSong").attr("placeholder", $(this).attr("data-placeholder"));
    });
});

function downloadMusic(musicID) {
    $.ajax({
        url: baseUrl + "api/v1/music/" + musicID + "/generate-download-link",
        type: "GET",
        data: {},
        success: function (data) {
            var win = window.open(data);
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                alert('Please allow popups for this website');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 400)
                showValidationMessages(jqXHR.responseJSON);
            else if (jqXHR.status === 401)
                window.location.href = baseUrl + "login";
            else if (jqXHR.status === 403)
                $("#popup-message-title").text("Access Denied");
            $("#popup-message-text").text("You have reached download limit!");
            $('#message-popup').modal({
                closable: false,
                onApprove: function () {
                    location.reload();
                }
            }).modal('show');
        }
    });
}

function downloadMusicExchange(musicExchangeId) {
    $.ajax({
        url: baseUrl + "api/v1/music/" + musicExchangeId + "/generate-download-link-for-exchange",
        type: "GET",
        data: {},
        success: function (data) {
            var win = window.open(data);
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                alert('Please allow popups for this website');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 400)
                showValidationMessages(jqXHR.responseJSON);
            else if (jqXHR.status === 401)
                window.location.href = baseUrl + "login";
            else if (jqXHR.status === 403)
                $("#popup-message-title").text("Access Denied");
            $("#popup-message-text").text("You have reached download limit!");
            $('#message-popup').modal({
                closable: false,
                onApprove: function () {
                    location.reload();
                }
            }).modal('show');
        }
    });
}