$(document).ready(function() {

    $(document).on("change", "input[name='toggle-pic-access']", function() {
        var userID = $(this).attr("data-id");

        if (this.checked) {

            $(this).prop("checked", enableOrDisablePicAccess(userID, "POST"));


        } else {
            if (enableOrDisablePicAccess(userID, "DELETE"))
                $(this).prop("checked", false);


        }
    });


$('input[name=photo-access-user]').on("change", function() {
  window.location.href = baseUrl + 'member/profiles/access?filter=' + $(this).val() +'&photo-access='+$('input[name=photo_access]:checked').val();
});

        $('#save-changes').on("click", function(){
          if(callAccessProfilePicAPI($('input[name=photo_access]:checked').val(), $("#managedCounter").val()))
          location.reload();
        });

     $('input[name=photo_access]').on("change", function() {
       var value = $('input[name=photo_access]:checked').val();

       if(value === 'MANAGED') {
              $(".private-access").hide();
                 $(".public-access").hide();
       $(".managed-access").show();

       } else if(value === 'PUBLIC'){
       $(".managed-access").hide();
       $(".private-access").hide();
          $(".public-access").show();
       }else if(value === 'PRIVATE'){
               $(".managed-access").hide();

                  $(".public-access").hide();
                   $(".private-access").show();
               }
     });
    function enableOrDisablePicAccess(userId, method) {
        $.ajax({
            url: baseUrl + "/api/v1/user/profile/" + userId + "/photo/access",
            type: method,
            data: {},
            success: function() {
                return true;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });
    }

 function callAccessProfilePicAPI(access, counter) {
            $.ajax({
                url: baseUrl + "/api/v1/user/profile/photo/access",
                type: "POST",

                data:  JSON.stringify({"access":access, "maxTransactions":counter}),
                contentType: "application/json; charset=utf-8",
                success: function () {
                location.reload();
               return true;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                               if(jqXHR.status === 400)
                                   showValidationMessages(jqXHR.responseJSON);
                               else if(jqXHR.status === 401)
                                   window.location.href = baseUrl + "login";
                           }
            });
    }
});