
var options = {
    viewMode:1,
    minCropBoxWidth: 482,
    minCropBoxHeight: 237,
    imageSmoothingEnabled: false,
    imageSmoothingQuality: 'high',
    aspectRatio: 482 / 237,
    preview: $('#img2')
};
$(document).ready(function() {
   $('#profile-img').on('click', function(e) {
        e.preventDefault();
        $('#image').cropper('destroy');
        options = {
            minCropBoxWidth: 198,
            minCropBoxHeight: 200,
            imageSmoothingEnabled: false,
            imageSmoothingQuality: 'high',
            aspectRatio: 198 / 200,
            preview: $('#img2')
        };
        $('#image').cropper('destroy').cropper(options);
        $('#upload-modal').modal('show');
        blogImageUpload = false;
    });
var saveBtn = $("#save-profile");
       $("#userForm").submit(function(e) {

            e.preventDefault();
            $(this).ajaxSubmit({
                type: 'POST',
                crossDomain: true,
                async: false,
                headers: {
                    Accept: "application/json; charset=utf-8"
                },

                url: baseUrl + "profile/save",
                success: function(data) {

                    saveBtn.prop("disabled", false);
                    saveBtn.find("i").hide();
                    location.reload();

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    saveBtn.prop("disabled", false);
                    saveBtn.find("i").hide();
                    console.log(jqXHR);
                    if (jqXHR.status === 400) {
                          $("#popup-message-title").text("Failed!");
                                        $("#popup-message-text").text("Failed to update settings!");
                                        $('#message-popup').modal({
                                            closable: false,
                                            onApprove: function() {
                                                location.reload();
                                            }
                                        }).modal('show');
                                        }
                    else if (jqXHR.status === 401)
                        window.location.href = baseUrl + "login";
                }



            });

        });


});