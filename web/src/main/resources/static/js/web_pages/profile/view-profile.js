/**
 * Created by hasan on 17/6/2017
 */

var lovdropPlayImage = "/images/play_block.png";
var lovdropPauseImage = "/images/pause_viewprof.png";
var messagePlay = "/images/icons/new-read-msg.png";//"/images/read_msg.png";
var messagePause = "/images/icons/new-pause-msg.png";//"/images/pause_msg.png";
var blockBtn;
var likeBtn;
var pausedText = "#" + userID + " Paused";

//Slide showd
var slideIndex = 1;
$(document).ready(function () {



     $('#toggle-pic-access').change(function() {
             if($(this).is(':checked')) {

   $(this).prop("checked",  enableOrDisablePicAccess( userID, "POST"));

            } else{

     if(enableOrDisablePicAccess( userID, "DELETE"))
                   $(this).prop("checked",  false);
             }
        });

     function enableOrDisablePicAccess(userId, method) {
            $.ajax({
                url: baseUrl + "/api/v1/user/profile/" + userId + "/photo/access",
                type: method,
                data: {},
                success: function () {
               return true;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                               if(jqXHR.status === 400)
                                   showValidationMessages(jqXHR.responseJSON);
                               else if(jqXHR.status === 401)
                                   window.location.href = baseUrl + "login";
                           }
            });
    }

    blockBtn = $("#block-btn");
    blockBtn.data('action', isBlocked ? 'unblock' : 'block');
    likeBtn = $("#like-profile-btn");



    $('.icons-map').click(function () {
        var selectedArea = $(this).data("edit");
        $(".modal[data-edit='" + selectedArea + "']").modal({
            autofocus: false,
        }).modal('show');
    });


    var button_profile_ok = "#profile-ok";
    $(button_profile_ok).click(function () {
        var pathname = window.location.pathname;
        window.location.href = pathname;
    });

    var button_goto_likes = "#goto_likes";
    $(button_goto_likes).click(function () {
        window.location.href = '/like/mylikes';
    });

    var button_cont_browse = "#cont_browse";
    $(button_cont_browse).click(function () {
        hideModal("likeModal");
    });


    /*
     *Like Other Member
     */

    likeBtn.click(function () {
        if (isLiked) {
            unlikeTo();
        } else {
            likeTo();
        }

    });


    /*
     *Unlike Other Member
     */
//    likeBtn.click(function () {
//        unlikeTo();
//    });


    /*
     *Send Private Message
     */
    $("#send-private-message-btn").click(function () {
        $(".modal[data-edit='private-message']").modal("show");
    });




// Get the modal
    var modal = document.getElementById('pictModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    //var modalImg = document.getElementById("img01");

   $("#profilePic").click(function () {
        modal.style.display = "block";
        showSlides(1);
        //modalImg.src = this.src;
    });
    $("#hand_pic").click(function () {
        modal.style.display = "block";
        showSlides(2);
        //modalImg.src = this.src;
    });
    $("#foot_pic").click(function () {
        modal.style.display = "block";
        showSlides(3);
        // modalImg.src = this.src;
    });
    $("#legs_pic").click(function () {
        modal.style.display = "block";
        showSlides(4);
        //modalImg.src = this.src;
    });

    $(".messages-area div h3").click(function () {
        if (!$(".messages-content").is(':animated')) {
            $(".messages-content").toggle("swing");
        }
    });
    $(".received-music-area div h3").click(function () {
        if (!$(".received-music-content").is(':animated')) {
            $(".received-music-content").toggle("swing");
        }
    });


    $('#unlock-profile-link').click(function () {
        $.ajax({
            url: baseUrl + "api/v1/user/profile/unlock_profile_picture",
            type: "POST",
            data: {user_id: userID},

            success: function (data, textStatus, jqXHR) {},

            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 500 && jqXHR.responseText === "INSUFFICIENT_COMMUNICATIONS")
                    $(".modal[data-edit='locked-profile']").modal("show");
                else if(jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            },

            complete: function(jqXHR, textStatus) {
                if (jqXHR.status === 201) {
                    showPopupMessage("Email Sent",
                        "An email has been sent to the user requesting unlocking " +
                        "of the profile picture, you will be emailed about his reply");
                } else if (jqXHR.status === 409) {
                    if (jqXHR.responseJSON.status === "PENDING") {
                        showPopupMessage("Already Sent", "You have already sent a request");
                    } else if (jqXHR.responseJSON.status === "APPROVED") {
                        showPopupMessage("Request Approved",
                            "Your request has been approved, you should see the profile photo now");
                    } else if (jqXHR.responseJSON.status === "REJECTED") {
                        showPopupMessage("Request Rejected", "Your request has been rejected")
                    }
                }
            }
        });
    });

    //trim text
    trimMusicTexts();
    /*
     *Play LovDrop
     */
    var lovdropProfilePauseImage = "/images/lovdrop-playbar/view-profile-pause.png"
    var lovdropProfilePlayImage = "/images/lovdrop-playbar/view-profile-play.png"

    var playLovDropButton = $("#play_lovdrop");
    var playLovDropImage = playLovDropButton.find("img");

    var playLovDropProfileSpan = $("#profile_play_bar");
    var playLovDropProfileImage = $("#playPauseId");
    var playLovDropProfileText = playLovDropProfileSpan.find("p");

    var sound = new Howl({
        src: [recordFile],
        html5: true, // Force to HTML5 so that the audio can stream in (best for large files).
        onplay: function () {
            playLovDropImage.attr("src", baseUrl + lovdropPauseImage);
            playLovDropProfileImage.attr("src", baseUrl + lovdropProfilePauseImage);
            playLovDropProfileText.text("#" + userID + " Playing");

        },
        onload: function () {
            playLovDropImage.attr("src", baseUrl + lovdropPlayImage);
            playLovDropProfileImage.attr("src", baseUrl + lovdropProfilePlayImage);
            //playLovDropProfileText.text(pausedText);
        },
        onend: function () {
            playLovDropImage.attr("src", baseUrl + lovdropPlayImage);
            playLovDropProfileImage.attr("src", baseUrl + lovdropProfilePlayImage);
            playLovDropProfileText.text(pausedText);

        },
        onpause: function () {
            playLovDropImage.attr("src", baseUrl + lovdropPlayImage);
            playLovDropProfileImage.attr("src", baseUrl + lovdropProfilePlayImage);
            playLovDropProfileText.text(pausedText);
        },
        onstop: function () {
            playLovDropImage.attr("src", baseUrl + lovdropPlayImage);
            playLovDropProfileImage.attr("src", baseUrl + lovdropProfilePlayImage);
            playLovDropProfileText.text(pausedText);
            listenToLovdrop(userID);
        }
    });

    playLovDropButton.click(function () {
        if (sound.playing()) {
            sound.stop();
        } else {
            sound.play();
        }
    });


    playLovDropProfileImage.click(function () {
        if (sound.playing()) {
            sound.stop();
        } else {
            sound.play();
        }
    });


    /*
     *Private Messages
     */
    var userInfoBar;
    var genderImg;
    var viewProfileLink;

    //userInfoBar = document.getElementById("user-info-bar");
    //genderImg = $("#user-info-bar").siblings("img");
    //viewProfileLink = $("#view-profile-link");

    /**
     * Player class containing the state of our playlist and where we are in it.
     * Includes all methods for playing, skipping, updating the display, etc.
     * @param {Array} playlist Array of objects with playlist song details ({title, file, howl}).
     */
    var Player = function (playlist) {
        this.playlist = playlist;
        this.index = 0;

        // Display the title of the first track.
        /*userInfoBar.innerHTML = ' ' + playlist[0].user_age + ' • ' +
         playlist[0].user_city + ', ' + playlist[0].user_state + ' (' + playlist[0].user_language + ')';
         genderImg.attr("src", playlist[0].user_gender === 'FEMALE' ?
         baseUrl + "/images/female_icon.png" : baseUrl + "/images/boy_icon.png");
         viewProfileLink.attr("href", baseUrl + "member/" + playlist[0].user_id);*/

        // Setup the playlist.
        playlist.forEach(function (song) {
            var div = document.getElementById(song.id);
            div.onclick = function () {
                player.skipTo(playlist.indexOf(song));
            };
        });
    };

    Player.prototype = {
        /**
         * Play a song in the playlist.
         * @param  {Number} index Index of the song in the playlist (leave empty to play the first or current).
         */
        play: function (index) {
            var self = this;
            var sound;

            index = typeof index === 'number' ? index : self.index;
            var data = self.playlist[index];

            // If we already loaded this track, use the current one.
            // Otherwise, setup and load a new Howl.
            if (data.howl) {
                sound = data.howl;
            } else {
                sound = data.howl = new Howl({
                    src: [data.file],
                    html5: true, // Force to HTML5 so that the audio can stream in (best for large files).
                    onplay: function () {
                        stopAll(false);
                        //$("#below-title").show(250);
                        document.getElementById(data.id).src = baseUrl + messagePause;
                    },
                    onload: function () {
                    },
                    onend: function () {
                        //Prevent loop
                        if (index < (self.playlist.length - 1)) {
                            self.skip('right');
                            stopAll(false);
                        } else {
                            stopAll(true);
                        }
                    },
                    onpause: function () {
                    },
                    onstop: function () {
                        stopAll(true);
                    }
                });
            }

            // Begin playing the sound.
            sound.play();

            // Update the track display.
            /*userInfoBar.innerHTML = ' ' + data.user_age + ' • ' +
             data.user_city + ', ' + data.user_state + ' (' + data.user_language + ')';
             genderImg.attr("src", data.user_gender === 'FEMALE' ?
             baseUrl + "/images/female_icon.png" : baseUrl + "/images/boy_icon.png");
             viewProfileLink.attr("href", baseUrl + "member/" + data.user_id);*/

            // Show the pause button.
            /*if (sound.state() === 'loaded') {
             stopAll();
             document.getElementById(data.id).src = baseUrl + "/images/pause_msg.png";
             $("#below-title").show(250);
             } else {
             stopAll();
             }*/

            // Keep track of the index we are currently playing.
            self.index = index;
        },

        /**
         * Pause the currently playing track.
         */
        pause: function (hideBar) {
            var self = this;

            // Get the Howl we want to manipulate.
            var sound = self.playlist[self.index].howl;

            // Pause the sound.
            sound.pause();

            // Show the play button.
            stopAll(hideBar);
        },

        /**
         * Stop the currently playing track.
         */
        stop: function (hideBar) {
            var self = this;

            // Get the Howl we want to manipulate.
            var sound = self.playlist[self.index].howl;

            // Stop the sound.
            sound.stop();

            stopAll(hideBar);
        },

        /**
         * Skip to the next or previous track.
         * @param  {String} direction 'next' or 'prev'.
         */
        skip: function (direction) {
            var self = this;

            // Get the next track based on the direction of the track.
            var index = 0;
            if (direction === 'prev') {
                index = self.index - 1;
                if (index < 0) {
                    index = self.playlist.length - 1;
                }
            } else {
                index = self.index + 1;
                if (index >= self.playlist.length) {
                    index = 0;
                }
            }

            self.skipTo(index);
        },

        /**
         * Skip to a specific track based on its playlist index.
         * @param  {Number} index Index in the playlist.
         */
        skipTo: function (index) {
            var self = this;

            // Get the Howl we want to manipulate.
            var sound = self.playlist[self.index].howl;

            // Stop the current track.
            if (sound && sound.playing() && index === self.index) {
                self.stop(true);
            } else if (sound && sound.playing() && index !== self.index) {
                self.stop(false);
                self.play(index);
            } else {
                self.play(index);
            }
        }
    };


    // Setup our new audio player class and pass it the playlist.
    var playlistArray = [];
    for (var i = 0; i < messagesJson.length; i++) {
        var file = messagesJson[i].audioFileCloud.url;
        playlistArray.push({
            id: messagesJson[i].id,
            file: file,
            howl: null,
            user_age: messagesJson[i].fromUser.userProfile.age,
            user_state: messagesJson[i].fromUser.stateShort,
            user_city: messagesJson[i].fromUser.userProfile.address.city,
            user_language: messagesJson[i].fromUser.userProfile.speakingLanguagesAbb,
            user_gender: messagesJson[i].fromUser.userProfile.gender,
            user_id: messagesJson[i].fromUser.userProfile.userId
        });
    }


    var player = new Player(playlistArray);

    //make images square if they not
    $(".profile-images div img.member-pic").each(function () {
        if($(this).height() > $(this).width())
            $(this).css('height','auto');
        if($(this).width() > $(this).height())
            $(this).css('width','155');

    });



 blockBtn.click(function () {

        var action = $(this).data('action');
        if (action === 'block') {
            $(".modal[data-edit='block-modal']").modal("show");
        } else {
            userBlockUnblock();
        }

    });

      $('#modalBlockBtn').click(function () {
             blockUser(blockBtn, userID);
            $(".modal[data-edit='block-modal']").modal("hide");
        });
function userBlockUnblock() {
   var action = blockBtn.data('action');
    if (action === 'block') {
        $(".modal[data-edit='block-modal']").modal("show");
        return;
    }
    unBlockUser(blockBtn, userID);
}



    $("#flagUserBtn").click(function () {

        $(".modal[data-edit='flag-modal']").modal("show");

    });


    $('#submit-flag').click(function (e) {
                            e.preventDefault();
                            $('#flagForm').attr('action', baseUrl + '/member/'+userID+'/report');
                                $('#flagForm').submit();

             });


});

function hideModal(modalID) {
    var modal = document.getElementById(modalID);
    modal.style.display = "none";
}


function stopAll(hideBar) {
    $(".play_status_img").each(function () {
        $(this).attr("src", baseUrl + messagePlay);

        //if(hideBar)
        //$("#below-title").hide(250);
    });
}


function likeTo() {

    var me = $(this);

    if (me.data('requestRunning')) {
        return;
    }
    var likeImage = baseUrl + 'images/icons/like_them.png';
    var matchesImage = baseUrl + 'images/icons/match_icon.png';
    var modal = document.getElementById('likeModal');
    var like_img = document.getElementById('like_user');
    var captionText = document.getElementById("likeCaption");
    me.data('requestRunning', true);
    $.ajax({
        url: baseUrl + "/like/" + userID,
        type: "POST",
        data: "{}",
        processData: false,
        contentType: false,
        cache: false,
        success: function () {
            isLiked = true;


            //Matched
            if (isLikedMe) {
                console.log('set to unlike');
                $(".modal[data-edit='matched-modal']").modal("show");
                likeBtn.attr('src', matchesImage);
                isMatched = true;
                likeBtn.attr('title', 'Like');
            } else {
                console.log('set to like');
                $(".modal[data-edit='like-modal']").modal("show");
                likeBtn.attr('src', likeImage);
                likeBtn.attr('title', 'Unlike');
            }
        },
                    error: function (jqXHR, textStatus, errorThrown) {
                                       if(jqXHR.status === 400)
                                           showValidationMessages(jqXHR.responseJSON);
                                       else if(jqXHR.status === 401)
                                           window.location.href = baseUrl + "login";
                                   },

        complete: function () {
            me.data('requestRunning', false);
        }
    });
}


function unlikeTo() {

    var me = $(this);

    if (me.data('requestRunning')) {
        return;
    }
    var likeImage = baseUrl + 'images/icons/like.png';
    var likeMeImage = baseUrl + 'images/icons/like_you.png';
    console.log("UserId " + userID);
    me.data('requestRunning', true);
    $.ajax({
        url: baseUrl + "/like/" + userID,
        type: "DELETE",
        data: "{}",
        processData: false,
        contentType: false,
        cache: false,
        success: function () {
            isLiked = false;
//            var pathname = window.location.pathname;
//            window.location.href=pathname;
            if (isMatched) {
                likeBtn.attr('src', likeMeImage);
                isMatched = false;
                likeBtn.attr('title', 'Unlike');
            } else {
                likeBtn.attr('src', likeImage);
                isLikedMe = false;
                likeBtn.attr('title', 'Like');
            }
        },
                   error: function (jqXHR, textStatus, errorThrown) {
                                      if(jqXHR.status === 400)
                                          showValidationMessages(jqXHR.responseJSON);
                                      else if(jqXHR.status === 401)
                                          window.location.href = baseUrl + "login";
                                  },
        complete: function () {
            me.data('requestRunning', false);
        }
    });
}



function changeIsBlocked(userID, isBlocked) {
    for (var i in playlistArray) {
        if (playlistArray[i].user_id === userID) {
            playlistArray[i].is_blocked = isBlocked;
            break;
        }
    }
}


function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    console.log('showSlides' + n);
    slideIndex = n;
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
}
function trimMusicTexts() {


    $(".music-song span p.song-name").each(function () {
        $(this).text(trimText(6, $(this).text()));
    });
    $(".music-song span p.song-artist").each(function () {
        $(this).text(trimText(6, $(this).text()));
    });
    //$(".music-song span p:last").text(trimText(6, musicArtist));
}