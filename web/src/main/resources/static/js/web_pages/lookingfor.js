/**
 * Created by Eias Altawil on 9/9/2017
 */

$(document).ready(function () {

 $("#back-button").on('click', function(e) {
     e.preventDefault();
          window.location.href = baseUrl + "registration/gender";
    });
    var submitBtn = $("#submit-btn");

    submitBtn.on('click', function(e) {
        e.preventDefault();
        submitBtn.addClass("disabled");
        submitBtn.addClass("loading");

        $.ajax({
            url: baseUrl + "api/v1/registration/step2",
            type: "POST",
            data: $('#main-form').serialize(),
            success: function (data) {
                window.location.href = baseUrl + 'registration/personalitylifestyle';
                submitBtn.removeClass("disabled");
                submitBtn.removeClass("loading");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                submitBtn.removeClass("disabled");
                submitBtn.removeClass("loading");

                if(jqXHR.status === 400) {
                    showValidationMessages(jqXHR.responseJSON);
                    toastr.error("Please fill all required fields !!");
                }else if(jqXHR.status === 401) {
                    window.location.href = baseUrl + "login";
                }
            }
        });
    });
    
    //Set ft and in to "" when centimeter changes
    $("select[name='heightFt']").change(function() {
    	$("input[name='heightCM']").val('');
    });
    
    
});