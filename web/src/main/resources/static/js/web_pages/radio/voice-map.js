

function startDictation() {

    if (window.hasOwnProperty('webkitSpeechRecognition')) {

        var recognition = new webkitSpeechRecognition();

        recognition.continuous = false;
        recognition.interimResults = false;

        recognition.lang = "en-US";
        recognition.start();

        recognition.onresult = function(e) {
            var speechText = e.results[0][0].transcript;
            document.getElementById('transcript').value = speechText;

            recognition.stop();
            var sendInfo = {
                query_input: {
                    text: {
                        text: speechText,
                        language_code: "en-US"
                    }
                }
            };
            var receivedData;
            var bearer = "Bearer " + dialog_auth_token ;

            $.ajax({
                url: dialog_detect_intent,
                type: "POST",
                headers: {
                    "Content-Type":"application/json",
                    "Authorization":bearer
                },
                data: JSON.stringify(sendInfo),
                success: function (data) {
                    console.log("dialogflow send the data successfully");
                    console.log(data);
                    // data = JSON.parse(data);
                    receivedData = data.queryResult.parameters;
                    console.log(receivedData);
                    if (receivedData.Gender.length > 0){
                        var gender = receivedData.Gender[0];
                        if (typeof gender != 'undefined'){
                            gender = gender.trim();
                            if (gender == 'female'){
                                $("#filter-gender").val("FEMALE");
                            } else {
                                $("#filter-gender").val("MALE");
                            }
                        }

                    }

                    var height = receivedData.Height;
                    if (typeof height != 'undefined'){
                        height = height.trim();
                        $("#filter-height").val(height);

                    }

                    var breast = receivedData.Breast_size;
                    if (typeof breast != 'undefined'){
                        breast = breast.trim();
                        $("#filter-breast").val(breast);
                    }

                    var butt = receivedData.Butt_size;
                    if (typeof butt != 'undefined'){
                        butt = butt.trim();
                        $("#filter-butt").val(butt);
                    }

                    var minAge = receivedData.min_age;
                    if (typeof minAge != 'undefined'){
                        minAge = minAge;
                        $("#filter-age-from").val(minAge);
                    }

                    var maxAge = receivedData.max_age;
                    if (typeof maxAge != 'undefined'){
                        maxAge = maxAge;
                        $("#filter-age-to").val(maxAge);
                    }

                    var location = receivedData["geo-country"];
                    if (typeof location != 'undefined' && location != ''){
                        location = location.trim();
                        $("#last-address-search").val(location);
                        $.when( $( "#btn-search-map" ).trigger( "click" ) ).done(
                            $( "#set-location-btn" ).trigger( "click" )
                        );

                    }

                    pageNumber = 1;
                    useFilter = true;
                    excludedAdsIds = [];
                    searchLovstamps(true);

                },
                error: function (data) {
                    console.log("dialogflow request received errors");
                    console.log(data);
                    receivedData = data;
                }
            });


            // document.getElementById('filter-form').submit();
        };

        recognition.onerror = function(e) {
            recognition.stop();
        }

    }
}