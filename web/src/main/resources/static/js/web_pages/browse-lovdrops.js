/**
 * Created by Eias Altawil on 5/21/17
 */
var Player;
var player;
var playlistArray;
var timer;
var userInfo;
var genderImg;
var viewProfileLink;
var playBtn;
var userInfoBar;
var onAirIcon;
var progress;
var duration;
var blockBtn;
var likeBtn;
var currentPlayUserID;
var loadMoreBtn;
var pageNumber = 1;
var background = ['lovdrop-cover.jpg', 'lovdrop-cover-1.jpg', 'lovdrop-cover-2.jpg', 'lovdrop-cover-3.jpg', 'lovdrop-cover-4.jpg'];
var soundEffects;

var useFilter = false;
var excludedAdsIds = [];

$(document).ready(function() {

    timer = document.getElementById("timer");
    userInfo = document.getElementById("playing-user-info");
    genderImg = $("#playing-user-gender");
    viewProfileLink = $("#view-profile-link");
    playBtn = $("#playBtn");
    userInfoBar = $("#play-bar");
    onAirIcon = $(".on-air");
    progress = document.getElementById("progress");
    duration = document.getElementById("duration");
    blockBtn = $("#block-btn");
    likeBtn = $("#like-btn");
    loadMoreBtn = $("#load-more-btn");

    soundEffects = new Howl({
        src: [maleSoundBeforeUrl]
    });

    var elementA = document.createElement('script');
    elementA.type = 'text/javascript';
    elementA.async = true;
    elementA.defer = true;
    elementA.src = google_url;
    var elementScript = document.getElementsByTagName('script')[0];
    elementScript.parentNode.insertBefore(elementA, elementScript);

    $('#languages-select').dropdown();

    var surveyAnswerStatus = $('#surveyAnswerStatus');
    console.log('isAnswered: ' + surveyAnswerStatus.val());
    if (surveyAnswerStatus.length && !$('#hiddenSurveyPage').length && surveyAnswerStatus.val() !== "1" && loggedIn)
        $('#userSurveyModal').modal('show');

    //region Filter options selectors
    $('.filter-checkbox input[type=checkbox]').on("change", function() {
        if ($(this).is(':checked')) {
            $('.filter-options').append(
                '<span class="option-slice" data-text="' + $(this).data('text') + '">' + $(this).data('text') + ' <span class="filter-close">x</span></span>'
            );
        } else {
            $(".option-slice[data-text='" + $(this).data('text') + "']").remove();
        }
    });

    $('.filter-area select').on("change", function() {
        $(".option-slice[data-text='" + $(this).data('text') + "']").remove();
        if ($(this).val() != "") {
            var value = $(this).data('text') + ': ' + $(this).children('option:selected').text();
            $('.filter-options').append(
                '<span class="option-slice" data-text="' + $(this).data('text') + '">' + value + '</span>'
            );
        }
    });

    $('.filter-options').on("click", ".option-slice", function() {
        $(".filter-checkbox input[data-text='" + $(this).data('text') + "']").click();
        $(".filter-area select[data-text='" + $(this).data('text') + "']").val("");
        $(this).remove();
    });
    //endregion

    var loadPrePlaySound = true;

    /**
     * Player class containing the state of our playlist and where we are in it.
     * Includes all methods for playing, skipping, updating the display, etc.
     * @param {Array} playlist Array of objects with playlist song details ({title, file, howl}).
     */
    Player = function(playlist) {
        this.playlist = playlist;
        this.index = 0;

        // Display the title of the first track (Jocelyn do not want it).
        /*userInfo.innerHTML = ' ' + playlist[0].user_age + ' • ' +
         playlist[0].user_city + ', ' + playlist[0].user_state + ' (' + playlist[0].lovdrop_language + ')';
         genderImg.attr("src", playlist[0].user_gender === 'FEMALE' ?
         baseUrl + "images/new-female-icon.png" : baseUrl + "images/new-male-icon.png");
         viewProfileLink.attr("href", baseUrl + "member/" + playlist[0].user_id);
         blockBtn.data('userid', playlist[0].user_id);
         blockBtn.data('action', playlist[0].is_blocked ? 'unblock' : 'block');
         blockBtn.attr('src', playlist[0].is_blocked ?  baseUrl + 'images/icons/unblock.png' :  baseUrl + 'images/icons/block.png')*/

        // Setup the playlist.
        playlist.forEach(function(song) {
            $("#" + song.id).click(function() {
                player.skipTo(playlist.indexOf(song));
            });
        });
    };

    Player.prototype = {
        /**
         * Play a song in the playlist.
         * @param  {Number} index Index of the song in the playlist (leave empty to play the first or current).
         */
        play: function(index) {
            var self = this;
            var sound;

            index = typeof index === 'number' ? index : self.index;

            var data = self.playlist[index];

            // If we already loaded this track, use the current one.
            // Otherwise, setup and load a new Howl.
            if (data.howl) {
                sound = data.howl;
            } else {
                sound = data.howl = new Howl({
                    src: [data.file],
                    html5: true, // Force to HTML5 so that the audio can stream in (best for large files).
                    onplay: function() {

                        if (data.is_blocked) {
                            player.stop(false);
                            player.skip("next");
                        }

                        // Update the track display.
                        if (data.audioMediaType === "LOVDROP") {
                            userInfo.innerHTML =
                                ' ' + data.user_age + ' / ' + data.user_state + ' / ' + data.lovdrop_language;

                            genderImg.show();
                            genderImg.attr("src", data.user_gender === 'FEMALE' ?
                                baseUrl + "images/new-female-icon.png" : baseUrl + "images/new-male-icon.png");

                            viewProfileLink.show();
                            viewProfileLink.attr("href", baseUrl + "member/" + data.user_id);

                            var actionBtnInBox = $("img[data-userid='" + data.user_id + "'].block-lovdrop");
                            actionBtnInBox.data('userid', data.user_id);
                            actionBtnInBox.data('action', data.is_blocked ? 'unblock' : 'block');

                            likeBtn.show();
                            blockBtn.show();
                            blockBtn.data('userid', data.user_id);
                            blockBtn.data('action', data.is_blocked ? 'unblock' : 'block');
                            blockBtn.attr('src', data.is_blocked ? baseUrl + 'images/icons/lovdrop-unblock.png' : baseUrl + 'images/icons/lovdrop-block.png');
                        } else {
                            userInfo.innerHTML = "";
                            genderImg.hide();
                            viewProfileLink.hide();
                            likeBtn.hide();
                            blockBtn.hide();
                        }

                        currentPlayUserID = data.user_id;

                        // Display the duration.
                        duration.innerHTML = self.formatTime(Math.round(sound.duration()));

                        // Start upating the progress of the track.
                        requestAnimationFrame(self.step.bind(self));

                        stopAll(false);

                        //userInfoBar.show(250);
                        onAirIcon.removeClass("grayscale-img");

                        $("button[id='" + data.id + "']").find("img").attr("src", baseUrl + "images/icons/lovdrop-pause.png");
                        playBtn.attr("src", baseUrl + "images/lovdrop-playbar/pause-btn.png");

                        if (loadPrePlaySound) {
                            sound.pause();
                            soundEffects.stop();

                            if (data.audioMediaType === "LOVDROP") {
                                if (data.user_gender === "MALE") {
                                    soundEffects = new Howl({
                                        src: [maleSoundBeforeUrl]
                                    });
                                } else {
                                    soundEffects = new Howl({
                                        src: [femaleSoundBeforeUrl]
                                    });
                                }
                            } else if (data.audioMediaType === "ADVERTISEMENT") {
                                soundEffects = new Howl({
                                    src: [adSoundBeforeUrl]
                                });

                            } else if (data.audioMediaType === "NEWS") {
                                soundEffects = new Howl({
                                    src: [newsSoundBeforeUrl]
                                });

                            } else if (data.audioMediaType === "COMEDY") {
                                soundEffects = new Howl({
                                    src: [comedySoundBeforeUrl]
                                });

                            } else if (data.audioMediaType === "PODCAST") {
                                soundEffects = new Howl({
                                    src: [podcastSoundBeforeUrl]
                                });

                            } else if (data.audioMediaType === "MUSIC") {
                                soundEffects = new Howl({
                                    src: [musicSoundBeforeUrl]
                                });

                            }

                            soundEffects.on('end', function() {
                                if (!player.isPlaying()) {
                                    loadPrePlaySound = false;
                                    player.play();
                                }
                            });
                            soundEffects.play();
                        }
                    },
                    onload: function() {
                        console.log("player loaded");
                    },
                    onend: function() {
                        //Prevent loop
                        if (index < (self.playlist.length - 1)) {
                            self.skip('next');
                            stopAll(false);
                        } else {
                            stopAll(true);
                        }

                        loadPrePlaySound = true;
                    },
                    onpause: function() {
                        loadPrePlaySound = true;
                    },
                    onstop: function() {
                        listenToLovdrop(currentPlayUserID);
                        loadPrePlaySound = true;
                    }
                });
            }

            // Begin playing the sound.
            sound.play();

            // Keep track of the index we are currently playing.
            self.index = index;
        },

        /**
         * Pause the currently playing track.
         */
        pause: function(hideBar) {
            var self = this;

            // Get the Howl we want to manipulate.
            var sound = self.playlist[self.index].howl;

            // Puase the sound.
            sound.pause();

            stopAll(hideBar);
        },

        /**
         * Stop the currently playing track.
         */
        stop: function(hideBar) {
            var self = this;

            // Get the Howl we want to manipulate.
            var sound = self.playlist[self.index].howl;

            // Stop the sound.
            sound.stop();

            stopAll(hideBar);
        },

        /**
         * Skip to the next or previous track.
         * @param  {String} direction 'next' or 'prev'.
         */
        skip: function(direction) {
            var self = this;

            // Get the next track based on the direction of the track.
            var index = 0;
            if (direction === 'prev') {
                index = self.index - 1;
                if (index < 0) {
                    index = self.playlist.length - 1;
                }
            } else {
                index = self.index + 1;

                //Prevent continuous playing
                if (index >= self.playlist.length) {
                    self.stop(false);
                    self.index = 0;
                    return;
                }
            }

            self.skipTo(index);
        },

        /**
         * Skip to a specific track based on its playlist index.
         * @param  {Number} index Index in the playlist.
         */
        skipTo: function(index) {
            var self = this;

            // Get the Howl we want to manipulate.
            var sound = self.playlist[self.index].howl;

            // Stop the current track.
            if (sound && (sound.playing() || soundEffects.playing()) && index === self.index) {
                soundEffects.stop();
                self.stop(false);
            } else if (sound && (sound.playing() || soundEffects.playing()) && index !== self.index) {
                soundEffects.stop();
                self.stop(false);
                self.play(index);
            } else {
                self.play(index);
            }


            // Play the new track.
            /*if(self.index !== index)
             self.play(index);
             else if(self.index === index === 0)
             self.pause();*/
        },

        /**
         * Seek to a new position in the currently playing track.
         * @param  {Number} per Percentage through the song to skip.
         */
        seek: function(per) {
            var self = this;

            // Get the Howl we want to manipulate.
            var sound = self.playlist[self.index].howl;

            // Convert the percent into a seek position.
            if (sound.playing()) {
                sound.seek(sound.duration() * per);
            }
        },

        /**
         * The step called within requestAnimationFrame to update the playback position.
         */
        step: function() {
            var self = this;

            // Get the Howl we want to manipulate.
            var sound = self.playlist[self.index].howl;

            // Determine our current seek position.
            var seek = sound.seek() || 0;
            timer.innerHTML = self.formatTime(Math.round(seek));
            progress.style.width = (((seek / sound.duration()) * 100) || 0) + '%';

            // If the sound is still playing, continue stepping.
            if (sound.playing()) {
                requestAnimationFrame(self.step.bind(self));
            }
        },

        /**
         * Format the time from seconds to M:SS.
         * @param  {Number} secs Seconds to format.
         * @return {String}      Formatted time.
         */
        formatTime: function(secs) {
            var minutes = Math.floor(secs / 60) || 0;
            var seconds = (secs - minutes * 60) || 0;

            return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
        },

        isPlaying: function() {
            var self = this;

            // Get the Howl we want to manipulate.
            var sound = self.playlist[self.index].howl;

            return sound && sound.playing();
        }
    };


    // Setup our new audio player class and pass it the playlist.
    playlistArray = [];
    player = new Player(playlistArray);

    searchLovdrops(true);

    $(document).on('scroll resize', function(event) {
        if ($(window).scrollTop() > 150) {
            $('.gray-bar').addClass('fixed-bar');
        } else if ($(window).scrollTop() < 150) {
            $('.gray-bar').removeClass('fixed-bar');

        }
    });

    $("#backBtn").click(function() {
        player.skip("prev");
    });

    playBtn.click(function() {
        if (player.isPlaying()) {
            player.stop(false);
        } else {
            player.play("");
        }
    });

    $("#nextBtn").click(function() {
        player.skip("next");
    });

    progress.addEventListener('click', function(event) {
        player.seek(event.clientX / window.innerWidth);
    });

    $('#lovdrop-list').on('click', '.block-lovdrop', function() {
        if (!loggedIn) {
            window.location = baseUrl + "login";
            return;
        }
        var action = $(this).data('action');
        if (action === 'block') {
            $(".modal[data-edit='block-modal']").attr("userId", $(this).data('userid'));
            $(".modal[data-edit='block-modal']").modal("show");
        } else {
            userBlockUnblock($(this).data('userid'));
        }
    });

    blockBtn.click(function() {
        if (!loggedIn) {
            window.location = baseUrl + "login";
            return;
        }
        var action = $(this).data('action');
        if (action === 'block') {
            $(".modal[data-edit='block-modal']").attr("userId", $(this).data('userid'));
            $(".modal[data-edit='block-modal']").modal("show");
        } else {
            userBlockUnblock($(this).data('userid'));
        }
    });

    $('#modalBlockBtn').click(function() {
        if (!loggedIn) {
            window.location = baseUrl + "login";
            return;
        }
        userBlockUnblock($(".modal[data-edit='block-modal']").attr("userId"));

    });

    $("#flagUserBtn").click(function() {
        console.log("  USer ID " + $(".modal[data-edit='block-modal']").attr("userId"));
        $(".modal[data-edit='flag-modal']").attr("userId", $(".modal[data-edit='block-modal']").attr("userId"));
        $(".modal[data-edit='flag-modal']").modal("show");

    });

    $('#submit-flag').click(function(e) {
        e.preventDefault();
        var userId = $(".modal[data-edit='flag-modal']").attr("userId");

        var blockImg = baseUrl + 'images/icons/lovdrop-block.png';
        var unblockImg = baseUrl + 'images/icons/lovdrop-unblock.png';
        var actionBtnInBox = $("img[data-userid='" + userId + "'].block-lovdrop");
        var action = actionBtnInBox.data('action');
        $('#flagForm').ajaxSubmit({
            type: 'POST',
            crossDomain: true,
            headers: {
                Accept: "application/json; charset=utf-8"
            },

            url: baseUrl + 'member/flag/' + userId,

            success: function() {

                if (action === 'block') {

                    //If track is playing, skip to the next one
                    if (player.isPlaying()) {
                        player.stop(false);
                        player.skip("next");
                    }

                    actionBtnInBox.attr('src', unblockImg);
                    actionBtnInBox.data('action', 'unblock');
                    actionBtnInBox.attr('title', 'Unblock');
                    actionBtnInBox.closest('.lovdrop-container').addClass('disabled-lovdrop');

                    //Update bar only if playing for that user
                    if (currentPlayUserID === userId) {
                        blockBtn.attr('src', unblockImg);
                        blockBtn.attr('title', "Unblock");
                        blockBtn.data('action', 'unblock');
                    }

                    changeIsBlocked(userId, true);
                    $(".modal[data-edit='flag-modal']").modal("hide");
                }
            },
            error: function(e) {
                console.log(e);
            }




        });

    });

    $('#lovdrop-list').on('click', '.like-lovdrop', function() {
        userLikeUnlike($(this));
    });

    likeBtn.click(function() {
        userLikeUnlike($(this));
    });

    $('#lovdrop-list').on('click', '.share-lovdrop-list', function() {
        $(".modal[data-edit='share-drop']").modal("show");
        var userID = $(this).data("userid");
        $("#share-btn-facebook").attr("href", "http://www.facebook.com/sharer.php?u=" + shareBaseUrl + "/member/" + userID);
        $("#share-btn-twitter").attr("href", "https://twitter.com/share?url=" + shareBaseUrl + "/member/" + userID);
        $("#share-btn-google").attr("href", "https://plus.google.com/share?url=" + shareBaseUrl + "/member/" + userID);
        $("#share-btn-tumblr").attr("href", "https://www.tumblr.com/widgets/share/tool?canonicalUrl=" + shareBaseUrl + "/member/" + userID);
        $("#share-btn-pinterest").attr("href", "https://pinterest.com/pin/create/bookmarklet/?url=" + shareBaseUrl + "/member/" + userID);
        $("#share-btn-reddit").attr("href", "https://reddit.com/submit?url=" + shareBaseUrl + "/member/" + userID);
        $("#share-btn-email").attr("shareUrl", shareBaseUrl + "/member/" + userID);
    });

    $('#share-btn-email').click(function() {
         $(".modal[data-edit='share-email-radio']").attr("shareUrl", $(this).attr("shareUrl"));
           console.log("Share slink " +  $(this).attr("shareUrl"));
         $(".modal[data-edit='share-email-radio']").modal("show");
    });

   $('#emailShareBtn').click(function() {
        var shareUrl = $(".modal[data-edit='share-email-radio']").attr("shareUrl");
        console.log("Share link " + shareUrl);
        shareViaEmail(shareUrl);
    });

    $("input.numeric").numeric();

    $('.filter-btn').click(function() {
        $('.filter-area').slideToggle("50");
    });

    loadMoreBtn.click(function() {
        searchLovdrops(false);
    });

    $("#filter-submit").click(function(e) {
        e.preventDefault();
        pageNumber = 1;
        useFilter = true;
        excludedAdsIds = [];
        searchLovdrops(true);
    });

    $("#show-map-btn").click(function() {
        $('#location-modal').modal('setting', 'closable', false).modal('show');
        google.maps.event.trigger(map, "resize");
        map.setCenter(new google.maps.LatLng($('#latitude').val(), $('#longitude').val()));
    });

    $("#set-location-btn").on('click', function() {
        var locationStr =
            $("#country").val() + ', ' + $("#state").val() + ', ' + $("#city").val();
        $("#filter-location-input").val(locationStr);

        $("#latitude-hidden").val($('#latitude').val());
        $("#longitude-hidden").val($('#longitude').val());
        $("#radius-hidden").val($('#last-mile-radius-search').val());
    });

        $(".lovdrop-actions").find("img").click(function () {
            $(".modal[data-edit='unregistered-popup']").modal("show");
        });

           $(".user-profile").click(function () {
                $(".modal[data-edit='unregistered-popup']").modal("show");
            });

});

function shareViaEmail(shareUrl){

         $("#shareUrl").attr("value", shareUrl);
         $("#shareEmailForm").ajaxSubmit({type: 'GET',
                                async: false,
                                crossDomain: true,
                                url: baseUrl + "radio/share/email",
                                success: function() {
                                console.log("shared");
                     $(".modal[data-edit='share-email-radio']").modal("hide");
                                } ,
      error: function(e) {
                console.log(e);
            }


                                });

}

function searchLovdrops(clearResults) {
    if (loggedIn) {
        if (clearResults)
            $("#lovdrop-list").html("");

        $('.filter-area').slideUp("50");

        var formData = $("#filter-form").serializeArray();
        formData.push({
            name: "pageNumber",
            value: pageNumber
        });
        formData.push({
            name: "pageSize",
            value: 5
        });
        formData.push({
            name: "useFilter",
            value: useFilter
        });
        formData.push({
            name: "excludedAds",
            value: excludedAdsIds
        });

        /*
         {
         pageNumber: pageNumber,
         pageSize: 5,
         ageFrom: $("#filter-age-from").val(),
         ageTo: $("#filter-age-to").val(),
         gender: $("#filter-gender").val(),
         heightFeetFrom: $("#filter-height-ft-from").val(),
         heightInchFrom: $("#filter-height-in-from").val(),
         heightFeetTo: $("#filter-height-ft-to").val(),
         heightInchTo: $("#filter-height-in-to").val(),
         languages: $("#filter-languages").val(),
         penisSizeMatter: $("#filter-penis").val(),
         breastsSize: $("#filter-breast").val(),
         buttsSize: $("#filter-butt").val()
         }
         */

        $.ajax({
            url: baseUrl + "lovdrop/load",
            type: "POST",
            data: formData,
            success: function(data) {
                appendLovdrops(data.result.content, clearResults);
                loadLovdrops(data.result.content);
                pageNumber++;

                if (data.result.last) {
                    loadMoreBtn.hide();
                } else {
                    loadMoreBtn.show();
                }

                if (data.result.totalPages > 0) {
                    $("#no-match-message").hide();
                } else {
                    $("#no-match-message").show();
                }
            },
            error: function(e) {
                console.log(e);
            }
        });
    } else {
        $.ajax({
            url: baseUrl + "lovdrop/load",
            type: "POST",
            success: function(data) {
                appendLovdrops(data.result.content, clearResults);
                loadLovdrops(data.result.content);
            },
            error: function(e) {
                console.log(e);
            }
        });
    }
}

function appendLovdrops(data, clearResults) {
    if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {

            var html = "";
            var recordedSeconds = data[i].recordSeconds;

            if (data[i].audioMediaType === "LOVDROP") {
                var userID = data[i].user.id;
                var isBlocked = blockedUsersIDs.indexOf(userID) > -1;
                var isLiked = likedUserIds.indexOf(userID) > -1;
                var blockAction = isBlocked ? "unblock" : "block";
                var blockTitle = isBlocked ? "Unblock" : "Block";
                var likeAction = isLiked ? "unlike" : "like";
                var likeTitle = isLiked ? "Unlike" : "Like";
                var blockIcon = baseUrl + (isBlocked ? "/images/icons/lovdrop-unblock.png" : "/images/icons/lovdrop-block.png");
                var likeIcon = baseUrl + "/images/icons/" + (isLiked ? "like_them.png" : "like.png");
                var grayFilterClass = isBlocked ? "disabled-lovdrop" : "";
                var genderImage = baseUrl + (data[i].user.userProfile.gender === "FEMALE" ? "images/new-female-icon.png" : "images/new-male-icon.png");
                var viewProfileClass = data[i].user.userProfile.gender === "FEMALE" ? "view-female-profile" : "view-male-profile";
                var state = data[i].user.userProfile.address.stateShort;
                var stateSub = (state !== null && state.length > 4) ? state.substring(0, 4) : state;

                html += '<div class="lovdrop-container ' + grayFilterClass + '">' +
                    '       <div class="lovdrop-cover" style="background:url(' + baseUrl + 'images/' + background[Math.floor(Math.random() * background.length)] + ');"></div>' +
                    '          <div class="user-box"><div class="lovdrop-user-info">' +
                    '           <img src="' + genderImage + '"/>' +
                    '           <p>' + data[i].user.userProfile.age + ' / ' + stateSub +
                    '               / ' + data[i].language.abbreviation +
                    '           </p>' +
                    '       </div></div>' +
                    '       <a href="' + baseUrl + 'member/' + userID + '" class="' + viewProfileClass + '">View Profile</a>' +
                    '       <div class="lovdrop-actions">' +
                    '           <img src="' + baseUrl + 'images/icons/share.png" class="share-lovdrop-list" data-userid="' + userID + '"/>' +
                    '           <img src="' + likeIcon + '" class="like-lovdrop" title="' + likeTitle + '" ' +
                    '               data-userid="' + userID + '" data-action="' + likeAction + '"/>' +
                    '           <img src="' + blockIcon + '" class="block-lovdrop" title="' + blockTitle + '" ' +
                    '               data-userid="' + userID + '" data-action="' + blockAction + '" />' +
                    '       </div>' +
                    '       <div class="lovdrop-sound-container">' +
                    '           <div class="lovdrop-sound-wrapper">' +
                    '               <button id="' + data[i].audioMediaType + data[i].id + '" class="lovdrop-play-btn">' +
                    '                   <img src="' + baseUrl + 'images/icons/lovdrop-play.png"/>' +
                    '               </button>' +
                    '               <p class="lovdrop-play-time">' + player.formatTime(recordedSeconds) + '</p>' +
                    '           </div>' +
                    '       </div>' +
                    '       <div class="lovappy-radio-id">' + userID +
                    '       <img  width="20px" src="' + baseUrl + getVerfiedImage(data[i].user.emailVerified)+'" alt="User Status"/>'+
                    '       </div>' +
                    '</div>';
            } else {
                var mediaTypeName = data[i].audioMediaType;
                if (mediaTypeName === "ADVERTISEMENT")
                    mediaTypeName = "AD";

                html += '<div class="lovdrop-container ppt" onclick="openUrl(\'' + data[i].url + '\');">' +
                    '       <div class="lovdrop-ads-name">' + mediaTypeName + '</div>' +
                    '        <div class="lovdrop-cover" style="background:url(' + data[i].coverImage.url + ');cursor: pointer;"></div>' +
                    '        <a class="ad-button" data-ad="link" href="javascript:void(0);"></a>' +
                    '       <p style="cursor: pointer;">Sample Ad</p>' +
                    '       <div class="lovdrop-sound-container">' +
                    '           <div class="lovdrop-sound-wrapper">' +
                    '           <button id="' + data[i].audioMediaType + data[i].id + '" class="lovdrop-play-btn">' +
                    '               <img src="' + baseUrl + 'images/icons/lovdrop-play.png"/>' +
                    '           </button>' +
                    '           <p class="lovdrop-play-time">' + player.formatTime(recordedSeconds) + '</p>' +
                    '       </div>' +
                    '   </div>';
            }
            $("#lovdrop-list").append(html);
        }
    }
}

function getVerfiedImage(isVerified) {
if(isVerified)
return "images/user-verified.png"

return "images/alert-icon-image-gallery-27.png"
}
function openUrl(url) {
    if (url !== 'null') {
        var win = window.open(url, '_blank');
        win.focus();
    }
}

function gotolike() {
    window.location.href = '/like/mylikes';
}

function stopAll(hideBar) {
    $(".lovdrop-play-btn").each(function() {
        $(this).find("img").attr("src", baseUrl + "images/icons/lovdrop-play.png");
        playBtn.attr("src", baseUrl + "images/lovdrop-playbar/play-btn.png");
        onAirIcon.addClass("grayscale-img");

        //if(hideBar)
        //userInfoBar.hide(250);
    });
}

function userBlockUnblock(userID) {
    if (!loggedIn) {
        window.location = baseUrl + "login";
        return;
    }

    var blockImg = baseUrl + 'images/icons/lovdrop-block.png';
    var unblockImg = baseUrl + 'images/icons/lovdrop-unblock.png';
    var actionBtnInBox = $("img[data-userid='" + userID + "'].block-lovdrop");
    var action = actionBtnInBox.data('action');
    $.ajax({
        url: baseUrl + "member/" + action,
        type: "POST",
        data: {
            userId: userID
        },
        success: function() {

            if (action === 'block') {

                //If track is playing, skip to the next one
                if (player.isPlaying()) {
                    player.stop(false);
                    player.skip("next");
                }

                actionBtnInBox.attr('src', unblockImg);
                actionBtnInBox.data('action', 'unblock');
                actionBtnInBox.attr('title', 'Unblock');
                actionBtnInBox.closest('.lovdrop-container').addClass('disabled-lovdrop');

                //Update bar only if playing for that user
                if (currentPlayUserID === userID) {
                    blockBtn.attr('src', unblockImg);
                    blockBtn.attr('title', "Unblock");
                    blockBtn.data('action', 'unblock');
                }

                changeIsBlocked(userID, true);
                $(".modal[data-edit='block-modal']").modal("hide");
            } else {
                actionBtnInBox.attr('src', blockImg);
                actionBtnInBox.data('action', 'block');
                actionBtnInBox.attr('title', 'Block');
                actionBtnInBox.closest('.lovdrop-container').removeClass('disabled-lovdrop');

                //Update bar only if playing for that user
                if (currentPlayUserID === userID) {
                    blockBtn.attr('src', blockImg);
                    blockBtn.attr('title', "Block");
                    blockBtn.data('action', 'block');
                }

                changeIsBlocked(userID, false);
            }
        },
        error: function(e) {
            console.log(e);
        }
    });
}

function userLikeUnlike(elem) {
    if (!loggedIn) {
        window.location = baseUrl + "login";
        return;
    }

    var likeImg = baseUrl + 'images/icons/like.png';
    var unlikeImg = baseUrl + 'images/icons/like_them.png';
    var userID = elem.data('userid');
    var actionBtnInBox = $("img[data-userid='" + userID + "'].like-lovdrop");
    var action = actionBtnInBox.data('action');
    $.ajax({
        url: baseUrl + "/like/" + userID,
        type: action === 'like' ? "POST" : "DELETE",
        data: {
            userId: userID
        },
        success: function() {

            if (action === 'like') {
                actionBtnInBox.attr('src', unlikeImg);
                actionBtnInBox.data('action', 'unlike');
                actionBtnInBox.attr('title', 'Unlike');

                //Update bar only if playing for that user
                if (currentPlayUserID === userID) {
                    likeBtn.attr('src', unlikeImg);
                    likeBtn.attr('title', "Unlike");
                    likeBtn.data('action', 'unlike');
                }
                $(".modal[data-edit='like-modal']").modal("show");

            } else {
                actionBtnInBox.attr('src', likeImg);
                actionBtnInBox.data('action', 'like');
                actionBtnInBox.attr('title', 'Like');

                //Update bar only if playing for that user
                if (currentPlayUserID === userID) {
                    likeBtn.attr('src', likeImg);
                    likeBtn.attr('title', "Like");
                    likeBtn.data('action', 'like');
                }
            }
        },
        error: function(e) {
            console.log(e);
        }
    });
}

function changeIsBlocked(userID, isBlocked) {
    for (var i in playlistArray) {
        if (playlistArray[i].user_id === userID) {
            playlistArray[i].is_blocked = isBlocked;
            break;
        }
    }
}

function loadLovdrops(lovdropsJson) {
    for (var i = 0; i < lovdropsJson.length; i++) {
        var file = lovdropsJson[i].audioFileCloud.url;

        if (lovdropsJson[i].audioMediaType === "LOVDROP") {
            var isBlocked = blockedUsersIDs.indexOf(lovdropsJson[i].user.userProfile.userId) !== -1;

            playlistArray.push({
                audioMediaType: lovdropsJson[i].audioMediaType,
                id: lovdropsJson[i].audioMediaType + lovdropsJson[i].id,
                file: file,
                howl: null,
                user_age: lovdropsJson[i].user.userProfile.age,
                user_state: lovdropsJson[i].user.userProfile.address.stateShort,
                user_city: lovdropsJson[i].user.userProfile.address.city,
                user_language: lovdropsJson[i].user.userProfile.speakingLanguagesAbb,
                lovdrop_language: lovdropsJson[i].language.abbreviation,
                user_gender: lovdropsJson[i].user.userProfile.gender,
                user_id: lovdropsJson[i].user.userProfile.userId,
                is_blocked: isBlocked
            });
        } else {
            playlistArray.push({
                audioMediaType: lovdropsJson[i].audioMediaType,
                id: lovdropsJson[i].audioMediaType + lovdropsJson[i].id,
                file: file,
                howl: null,
                user_age: "",
                user_state: "",
                user_city: "",
                user_language: "",
                lovdrop_language: "",
                user_gender: "",
                user_id: "",
                is_blocked: false
            });

            excludedAdsIds.push(lovdropsJson[i].id);
        }


    }
    player = new Player(playlistArray);
}

function dontShowSurvey() {
    $.ajax({
        url: baseUrl + "registration/survey/dontShow",
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        data: null,
        success: function(data) {
            $('#userSurveyModal .survey-content').html("<h2>This message won't appear again</h2>")
            $('#userSurveyModal').modal('show');
        },
        error: function(data) {
            console.log(data);
        }
    });
}