/**
 * Created by Manoj Senevirathne on 06/01/2018
 */

$(document).ready(function () {

    var submitBtn = $("#register-musician");

    $("input:checkbox").change(function () {
        var ischecked = $(this).is(':checked');
        if (!ischecked) {
            submitBtn.prop('disabled', true);
            submitBtn.css('cursor', 'not-allowed');
        }
        else {
            submitBtn.prop('disabled', false);
            submitBtn.css('cursor', 'pointer');
        }

    });

    submitBtn.on('click', function (e) {
        e.preventDefault();
        submitBtn.addClass("disabled");
        submitBtn.addClass("loading");

        var x = $('#musician-name').validate()
            & $('#musician-number').validate()
            & $('#musician-email').validate()
            & $('#birthdate').validate()
            & validatePassword()
            & $('#musician-confirm-password').validate()
            & $('#musician-password').validate()
            & $('#musician-zipcode').validate();


        var z = $('#confirm-form').validateConfirmForm();

        if (x && z) {

            var url = (!!$('#user-id').val()) ? "/user/registration/signup" : "/user/registration/new/signup";

            $.ajax({
                url: baseUrl + url,
                type: "POST",
                data: $('#reg-form').serialize(),
                success: function (data) {
                    if (containsValidationErrors(data)) {
                        submitBtn.removeClass("disabled");
                        submitBtn.removeClass("loading");
                    } else {
                        $('#musician-name').val('');
                        $('#musician-number').val('');
                        $('#confirm-form').prop('checked', false);
                        $('#musician-modal').show();
                        submitBtn.removeClass("disabled");
                        submitBtn.removeClass("loading");
                         window.location.href = baseUrl + "music/upload";
                    }
                },
                error: function (data) {
                    submitBtn.removeClass("disabled");
                    submitBtn.removeClass("loading");
                    console.log(data);
                    toastr.error('Error');
                }
            });
        } else {
            toastr.error($('#fill-all-fields').val());
        }

    });

    $('#events-checkuser').click(function () {
        var userId = $('#loggedInuserId').val();
        console.log(userId);
        if (userId === '' || jQuery.type(userId) === null) {
            $('#events-loggedout-modal').modal('show');
        } else {
            window.location.href = baseUrl + '/user/registration/signup';
        }
    });

    (function ($) {
        $.fn.validate = function () {
            var success = true;
            if ($(this).val() == '') {
                $(this).css('border', '1px solid red');
                success = false;
            } else
                $(this).css('border', '1px solid #898989');

            return success;
        }

    }(jQuery));

    (function ($) {
        $.fn.validatePhone = function () {
            var success = true;
            if ($(this).val() == '') {
                $('#phoneNumber').css({
                    'display': 'inline-block',
                    'background-color': '#fff',
                    'margin-top': '-9px'
                }).html('Please provide your phone number');
                success = false;
            } else
                $('#phoneNumber').html('');

            return success;
        }

    }(jQuery));

    (function ($) {
        $.fn.validateConfirmForm = function () {
            var success = true;
            if (!$(this).is(':checked')) {
                $('#confirmTerms').css({
                    'display': 'inline-block',
                    'background-color': '#fff'
                }).html('Please agree to proceed');
                success = false;
            } else
                $('#confirmTerms').html('');

            return success;
        }

    }(jQuery));

    function validatePassword() {
        var password = $("#musician-password").val();
        var confirmPassword = $("#musician-confirm-password").val();
        if (password != confirmPassword) {
            $("#musician-password").css('border', '1px solid red');
            $("#musician-confirm-password").css('border', '1px solid red');
            toastr.error($('#password-not-equal').val());
            return false;
        }
        $("#musician-password").css('border', '1px solid #898989');
        $("#musician-confirm-password").css('border', '1px solid #898989');
        return true;
    }

});


function containsValidationErrors(response) {
    $('.error-msg').each(function () {
        $(this).text('');
        $(this).hide();
    });

    if (response !== null && response.errorMessageList !== null && typeof response.errorMessageList !== 'undefined') {
        var errorMessages;
        for (var i = 0; i < response.errorMessageList.length; i++) {

            if (!!errorMessages) {
                errorMessages += response.errorMessageList[i].message + "</br>";
            } else {
                errorMessages = response.errorMessageList[i].message + "</br>";
            }
            toastr.error(response.errorMessageList[i].message);

        }
        if (!!errorMessages) {
            $('#confirmTerms').html(errorMessages);
            $('#confirmTerms').show();
        }
        return true;
    }
    return false;
}

