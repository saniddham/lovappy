package com.realroofers.lovappy.web.rest.v1;

import com.realroofers.lovappy.service.blog.CategoryService;
import com.realroofers.lovappy.service.blog.PostService;
import com.realroofers.lovappy.service.blog.dto.CategoryDto;
import com.realroofers.lovappy.service.blog.dto.PostDto;
import com.realroofers.lovappy.service.blog.model.BlogTypes;
import com.realroofers.lovappy.service.blog.model.CategoryPostCount;
import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.core.SocialType;
import com.realroofers.lovappy.service.exception.EntityValidationException;
import com.realroofers.lovappy.service.exception.GenericServiceException;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.web.exception.BadRequestException;
import com.realroofers.lovappy.web.exception.ResourceNotFoundException;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daoud Shaheen on 9/3/2017.
 */
@RestController
@RequestMapping({"/api/v1/blogs", "/api/v2/blogs"})
public class RestBlogsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestBlogsController.class);

    private final PostService postService;

    private final CategoryService categoryService;

    private final NotificationService notificationService;

    private final CloudStorage cloudStorage;

    private final CloudStorageService cloudStorageService;

    private final UserService userService;

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @Autowired
    public RestBlogsController(PostService postService, CategoryService categoryService, NotificationService notificationService, CloudStorage cloudStorage, CloudStorageService cloudStorageService, UserService userService) {
        this.postService = postService;
        this.categoryService = categoryService;
        this.notificationService = notificationService;
        this.cloudStorage = cloudStorage;
        this.cloudStorageService = cloudStorageService;
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public Page<PostDto> getListByKeyword(@RequestParam(value = "keyword", defaultValue = "") String keyword,
                                          @RequestParam(value = "page", defaultValue = "1") Integer page,
                                         @RequestParam(value= "limit", defaultValue = "10") Integer limit){

        validatePageAndSize(page, limit);
        return postService.findPostByKeyword(keyword, new PageRequest(page - 1, limit));
    }

    @RequestMapping(value = "/most-viewed/posts", method = RequestMethod.GET)
    public Page<PostDto> getMostViewedPosts(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                          @RequestParam(value= "limit", defaultValue = "10") Integer limit){

        validatePageAndSize(page, limit);
        return postService.findMostViewed(page, limit);
    }

    @RequestMapping(value = "/{post-id}", method = RequestMethod.GET)
    public PostDto getPostById(@PathVariable("post-id") Integer postId){

        PostDto post = postService.getPost(postId);
        if(post == null) {
            throw new ResourceNotFoundException("Post with id = " + postId + "is not Found");
        }

        postService.getAndUpdateViewCountById(1, postId);
        return post;
    }

    @RequestMapping( method = RequestMethod.POST)
    public PostDto addNewPost(@RequestBody PostDto postDto) {
        LOGGER.debug(" add new Post {}", postDto);

        postDto.setDraft(false);
        postDto.setContent(postDto.getHtmlContent());
        PostDto post = postService.save(postDto, UserUtil.INSTANCE.getCurrentUserId());
        //send notification to admin
        NotificationDto notificationDto = new NotificationDto();
        notificationDto.setContent(UserUtil.INSTANCE.getCurrentUser().getEmail() + " submit a blog ");
        notificationDto.setType(NotificationTypes.USER_BLOGS);
        notificationDto.setUserId(UserUtil.INSTANCE.getCurrentUser().getUserId());
        notificationService.sendNotificationToAdmins(notificationDto);

        return post;
    }




    @RequestMapping(value = "/{post-id}", method = RequestMethod.PUT)
    public PostDto updatePost(@PathVariable("post-id") Integer postId, @RequestBody PostDto updatePostDto){

        PostDto post = postService.getPost(postId);
        if(post == null) {
            throw new ResourceNotFoundException("Post with id = " + postId + "is not Found");
        }
        postService.update(postId, updatePostDto);
        return post;
    }

    @RequestMapping(value = "/{post-id}", method = RequestMethod.DELETE)
    public void deletePost(@PathVariable("post-id") Integer postId){
        if(!postService.delete(postId)) {
            throw new ResourceNotFoundException("Post with id = " + postId + "is not Found");
        }
    }

    @GetMapping(value = "/{type}/posts")
    public Page<PostDto> getList(@PathVariable("type") String type, @RequestParam(value = "page", defaultValue = "1") Integer page,
                        @RequestParam(value= "limit", defaultValue = "10") Integer limit){

        validatePageAndSize(page, limit);
        BlogTypes blogTypes;
        try {
             blogTypes = BlogTypes.valueOf(type.toUpperCase());
        } catch (Exception ex) {
            throw new BadRequestException("Invalid Type");
        }
        return postService.getPostByType(blogTypes, page, limit);
    }

    @RequestMapping(value = "/featured", method = RequestMethod.GET)
    public Page<PostDto> getFeaturedList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                 @RequestParam(value= "limit", defaultValue = "10") Integer limit){

        validatePageAndSize(page, limit);
        return postService.getFeaturedPosts(page, limit);
    }

    @RequestMapping(value = "/lovappy", method = RequestMethod.GET)
    public Page<PostDto> getLovappyList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                         @RequestParam(value= "limit", defaultValue = "10") Integer limit){

        validatePageAndSize(page, limit);
        return postService.getAdminPosts(page, limit);
    }

    @RequestMapping(value = "/categories/{category-id}/posts", method = RequestMethod.GET)
    public Page<PostDto> getListByCategory(@PathVariable("category-id") Integer categoryId, @RequestParam(value = "page", defaultValue = "1") Integer page,
                                        @RequestParam(value= "limit", defaultValue = "10") Integer limit){

        validatePageAndSize(page, limit);
        return postService.getPostsByCategory(categoryId, page, limit);
    }

    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public Page<CategoryDto> getCategoryList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                               @RequestParam(value= "limit", defaultValue = "10") Integer limit){

        validatePageAndSize(page, limit);
        return categoryService.getAllCategories(page, limit);
    }

    @RequestMapping(value = "/categories/{category-id}", method = RequestMethod.GET)
    public CategoryDto getCategoryById(@PathVariable("category-id") Integer categoryId){

        CategoryDto categoryDto = categoryService.getOne(categoryId);
        if(categoryDto == null){
           throw new ResourceNotFoundException("Category with id " + categoryId + " is not found");
        }
        return categoryDto;
    }

    @RequestMapping(value = "/categories/counts", method = RequestMethod.GET)
    public Page<CategoryPostCount> getCategoryCountsList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                   @RequestParam(value= "limit", defaultValue = "10") Integer limit){

        validatePageAndSize(page, limit);
        return categoryService.getCategoryPostsCount(page, limit);
    }

    @RequestMapping(value = "/{post-id}/image/upload", method = RequestMethod.POST)
    public CloudStorageFileDto uploadBlogImage(@PathVariable("post-id") Integer postId, @RequestParam("file") MultipartFile uploadfile){

        if (uploadfile != null && uploadfile.getSize() > 0) {
            try {
                CloudStorageFileDto cloudStorageFileDto =
                        cloudStorage.uploadImageWithThumbnails(uploadfile, imagesBucket, FileExtension.jpg.toString(), "image/jpeg");
                postService.updateImage(postId, cloudStorageFileDto);
                return cloudStorageFileDto;
            } catch (IOException e) {
                LOGGER.error(e.getMessage());

            }
        }
        throw new GenericServiceException("Internal server error");
    }




    @RequestMapping(value = "upload_pictures", method = RequestMethod.POST)
    public ResponseEntity uploadCoverImage(@RequestParam("image_file") MultipartFile picture) {

        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (picture.isEmpty())
            errorMessages.add(new ErrorMessage("cover", "Add a location picture."));

        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        try {
            CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadFile(picture, imagesBucket, FileExtension.png.toString(), "image/png");
            CloudStorageFileDto added = cloudStorageService.add(cloudStorageFileDto);

            return ResponseEntity.ok(added);

        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @RequestMapping(value = "/author/profile", method = RequestMethod.PUT)
    public CloudStorageFileDto updateAuthorProfile(@RequestParam("name") String name,
                                                   @RequestParam("zip-code") String zipCode,
                                                   @RequestParam("authorImage") MultipartFile authorImage){

        if (authorImage != null && authorImage.getSize() > 0) {
            try {
                CloudStorageFileDto cloudStorageFileDto =
                        cloudStorage.uploadImageWithThumbnails(authorImage, imagesBucket, FileExtension.jpg.toString(), "image/jpeg");
                userService.updateAuthorDetails(UserUtil.INSTANCE.getCurrentUserId(), cloudStorageFileDto, zipCode, name);
               return cloudStorageFileDto;
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
        }
        throw new GenericServiceException("Internal server error");
    }

    @RequestMapping(value = "/{post-id}/share/{social-provider}", method = RequestMethod.POST)
    public void share(@PathVariable("post-id") Integer postId, @PathVariable("social-provider") String socialProvider) {
            postService.shareBySocialProvider(postId, SocialType.fromString(socialProvider));
    }

    private void validatePageAndSize(Integer page, Integer limit) {
        if(page < 1) {
            throw new BadRequestException("Page should be more than 1");
        }
        if(limit < 1) {
            throw new BadRequestException("PageSize should be more than 1");
        }
    }


}
