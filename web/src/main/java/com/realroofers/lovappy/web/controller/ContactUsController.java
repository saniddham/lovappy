package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Daoud Shaheen on 6/10/2017.
 */
@Controller
@RequestMapping("/contact-us")
public class ContactUsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContactUsController.class);

    private CMSService cmsService;

    public ContactUsController(CMSService cmsService) {
        this.cmsService = cmsService;
    }

    @GetMapping
    public String contactUs(Model model) {
        model.addAttribute(PageConstants.CONTACT_US_ADDRESS, cmsService.getTextByTagAndLanguage(PageConstants.CONTACT_US_ADDRESS, "EN"));
        model.addAttribute(PageConstants.CONTACT_US_NUMBER, cmsService.getTextByTagAndLanguage(PageConstants.CONTACT_US_NUMBER, "EN"));
        model.addAttribute(PageConstants.CONTACT_US_EMAIL, cmsService.getTextByTagAndLanguage(PageConstants.CONTACT_US_EMAIL, "EN"));
        return "contact-us";
    }
}
