package com.realroofers.lovappy.web.config.social.instagram.connect;

import com.realroofers.lovappy.web.config.social.instagram.api.Instagram;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;

public class InstagramConnectionFactory extends OAuth2ConnectionFactory<Instagram> {

	public InstagramConnectionFactory(String clientId, String clientSecret) {
		super("instagram", new InstagramServiceProvider(clientId, clientSecret), new InstagramAdapter());
	}
	
}
