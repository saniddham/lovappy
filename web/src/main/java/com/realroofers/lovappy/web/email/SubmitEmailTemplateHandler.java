package com.realroofers.lovappy.web.email;

import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import org.springframework.util.StringUtils;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Created by Daoud Shaheen on 9/10/2017.
 */
public class SubmitEmailTemplateHandler extends AbstractEmailTemplateHandler {


    public SubmitEmailTemplateHandler(String baseUrl, String email, MailService mailService, ITemplateEngine templateEngine, EmailTemplateDto emailTemplateDto) {
        super(baseUrl, email, mailService, templateEngine, emailTemplateDto);
    }

    @Override
    public void send() {
        if (!emailTemplateDto.isEnabled())
            return;

        try {

            Context context = new Context();

            String body = emailTemplateDto.getBody();

            String title = emailTemplateDto.getAdditionalInformation().get("title");
            if (!StringUtils.isEmpty(title)) {
                body = body.replace("TITLE", title);
            }
            context.setVariable("body", body);
            context.setVariable("link", emailTemplateDto.getAdditionalInformation().get("url"));
            context.setVariable("buttonText", emailTemplateDto.getAdditionalInformation().get("buttonText") == null ? "Click Here" : emailTemplateDto.getAdditionalInformation().get("buttonText"));
            context.setVariable("baseUrl", baseUrl);
            context.setVariable("title", emailTemplateDto.getName());
            context.setVariable("emailBG", emailTemplateDto.getImageUrl());
            String content = templateEngine.process(emailTemplateDto.getTemplateUrl(), context);
            mailService.sendMail(email, content, emailTemplateDto.getSubject(), emailTemplateDto.getFromEmail(), emailTemplateDto.getFromName());
        } catch (Exception ex) {
            LOGGER.error("Failed to send email: {}", ex);
        }
    }
}
