package com.realroofers.lovappy.web.rest.dto;

import com.realroofers.lovappy.service.lovstamps.dto.LovstampDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.dto.UserPreferenceDto;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

/**
 * @author Daoud Shaheen
 */
public class LoginResponse {

  private OAuth2AccessToken token;
  private UserDto user;
  private UserPreferenceDto preference;
  private LovstampDto lovastamp;

  public LoginResponse(OAuth2AccessToken token, UserDto user, UserPreferenceDto preference, LovstampDto lovastamp) {
    super();
    this.token = token;
    this.user = user;
    this.preference = preference;
    this.lovastamp = lovastamp;
  }

  public LoginResponse() {
    super();
  }

  public OAuth2AccessToken getToken() {
    return token;
  }

  public void setToken(OAuth2AccessToken token) {
    this.token = token;
  }

  public UserDto getUser() {
    return user;
  }

  public void setUser(UserDto user) {
    this.user = user;
  }

  public UserPreferenceDto getPreference() {
    return preference;
  }

  public void setPreference(UserPreferenceDto preference) {
    this.preference = preference;
  }

  public LovstampDto getLovastamp() {
    return lovastamp;
  }

  public void setLovastamp(LovstampDto lovastamp) {
    this.lovastamp = lovastamp;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    LoginResponse that = (LoginResponse) o;

    return new EqualsBuilder()
        .append(token, that.token)
        .append(user, that.user)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(token)
        .append(user)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("token", token)
        .append("user", user)
        .toString();
  }
}
