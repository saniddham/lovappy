package com.realroofers.lovappy.web.rest.v1;

import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.credits.CreditService;
import com.realroofers.lovappy.service.exception.GenericServiceException;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.music.MusicExchangeResponseService;
import com.realroofers.lovappy.service.music.MusicService;
import com.realroofers.lovappy.service.music.dto.*;
import com.realroofers.lovappy.service.music.model.MusicExchange;
import com.realroofers.lovappy.service.music.model.MusicResponse;
import com.realroofers.lovappy.service.music.model.MusicResponseType;
import com.realroofers.lovappy.service.music.support.MusicDownload;
import com.realroofers.lovappy.service.music.support.MusicProvider;
import com.realroofers.lovappy.service.music.support.MusicStatus;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.CouponDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.exception.AccessDeniedException;
import com.realroofers.lovappy.web.exception.BadRequestException;
import com.realroofers.lovappy.web.exception.ResourceNotFoundException;
import com.realroofers.lovappy.web.external.JamendoMusicService;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.*;
import com.squareup.connect.ApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Eias Altawil
 */

@RestController
@RequestMapping({"/api/v1/music", "/api/v2/music"})
public class RestMusicController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestMusicController.class);
    private final MusicService musicService;

    private final GCSSignUrlUtil signUrlUtil;

    private final CloudStorage cloudStorage;

    private final MailService mailService;

    private final ITemplateEngine templateEngine;

    private final EmailTemplateService emailTemplateService;

    private final OrderService orderService;

    private final PaymentUtil paymentUtil;

    private final NotificationService notificationService;
    private final CloudStorageService cloudStorageService;
    private final FileUtil fileUploadUtil;
    private final UserProfileService userProfileService;
    private final CreditService creditService;

    private final MusicExchangeResponseService musicExchangeResponseService;

    private final JamendoMusicService jamendoMusicService;
    @Value("${lovappy.cloud.bucket.music}")
    private String musicBucket;

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @Autowired
    public RestMusicController(MusicService musicService, GCSSignUrlUtil signUrlUtil, CloudStorage cloudStorage, MailService mailService, ITemplateEngine templateEngine, EmailTemplateService emailTemplateService, OrderService orderService, PaymentUtil paymentUtil, NotificationService notificationService, CloudStorageService cloudStorageService, FileUtil fileUploadUtil, UserProfileService userProfileService, CreditService creditService, MusicExchangeResponseService musicExchangeResponseService, JamendoMusicService jamendoMusicService) {
        this.musicService = musicService;
        this.signUrlUtil = signUrlUtil;
        this.cloudStorage = cloudStorage;
        this.mailService = mailService;
        this.templateEngine = templateEngine;
        this.emailTemplateService = emailTemplateService;
        this.orderService = orderService;
        this.paymentUtil = paymentUtil;
        this.notificationService = notificationService;
        this.cloudStorageService = cloudStorageService;
        this.fileUploadUtil = fileUploadUtil;
        this.userProfileService = userProfileService;
        this.creditService = creditService;
        this.musicExchangeResponseService = musicExchangeResponseService;
        this.jamendoMusicService = jamendoMusicService;
    }


    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<Page<MusicDto>> getAll(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                 @RequestParam(value = "search") String searchText,
                                                 @RequestParam(value = "genre") int genre,
                                                 @RequestParam(value = "decade") int decade) {
        Page<MusicDto> mobileDtos = musicService.getAllMusic(new PageRequest(page - 1, 10), searchText, genre, decade + "");

        return new ResponseEntity<>(mobileDtos, HttpStatus.OK);
    }


    @RequestMapping(value = "/genres", method = RequestMethod.GET)
    public ResponseEntity<List<GenreDto>> getAllGenre() {
        List<GenreDto> genres = musicService.getAllEnabledGenres();
        GenreDto genreDto = new GenreDto();
        genreDto.setTitle("All");
        genreDto.setId(0);
        if (genres.isEmpty()) {
            genres.add(genreDto);
        } else {
            genres.add(0, genreDto);
        }
        return new ResponseEntity<>(genres, HttpStatus.OK);
    }


    @RequestMapping(value = "/decades", method = RequestMethod.GET)
    public ResponseEntity<List<DecadeDto>> getAllDecades() {
        List<DecadeDto> decadeDtoList = new ArrayList<>();

        DecadeDto decadeDto = new DecadeDto(0, "All");
        decadeDtoList.add(decadeDto);
        decadeDto = new DecadeDto(1930, "1930s");
        decadeDtoList.add(decadeDto);
        decadeDto = new DecadeDto(1940, "1940s");
        decadeDtoList.add(decadeDto);
        decadeDto = new DecadeDto(1950, "1950s");
        decadeDtoList.add(decadeDto);
        decadeDto = new DecadeDto(1960, "1960s");
        decadeDtoList.add(decadeDto);
        decadeDto = new DecadeDto(1970, "1970s");
        decadeDtoList.add(decadeDto);
        decadeDto = new DecadeDto(1980, "1980s");
        decadeDtoList.add(decadeDto);
        decadeDto = new DecadeDto(1990, "1990s");
        decadeDtoList.add(decadeDto);
        decadeDto = new DecadeDto(2000, "2000s");
        decadeDtoList.add(decadeDto);
        decadeDto = new DecadeDto(2010, "2010s");
        decadeDtoList.add(decadeDto);


        return new ResponseEntity<>(decadeDtoList, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<MusicDto> getByIdMobile(@PathVariable("id") Integer id) {

        try {
            MusicDto itemDto = musicService.getMusicWithRating(id, UserUtil.INSTANCE.getCurrentUserId());
            CalculatePriceDto price = musicService.calculatePrice(id, null);
            if (price != null)
                itemDto.setPrice(price.getPrice());
            return new ResponseEntity<>(itemDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value = "/inbox", method = RequestMethod.GET)
    public ResponseEntity<Page<MusicExchangeDto>> getAllInbox(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                              @RequestParam(value = "sort") String sortOrder) {
        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        String sort = "exchangeDate";
        Sort.Direction sortDirection = Sort.Direction.DESC;

        if (sortOrder.equalsIgnoreCase("Date")) {
            sort = "exchangeDate";
            sortDirection = Sort.Direction.ASC;
        } else if (sortOrder.equalsIgnoreCase("Date_DESC")) {
            sort = "exchangeDate";
            sortDirection = Sort.Direction.DESC;
        } else if (sortOrder.equalsIgnoreCase("User")) {
            sort = "fromUser";
            sortDirection = Sort.Direction.ASC;
        } else if (sortOrder.equalsIgnoreCase("User_DESC")) {
            sort = "fromUser";
            sortDirection = Sort.Direction.DESC;
        } else if (sortOrder.equalsIgnoreCase("Song")) {
            sort = "music.title";
            sortDirection = Sort.Direction.ASC;
        } else if (sortOrder.equalsIgnoreCase("Song_DESC")) {
            sort = "music.title";
            sortDirection = Sort.Direction.DESC;
        }

        return ResponseEntity.ok(musicService.getMusicExchangeByToUser(userId, new PageRequest(page - 1, 10, sortDirection, sort)));
    }

    @RequestMapping(value = "/inbox/count", method = RequestMethod.GET)
    public long getInboxCount() {
        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        return musicService.countMusicReceived(userId);
    }


    @RequestMapping(value = "/inbox/reply", method = RequestMethod.POST)
    public ResponseEntity getInboxSendReply(@RequestParam(value = "type") MusicResponseType type,
                                            @RequestParam(value = "musicExchangeId") Integer musicExchangeId) {
        MusicExchange musicExchange = musicService.getMusicExchange(musicExchangeId);

        if (!type.equals(MusicResponseType.NO_RESPONSE)) {
            MusicResponse responseForExchange = musicExchangeResponseService.getResponseForExchange(musicExchange);

            if (responseForExchange == null) {

                MusicResponse musicResponse = new MusicResponse();
                musicResponse.setMusicExchange(musicExchange);
                musicResponse.setType(type);

                musicExchangeResponseService.sendResponse(musicResponse);
            } else {
                musicService.setUserResponded(musicExchangeId);
                return ResponseEntity.status(HttpStatus.CONFLICT).build();
            }
        }
        musicService.setUserResponded(musicExchangeId);

        return ResponseEntity.ok().build();
    }


    @RequestMapping(value = "/inbox/responses/all", method = RequestMethod.GET)
    public ResponseEntity<Page<MusicResponseDto>> getAllInboxResponses(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        Page<MusicResponseDto> list = musicExchangeResponseService.getResponseForLoginUser(userId, new PageRequest(page - 1, 10));

        return new ResponseEntity(list, HttpStatus.OK);
    }


    @RequestMapping(value = "/inbox/responses", method = RequestMethod.GET)
    public ResponseEntity<List<MusicResponseDto>> getInboxResponses() {
        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        List<MusicResponseDto> list = musicExchangeResponseService.getInboxResponsesForLoginUser(userId);

        return new ResponseEntity(list, HttpStatus.OK);
    }


    @RequestMapping(value = "/inbox/{musicExchangeId}/response", method = RequestMethod.GET)
    public ResponseEntity<MusicResponseDto> getInboxResponseById(@PathVariable("musicExchangeId") Integer musicExchangeId) {
        MusicExchange musicExchange = musicService.getMusicExchange(musicExchangeId);
        MusicResponse responseForExchange = musicExchangeResponseService.getResponseForExchange(musicExchange);

        if (responseForExchange == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return new ResponseEntity(new MusicResponseDto(responseForExchange), HttpStatus.OK);
    }


    @RequestMapping(value = "/inbox/{musicResponseId}/response/seen", method = RequestMethod.POST)
    public ResponseEntity getInboxResponseSeen(@PathVariable("musicResponseId") Integer musicResponseId) {
        MusicResponse response = musicExchangeResponseService.findById(musicResponseId);

        if (response == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        response.setSeen(true);
        musicExchangeResponseService.update(response);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/outbox", method = RequestMethod.GET)
    public ResponseEntity<Page<MusicExchangeDto>> getAllOutbox(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                               @RequestParam(value = "sort") String sortOrder) {
        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        String sort = "exchangeDate";
        Sort.Direction sortDirection = Sort.Direction.DESC;

        if (sortOrder.equalsIgnoreCase("Date")) {
            sort = "exchangeDate";
            sortDirection = Sort.Direction.ASC;
        } else if (sortOrder.equalsIgnoreCase("Date_DESC")) {
            sort = "exchangeDate";
            sortDirection = Sort.Direction.DESC;
        } else if (sortOrder.equalsIgnoreCase("User")) {
            sort = "toUser";
            sortDirection = Sort.Direction.ASC;
        } else if (sortOrder.equalsIgnoreCase("User_DESC")) {
            sort = "toUser";
            sortDirection = Sort.Direction.DESC;
        } else if (sortOrder.equalsIgnoreCase("Song")) {
            sort = "music.title";
            sortDirection = Sort.Direction.ASC;
        } else if (sortOrder.equalsIgnoreCase("Song_DESC")) {
            sort = "music.title";
            sortDirection = Sort.Direction.DESC;
        }

        return ResponseEntity.ok(musicService.getMusicExchangeByFromUser(userId, new PageRequest(page - 1, 10, sortDirection, sort)));
    }

    @RequestMapping(value = "/outbox/count", method = RequestMethod.GET)
    public long getOutboxCount() {
        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        return musicService.countMusicSent(userId);
    }


    @RequestMapping(value = "/rate", method = RequestMethod.POST)
    public ResponseEntity<Void> rate(@RequestParam("music_id") Integer musicID,
                                     @RequestParam("rating") Integer rating) {
        musicService.rateMusic(musicID, UserUtil.INSTANCE.getCurrentUserId(), rating);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{music-id}/credits/send", method = RequestMethod.POST)
    public ResponseEntity<Void> sendByCredits(HttpServletRequest request, @PathVariable("music-id") Integer musicId,
                                              @RequestParam(value = "userId") Integer userId,
                                              @RequestParam(value = "lyrics", required = false) String lyrics) {

        creditService.chargeByCredit(UserUtil.INSTANCE.getCurrentUserId(), userId, 1);
        return send(request, musicId, userId, lyrics);
    }

    @RequestMapping(value = "/{music-id}/send", method = RequestMethod.POST)
    public ResponseEntity<Void> send(HttpServletRequest request, @PathVariable("music-id") Integer musicId,
                                     @RequestParam(value = "userId") Integer userId,
                                     @RequestParam(value = "lyrics", required = false) String lyrics) {
        MusicExchangeDto musicExchange = musicService.send(UserUtil.INSTANCE.getCurrentUserId(), userId, musicId, lyrics);
        //send  email to user
        EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.NEW_SONGS, "EN");
        emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/music/" + musicId + "/received?userId=" + UserUtil.INSTANCE.getCurrentUserId());
        emailTemplate.getAdditionalInformation().put("title", musicExchange.getMusic().getTitle());
        emailTemplate.getAdditionalInformation().put("userId", String.valueOf(UserUtil.INSTANCE.getCurrentUserId()));
        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), musicExchange.getToUser().getEmail(), templateEngine,
                mailService, emailTemplate).send();
        musicService.setReceived(musicId, userId);

        //saveTransactionu
        if (userProfileService.saveTransaction(UserUtil.INSTANCE.getCurrentUserId(), userId)) {
            //send email
            //send  email to user
            EmailTemplateDto unlockEmailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.PHOTO_UNLOCK, "EN");
            unlockEmailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/member/" + UserUtil.INSTANCE.getCurrentUserId());
            new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), musicExchange.getToUser().getEmail(), templateEngine,
                    mailService, unlockEmailTemplate).send();
        }
        request.getSession().setAttribute("send-music-to", null);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{music-exchange-id}/generate-download-link-for-exchange", method = RequestMethod.GET)
    public ResponseEntity<String> generateExchangeDownloadLink(@PathVariable("music-exchange-id") Integer musicExchangeId,
                                                               @RequestParam(value = "download-by", defaultValue = "WEB") MusicDownload downloadBy) throws AccessDeniedException {

        MusicExchange musicExchange = musicService.getMusicExchange(musicExchangeId);

        if (musicExchange == null) {
            throw new ResourceNotFoundException("Invalid music id or user not allowed to download this music!");
        }
        MusicDto musicDto = new MusicDto(musicExchange.getMusic());

        if (musicExchange.getFromUser().getUserId().equals(UserUtil.INSTANCE.getCurrentUserId())) {

            if (downloadBy == MusicDownload.MOBILE) {
                validateDownloadLimit(musicExchange.getSenderMobileDownloads() == 1);
                musicExchange.setSenderMobileDownloads(musicExchange.getSenderMobileDownloads() + 1);
            } else if (downloadBy == MusicDownload.WEB) {
                validateDownloadLimit(musicExchange.getSenderWebDownloads() == 1);
                musicExchange.setSenderWebDownloads(musicExchange.getSenderWebDownloads() + 1);
            }
        } else if (musicExchange.getToUser().getUserId().equals(UserUtil.INSTANCE.getCurrentUserId())) {

            if (downloadBy == MusicDownload.MOBILE) {
                validateDownloadLimit(musicExchange.getReceiverMobileDownloads() ==1);
                musicExchange.setReceiverMobileDownloads(musicExchange.getReceiverMobileDownloads() + 1);
            } else if (downloadBy == MusicDownload.WEB) {
                validateDownloadLimit(musicExchange.getReceiverWebDownloads() == 1);
                musicExchange.setReceiverWebDownloads(musicExchange.getReceiverWebDownloads() + 1);
            }
        }

        if (musicDto.getProvider().equals(MusicProvider.LOVAPPY)) {
            CloudStorageFileDto musicFile = musicService.getMusicFile(musicDto.getId());
            try {
                String signedUrl = signUrlUtil.createSignedUrl(musicFile.getBucket(), musicFile.getName());
                musicService.setDownloadStatus(musicDto.getId(), UserUtil.INSTANCE.getCurrentUserId(), downloadBy);
                musicService.updateMusicExchange(musicExchange);
                return ResponseEntity.ok(signedUrl);
            } catch (Exception e) {
                throw new GenericServiceException(e.getMessage());
            }
        } else {
            musicService.setDownloadStatus(musicDto.getId(), UserUtil.INSTANCE.getCurrentUserId(), downloadBy);
            if (musicDto.getDownloadUrl() != null) {
                musicService.updateMusicExchange(musicExchange);
                return ResponseEntity.ok(musicDto.getDownloadUrl());
            } else {
                throw new ResourceNotFoundException("NO Url for this Music");
            }
        }
    }

    private void validateDownloadLimit(boolean expression) throws AccessDeniedException{
        if (expression) {
            throw new AccessDeniedException("You have reached download limit!");
        }
    }

    @RequestMapping(value = "/{music-id}/generate-download-link", method = RequestMethod.GET)
    public ResponseEntity<String> generateDownloadLink(@PathVariable("music-id") Integer musicID,
                                                       @RequestParam(value = "download-by", defaultValue = "WEB") MusicDownload downloadBy) throws AccessDeniedException {

        MusicDto musicDto = musicService.getMusic(musicID);
        MusicUserDto musicUserDto = musicService.getMusicUserDetails(musicID, UserUtil.INSTANCE.getCurrentUserId());

        if (musicDto == null || musicUserDto == null) {
            throw new ResourceNotFoundException("Invalid music id or user not allowed to download this music!");
        }

        if (musicUserDto.getPurchased()) {
            if (musicUserDto.getMobileDownloads() > 0) {
                throw new AccessDeniedException("You have reached download limit!");
            }
        }

        if (musicDto.getProvider().equals(MusicProvider.LOVAPPY)) {
            CloudStorageFileDto musicFile = musicService.getMusicFile(musicID);
            try {
                String signedUrl = signUrlUtil.createSignedUrl(musicFile.getBucket(), musicFile.getName());
                musicService.setDownloadStatus(musicID, UserUtil.INSTANCE.getCurrentUserId(), downloadBy);
                return ResponseEntity.ok(signedUrl);
            } catch (Exception e) {
                throw new GenericServiceException(e.getMessage());
            }
        } else {
            musicService.setDownloadStatus(musicID, UserUtil.INSTANCE.getCurrentUserId(), downloadBy);
            if (musicDto.getDownloadUrl() != null) {
                return ResponseEntity.ok(musicDto.getDownloadUrl());
            } else {
                throw new ResourceNotFoundException("NO Url for this Music");
            }
        }
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public MusicDto submit(@Valid MusicSubmitDto request,
                           @RequestParam("file") MultipartFile musicFile,
                           @RequestParam("coverPhoto") MultipartFile coverPhoto
    ) {

        LOGGER.debug("create new song {}", request);
        MusicDto musicDto = musicService.create(request, UserUtil.INSTANCE.getCurrentUserId());
        musicDto.setFileUrl(upload(musicDto.getId(), musicFile).getUrl());
        musicDto.setCoverImageUrl(uploadCoverPhoto(musicDto.getId(), coverPhoto).getUrl());

        return musicDto;
    }

    @RequestMapping(value = "/{music-id}", method = RequestMethod.POST)
    public void update(@Valid MusicSubmitDto request,
                       @PathVariable("music-id") Integer musicId,
                       @RequestParam(value = "file", required = false) MultipartFile musicFile,
                       @RequestParam(value = "coverPhoto", required = false) MultipartFile coverPhoto
    ) {

        LOGGER.debug("create new song {}", request);
        MusicDto musicDto = musicService.create(request, UserUtil.INSTANCE.getCurrentUserId());
        if (!musicFile.isEmpty()) {
            upload(musicDto.getId(), musicFile);
        }
        if (!coverPhoto.isEmpty()) {
            uploadCoverPhoto(musicDto.getId(), coverPhoto);
        }

    }

    @RequestMapping(value = "/{music-id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("music-id") Integer musicId) {
        musicService.delete(musicId);
    }

    @RequestMapping(value = "/{music-id}/upload", method = RequestMethod.POST)
    public CloudStorageFileDto upload(@PathVariable("music-id") Integer musicID,
                                      @RequestParam("file") MultipartFile musicFile) {


        if (!musicFile.isEmpty()) {
            try {
                CloudStorageFileDto cloudStorageFileDto =
                        cloudStorage.uploadAndPersistFile(musicFile, musicBucket, FileExtension.mp3.toString(), "audio/mpeg");

                CloudStorageFileDto songPreviewFile = uploadPreviewSong(musicFile);
                musicService.addFile(musicID, cloudStorageFileDto, songPreviewFile);
                return cloudStorageFileDto;
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
        }
        throw new BadRequestException("Invalid Music input");
    }

    private CloudStorageFileDto uploadPreviewSong(MultipartFile musicFile) {
        if (musicFile != null) {
            try {

                byte[] splitedStream = fileUploadUtil.spliteFile(musicFile);
                CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadFile(splitedStream,
                        musicBucket, FileExtension.mp3.toString(), "audio/mpeg", false);
                return cloudStorageService.add(cloudStorageFileDto);
            } catch (IOException e) {
                LOGGER.error("error" + e);
            }
        }
        return null;
    }

    @RequestMapping(value = "/{music-id}/coverPhoto/upload", method = RequestMethod.POST)
    public CloudStorageFileDto uploadCoverPhoto(@PathVariable("music-id") Integer musicID,
                                                @RequestParam("coverPhoto") MultipartFile coverPhoto) {

        if (!coverPhoto.isEmpty()) {
            try {
                CloudStorageFileDto cloudStorageFileDto =
                        cloudStorage.uploadAndPersistImageWithThumbnails(coverPhoto, imagesBucket, FileExtension.jpg.toString(), "image/jpeg");
                musicService.addCoverPhoto(musicID, cloudStorageFileDto);
                return cloudStorageFileDto;
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
        }
        throw new BadRequestException("Invalid Cover photo input");
    }


    @RequestMapping(value = "/price", method = RequestMethod.GET)
    public ResponseEntity<CalculatePriceDto> calculatePrice(@RequestParam("music_id") Integer musicId,
                                                            @RequestParam("coupon") String coupon) {

        CouponDto order = orderService.getCoupon(coupon, CouponCategory.MUSIC);
        return ResponseEntity.ok(musicService.calculatePrice(musicId, order));
    }

    @RequestMapping(value = "/{music-id}/order", method = RequestMethod.POST)
    public ResponseEntity<?> buy(@PathVariable("music-id") Integer musicId,
                                 @RequestParam("nonce") String nonce,
                                 @RequestParam("coupon") String couponCode) {

        OrderDto order = musicService.order(musicId, couponCode, PaymentMethodType.SQUARE);
        if (order != null) {

            Boolean executed = null;
            try {
                executed = paymentUtil.executeTransaction(nonce, order, null);
            } catch (ApiException e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
            if (executed) {
                musicService.setPurchased(musicId, UserUtil.INSTANCE.getCurrentUserId());
                //send notification to admin
                NotificationDto notificationDto = new NotificationDto();
                notificationDto.setContent("New Music has been ordered");
                notificationDto.setType(NotificationTypes.MUSIC_ORDER);
                notificationDto.setUserId(UserUtil.INSTANCE.getCurrentUser().getUserId());
                notificationDto.setSourceId(musicId.longValue());
                notificationService.sendNotificationToAdmins(notificationDto);

                return ResponseEntity.ok().build();
            } else {
                orderService.revertOrder(order.getId());
            }
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @RequestMapping(value = "/jamendo", method = RequestMethod.GET)
    public List<MusicDto> jamedo(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                 @RequestParam(value = "limit", defaultValue = "10") Integer limit) {


        List<MusicDto> songsPage = jamendoMusicService.getSongs(page, limit);
        for (MusicDto song : songsPage) {

            if (song.getGenre() != null) {
                song.setGenre(musicService.addGenreIfNotExist(song.getGenre()));
            } else {
                song.setGenre(musicService.getAllGenres().get(0));
            }
            song.setStatus(MusicStatus.APPROVED);
            musicService.create(song, MusicProvider.JAMENDO, UserUtil.INSTANCE.getCurrentUserId());

        }

        return songsPage;
    }


}

