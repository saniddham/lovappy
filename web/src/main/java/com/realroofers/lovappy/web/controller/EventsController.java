package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.blog.dto.EmailShareDto;
import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import com.realroofers.lovappy.service.core.SocialType;
import com.realroofers.lovappy.service.event.EventService;
import com.realroofers.lovappy.service.event.EventSupportService;
import com.realroofers.lovappy.service.event.dto.EventDto;
import com.realroofers.lovappy.service.event.dto.EventLocationDto;
import com.realroofers.lovappy.service.event.dto.EventSearchDto;
import com.realroofers.lovappy.service.event.dto.EventUserPictureDto;
import com.realroofers.lovappy.service.event.support.EventDateUtil;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.system.LanguageService;
import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.RegisterUserDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.dto.UserProfileDto;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.util.CommonUtils;
import com.realroofers.lovappy.web.util.PageWrapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by Daoud Shaheen on 8/18/2017.
 */
@Controller
@RequestMapping("/events")
public class EventsController {

    private static final Logger LOG = LoggerFactory.getLogger(EventsController.class);
    private static final String GOOGLE_KEY = "google_key";

    private final EventService eventService;
    private final EventSupportService eventSupportService;
    private final LanguageService languageService;
    private final UserService userService;
    private final UserProfileService userProfileService;
    private final ITemplateEngine templateEngine;
    private final CMSService cmsService;
    private final EmailTemplateService emailTemplateService;
    private final MailService mailService;

    @Value("${google.maps.api.key}")
    private String googleKeyId;

    @Value("${square.applicationId}")
    private String squareAppID;

    @Autowired
    public EventsController(EventService eventService, EventSupportService eventSupportService,
                            LanguageService languageService, UserService userService,
                            UserProfileService userProfileService, MailService mailService,
                            EmailTemplateService emailTemplateService, CMSService cmsService,
                            ITemplateEngine templateEngine) {
        this.eventService = eventService;
        this.eventSupportService = eventSupportService;
        this.languageService = languageService;
        this.userService = userService;
        this.userProfileService = userProfileService;
        this.mailService = mailService;
        this.emailTemplateService = emailTemplateService;
        this.cmsService = cmsService;
        this.templateEngine = templateEngine;
    }


    @GetMapping("/create")
    public String createEvent(Model model) {
        RegisterUserDto ambassadorDto = userService.getAmbassador(UserUtil.INSTANCE.getCurrentUserId());
        if (ambassadorDto != null) {
            model.addAttribute("eventCreate", new EventDto());
            model.addAttribute(GOOGLE_KEY, googleKeyId);
            model.addAttribute("categories", eventSupportService.getEventCategories());
            model.addAttribute("languages", languageService.getAllLanguages(true));
            model.addAttribute("userRoles", userService.getRolesForUser(UserUtil.INSTANCE.getCurrentUserId()));
            return "events/event-create";
        }
        return "redirect:/events";
    }

    /**
     * This api call returns paginated EventDTO list
     *
     * @param model
     * @param pageable
     * @return modelAndview object
     */
    @GetMapping("/browse")
    public ModelAndView getEventList(ModelAndView model, Pageable pageable, @ModelAttribute("eventLocationDto") EventLocationDto eventLocationDto,
                                     @ModelAttribute("pickedDate") String pickedDate, @ModelAttribute("searchEventDto") EventSearchDto searchDto) {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        UserProfileDto userProfileDto = userProfileService.findByUserId(userId);
        model.setViewName("events/events-list");
        model.addObject("eventHeaderImage", cmsService.getImageUrlByTag(PageConstants.EVENT_HEADER_COVER));
        Page<EventDto> eventDtos;
        PageWrapper<EventDto> pageWrapper;

        if (searchDto.getLatitude() != null) {

            eventDtos = eventService.getNearByEventsPage(searchDto, pageable);
            pageWrapper = new PageWrapper<EventDto>(eventDtos, "/events/browse?radius=" + searchDto.getRadius() + "&latitude=" + searchDto.getLatitude() + "&longitude=" + searchDto.getLongitude());

        } else if ((eventLocationDto == null || eventLocationDto.getTerm() == null) && pickedDate.isEmpty()) {

            eventDtos = eventService.findAllFutureActiveEvents(pageable);
            pageWrapper = new PageWrapper<EventDto>(eventDtos, "/events/browse?");

        } else if (!pickedDate.isEmpty() && (eventLocationDto == null || eventLocationDto.getTerm() == null)) {
            if (EventDateUtil.convertToDateFormat(pickedDate).before(EventDateUtil.getDateOnly())) {

                return new ModelAndView("redirect:/events/gallery?date=" + pickedDate);

            }
            eventDtos = eventService.findEventsByEventDateGreaterThanEqual(pageable, pickedDate);
            pageWrapper = new PageWrapper<EventDto>(eventDtos, "/events/browse?pickedDate=");

        } else {

            if (eventLocationDto.getTerm().isEmpty()) {
                eventDtos = eventService.findAllFutureActiveEvents(pageable);
                pageWrapper = new PageWrapper<EventDto>(eventDtos, "/events/browse?");
            } else {
                eventDtos = eventService.findAllByEventLocationAndEventStatus(pageable, eventLocationDto.getTerm());
                pageWrapper = new PageWrapper<EventDto>(eventDtos, "/events/browse?term=" + eventLocationDto.getTerm());
            }
        }
        model.addObject("eventsList", eventDtos);
        model.addObject("pickedDate", pickedDate);
        model.addObject("userId", userId);
        model.addObject("userRoles", userService.getRolesForUser(userId));
        model.addObject(GOOGLE_KEY, googleKeyId);
        model.addObject("userProfile", userProfileDto);
        model.addObject("page", pageWrapper);

        return model;
    }

    /**
     * This API call returns the event object depending the eventId
     *
     * @param eventId
     * @param model
     * @return
     */

    @GetMapping("/{id}")
    public String getEvent(@PathVariable("id") Integer eventId, Model model,
                           @ModelAttribute("eventLocationDto") EventLocationDto eventLocationDto,
                           @ModelAttribute("searchEventDto") EventSearchDto searchDto) {

        EventDto eventDto;
        try {
            Integer userId = UserUtil.INSTANCE.getCurrentUserId();
            eventDto = eventService.findEventById(eventId);
            if (eventDto == null) {
                return "error/404";
            }
            model.addAttribute("event", eventDto);
            model.addAttribute(GOOGLE_KEY, googleKeyId);
            model.addAttribute("userId", userId);
            model.addAttribute("userRoles", userService.getRolesForUser(userId));
            model.addAttribute("userProfile", userProfileService.findByUserId(userId));
            model.addAttribute("emailShare", new EmailShareDto());
            model.addAttribute("attendeesCount", eventService.getNoOfUserAttendees(eventId));
            model.addAttribute("eventHeaderImage", cmsService.getImageUrlByTag(PageConstants.EVENT_HEADER_COVER));
            model.addAttribute("isRegisteredForEvent", eventService.isRegisteredForEvent(eventId, userId));

            model.addAttribute("squareAppID", squareAppID);

        } catch (Exception ex) {

            LOG.error(ex.getMessage());
        }
        return "events/event-details";
    }

    @GetMapping("/gallery")
    public String gallery(Model model, @ModelAttribute("date") String date, @ModelAttribute("eventLocationDto") EventLocationDto eventLocationDto, @ModelAttribute("searchEventDto") EventSearchDto searchDto) {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        if (StringUtils.isEmpty(date)) {
            model.addAttribute("previousEvents", eventService.getPreviousEvents());
        } else {
            model.addAttribute("previousEvents", eventService.getPreviousEvents(EventDateUtil.convertToDateFormat(date)));
        }
        model.addAttribute(GOOGLE_KEY, googleKeyId);
        model.addAttribute("eventHeaderImage", cmsService.getImageUrlByTag(PageConstants.EVENT_HEADER_COVER));
        model.addAttribute("userProfile", userProfileService.findByUserId(userId));
        model.addAttribute("userId", userId);
        model.addAttribute("userRoles", userService.getRolesForUser(userId));
        model.addAttribute("pickedDate", date);
        return "events/event-gallery";
    }

    /**
     * This API call redirects the user
     *
     * @param model
     * @return
     */

    @GetMapping
    public ModelAndView landingPage(ModelAndView model) {

        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        List<EventDto> eventDtos = eventService.findAllFutureActiveEvents();
        model.addObject("eventsList", eventDtos);
        model.addObject("user", new UserDto());
        model.addObject("eventsLandingPageTitle", cmsService.getTextByTagAndLanguage(PageConstants.EVENTS_LANDING_PAGE_TITLE, "EN").replace("\\n", "<br>"));
        model.addObject("eventsLandingPageDescription", cmsService.getTextByTagAndLanguage(PageConstants.EVENTS_LANDING_PAGE_DESCRIPTION, "EN").replace("\\n", "<br>"));
        model.addObject("eventLandingPageHeaderImage", cmsService.getImageUrlByTag(PageConstants.EVENTS_LANDING_PAGE_IMAGE));
        model.setViewName("events/event-landing");
        return model;
    }

    /**
     * This API call returns the event object depending the eventId
     *
     * @param model
     * @return
     */

    @GetMapping("/gallery/details/{id}")
    public ModelAndView getEvent(ModelAndView model, @PathVariable("id") Integer eventId, Pageable pageable) {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        try {
            Page<EventUserPictureDto> pictureDtos = eventService.getEventPhotos(eventId, pageable);
            model.addObject("eventPhotos", pictureDtos);
            PageWrapper<EventUserPictureDto> pageWrapper = new PageWrapper<>(pictureDtos, "/events/gallery/details/" + eventId);
            model.addObject("eventHeaderImage", cmsService.getImageUrlByTag(PageConstants.EVENT_HEADER_COVER));
            model.addObject("event", eventService.findEventById(eventId));
            model.addObject("page", pageWrapper);
            model.addObject("userRoles", userService.getRolesForUser(userId));
            model.addObject("userId", userId);
            model.addObject("isRegisteredForEvent", eventService.isRegisteredForEvent(eventId, userId));
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
        model.setViewName("events/event-gallery-details");
        return model;
    }

    @GetMapping("/share/email")
    public ModelAndView shareEmail(HttpServletRequest request,
                                   ModelAndView model, EmailShareDto emailShareDto,
                                   @RequestParam(value = "eventId", required = false) Integer eventId,
                                   @RequestParam(value = "lang", defaultValue = "EN") String language) {

        EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.EVENT_SHARE, language);

        emailTemplate.getAdditionalInformation().put("recipientName", emailShareDto.getRecipientName());
        emailTemplate.getAdditionalInformation().put("name", emailShareDto.getName());
        emailTemplate.getAdditionalInformation().put("recipientEmail", emailShareDto.getShareWithEmail());
        emailTemplate.getAdditionalInformation().put("sharedBy", emailShareDto.getEmail());
        emailTemplate.getAdditionalInformation().put("shareLink", emailShareDto.getShareUrl());
        emailTemplate.getAdditionalInformation().put("buttonText", "Event Shared");

        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), emailShareDto.getShareWithEmail(), templateEngine,
                mailService, emailTemplate).send();
        model.setViewName("redirect:" + emailShareDto.getShareUrl());

        if (eventId != null)
            eventService.shareBySocialProvider(eventId, SocialType.EMAIl);
        return model;
    }

    @PostMapping("/show/nearby/count")
    public ResponseEntity<Integer> showNearByEventsCount(@RequestBody @Valid EventSearchDto eventSearchDto) {
        return new ResponseEntity<>(eventService.getNearByEventsCount(eventSearchDto), HttpStatus.OK);
    }
}
