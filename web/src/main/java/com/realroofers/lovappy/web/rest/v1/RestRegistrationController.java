package com.realroofers.lovappy.web.rest.v1;

import com.google.common.base.Strings;
import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.support.PhotoNames;
import com.realroofers.lovappy.service.exception.EntityValidationException;
import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.user.UserProfileAndPreferenceService;
import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.*;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.PrefHeight;
import com.realroofers.lovappy.service.user.support.Size;
import com.realroofers.lovappy.service.user.support.SubscriptionType;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.FormValidator;
import com.realroofers.lovappy.service.validator.UserRegister;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.exception.BadRequestException;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.social.twitter.api.TweetData;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ViewResolver;
import org.apache.commons.lang.StringUtils;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;

/**
 * @author Eias Altawil
 */

@RestController
@RequestMapping({"/api/v1/registration"})
public class RestRegistrationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(RestRegistrationController.class);
	private final UserService userService;
	private final UserProfileAndPreferenceService userProfileAndPreferenceService;
	private final UserProfileService userProfileService;
	private final FormValidator formValidator;
	private ITemplateEngine templateEngine;
	private MailService mailService;
	private final TwitterTemplate twitterTemplate;
	private final EmailTemplateService emailTemplateService;

	private final CloudStorageService cloudStorageService;
	private final LovstampService lovstampService;

	private final NotificationService notificationService;
	private final CloudStorage cloudStorage;
	@Value("${lovappy.cloud.bucket.images}")
	private String imagesBucket;

	@Autowired
	public RestRegistrationController(UserProfileAndPreferenceService userProfileAndPreferenceService,
									  UserProfileService userProfileService, UserService userService, FormValidator formValidator,
									  List<ViewResolver> viewResolvers, MailService mailService, TwitterTemplate twitterTemplate,
									  EmailTemplateService emailTemplateService, CloudStorage cloudStorage,
									  CloudStorageService cloudStorageService, LovstampService lovstampService, NotificationService notificationService) {
		this.userService = userService;
		this.userProfileAndPreferenceService = userProfileAndPreferenceService;
		this.userProfileService = userProfileService;
		this.formValidator = formValidator;
		this.mailService = mailService;
		this.twitterTemplate = twitterTemplate;
		this.emailTemplateService = emailTemplateService;
		this.cloudStorageService = cloudStorageService;
		this.lovstampService = lovstampService;
		this.notificationService = notificationService;

		for (ViewResolver viewResolver : viewResolvers) {
			if (viewResolver instanceof ThymeleafViewResolver) {
				ThymeleafViewResolver thymeleafViewResolver = (ThymeleafViewResolver) viewResolver;
				this.templateEngine = thymeleafViewResolver.getTemplateEngine();
			}
		}


		this.cloudStorage = cloudStorage;
	}




	@RequestMapping(value = "/step1", method = RequestMethod.POST)
	public ResponseEntity<Void> postPage1(@RequestParam(value = "gender", required = false) Gender gender,
			@RequestParam(value = "prefGender", required = false) Gender prefGender,
			@RequestParam(value = "birthDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date birthDate,
			@RequestParam("location.longitude") Double longitude, @RequestParam("location.latitude") Double latitude,
			@RequestParam("location.lastMileRadiusSearch") Integer radius, @RequestParam("userType") String userType) {

		GenderAndPrefGenderDto dto = new GenderAndPrefGenderDto();
		dto.setGender(gender);
		dto.setPrefGender(prefGender);
		dto.setBirthDate(birthDate);
		dto.setLocation(new AddressDto(longitude, latitude, radius));
		dto.setUserType(userType);

		List<ErrorMessage> errorMessages = formValidator.validate(dto, new Class[] { UserRegister.class });

//		if (longitude == null || latitude == null)
//			errorMessages.add(new ErrorMessage("location", "Location is required."));

		if (dto.getBirthDate() == null) {
			errorMessages.add(new ErrorMessage("birthDate", "Date of Birth must be provided."));
		}
		int age = 0;
		if (dto.getBirthDate() != null && dto.getBirthDate().toInstant() != null) {
			LocalDate birthDateLocal = dto.getBirthDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			age = Period.between(birthDateLocal, LocalDate.now()).getYears();
		}
		if (age < 18)
			errorMessages.add(new ErrorMessage("birthDate", "You should be at least 18 to continue."));

		if (errorMessages.size() > 0) {
			throw new EntityValidationException(errorMessages);
		}
		userProfileAndPreferenceService.saveOrUpdate(UserUtil.INSTANCE.getCurrentUser().getUserId(), dto);
		return ResponseEntity.ok().build();
	}


	@RequestMapping(value = "/location/save", method = RequestMethod.POST)
	public ResponseEntity<?> postPage1(@RequestBody WithinRadiusDto searchDto) {

		LOGGER.info("Daoud latitude {}, long {}, rad {}", searchDto.getLatitude(), searchDto.getLongitude(), searchDto.getRadius());
		AddressDto addressDto = userProfileAndPreferenceService.saveLocation(UserUtil.INSTANCE.getCurrentUser().getUserId(), new AddressDto(searchDto.getLatitude(), searchDto.getLongitude(), searchDto.getRadius()), searchDto.getRadius());

		return ResponseEntity.ok(addressDto);
	}


	@RequestMapping(value = "/step2", method = RequestMethod.POST)
	public ResponseEntity<Void> postPage2(@RequestParam(value="heightFt",  required = false) Integer heightFt,
			@RequestParam(value="heightIn",  required = false) Integer heightIn, @RequestParam(value="heightCM",  required = false) Double heightCM,

			@RequestParam(value = "penisSizeMatter", required = false) Boolean penisSizeMatter,
			@RequestParam(value = "prefBustSize", required = false) Size prefBustSize,
			@RequestParam(value = "prefButtSize", required = false) Size prefButtSize,
			@RequestParam(value="prefMinAge",  required = false) Integer prefMinAge, @RequestParam(value="prefMaxAge",  required = false) Integer prefMaxAge,
			@RequestParam(value = "prefHeight",  required = false) PrefHeight prefHeight) {

		Integer userID = UserUtil.INSTANCE.getCurrentUser().getUserId();
		GenderAndPrefGenderDto profile = userProfileAndPreferenceService.getGenderAndPrefGender(userID);

		AgeHeightLangAndPrefSizeAgeHeightLangDto dto = new AgeHeightLangAndPrefSizeAgeHeightLangDto(heightFt, heightIn,
				heightCM, penisSizeMatter, prefBustSize, prefButtSize, prefMinAge, prefMaxAge, prefHeight,
				profile.getPrefGender());

		List<ErrorMessage> errorMessages = formValidator.validate(dto, new Class[] { UserRegister.class });

		if (dto.getPrefGender() == null) {
			throw new EntityValidationException(errorMessages);
		} else {
			if (Gender.MALE.equals(dto.getPrefGender())) {
				if(penisSizeMatter == null )
					errorMessages.add(new ErrorMessage("penisSizeMatter", "Does Size Matter?"));

			} else if (Gender.FEMALE.equals(dto.getPrefGender())) {
				if(prefBustSize == null )
					errorMessages.add(new ErrorMessage("prefBustSize", "Preferred Breast size required"));
				if(prefButtSize == null )
					errorMessages.add(new ErrorMessage("prefButtSize", "Preferred Butt size required"));


			} else {
				if(penisSizeMatter == null )
					errorMessages.add(new ErrorMessage("penisSizeMatter", "Does Size Matter?"));

				if(prefBustSize == null )
					errorMessages.add(new ErrorMessage("prefBustSize", "Preferred Breast size required"));
				if(prefButtSize == null )
					errorMessages.add(new ErrorMessage("prefButtSize", "Preferred Butt size required"));

			}

			if((heightCM == null || heightCM == 0d) && (heightFt == null || heightFt == 0d) && ( heightIn == null || heightIn == 0d))
				errorMessages.add(new ErrorMessage("heightFt", "What is the height you are seeking?"));

			if(prefMinAge == null || prefMaxAge == null || prefMinAge > prefMaxAge)
				errorMessages.add(new ErrorMessage("prefMinAge", "What is the age you are seeking?"));
			if(prefHeight == null )
				errorMessages.add(new ErrorMessage("prefHeight", "Preferred height required"));
		}

		if (errorMessages.size() > 0)
			throw new EntityValidationException(errorMessages);

		userProfileAndPreferenceService.saveOrUpdate(userID, dto);

		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/step3", method = RequestMethod.POST)
	public ResponseEntity<Void> postPage3(@RequestParam(value = "catORdog", required = false) String catORdog,
			@RequestParam(value = "drinker", required = false) String drinker,
			@RequestParam(value = "smoker", required = false) String smoker,
			@RequestParam(value = "user", required = false) String user,
			@RequestParam(value = "no-habit", required = false) String nohabit,
			@RequestParam(value = "ruralORurbanORsuburban", required = false) String ruralORurbanORsuburban,
			@RequestParam(value = "politicalopinion", required = false) String politicalopinion,
			@RequestParam(value = "outdoorsyORindoorsy", required = false) String outdoorsyORindoorsy,
			@RequestParam(value = "frugalORbigspender", required = false) String frugalORbigspender,
			@RequestParam(value = "morningpersonORnightowl", required = false) String morningpersonORnightowl,
			@RequestParam(value = "extrovertORintrovert", required = false) String extrovertORintrovert) {

		Map<String, String> lifestyles = new HashMap<>();
		lifestyles.put("catORdog", catORdog);

		if(!StringUtils.isEmpty(nohabit) && (!StringUtils.isEmpty(drinker) ||
				!StringUtils.isEmpty(smoker ) || !StringUtils.isEmpty(user ))) {
			throw new BadRequestException("No Habbit cant be chosen with drinker and smoker");
		}

		if (!StringUtils.isEmpty(nohabit))
			lifestyles.put("no-habit", nohabit);
		else{
			if (!StringUtils.isEmpty(drinker))
				lifestyles.put("drinker", drinker);
			if (!StringUtils.isEmpty(smoker ))
				lifestyles.put("smoker", smoker);
			if (!StringUtils.isEmpty(user ))
				lifestyles.put("user", user);
		}
		lifestyles.put("ruralORurbanORsuburban", ruralORurbanORsuburban);
		lifestyles.put("politicalopinion", politicalopinion);

		Map<String, String> personalities = new HashMap<>();
		personalities.put("outdoorsyORindoorsy", outdoorsyORindoorsy);
		personalities.put("frugalORbigspender", frugalORbigspender);
		personalities.put("morningpersonORnightowl", morningpersonORnightowl);
		personalities.put("extrovertORintrovert", extrovertORintrovert);

		PersonalityLifestyleDto dto = new PersonalityLifestyleDto();
		dto.setLifestyles(lifestyles);
		dto.setPersonalities(personalities);

		List<ErrorMessage> errorMessages = formValidator.validate(dto, new Class[] { UserRegister.class });

		if (catORdog == null)
			errorMessages.add(new ErrorMessage("catORdog", "What do you prefer?"));

		if (lifestyles.get("drinker") == null && lifestyles.get("smoker") == null && lifestyles.get("user") == null
				&& lifestyles.get("no-habit") == null)
			errorMessages.add(new ErrorMessage("habit", "What is your habit?"));

		if (lifestyles.get("ruralORurbanORsuburban") == null)
			errorMessages.add(new ErrorMessage("ruralORurbanORsuburban", "What is your ideal neighborhood?"));

		if (lifestyles.get("politicalopinion") == null)
			errorMessages.add(new ErrorMessage("politicalopinion", "What is your political opinion?"));

		if (personalities.get("outdoorsyORindoorsy") == null)
			errorMessages.add(new ErrorMessage("outdoorsyORindoorsy", "Are you outdoorsy or indoorsy?"));

		if (personalities.get("frugalORbigspender") == null)
			errorMessages.add(new ErrorMessage("frugalORbigspender", "Are you frugal or big spender?"));

		if (personalities.get("morningpersonORnightowl") == null)
			errorMessages.add(new ErrorMessage("morningpersonORnightowl", "Are you morning person or night owl?"));

		if (personalities.get("extrovertORintrovert") == null)
			errorMessages.add(new ErrorMessage("extrovertORintrovert", "Are you extrovert or introvert?"));

		if (errorMessages.size() > 0)
			throw new EntityValidationException(errorMessages);

		userProfileService.saveOrUpdate(UserUtil.INSTANCE.getCurrentUserId(), dto);
		return ResponseEntity.ok().build();
	}



	private void sendUserMail(String userEmail, PhotoNames photoNames, String emailTemplateConstant) {
        EmailTemplateDto emailTemplate =
                emailTemplateService.getByNameAndLanguage(emailTemplateConstant, "EN");
        emailTemplate.getVariables().put("##PHOTO##", photoNames.getValue());
        new EmailTemplateFactory()
                .build(null, userEmail, templateEngine, mailService, emailTemplate)
                .send();
	}
	
	

	@RequestMapping(value = "/step4", method = RequestMethod.POST)
	public ResponseEntity<Void> postPage4(@RequestParam(value = "employed", required = false) String employed,
			@RequestParam(value = "risktaker", required = false) String risktaker,
			@RequestParam(value = "believeingod", required = false) String believeingod,
			@RequestParam(value = "wantkids", required = false) String wantkids,
			@RequestParam(value = "havekids", required = false) String havekids,
			@RequestParam(value = "attractmany", required = false) String attractmany,
			@RequestParam("language") String language, @RequestParam("prefLanguage") String prefLanguage) {

		Map<String, String> statuses = new HashMap<>();
		statuses.put("employed", employed);
		statuses.put("risktaker", risktaker);
		statuses.put("believeingod", believeingod);
		statuses.put("wantkids", wantkids);
		statuses.put("havekids", havekids);
		statuses.put("attractmany", attractmany);

		StatusLangDto dto = new StatusLangDto();
		dto.setStatuses(statuses);
		dto.setLanguage(language);
		dto.setPrefLanguage(prefLanguage);

		List<ErrorMessage> errorMessages = formValidator.validate(dto, new Class[] { UserRegister.class });

		if (dto.getStatuses() == null || dto.getStatuses().get("employed") == null)
			errorMessages.add(new ErrorMessage("employed", "Are you currently employed?"));

		if (dto.getStatuses() == null || dto.getStatuses().get("risktaker") == null)
			errorMessages.add(new ErrorMessage("risktaker", "Are you a risk taker?"));

		if (dto.getStatuses() == null || dto.getStatuses().get("believeingod") == null)
			errorMessages.add(new ErrorMessage("believeingod", "Are you a religious or spiritual?"));

		if (dto.getStatuses() == null || dto.getStatuses().get("wantkids") == null)
			errorMessages.add(new ErrorMessage("wantkids", "Do you want kids?"));

		if (dto.getStatuses() == null || dto.getStatuses().get("havekids") == null)
			errorMessages.add(new ErrorMessage("havekids", "Do you have kids?"));

		if (dto.getStatuses() == null || dto.getStatuses().get("attractmany") == null)
			errorMessages.add(new ErrorMessage("attractmany", "I attract many people?"));

		if (Strings.isNullOrEmpty(dto.getLanguage()))
			errorMessages.add(new ErrorMessage("language", "What languages do you speak?"));

		if (Strings.isNullOrEmpty(dto.getPrefLanguage()))
			errorMessages.add(new ErrorMessage("prefLanguage", "What is the languages you are seeking?"));

		if (errorMessages.size() > 0)
			throw new EntityValidationException(errorMessages);

		String[] languagesIDs1 = dto.getLanguage().split(",");
		List<LanguageDto> lista = new ArrayList<>();
		for (String s : languagesIDs1) {
			lista.add(new LanguageDto(Integer.valueOf(s.trim())));
		}
		dto.setSpeakingLanguages(lista);

		String[] languagesIDs2 = dto.getPrefLanguage().split(",");
		List<LanguageDto> listb = new ArrayList<>();
		for (String s : languagesIDs2) {
			listb.add(new LanguageDto(Integer.valueOf(s.trim())));
		}
		dto.setSeekingLanguages(listb);

		userProfileAndPreferenceService.saveOrUpdate(UserUtil.INSTANCE.getCurrentUserId(), dto);

		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/author", method = RequestMethod.POST)
	public ResponseEntity<?> authorRegistration(@RequestParam(value = "language", defaultValue = "EN") String language,
			HttpServletRequest request) {
		UserDto userDto = userService.getUser(UserUtil.INSTANCE.getCurrentUserId());
		if (!userDto.getEmailVerified()) {
			String key = UUID.randomUUID().toString();
			UserDto newUser = userService.updateVerifyEmailKey(UserUtil.INSTANCE.getCurrentUserId(), key);
			userService.assignRole(Roles.AUTHOR.getValue(), UserUtil.INSTANCE.getCurrentUserId());
			if (newUser != null) {

				EmailTemplateDto emailTemplate = emailTemplateService
						.getByNameAndLanguage(EmailTemplateConstants.ACCOUNT_VERIFICATION, language);
				String baseUrl = CommonUtils.getBaseURL(request);
				emailTemplate.getAdditionalInformation().put("url", baseUrl + "/register/verify/" + key);
				new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), userDto.getEmail(), templateEngine,
						mailService, emailTemplate).send();

			}
			return ResponseEntity.ok("not-social");
			// Should redirect to /account_not_verified
		} else {
			userService.assignRole(Roles.AUTHOR.getValue(), UserUtil.INSTANCE.getCurrentUserId());
			if (!userDto.getTweeted()) {
				registrationTweet(userDto);

			}
		}
		return ResponseEntity.ok("social");
	}

	@RequestMapping(value = "/musician", method = RequestMethod.POST)
	public ResponseEntity<?> musicianRegistration(
			@RequestParam(value = "language", defaultValue = "EN") String language, HttpServletRequest request) {
		UserDto userDto = userService.getUser(UserUtil.INSTANCE.getCurrentUserId());
		if (!userDto.getEmailVerified()) {
			String key = UUID.randomUUID().toString();
			UserDto newUser = userService.updateVerifyEmailKey(UserUtil.INSTANCE.getCurrentUserId(), key);
			userService.assignRole(Roles.MUSICIAN.getValue(), UserUtil.INSTANCE.getCurrentUserId());
			if (newUser != null) {

				EmailTemplateDto emailTemplate = emailTemplateService
						.getByNameAndLanguage(EmailTemplateConstants.ACCOUNT_VERIFICATION, language);
				String baseUrl = CommonUtils.getBaseURL(request);
				emailTemplate.getAdditionalInformation().put("url", baseUrl + "/register/verify/" + key);
				new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), userDto.getEmail(), templateEngine,
						mailService, emailTemplate).send();

			}
			return ResponseEntity.ok("not-social");
			// Should redirect to /account_not_verified
		} else {

			userService.assignRole(Roles.MUSICIAN.getValue(), UserUtil.INSTANCE.getCurrentUserId());
			if (!userDto.getTweeted()) {
				registrationTweet(userDto);

			}
		}

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		UserAuthDto userAuthDto = UserUtil.INSTANCE.getCurrentUser();
		if (UserAuthDto.class.isAssignableFrom(authentication.getPrincipal().getClass())) {

			UsernamePasswordAuthenticationToken updatedAuth = new UsernamePasswordAuthenticationToken(userAuthDto,
					userAuthDto.getSocialId(), userService.getAuthorities(userDto.getID()));

			updatedAuth.setDetails(authentication.getPrincipal());

			SecurityContextHolder.getContext().setAuthentication(updatedAuth);
			// add authentication to the session
			request.getSession(true).setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
					SecurityContextHolder.getContext());
		}
		return ResponseEntity.ok("social");
	}

	@RequestMapping(value = "/subscription", method = RequestMethod.POST)
	public ResponseEntity<?> postSubscription(
			@RequestParam(value = "subscriptionType") SubscriptionType subscriptionType,
			@RequestParam(value = "language", defaultValue = "EN") String language, @RequestHeader("host") String host,
			HttpServletRequest request) {

		Integer userId = UserUtil.INSTANCE.getCurrentUserId();
		userProfileService.saveSubscriptionType(userId, subscriptionType);

		UserDto user = userService.getUser(userId);
		if (!user.getTweeted()) {
			registrationTweet(user);
		}

		if (user.getEmailVerified() != null && user.getEmailVerified() ) {
			return ResponseEntity.ok("social");
		}
		String key = UUID.randomUUID().toString();
		UserDto newUser = userService.updateVerifyEmailKey(userId, key);

		if (newUser != null) {

			EmailTemplateDto emailTemplate = emailTemplateService
					.getByNameAndLanguage(EmailTemplateConstants.ACCOUNT_VERIFICATION, language);
			String baseUrl = CommonUtils.getBaseURL(request);
			emailTemplate.getAdditionalInformation().put("url", baseUrl + "/register/verify/" + key);
			new EmailTemplateFactory()
					.build(CommonUtils.getBaseURL(request), user.getEmail(), templateEngine, mailService, emailTemplate)
					.send();

		}
		return ResponseEntity.ok("not-social");
		// Should redirect to /account_not_verified
	}

	private void registrationTweet(UserDto user){
		try{
			// Tweet new status on lovappy account about new registered user
			Integer numberOfMales = userService.countUsersByGender(Gender.MALE);
			Integer numberOfFemales = userService.countUsersByGender(Gender.FEMALE);
			String address = user.getUserProfile().getAddress() != null
					? "from #" + user.getUserProfile().getAddress().getFullAddress() : "";
			TweetData tweetData = new TweetData(String.format("%d members males %d female %d new member %s",
					(numberOfMales + numberOfFemales), numberOfMales, numberOfFemales, address));
			twitterTemplate.timelineOperations().updateStatus(tweetData);
			userService.setTweeted(true, user.getID());
		} catch (Exception ex) {
			LOGGER.error("Cant tweet error {}", ex);
		}
	}
}
