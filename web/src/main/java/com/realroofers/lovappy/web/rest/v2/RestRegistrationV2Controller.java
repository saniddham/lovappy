package com.realroofers.lovappy.web.rest.v2;

import com.realroofers.lovappy.service.cloud.support.PhotoNames;
import com.realroofers.lovappy.service.lovstamps.LovstampSampleService;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampSampleDto;
import com.realroofers.lovappy.service.lovstamps.support.SampleTypes;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.user.UserProfileAndPreferenceService;
import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.*;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.SubscriptionType;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.FormValidator;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.service.validation.ResponseStatus;
import com.realroofers.lovappy.service.validator.UserRegister;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.exception.DuplicateUserEmailException;
import com.realroofers.lovappy.web.exception.ValidationException;
import com.realroofers.lovappy.web.rest.dto.UserRegistrationRequest;
import com.realroofers.lovappy.web.util.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.social.twitter.api.TweetData;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.List;
import java.util.UUID;



@RestController
@RequestMapping({"/api/v2"})
public class RestRegistrationV2Controller {

	private static final Logger LOGGER = LoggerFactory.getLogger(RestRegistrationV2Controller.class);
	private final UserService userService;
	private final LovstampSampleService lovstampSampleService;
	private final UserProfileAndPreferenceService userProfileAndPreferenceService;
	private final UserProfileService userProfileService;
	private final FormValidator formValidator;
	private ITemplateEngine templateEngine;
	private MailService mailService;
	private final TwitterTemplate twitterTemplate;
	private final EmailTemplateService emailTemplateService;

	private final NotificationService notificationService;
	@Value("${lovappy.cloud.bucket.images}")
	private String imagesBucket;

	@Autowired
	public RestRegistrationV2Controller(UserProfileAndPreferenceService userProfileAndPreferenceService,
										UserProfileService userProfileService, UserService userService, FormValidator formValidator,
										List<ViewResolver> viewResolvers, LovstampSampleService lovstampSampleService, MailService mailService, TwitterTemplate twitterTemplate,
										EmailTemplateService emailTemplateService, NotificationService notificationService) {
		this.userService = userService;
		this.userProfileAndPreferenceService = userProfileAndPreferenceService;
		this.userProfileService = userProfileService;
		this.formValidator = formValidator;
		this.lovstampSampleService = lovstampSampleService;
		this.mailService = mailService;
		this.twitterTemplate = twitterTemplate;
		this.emailTemplateService = emailTemplateService;
		this.notificationService = notificationService;

		for (ViewResolver viewResolver : viewResolvers) {
			if (viewResolver instanceof ThymeleafViewResolver) {
				ThymeleafViewResolver thymeleafViewResolver = (ThymeleafViewResolver) viewResolver;
				this.templateEngine = thymeleafViewResolver.getTemplateEngine();
			}
		}


	}

	@RequestMapping(value = "/registration/gender", method = RequestMethod.POST)
	@org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.NO_CONTENT)
	public void postPage1(@RequestBody GenderAndPrefGenderDto dto) {

		List<ErrorMessage> errorMessages = formValidator.validate(dto, new Class[] { UserRegister.class });

//		if (dto.getLocation().getLatitude() == null || dto.getLocation().getLatitude() == null)
//			errorMessages.add(new ErrorMessage("location", "Location is required."));

		if (dto.getBirthDate() == null) {
			errorMessages.add(new ErrorMessage("birthDate", "Date of Birth must be provided."));
		}
		int age = 0;
		if (dto.getBirthDate() != null && dto.getBirthDate().toInstant() != null) {
			LocalDate birthDateLocal = dto.getBirthDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			age = Period.between(birthDateLocal, LocalDate.now()).getYears();
		}
		if (age < 18)
			errorMessages.add(new ErrorMessage("birthDate", "You should be at least 18 to continue."));

		if (errorMessages.size() > 0) {
			throw new ValidationException("Validation Error", errorMessages);
		}
		LOGGER.info("Gender registration {} " , dto);
		dto.setUserType(null);
		userProfileAndPreferenceService.update(UserUtil.INSTANCE.getCurrentUser().getUserId(), dto);
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public JsonResponse registerNewUser(HttpServletRequest request, @RequestBody UserRegistrationRequest userRegistrationRequest) {

		JsonResponse response = new JsonResponse();

		UserDto user = new UserDto();
		user.setEmail(userRegistrationRequest.getEmail());
		user.setPassword(userRegistrationRequest.getPassword());
		user.setConfirmPassword(userRegistrationRequest.getConfirmPassword());

		List<ErrorMessage> errorMessages = formValidator.validate(userRegistrationRequest, new Class[]{UserRegister.class});

		if (!user.getPassword().equals(user.getConfirmPassword()))
			errorMessages.add(new ErrorMessage("confirmPassword", "Password doesn't match"));

		if (userService.emailExists(user.getEmail()))
			throw new DuplicateUserEmailException("You are already registered");

		if (errorMessages.size() > 0) {
			throw new ValidationException("Validation error", errorMessages);
		} else {
			WithinRadiusDto withinRadiusDto = null;
			LOGGER.info("location " + userRegistrationRequest.getLatitude() + "" + userRegistrationRequest.getLongitude());
			if(userRegistrationRequest.getLatitude() != null && userRegistrationRequest.getLongitude() != null) {
				withinRadiusDto = new WithinRadiusDto();
				withinRadiusDto.setLatitude(userRegistrationRequest.getLatitude());
				withinRadiusDto.setLongitude(userRegistrationRequest.getLongitude());
				withinRadiusDto.setRadius(600);
			}
			UserDto newUser = userService.registerNewUser(user,  withinRadiusDto);

			userProfileService.saveSubscriptionType(newUser.getID(), SubscriptionType.LITE);


			if (newUser.getEmailVerified() ) {

				if (!newUser.getTweeted()) {
					registrationTweet(user);
				}
			}
			String key = UUID.randomUUID().toString();
			newUser = userService.updateVerifyEmailKey(newUser.getID(), key);

			if (newUser != null) {

				EmailTemplateDto emailTemplate = emailTemplateService
						.getByNameAndLanguage(EmailTemplateConstants.ACCOUNT_VERIFICATION, "en");
				String baseUrl = CommonUtils.getBaseURL(request);
				emailTemplate.getAdditionalInformation().put("url", baseUrl + "/register/verify/" + key);
				new EmailTemplateFactory()
						.build(CommonUtils.getBaseURL(request), user.getEmail(), templateEngine, mailService, emailTemplate)
						.send();

			}
			NotificationDto notificationDto = new NotificationDto();
			notificationDto.setContent(newUser.getEmail() + " is new member in lovappy ");
			notificationDto.setType(NotificationTypes.USER_REGISTRATION);
			notificationDto.setUserId(newUser.getID());
			notificationService.sendNotificationToAdmins(notificationDto);

			response.setStatus(ResponseStatus.SUCCESS);
			userRegistrationRequest.setPassword(null);
			userRegistrationRequest.setConfirmPassword(null);
			response.setResult(userRegistrationRequest);
		}
		return response;
	}







	private void sendUserMail(String userEmail, PhotoNames photoNames, String emailTemplateConstant) {
        EmailTemplateDto emailTemplate =
                emailTemplateService.getByNameAndLanguage(emailTemplateConstant, "EN");
        emailTemplate.getVariables().put("##PHOTO##", photoNames.getValue());
        new EmailTemplateFactory()
                .build(null, userEmail, templateEngine, mailService, emailTemplate)
                .send();
	}

	@RequestMapping(value = "/location/save", method = RequestMethod.POST)
	public ResponseEntity<?> postPage1(@RequestBody WithinRadiusDto searchDto) {

		LOGGER.info("latitude {}, long {}, rad {}", searchDto.getLatitude(), searchDto.getLongitude(), searchDto.getRadius());
		AddressDto addressDto = userProfileAndPreferenceService.saveLocation(UserUtil.INSTANCE.getCurrentUser().getUserId(), new AddressDto(searchDto.getLatitude(), searchDto.getLongitude(), searchDto.getRadius()), searchDto.getRadius());

		return ResponseEntity.ok(addressDto);
	}

	@RequestMapping(value = "/address/save", method = RequestMethod.POST)
	public ResponseEntity<?> savelocation(@RequestBody AddressDto searchDto) {

		LOGGER.info("latitude {}, long {}, rad {}", searchDto.getLatitude(), searchDto.getLongitude());
		AddressDto addressDto = userProfileAndPreferenceService.saveAddress(UserUtil.INSTANCE.getCurrentUser().getUserId(), searchDto);

		return ResponseEntity.ok(addressDto);
	}

	@RequestMapping(value = "/voices/samples", method = RequestMethod.GET)
	public Page<LovstampSampleDto> getAllSampleSounds(@RequestParam("language") String languageID,
													  @RequestParam("page") int page,
													  @RequestParam("limit") int limit){
			UserProfileDto profile = userProfileService.findOne(UserUtil.INSTANCE.getCurrentUserId());
			return lovstampSampleService.getLovstampSamplesByLanguageAndGenderAndType(languageID, profile.getGender(), SampleTypes.SAMPLE, new PageRequest(page - 1, limit));
		}

	private void registrationTweet(UserDto user){
		try{
			// Tweet new status on lovappy account about new registered user
			Integer numberOfMales = userService.countUsersByGender(Gender.MALE);
			Integer numberOfFemales = userService.countUsersByGender(Gender.FEMALE);
			String address = user.getUserProfile().getAddress() != null
					? "from #" + user.getUserProfile().getAddress().getFullAddress() : "";
			TweetData tweetData = new TweetData(String.format("%d members males %d female %d new member %s",
					(numberOfMales + numberOfFemales), numberOfMales, numberOfFemales, address));
			twitterTemplate.timelineOperations().updateStatus(tweetData);
			userService.setTweeted(true, user.getID());
		} catch (Exception ex) {
			LOGGER.error("Cant tweet error {}", ex);
		}
	}
}
