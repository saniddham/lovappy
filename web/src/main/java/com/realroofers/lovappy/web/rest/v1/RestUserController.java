package com.realroofers.lovappy.web.rest.v1;


import com.realroofers.lovappy.service.exception.EntityValidationException;
import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampDto;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.user.UserPreferenceService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.MyAccountDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.dto.UserPreferenceDto;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.FormValidator;
import com.realroofers.lovappy.service.validator.UserRegister;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.exception.BadRequestException;
import com.realroofers.lovappy.web.exception.ResourceNotFoundException;
import com.realroofers.lovappy.web.util.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author Eias Altawil
 */

@RestController
@RequestMapping({"/api/v1/user"})
public class RestUserController {

    private final UserService userService;
    private final FormValidator formValidator;
    private final NotificationService notificationService;
    private final EmailTemplateService emailTemplateService;
    private ITemplateEngine templateEngine;
    private final MailService mailService;
    private final UserPreferenceService userPreferenceService;
    private final LovstampService lovstampService;
    @Autowired
    public RestUserController(UserService userService, FormValidator formValidator, NotificationService notificationService,
                              List<ViewResolver> viewResolvers, EmailTemplateService emailTemplateService, MailService mailService, UserPreferenceService userPreferenceService, LovstampService lovstampService) {
        this.userService = userService;
        this.formValidator = formValidator;
        this.notificationService = notificationService;
        this.emailTemplateService = emailTemplateService;
        this.mailService = mailService;
        this.userPreferenceService = userPreferenceService;
        this.lovstampService = lovstampService;

        for (ViewResolver viewResolver : viewResolvers) {
            if (viewResolver instanceof ThymeleafViewResolver) {
                ThymeleafViewResolver thymeleafViewResolver = (ThymeleafViewResolver) viewResolver;
                this.templateEngine = thymeleafViewResolver.getTemplateEngine();
            }
        }
    }



    @RequestMapping(value = "/resend_activation_email", method = RequestMethod.POST)
    public ResponseEntity<Void> resendActivationEmail(@RequestParam(value = "language", defaultValue = "EN") String language,
                                                      HttpServletRequest request) {
        String key = UUID.randomUUID().toString();
        UserDto user = userService.updateVerifyEmailKey(UserUtil.INSTANCE.getCurrentUserId(), key);

        if (user != null) {
            EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.ACCOUNT_VERIFICATION, language);
            String baseUrl = CommonUtils.getBaseURL(request);
            emailTemplate.getAdditionalInformation().put("url", baseUrl + "/register/verify/" + key);
            new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), user.getEmail(), templateEngine,
                    mailService, emailTemplate).send();
        }
        return ResponseEntity.ok().build();
    }


    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<UserDto> registerNewUser(@RequestParam("email") String email,
                                                   @RequestParam("password") String password,
                                                   @RequestParam("confirm_password") String confirmPassword,
                                                   @RequestParam("zip_code") String zipCode) {
        UserDto user = new UserDto();
        user.setEmail(email);
        user.setPassword(password);
        user.setConfirmPassword(confirmPassword);
        user.setZipcode(zipCode);

        List<ErrorMessage> errorMessages = formValidator.validate(user, new Class[]{UserRegister.class});

        if (!user.getPassword().equals(user.getConfirmPassword()))
            errorMessages.add(new ErrorMessage("confirmPassword", "Password doesn't match"));

        if (userService.emailExists(user.getEmail()))
            errorMessages.add(new ErrorMessage("email", "You are already registered"));

        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);

        } else {
            User newUser = userService.registerNewUser(user);

            NotificationDto notificationDto = new NotificationDto();
            notificationDto.setContent(newUser.getEmail() + " is new member in lovappy ");
            notificationDto.setType(NotificationTypes.USER_REGISTRATION);
            notificationDto.setUserId(newUser.getUserId());
            notificationService.sendNotificationToAdmins(notificationDto);

            UserDto userDto = userService.getUser(newUser.getUserId());

            return ResponseEntity.ok(userDto);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<UserDto> getUserProfile(@RequestParam(value = "id", required = false) Integer id) {
        if (id == null)
            id = UserUtil.INSTANCE.getCurrentUserId();

        UserDto user = userService.getUser(id);

        if (user == null)
            return ResponseEntity.notFound().build();
        else
            return ResponseEntity.ok(user);
    }


    @PostMapping(value = {"/settings/save"})
    public void save(@Valid MyAccountDto myAccount) {

        MyAccountDto myAccountDto = userService.updateMyAccount(UserUtil.INSTANCE.getCurrentUserId(), myAccount);

        if (myAccountDto == null)
           throw new BadRequestException("Error while updating account.");

    }

    @RequestMapping(value = "/settings", method = RequestMethod.POST)
    public void updateSettings(@RequestBody MyAccountDto myAccount) {

        MyAccountDto myAccountDto = userService.updateMyAccount(UserUtil.INSTANCE.getCurrentUserId(), myAccount);

        if (myAccountDto == null)
            throw new BadRequestException("Error while updating account.");

    }
    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public MyAccountDto getSettings() {

        MyAccountDto myAccountDto = userService.getMyAccount(UserUtil.INSTANCE.getCurrentUserId());

        if (myAccountDto == null)
            throw new ResourceNotFoundException("Error User not found");

        return myAccountDto;
    }
    @RequestMapping(value = "/verification/resend", method = RequestMethod.GET)
    public void resendVerdication(HttpServletRequest request,
                              @RequestParam(value = "lang", defaultValue = "EN") String language) {

       resendVerdicationByUser(request, language, UserUtil.INSTANCE.getCurrentUserId());
    }


    @GetMapping(value = {"/verification/resend/{userId}"})
    public void resendVerdicationByUser(HttpServletRequest request,
                                  @RequestParam(value = "lang", defaultValue = "EN") String language,
                                        @PathVariable("userId") Integer userId) {

        String key = UUID.randomUUID().toString();
        UserDto newUser = userService.updateVerifyEmailKey(userId, key);
        if (newUser != null) {
            EmailTemplateDto emailTemplate = emailTemplateService
                    .getByNameAndLanguage(EmailTemplateConstants.ACCOUNT_VERIFICATION, language);
            String baseUrl = CommonUtils.getBaseURL(request);
            emailTemplate.getAdditionalInformation().put("url", baseUrl + "/register/verify/" + key);
            new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), newUser.getEmail(), templateEngine,
                    mailService, emailTemplate).send();

        }
    }

    @RequestMapping(value = "/password/reset", method = RequestMethod.POST)
    public ResponseEntity<?> passwordReset(HttpServletRequest request, @RequestParam(value = "lang", defaultValue = "EN") String language,
    @RequestParam(value="email") String email) {

        EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.RESET_PASSWORD, language);
        String baseUrl = CommonUtils.getBaseURL(request);
        emailTemplate.getAdditionalInformation().put("baseUrl", baseUrl);
        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), email, templateEngine,
                mailService, emailTemplate).send();

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/activate", method = RequestMethod.DELETE)
    public void deactivate(HttpServletRequest request, @RequestParam(value = "lang", defaultValue = "EN") String language) {
        //send email

        EmailTemplateDto emailTemplate = emailTemplateService
                .getByNameAndLanguage(EmailTemplateConstants.ACCOUNT_DEACTIVATION, language);
        String baseUrl = CommonUtils.getBaseURL(request);
        emailTemplate.getAdditionalInformation().put("link", baseUrl + "/my_account/deactivate");
        emailTemplate.getAdditionalInformation().put("resetLink", baseUrl + "/reset");
        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), UserUtil.INSTANCE.getCurrentUser().getEmail(), templateEngine,
                mailService, emailTemplate).send();

    }
    @RequestMapping(value = "/activate", method = RequestMethod.POST)
    public void activate() {
        userService.activateUser(UserUtil.INSTANCE.getCurrentUserId(), true);
    }
}