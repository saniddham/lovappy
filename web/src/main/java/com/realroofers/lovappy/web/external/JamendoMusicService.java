package com.realroofers.lovappy.web.external;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.music.dto.GenreDto;
import com.realroofers.lovappy.service.music.dto.MusicDto;
import com.realroofers.lovappy.service.user.support.Gender;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Daoud Shaheen on 11/25/2017.
 */
@Service
public class JamendoMusicService {

    @Value("${music.jamendo.clientId}")
    private String jamendoClientId;

    @Value("${music.jamendo.secret}")
    private String jamendoSecret;

    @Value("${music.jamendo.baseurl}")
    private String baseUrl;

    private RestTemplate restTemplate;

    @PostConstruct
    public void init(){
        this.restTemplate = new RestTemplate();
        Map<String, String> variables = new HashMap<>();
        variables.put("client_id", "04c41e37");
        variables.put("format", "json");
        restTemplate.setDefaultUriVariables(variables);
    }

    public  List<MusicDto> getSongs(int page, int limit){
//https://api.jamendo.com/v3.0/tracks/?client_id=04c41e37&format=jsonpretty&limit=2&speed=high+veryhigh&include=musicinfo+lyrics&groupby=artist_id

        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(baseUrl + "/tracks/")
                // Add query parameter
                .queryParam("speed", "high")
                .queryParam("client_id", jamendoClientId)
                .queryParam("limit", limit)
                .queryParam("format", "json")
                .queryParam("offset", page - 1)
                .queryParam("include", "musicinfo", "lyrics")
                .queryParam("groupby", "artist_id");

        ResponseEntity<ObjectNode> response = restTemplate.getForEntity(builder.build().encode().toUriString(), ObjectNode.class);
        List<MusicDto> musicDtoList = new ArrayList<>();
        if(response.getStatusCode().equals(HttpStatus.OK)) {
            ObjectNode responseBody = response.getBody();
            JsonNode results = responseBody.get("results");
                for(int i =0 ; i< results.size() ; i++ ) {
                    musicDtoList.add(jsontoMusic(results.get(i)));
                }


        }
        return musicDtoList;
    }

    private MusicDto jsontoMusic(JsonNode response) {
        MusicDto musicDto = new MusicDto();
        musicDto.setLyrics(response.get("lyrics").asText());
        musicDto.setProviderResourceId(response.get("id").asLong());
        musicDto.setCoverImageUrl(response.get("image").asText());
        musicDto.setArtistName(response.get("artist_name").asText());

        musicDto.setFileUrl(response.get("audio").asText());
        musicDto.setPreviewFileUrl(musicDto.getFileUrl());
        musicDto.setDownloadUrl(response.get("audiodownload").asText());
        if(response.has("genres")) {
            JsonNode array = response.get("genres");
            if(array.size() > 0) {
                GenreDto genreDto = new GenreDto();
                genreDto.setTitle(array.get(0).asText());
                musicDto.setGenre(genreDto);
            }
        }


        if(response.has("musicinfo") &&  response.get("musicinfo").has("gender")){
            musicDto.setGender(Gender.valueOf( response.get("musicinfo").get("gender").asText().toUpperCase()));
        }
           musicDto.setTitle(response.get("name").asText());
        return musicDto;
    }
}
