package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.gift.GiftExchangeService;
import com.realroofers.lovappy.service.gift.dto.FilerObject;
import com.realroofers.lovappy.service.product.BrandCommissionService;
import com.realroofers.lovappy.service.product.ProductService;
import com.realroofers.lovappy.service.product.ProductTypeService;
import com.realroofers.lovappy.service.product.RetailerCommissionService;
import com.realroofers.lovappy.service.product.dto.ProductDto;
import com.realroofers.lovappy.service.product.dto.ProductTypeDto;
import com.realroofers.lovappy.service.product.model.ProductType;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.vendor.VendorService;
import com.realroofers.lovappy.web.util.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Manoj on 07/02/2018.
 */
@Controller
@RequestMapping("/lox/product")
public class ProductController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 10;
    private static final int[] PAGE_SIZES = {10};

    private final ProductTypeService productTypeService;
    private final ProductService productService;
    private final VendorService vendorService;
    private final BrandCommissionService brandCommissionService;
    private final RetailerCommissionService retailerCommissionService;
    private final GiftExchangeService giftExchangeService;

    @Autowired
    public ProductController(ProductTypeService productTypeService,
                             ProductService productService, VendorService vendorService, BrandCommissionService brandCommissionService, RetailerCommissionService retailerCommissionService, GiftExchangeService giftExchangeService) {
        this.productTypeService = productTypeService;
        this.productService = productService;
        this.vendorService = vendorService;
        this.brandCommissionService = brandCommissionService;
        this.retailerCommissionService = retailerCommissionService;
        this.giftExchangeService = giftExchangeService;
    }

    @GetMapping("")
    public ModelAndView loadProducts(@RequestParam("pageSize") Optional<Integer> pageSize,
                                     @RequestParam("page") Optional<Integer> page,
                                     @RequestParam(value = "searchText", defaultValue = "", required = false) String searchText) {

        ModelAndView modelAndView = new ModelAndView("admin/product/product-management");

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        PageRequest pageable = new PageRequest(evalPage, evalPageSize);

        modelAndView.addObject("searchText", searchText);
        Page<ProductDto> products = productService.findAllByNameOrderByApproved(searchText, pageable);
        Pager pager = new Pager(products.getTotalPages(), products.getNumber(), BUTTONS_TO_SHOW);
        modelAndView.addObject("products", products);

        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pageSizes", PAGE_SIZES);
        modelAndView.addObject("pager", pager);
        return modelAndView;
    }

    @GetMapping("/updateProductStatus{id}")
    public ModelAndView updateProductStatus(@PathVariable("id") Integer id) {

        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        productService.updateProductStatus(id, userId);

        return new ModelAndView("redirect:/lox/product");
    }

    @GetMapping("/approveProduct{id}")
    public ModelAndView approveProduct(@PathVariable("id") Integer id) {

        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        productService.approveProduct(id, userId);

        return new ModelAndView("redirect:/lox/product");
    }

    @GetMapping("/updateProductStatusAdmin{id}")
    public ModelAndView updateProductStatusAdmin(@PathVariable("id") Integer id) {

        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

//        ProductDto productDto = productService.findById(id);
//        if (productDto!=null){
//            VendorDto vendorDto = vendorService.findByUser(productDto.getCreatedBy().getID());
//
//            if (vendorDto != null) {
//                if (vendorDto.getVendorType().equals(VendorType.VENDOR)) {
//                    BrandCommissionDto currentCommission = brandCommissionService.getCurrentCommission();
//                    if (currentCommission != null) {
//                        productService.updateProductStatusAdmin(id, userId);
//                    }
//                } else {
//                    RetailerCommissionDto currentCommission = retailerCommissionService.getCurrentCommission(vendorDto.getCountry());
//                    if (currentCommission != null) {
//                        productService.updateProductStatusAdmin(id, userId);
//                    }
//                }
//            }
//        }

        productService.updateProductStatusAdmin(id, userId);

        return new ModelAndView("redirect:/lox/product");
    }


    @GetMapping("/sales")
    public ModelAndView sales(@SortDefault("id") Pageable pageable,
                              @ModelAttribute(value = "filerObject") FilerObject filerObject) {

        ModelAndView model = new ModelAndView();

        model.addObject("filerObject", filerObject != null ? filerObject : new FilerObject());
        model.addObject("page", giftExchangeService.findAllSales(pageable,filerObject));

        model.setViewName("admin/product/sales");
        return model;
    }


    @GetMapping("/type")
    public ModelAndView addProductType(ModelAndView model) {

        List<ProductTypeDto> productTypeList = productTypeService.findAllActive();
        if (productTypeList == null || productTypeList.size() < 0) {
            productTypeList = new ArrayList<>();
        }
        model.addObject("productTypeList", productTypeList);
        model.setViewName("admin/product/product-type");
        return model;
    }

    @GetMapping("/type/new")
    public ModelAndView addNewProductType() {
        ModelAndView mav = new ModelAndView("admin/product/product-type-details");
        mav.addObject("productType", new ProductType());
        return mav;
    }

    @GetMapping("/type/{id}")
    public ModelAndView ProductTypeDetails(@PathVariable(value = "id") Integer id) {
        ModelAndView mav = new ModelAndView("admin/product/product-type-details");
        mav.addObject("productType", productTypeService.findById(id));
        return mav;
    }

    @PostMapping("/type/save")
    public ResponseEntity saveProductType(@ModelAttribute("productType") @Valid ProductTypeDto productType) {
        ProductTypeDto addProductType = null;
        if (productType.getId() == null) {
            try {
                addProductType = productTypeService.create(productType);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
            }

            return ResponseEntity.status(HttpStatus.OK).body(addProductType.getId());
        } else {
            ProductTypeDto updateProductType = null;
            try {
                updateProductType = productTypeService.update(productType);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
            }
            return ResponseEntity.status(HttpStatus.OK).body(updateProductType.getId());
        }

    }

}
