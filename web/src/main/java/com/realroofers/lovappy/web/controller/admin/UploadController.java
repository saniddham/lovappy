package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.upload.FileUploadService;
import com.realroofers.lovappy.service.upload.dto.FileUploadDto;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

/**
 * Created by Manoj Senevirathne
 */
@Controller
@RequestMapping("/lox/upload")
public class UploadController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UploadController.class);

    private final CloudStorage cloudStorage;
    private final FileUploadService fileUploadService;

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 20;
    private static final int[] PAGE_SIZES = {5, 10, 20};


    public UploadController(CloudStorage cloudStorage, FileUploadService fileUploadService) {
        this.cloudStorage = cloudStorage;
        this.fileUploadService = fileUploadService;
    }


    @GetMapping("/manager")
    public ModelAndView getPageManagement(@RequestParam("pageSize") Optional<Integer> pageSize,
                                        @RequestParam("page") Optional<Integer> page,
                                        @RequestParam(value = "searchText", defaultValue = "", required = false) String searchText) {

        ModelAndView modelAndView = new ModelAndView("admin/upload/upload-management");

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        PageRequest pageable = new PageRequest(evalPage, evalPageSize);

        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        modelAndView.addObject("searchText", searchText);
        org.springframework.data.domain.Page<FileUploadDto> pages = fileUploadService.findAll(pageable,userId);
        Pager pager = new Pager(pages.getTotalPages(), pages.getNumber(), BUTTONS_TO_SHOW);
        modelAndView.addObject("pages", pages);

        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pageSizes", PAGE_SIZES);
        modelAndView.addObject("pager", pager);
        return modelAndView;
    }


}
