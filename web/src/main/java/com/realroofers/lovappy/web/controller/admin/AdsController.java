package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.ads.AdsService;
import com.realroofers.lovappy.service.ads.dto.AdBackgroundImageDto;
import com.realroofers.lovappy.service.ads.dto.AdDto;
import com.realroofers.lovappy.service.ads.dto.AdPricingDto;
import com.realroofers.lovappy.service.ads.dto.AdSettings;
import com.realroofers.lovappy.service.ads.support.AdMediaType;
import com.realroofers.lovappy.service.ads.support.AdStatus;
import com.realroofers.lovappy.service.ads.support.AdType;
import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import com.realroofers.lovappy.service.exception.EntityValidationException;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import com.realroofers.lovappy.web.util.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

/**
 * Created by Eias Altawil on 7/11/2017
 */

@Controller
@RequestMapping("/lox/ads")
public class AdsController {

    private final AdsService adsService;
    private final CMSService cmsService;
    private final MailService mailService;
    private final ITemplateEngine templateEngine;
    private final EmailTemplateService emailTemplateService;
    private final CloudStorage cloudStorage;
    private final CloudStorageService cloudStorageService;

    @Value("${lovappy.cloud.bucket.ads.backgrounds}")
    private String adsBackgroundsBucket;

    @Autowired
    public AdsController(AdsService adsService, CMSService cmsService, MailService mailService, ITemplateEngine templateEngine,
                         EmailTemplateService emailTemplateService, CloudStorage cloudStorage,
                         CloudStorageService cloudStorageService) {
        this.adsService = adsService;
        this.cmsService = cmsService;
        this.mailService = mailService;
        this.templateEngine = templateEngine;
        this.emailTemplateService = emailTemplateService;
        this.cloudStorage = cloudStorage;
        this.cloudStorageService = cloudStorageService;
    }

    @GetMapping
    public ModelAndView list(ModelAndView model,
                             @RequestParam("pageSize") Optional<Integer> pageSize,
                             @RequestParam("page") Optional<Integer> pageNumber,
                             @RequestParam(value = "filter", defaultValue = "") String filter,
                             @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){

        PageRequest pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize));

        if(!StringUtils.isEmpty(filter)) {
            if(pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize), new Sort(sort, filter));
            }
        }
        model.addObject("filter", filter);
        model.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);
        Page<AdDto> adDtoPage = adsService.findAll(pageable);
        model.addObject("ads", adDtoPage);
        Pager pager = new Pager(adDtoPage.getTotalPages(), adDtoPage.getNumber(), Pager.BUTTONS_TO_SHOW);

        model.addObject("selectedPageSize", pageSize.orElse(Pager.INITIAL_PAGE_SIZE));
        model.addObject("pageSizes", Pager.PAGE_SIZES);
        model.addObject("pager", pager);
        model.setViewName("admin/ads/ads_list");
        return model;
    }

    @GetMapping("/{id}")
    public ModelAndView adDetails(ModelAndView model, @PathVariable(value = "id") Integer id){
        model.addObject("ad",  adsService.getAd(id));
        model.setViewName("admin/ads/ad_details");
        return model;
    }

    @GetMapping("/settings")
    public ModelAndView settings(ModelAndView model){
        AdSettings settings = new AdSettings(cmsService.getPageSettings(PageConstants.ADVERTISE_WITH_US));
        model.addObject("settings", settings);
        model.setViewName("admin/ads/ad_settings");
        return model;
    }

    @PostMapping("/settings/save")
    public ModelAndView saveSettings(AdSettings adSettings, ModelAndView model){

        cmsService.updateSettings(PageConstants.ADVERTISE_WITH_US, adSettings.toMap());
        model.setViewName("redirect:/lox/ads/settings");
        return model;
    }
    @GetMapping("/backgrounds")
    public ModelAndView backgrounds(ModelAndView model){
        model.addObject("images",  adsService.getAllBackgroundImages());
        model.addObject("colors",  adsService.getAllBackgroundColors());
        model.setViewName("admin/ads/backgrounds");
        return model;
    }

    @GetMapping("/pricing")
    public ModelAndView pricingSetup(ModelAndView model) {
        model.setViewName("admin/ads/pricing_setup");

        Map<String, List<AdPricingDto>> adsPricing = new HashMap<>();

        adsPricing.put("ADVERTISEMENT", adsService.getAdPricing(AdType.ADVERTISEMENT));
        adsPricing.put("COMEDY", adsService.getAdPricing(AdType.COMEDY));
        adsPricing.put("NEWS", adsService.getAdPricing(AdType.NEWS));
        adsPricing.put("PODCAST", adsService.getAdPricing(AdType.PODCAST));
        adsPricing.put("MUSIC", adsService.getAdPricing(AdType.MUSIC));
        adsPricing.put("AUDIO BOOK", adsService.getAdPricing(AdType.AUDIO_BOOK));

        model.addObject("adsPricing", adsPricing);

        return model;
    }

    @GetMapping("/pricing/new")
    public ModelAndView pricingSetupNew(ModelAndView model) {
        model.setViewName("admin/ads/pricing_setup_details");
        model.addObject("pricing", new AdPricingDto());
        return model;
    }

    @GetMapping("/pricing/{id}")
    public ModelAndView pricingSetupEdit(ModelAndView model, @PathVariable(value = "id") Integer id) {
        model.setViewName("admin/ads/pricing_setup_details");
        model.addObject("pricing", adsService.getAdPricingById(id));
        return model;
    }

    @PostMapping("/pricing")
    public ResponseEntity savePricing(@RequestParam("id") Integer id,
                                      @RequestParam("duration") Integer duration,
                                      @RequestParam("durationUnit") String durationUnit,
                                      @RequestParam("adType") AdType adType,
                                      @RequestParam("mediaType") AdMediaType mediaType,
                                      @RequestParam("price") Double price) {

        if (duration != null && durationUnit.equals("Minutes"))
            duration *= 60;

        AdPricingDto adPricing = new AdPricingDto();
        adPricing.setId(id);
        adPricing.setDuration(duration);
        adPricing.setAdType(adType);
        adPricing.setMediaType(mediaType);
        adPricing.setPrice(price);

        AdPricingDto pricing = adsService.savePricing(adPricing);

        return ResponseEntity.status(HttpStatus.OK).body(pricing);
    }

    @PostMapping("/approve")
    public ResponseEntity approveAd(HttpServletRequest request, @RequestParam("id") Integer id){
        AdDto adDto = adsService.changeAdStatus(id, AdStatus.APPROVED);

        //send approval email
        EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.SUBMITTED_AD_APPROVAL, "EN");
        emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/advertise");
        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), adDto.getEmail(), templateEngine,
                mailService, emailTemplate).send();
        return ResponseEntity.status(HttpStatus.OK).body("");
    }

    @PostMapping("/reject")
    public ResponseEntity rejectAd(HttpServletRequest request, @RequestParam("id") Integer id){
        AdDto adDto = adsService.changeAdStatus(id, AdStatus.REJECTED);

        //send denial email
        EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.SUBMITTED_AD_DENIAL, "EN");
        emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/advertise");
        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), adDto.getEmail(), templateEngine,
                mailService, emailTemplate).send();
        return ResponseEntity.status(HttpStatus.OK).body("");
    }

    @PostMapping("/disable")
    public ResponseEntity disableAd(@RequestParam("id") Integer id){
        adsService.changeAdStatus(id, AdStatus.DISABLED);
        return ResponseEntity.status(HttpStatus.OK).body("");
    }

    @PostMapping("/enable")
    public ResponseEntity enableAd(@RequestParam("id") Integer id){
        adsService.changeAdStatus(id, AdStatus.APPROVED);
        return ResponseEntity.status(HttpStatus.OK).body("");
    }

    @PostMapping("/background/image")
    public ResponseEntity<AdBackgroundImageDto> uploadBackgroundImage(@RequestParam("image") MultipartFile image,
                                                                      @RequestParam("featured_image") MultipartFile featuredImage){
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (image.isEmpty())
            errorMessages.add(new ErrorMessage("cover", "You should add cover image."));

        if (errorMessages.size() > 0)
            throw new EntityValidationException(errorMessages);

        try {
            CloudStorageFileDto cloudStorageFile =
                    cloudStorage.uploadFile(image, adsBackgroundsBucket, FileExtension.png.toString(), "image/png");

            CloudStorageFileDto cloudStorageFile2 =
                    cloudStorage.uploadFile(featuredImage, adsBackgroundsBucket, FileExtension.png.toString(), "image/png");

            AdBackgroundImageDto adBackgroundImage = adsService.addBackgroundImage(cloudStorageFile,cloudStorageFile2);

            return ResponseEntity.ok(adBackgroundImage);

        } catch (IOException ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/background/color")
    public ResponseEntity<Void> addBackgroundColor(@RequestParam("color") String color){
        adsService.addBackgroundColor(color);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/background/color/delete")
    public ResponseEntity<Void> deleteBackgroundColor(@RequestParam("id") Integer id){
        adsService.deleteBackgroundColor(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/background/image/delete")
    public ResponseEntity<Void> deleteBackgroundImage(@RequestParam("id") Integer id){
        adsService.deleteBackgroundImage(id);
        return ResponseEntity.ok().build();
    }
}
