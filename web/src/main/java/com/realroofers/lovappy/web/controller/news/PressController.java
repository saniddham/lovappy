package com.realroofers.lovappy.web.controller.news;

import com.realroofers.lovappy.service.news.NewsStoryService;
import com.realroofers.lovappy.service.news.dto.NewsDto;
import com.realroofers.lovappy.service.news.dto.NewsStoryAdminDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/news")
public class PressController {
    @Autowired
    private NewsStoryService newsStoryService;

    @RequestMapping(value = {"/search"}, method = RequestMethod.GET)
    public ResponseEntity<List<NewsStoryAdminDto>> getSearchedNewsStories(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        String txtSearch = request.getParameter("txtSearch");
        // resultMap.put("newsStories", newsStoryService.getAllNewsStoryAdminDtoByTitleAndContain(txtSearch));
        List<NewsStoryAdminDto> list = newsStoryService.getAllNewsStoryAdminDtoByTitleAndContain(txtSearch);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(value = {"/approved"}, method = RequestMethod.GET)
    public ResponseEntity<List<NewsDto>> getApprovedNewsStories() {

        List<NewsDto> list = newsStoryService.getApprovedNewsStories();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(value = {"/latest"}, method = RequestMethod.GET)
    public ResponseEntity<List<NewsDto>> getLatestNewsStories() {

        List<NewsDto> list = newsStoryService.getLatesNewsStories();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

}
