package com.realroofers.lovappy.web.config.security;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Eias Altawil
 */

public class AuthorizationHandler extends AccessDeniedHandlerImpl {
    @Override
    public void handle(HttpServletRequest req, HttpServletResponse resp, AccessDeniedException reason)
            throws ServletException, IOException {

        if(req.getRequestURI().contains("/api/")){
            resp.sendError(HttpServletResponse.SC_FORBIDDEN, reason.getMessage());
            return;
        }

        super.handle(req, resp, reason);
    }
}
