package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.exception.EntityValidationException;
import com.realroofers.lovappy.service.questions.DailyQuestionService;
import com.realroofers.lovappy.service.questions.dto.DailyQuestionDto;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darrel Rayen on 10/29/17.
 */
@Controller
@RequestMapping("/lox/questions")
public class DailyQuestionManagementController {

    private final DailyQuestionService dailyQuestionService;

    @Autowired
    public DailyQuestionManagementController(DailyQuestionService dailyQuestionService) {
        this.dailyQuestionService = dailyQuestionService;
    }

    @GetMapping
    public ModelAndView getAllQuestions(ModelAndView model, @RequestParam(value = "page", defaultValue = "1") Integer page) {

        model.addObject("newQuestion", new DailyQuestionDto());
        model.addObject("questions", dailyQuestionService.getAllQuestions(new PageRequest(page - 1, 10)));
        model.addObject("actionUrl", "/lox/questions?");
        model.addObject("currentPage", page);
        model.setViewName("admin/questions/questions");
        return model;
    }

    @GetMapping("/{id}")
    public ModelAndView getAllQuestion(ModelAndView model, @PathVariable("id") Long id, @RequestParam(value = "page", defaultValue = "1") Integer page) {
        model.addObject("question", dailyQuestionService.findQuestionById(id));
        model.addObject("responses", dailyQuestionService.findResponsesByQuestionId(id, new PageRequest(page - 1, 15)));
        model.addObject("actionUrl", "/lox/questions/" + id + "?");
        model.addObject("currentPage", page);
        model.setViewName("admin/questions/question");
        return model;
    }

    @PostMapping
    public ResponseEntity addDailyQuestion(@ModelAttribute("newQuestion") DailyQuestionDto questionDto) {
        List<ErrorMessage> messages = new ArrayList<>();

        if (questionDto.getQuestion().isEmpty() || questionDto.getQuestion() == null) {
            messages.add(new ErrorMessage("question", "Please type a Question"));
        }
        if (messages.size() > 0) {
            throw new EntityValidationException(messages);
        } else {
            DailyQuestionDto dailyQuestionDto = dailyQuestionService.addQuestion(questionDto);

            if (dailyQuestionDto != null) {
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    @GetMapping("/delete/{id}")
    public ModelAndView deleteQuestion(ModelAndView model, @PathVariable("id") Long id) {
        Boolean isDeleted = dailyQuestionService.deleteDailyQuestion(id);
        model.setViewName("redirect:/lox/questions");
        return model;
    }

    @PutMapping("/{id}")
    public ResponseEntity updateDailyQuestion(@PathVariable("id") Long id, @RequestParam("question") String question) {
        List<ErrorMessage> messages = new ArrayList<>();

        if (question.isEmpty()) {
            messages.add(new ErrorMessage("question", "Please type a Question"));
        }
        if (messages.size() > 0) {
            throw new EntityValidationException(messages);
        } else {
            DailyQuestionDto dailyQuestionDto = dailyQuestionService.updateQuestion(id, question);
            if (dailyQuestionDto != null) {
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    @PostMapping("/response/approve/{id}")
    public ResponseEntity approveResponse(@PathVariable("id") Long responseId) {

        boolean isApproved = dailyQuestionService.changeResponseApprovalStatus(responseId, ApprovalStatus.APPROVED);
        if (isApproved) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/response/reject/{id}")
    public ResponseEntity rejectResponse(@PathVariable("id") Long responseId) {

        boolean isApproved = dailyQuestionService.changeResponseApprovalStatus(responseId, ApprovalStatus.REJECTED);
        if (isApproved) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
