package com.realroofers.lovappy.web.dto;

import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 11/4/2017.
 */
public class InviteAdminRequest implements Serializable{
    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
