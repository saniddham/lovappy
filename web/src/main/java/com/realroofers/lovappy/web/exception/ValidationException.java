package com.realroofers.lovappy.web.exception;

import com.realroofers.lovappy.service.validation.ErrorMessage;

import java.util.List;

public class ValidationException extends RuntimeException {
    private List<ErrorMessage> errorMessages;

    public ValidationException(List<ErrorMessage> errorMessages) {
        this.errorMessages = errorMessages;
    }
    public ValidationException(String error, List<ErrorMessage> errorMessages) {
        super(error);
        this.errorMessages = errorMessages;

    }
    public List<ErrorMessage> getErrorMessages() {
        return errorMessages;
    }
}