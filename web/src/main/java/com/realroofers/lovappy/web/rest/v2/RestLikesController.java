package com.realroofers.lovappy.web.rest.v2;

import com.realroofers.lovappy.service.likes.LikeService;
import com.realroofers.lovappy.service.likes.dto.LikeDto;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.MyAccountDto;
import com.realroofers.lovappy.service.user.dto.UserLikesDto;
import com.realroofers.lovappy.web.controller.LikeController;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.exception.BadRequestException;
import com.realroofers.lovappy.web.util.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Daoud Shaheen on 5/2/2018.
 */
@RestController
@RequestMapping("/api/v2/likes")
public class RestLikesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LikeController.class);

    private final LikeService likeService;
    private final UserService userService;
    private final EmailTemplateService emailTemplateService;
    private final MailService mailService;
    private final ITemplateEngine templateEngine;

    @Autowired
    public RestLikesController(LikeService likeService, UserService userService, EmailTemplateService emailTemplateService, MailService mailService, ITemplateEngine templateEngine) {
        super();
        this.likeService = likeService;
        this.userService = userService;
        this.emailTemplateService = emailTemplateService;
        this.mailService = mailService;
        this.templateEngine = templateEngine;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{user-id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void sendALike(@PathVariable("user-id") Integer userId,
                                       HttpServletRequest request,
                                       @RequestParam(value = "lang", defaultValue = "EN") String language) {
        if (UserUtil.INSTANCE.getCurrentUserId().equals(userId)) {
            LOGGER.error("Can not like the same as login User :  {} ", UserUtil.INSTANCE.getCurrentUser().getEmail());
            throw new BadRequestException("Can not like the same as login User");
        }
        //validate, if the user already like him/her, addLike should be skip or give error response
        try {
            LikeDto like = likeService.addLike(UserUtil.INSTANCE.getCurrentUserId(), userId);

            if(like != null) {
                //get Like acount
                MyAccountDto myAccountDto = userService.getMyAccount(userId);

                EmailTemplateDto emailTemplate = null;
                //send match email
                if (likeService.match(UserUtil.INSTANCE.getCurrentUserId(), userId)) {
                    //send Likes email
                    if (myAccountDto.isNewMatchesNotifications())
                        emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.NEW_MATCHES, language);
                } else if (myAccountDto.isNewLikesNotifications()) {
                    //send Likes email
                    emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.NEW_LIKES, language);
                }

                if(emailTemplate != null) {
                    emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/member/" + userId);
                    new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), like.getLikeTo().getEmail(), templateEngine,
                            mailService, emailTemplate).send();
                }
            }
//			}

        }catch (Exception ex) {
            LOGGER.error(" error add new like",ex);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{user-id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void unLike(@PathVariable("user-id") Integer userId) {
        LOGGER.info("DELETE sendALike To {}", userId);
        if (UserUtil.INSTANCE.getCurrentUserId().equals(userId)) {
            LOGGER.error("Can not like the same as login User :  {} ", UserUtil.INSTANCE.getCurrentUser().getEmail());
            throw new BadRequestException("Can not like the same as login User");
        }
        //validate, if the user already like him/her, addLike should be skip or give error response
        likeService.unLike(UserUtil.INSTANCE.getCurrentUserId(), userId);

    }
    /**
     * Get Profile That Login User like
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/mylikes")

    public  Page<UserLikesDto> myLikes(@RequestParam(name = "page", required = true, defaultValue = "1") Integer page,
                                       @RequestParam(name = "limit", required = true, defaultValue = "10") Integer limit) {

        Page<UserLikesDto> myLikes = likeService.getLikesByUserLikeBy(page, limit, UserUtil.INSTANCE.getCurrentUserId());
                 return myLikes;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/likeme")
    public   Page<UserLikesDto>  likeMe(@RequestParam(name = "page", required = true, defaultValue = "1") Integer page,
                               @RequestParam(name = "limit", required = true, defaultValue = "10") Integer limit) {
        Page<UserLikesDto> likeme = likeService.getLikesByUserLikeTo(page, limit, UserUtil.INSTANCE.getCurrentUserId());

        return likeme;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/matches")
    public   Page<UserLikesDto>  matches(@RequestParam(name = "page", required = true, defaultValue = "1") Integer page,
                                         @RequestParam(name = "limit", required = true, defaultValue = "10") Integer limit) {

        Page<UserLikesDto> matches = likeService.getMatchesForUser(page, limit, UserUtil.INSTANCE.getCurrentUserId());
        return matches;
    }
}
