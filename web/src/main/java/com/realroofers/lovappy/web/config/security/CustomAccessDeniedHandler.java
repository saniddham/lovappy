package com.realroofers.lovappy.web.config.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;

/**
 * Created by Daoud Shaheen on 02/09/2018.
 */
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

  private static final Logger logger = LoggerFactory.getLogger(CustomAccessDeniedHandler.class);

  @Override
  public void handle(HttpServletRequest request, HttpServletResponse response,
      AccessDeniedException accessDeniedException) throws IOException, ServletException {

    // Requested URI
    String requestURI = request.getRequestURI();

    // Logged in User
    String loggedInUser = "anonymous";
    if (SecurityContextHolder.getContext().getAuthentication() != null) {
      loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
    }

    logger.error(" access is denied, User with email {} is trying to access this resource {} ",
        loggedInUser, requestURI, accessDeniedException.getLocalizedMessage());
    response.sendError(HttpStatus.FORBIDDEN.value(), accessDeniedException.getLocalizedMessage());
  }

}
