package com.realroofers.lovappy.web.controller.news;

import com.realroofers.lovappy.service.blog.CategoryService;
import com.realroofers.lovappy.service.blog.PostService;
import com.realroofers.lovappy.service.blog.dto.CategoryDto;
import com.realroofers.lovappy.service.blog.model.Category;
import com.realroofers.lovappy.service.blog.model.Post;
import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.news.NewsCategoryService;
import com.realroofers.lovappy.service.news.NewsStoryService;
import com.realroofers.lovappy.service.news.dto.NewsCategoryDto;
import com.realroofers.lovappy.service.news.dto.NewsDto;
import com.realroofers.lovappy.service.news.model.NewsCategory;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by Daoud Shaheen on 7/7/2017.
 */
@Controller
@RequestMapping("/lox/news2")
public class NewsCategoryController {

    private static final Logger LOGGER = LoggerFactory.getLogger(NewsCategoryController.class);

    @Autowired
    private NewsCategoryService newsCategoryService;

    @Autowired
    private NewsStoryService newsStoryService;

    @Value("${lovappy.cloud.bucket.images}")
    protected String imagesBucket;

    //ADMIN CATEGORY
    @GetMapping("/category")
    public ModelAndView categoryList(ModelAndView model,
                                     @RequestParam("pageSize") Optional<Integer> pageSize,
                                     @RequestParam("page") Optional<Integer> pageNumber,
                                     @RequestParam(value = "filter", defaultValue = "") String filter,
                                     @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){


        System.out.println("received the news listing request");
        PageRequest pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize));
        if(!StringUtils.isEmpty(filter)) {
            if(pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize), new Sort(sort, filter));
            }
        }
        System.out.println("this is controller pageable");
        System.out.println(pageable);
        model.addObject("filter", filter);
        model.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);

        Page<NewsCategoryDto> categories = newsCategoryService.findAll(pageable);
        model.addObject("categories", categories);
        System.out.println("this is new categories");
        System.out.println(categories);

        model.addObject("category", new NewsCategoryDto());
        Pager pager = new Pager(categories.getTotalPages(), categories.getNumber(), Pager.BUTTONS_TO_SHOW);

        model.addObject("selectedPageSize", pageSize.orElse(Pager.INITIAL_PAGE_SIZE));
        model.addObject("pageSizes", Pager.PAGE_SIZES);
        model.addObject("pager", pager);
        model.setViewName("admin/news/manage_news_category");
        return model;
    }

    //ADMIN CATEGORY
    @GetMapping("/category/{id}")
    public ModelAndView getCategory(ModelAndView model, @PathVariable(value = "id") Integer id) {
        LOGGER.debug("==========Get Dashboard categoryList============" + id);
        model.getModelMap().addAttribute("category", newsCategoryService.getOne(new Long(id)));
        model.setViewName("admin/news/category_details");
        return model;
    }


    @PostMapping("/category/save")
    public ModelAndView saveNewCategory(@Valid NewsCategoryDto category, BindingResult err, ModelAndView model, RedirectAttributes redirectAttributes, Pageable pageable) {
        if (err.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.category", err);
            redirectAttributes.addFlashAttribute("category", category);
            model.setViewName("redirect:/lox/news2/category");
            return model;
        }

        NewsCategoryDto existingCategory = newsCategoryService.getCategoryByName(category.getCategoryName());
        if (existingCategory != null) {
            redirectAttributes.addFlashAttribute("category", category);
            redirectAttributes.addFlashAttribute("errorMessage", "Duplicate Category");
            model.setViewName("redirect:/lox/news2/category");
            return model;
        }
        NewsCategory entity = new NewsCategory(category);
        entity.setStatus(1);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
//            User user = (User) authentication.getDetails();
//            entity.setCreatedBy(user);
//        }
        newsCategoryService.save(entity);
        model.setViewName("redirect:/lox/news2/category");
        return model;
    }

    @PostMapping("/category/{id}/save")
    public ModelAndView updateCategory(@PathVariable("id") Integer categoryId,
                                       @Valid NewsCategoryDto categoryDto, BindingResult err, ModelAndView model, RedirectAttributes redirectAttributes, Pageable pageable) {
        if (err.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.category", err);
            redirectAttributes.addFlashAttribute("category", categoryDto);
            model.setViewName("redirect:/lox/news2/category");
            return model;
        }

        NewsCategoryDto newsCategory= newsCategoryService.getCategoryById(new Long(categoryId));
        if (newsCategory == null) {
            redirectAttributes.addFlashAttribute("category", categoryDto);
            redirectAttributes.addFlashAttribute("errorTableMessage", "News Category Doesn't exist with the name " + categoryDto.getCategoryName());
            model.setViewName("redirect:/lox/news2/category");
            return model;
        }
        System.out.println("received data");
        System.out.println(categoryDto.toString());
        newsCategory.setCategoryName(categoryDto.getCategoryName());
        newsCategoryService.save(newsCategory);
        model.setViewName("redirect:/lox/news2/category");
        return model;
    }

    @GetMapping("/category/{id}/delete")
    public ModelAndView deleteCategory(@PathVariable("id") Integer categoryId, RedirectAttributes redirectAttributes, Pageable pageable) {

        List<NewsDto> posts = newsStoryService.getNewsByCategoryId(new Long(categoryId));
        if (posts != null && posts.size() > 0) {
            NewsCategoryDto category = newsCategoryService.getCategoryById(new Long(categoryId));
            if (category != null) {
                redirectAttributes.addFlashAttribute("errorTableMessage", "Category " + category.getCategoryName() + " is used already ");
            } else {
                redirectAttributes.addFlashAttribute("errorTableMessage", "Category is used already ");
            }
            LOGGER.info("There are posts");
            return new ModelAndView("redirect:/lox/news2/category");
        }
        newsCategoryService.delete(categoryId);
        return new ModelAndView("redirect:/lox/news2/category");
    }


    @RequestMapping(value = { "/category/status/{id}/{status}" }, method = RequestMethod.GET)
    public ModelAndView updateStatus(@PathVariable Long id, @PathVariable int status) {

        NewsCategoryDto category=newsCategoryService.getCategoryById(id);
        category.setStatus(status);
        NewsCategoryDto savedCategory=newsCategoryService.save(category);
        return  new ModelAndView("redirect:/lox/news2/category");
    }

}
