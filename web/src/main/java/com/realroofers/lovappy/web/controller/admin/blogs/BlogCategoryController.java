package com.realroofers.lovappy.web.controller.admin.blogs;

import com.realroofers.lovappy.service.blog.CategoryService;
import com.realroofers.lovappy.service.blog.PostService;
import com.realroofers.lovappy.service.blog.dto.CategoryDto;
import com.realroofers.lovappy.service.blog.model.Category;
import com.realroofers.lovappy.service.blog.model.Post;
import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampDto;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by Daoud Shaheen on 7/7/2017.
 */
@Controller
@RequestMapping("/lox/blog")
public class BlogCategoryController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlogCategoryController.class);

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private PostService postService;
    @Autowired
    private  CloudStorage cloudStorage;
    @Autowired
    private CloudStorageService cloudStorageService;

    @Value("${lovappy.cloud.bucket.images}")
    protected String imagesBucket;

    //ADMIN CATEGORY
    @GetMapping("/category")
    public ModelAndView categoryList(ModelAndView model,
                                     @RequestParam("pageSize") Optional<Integer> pageSize,
                                     @RequestParam("page") Optional<Integer> pageNumber,
                                     @RequestParam(value = "filter", defaultValue = "") String filter,
                                     @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){


        PageRequest pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize));
        if(!StringUtils.isEmpty(filter)) {
            if(pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize), new Sort(sort, filter));
            }
        }
        model.addObject("filter", filter);
        model.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);

        Page<CategoryDto> categories = categoryService.findAll(pageable);
        model.addObject("categories", categories);

        model.addObject("category", new CategoryDto());
        Pager pager = new Pager(categories.getTotalPages(), categories.getNumber(), Pager.BUTTONS_TO_SHOW);

        model.addObject("selectedPageSize", pageSize.orElse(Pager.INITIAL_PAGE_SIZE));
        model.addObject("pageSizes", Pager.PAGE_SIZES);
        model.addObject("pager", pager);
        model.setViewName("admin/blog/manage_category");
        return model;
    }

    //ADMIN CATEGORY
    @GetMapping("/category/{id}")
    public ModelAndView getCategory(ModelAndView model, @PathVariable(value = "id") Integer id) {
        LOGGER.debug("==========Get Dashboard categoryList============" + id);
        model.getModelMap().addAttribute("category", categoryService.getOne(id));
        model.setViewName("admin/blog/category_details");
        return model;
    }


    @PostMapping("/category/save")
    public ModelAndView saveNewCategory(@Valid CategoryDto category, BindingResult err, ModelAndView model, RedirectAttributes redirectAttributes, Pageable pageable) {
        if (err.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.category", err);
            redirectAttributes.addFlashAttribute("category", category);
            model.setViewName("redirect:/lox/blog/category");
            return model;
        }

        List<CategoryDto> categories = categoryService.findCategory(category.getName());
        if (categories != null && categories.size() > 0) {
            redirectAttributes.addFlashAttribute("category", category);
            redirectAttributes.addFlashAttribute("errorMessage", "Duplicate Category");
            model.setViewName("redirect:/lox/blog/category");
            return model;
        }
        Category entity = new Category(category);
        entity.setCreatedAt(new Date());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
            User user = (User) authentication.getDetails();
            entity.setCreatedBy(user);
        }
        entity.setCatState("INACTIVE");
        categoryService.save(entity);
        model.setViewName("redirect:/lox/blog/category");
        return model;
    }

    @PostMapping("/category/{id}/save")
    public ModelAndView updateCategory(@PathVariable("id") Integer categoryId,
                                       @Valid CategoryDto categoryDto, BindingResult err, ModelAndView model, RedirectAttributes redirectAttributes, Pageable pageable) {
        if (err.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.category", err);
            redirectAttributes.addFlashAttribute("category", categoryDto);
            model.setViewName("redirect:/lox/blog/category");
            return model;
        }

        List<CategoryDto> categories = categoryService.findCategory(categoryDto.getName());
        if (categories != null && categories.size() > 0) {
            redirectAttributes.addFlashAttribute("category", categoryDto);
            redirectAttributes.addFlashAttribute("errorTableMessage", "Duplicate Category");
            model.setViewName("redirect:/lox/blog/category");
            return model;
        }
        Category entity = categoryService.get(categoryId);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
            User user = (User) authentication.getDetails();
            entity.setUpdatedBy(user);
        }

        entity.setName(categoryDto.getName());
        categoryService.save(entity);
        model.setViewName("redirect:/lox/blog/category");
        return model;
    }

    @GetMapping("/category/{id}/delete")
    public ModelAndView deleteCategory(@PathVariable("id") Integer categoryId, RedirectAttributes redirectAttributes, Pageable pageable) {

        List<Post> posts = postService.findByCategory(categoryId);
        if (posts != null && posts.size() > 0) {
            CategoryDto category = categoryService.getOne(categoryId);
            if (category != null) {
                redirectAttributes.addFlashAttribute("errorTableMessage", "Category " + category.getName() + " is used already ");
            } else {
                redirectAttributes.addFlashAttribute("errorTableMessage", "Category is used already ");
            }
            LOGGER.info("There are posts");
            return new ModelAndView("redirect:/lox/blog/category");
        }
        categoryService.delete(categoryId);
        return new ModelAndView("redirect:/lox/blog/category");
    }

    @GetMapping("/category/{id}/activate")
    public ModelAndView activateCategory(@PathVariable("id") Integer categoryId) {
        Integer result = categoryService.activate(categoryId);
        return new ModelAndView("redirect:/lox/blog/category");
    }


    @GetMapping("/category/{id}/deactivate")
    public ModelAndView deactivateCategory(@PathVariable("id") Integer categoryId) {
        Integer result = categoryService.deactivate(categoryId);
        return new ModelAndView("redirect:/lox/blog/category");
    }


    @PostMapping("/category/{id}/image/save")
    public ModelAndView addImage(@PathVariable("id") Integer categoryId,
                                 @RequestParam("file") MultipartFile uploadfile,  ModelAndView model) {
        try {
            CloudStorageFileDto cloudStorageFile =
                    cloudStorage.uploadAndPersistFile(uploadfile, imagesBucket, FileExtension.png.toString(), "image/png");

            if (cloudStorageFile != null) {
                categoryService.addImage(categoryId, cloudStorageFile.getId());
                model.setViewName("redirect:/lox/blog/category/" + categoryId);
            }
        } catch (IOException e) {

        }
        model.setViewName("redirect:/lox/blog/category/" + categoryId);
        return model;
    }



    @GetMapping("/category/{id}/image/{imgId}/delete")
    public ModelAndView addImage(@PathVariable("id") Integer categoryId,
                                 @PathVariable("imgId") Long imageId,  ModelAndView model) {

        categoryService.deleteImage(categoryId, imageId);
            cloudStorageService.delete(imageId);
        model.setViewName("redirect:/lox/blog/category/" + categoryId);
        return model;
    }
}
