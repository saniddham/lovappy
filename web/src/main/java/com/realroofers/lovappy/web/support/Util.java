package com.realroofers.lovappy.web.support;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

public class Util
{

	public static boolean isEmpty(String input)
	{
        return input == null || input.trim().length() <= 0;

    }
	
	
	/**
	 * SAVE IMAGE
	 * 
	 * @param bais
	 * @param width
	 * @param height
	 * @return
	 * @throws IOException
	 */
	public static ByteArrayOutputStream crop(ByteArrayInputStream bais,
			int width, int height) throws IOException {
		BufferedImage src = ImageIO.read(bais);
		BufferedImage clipping = crop(src, width, height);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(clipping, "JPG", baos);
		return baos;
	}

	/**
	 * METHOD TO HANDLE IMAGE
	 * 
	 * @param src
	 * @param width
	 * @param height
	 * @return
	 * @throws IOException
	 */
	public static BufferedImage crop(BufferedImage src, int width, int height)
			throws IOException {
		int x = src.getWidth() / 2 - width / 2;
		int y = src.getHeight() / 2 - height / 2;

		BufferedImage clipping = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);// src.getType());
		Graphics2D area = (Graphics2D) clipping.getGraphics().create();
		area.drawImage(src, 0, 0, clipping.getWidth(), clipping.getHeight(), x,
				y, x + clipping.getWidth(), y + clipping.getHeight(), null);
		area.dispose();
		// File imageFile = new File("2.png");
		// ImageIO.write(clipping, "png", imageFile);
		return clipping;
	}

	public static <T> Predicate<T> distinctByKey(Function<? super T,Object> keyExtractor) {
		Map<Object,Boolean> seen = new ConcurrentHashMap<>();
		return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}

}

