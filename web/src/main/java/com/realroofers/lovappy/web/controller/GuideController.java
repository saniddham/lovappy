package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.cms.utils.PageConstants;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Daoud Shaheen on 8/30/2017.
 */
@Controller
public class GuideController {

    @GetMapping("/how-this-works")
    public String passwordReset() {
        return "how-this-work";
    }

    @GetMapping("/patent")
    public String patent(Model model) {
        return "info/patent";
    }



    @GetMapping("/team")
    public String team(Model model) {
        return "info/team";
    }
    @GetMapping("/faq")
    public String faq(Model model) {
        return "faq/faq";
    }


    @GetMapping("/faq/2")
    public String faqtwo(Model model) {
        return "faq/faq-two";
    }

    @GetMapping("/faq/3")
    public String faqthree(Model model) {
        return "faq/faq-three";
    }
    //DatingPlaces
    @GetMapping("/ambassadors")
    public ModelAndView ambassadorLanding(ModelAndView model) {
        model.setViewName("registration/ambassador-landing");
        return model;
    }

    //DatingPlaces
    @GetMapping("/sitemap")
    public ModelAndView mapSite(ModelAndView model) {
        model.setViewName("views/map-site");
        return model;
    }

    //DatingPlaces
    @GetMapping("/terms")
    public ModelAndView terms(ModelAndView model) {
        model.setViewName("info/terms");
        return model;
    }
    @GetMapping("/privacy")
    public ModelAndView privacy(ModelAndView model) {
        model.setViewName("info/privacy");
        return model;
    }
    @GetMapping("/infographic-library")
    public String infographicLibrary(Model model) {
        return "info/infographic-library";
    }

}
