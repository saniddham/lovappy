package com.realroofers.lovappy.web.rest.v2;


import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.lovstamps.dto.LovastampDetailsDto;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampDto;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.system.LanguageService;
import com.realroofers.lovappy.service.user.UserPreferenceService;
import com.realroofers.lovappy.service.user.UserProfileAndPreferenceService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.*;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.FormValidator;
import com.realroofers.lovappy.service.validator.UserRegister;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.exception.BadRequestException;
import com.realroofers.lovappy.web.exception.ResourceNotFoundException;
import com.realroofers.lovappy.web.exception.ValidationException;
import com.realroofers.lovappy.web.rest.dto.UserCompleteProfileDto;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author Eias Altawil
 */

@RestController
@RequestMapping({"/api/v2/user"})
public class RestUserV2Controller {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestUserV2Controller.class);
    private final CloudStorage cloudStorage;
    private final UserService userService;
    private final FormValidator formValidator;
    private final NotificationService notificationService;
    private final EmailTemplateService emailTemplateService;
    private ITemplateEngine templateEngine;
    private final MailService mailService;
    private final UserProfileAndPreferenceService userProfileAndPreferenceService;
    private final UserPreferenceService userPreferenceService;
    private final LovstampService lovstampService;
    private final LanguageService languageService;
    @Value("${lovappy.cloud.bucket.lovdrop}")
    private String lovdropsBucket;
    @Autowired
    public RestUserV2Controller(CloudStorage cloudStorage, UserService userService, FormValidator formValidator, NotificationService notificationService,
                                List<ViewResolver> viewResolvers, EmailTemplateService emailTemplateService, MailService mailService, UserProfileAndPreferenceService userProfileAndPreferenceService, UserPreferenceService userPreferenceService, LovstampService lovstampService, LanguageService languageService) {
        this.cloudStorage = cloudStorage;
        this.userService = userService;
        this.formValidator = formValidator;
        this.notificationService = notificationService;
        this.emailTemplateService = emailTemplateService;
        this.mailService = mailService;
        this.userProfileAndPreferenceService = userProfileAndPreferenceService;
        this.userPreferenceService = userPreferenceService;
        this.lovstampService = lovstampService;
        this.languageService = languageService;

        for (ViewResolver viewResolver : viewResolvers) {
            if (viewResolver instanceof ThymeleafViewResolver) {
                ThymeleafViewResolver thymeleafViewResolver = (ThymeleafViewResolver) viewResolver;
                this.templateEngine = thymeleafViewResolver.getTemplateEngine();
            }
        }
    }



    @RequestMapping(value = "/resend_activation_email", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void resendActivationEmail(@RequestParam(value = "language", defaultValue = "en") String language,
                                      HttpServletRequest request) {
        String key = UUID.randomUUID().toString();
        UserDto user = userService.updateVerifyEmailKey(UserUtil.INSTANCE.getCurrentUserId(), key);

        if (user != null) {
            EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.ACCOUNT_VERIFICATION, language);
            String baseUrl = CommonUtils.getBaseURL(request);
            emailTemplate.getAdditionalInformation().put("url", baseUrl + "/register/verify/" + key);
            new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), user.getEmail(), templateEngine,
                    mailService, emailTemplate).send();
        }

    }


    @RequestMapping(value = "/settings", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateSettings(@RequestBody MyAccountDto myAccount) {

        MyAccountDto myAccountDto = userService.updateMyAccount(UserUtil.INSTANCE.getCurrentUserId(), myAccount);

        if (myAccountDto == null)
            throw new BadRequestException("Error while updating account.");

    }
    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public MyAccountDto getSettings() {

        MyAccountDto myAccountDto = userService.getMyAccount(UserUtil.INSTANCE.getCurrentUserId());

        if (myAccountDto == null)
            throw new ResourceNotFoundException("Error User not found");

        return myAccountDto;
    }
    @RequestMapping(value = "/verification/resend", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void resendVerdication(HttpServletRequest request,
                                  @RequestParam(value = "lang", defaultValue = "en") String language) {

        resendVerdicationByUser(request, language, UserUtil.INSTANCE.getCurrentUserId());
    }


    @GetMapping(value = {"/verification/resend/{userId}"})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void resendVerdicationByUser(HttpServletRequest request,
                                        @RequestParam(value = "lang", defaultValue = "en") String language,
                                        @PathVariable("userId") Integer userId) {

        String key = UUID.randomUUID().toString();
        UserDto newUser = userService.updateVerifyEmailKey(userId, key);
        if (newUser != null) {
            EmailTemplateDto emailTemplate = emailTemplateService
                    .getByNameAndLanguage(EmailTemplateConstants.ACCOUNT_VERIFICATION, language);
            String baseUrl = CommonUtils.getBaseURL(request);
            emailTemplate.getAdditionalInformation().put("url", baseUrl + "/register/verify/" + key);
            new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), newUser.getEmail(), templateEngine,
                    mailService, emailTemplate).send();

        }
    }

    @RequestMapping(value = "/password/reset", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void passwordReset(HttpServletRequest request, @RequestParam(value = "lang", defaultValue = "en") String language,
                              @RequestParam(value="email") String email) {

        EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.RESET_PASSWORD, language);
        String baseUrl = CommonUtils.getBaseURL(request);
        emailTemplate.getAdditionalInformation().put("baseUrl", baseUrl);
        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), email, templateEngine,
                mailService, emailTemplate).send();

    }
    @RequestMapping(value = "/profile/voice/upload", method = RequestMethod.POST)
    public ResponseEntity<LovastampDetailsDto> uploadRecord(@RequestParam("data") MultipartFile uploadfile,
                                                            @RequestParam("seconds") Integer seconds,
                                                            @RequestParam("language") String  languagecode) {
        try {
            CloudStorageFileDto cloudStorageFileDto =
                    cloudStorage.uploadFile(uploadfile, lovdropsBucket, FileExtension.mp3.toString(), "audio/mp3");

            if (cloudStorageFileDto != null) {
                LovstampDto lovstamp = new LovstampDto();
                lovstamp.setUser(new UserDto(UserUtil.INSTANCE.getCurrentUserId()));
                lovstamp.setAudioFileCloud(cloudStorageFileDto);
                lovstamp.setRecordSeconds(seconds);
                lovstamp.setLanguage(languageService.getLanguageByCode(languagecode));


                LovastampDetailsDto out = lovstampService.save(lovstamp, false) ;
                return ResponseEntity.ok().body(out);
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
    @RequestMapping(value = "/profile/voice", method = RequestMethod.GET)
    public LovastampDetailsDto getVoice() {
        return lovstampService.getVoiceByUserID(UserUtil.INSTANCE.getCurrentUserId());
    }
    @RequestMapping(value = "/preferences", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updatePrefGenderAndPrefrences(@RequestBody UpdateProfileRequest completeProfileRequest) {
        userProfileAndPreferenceService.updateProfile(UserUtil.INSTANCE.getCurrentUserId(), completeProfileRequest);
    }

    @RequestMapping(value = "/profile/complete", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void completeProfile(@RequestBody UserCompleteProfileDto completeProfileRequest) {

        Integer userID = UserUtil.INSTANCE.getCurrentUser().getUserId();
        GenderAndPrefGenderDto profile = userProfileAndPreferenceService.getGenderAndPrefGender(userID);

        AgeHeightLangAndPrefSizeAgeHeightLangDto dto = new AgeHeightLangAndPrefSizeAgeHeightLangDto(completeProfileRequest.getHeightFt(), completeProfileRequest.getHeightIn(),
                completeProfileRequest.getHeightCm(), completeProfileRequest.getPreference().getPenisSizeMatter(), completeProfileRequest.getPreference().getBustSize(),
                completeProfileRequest.getPreference().getButtSize(), completeProfileRequest.getPreference().getMinAge(), completeProfileRequest.getPreference().getMaxAge(), completeProfileRequest.getPreference().getHeight(),
                profile.getPrefGender());

        List<ErrorMessage> errorMessages = formValidator.validate(dto, new Class[] { UserRegister.class });



        if (errorMessages.size() > 0)
            throw new ValidationException(errorMessages);

        Map<String, String> lifestyles = new HashMap<>();
        lifestyles.put("catORdog", completeProfileRequest.getLifestyles().getCatORdog());

        if (completeProfileRequest.getLifestyles().getNohabit() != null)
            lifestyles.put("no-habit", "no-habit");
        else {
            if (completeProfileRequest.getLifestyles().getDrinker() != null)
                lifestyles.put("drinker", "drinker");
            if (completeProfileRequest.getLifestyles().getSmoker() != null)
                lifestyles.put("smoker", "smoker");
            if (completeProfileRequest.getLifestyles().getUser() != null)
                lifestyles.put("user", "user");
        }

        lifestyles.put("ruralORurbanORsuburban", completeProfileRequest.getLifestyles().getRuralORurbanORsuburban());
        lifestyles.put("politicalopinion", completeProfileRequest.getLifestyles().getPoliticalopinion());

        Map<String, String> personalities = new HashMap<>();
        personalities.put("outdoorsyORindoorsy", completeProfileRequest.getPersonalities().getOutdoorsyORindoorsy());
        personalities.put("frugalORbigspender", completeProfileRequest.getPersonalities().getFrugalORbigspender());
        personalities.put("morningpersonORnightowl", completeProfileRequest.getPersonalities().getMorningpersonORnightowl());
        personalities.put("extrovertORintrovert", completeProfileRequest.getPersonalities().getExtrovertORintrovert());

        PersonalityLifestyleDto personalityLifestyleDto = new PersonalityLifestyleDto();
        personalityLifestyleDto.setLifestyles(lifestyles);
        personalityLifestyleDto.setPersonalities(personalities);

        List<ErrorMessage> personalityErrorMessages = formValidator.validate(personalityLifestyleDto, new Class[] { UserRegister.class });

        if (completeProfileRequest.getLifestyles().getCatORdog() == null)
            personalityErrorMessages.add(new ErrorMessage("catORdog", "What do you prefer?"));

        if(!StringUtils.isEmpty(completeProfileRequest.getLifestyles().getNohabit()) && (!StringUtils.isEmpty(completeProfileRequest.getLifestyles().getSmoker()) ||
                !StringUtils.isEmpty(completeProfileRequest.getLifestyles().getDrinker() ) || !StringUtils.isEmpty(completeProfileRequest.getLifestyles().getUser() ))) {
            personalityErrorMessages.add(new ErrorMessage("habit", "No Habit can't be chosen with other options"));
        }
        if (lifestyles.get("drinker") == null && lifestyles.get("smoker") == null && lifestyles.get("user") == null
                && lifestyles.get("no-habit") == null)
            personalityErrorMessages.add(new ErrorMessage("habit", "What is your habit?"));

        if (lifestyles.get("ruralORurbanORsuburban") == null)
            personalityErrorMessages.add(new ErrorMessage("ruralORurbanORsuburban", "What is your ideal neighborhood?"));

        if (lifestyles.get("politicalopinion") == null)
            personalityErrorMessages.add(new ErrorMessage("politicalopinion", "What is your political opinion?"));

        if (personalities.get("outdoorsyORindoorsy") == null)
            personalityErrorMessages.add(new ErrorMessage("outdoorsyORindoorsy", "Are you outdoorsy or indoorsy?"));

        if (personalities.get("frugalORbigspender") == null)
            personalityErrorMessages.add(new ErrorMessage("frugalORbigspender", "Are you frugal or big spender?"));

        if (personalities.get("morningpersonORnightowl") == null)
            personalityErrorMessages.add(new ErrorMessage("morningpersonORnightowl", "Are you morning person or night owl?"));

        if (personalities.get("extrovertORintrovert") == null)
            personalityErrorMessages.add(new ErrorMessage("extrovertORintrovert", "Are you extrovert or introvert?"));

        errorMessages.addAll(personalityErrorMessages);

        Map<String, String> statuses = new HashMap<>();
        statuses.put("employed", completeProfileRequest.getStatuses().getEmployed());
        statuses.put("risktaker", completeProfileRequest.getStatuses().getRisktaker());
        statuses.put("believeingod", completeProfileRequest.getStatuses().getBelieveingod());
        statuses.put("wantkids", completeProfileRequest.getStatuses().getWantkids());
        statuses.put("havekids", completeProfileRequest.getStatuses().getHavekids());
        statuses.put("attractmany", completeProfileRequest.getStatuses().getAttractmany());

        StatusLangDto statusLangDto = new StatusLangDto();
        statusLangDto.setStatuses(statuses);


        List<ErrorMessage> statusesErrorMessages = formValidator.validate(statusLangDto, new Class[] { UserRegister.class });

        if (statusLangDto.getStatuses() == null || statusLangDto.getStatuses().get("employed") == null)
            statusesErrorMessages.add(new ErrorMessage("employed", "Are you currently employed?"));

        if (statusLangDto.getStatuses() == null || statusLangDto.getStatuses().get("risktaker") == null)
            statusesErrorMessages.add(new ErrorMessage("risktaker", "Are you a risk taker?"));

        if (statusLangDto.getStatuses() == null || statusLangDto.getStatuses().get("believeingod") == null)
            statusesErrorMessages.add(new ErrorMessage("believeingod", "Are you a religious or spiritual?"));

        if (statusLangDto.getStatuses() == null || statusLangDto.getStatuses().get("wantkids") == null)
            statusesErrorMessages.add(new ErrorMessage("wantkids", "Do you want kids?"));

        if (statusLangDto.getStatuses() == null || statusLangDto.getStatuses().get("havekids") == null)
            statusesErrorMessages.add(new ErrorMessage("havekids", "Do you have kids?"));

        if (statusLangDto.getStatuses() == null || statusLangDto.getStatuses().get("attractmany") == null)
            statusesErrorMessages.add(new ErrorMessage("attractmany", "I attract many people?"));


        errorMessages.addAll(statusesErrorMessages);

        if (errorMessages.size() > 0)
            throw new ValidationException("Validation Error", errorMessages);


        userProfileAndPreferenceService.saveOrUpdate(UserUtil.INSTANCE.getCurrentUserId(), statuses, completeProfileRequest.getSeekingLanguages(), completeProfileRequest.getSpeakinglanguages(),
                dto, personalityLifestyleDto);

    }
    @RequestMapping(value = "/location/save", method = RequestMethod.POST)
    public ResponseEntity<?> postPage1(@RequestBody WithinRadiusDto searchDto) {

        LOGGER.info("Daoud latitude {}, long {}, rad {}", searchDto.getLatitude(), searchDto.getLongitude(), searchDto.getRadius());
        AddressDto addressDto = userProfileAndPreferenceService.saveLocation(UserUtil.INSTANCE.getCurrentUser().getUserId(), new AddressDto(searchDto.getLatitude(), searchDto.getLongitude(), searchDto.getRadius()), searchDto.getRadius());

        return ResponseEntity.ok(addressDto);
    }

}