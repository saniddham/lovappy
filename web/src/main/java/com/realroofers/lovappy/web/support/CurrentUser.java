package com.realroofers.lovappy.web.support;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
//@Scope("session")
public class CurrentUser implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7868770838329394604L;

	private String sessionId;
	
	private String userId;
	
	private boolean showBlogPostLink;
	
	private boolean allowBlogPost;
	
	private boolean alreadyRequestedBlog;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public boolean isShowBlogPostLink() {
		return showBlogPostLink;
	}

	public void setShowBlogPostLink(boolean showBlogPostLink) {
		this.showBlogPostLink = showBlogPostLink;
	}

	public boolean isAllowBlogPost() {
		return allowBlogPost;
	}

	public void setAllowBlogPost(boolean allowBlogPost) {
		this.allowBlogPost = allowBlogPost;
	}

	public boolean isAlreadyRequestedBlog() {
		return alreadyRequestedBlog;
	}

	public void setAlreadyRequestedBlog(boolean alreadyRequestedBlog) {
		this.alreadyRequestedBlog = alreadyRequestedBlog;
	}
	

}
