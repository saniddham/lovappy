package com.realroofers.lovappy.web.config.social;

import com.realroofers.lovappy.service.user.SocialProfileService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.dto.SocialProfileDto;
import com.realroofers.lovappy.service.user.dto.UserAuthDto;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.web.config.security.UserAuthorizationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Slf4j
@Service
public class SocialConnectionSignup implements ConnectionSignUp {
 
    @Autowired
    private UserAuthorizationService userAuthorizationService;
    @Autowired
    private UserService userService;
    @Autowired
    private SocialProfileService socialProfileService;
    @Override
    public String execute(Connection<?> connection) {
        log.debug("before singin");
        UserAuthDto user = userAuthorizationService.socialLogin(connection, connection.getKey().getProviderId(), null);
        return user.getEmail();
    }
}