package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.support.FileApprovalStatus;
import com.realroofers.lovappy.service.cloud.support.FileApprovalValue;
import com.realroofers.lovappy.service.cloud.support.PhotoNames;
import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.survey.UserSurveyService;
import com.realroofers.lovappy.service.user.UserRolesService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.*;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import com.realroofers.lovappy.web.util.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Eias Altawil on 5/21/17
 */

@Controller
@RequestMapping("/lox/users")
public class UserManagementController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserManagementController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private LovstampService lovstampService;
    @Autowired
    private MailService mailService;
    @Autowired
    private ITemplateEngine templateEngine;

    @Autowired
    private UserSurveyService userSurveyService;
    @Autowired
    private  EmailTemplateService emailTemplateService;
    @Autowired
    private  CloudStorage cloudStorage;
    @Autowired
    private  CloudStorageService cloudStorageService;
    @Autowired
    private UserRolesService userRolesService;

    @Value("${google.maps.api.key}")
    private String googleKeyId;

    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 10;
    private static final int[] PAGE_SIZES = { 5, 10, 20 };

    @GetMapping()
    public ModelAndView manageUsers(@RequestParam("pageSize") Optional<Integer> pageSize,
                                    @RequestParam("page") Optional<Integer> page,
                                    @RequestParam(value = "searchText", defaultValue = "", required = false) String searchText,
                                    @RequestParam(value = "filter", defaultValue = "") String filter,
                                    @RequestParam(value = "role", defaultValue = "ALL") String role,
                                    @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){
        ModelAndView modelAndView = new ModelAndView("admin/user/manage_users");

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        PageRequest pageable = new PageRequest(evalPage, evalPageSize);


        if (!StringUtils.isEmpty(filter)) {
            if (pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(evalPage, evalPageSize, new Sort(sort, filter));
            }
        }

        modelAndView.addObject("filter", filter);
        modelAndView.addObject("role", role);
        modelAndView.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);

        modelAndView.addObject("google_key", googleKeyId);
        Page<UserDto> users = userService.getAllByKeyword(role, searchText, pageable);
        Pager pager = new Pager(users.getTotalPages(), users.getNumber(), BUTTONS_TO_SHOW);

        modelAndView.addObject("users", users);

        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pageSizes", PAGE_SIZES);
        modelAndView.addObject("pager", pager);

        return modelAndView;
    }


    @GetMapping("/artists")
    public ModelAndView manageMusicians(@RequestParam("pageSize") Optional<Integer> pageSize,
                                    @RequestParam("page") Optional<Integer> page,
                                    @RequestParam(value = "searchText", defaultValue = "", required = false) String searchText,
                                    @RequestParam(value = "filter", defaultValue = "") String filter,
                                    @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){
        ModelAndView modelAndView = new ModelAndView("admin/music/manage_artists");

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        PageRequest pageable = new PageRequest(evalPage, evalPageSize);


        if (!StringUtils.isEmpty(filter)) {
            if (pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(evalPage, evalPageSize, new Sort(sort, filter));
            }
        }

        modelAndView.addObject("filter", filter);
        modelAndView.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);

        modelAndView.addObject("google_key", googleKeyId);
        Page<UserDto> users = userService.getMusicianByKeyword(searchText, pageable);
        Pager pager = new Pager(users.getTotalPages(), users.getNumber(), BUTTONS_TO_SHOW);

        modelAndView.addObject("artists", users);

        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pageSizes", PAGE_SIZES);
        modelAndView.addObject("pager", pager);

        return modelAndView;
    }


    @GetMapping("/vendors")
    public ModelAndView manageVendors(@RequestParam("pageSize") Optional<Integer> pageSize,
                                    @RequestParam("page") Optional<Integer> page,
                                    @RequestParam(value = "searchText", defaultValue = "", required = false) String searchText,
                                    @RequestParam(value = "filter", defaultValue = "") String filter,
                                    @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){
        ModelAndView modelAndView = new ModelAndView("admin/vendor/manage_vendors");

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        PageRequest pageable = new PageRequest(evalPage, evalPageSize);


        if (!StringUtils.isEmpty(filter)) {
            if (pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(evalPage, evalPageSize, new Sort(sort, filter));
            }
        }

        modelAndView.addObject("filter", filter);
        modelAndView.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);

        modelAndView.addObject("google_key", googleKeyId);
        Page<UserDto> users = userService.getVendorByKeyword(searchText, pageable);
        Pager pager = new Pager(users.getTotalPages(), users.getNumber(), BUTTONS_TO_SHOW);

        modelAndView.addObject("artists", users);

        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pageSizes", PAGE_SIZES);
        modelAndView.addObject("pager", pager);

        return modelAndView;
    }

    //review
    @GetMapping("/review")
    public ModelAndView reviewUsers(@RequestParam("pageSize") Optional<Integer> pageSize,
                                    @RequestParam("page") Optional<Integer> page,
                                    @RequestParam(value = "searchText", defaultValue = "", required = false) String searchText,
                                    @RequestParam(value = "filter", defaultValue = "user.userId") String filter,
                                    @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){
        ModelAndView modelAndView = new ModelAndView("admin/user/review_users");

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        PageRequest pageable = new PageRequest(evalPage, evalPageSize);


        if (!StringUtils.isEmpty(filter)) {
            if (pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(evalPage, evalPageSize, new Sort(sort, filter));
            }
        }

        modelAndView.addObject("filter", filter);
        modelAndView.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);

        modelAndView.addObject("google_key", googleKeyId);
        //Page<UserDto> users = userService.getAllByKeyword(searchText, pageable);
        modelAndView.addObject("searchText", searchText);

       Page<UserReviewDto> users = userService.findAllUsersWithLovdrop(searchText, pageable);
        Pager pager = new Pager(users.getTotalPages(), users.getNumber(), BUTTONS_TO_SHOW);
        modelAndView.addObject("users", users);

        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pageSizes", PAGE_SIZES);
        modelAndView.addObject("pager", pager);

        return modelAndView;
    }

    @GetMapping("/{id}")
    public ModelAndView usersDetails(ModelAndView model, @PathVariable(value = "id") Integer id){
        model.addObject("user",  userService.getUser(id));
        model.addObject("lovdrop", lovstampService.getLovstampByUserID(id));
        model.addObject("survey", userSurveyService.getUserSurvey(id));
        model.setViewName("admin/user/user_details");
        return model;
    }

    @GetMapping("/lastlogin")
    public ModelAndView lastLogins(ModelAndView modelAndView,
                                   @RequestParam("pageSize") Optional<Integer> pageSize,
                                   @RequestParam("page") Optional<Integer> pageNumber,
                                   @RequestParam(value = "searchText", defaultValue = "", required = false) String searchText,
                                   @RequestParam(value = "filter", defaultValue = "") String filter,
                                   @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){

        PageRequest pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize));

        if(!StringUtils.isEmpty(filter)) {
            if(pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize), new Sort(sort, filter));
            }
        }
        modelAndView.addObject("filter", filter);
        modelAndView.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);

        modelAndView.addObject("searchText", searchText);

//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) -1);
//        Page<UserDto> users = userService.getLastLoginUsers(searchText, pageable ,calendar.getTime(), new Date());

        Page<UserDto> users = userService.getAllByKeyword("ALL", searchText, pageable);
        
        
        Pager pager = new Pager(users.getTotalPages(), users.getNumber(), BUTTONS_TO_SHOW);

        modelAndView.addObject("users", users);

        modelAndView.addObject("selectedPageSize", Pager.evalPageSize(pageSize));
        modelAndView.addObject("pageSizes", PAGE_SIZES);
        modelAndView.addObject("pager", pager);

        modelAndView.setViewName("admin/user/last_login_users");
        return modelAndView;
    }


    @GetMapping("/purchases")
    public ModelAndView purchases(ModelAndView model ){
        //model.addObject("users", userService.getUserByRole(Roles.USER.getValue(),1 ,0));
        model.setViewName("admin/user/purchases_users");
        return model;
    }

    @GetMapping("/complaints")
    public ModelAndView complaints(ModelAndView modelAndView,
                                   @RequestParam("pageSize") Optional<Integer> pageSize,
                                   @RequestParam("page") Optional<Integer> pageNumber,
                                   @RequestParam(value = "searchText", defaultValue = "", required = false) String searchText,
                                   @RequestParam(value = "filter", defaultValue = "") String filter,
                                   @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){

        PageRequest pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize));

        if(!StringUtils.isEmpty(filter)) {
            if(pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize), new Sort(sort, filter));
            }
        }
        modelAndView.addObject("filter", filter);
        modelAndView.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);
        modelAndView.addObject("searchText", searchText);

        Page<UserFlagDto> users = userService.getUserComplaints(searchText, pageable);
        Pager pager = new Pager(users.getTotalPages(), users.getNumber(), BUTTONS_TO_SHOW);

        modelAndView.addObject("users", users);

        modelAndView.addObject("selectedPageSize", Pager.evalPageSize(pageSize));
        modelAndView.addObject("pageSizes", PAGE_SIZES);
        modelAndView.addObject("pager", pager);
        modelAndView.setViewName("admin/user/complaints_users");
        return modelAndView;
    }

    @PostMapping("/save_roles")
    public ResponseEntity saveRoles(@RequestParam("user_id") Integer userID,
                                    @RequestParam("author") Boolean author,
                                    @RequestParam("event") Boolean event,
                                    HttpServletRequest request){

        if(author) {
            userService.assignRole(Roles.AUTHOR.getValue(), userID);
        } else {
            userService.unassignRole(Roles.AUTHOR.getValue(), userID);
        }

        UserDto ambassador = userService.getUser(userID);
        if(event) {
            if(userService.approveAmbassador(userID)) {
                //send approval email
                EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.AMBASSADOR_APPROVAL, "EN");
                emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/events");
                new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), ambassador.getEmail(), templateEngine,
                        mailService, emailTemplate).send();
            }

        } else {
            if(userService.unassignRole(Roles.EVENT_AMBASSADOR.getValue(), userID)) {
                //send decline email
                EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.AMBASSADOR_DENIAL, "EN");
                new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), ambassador.getEmail(), templateEngine,
                        mailService, emailTemplate).send();
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body("");
    }



    @DeleteMapping("/{user-id}/ban")
    @ResponseBody
    public ResponseEntity banUser(ModelAndView modelAndView, @PathVariable("user-id") Integer userId){

        userService.setBanUserStatus(userId, true);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/{user-id}/pause")
    @ResponseBody
    public ResponseEntity pauseUser(ModelAndView modelAndView, @PathVariable("user-id") Integer userId){

        userService.setPauseUserStatus(userId, true);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/{user-id}/ban")
    @ResponseBody
    public ResponseEntity unBanUser(ModelAndView modelAndView, @PathVariable("user-id") Integer userId){

        userService.setBanUserStatus(userId, false);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/{user-id}/pause")
    @ResponseBody
    public ResponseEntity unPauseUser(ModelAndView modelAndView, @PathVariable("user-id") Integer userId){

        userService.setPauseUserStatus(userId, false);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/requests")
    public ModelAndView manageUsersRoles(@RequestParam("pageSize") Optional<Integer> pageSize,
                                    @RequestParam("page") Optional<Integer> page,
                                    @RequestParam(value = "searchText", defaultValue = "", required = false) String searchText,
                                    @RequestParam(value = "filter", defaultValue = "created") String filter,
                                    @RequestParam(value = "sort", defaultValue = "DESC") Sort.Direction sort){
        ModelAndView modelAndView = new ModelAndView("admin/user/users_requests");

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        PageRequest pageable = new PageRequest(evalPage, evalPageSize);


        if (!StringUtils.isEmpty(filter)) {
            if (pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(evalPage, evalPageSize, new Sort(sort, filter));
            }
        }

        modelAndView.addObject("filter", filter);
        modelAndView.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);

        modelAndView.addObject("google_key", googleKeyId);
        Page<UserRolesDto> users = userRolesService.getUserRequestsByEmail(searchText, pageable);
        Pager pager = new Pager(users.getTotalPages(), users.getNumber(), BUTTONS_TO_SHOW);

        modelAndView.addObject("users", users);

        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pageSizes", PAGE_SIZES);
        modelAndView.addObject("pager", pager);

        return modelAndView;
    }
    @PostMapping("/{userId}/role/{roleId}")
    @ResponseBody
    public boolean approveRole(HttpServletRequest request, @PathVariable("userId") Integer userId,
                                         @PathVariable("roleId") Integer roleId){


       UserRoleResponse roleResponse = userRolesService.approveRoleAndGeneratePassword(userId, roleId);
       if(roleResponse.getApproved()) {
           //send approval email
           EmailTemplateDto emailTemplate = null;

           switch (roleResponse.getRole()) {
               case ADMIN:
                   break;
               case AUTHOR:
                   break;
               case MUSICIAN:
                   break;
               case EVENT_AMBASSADOR:
                   emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.AMBASSADOR_APPROVAL, "EN");
                   emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/events");
                   break;
               case DEVELOPER:
                   emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.DEVELOPER_APPROVAL, "EN");
                   emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/developer/login");
                   emailTemplate.getAdditionalInformation().put("password", roleResponse.getPasspharse());
                   emailTemplate.getAdditionalInformation().put("email", roleResponse.getEmail());
                   break;
           }

           if(emailTemplate != null)
           new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), roleResponse.getEmail(), templateEngine,
                   mailService, emailTemplate).send();

           return true;
       }
        return false;
}


    @DeleteMapping("/{userId}/role/{roleId}")
    @ResponseBody
    public boolean rejectRole(HttpServletRequest request,
                           @PathVariable("userId") Integer userId,
                            @PathVariable("roleId") Integer roleId){

        UserRoleResponse roleResponse =  userRolesService.rejectRole(userId, roleId);
        if(roleResponse.getApproved()) {
            //send approval email
            EmailTemplateDto emailTemplate = null;

            switch (roleResponse.getRole()) {
                case ADMIN:
                    break;
                case AUTHOR:
                    break;
                case MUSICIAN:
                    break;
                case EVENT_AMBASSADOR:
                    emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.AMBASSADOR_DENIAL, "EN");
                    break;
                case DEVELOPER:
                    emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.DEVELOPER_DENIAL, "EN");
                    break;
            }
            if(emailTemplate != null)
            new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), roleResponse.getEmail(), templateEngine,
                    mailService, emailTemplate).send();

            return true;
        }
        return false;
    }


    @RequestMapping(value = "/updateapproval", method = RequestMethod.POST)
    public ResponseEntity<Void> userApproval(@RequestBody Map<String, String> request, HttpServletRequest httpServletRequest) {
        Integer userId = new Integer(request.get("userId").trim());
        if (request.get("profilephoto"+userId) != null && request.get("profilephoto"+userId).equals("1")) {
            cloudStorageService.approveUserFile(userId, UserUtil.INSTANCE.getCurrentUserId(),
                    FileApprovalStatus.APPROVEPROFILEPHOTO, FileApprovalValue.APPROVE);
            sendMissingPhotoMail(httpServletRequest, userId, FileApprovalStatus.APPROVEPROFILEPHOTO,  PhotoNames.PROFILEPHOTO);
        } else if ( request.get("profilephoto"+userId) != null && request.get("profilephoto"+userId).equals("0")) {
            cloudStorageService.approveUserFile(userId, UserUtil.INSTANCE.getCurrentUserId(),
                    FileApprovalStatus.REJECTPROFILEPHOTO, FileApprovalValue.REJECT);
            sendUserMail(httpServletRequest, userService.getUserById(userId).getEmail(), PhotoNames.PROFILEPHOTO, EmailTemplateConstants.PHOTO_REJECTED);
        }

      if (request.get("profileaudio"+userId) != null && request.get("profileaudio"+userId).equals("0")) {
            cloudStorageService.approveUserFile(userId, UserUtil.INSTANCE.getCurrentUserId(),
                    FileApprovalStatus.REJECTPROFILEAUDIO, FileApprovalValue.REJECT);
            sendVoiceUserMail(userService.getUserById(userId).getEmail(), PhotoNames.PROFILEAUDIO, EmailTemplateConstants.VOICE_REJECTED);
        }else if ( request.get("profileaudio"+userId) != null && request.get("profileaudio"+userId).equals("1")) {
          cloudStorageService.approveUserFile(userId, UserUtil.INSTANCE.getCurrentUserId(),
                  FileApprovalStatus.APPROVEPROFILEAUDIO, FileApprovalValue.APPROVE);
      }

        if (request.get("handsphoto"+userId) != null && request.get("handsphoto"+userId).equals("1")) {
            cloudStorageService.approveUserFile(userId, UserUtil.INSTANCE.getCurrentUserId(),
                    FileApprovalStatus.APPROVEHANDSPHOTO, FileApprovalValue.APPROVE);
            sendMissingPhotoMail(httpServletRequest, userId, FileApprovalStatus.APPROVEHANDSPHOTO,  PhotoNames.HANDSPHOTO);
        } else if (request.get("handsphoto"+userId) != null && request.get("handsphoto"+userId).equals("0")) {
            cloudStorageService.approveUserFile(userId, UserUtil.INSTANCE.getCurrentUserId(),
                    FileApprovalStatus.REJECTHANDSPHOTO, FileApprovalValue.REJECT);
            sendUserMail(httpServletRequest, userService.getUserById(userId).getEmail(), PhotoNames.HANDSPHOTO, EmailTemplateConstants.PHOTO_REJECTED);
        }

        if (request.get("feetphoto"+userId) != null && request.get("feetphoto"+userId).equals("1")) {
            cloudStorageService.approveUserFile(userId, UserUtil.INSTANCE.getCurrentUserId(),
                    FileApprovalStatus.APPROVEFEETPHOTO, FileApprovalValue.APPROVE);
            sendMissingPhotoMail(httpServletRequest, userId, FileApprovalStatus.APPROVEFEETPHOTO,  PhotoNames.FEETPHOTO);
        } else if (request.get("feetphoto"+userId) != null && request.get("feetphoto"+userId).equals("0")) {
            cloudStorageService.approveUserFile(userId, UserUtil.INSTANCE.getCurrentUserId(),
                    FileApprovalStatus.REJECTFEETPHOTO, FileApprovalValue.REJECT);
            sendUserMail(httpServletRequest, userService.getUserById(userId).getEmail(), PhotoNames.FEETPHOTO, EmailTemplateConstants.PHOTO_REJECTED);
        }

        if (request.get("legsphoto"+userId) != null && request.get("legsphoto"+userId).equals("1")) {
            cloudStorageService.approveUserFile(userId, UserUtil.INSTANCE.getCurrentUserId(),
                    FileApprovalStatus.APPROVELEGSPHOTO, FileApprovalValue.APPROVE);
            sendMissingPhotoMail(httpServletRequest, userId, FileApprovalStatus.APPROVELEGSPHOTO,  PhotoNames.LEGSPHOTO);
        } else if (request.get("legsphoto"+userId) != null &&  request.get("legsphoto"+userId).equals("0")) {
            cloudStorageService.approveUserFile(userId, UserUtil.INSTANCE.getCurrentUserId(),
                    FileApprovalStatus.REJECTLEGSPHOTO, FileApprovalValue.REJECT);
            sendUserMail(httpServletRequest, userService.getUserById(userId).getEmail(), PhotoNames.LEGSPHOTO, EmailTemplateConstants.PHOTO_REJECTED);
        }

        return ResponseEntity.ok().build();
    }

    private void sendMissingPhotoMail(HttpServletRequest request, Integer userId, FileApprovalStatus photoApprovalStatus, PhotoNames profilephoto) {
        //If photoApprovalStatus is missing then send mail
        if (!cloudStorageService.profileFileProvided(userId, photoApprovalStatus)){
            sendUserMail(request, userService.getUserById(userId).getEmail(), profilephoto, EmailTemplateConstants.PHOTO_MISSING);
        }
    }

    private void sendUserMail(HttpServletRequest request, String userEmail, PhotoNames photoNames, String emailTemplateConstant) {
        EmailTemplateDto emailTemplate =
                emailTemplateService.getByNameAndLanguage(emailTemplateConstant, "EN");
        emailTemplate.getVariables().put("##PHOTO##", photoNames.getValue());
        new EmailTemplateFactory()
                .build(CommonUtils.getBaseURL(request), userEmail, templateEngine, mailService, emailTemplate)
                .send();
    }

    private void sendVoiceUserMail(String userEmail, PhotoNames photoNames, String emailTemplateConstant) {
        EmailTemplateDto emailTemplate =
                emailTemplateService.getByNameAndLanguage(emailTemplateConstant, "EN");
        emailTemplate.getVariables().put("##VOICE##", photoNames.getValue());
        new EmailTemplateFactory()
                .build(null, userEmail, templateEngine, mailService, emailTemplate)
                .send();
    }

}
