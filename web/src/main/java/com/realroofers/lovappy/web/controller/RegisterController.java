package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.UserAuthDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.validator.UserRegister;
import com.realroofers.lovappy.service.validation.ResponseStatus;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.FormValidator;
import com.realroofers.lovappy.service.validation.JsonResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.social.twitter.api.TweetData;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/register")
public class RegisterController {

    private static Logger LOG = LoggerFactory.getLogger(RegisterController.class);

    private UserService userService;
    private DaoAuthenticationProvider authenticationProvider;
    private FormValidator formValidator;
    private NotificationService notificationService;

    private final TwitterTemplate twitterTemplate;
    @Value("${google.maps.api.key}")
    private String googleApiKey;

    @Autowired
    public RegisterController(NotificationService notificationService, UserService userService, DaoAuthenticationProvider authenticationProvider,
                              FormValidator formValidator, TwitterTemplate twitterTemplate) {
        this.userService = userService;
        this.authenticationProvider = authenticationProvider;
        this.formValidator = formValidator;
        this.notificationService = notificationService;
        this.twitterTemplate = twitterTemplate;
    }

    @PostMapping
    public ResponseEntity registerNewUser(@ModelAttribute("user") UserDto user, HttpServletRequest request) {
        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = formValidator.validate(user, new Class[]{UserRegister.class});

        if (!user.getPassword().equals(user.getConfirmPassword()))
            errorMessages.add(new ErrorMessage("confirmPassword", "Password doesn't match"));

        if (userService.emailExists(user.getEmail()))
            errorMessages.add(new ErrorMessage("email", "You are already registered"));

        if (errorMessages.size() > 0) {
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(HttpStatus.OK).body(res);
        } else {
            User newUser = userService.registerNewUser(user);
            NotificationDto notificationDto = new NotificationDto();
            notificationDto.setContent(newUser.getEmail() + " is a new member in lovappy ");
            notificationDto.setType(NotificationTypes.USER_REGISTRATION);
            notificationDto.setUserId(newUser.getUserId());
            notificationService.sendNotificationToAdmins(notificationDto);
            if(newUser != null) {
                authenticateUserAndSetSession(user, request);

                res.setStatus(ResponseStatus.SUCCESS);
                res.setResult(user.getIsInLove() ? "yes_love" : "no_love");
                return ResponseEntity.ok().body(res);
            }
        }

        res.setStatus(ResponseStatus.FAIL);
        return ResponseEntity.ok().body(res);
    }

    @GetMapping("/vendor")
    public String vendor(){
        return "registration/registration-vendor";
    }
    @GetMapping("/brand")
    public String brand(){
        return "registration/registration-brand";
    }

    @GetMapping("/verify/{key}")
    public String verifyEmail(@PathVariable("key") String key, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        UserDto userDto = userService.verifyEmail(key);
        if (userDto != null) {
            redirectAttributes.addFlashAttribute("popup_msg", "activated");
            redirectAttributes.addFlashAttribute("popup_msg_title", "Account Activated");
            redirectAttributes.addFlashAttribute("popup_msg_text", "Your account has been activated!");

            if(!userDto.getTweeted()) {
                //Tweet new status on lovappy account about new registered user
                try {
                    Integer numberOfMales = userService.countUsersByGender(Gender.MALE);
                    Integer numberOfFemales = userService.countUsersByGender(Gender.FEMALE);
                    String address = userDto.getUserProfile().getAddress() != null ? "from #" + userDto.getUserProfile().getAddress().getFullAddress() : "";
                    TweetData tweetData = new TweetData(String.format("%d members males %d female %d new member %s", (numberOfMales + numberOfFemales), numberOfMales, numberOfFemales, address));
                    twitterTemplate.timelineOperations().updateStatus(tweetData);
                    userService.setTweeted(true, userDto.getID());

                } catch (Exception ex) {
                LOG.error("{}", ex);
                }
            }
            if(UserUtil.INSTANCE.getCurrentUserId() != null){
                UserAuthDto userAuthDto = UserUtil.INSTANCE.getCurrentUser();
                userAuthDto.setVerified(true);
                UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userAuthDto, userAuthDto.getPassword(), userAuthDto.getAuthorities());

                token.setDetails( userAuthDto);

                SecurityContextHolder.getContext().setAuthentication(token);
                // add authentication to the session
                request.getSession().setAttribute(
                        HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
                        SecurityContextHolder.getContext());
                if(userDto.getRoles().stream().anyMatch(r->r.getName().equals(Roles.COUPLE.getValue())))
                return "redirect:/couples/dashboard";
                return "redirect:/radio";
            }
            return "redirect:/login";
        } else {
            redirectAttributes.addFlashAttribute("popup_msg", "invalid_key");
            redirectAttributes.addFlashAttribute("popup_msg_title", "Invalid Activation Link");
            redirectAttributes.addFlashAttribute("popup_msg_text", "Activation link is invalid or the account is already activated.");
            return "redirect:/";
        }
    }

    @GetMapping("/how-this-work")
    public String profileHowThisWork(Model model) {

        return "profile-how-this-work";
    }

    @PostMapping ("/social")
    @ResponseBody
    public void socialCoupleRegisteration(HttpServletRequest request){
       request.getSession(true).setAttribute("INLOVE", true);
    }


    //Login the user after register
    private void authenticateUserAndSetSession(UserDto user, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword());

        // generate session if one doesn't exist
        request.getSession();

        token.setDetails(new WebAuthenticationDetails(request));
        Authentication authenticatedUser = authenticationProvider.authenticate(token);

        SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
    }
}
