package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.news.*;
import com.realroofers.lovappy.service.news.dto.NewsStoryDto;
import com.realroofers.lovappy.service.news.model.NewsCategory;
import com.realroofers.lovappy.web.controller.news.NewsSubmitController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Daoud Shaheen on 7/3/2018.
 */
@Controller
@RequestMapping("/voice-dating")
public class NewsController {

    private static Logger LOG = LoggerFactory.getLogger(NewsSubmitController.class);

    @Autowired
    NewsCategoryService newsCategoryService;


    @GetMapping("/affiliates")
    public ModelAndView affiliates(ModelAndView model) {
        model.setViewName("registration/affiliate-registration");
        return model;
    }

    @GetMapping("/press")
    public ModelAndView press(ModelAndView model) {
        model.setViewName("news/press");
        return model;
    }
    @GetMapping("/newsroom")
    public ModelAndView newsroom(ModelAndView model) {
        model.setViewName("news/newsroom");
        return model;
    }

    @GetMapping("/submit-a-news-story")
    public String initialNewsSubmitPage(Model model) {
        List<NewsCategory> newsCategories = newsCategoryService.getAllNewsCategory();
        model.addAttribute("newsCategories", newsCategories);
        model.addAttribute("newsStoryDto", new NewsStoryDto());
        return "news/submit";
    }
}
