package com.realroofers.lovappy.web.email;

import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Created by Manoj Senevirathne.
 */
public class GiftRedeemEmailTemplateHandler extends AbstractEmailTemplateHandler {


    public GiftRedeemEmailTemplateHandler(String baseUrl, String email, MailService mailService, ITemplateEngine templateEngine, EmailTemplateDto emailTemplateDto) {
        super(baseUrl, email, mailService, templateEngine, emailTemplateDto);
    }

    @Override
    public void send() {
        if (!emailTemplateDto.isEnabled())
            return;

        try {
            Context context = new Context();
            context.setVariable("url", emailTemplateDto.getAdditionalInformation().get("url"));

            context.setVariable("baseUrl", baseUrl);
            context.setVariable("toUser", emailTemplateDto.getAdditionalInformation().get("toUser"));
            context.setVariable("refNumber", emailTemplateDto.getAdditionalInformation().get("refNumber"));
            context.setVariable("productName", emailTemplateDto.getAdditionalInformation().get("productName"));
            context.setVariable("price", emailTemplateDto.getAdditionalInformation().get("price"));
            context.setVariable("date", emailTemplateDto.getAdditionalInformation().get("date"));
            context.setVariable("redeemCode", emailTemplateDto.getAdditionalInformation().get("redeemCode"));
            context.setVariable("body", emailTemplateDto.getBody());
            context.setVariable("buttonText", emailTemplateDto.getAdditionalInformation().get("buttonText") == null ? "Click Here" : emailTemplateDto.getAdditionalInformation().get("buttonText"));
            String content = templateEngine.process(emailTemplateDto.getTemplateUrl(), context);
            mailService.sendMail(email, content, emailTemplateDto.getSubject(), emailTemplateDto.getFromEmail(), emailTemplateDto.getFromName());
        } catch (Exception ex) {
            LOGGER.error("Failed to send email: {}", ex);
        }
    }

}
