package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.credits.CreditService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.CouponDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.web.util.PaymentUtil;
import com.squareup.connect.ApiException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by daoud on 10/12/2018.
 */
@Controller
@RequestMapping("/credits")
@Slf4j
public class CreditController {

    @Autowired
    private CreditService creditService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private PaymentUtil paymentUtil;




    //TODO user rest API later after using Stripe in mobile
    @PostMapping("/order")
    @ResponseBody
    public ResponseEntity<?>  buyCredits( @RequestParam("nonce") String nonce,
                                          @RequestParam("coupon") String couponCode,
                                          @RequestParam("quantity") Integer quantity) {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        CouponDto coupon = orderService.getCoupon(couponCode, CouponCategory.CREDITS);
        CalculatePriceDto calculatePriceDto = creditService.calculatePrice(coupon, quantity);
        OrderDto order =  orderService.addOrder(userId, OrderDetailType.CREDITS , calculatePriceDto.getTotalPrice(), quantity,
                userId, couponCode, null);
        if (order != null) {

            Boolean executed = null;
            try {
                executed = paymentUtil.executeTransaction(nonce, order, null);
                if (executed) {
                    creditService.addCredit(userId, quantity);
                    return ResponseEntity.ok().build();
                } else {
                    orderService.revertOrder(order.getId());
                }
            } catch (Exception e) {
                log.error("Error during ordering {}", e);
                orderService.revertOrder(order.getId());
            }

        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
