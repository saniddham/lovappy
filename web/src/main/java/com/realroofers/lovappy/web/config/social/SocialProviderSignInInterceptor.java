package com.realroofers.lovappy.web.config.social;

import com.realroofers.lovappy.service.user.model.Roles;
import lombok.extern.slf4j.Slf4j;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.web.ProviderSignInInterceptor;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Daoud Shaheen on 4/7/2018.
 */
@Slf4j
public class SocialProviderSignInInterceptor implements ProviderSignInInterceptor<Twitter> {


    @Override
    public void postSignIn(Connection connection, WebRequest webRequest) {
//        log.debug(" post Interceptor ROLE {}", webRequest.getParameterMap().size());
    }

    @Override
    public void preSignIn(ConnectionFactory<Twitter> connectionFactory, MultiValueMap<String, String> multiValueMap, WebRequest webRequest) {
      try {
          NativeWebRequest nativeWebRequest = (NativeWebRequest) webRequest;
          HttpServletRequest servletRequest = nativeWebRequest.getNativeRequest(HttpServletRequest.class);
//          servletRequest.getSession().setAttribute("role", webRequest.getParameterMap().get("role")[0]);
      } catch (Exception ex) {
          log.error(" post Interceptor ROLE error {}", ex);
      }

    }
}
