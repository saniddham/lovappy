package com.realroofers.lovappy.web.config.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Eias Altawil
 */

public class AuthenticationFilter extends ExceptionTranslationFilter {

    public AuthenticationFilter(AuthenticationEntryPoint authenticationEntryPoint){
        super(authenticationEntryPoint, new HttpSessionRequestCache());
    }

    @Override
    protected void sendStartAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                           AuthenticationException reason)
            throws ServletException, IOException {

        if (request.getRequestURI().contains("/api/")) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, reason.getMessage());
            return;
        }

        super.sendStartAuthentication(request, response, chain, reason);
    }
}