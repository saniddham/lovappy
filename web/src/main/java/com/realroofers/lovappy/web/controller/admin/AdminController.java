package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.dto.AdminProfileDto;
import com.realroofers.lovappy.service.user.dto.ChangePasswordRequest;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.web.dto.InviteAdminRequest;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import com.realroofers.lovappy.web.util.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * @author mwiyono
 */
@Controller()
@RequestMapping("/lox")
public class AdminController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);
    @Autowired
    private RequestCache requestCache;
    @Autowired
    private ITemplateEngine templateEngine;
    @Autowired
    private MailService mailService;
    @Autowired
    private LogoutHandler logoutHandler;
    @Autowired
    private UserService userService;

    @Autowired
    private CloudStorage cloudStorage;

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @GetMapping("/admins")
    public ModelAndView adminsList(ModelAndView model,
                                   @RequestParam(value = "error", required = false) String error,
                                   @RequestParam("pageSize") Optional<Integer> pageSize,
                                   @RequestParam("page") Optional<Integer> pageNumber) {

        Page<UserDto> adminPage = userService.getUsersByRole(Roles.ADMIN.getValue(),
                new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize)));

        model.addObject("users", adminPage);
        model.addObject("error", error);
        model.addObject("inviteRequest", new InviteAdminRequest());
        Pager pager = new Pager(adminPage.getTotalPages(), adminPage.getNumber(), Pager.BUTTONS_TO_SHOW);

        model.addObject("selectedPageSize", pageSize.orElse(Pager.INITIAL_PAGE_SIZE));
        model.addObject("pageSizes", Pager.PAGE_SIZES);
        model.addObject("pager", pager);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {

            User user = userService.getUserByEmail(authentication.getName());
            model.addObject("adminName", user.getFirstName() + " " + user.getLastName());
            model.addObject("adminEmail", user.getEmail());
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            String formatted = format1.format(user.getCreatedOn() == null ? new Date() : user.getCreatedOn());
            model.addObject("sinceDate", "User since " + formatted);

            model.addObject("isSuperAdmin", !authentication.getAuthorities().contains(new SimpleGrantedAuthority(Roles.SUPER_ADMIN.getValue())));
        }
        model.setViewName("admin/admins/manage_admins");
        return model;
    }

    @GetMapping("/admins/{admin-id}/revoke")
    public ModelAndView revokeAdmin(@PathVariable("admin-id") Integer adminId) {
        LOGGER.debug("==========REvoke admin {} ============", adminId);
        userService.revokeAdminRole(adminId);
        return new ModelAndView("redirect:/lox/admins");
    }

    @PostMapping("/admins/invite")
    public ModelAndView inviteAdmin( HttpServletRequest request, InviteAdminRequest inviteAdminRequest, RedirectAttributes redirectAttributes) {
        ModelAndView modelAndView = new ModelAndView("redirect:/lox/admins");
        UserDto userDto = userService.getUser(inviteAdminRequest.getEmail());
        String  password = inviteAdminRequest.getPassword();
        if (userDto == null) {

            userDto.setEmail(inviteAdminRequest.getEmail());
            userDto.setFirstName("");
            userDto.setLastName("");
            userDto.setPassword(password);
            userService.createNewAdmin(userDto);
        } else {
            userService.updatePassword(userDto.getID(), userDto.getPassword(), password);
            if(!userService.assignRole(Roles.ADMIN.getValue(), userDto.getID())){
                modelAndView.addObject("error", "Invited User is already exists");
                return modelAndView;
            }
        }
        mailService.sendInvitationEmail(inviteAdminRequest.getEmail(), userDto, (user, adminInvitationDto) -> {
            String name = userDto.getFirstName() + " " + userDto.getLastName();
            Context context = new Context();
            context.setVariable("name", name);
            context.setVariable("email", userDto.getEmail());
            context.setVariable("password", password);
            context.setVariable("link", CommonUtils.getBaseURL(request)+"/lox");

            return templateEngine.process("email/admin_invitation", context);
        });

        redirectAttributes.addFlashAttribute("popup_msg", "Admin Invitation");
        redirectAttributes.addFlashAttribute("popup_msg_title", "Admin Invitation");
        redirectAttributes.addFlashAttribute("popup_msg_text", "An Admin Invitation email has been sent");
        return modelAndView;
    }


    @GetMapping("/login")
    public ModelAndView logindmin(ModelAndView model, @ModelAttribute("error") String error) {
        LOGGER.debug("==========Login Dashboard============");
        model.getModelMap().addAttribute("error", error);
        model.setViewName("admin/LTE_login");
        return model;
    }

    @PostMapping("/login")
    public ModelAndView loginAdmin(ModelAndView model, HttpServletResponse response,
                                   @ModelAttribute("username") String username, @ModelAttribute("password") String password, HttpServletRequest request) {
        LOGGER.debug("==========Login Dashboard============");
        String status = userService.adminLogin(username, password);
        if ("success".equals(status)) {
            // add authentication to the session
            request.getSession(true).setAttribute(
                    HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
                    SecurityContextHolder.getContext());

            SavedRequest savedRequest = requestCache.getRequest(request, response);
            // for some reason, in staging it goes to https://mycusto.com:8443/error
            if (savedRequest != null && savedRequest.getRedirectUrl() != null && !savedRequest.getRedirectUrl().contains("error") && !savedRequest.getRedirectUrl().contains("null")) {
                model.setViewName("redirect:" + savedRequest.getRedirectUrl());
                LOGGER.debug("[On Login] Redirecting to DefaultSavedRequest Url: " + savedRequest.getRedirectUrl());
            } else {
                model.setViewName("redirect:/lox");
            }
            return model;
        }
        return new ModelAndView("redirect:/lox/login?error=" + status);
    }

    @GetMapping("/logout")
    public ModelAndView logoutAdmin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        LOGGER.debug("==========logout============");
        logoutHandler.logout(request, response, SecurityContextHolder.getContext().getAuthentication());
        return new ModelAndView("redirect:/lox/login");
    }

    @GetMapping
    public ModelAndView adminLTE(ModelAndView model) {
        LOGGER.debug("==========User name============" + SecurityContextHolder.getContext().getAuthentication().getName());
//		model.setViewName("admin/LTE_dashboard");
        return new ModelAndView("admin/LTE_dashboard");
    }

    @GetMapping("/profile")
    public ModelAndView profile() {
        ModelAndView mav = new ModelAndView("admin/admins/admin_profile");
        AdminProfileDto adminProfileDto = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
            adminProfileDto = new AdminProfileDto((User) authentication.getDetails());

        } else {
            adminProfileDto = new AdminProfileDto();
        }

        mav.addObject("changePasswordRequest", new ChangePasswordRequest());

        mav.addObject("adminProfile", adminProfileDto);
        return mav;
    }


    @PostMapping("/profile/save")
    public ModelAndView save(@Valid @ModelAttribute("adminProfile") AdminProfileDto adminProfile,
                             @RequestParam("file") MultipartFile uploadfile,
                             BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return new ModelAndView("admin/admins/admin_profile");

        ModelAndView mav = new ModelAndView("redirect:/lox/profile");
        try {
            CloudStorageFileDto cloudStorageFileDto =
                    cloudStorage.uploadFile(uploadfile, imagesBucket, FileExtension.jpg.toString(), "image/jpeg");
            adminProfile.setProfileUrl(cloudStorageFileDto);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
            AdminProfileDto adminProfileDto = userService.updateAdminProfile(((User) authentication.getDetails()).getUserId(), adminProfile);

            if (adminProfileDto == null)
                mav.addObject("message", "Error while updating account.");
        }
        return mav;
    }

    @PostMapping("/profile/password/save")
    public ResponseEntity<String> save(ChangePasswordRequest changePasswordRequest ) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        boolean isValid = false;
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
            isValid = userService.updatePassword(((User) authentication.getDetails()).getUserId(), changePasswordRequest.getCurrentPassword(), changePasswordRequest.getNewPassword());
        }

        LOGGER.info("isValid  " + isValid);
        return isValid? ResponseEntity.ok().body("Success") : ResponseEntity.badRequest().body("Current Password is not valid");
    }
}