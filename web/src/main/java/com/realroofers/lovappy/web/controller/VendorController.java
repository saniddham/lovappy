package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.repo.CloudStorageRepo;
import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.datingPlaces.support.DatingPlaceUtil;
import com.realroofers.lovappy.service.datingPlaces.support.VerificationType;
import com.realroofers.lovappy.service.exception.EntityValidationException;
import com.realroofers.lovappy.service.gift.GiftExchangeService;
import com.realroofers.lovappy.service.gift.dto.FilerObject;
import com.realroofers.lovappy.service.gift.dto.TopSellingItem;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.product.BrandService;
import com.realroofers.lovappy.service.product.ProductLocationService;
import com.realroofers.lovappy.service.product.ProductService;
import com.realroofers.lovappy.service.product.ProductTypeService;
import com.realroofers.lovappy.service.product.dto.BrandDto;
import com.realroofers.lovappy.service.product.dto.ProductDto;
import com.realroofers.lovappy.service.product.dto.ProductRegDto;
import com.realroofers.lovappy.service.product.dto.ProductTypeDto;
import com.realroofers.lovappy.service.user.CountryService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.ChangePasswordRequest;
import com.realroofers.lovappy.service.user.dto.RegisterUserDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.FormValidator;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.service.validation.ResponseStatus;
import com.realroofers.lovappy.service.validator.UserRegister;
import com.realroofers.lovappy.service.vendor.VendorLocationService;
import com.realroofers.lovappy.service.vendor.VendorService;
import com.realroofers.lovappy.service.vendor.dto.VendorDto;
import com.realroofers.lovappy.service.vendor.dto.VendorRegistrationDto;
import com.realroofers.lovappy.service.vendor.model.VendorLocation;
import com.realroofers.lovappy.service.vendor.model.VendorType;
import com.realroofers.lovappy.web.external.MessageByLocaleService;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import com.realroofers.lovappy.web.util.HttpDownloadUtility;
import com.realroofers.lovappy.web.util.Pager;
import com.realroofers.lovappy.web.util.dto.DownloadFile;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Call;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

/**
 * Created by Manoj on 03/02/2018.
 */
@Controller
@RequestMapping("/vendor")
public class VendorController {
    @Autowired
    private CMSService cmsService;
    private final ProductService productService;
    private final VendorLocationService vendorLocationService;
    private final VendorService vendorService;
    private final FormValidator formValidator;
    private final UserService userService;
    private final MessageByLocaleService messageByLocaleService;
    private final CloudStorageRepo cloudStorageRepo;
    private final EmailTemplateService emailTemplateService;
    private final MailService mailService;
    private final GiftExchangeService giftExchangeService;
    private final ITemplateEngine templateEngine;
    private final NotificationService notificationService;
    private final CountryService countryService;
    private final BrandService brandService;
    private final ProductTypeService productTypeService;
    private final ProductLocationService productLocationService;
    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;
    private final CloudStorage cloudStorage;
    private final CloudStorageService cloudStorageService;

    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 10;
    private static final int[] PAGE_SIZES = {5, 10, 20};


    @Value("${twilio.account.id}")
    private String twilioAccountId;

    @Value("${twilio.authentication.token}")
    private String twilioAuthToken;

    @Value("${twilio.phone.number}")
    private String twilioSenderNumber;

    @Value("${google.maps.api.key}")
    private String googleKeyId;
    private static final String GOOGLE_KEY = "google_key";

    @Autowired
    public VendorController(ProductService productService, VendorLocationService vendorLocationService,
                            VendorService vendorService, FormValidator formValidator, UserService userService,
                            MessageByLocaleService messageByLocaleService, CloudStorageRepo cloudStorageRepo,
                            EmailTemplateService emailTemplateService, MailService mailService,
                            GiftExchangeService giftExchangeService, ITemplateEngine templateEngine,
                            NotificationService notificationService, CountryService countryService,
                            BrandService brandService, ProductTypeService productTypeService,
                            ProductLocationService productLocationService, CloudStorage cloudStorage,
                            CloudStorageService cloudStorageService) {
        this.productService = productService;
        this.vendorLocationService = vendorLocationService;
        this.vendorService = vendorService;
        this.formValidator = formValidator;
        this.userService = userService;
        this.messageByLocaleService = messageByLocaleService;
        this.cloudStorageRepo = cloudStorageRepo;
        this.emailTemplateService = emailTemplateService;
        this.mailService = mailService;
        this.giftExchangeService = giftExchangeService;
        this.templateEngine = templateEngine;
        this.notificationService = notificationService;
        this.countryService = countryService;
        this.brandService = brandService;
        this.productTypeService = productTypeService;
        this.productLocationService = productLocationService;
        this.cloudStorage = cloudStorage;
        this.cloudStorageService = cloudStorageService;
    }

    @GetMapping
    public ModelAndView landingPageSales() {

        int userId = UserUtil.INSTANCE.getLoggedInUserId();

        if (!userService.isVendor(userId)) {
            return new ModelAndView("redirect:/vendor/registration");
        }

        ModelAndView model = new ModelAndView("vendor/dashboard/sales");

        model.addObject("vendor", vendorService.findByUser(userId));
        TopSellingItem topSales = giftExchangeService.getTopSellingItemByVendor(userId);
        ProductDto productDto = null;
        if (topSales != null) {
            Integer id = topSales.getId();
            if (id != null) {
                productDto = productService.findById(id);
            }
        }

        model.addObject("topSales", topSales);
        model.addObject("topSalesProduct", productDto);


        model.addObject("saleByGender", giftExchangeService.getSaleByGenderForVendor(userId));

        model.addObject("saleByBrand", giftExchangeService.getSaleByBrandForVendor(userId));

        model.addObject("saleByCategory", giftExchangeService.getSaleByCategoryForVendor(userId));

        Double totalSales = giftExchangeService.getTotalSalesByVendor(userId);
        Double lovappyCommision = giftExchangeService.getTotalCommissionByVendor(userId);
        Double earning = totalSales - lovappyCommision;


        model.addObject("totalSales", totalSales);
        model.addObject("lovappyCommision", lovappyCommision);
        model.addObject("earning", earning);

        return model;
    }

    @GetMapping("/sales/details")
    public ModelAndView landingPageSalesDetails(@SortDefault("id") Pageable pageable,
                                                @ModelAttribute(value = "filerObject") FilerObject filerObject) {

        int userId = UserUtil.INSTANCE.getLoggedInUserId();

        if (!userService.isVendor(userId)) {
            return new ModelAndView("redirect:/vendor/registration");
        }

        ModelAndView model = new ModelAndView("vendor/dashboard/sales-details");

        model.addObject("vendor", vendorService.findByUser(userId));
        TopSellingItem topSales = giftExchangeService.getTopSellingItemByVendor(userId);
        ProductDto productDto = null;
        if (topSales != null) {
            Integer id = topSales.getId();
            if (id != null) {
                productDto = productService.findById(id);
            }
        }

        model.addObject("topSales", topSales);
        model.addObject("topSalesProduct", productDto);

        Double totalSales = giftExchangeService.getTotalSalesByVendor(userId);
        Double lovappyCommision = giftExchangeService.getTotalCommissionByVendor(userId);
        Double earning = totalSales - lovappyCommision;


        model.addObject("totalSales", totalSales);
        model.addObject("lovappyCommision", lovappyCommision);
        model.addObject("earning", earning);

        model.addObject("filerObject", filerObject != null ? filerObject : new FilerObject());
        model.addObject("page", giftExchangeService.findAllByProductUser(userId, pageable, filerObject));

        return model;
    }

    @GetMapping("/location")
    public String landingPage(ModelMap model, @SortDefault("id") Pageable pageable,
                              @ModelAttribute(value = "filerObject") FilerObject filerObject) {

        int userId = UserUtil.INSTANCE.getLoggedInUserId();

        if (!userService.isVendor(userId)) {
            return "redirect:/vendor/registration";
        }

        model.addAttribute("filerObject", filerObject != null ? filerObject : new FilerObject());
        model.addAttribute("locationsCount", vendorLocationService.getVendorLocationCountByUser(userId));
        model.addAttribute("page", vendorLocationService.findLocationsByLoginUser(userId, pageable, filerObject));
        model.addAttribute("vendor", vendorService.findByUser(userId));
        model.addAttribute("location", new VendorLocation());
        model.addAttribute("countryList", countryService.findAllActive());
        model.addAttribute("totalSales", giftExchangeService.getTotalSalesByVendor(userId));
        model.addAttribute(GOOGLE_KEY, googleKeyId);

        return "vendor/dashboard/location";
    }

    @GetMapping("/edit/location/{id}")
    public String productLocationEdit(Model model, @PathVariable("id") Integer id, HttpServletRequest request) {

        int userId = UserUtil.INSTANCE.getLoggedInUserId();

        if (!userService.isVendor(userId)) {
            return "redirect:/vendor/registration";
        }

        VendorLocation vendorLocation = vendorLocationService.findById(id);

        if (vendorLocation == null) {
            return "redirect:/vendor/location";
        }

        if (!vendorLocation.getCreatedBy().getUserId().equals(userId)) {
            return "redirect:/vendor/location";
        }
        model.addAttribute("location", vendorLocation);
        model.addAttribute(GOOGLE_KEY, googleKeyId);
        return "vendor/product/edit-products-location";
    }

    @GetMapping("/brand")
    public String landingPageBrand(ModelMap model, @SortDefault("id") Pageable pageable,
                                   @ModelAttribute(value = "filerObject") FilerObject filerObject) {

        int userId = UserUtil.INSTANCE.getLoggedInUserId();

        if (!userService.isVendor(userId)) {
            return "redirect:/vendor/registration";
        }

        model.addAttribute("filerObject", filerObject != null ? filerObject : new FilerObject());
        model.addAttribute("brandCount", brandService.getBrandCountByUser(userId));
        model.addAttribute("page", brandService.findBrandsByLoginUser(userId, pageable, filerObject));
        model.addAttribute("brand", new BrandDto());
        model.addAttribute("vendor", vendorService.findByUser(userId));
        model.addAttribute("totalSales", giftExchangeService.getTotalSalesByVendor(userId));

        return "vendor/dashboard/brand";
    }

    @GetMapping("/products")
    public ModelAndView landingPageMyProducts(@RequestParam("pageSize") Optional<Integer> pageSize,
                                              @RequestParam("page") Optional<Integer> page,
                                              @ModelAttribute(value = "filerObject") FilerObject filerObject) {

        int userId = UserUtil.INSTANCE.getLoggedInUserId();

        if (!userService.isVendor(userId)) {
            return new ModelAndView("redirect:/vendor/registration");
        }

        User user = userService.getUserById(userId);

        ModelAndView model = new ModelAndView("vendor/dashboard/products");

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        PageRequest pageable = new PageRequest(evalPage, evalPageSize);

        Page<ProductDto> pages = productService.getAllProductsByNameAndByUser(filerObject, pageable, user);

        model.addObject("products", pages);
        model.addObject("productCount", pages.getTotalElements());
        Pager pager = new Pager(pages.getTotalPages(), pages.getNumber(), BUTTONS_TO_SHOW);
        model.addObject("selectedPageSize", evalPageSize);
        model.addObject("pageSizes", PAGE_SIZES);
        model.addObject("pager", pager);
        model.addObject("vendor", vendorService.findByUser(userId));
        model.addObject("totalSales", giftExchangeService.getTotalSalesByVendor(userId));

        model.addObject("filerObject", filerObject != null ? filerObject : new FilerObject());
        return model;
    }

    @GetMapping("/account")
    public ModelAndView landingPageMyAccount() {

        int userId = UserUtil.INSTANCE.getLoggedInUserId();

        if (!userService.isVendor(userId)) {
            return new ModelAndView("redirect:/vendor/registration");
        }

        ModelAndView model = new ModelAndView("vendor/dashboard/account");

        model.addObject("vendor", vendorService.findByUser(userId));
        model.addObject("totalSales", giftExchangeService.getTotalSalesByVendor(userId));
        model.addObject("changePasswordRequest", new ChangePasswordRequest());
        model.addObject("countryList", countryService.findAllActive());

        return model;
    }

    @GetMapping("/updateProductStatus{id}")
    public ModelAndView updateProductStatus(@PathVariable("id") Integer id) {

        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        productService.updateProductStatus(id, userId);

        return new ModelAndView("redirect:/vendor/products");
    }


    @GetMapping("/registration")
    public ModelAndView registrationPage() {
        Locale locale = LocaleContextHolder.getLocale();
        String language = locale.getLanguage();
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        if (userService.isVendor(userId)) {
            return new ModelAndView("redirect:/vendor");
        }
        ModelAndView model = new ModelAndView("vendor/registration/vendor");
        model.addObject("userId", userId);
        model.addObject("vendorTerms", cmsService.findPageByTagWithImagesAndTexts("vendor-terms", language).getTexts());

        model.addObject("info", new VendorRegistrationDto());

        return model;
    }


    @GetMapping("/mobile/verification{id}")
    public ModelAndView mobileVerificationPage(@PathVariable("id") Integer userId) {

        if (userId == null) {
            userId = UserUtil.INSTANCE.getCurrentUserId();
        }
        if (userId != null && userService.isVendor(userId)) {

            UserDto user = userService.getUser(userId);

            if (user.getMobileVerified()) {
                return new ModelAndView("redirect:/vendor");
            } else {
                ModelAndView model = new ModelAndView("vendor/registration/mobile-verification");
                model.addObject("userId", userId);

                model.addObject("info", new VendorRegistrationDto());

                return model;
            }
        } else {
            return new ModelAndView("redirect:/vendor/registration");
        }
    }


    @GetMapping("/mobile/verification/resend{id}")
    public ModelAndView mobileVerificationResendPage(@PathVariable("id") Integer userId) {

        if (userId == null) {
            userId = UserUtil.INSTANCE.getCurrentUserId();
        }

        if (userService.isVendor(userId)) {

            UserDto user = userService.getUser(userId);

            if (user.getMobileVerified()) {
                return new ModelAndView("redirect:/vendor");
            } else {
                try {
                    VendorDto vendor = userService.findVendorByUserId(userId);
                    vendor.setMobileVerificationCode(DatingPlaceUtil.generateRandomId(6));
                    VendorDto vendorDto = vendorService.update(vendor);

                    Twilio.init(twilioAccountId, twilioAuthToken);
                    if (vendorDto.getVerificationType().name().equals(VerificationType.CALL.name())) {
                        Call call = Call.creator(new PhoneNumber(vendorDto.getPrimaryContactNumber()), new PhoneNumber(twilioSenderNumber),
                                new URI("https://handler.twilio.com/twiml/EHb45bfbb40cd012384a37bd12c2c83e68?verificationCode=" + vendorDto.getMobileVerificationCode())).create();
                        vendorDto.setMobileContent(call.getSid());
                        vendorService.update(vendorDto);
                    } else {
                        String smsBody;
                        if (vendorDto.getVendorType().equals(VendorType.BRAND)) {
                            smsBody = "Thanks for registering your brand with Lovappy. Your verification code is " + vendorDto.getMobileVerificationCode() + ".  Please use this code to complete registration.";
                        } else {
                            smsBody = "Thanks for registering your business with Lovappy. Your verification code is " + vendorDto.getMobileVerificationCode() + ".  Please use this code to complete registration.";
                        }

                        Message message = Message.creator(new PhoneNumber(vendorDto.getPrimaryContactNumber()),
                                new PhoneNumber(twilioSenderNumber), smsBody).create();
                        vendorDto.setMobileContent(message.getSid());
                        vendorService.update(vendorDto);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return new ModelAndView("redirect:/vendor/mobile/verification" + user.getID());
            }
        } else {
            return new ModelAndView("redirect:/vendor/registration");
        }
    }

    @PostMapping("/mobile/verify")
    public ResponseEntity mobileVerification(@RequestParam(value = "code") String code, @RequestParam(value = "id") Integer userId) {

        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();
        try {
            if (userId == null) {
                userId = UserUtil.INSTANCE.getCurrentUserId();
            }

            if (userService.isVendor(userId)) {

                UserDto user = userService.getUser(userId);

                if (user.getMobileVerified()) {
                    errorMessages.add(new ErrorMessage("name", "Already verified!"));
                    res.setErrorMessageList(errorMessages);
                    res.setStatus(ResponseStatus.FAIL);
                    return ResponseEntity.ok().body(res);
                } else {
                    VendorDto vendor = userService.findVendorByUserId(userId);
                    if (vendor.getMobileVerificationCode().equals(code)) {
                        userService.verifyMobile(userId);
                    } else {
                        errorMessages.add(new ErrorMessage("name", "Invalid code!"));
                        res.setErrorMessageList(errorMessages);
                        res.setStatus(ResponseStatus.FAIL);
                        return ResponseEntity.ok().body(res);
                    }
                }
            } else {
                errorMessages.add(new ErrorMessage("name", "You have not registered as a vendor!"));
                res.setErrorMessageList(errorMessages);
                res.setStatus(ResponseStatus.FAIL);
                return ResponseEntity.ok().body(res);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body(res);
        }
        res.setStatus(ResponseStatus.SUCCESS);

        return ResponseEntity.ok().body(res);
    }

    @PostMapping("/signup")
    public ResponseEntity vendorRegistration(@ModelAttribute @Valid VendorRegistrationDto vendorRegistrationDto,
                                             @RequestParam(value = "language", defaultValue = "EN") String language,
                                             HttpServletRequest request) {

        RegisterUserDto dto = new RegisterUserDto();
        dto.setName(vendorRegistrationDto.getPrimaryContactPerson());
        dto.setEmail(vendorRegistrationDto.getPrimaryContactEmail());
        dto.setPassword(vendorRegistrationDto.getPassword());
        dto.setConfirmPassword(vendorRegistrationDto.getConfirmPassword());
        dto.setZipcode("10600");
        dto.setNotificationType(NotificationTypes.VENDOR_REGISTRATION);
        dto.setRole(Roles.VENDOR);
        Integer userId = null;

        JsonResponse res = new JsonResponse();
        try {
            List<ErrorMessage> errorMessages = formValidator.validate(dto, new Class[]{UserRegister.class});
            String name = dto.getName();

            userId = UserUtil.INSTANCE.getCurrentUserId();

            if (userId == null) {
                User userByEmail = userService.getUserByEmail(dto.getEmail());
                if (userByEmail != null) {
                    errorMessages.add(new ErrorMessage("name", messageByLocaleService.getMessage("message.email-in-use")));
                    res.setErrorMessageList(errorMessages);
                    res.setStatus(ResponseStatus.FAIL);
                    return ResponseEntity.ok().body(res);
                }
            }

            if (!name.matches("[-a-zA-Z \'\\.]+")) {
                errorMessages.add(new ErrorMessage("name", messageByLocaleService.getMessage("message.characters-only-name")));
            }
            if (vendorRegistrationDto.getPhotoIdentificationCloudFile().getId() == null) {
                errorMessages.add(new ErrorMessage("photoIdentificationCloudFile", messageByLocaleService.getMessage("message.no-identification-file")));
            }

            if (vendorRegistrationDto.getVendorType().equals(VendorType.BRAND)) {
                BrandDto byName = brandService.findByName(vendorRegistrationDto.getCompanyName());
                if (byName != null) {
                    errorMessages.add(new ErrorMessage("brand-name", messageByLocaleService.getMessage("message.brand-exist")));
                }
            }

            if (errorMessages.size() > 0) {
                res.setErrorMessageList(errorMessages);
                res.setStatus(ResponseStatus.FAIL);
                return ResponseEntity.ok().body(res);
            }
            User user = userService.createNewVendorRegistration(dto);
            userId = user.getUserId();

            VendorDto vendor = new VendorDto();
            vendor.setVendorType(vendorRegistrationDto.getVendorType());
            vendor.setVerificationType(vendorRegistrationDto.getVerificationType());
            vendor.setCompanyName(vendorRegistrationDto.getCompanyName());
            vendor.setPrimaryContactPerson(vendorRegistrationDto.getPrimaryContactPerson());
            vendor.setPrimaryContactNumber(vendorRegistrationDto.getPrimaryContactNumber());
            vendor.setPrimaryContactEmail(vendorRegistrationDto.getPrimaryContactEmail());
            vendor.setAlternateContactPerson(vendorRegistrationDto.getAlternateContactPerson());
            vendor.setAlternateContactEmail(vendorRegistrationDto.getAlternateContactEmail());
            vendor.setApproved(false);
            vendor.setRegistrationStep1(false);
            vendor.setRegistrationStep2(false);
            UserDto user1 = userService.getUser(userId);
            vendor.setUser(user1);
            vendor.setCompanyLogo(vendorRegistrationDto.getPhotoIdentificationCloudFile());
            vendor.setMobileVerificationCode(DatingPlaceUtil.generateRandomId(6));
            VendorDto vendorDto = vendorService.create(vendor);

            if (vendorDto.getVendorType().equals(VendorType.BRAND)) {
                BrandDto brandDto = new BrandDto();
                brandDto.setBrandName(vendor.getCompanyName());
                brandDto.setActive(true);
                brandDto.setCreatedBy(user1);
                brandService.create(brandDto);
            }

            Twilio.init(twilioAccountId, twilioAuthToken);
            if (vendorDto.getVerificationType().name().equals(VerificationType.CALL.name())) {
                try {
                    Call call = Call.creator(new PhoneNumber(vendorDto.getPrimaryContactNumber()), new PhoneNumber(twilioSenderNumber),
                            new URI("https://handler.twilio.com/twiml/EHb45bfbb40cd012384a37bd12c2c83e68?verificationCode=" + vendorDto.getMobileVerificationCode())).create();
                    vendorDto.setMobileContent(call.getSid());
                    vendorService.update(vendorDto);
//                response.setResult(message.getSid());
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            } else {
                String smsBody;

                if (vendorDto.getVendorType().equals(VendorType.BRAND)) {
                    smsBody = "Thanks for registering your brand with Lovappy. Your verification code is " + vendorDto.getMobileVerificationCode() + ".  Please use this code to complete registration.";
                } else {
                    smsBody = "Thanks for registering your business with Lovappy. Your verification code is " + vendorDto.getMobileVerificationCode() + ".  Please use this code to complete registration.";
                }

                Message message = Message.creator(new PhoneNumber(vendorDto.getPrimaryContactNumber()),
                        new PhoneNumber(twilioSenderNumber), smsBody).create();
                vendorDto.setMobileContent(message.getSid());
                vendorService.update(vendorDto);
//                response.setResult(message.getSid());
            }


//            String key = UUID.randomUUID().toString();
//            UserDto newUser = userService.updateVerifyEmailKey(userId, key);
//            if (newUser != null) {
//                EmailTemplateDto emailTemplate = emailTemplateService
//                        .getByNameAndLanguage(EmailTemplateConstants.ACCOUNT_VERIFICATION, language);
//                String baseUrl = CommonUtils.getBaseURL(request);
//                emailTemplate.getAdditionalInformation().put("url", baseUrl + "/register/verify/" + key);
//                new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), user.getEmail(), templateEngine,
//                        mailService, emailTemplate).send();
//
//            }

            NotificationDto notificationDto = new NotificationDto();
            notificationDto.setContent(dto.getName() + " is registered as a Vendor in lovappy");
            notificationDto.setType(dto.getNotificationType());
            notificationDto.setUserId(userId);
            notificationService.sendNotificationToAdmins(notificationDto);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body(res);
        }
        res.setStatus(ResponseStatus.SUCCESS);
        res.setResult(userId);
        return ResponseEntity.ok().body(res);

    }


    @GetMapping("/product/submission/options")
    public String productSubmissionOptions() {

        return "vendor/product/submission-options";
    }


    @GetMapping("/product/submission")
    public String productSubmission(Model model) {

        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        ProductRegDto productRegDto = new ProductRegDto();

        VendorDto user = vendorService.findByUser(userId);
        List<BrandDto> brandList;

        if (user.getVendorType().equals(VendorType.VENDOR)) {
            productRegDto.setVendorCode("R" + user.getId());
            brandList = brandService.findAllActive();
        } else {
            productRegDto.setVendorCode("B" + user.getId());
            brandList = new ArrayList<>();
            BrandDto brandDto = brandService.findByName(user.getCompanyName());
            if (brandDto != null) {
                brandList.add(brandDto);
            } else {
                BrandDto branddto = new BrandDto();
                branddto.setBrandName(user.getCompanyName());
                branddto.setActive(true);
                branddto.setCreatedBy(user.getUser());
                try {
                    BrandDto brandDto1 = brandService.create(branddto);
                    brandList.add(brandDto1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        model.addAttribute("product", productRegDto);
        model.addAttribute("location", new VendorLocation());
        model.addAttribute("brand", new BrandDto());
        model.addAttribute("productType", new ProductTypeDto());
        model.addAttribute("locations", vendorLocationService.findLocationsByLoginUser(userId));
        model.addAttribute("brandList", brandList);
        model.addAttribute("countryList", countryService.findAllActive());
        model.addAttribute("productTypeList", productTypeService.findAllActive());
        model.addAttribute("vendorType", user.getVendorType().toString());
        model.addAttribute(GOOGLE_KEY, googleKeyId);

        return "vendor/product/submission";
    }


    @GetMapping("/add/product")
    public String productAdd(Model model) {

        Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        ProductRegDto productRegDto = new ProductRegDto();

        VendorDto user = vendorService.findByUser(userId);
        List<BrandDto> brandList;

        if (user.getVendorType().equals(VendorType.VENDOR)) {
            productRegDto.setVendorCode("R" + user.getId());
            brandList = brandService.findAllActive();
        } else {
            productRegDto.setVendorCode("B" + user.getId());
            brandList = new ArrayList<>();
            BrandDto brandDto = brandService.findByName(user.getCompanyName());
            if (brandDto != null) {
                brandList.add(brandDto);
            } else {
                BrandDto branddto = new BrandDto();
                branddto.setBrandName(user.getCompanyName());
                branddto.setActive(true);
                branddto.setCreatedBy(user.getUser());
                try {
                    BrandDto brandDto1 = brandService.create(branddto);
                    brandList.add(brandDto1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        model.addAttribute("product", productRegDto);
        model.addAttribute("location", new VendorLocation());
        model.addAttribute("brand", new BrandDto());
        model.addAttribute("productType", new ProductTypeDto());
        model.addAttribute("locations", vendorLocationService.findLocationsByLoginUser(userId));
        model.addAttribute("brandList", brandList);
        model.addAttribute("countryList", countryService.findAllActive());
        model.addAttribute("productTypeList", productTypeService.findAllActive());
        model.addAttribute("vendorType", user.getVendorType().toString());
        model.addAttribute(GOOGLE_KEY, googleKeyId);

        return "vendor/product/add-products";
    }


    @GetMapping("/edit/product/{id}")
    public String productEdit(Model model, @PathVariable("id") Integer id, HttpServletRequest request) {

        ProductDto productDto = productService.findById(id);

        if (productDto == null) {
            return "redirect:/vendor/products";
        }

        Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        if (!productDto.getCreatedBy().getID().equals(userId)) {
            return "redirect:/vendor/products";
        }

        ProductRegDto productRegDto = new ProductRegDto();

        VendorDto user = vendorService.findByUser(userId);
        List<BrandDto> brandList;

        if (user.getVendorType().equals(VendorType.VENDOR)) {
            brandList = brandService.findAllActive();
        } else {
            brandList = new ArrayList<>();
            BrandDto brandDto = brandService.findByName(user.getCompanyName());
            if (brandDto != null) {
                brandList.add(brandDto);
            }
        }

        String defaultImage = CommonUtils.getBaseURL(request) + "/images/vendor/upload.svg";
        productRegDto.setId(productDto.getId());
        productRegDto.setProductCode(productDto.getProductCode());
        productRegDto.setProductName(productDto.getProductName());
        productRegDto.setProductDescription(productDto.getProductDescription());
        productRegDto.setProductEan(productDto.getProductEan());
        productRegDto.setPrice(productDto.getPrice());

        if (productDto.getProductType() != null) {
            productRegDto.setProductType(productDto.getProductType().getId());
        }

        if (productDto.getBrand() != null) {
            productRegDto.setBrand(productDto.getBrand().getId());
        }

        if (productDto.getImage1() != null && productDto.getImage1().getUrl() != null && !productDto.getImage1().getUrl().equals("")) {
            productRegDto.setImage1(productDto.getImage1().getId());
            productRegDto.setImageUrl1(productDto.getImage1().getUrl());
        } else {
            productRegDto.setImageUrl1(defaultImage);
        }

        if (productDto.getImage2() != null && productDto.getImage2().getUrl() != null && !productDto.getImage2().getUrl().equals("")) {
            productRegDto.setImage2(productDto.getImage2().getId());
            productRegDto.setImageUrl2(productDto.getImage2().getUrl());
        } else {
            productRegDto.setImageUrl2(defaultImage);
        }

        if (productDto.getImage3() != null && productDto.getImage3().getUrl() != null && !productDto.getImage3().getUrl().equals("")) {
            productRegDto.setImage3(productDto.getImage3().getId());
            productRegDto.setImageUrl3(productDto.getImage3().getUrl());
        } else {
            productRegDto.setImageUrl3(defaultImage);
        }


        if (user.getVendorType().equals(VendorType.VENDOR)) {
            productRegDto.setVendorCode("R" + user.getId());
        } else {
            productRegDto.setVendorCode("B" + user.getId());
        }

        productRegDto.setProductCode(productRegDto.getProductCode().replace(productRegDto.getVendorCode()+"-",""));

        final List<Integer> vendorLocations = new ArrayList<>();

        productLocationService.findByProduct(id).stream().forEach(productLocation -> vendorLocations.add(productLocation.getLocation().getId()));

        productRegDto.setVendorLocations(vendorLocations);

        model.addAttribute("product", productRegDto);
        model.addAttribute("locations", vendorLocationService.findLocationsByLoginUser(userId));
        model.addAttribute("brandList", brandList);
        model.addAttribute("countryList", countryService.findAllActive());
        model.addAttribute("productTypeList", productTypeService.findAllActive());
        model.addAttribute("vendorType", user.getVendorType().toString());
        model.addAttribute(GOOGLE_KEY, googleKeyId);

        return "vendor/product/edit-products";
    }

    @PostMapping("/product/image_upload")
    public ResponseEntity upload(@RequestParam("image") MultipartFile imageFile) {
        List<ErrorMessage> errorMessages = new ArrayList<>();
        if (imageFile.isEmpty())
            errorMessages.add(new ErrorMessage("image", "Image file required!"));

        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        try {
            CloudStorageFileDto coverFileDto = cloudStorage.uploadFile(imageFile, imagesBucket, FilenameUtils.getExtension(imageFile.getOriginalFilename()), imageFile.getContentType());
            CloudStorageFileDto added = cloudStorageService.add(coverFileDto);
            return ResponseEntity.ok(added);

        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/product/image_upload_url")
    public ResponseEntity uploadFromUrl(@RequestParam("image") String imageUrl) {
        List<ErrorMessage> errorMessages = new ArrayList<>();
        if (imageUrl == null || imageUrl.equals(""))
            errorMessages.add(new ErrorMessage("image", "Image file required!"));

        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        try {
            DownloadFile downloadFile = HttpDownloadUtility.downloadFile(imageUrl);

            if (downloadFile.getImageBytes().length > -1) {
                CloudStorageFileDto coverFileDto = cloudStorage.uploadFile(downloadFile.getImageBytes(), imagesBucket, FilenameUtils.getExtension(downloadFile.getFileName()), downloadFile.getContentType(), true);
                CloudStorageFileDto added = cloudStorageService.add(coverFileDto);
                return ResponseEntity.ok(added);
            } else {
                errorMessages.add(new ErrorMessage("image", "Invalid file!"));
                throw new EntityValidationException(errorMessages);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/location/deactivate{id}")
    public String locationDeactivate(@PathVariable("id") Integer id) throws Exception {
        VendorLocation location = vendorLocationService.findById(id);
        if (location != null) {
            location.setActive(false);
            vendorLocationService.update(location);
        }

        return "redirect:/vendor/location";
    }

    @GetMapping("/location/activate{id}")
    public String locationActivate(@PathVariable("id") Integer id) throws Exception {
        VendorLocation location = vendorLocationService.findById(id);
        if (location != null) {
            location.setActive(true);
            vendorLocationService.update(location);
        }

        return "redirect:/vendor/location";
    }
}