package com.realroofers.lovappy.web.config.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;

/**
 * Created by Daoud Shaheen on 02/09/2018.
 */
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

  private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationEntryPoint.class);

  @Override
  public void commence(HttpServletRequest request, HttpServletResponse response,
      AuthenticationException authException) throws IOException, ServletException {

    // Requested URI
    String requestURI = request.getRequestURI();

    // Logged in User
    String loggedInUser = "anonymous";
    if (SecurityContextHolder.getContext().getAuthentication() != null) {
      loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
    }
    
    logger.error(" Authentication error, User with email {} is not authenticated to access this resource {} ",
        loggedInUser, requestURI, authException.getLocalizedMessage());
    response.sendError(HttpStatus.UNAUTHORIZED.value(), authException.getLocalizedMessage());
  }
}
