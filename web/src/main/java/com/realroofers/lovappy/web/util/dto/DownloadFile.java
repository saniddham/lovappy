package com.realroofers.lovappy.web.util.dto;

import lombok.Data;

@Data
public class DownloadFile {
    String fileName;
    String contentType;
    int contentLength;
    byte[] imageBytes;
}
