package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.user.dto.UserDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Manoj Senevirathe on 12/05/2018.
 */
@Controller
@RequestMapping("/video_blogger")
public class VideoBloggerController {

    @GetMapping("/registration")
    public ModelAndView aboutUs(ModelAndView model) {
        model.setViewName("blog/video-blog-reg");
        model.addObject("user", new UserDto());
        return model;
    }
}
