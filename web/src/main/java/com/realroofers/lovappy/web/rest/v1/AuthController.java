package com.realroofers.lovappy.web.rest.v1;


import java.util.*;
import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.user.SocialProfileService;
import com.realroofers.lovappy.service.user.UserPreferenceService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.dto.SocialProfileDto;
import com.realroofers.lovappy.service.user.dto.UserAuthDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.dto.UserPreferenceDto;
import com.realroofers.lovappy.web.config.security.ClientService;
import com.realroofers.lovappy.web.config.security.UserAuthorizationService;
import com.realroofers.lovappy.web.exception.BadRequestException;
import com.realroofers.lovappy.web.rest.dto.ErrorStatus;
import com.realroofers.lovappy.web.rest.dto.LoginRequest;
import com.realroofers.lovappy.web.rest.dto.LoginResponse;
import com.realroofers.lovappy.web.rest.dto.SocialAccessTokenRequest;
import com.realroofers.lovappy.web.rest.support.GoogleVerifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedClientException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.social.ExpiredAuthorizationException;
import org.springframework.social.UncategorizedApiException;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.support.OAuth1ConnectionFactory;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.oauth1.OAuthToken;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.util.StringUtils;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

/**
 * Contain main authentication APIs
 *
 * @author Daoud Shaheen [03-02-2018]
 */
@RestController
public class AuthController {

  private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

  private final ClientService clientService;
  private final UserAuthorizationService userAuthorizationService;
  private final ConsumerTokenServices consumerTokenServices;
  private final TokenEndpoint tokenEndpoint;
  private final UserService userService;
  private final LovstampService lovstampService;
  private final UserPreferenceService userPreferenceService;
  private final SocialProfileService socialProfileService;
  @Value("${lovappy.oauth2.client-id}")
  private String clientId;

  @Autowired
  private GoogleVerifier googleVerifier;

  private final ConnectionFactoryLocator connectionFactoryLocator;

  public AuthController(
          ClientService clientService, UserAuthorizationService userAuthorizationService,
          ConsumerTokenServices consumerTokenServices,
          TokenEndpoint tokenEndpoint, UserService userService, LovstampService lovstampService, UserPreferenceService userPreferenceService, SocialProfileService socialProfileService, ConnectionFactoryLocator connectionFactoryLocator) {
    this.clientService = clientService;
    this.userAuthorizationService = userAuthorizationService;
    this.consumerTokenServices = consumerTokenServices;
    this.tokenEndpoint = tokenEndpoint;
    this.userService = userService;
    this.lovstampService = lovstampService;
    this.userPreferenceService = userPreferenceService;
    this.socialProfileService = socialProfileService;
    this.connectionFactoryLocator = connectionFactoryLocator;
  }

  @PermitAll
  @RequestMapping(value = "//api/v2/login", method = RequestMethod.POST)
  public OAuth2AccessToken login(HttpServletRequest request,
                             @RequestBody @Valid LoginRequest loginRequest)
      throws HttpRequestMethodNotSupportedException {
    // make sure user exist
    // login service
    try {
      UserDto user = userAuthorizationService.login(loginRequest.getUsername(), loginRequest.getPassword());

      // get user authentication object
      Authentication userAuthentication = SecurityContextHolder.getContext().getAuthentication();
      // prepare OAuth2 password flow to generate access token
      Map<String, String> parameters = new HashMap<>();
      parameters.put("grant_type", "password");
      parameters.put("username", loginRequest.getUsername());
      parameters.put("password", loginRequest.getPassword());

      //make sure to add the main application to database
      ClientDetails client = clientService.loadClientByClientId(clientId);

      OAuth2Request storedRequest = new OAuth2Request(null, client.getClientId(),
              client.getAuthorities(), true, client.getScope(), client.getResourceIds(), null,
              client.getAuthorizedGrantTypes(), null);
      Authentication authentication = new OAuth2Authentication(storedRequest, userAuthentication);

      SecurityContextHolder.getContext().setAuthentication(authentication);

      // generate the access token
      OAuth2AccessToken token = tokenEndpoint.getAccessToken(authentication, parameters).getBody();

      return token;
    } catch (InvalidGrantException ex) {
      throw new BadRequestException(ex.getMessage());
    }
  }

  @PermitAll
  @RequestMapping(value = "/api/v2/social/{provider}/login", method = RequestMethod.POST)
  public ResponseEntity<?> login(HttpServletRequest request,
                             @PathVariable("provider") String providerId,
                             @RequestBody @Valid SocialAccessTokenRequest loginRequest)
          throws HttpRequestMethodNotSupportedException {


    logger.debug("socialLogin()--> ProviderId = {} <--> accessTokenRequest = {}", providerId, loginRequest);
    try {
      Connection<?> connection = null;
      UserAuthDto dto = null;
      switch (providerId) {
        case "facebook":
        case "instagram":

          OAuth2ConnectionFactory<?> connectionFactory = (OAuth2ConnectionFactory<?>) connectionFactoryLocator
                  .getConnectionFactory(providerId.toString());
          connection = connectionFactory
                  .createConnection(new AccessGrant(loginRequest.getAccessToken()));
          //Social login
           dto = toUser(connection, providerId, loginRequest.getEmail());
          break;
        case "google":
            UserProfile socialProfile = googleVerifier.verfiyToken(loginRequest.getAccessToken());
            if(socialProfile == null) {
              throw new BadRequestException("invalid accessToken");
            }
          dto = userAuthorizationService.fromSocialToAuthUser(socialProfile, providerId, null, socialProfile.getId());
          break;
        case "twitter":

          if (loginRequest.getAccessTokenSecret() == null) {
            throw new BadRequestException("invalid accessToken");
          }

          OAuth1ConnectionFactory<?> connectionFactoryTest = (OAuth1ConnectionFactory<?>) connectionFactoryLocator.getConnectionFactory(providerId.toString());
          connection = connectionFactoryTest
                  .createConnection(new OAuthToken(loginRequest.getAccessToken(), loginRequest.getAccessTokenSecret()));
          dto = toUser(connection, providerId, null);
          break;

          default:
            throw new BadRequestException("Socail media " + providerId + " is not supported");


      }
      UserDto userByEmail = null;
      String email = dto.getEmail();
      if(StringUtils.isEmpty(email)){
        SocialProfileDto socialProfileDto = toSocialUser(dto, dto.getUsername());
        try {
          socialProfileDto = socialProfileService.create(socialProfileDto);
        } catch (Exception e) {
          e.printStackTrace();
        }

        dto.setUserId(socialProfileDto.getUserId());
      }else {
        userByEmail = userService.getUser(email);
        if (userByEmail != null) {
          dto.setUserId(userByEmail.getID());
          SocialProfileDto socialProfileDto = toSocialUser(dto, dto.getUsername());
          socialProfileDto.setUserId(userByEmail.getID());
          try {
            socialProfileService.update(socialProfileDto);
          } catch (Exception e) {
            e.printStackTrace();
          }
          userService.update(dto, loginRequest.getRole());
        } else {
          dto.setUserId(socialProfileService.saveSocial(dto, loginRequest.getRole()));
        }
      }

      // get user authentication object
      //create user authentication object used later for authentication
      Authentication userAuthentication = new UsernamePasswordAuthenticationToken(email,
              dto.getPassword(), dto.getAuthorities());
      // prepare OAuth2 password flow to generate access token
      Map<String, String> parameters = new HashMap<>();
      parameters.put("grant_type", "password");
      parameters.put("username", dto.getEmail());
      parameters.put("password", dto.getPassword());

      //make sure to add the main application to database
      ClientDetails client = clientService.loadClientByClientId(clientId);

      OAuth2Request storedRequest = new OAuth2Request(null, client.getClientId(),
              client.getAuthorities(), true, client.getScope() , client.getResourceIds(), null,
              client.getAuthorizedGrantTypes() , null);
      Authentication authentication = new OAuth2Authentication(storedRequest, userAuthentication);

      SecurityContextHolder.getContext().setAuthentication(authentication);

      // generate the access token
      OAuth2AccessToken token = tokenEndpoint.getAccessToken(authentication, parameters).getBody();
      return ResponseEntity.ok(token);
    } catch (IllegalArgumentException | InvalidGrantException | HttpClientErrorException  | UnauthorizedClientException | ExpiredAuthorizationException | UncategorizedApiException e) {
      logger.debug("Invalid access token {}", e);
      return ResponseEntity.badRequest().body(new ErrorStatus("400", e.getMessage()));
    }

  }
  private SocialProfileDto toSocialUser(UserAuthDto dto, String name){
    SocialProfileDto socialProfileDto = new SocialProfileDto();
    socialProfileDto.setFirstName(dto.getFirstName());
    socialProfileDto.setLastName(dto.getLastName());
    socialProfileDto.setSocialId(dto.getSocialId());
    socialProfileDto.setSocialPlatform(dto.getSocialPlatform());
    socialProfileDto.setImageUrl(dto.getImageUrl());
    socialProfileDto.setName(name);
    return socialProfileDto;
  }
  private UserAuthDto toUser(Connection<?> connection, String socialPlatform, String email) {
    return userAuthorizationService.socialLogin(connection, socialPlatform, email);
  }

  @GetMapping("/oauth/revoke")
  public void revokeToken(@RequestParam(value = "token", required = true) String token) {

    if (consumerTokenServices.revokeToken(token)) {
      logger.info("revoke access token {} was successfully done", token);
    } else {
      throw new InvalidTokenException("invalid Token :" + token);
    }
  }
}
