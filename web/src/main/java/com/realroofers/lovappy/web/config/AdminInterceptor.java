package com.realroofers.lovappy.web.config;

import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.Notification;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.model.User;
import org.aspectj.weaver.ast.Not;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class AdminInterceptor extends HandlerInterceptorAdapter {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminInterceptor.class);
	@Autowired
	private RequestCache requestCache;
	@Autowired
	private NotificationService notificationService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if (request.getRequestURI().contains("lox")){

			if(request.getRequestURI().contains("login"))
				return true;
			org.springframework.security.core.Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			if (authentication == null || authentication.getName().equals("anonymousUser")) {
				requestCache.saveRequest(request, response);
				response.sendRedirect(request.getContextPath() + "/lox/login");
			} else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority(Roles.ADMIN.getValue()))) {
				return true;
			}
			else if(authentication.getAuthorities().contains(new SimpleGrantedAuthority(Roles.SUPER_ADMIN.getValue()))){
					return true;
			} else {
				response.sendRedirect(request.getContextPath() + "/error/403");
			}
			return true;
		}


		return false;

	}

	@Override
	public void postHandle(
			HttpServletRequest req,
			HttpServletResponse res,
			Object o,
			ModelAndView model) throws Exception {

		if (model != null ) {

			initializeAdminHeader(model);

		}
	}

	private void initializeAdminHeader(ModelAndView model) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
			User user = null;
			if( authentication.getDetails() instanceof User) {

				user = (User) authentication.getDetails();
				model.addObject("adminName", user.getFirstName() + " " + user.getLastName());
				model.addObject("adminEmail", user.getEmail());
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
				String formatted = format1.format(user.getCreatedOn() == null ? new Date() : user.getCreatedOn());
				model.addObject("sinceDate", "User since " + formatted);

				//blogs notification
				Page<NotificationDto> blogsNotificationPage = notificationService.getPageOfNotificaionByUserAndType(user, NotificationTypes.USER_BLOGS,1, 10);
				model.addObject("blogsNotifications", blogsNotificationPage.getContent());
				int blogBadgeNumber = notificationService.countNotSeenNotificationsByUserAndType(user, NotificationTypes.USER_BLOGS);
				model.addObject("blogBadgeNumber", blogBadgeNumber > 0 ? blogBadgeNumber : null);
				model.addObject("blogSeenNumber", blogBadgeNumber > 0 ? "" + blogBadgeNumber : "no");

				boolean isBlogHidden = false;
				if (blogsNotificationPage.getTotalElements() > 0) {
					isBlogHidden = true;
				}
				model.addObject("isBlogHidden", isBlogHidden);


				//blogs notification
				Page<NotificationDto> adsNotificationPage = notificationService.getPageOfNotificaionByUserAndType(user, NotificationTypes.USER_ADS,1, 10);
				model.addObject("adsNotifications", adsNotificationPage.getContent());
				int adsBadgeNumber = notificationService.countNotSeenNotificationsByUserAndType(user, NotificationTypes.USER_ADS);
				model.addObject("adsBadgeNumber", adsBadgeNumber > 0 ? adsBadgeNumber : null);
				model.addObject("adsSeenNumber", adsBadgeNumber > 0 ? "" + adsBadgeNumber : "no");

				boolean isAdsHidden = false;
				if (adsNotificationPage.getTotalElements() > 0) {
					isAdsHidden = true;
				}
				model.addObject("isAdsHidden", isAdsHidden);

				//user registration notification
				Page<NotificationDto> userRegistrationNotificationPage = notificationService.getPageOfNotificaionByUserAndType(user, NotificationTypes.USER_REGISTRATION,1, 10);
				model.addObject("registrationNotifications", userRegistrationNotificationPage.getContent());
				int registrationBadgeNumber = notificationService.countNotSeenNotificationsByUserAndType(user, NotificationTypes.USER_REGISTRATION);
				model.addObject("registrationBadgeNumber", registrationBadgeNumber > 0 ? registrationBadgeNumber : null);
				model.addObject("registrationSeenNumber", registrationBadgeNumber > 0 ? "" + registrationBadgeNumber : "no");

				boolean isRegistrationHidden = false;
				if (userRegistrationNotificationPage.getTotalElements() > 0) {
					isRegistrationHidden = true;
				}
				model.addObject("isRegistrationHidden", isRegistrationHidden);


				//other notifications
				Page<NotificationDto> otherNotificationPage = notificationService.getPageOfNotificaionByUserExceptRegistartionAndBlog(user,1, 10);
				model.addObject("otherNotifications", otherNotificationPage.getContent());
				int otherBadgeNumber = notificationService.countNotSeenNotificationsByUserExceptRegistrationAndBlog(user);
				model.addObject("otherBadgeNumber", otherBadgeNumber > 0 ? otherBadgeNumber : null);
				model.addObject("otherSeenNumber", otherBadgeNumber > 0 ? "" + otherBadgeNumber : "no");

				boolean isOtherHidden = false;
				if (otherNotificationPage.getTotalElements() > 0) {
					isOtherHidden = true;
				}
				model.addObject("isOtherHidden", isOtherHidden);

				model.addObject("profileUrl",
						user.getUserProfile().getProfilePhotoCloudFile() == null ? "/images/adminLTE/user2-160x160.jpg"
								: user.getUserProfile().getProfilePhotoCloudFile().getUrl());

			}
		}
	}
}