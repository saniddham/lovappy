package com.realroofers.lovappy.web.controller;

import com.google.auth.oauth2.AccessToken;
import com.google.auth.oauth2.GoogleCredentials;
import com.realroofers.lovappy.service.blog.dto.EmailShareDto;
import com.realroofers.lovappy.service.lovstamps.LovstampSampleService;
import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.lovstamps.support.SampleTypes;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.music.MusicExchangeResponseService;
import com.realroofers.lovappy.service.music.dto.MusicResponseDto;
import com.realroofers.lovappy.service.radio.dto.RadioFilterDto;
import com.realroofers.lovappy.service.system.LanguageService;
import com.realroofers.lovappy.service.user.UserProfileAndPreferenceService;
import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.GenderAndPrefGenderDto;
import com.realroofers.lovappy.service.user.dto.UserFlagDto;
import com.realroofers.lovappy.service.user.dto.UserProfileDto;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.util.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Daoud Shaheen on 12/18/2017.
 */
@Controller
public class RadioController {
    private static Logger LOG = LoggerFactory.getLogger(RadioController.class);
    @Value("${google.maps.api.key}")
    private String googleKeyId;
    @Value("${lovappy.cloud.bucket.lovdrop}")
    private String lovdropsBucket;
    @Value("https://www.googleapis.com/auth/cloud-platform")
    private String dialogflowScope;
//    @Value("files/LovappyTest-23c115e5e061.json")
//    private String dialogAuthFile;
    private LovstampService lovstampService;
    private final LovstampSampleService lovstampSampleService;
    private UserService userService;
    private final LanguageService languageService;
    private final UserProfileService userProfileService;
    private final ITemplateEngine templateEngine;
    private final UserProfileAndPreferenceService userProfileAndPreferenceService;
    private final EmailTemplateService emailTemplateService;
    private final MailService mailService;
    private final MusicExchangeResponseService musicExchangeResponseService;


    public RadioController(LovstampService lovstampService, LovstampSampleService lovstampSampleService, UserService userService, LanguageService languageService, UserProfileService userProfileService, ITemplateEngine templateEngine, UserProfileAndPreferenceService userProfileAndPreferenceService, EmailTemplateService emailTemplateService, MailService mailService, MusicExchangeResponseService musicExchangeResponseService) {
        this.lovstampService = lovstampService;
        this.lovstampSampleService = lovstampSampleService;
        this.userService = userService;
        this.languageService = languageService;
        this.userProfileService = userProfileService;
        this.templateEngine = templateEngine;
        this.userProfileAndPreferenceService = userProfileAndPreferenceService;
        this.emailTemplateService = emailTemplateService;
        this.mailService = mailService;
        this.musicExchangeResponseService = musicExchangeResponseService;
    }

    @GetMapping("/radio")
    public ModelAndView radio(ModelAndView mav,  @RequestParam(value = "lang", defaultValue = "EN") String language){
        mav.setViewName("radio/lovappy-radio");
        Locale locale = LocaleContextHolder.getLocale();
        language = locale.getLanguage().toUpperCase();
        Integer currentUserId = UserUtil.INSTANCE.getCurrentUserId();
        GenderAndPrefGenderDto info;

        mav.addObject("languages", languageService.getAllLanguages(null));

        if (currentUserId == null){
            mav.addObject("blockedUsersIDs", new ArrayList<Integer>());
            mav.addObject("likedUserIds", new ArrayList<Integer>());
            mav.addObject("music", null);
            info = new GenderAndPrefGenderDto();
        } else {
            MusicResponseDto musicResponse = musicExchangeResponseService.getOneInboxResponsesForLoginUser(currentUserId);
            mav.addObject("music", musicResponse);
            mav.addObject("blockedUsersIDs", userService.getUser(currentUserId).getBlockedUsersIDs());
            mav.addObject("likedUserIds", userService.getUserLikeIDs(currentUserId));
            info = userProfileAndPreferenceService.getGenderAndPrefGender(currentUserId);
        }

        mav.addObject("userFlag", new UserFlagDto());

        mav.addObject("filter", new RadioFilterDto());
        mav.addObject("emailShare", new EmailShareDto());

        mav.addObject("loggedIn", currentUserId != null);
        mav.addObject("info", info);

        String accessToken = getDialogFlowAuthToken();
        mav.addObject("access_token", accessToken);
        mav.addObject("google_key", googleKeyId);
        return mav;
    }

    @GetMapping("/radio/share/email")
    @ResponseBody
    public ResponseEntity<?> shareEmail(
            HttpServletRequest request,
            EmailShareDto emailShareDto,
            @RequestParam(value = "lang", defaultValue = "EN") String language) {

        EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.LOVSTAMP_SHARE, language);

        emailTemplate.getAdditionalInformation().put("recipientName", emailShareDto.getRecipientName());
        emailTemplate.getAdditionalInformation().put("name", emailShareDto.getName());
        emailTemplate.getAdditionalInformation().put("recipientEmail", emailShareDto.getShareWithEmail());
        emailTemplate.getAdditionalInformation().put("sharedBy", emailShareDto.getEmail());
        emailTemplate.getAdditionalInformation().put("shareLink", emailShareDto.getShareUrl());
        emailTemplate.getAdditionalInformation().put("buttonText", "Lovstamp share");

        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), emailShareDto.getShareWithEmail(), templateEngine,
                mailService, emailTemplate).send();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = {"/lovdrop/delete_recording"})
    @ResponseBody
    public ResponseEntity<?> deleteRecording(@RequestParam("id") Integer id){
        lovstampService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/lovdrop/samples")
    @ResponseBody
    public ResponseEntity<?> getLovdropsSamples(@RequestParam("language") Integer languageID){
        UserProfileDto profile = userProfileService.findOne(UserUtil.INSTANCE.getCurrentUserId());
        return ResponseEntity
                .ok()
                .body(lovstampSampleService.getLovstampSamplesByLanguageAndGenderAndType(languageID, profile.getGender(), SampleTypes.SAMPLE, new PageRequest(0, 100)).getContent());
    }

    @GetMapping("/block")
    public String blockUser(Integer userId) {
        LOG.info("blockUser {}" , userId);
        if (UserUtil.INSTANCE.getCurrentUserId().equals(userId)){
            LOG.error("Can not blockUser the same as login User :  {} ", UserUtil.INSTANCE.getCurrentUser().getEmail());
        } else {
            int blockedSuccess=userProfileService.blockUser(UserUtil.INSTANCE.getCurrentUserId(), userId);
        }

        return "redirect:/radio";
    }



    @GetMapping("/unblock")
    public String unblockUser(Integer userId) {
        LOG.info("unblockUser {}" , userId);
        if (UserUtil.INSTANCE.getCurrentUserId().equals(userId)){
            LOG.error("Can not unblockUser the same as login User :  {} ", UserUtil.INSTANCE.getCurrentUser().getEmail());
        } else {
            int blockedSuccess=userProfileService.unblockUser(UserUtil.INSTANCE.getCurrentUserId(), userId);
        }

        return "redirect:/radio";
    }

    @PostMapping("/lovdrop/listen/{user-id}")
    public ResponseEntity listenTo(@PathVariable("user-id") Integer userId) {
        if( UserUtil.INSTANCE.getCurrentUser() != null) {
            lovstampService.incrementListenByCounter(userId, UserUtil.INSTANCE.getCurrentUserId());
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private String getDialogFlowAuthToken(){
        LOG.info("getting dialog flow access token");
        String accessToken = "";
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        try{

            String dialogAuthFile= "{\n" +
                    "  \"type\": \"service_account\",\n" +
                    "  \"project_id\": \"lovappy-testing\",\n" +
                    "  \"private_key_id\": \"23c115e5e0616b6c03f1b4ac93885328e28ca487\",\n" +
                    "  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDtJUqlSb6sZiHw\\n0wK8tj9F9QLFbjXYexS4aSDFypDqvjLdbjW3RPtF7ZI+9+NquFwwQmNTZM+alMaO\\nPkmdtk9L41Qg8Fb7w3y6RjA0IRo7hKgIa270K1ZZFW9OVmOqtM1VH9Cx/3PiWOIK\\n7PQ7WhbD4pQYxbb/pfNEHMjYeZTwP3dUor+IRwmIC7ItDTlULT8HM9XxbqcTi0zu\\nfyNVVwKv4YDVIZGOo2k9kj6UK4BWNHaX08AX3/aOuCqs/lT1npnQ25L9QXqlPiLN\\neuPzYplZ8wQz6Wv42u3SFInvgrQTBGmOErcsjtumY7ZRN0cc1CZ/SAFKMbEbLIZc\\nEq20DLBVAgMBAAECggEAMrWstpeEXF0imNqmVTAra4wu4YTlorr+/UgNp/3e6zNs\\nsQjmF6B6ksNwaGKdkK6HmBI6JxUBh66UnsoW99CKOFIOJVT/ZKqyuypTGHKTp3KA\\nc/x1J2U0+6unt3qlqVUsKy24dLeJUjdrbqaouyBSieSjgoCU1ynnUQNOt2uYO58N\\nPLTWq5azvqbTHS4szrfSXzUl1ZYvHrtywUsgZ9qRb6yu7FnOWcwmBezyQjqTR5GU\\nJxa02pcEKvDh+i/nhSytvZ0s02rr3Wnj7BHBZfTNpMvSzJAzySrQt3SAOBdSu8es\\n7XyDu7r2cCQO6uG9Yz0l2Q1SFqLWuxs/x+KseY1eAQKBgQD9X044AfjqsTOMFoM1\\nNMskFrN+rbLv/kUfYuOOAnrWTZrh9eTSPkg6craZDKgtVaroV7A9s0grnaHv3YXF\\nDm6Stz5HVMuYxU+kiFfbo90lor5ISwaC+EXlBvNUcSp1UvGmBZsJgqFekg+JaHDU\\n0be7/v9GRSequriGEaX7y6FPQQKBgQDvmueqsy509rqYqVASbY5wTX3NCidoOVw3\\nwvXUeX8Fubz7lCUPVSIU/1dG/fr8J/FbPFLJW0dm8k9RJnZyzTUplWoaJHbrHKg2\\nVWaYX3u9tMSUTH+szeW0nLVKdCjq2JSrQK/GEIqfQYNup6hyRojLkhd/STJC6oTT\\n1/PUuLUwFQKBgFaH7AC4u7f7WK1pKtpK822ZyydiTxAjeJvle+2N70GtM04kWkdr\\nOeOsNaxDZFRHQ50iBXCs/+LVbO6savkHmr/EkSJvVTERgcehQNAkQGyQDghGhyzH\\neadkHkoic+uprhmTJIW09UOFAoVdA9vK28L6rVfkWk22gzpWYKeDvc2BAoGBAI3S\\nK4OvMTVMAOBxfxKZRDEWvPqLIw4evYXH4QgVreiuqrxiUJlFs7mn9XnwxQlRfxuY\\nlgvW+mllzSrkUEHdUrU9WiMFKFo7iRk37VUNV0nPYHfC2+Nlr6v7AZeDL+Dptv2y\\neRgMHHDtLyVlGWAifb+4xi2yHozbESnu7cUgWyaNAoGALIZfM/N7gOlru3A7Lk3E\\n0EZelTZQoh5EsqGHuGFIh81SmntO0Ao9m4ZbRL3pi5TV7NtCcxFDQ6K+n9PvtQld\\ndrOwiffrcVz0CoN3yON0lWGEje0+61GsOkNsOt2uJBvXRyJyJBQj1tZPerT9ioHg\\nHi0pjB0DjWLBexblw4kiQZA=\\n-----END PRIVATE KEY-----\\n\",\n" +
                    "  \"client_email\": \"dialogflow-rakscl@lovappy-testing.iam.gserviceaccount.com\",\n" +
                    "  \"client_id\": \"113342257377766020445\",\n" +
                    "  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" +
                    "  \"token_uri\": \"https://oauth2.googleapis.com/token\",\n" +
                    "  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" +
                    "  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/dialogflow-rakscl%40lovappy-testing.iam.gserviceaccount.com\"\n" +
                    "}\n";

            InputStream stream = new ByteArrayInputStream(dialogAuthFile.getBytes(StandardCharsets.UTF_8));
            GoogleCredentials credentials = GoogleCredentials.fromStream(stream);
            ArrayList<String> scopes = new ArrayList<>();
            scopes.add(dialogflowScope);
            credentials = credentials.createScoped(scopes);
            AccessToken token = credentials.refreshAccessToken();
            accessToken = token.getTokenValue();

        } catch (Exception e){
            e.printStackTrace();
            LOG.error(e.getMessage());
        }
        System.out.println(accessToken);
        return accessToken;
    }


}
