package com.realroofers.lovappy.web.controller.news;

import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.news.dto.NewsStoryAdminDto;
import com.realroofers.lovappy.web.util.CloudStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@Controller
@RequestMapping("/news")
public class NewsAdminController {

    @Autowired
    private CloudStorage cloudStorage;

    @Autowired
    private CloudStorageService cloudStorageService;


    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @GetMapping("/admin")
    public String viewAdmin(Model model) {
        return "admin/news/news";
    }

    @GetMapping("/file/{id}")
    @ResponseBody
    public ResponseEntity<String> getNewsFile(@PathVariable Long id) {
        CloudStorageFile file= cloudStorageService.findById(id);
        String link = cloudStorage.getFileLink(imagesBucket,file.getName());
        return new ResponseEntity<>(link, HttpStatus.OK);
    }





    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String submit(@ModelAttribute("story")NewsStoryAdminDto obj){

        System.out.println(obj);
        return "ss";
    }

}