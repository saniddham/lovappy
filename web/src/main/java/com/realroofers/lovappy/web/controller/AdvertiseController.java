package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.ads.AdsService;
import com.realroofers.lovappy.service.ads.dto.AdDto;
import com.realroofers.lovappy.service.ads.dto.AdTargetAreaDto;
import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import com.realroofers.lovappy.service.system.LanguageService;
import com.realroofers.lovappy.service.user.UserUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;

/**
 * Created by hasan on 28/6/2017.
 */
@Controller
@RequestMapping("/advertise")
public class AdvertiseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdvertiseController.class);

    private final LanguageService languageService;
    private final CMSService cmsService;
    private final AdsService adsService;

    @Value("${google.maps.api.key}")
    private String googleKeyId;

    @Value("${square.applicationId}")
    private String squareAppID;

    @Autowired
    public AdvertiseController(LanguageService languageService, CMSService cmsService, AdsService adsService) {
        this.languageService = languageService;
        this.cmsService = cmsService;
        this.adsService = adsService;
    }

    @GetMapping
    public String advertise(Model model) {
        Locale locale = LocaleContextHolder.getLocale();
        String language = locale.getLanguage();
        model.addAttribute("ad", new AdDto());
        model.addAttribute("languages", languageService.getAllLanguages(true));
        model.addAttribute("google_key", googleKeyId);
        model.addAttribute("squareAppID", squareAppID);

        model.addAttribute("adsTerms", cmsService.findPageByTagWithImagesAndTexts("ads-terms", language).getTexts());
        model.addAttribute("podcastTerms", cmsService.findPageByTagWithImagesAndTexts("podcast-terms", language).getTexts());
        model.addAttribute("newsTerms", cmsService.findPageByTagWithImagesAndTexts("news-terms", language).getTexts());
        model.addAttribute("musicTerms", cmsService.findPageByTagWithImagesAndTexts("music-terms", language).getTexts());
        model.addAttribute("comedyTerms", cmsService.findPageByTagWithImagesAndTexts("comedy-terms", language).getTexts());
        model.addAttribute("audioBookTerms", cmsService.findPageByTagWithImagesAndTexts("audiobook-terms", language).getTexts());

        Map<String, String> settings = cmsService.getPageSettings(PageConstants.ADVERTISE_WITH_US);
        model.addAttribute(PageConstants.ADS_AUDIO_STEP1, cmsService.getTextByTagAndLanguage(PageConstants.ADS_AUDIO_STEP1, language));
        model.addAttribute(PageConstants.ADS_TEXT_STEP1, cmsService.getTextByTagAndLanguage(PageConstants.ADS_TEXT_STEP1, language));
        model.addAttribute(PageConstants.ADS_VIDEO_STEP1, cmsService.getTextByTagAndLanguage(PageConstants.ADS_VIDEO_STEP1, language));


        model.addAttribute("text_limit", settings.get("text_limit"));
        model.addAttribute("audio_limit", settings.get("audio_limit"));
        model.addAttribute("video_limit", settings.get("video_limit"));

        Collection<AdTargetAreaDto> targetAreas = new ArrayList<>();
        model.addAttribute("targetAreas", targetAreas);
        model.addAttribute("loggedIn",
                UserUtil.INSTANCE.getCurrentUserId() != null);

        model.addAttribute("backgroundImages", adsService.getAllBackgroundImages());
        model.addAttribute("backgroundColors", adsService.getAllBackgroundColors());

        return "advertise/advertise";
    }

    @GetMapping("/landing")
    public ModelAndView adLanding(ModelAndView mav) {
        mav.setViewName("advertise/advertise-landing");
        return mav;
    }

    @GetMapping("/news")
    public ModelAndView newsLanding(ModelAndView mav) {
        mav.setViewName("advertise/news-landing");
        return mav;
    }

    @GetMapping("/podcast")
    public ModelAndView podcastLanding(ModelAndView mav) {
        mav.setViewName("advertise/podcast-landing");
        return mav;
    }

    @GetMapping("/comedy")
    public ModelAndView comedyLanding(ModelAndView mav) {
        mav.setViewName("advertise/comedy-landing");
        return mav;
    }

    @GetMapping("/audiobook")
    public ModelAndView audibookLanding(ModelAndView mav) {
        mav.setViewName("advertise/audiobook-landing");
        return mav;
    }

    @GetMapping("/music")
    public ModelAndView musicLanding(ModelAndView mav) {
        mav.setViewName("advertise/music-landing");
        return mav;
    }
}
