package com.realroofers.lovappy.web.rest.support;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.realroofers.lovappy.service.user.dto.AddressDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyEditorSupport;
import java.io.IOException;

/**
 * @author Eias Altawil
 */

public class AddressListConverter extends PropertyEditorSupport {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddressListConverter.class);

    public void setAsText(final String text) throws IllegalArgumentException {
        ObjectMapper mapper = new ObjectMapper();

        try {
            AddressDto[] addresses = mapper.readValue(text, AddressDto[].class);
            setValue(addresses);

        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }
}