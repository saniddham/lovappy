package com.realroofers.lovappy.web.exception;

/**
 * Created by Daoud Shaheen on 9/3/2017.
 */
public class BadRequestException extends RuntimeException {
    public BadRequestException(String message) {
        super(message);
    }

}
