package com.realroofers.lovappy.web.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @author Eias Altawil
 */

@Component
public class AddingHandlerInterceptor extends HandlerInterceptorAdapter {

    @Value("${spring.profiles.active}")
    String activeProfile;

    @Override
    public void postHandle(final HttpServletRequest request,
                           final HttpServletResponse response, final Object handler,
                           final ModelAndView modelAndView) throws Exception {

        if (modelAndView != null) {
            modelAndView.getModelMap().
                    addAttribute("activeProfile", activeProfile);
        }
    }
}
