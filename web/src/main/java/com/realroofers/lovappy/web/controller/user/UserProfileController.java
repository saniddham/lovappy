package com.realroofers.lovappy.web.controller.user;

import java.io.IOException;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.web.dto.IdDto;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@Controller
@RequestMapping("/user/profile")
public class UserProfileController {
    private UserProfileService userProfileService;
    private CloudStorage cloudStorage;

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @Autowired
    public UserProfileController(UserProfileService userProfileService, CloudStorage cloudStorage) {
        this.userProfileService = userProfileService;
        this.cloudStorage = cloudStorage;
    }

    @PostMapping("/hands")
    @ResponseBody
    public IdDto saveHandsPhoto(@RequestParam("file") MultipartFile multipartFile) throws IOException {
        CloudStorageFileDto cloudStorageFileDto =
                cloudStorage.uploadImageWithThumbnails(multipartFile, imagesBucket, FileExtension.jpg.toString(), "image/jpeg");
        userProfileService.saveHandsPhoto(cloudStorageFileDto);
        return new IdDto(null);
    }

    @PutMapping("/hands/skip")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void skipHandsPhoto() {
        userProfileService.skipHandsPhoto();
    }

    @PostMapping("/feet")
    @ResponseBody
    public IdDto saveFeetPhoto(@RequestParam("file") MultipartFile file) throws IOException {
        CloudStorageFileDto cloudStorageFileDto =
                cloudStorage.uploadImageWithThumbnails(file, imagesBucket, FileExtension.jpg.toString(), "image/jpeg");
        userProfileService.saveFeetPhoto(cloudStorageFileDto);
        return new IdDto(null);
    }

    @PutMapping("/feet/skip")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void skipFeetPhoto() {
        userProfileService.skipFeetPhoto();
    }

    @PostMapping("/legs")
    @ResponseBody
    public IdDto saveLegsPhoto(@RequestParam("file") MultipartFile file) throws IOException {
        CloudStorageFileDto cloudStorageFileDto =
                cloudStorage.uploadImageWithThumbnails(file, imagesBucket, FileExtension.jpg.toString(), "image/jpeg");
        userProfileService.saveLegsPhoto(cloudStorageFileDto);
        return new IdDto(null);
    }

    @PutMapping("/legs/skip")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void skipLegsPhoto() {
        userProfileService.skipLegsPhoto();
    }

    @PostMapping("/photo")
    @ResponseBody
    public IdDto saveProfilePhoto(@RequestParam("file") MultipartFile file) throws IOException {
        CloudStorageFileDto cloudStorageFileDto =
                cloudStorage.uploadImageWithThumbnails(file, imagesBucket, FileExtension.jpg.toString(), "image/jpeg");

        userProfileService.saveProfilePhoto(UserUtil.INSTANCE.getCurrentUserId(), cloudStorageFileDto);
        return new IdDto(null);
    }


}
