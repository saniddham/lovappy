package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.news.NewsCategoryService;
import com.realroofers.lovappy.service.news.dto.NewsCategoryDto;
import com.realroofers.lovappy.service.news.model.NewsCategory;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.web.util.Pager;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Optional;

/**
 * @author Eias Altawil
 */

@Controller
@RequestMapping("/lox/news")
public class NewsManagementController {

    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 20;
    private static final int[] PAGE_SIZES = {5, 10, 20};

    private final NewsCategoryService newsCategoryService;

    public NewsManagementController(NewsCategoryService newsCategoryService) {
        this.newsCategoryService = newsCategoryService;
    }

    @GetMapping
    public ModelAndView list(ModelAndView model) {
        model.setViewName("admin/news/news");
        return model;
    }

    @PostMapping("/category/save")
    public ResponseEntity saveGenre(@ModelAttribute("news") @Valid NewsCategoryDto news) {
        if(news.getCategoryId() == null){
            NewsCategory save = newsCategoryService.save(new NewsCategory(news.getCategoryName(),news.getCategoryDescription()));
            return ResponseEntity.status(HttpStatus.OK).body(save.getCategoryId());
        } else {
            NewsCategory newsCategory = newsCategoryService.getOne(news.getCategoryId());
            newsCategory.setStatus(news.getStatus());
            newsCategory.setCategoryDescription(news.getCategoryDescription());
            newsCategory.setCategoryName(news.getCategoryName());
            NewsCategory save = newsCategoryService.save(newsCategory);
            return ResponseEntity.status(HttpStatus.OK).body(save.getCategoryId());
        }
    }


    @RequestMapping("/category")
    public ModelAndView getNewsCategoryPage(@RequestParam("pageSize") Optional<Integer> pageSize,
                                            @RequestParam("page") Optional<Integer> page,
                                            @RequestParam(value = "searchText", defaultValue = "", required = false) String searchText) {

        ModelAndView modelAndView = new ModelAndView("admin/upload/news-category");

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        PageRequest pageable = new PageRequest(evalPage, evalPageSize);

        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        modelAndView.addObject("searchText", searchText);
        org.springframework.data.domain.Page<NewsCategoryDto> pages = newsCategoryService.findAll(pageable);
        Pager pager = new Pager(pages.getTotalPages(), pages.getNumber(), BUTTONS_TO_SHOW);
        modelAndView.addObject("pages", pages);

        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pageSizes", PAGE_SIZES);
        modelAndView.addObject("pager", pager);
        return modelAndView;
    }

    @GetMapping("/category/new")
    public ModelAndView addNewCategory() {
        ModelAndView mav = new ModelAndView("admin/upload/news_category_details");
        mav.addObject("news", new NewsCategoryDto());
        return mav;
    }

    @GetMapping("/category/{id}")
    public ModelAndView categoryDetails(@PathVariable(value = "id") Long id) {
        ModelAndView mav = new ModelAndView("admin/upload/news_category_details");
        NewsCategory one = newsCategoryService.getOne(id);
        mav.addObject("news", one!=null? new NewsCategoryDto(one): new NewsCategoryDto());
        return mav;
    }

}
