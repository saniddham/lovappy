package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.couples.CoupleProfileService;
import com.realroofers.lovappy.service.couples.dto.CouplesProfileDto;
import com.realroofers.lovappy.service.likes.LikeService;
import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampDto;
import com.realroofers.lovappy.service.music.MusicService;
import com.realroofers.lovappy.service.order.PriceService;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.privatemessage.PrivateMessageService;
import com.realroofers.lovappy.service.user.UserProfileAndPreferenceService;
import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.*;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.support.Gender;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@Controller
@RequestMapping("/user")
public class DashboardController {
    private static final Logger LOGGER = LoggerFactory.getLogger(DashboardController.class);
    private UserProfileAndPreferenceService userProfileAndPreferenceService;
    private UserProfileService userProfileService;
    private LovstampService lovstampService;


    private LikeService likeService;

    private UserService userService;
    private final MusicService musicService;
    private final CoupleProfileService coupleProfileService;
    private final PriceService priceService;
    private PrivateMessageService privateMessageService;

    public DashboardController(UserProfileService userProfileService, PrivateMessageService privateMessageService, UserService userService, UserProfileAndPreferenceService userProfileAndPreferenceService, LovstampService lovstampService, LikeService likeService, MusicService musicService, CoupleProfileService coupleProfileService, PriceService priceService) {
        this.userProfileAndPreferenceService = userProfileAndPreferenceService;
        this.lovstampService = lovstampService;
        this.likeService = likeService;
        this.userService = userService;
        this.userProfileService = userProfileService;
        this.privateMessageService = privateMessageService;
        this.musicService = musicService;
        this.coupleProfileService = coupleProfileService;
        this.priceService = priceService;
    }


    @GetMapping("/my-dashboard")
    public String myDashboard(Model model) {

        UserAuthDto userAuthDto = UserUtil.INSTANCE.getCurrentUser();
        UserDto user = userService.getUser(userAuthDto.getUserId());
        UserProfileDto userProfile = user.getUserProfile();
        LovstampDto lovdropDto = lovstampService.getLovstampByUserID(userAuthDto.getUserId());

        model.addAttribute("sinceDate", "User since " +  user.getCreatedOn());
        model.addAttribute("userId", userAuthDto.getUserId());

        SimpleDateFormat formatLastLogin = new SimpleDateFormat("MM/dd/yyyy");
        String formatted = formatLastLogin.format(user.getLastLogin()==null? new Date() : user.getLastLogin());
        model.addAttribute("lastLogin", "User last login " + formatted);

        Integer numberOfupdates = lovdropDto == null ? 0: lovdropDto.getNumberOfUpdates();
        String times = numberOfupdates == 1? "1 time." :  numberOfupdates + " times.";
        model.addAttribute("lovdropNumOfUpdates", times);
        model.addAttribute("lovdropNumOfListens", userProfile.getListenedToCount());
       //like statuses
        model.addAttribute("lovdropMaleLiked", "Liked: " + likeService.countAllLikeToByGender(userAuthDto.getUserId(), Gender.MALE));
        model.addAttribute("lovdropFemaleLiked", "Liked: " + likeService.countAllLikeToByGender(userAuthDto.getUserId(), Gender.FEMALE));
        model.addAttribute("lovdropMaleLikedBy", "Liked By: " + likeService.countAllLikeFromByGender(userAuthDto.getUserId(), Gender.MALE));
        model.addAttribute("lovdropFemaleLikedBy", "Liked By: " + likeService.countAllLikeFromByGender(userAuthDto.getUserId(), Gender.FEMALE));
        //listen statuses
        model.addAttribute("lovdropMaleListenedTo", "Listened To: " + userProfile.getListenedToMaleCount());
        model.addAttribute("lovdropFemaleListenedTo", "Listened To: " + userProfile.getListenedToFemaleCount());
        model.addAttribute("lovdropMaleListenedBy", "Listened By: " + (lovdropDto == null ? 0 : lovdropDto.getNumberOfMaleListeners()));
        model.addAttribute("lovdropFemaleListenedBy", "Listened By: " +( lovdropDto == null ? 0 : lovdropDto.getNumberOfFemaleListeners()));


        //music statuses\
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int i = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
        c.add(Calendar.DATE, -i - 7);
        Date start = c.getTime();

        model.addAttribute("maleMusicSending", musicService.countSendingMusicByFromUserAndGenderAndSendDateBetween(userAuthDto.getUserId(), Gender.MALE, start,date));
        model.addAttribute("femaleMusicSending",  musicService.countSendingMusicByFromUserAndGenderAndSendDateBetween(userAuthDto.getUserId(), Gender.FEMALE, start ,date));
        model.addAttribute("newArtistsCount",  userService.countArtistByDateBetween(start, date));
        model.addAttribute("newSongsCount",  musicService.countSongsByDateBetween(start, date));


        WithinRadiusDto searchRadius = new WithinRadiusDto();
        double latitude = 0.0;
        double longitude = 0.0;
        if(userProfile.getAddress()!=null) {
            latitude = userProfile.getAddress().getLatitude() == null ? 0.0 : userProfile.getAddress().getLatitude();
            longitude = userProfile.getAddress().getLongitude() == null ? 0.0 : userProfile.getAddress().getLongitude();
        }
        searchRadius.setLatitude(latitude);
        searchRadius.setLongitude(longitude);
        searchRadius.setRadius(300);
        searchRadius.setPrefGender(Gender.MALE);
        long malesNearBy =  userProfileService.countNearbyUsers(userAuthDto.getUserId(), searchRadius);
        model.addAttribute("malesNearBy", malesNearBy == 0 ? "no" : malesNearBy);
        searchRadius.setPrefGender(Gender.FEMALE);

        long femalesNearBy =  userProfileService.countNearbyUsers(userAuthDto.getUserId(), searchRadius);
        model.addAttribute("femalesNearBy", femalesNearBy == 0 ? "no" : femalesNearBy);

        User mostExchangeUser = userService.getUserMostExchangeMessagesWithByUserId(user.getID());
        if(mostExchangeUser != null) {
            UserProfile mostExchangeUserProfile = mostExchangeUser.getUserProfile();

            if(mostExchangeUserProfile != null) {
                LocalDate birthdate = new LocalDate(mostExchangeUserProfile.getBirthDate());
                LocalDate now = new LocalDate();
                Years age = Years.yearsBetween(birthdate, now);

            model.addAttribute("exchangeMsgsImage", mostExchangeUserProfile.getGender().equals(Gender.MALE) ? "/images/icons/male.png":"/images/icons/female.png");
            model.addAttribute("numOfExchangedMsgs", age.getYears());
            if(mostExchangeUserProfile.getAddress() !=null)
            model.addAttribute("userAddress", mostExchangeUserProfile.getAddress().getStateShort() == null? "" : mostExchangeUserProfile.getAddress().getStateShort());
            }
        } else {
            model.addAttribute("numOfExchangedMsgs", "no one");
        }
        model.addAttribute("avgDistanceOfViewers", new DecimalFormat("##.##").format(user.getUserProfile().getAvgDistanceOfViewers()/1000.0) + " km");

        return "user/my_dashboard";
    }


}
