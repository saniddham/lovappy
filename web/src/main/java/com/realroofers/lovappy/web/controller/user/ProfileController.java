package com.realroofers.lovappy.web.controller.user;

import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.ProfileDto;
import com.realroofers.lovappy.service.user.dto.UserPreferenceDto;
import com.realroofers.lovappy.service.validation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daoud Shaheen on 7/26/2018.
 */
@Controller
public class ProfileController {


    @Autowired
    private UserService userService;

    @PostMapping("/profile/save")
    public ResponseEntity saveProfile(@ModelAttribute("user") ProfileDto dto) {
        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();

        //TODO Add validation

        if (errorMessages.size() > 0) {
            res.setErrorMessageList(errorMessages);
            res.setStatus(com.realroofers.lovappy.service.validation.ResponseStatus.FAIL);
            return ResponseEntity.status(HttpStatus.OK).body(res);
        } else {
            userService.updateProfile(UserUtil.INSTANCE.getCurrentUserId(), dto.getEmail(), dto.getFirstname(), dto.getLastname());
            res.setStatus(com.realroofers.lovappy.service.validation.ResponseStatus.SUCCESS);
            return ResponseEntity.ok().body(res);
        }
    }
    @GetMapping("/musician/settings")
    public ModelAndView musicianSettings()  {

        return new ModelAndView("user/profiles/musician-profile");
    }
    @GetMapping("/musician/profile")
    public ModelAndView musicianProfie()  {

        return new ModelAndView("user/profiles/musician-profile");
    }
    @GetMapping("/ambassador/settings")
    public ModelAndView ambassadorSettings()  {

        return new ModelAndView("user/profiles/ambassador-profile");
    }
    @GetMapping("/ambassador/profile")
    public ModelAndView ambassadorProfie()  {

        return new ModelAndView("user/profiles/ambassador-profile");
    }

    @GetMapping("/blogger/settings")
    public ModelAndView bloggerSettings()  {

        return new ModelAndView("user/profiles/blogger-profile");
    }
    @GetMapping("/blogger/profile")
    public ModelAndView bloggerProfie()  {

        return new ModelAndView("user/profiles/blogger-profile");
    }
}
