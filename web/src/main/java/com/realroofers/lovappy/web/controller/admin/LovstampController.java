package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.lovstamps.LovstampSampleService;
import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampDto;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampSampleDto;
import com.realroofers.lovappy.service.lovstamps.support.SampleTypes;
import com.realroofers.lovappy.service.radio.*;
import com.realroofers.lovappy.service.radio.dto.RadioProgramControlDto;
import com.realroofers.lovappy.service.radio.dto.StartSoundDto;
import com.realroofers.lovappy.service.radio.support.AudioAction;
import com.realroofers.lovappy.service.radio.support.AudioInsertWhere;
import com.realroofers.lovappy.service.radio.support.StartSoundType;
import com.realroofers.lovappy.service.system.LanguageService;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.FileUtil;
import com.realroofers.lovappy.web.util.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

@Controller
@RequestMapping("/lox/lovstamp")
public class LovstampController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LovstampController.class);
    private final LovstampService lovstampService;
    private final CloudStorage cloudStorage;
    private final RadioProgramControlService radioProgramControlService;
    private final MediaTypeService mediaTypeService;
    private final AudioTypeService audioTypeService;
    private final AudioSubTypeService audioSubTypeService;
    private final LanguageService languageService;
    private final RadioService radioService;
    private final FileUtil fileUtil;
    private final StartSoundService startSoundService;
    private final LovstampSampleService lovstampSampleService;

    @Value("${lovappy.cloud.bucket.radio.startSounds}")
    private String startSoundsBucket;

    @Value("${lovappy.cloud.bucket.lovdropSamples}")
    private String recordingSamplesBucket;

    @Autowired
    public LovstampController(LovstampService lovstampService, CloudStorage cloudStorage, RadioProgramControlService radioProgramControlService, MediaTypeService mediaTypeService, AudioTypeService audioTypeService, AudioSubTypeService audioSubTypeService, LanguageService languageService, RadioService radioService, FileUtil fileUtil, StartSoundService startSoundService, LovstampSampleService lovstampSampleService) {
        this.lovstampService = lovstampService;
        this.cloudStorage = cloudStorage;
        this.radioProgramControlService = radioProgramControlService;
        this.mediaTypeService = mediaTypeService;
        this.audioTypeService = audioTypeService;
        this.audioSubTypeService = audioSubTypeService;
        this.languageService = languageService;
        this.radioService = radioService;
        this.fileUtil = fileUtil;
        this.startSoundService = startSoundService;
        this.lovstampSampleService = lovstampSampleService;
    }


    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 10;
    private static final int[] PAGE_SIZES = {5, 10, 20};

    @GetMapping()
    public ModelAndView manageLovdstmps(@RequestParam("pageSize") Optional<Integer> pageSize,
                                        @RequestParam("page") Optional<Integer> page,
                                        @RequestParam(value = "searchText", defaultValue = "", required = false) String searchText,
                                        @RequestParam(value = "filter", defaultValue = "user.userId") String filter,
                                        @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort) {
        ModelAndView modelAndView = new ModelAndView("admin/lovstamp/radio-list");

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        PageRequest pageable = new PageRequest(evalPage, evalPageSize);


        if (!StringUtils.isEmpty(filter)) {
            if (pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(evalPage, evalPageSize, new Sort(sort, filter));
            }
        }

        modelAndView.addObject("filter", filter);
        modelAndView.addObject("sort", sort.equals(Sort.Direction.ASC) ? Sort.Direction.DESC : Sort.Direction.ASC);

        Page<LovstampDto> radios = lovstampService.getLovstamps(pageable);
        Pager pager = new Pager(radios.getTotalPages(), radios.getNumber(), BUTTONS_TO_SHOW);

        modelAndView.addObject("radios", radios);

        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pageSizes", PAGE_SIZES);
        modelAndView.addObject("pager", pager);

        return modelAndView;
    }


    @GetMapping("/start-sounds")
    public ModelAndView startSounds(ModelAndView model) {

        for (StartSoundDto sound : startSoundService.getStartSounds()) {
            model.addObject(sound.getSoundType().name(), sound);
        }

        model.setViewName("admin/lovstamp/start-sounds");
        return model;
    }


    @GetMapping("/default-sounds")
    public ModelAndView defaultSounds(ModelAndView model) {

        for (LovstampSampleDto sound : lovstampSampleService.getAllRecordingSamplesAndType(SampleTypes.DEFAULT, new PageRequest(0,2))) {
            model.addObject(sound.getGender().name(), sound);
        }

        model.setViewName("admin/lovstamp/default-sounds");
        return model;
    }

    @GetMapping("/radio-controls")
    public ModelAndView radioControls(ModelAndView model) {
        model.addObject("languages", languageService.getAllLanguages(true));
        model.addObject("mediaTypes", mediaTypeService.findAllActive());
        model.addObject("audioTypes", audioTypeService.findAllActive());
        model.addObject("audioSubTypes", audioSubTypeService.findAllActive());
        model.addObject("actions", AudioAction.values());
        model.addObject("insertTypes", AudioInsertWhere.values());
        model.addObject("radioControl", new RadioProgramControlDto());

        model.setViewName("admin/lovstamp/radio-controls");
        return model;
    }

    @PostMapping(value = {"/radio-controls"})
    public ModelAndView updateRadioControls(@ModelAttribute RadioProgramControlDto radioControl) {
        try {
            radioProgramControlService.saveRadioControl(radioControl);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new ModelAndView("redirect:/lox/lovstamp/radio-controls");
    }

    @PostMapping("/start-sounds/save")
    public ResponseEntity<Void> uploadStartSound(@RequestParam("sound") MultipartFile sound,
                                                      @RequestParam("type") StartSoundType type) {
        //throw new ValidationException(errorMessages);
        try {
            CloudStorageFileDto cloudStorageFile =
                    cloudStorage.uploadFile(sound, startSoundsBucket, FileExtension.mp3.toString(), "audio/mp3");

            StartSoundDto soundDto = new StartSoundDto(cloudStorageFile, type);
            startSoundService.addStartSound(soundDto);

            return ResponseEntity.ok().build();

        } catch (IOException ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @PostMapping("/default-sounds/save")
    @ResponseBody
    public CloudStorageFileDto uploadDefaultSound(@RequestParam("sound") MultipartFile sound,
                                                 @RequestParam("type") Gender gender,
                                                   @RequestParam(value = "lang", defaultValue = "EN") String language) {
        //throw new ValidationException(errorMessages);
        try {
            CloudStorageFileDto cloudStorageFile =
                    cloudStorage.uploadAndPersistFile(sound, recordingSamplesBucket, FileExtension.mp3.toString(), "audio/mp3");


           lovstampSampleService.saveDefaultSample(fileUtil.getAudioDuration(sound), cloudStorageFile.getId(), gender, language);

            return cloudStorageFile;

        } catch (IOException ex) {
            LOGGER.info("error {}", ex);
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return null;

    }
    @PostMapping("/{radio-id}/home")
    public ResponseEntity showInHome(HttpServletRequest request, @PathVariable("radio-id") Integer id) {
        lovstampService.showLovstampInHomePage(id, true);
        return ResponseEntity.status(HttpStatus.OK).body("");
    }

    @DeleteMapping("/{radio-id}/home")
    public ResponseEntity hideFromHome(HttpServletRequest request, @PathVariable("radio-id") Integer id) {
        lovstampService.showLovstampInHomePage(id, false);
        return ResponseEntity.status(HttpStatus.OK).body("");
    }
}
