package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.event.EventService;
import com.realroofers.lovappy.service.event.EventSupportService;
import com.realroofers.lovappy.service.event.dto.EventCategoryDto;
import com.realroofers.lovappy.service.event.dto.EventDto;
import com.realroofers.lovappy.service.event.dto.EventLocationDto;
import com.realroofers.lovappy.service.event.dto.EventLocationPictureDto;
import com.realroofers.lovappy.service.event.support.EventDateUtil;
import com.realroofers.lovappy.service.exception.EntityValidationException;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.system.LanguageService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.*;

/**
 * @author Eias Altawil
 */

@Controller
@RequestMapping("/lox/events")
public class EventsManagementController {

    private static final Logger LOG = LoggerFactory.getLogger(EventsManagementController.class);
    private final EventService eventService;
    private final EventSupportService eventSupportService;

    private final EmailTemplateService emailTemplateService;
    private final MailService mailService;
    private final ITemplateEngine templateEngine;
    private final UserService userService;
    private final CloudStorage cloudStorage;
    private final CloudStorageService cloudStorageService;

    private static final String GOOGLE_KEY = "google_key";
    private final LanguageService languageService;

    @Value("${google.maps.api.key}")
    private String googleKeyId;

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @Autowired
    public EventsManagementController(EventService eventService, EventSupportService eventSupportService, EmailTemplateService emailTemplateService, MailService mailService, ITemplateEngine templateEngine, UserService userService, CloudStorage cloudStorage, CloudStorageService cloudStorageService, LanguageService languageService) {
        this.eventService = eventService;
        this.eventSupportService = eventSupportService;
        this.emailTemplateService = emailTemplateService;
        this.mailService = mailService;
        this.templateEngine = templateEngine;
        this.userService = userService;
        this.cloudStorage = cloudStorage;
        this.cloudStorageService = cloudStorageService;
        this.languageService = languageService;
    }

    @GetMapping
    public ModelAndView list(ModelAndView model, @RequestParam(value = "page", defaultValue = "1") Integer page) {

        if (page < 1) {
            throw new RuntimeException("Invalid Page Number");
        }
        Page<EventDto> eventsPage = eventService.getAllEvents(new PageRequest(page - 1, 10));

        model.addObject("events", eventsPage);
        model.addObject("currentPage", page);
        model.addObject(GOOGLE_KEY, googleKeyId);
        model.addObject("eventCreate", new EventDto());
        model.addObject("categories", eventSupportService.getEventCategories());
        model.addObject("languages", languageService.getAllLanguages(true));
        model.addObject("actionUrl", "/lox/events?");
        model.setViewName("admin/event/events_list_admin");
        return model;
    }

    @GetMapping("/{id}")
    public ModelAndView eventDetails(ModelAndView model, @PathVariable(value = "id") Integer id) {

        EventDto eventDto = eventService.findEventById(id);
        if (eventDto == null) {
            model.setViewName("error/404");
            return model;
        }
        model.addObject("event", eventDto);
        model.addObject("paidAttendees", eventService.getNoOfPaidAttendees(id));

        model.setViewName("admin/event/event_details_admin");
        return model;
    }

    @GetMapping("/categories")
    public ModelAndView categories(ModelAndView modelAndView) {
        modelAndView.addObject("categories", eventSupportService.getEventCategories());
        modelAndView.setViewName("admin/event/event_categories");
        return modelAndView;
    }

    @GetMapping("/categories/new")
    public ModelAndView addNewCategory() {
        ModelAndView mav = new ModelAndView("admin/event/event_category_details");
        mav.addObject("category", new EventCategoryDto());
        return mav;
    }

    @GetMapping("/categories/{id}")
    public ModelAndView categoryDetails(@PathVariable(value = "id") Integer id) {
        ModelAndView mav = new ModelAndView("admin/event/event_category_details");
        mav.addObject("category", eventSupportService.getCategory(id));
        return mav;
    }

    @PostMapping("/categories/save")
    public ResponseEntity saveCategory(@ModelAttribute("category") @Valid EventCategoryDto category) {
        if (category.getId() == null) {
            EventCategoryDto addCategory = eventSupportService.addCategory(category);
            return ResponseEntity.status(HttpStatus.OK).body(addCategory.getId());
        } else {
            EventCategoryDto updateCategory = eventSupportService.updateCategory(category);
            return ResponseEntity.status(HttpStatus.OK).body(updateCategory.getId());
        }
    }

    @GetMapping("/activate/{id}")
    public ModelAndView approveEvent(HttpServletRequest request, ModelAndView modelAndView, @PathVariable("id") Integer eventId) {

        modelAndView.setViewName("redirect:/lox/events/" + eventId);

        boolean isApproved = eventService.changeEventApprovalStatus(eventId, ApprovalStatus.APPROVED);

        if (!isApproved) {
            EventDto eventDto = eventService.findEventById(eventId);

            //send Approval email
            EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.SUBMITTED_EVENT_APPROVAL, "EN");
            emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/events/" + eventDto.getEventId());
            emailTemplate.getAdditionalInformation().put("title", eventDto.getEventTitle());
            if (eventDto.getAmbassadorUserId() != null) {
                UserDto ambassadorDTO = userService.getUser(eventDto.getAmbassadorUserId());
                new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), ambassadorDTO.getEmail(), templateEngine,
                        mailService, emailTemplate).send();
            } else {
                new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), userService.getUser(1).getEmail(), templateEngine,
                        mailService, emailTemplate).send();
            }

        }
        return modelAndView;
    }

    @GetMapping("/reject/{id}")
    public ModelAndView cancelEvent(HttpServletRequest request, ModelAndView modelAndView, @PathVariable("id") Integer eventId) {

        modelAndView.setViewName("redirect:/lox/events/" + eventId);
        EventDto eventDto = eventService.findEventById(eventId);
        if (eventDto != null && !eventDto.getApproved().equals(ApprovalStatus.REJECTED.name())) {
            eventService.changeEventApprovalStatus(eventId, ApprovalStatus.REJECTED);
            UserDto ambassadorDTO = userService.getUser(eventDto.getAmbassadorUserId());

            //send denial email
            EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.SUBMITTED_EVENT_DENIAL, "EN");
            emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/events/" + eventDto.getEventId());
            emailTemplate.getAdditionalInformation().put("title", eventDto.getEventTitle());
            new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), ambassadorDTO.getEmail(), templateEngine,
                    mailService, emailTemplate).send();
        }
        return modelAndView;
    }

    @GetMapping("/delete/{id}")
    public ModelAndView deleteEvent(ModelAndView modelAndView, @PathVariable("id") Integer eventId) {

        eventService.deleteEvent(eventId);
        modelAndView.setViewName("redirect:/lox/events");
        return modelAndView;

    }

    @PostMapping(value = "/create/location")
    public ResponseEntity<Void> addTitleAndLocation(@RequestParam("eventTitle") String eventTitle,
                                                    @RequestParam("eventLocation.longitude") Double longitude,
                                                    @RequestParam("eventLocation.floorRoom") String floorRoom,
                                                    @RequestParam("eventLocation.latitude") Double latitude) {
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (latitude == null || longitude == null)
            errorMessages.add(new ErrorMessage("locationValidation", "Select the event location"));

        if (StringUtils.isEmpty(eventTitle))
            errorMessages.add(new ErrorMessage("eventTitle", "Provide Title for the Event"));

        if (errorMessages.size() > 0)
            throw new EntityValidationException(errorMessages);

        return ResponseEntity.ok().build();
    }

    @PostMapping("/create/upload_location-pictures")
    public ResponseEntity uploadCoverImage(@RequestParam("event_location_picture") MultipartFile picture) {
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (picture.isEmpty())
            errorMessages.add(new ErrorMessage("cover", "Add a location picture."));

        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        try {
            CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadFile(picture, imagesBucket, FileExtension.png.toString(), "image/png");
            CloudStorageFileDto added = cloudStorageService.add(cloudStorageFileDto);
            return ResponseEntity.ok(added);

        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/create/others")
    public ResponseEntity<Void> addDescAndCategories(@RequestParam("eventDescription") String eventDescription,
                                                     @RequestParam("eventCategories") String eventCategories,
                                                     @RequestParam("locationPictures[0].picture.id") Double locationPicture) {

        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (StringUtils.isEmpty(eventDescription)) {
            errorMessages.add(new ErrorMessage("eventDescription", "Provide Description for Event"));
        }
        if (StringUtils.isEmpty(eventCategories)) {
            errorMessages.add(new ErrorMessage("eventCategories", "Please Select at least one Category"));
        }
        if (locationPicture == null) {
            errorMessages.add(new ErrorMessage("locationPicture", "Please upload location picture"));
        }

        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        return ResponseEntity.ok().build();
    }

    @PostMapping("/create/date")
    public ResponseEntity<Void> addDate(
            @RequestParam("eventTitle") String eventTitle,
            @RequestParam("eventLocation.longitude") Double longitude,
            @RequestParam("eventLocation.latitude") Double latitude,
            @RequestParam("eventLocation.zipCode") String zipCode,
            @RequestParam("eventLocation.floorRoom") String floorRoom,
            @RequestParam("eventLocation.country") String country,
            @RequestParam("eventLocation.state") String state,
            @RequestParam("eventLocation.province") String province,
            @RequestParam("eventLocation.city") String city,
            @RequestParam("eventLocation.streetNumber") String streetNumber,
            @RequestParam("eventLocation.stateShort") String stateShort,
            @RequestParam("eventLocation.fullAddress") String fullAddress,
            @RequestParam("eventLocation.premise") String premise,
            @RequestParam("eventLocation.route") String route,
            @RequestParam("eventDescription") String eventDescription,
            @RequestParam("eventCategories") String eventCategories,
            @RequestParam("admissionCost") Double admissionCost,
            @RequestParam("eventDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date eventDate,
            @RequestParam("timeZone") String timeZone,
            @RequestParam("finishTime") String finishTime,
            @RequestParam("startTime") String startTime,
            @RequestParam("outsiderCount") Integer outsiderCount,
            @RequestParam("locationPictures[0].picture.id") Long pictureId,
            @RequestParam(value = "targetedAudience.maleOrFemale", required = false) String gender,
            @RequestParam(value = "targetedAudience.ageRange", required = false) String ageRange,
            @RequestParam(value = "targetedAudience.languageSpoken", required = false) String languageSpoken,
            @RequestParam(value = "targetedAudience.catORdog", required = false) String catORdog,
            @RequestParam(value = "targetedAudience.drinker", required = false) String drinker,
            @RequestParam(value = "targetedAudience.smoker", required = false) String smoker,
            @RequestParam(value = "targetedAudience.user", required = false) String user,
            @RequestParam(value = "targetedAudience.no-habit", required = false) String nohabit,
            @RequestParam(value = "targetedAudience.ruralORurbanORsuburban", required = false) String ruralORurbanORsuburban,
            @RequestParam(value = "targetedAudience.politicalopinion", required = false) String politicalopinion,
            @RequestParam(value = "targetedAudience.outdoorsyORindoorsy", required = false) String outdoorsyORindoorsy,
            @RequestParam(value = "targetedAudience.frugalORbigspender", required = false) String frugalORbigspender,
            @RequestParam(value = "targetedAudience.morningpersonORnightowl", required = false) String morningpersonORnightowl,
            @RequestParam(value = "targetedAudience.extrovertORintrovert", required = false) String extrovertORintrovert) {

        List<ErrorMessage> errorMessages = new ArrayList<>();
        Double defaultCost = 10.0;

        if (eventDate == null || eventDate.compareTo(EventDateUtil.getDateOnly()) < 0) {
            errorMessages.add(new ErrorMessage("eventDate", "Select a valid date for the Event"));
        }
        if (StringUtils.isEmpty(startTime)) {
            errorMessages.add(new ErrorMessage("startTime", "Select start time"));
        }
        if (StringUtils.isEmpty(finishTime)) {
            errorMessages.add(new ErrorMessage("finishTime", "Select end time"));
        }
        if (!StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(finishTime)) {
            if (validateTimes(startTime, finishTime))
                errorMessages.add(new ErrorMessage("validTime", "Select start time before end time"));
        }
        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        if (admissionCost == null) {
            admissionCost = defaultCost;
        } else {

            admissionCost = defaultCost + admissionCost;
        }

        if (outsiderCount == null)
            outsiderCount = 0;

        Map<String, String> targetedAudience = new HashMap<>();
        if (gender != null) targetedAudience.put("maleOrFemale", gender);
        if (ageRange != null) targetedAudience.put("ageRange", ageRange);
        if (languageSpoken != null && !languageSpoken.equalsIgnoreCase("Select"))
            targetedAudience.put("languageSpoken", languageSpoken);
        if (catORdog != null) targetedAudience.put("catORdog", catORdog);
        if (drinker != null) targetedAudience.put("drinker", drinker);
        if (smoker != null) targetedAudience.put("smoker", smoker);
        if (user != null) targetedAudience.put("user", user);
        if (nohabit != null) targetedAudience.put("nohabit", nohabit);
        if (ruralORurbanORsuburban != null) targetedAudience.put("ruralORurbanORsuburban", ruralORurbanORsuburban);
        if (politicalopinion != null) targetedAudience.put("politicalopinion", politicalopinion);
        if (outdoorsyORindoorsy != null) targetedAudience.put("outdoorsyORindoorsy", outdoorsyORindoorsy);
        if (frugalORbigspender != null) targetedAudience.put("frugalORbigspender", frugalORbigspender);
        if (morningpersonORnightowl != null) targetedAudience.put("morningpersonORnightowl", morningpersonORnightowl);
        if (extrovertORintrovert != null) targetedAudience.put("extrovertORintrovert", extrovertORintrovert);


        EventLocationDto eventLocationDto = new EventLocationDto(zipCode, country, state, stateShort, province, city, streetNumber, fullAddress, latitude, longitude, premise, route, floorRoom);
        EventDto eventDto = new EventDto(eventTitle, eventDescription, eventDate, startTime, finishTime, outsiderCount, admissionCost, eventCategories);
        eventDto.setTimeZone(timeZone);
        Integer userId = null;

        if (UserUtil.INSTANCE.getCurrentUserId() != null) {
            userId = UserUtil.INSTANCE.getCurrentUserId();
        } else {
            User userNew = null;
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
                userNew = (User) authentication.getDetails();
            }
            userId = userNew.getUserId();
        }

        eventDto.setAmbassadorUserId(userId);
        eventDto.setEventLocation(eventLocationDto);
        eventDto.setLocationPictures(getPictureList(pictureId));
        eventDto.setTargetedAudience(targetedAudience);
        EventDto savedDto = eventService.addEvent(eventDto);

        if (savedDto.getEventId() != null) {
            return ResponseEntity.ok().build();
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private boolean validateTimes(String startTime, String finishTime) {

        return EventDateUtil.convertToDate(startTime).after(EventDateUtil.convertToDate(finishTime));
    }

    private List<EventLocationPictureDto> getPictureList(Long pictureId) {

        CloudStorageFileDto pic = new CloudStorageFileDto();
        pic.setId(pictureId);
        EventLocationPictureDto picDto = new EventLocationPictureDto();
        picDto.setPicture(pic);
        List<EventLocationPictureDto> list = new ArrayList<>();
        list.add(picDto);
        return list;
    }
}
