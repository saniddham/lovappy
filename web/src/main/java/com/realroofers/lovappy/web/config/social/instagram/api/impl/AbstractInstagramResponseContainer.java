package com.realroofers.lovappy.web.config.social.instagram.api.impl;

abstract class AbstractInstagramResponseContainer<C> {

    private C object;
    
    AbstractInstagramResponseContainer(C object) {
        this.object = object;
    }

    AbstractInstagramResponseContainer() {

    }
    public C getObject() {
        return object;
    }
    public void setObject(C object) {
        this.object = object;
    }
}
