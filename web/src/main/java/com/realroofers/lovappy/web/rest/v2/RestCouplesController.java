package com.realroofers.lovappy.web.rest.v2;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.couples.CoupleProfileService;
import com.realroofers.lovappy.service.couples.dto.CoupleProfileRequest;
import com.realroofers.lovappy.service.couples.dto.CouplesProfileDto;
import com.realroofers.lovappy.service.exception.EntityValidationException;
import com.realroofers.lovappy.service.exception.GenericServiceException;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.util.DateUtils;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.FormValidator;
import com.realroofers.lovappy.service.validator.UserRegister;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.exception.BadRequestException;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import java.util.List;

/**
 * Created by Daoud Shaheen on 7/29/2018.
 */
@RestController
@Slf4j
@RequestMapping({"/api/v2/couples", "/api/v1/couples"})
public class RestCouplesController {
    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;
    private static final Logger LOGGER = LoggerFactory.getLogger(RestCouplesController.class);
    private final CoupleProfileService coupleProfileService;
    private final CloudStorage cloudStorage;
    private final EmailTemplateService emailTemplateService;
    private final MailService mailService;
    private final ITemplateEngine templateEngine;
    private final FormValidator formValidator;
    private final UserService userService;
    public RestCouplesController(CoupleProfileService coupleProfileService, CloudStorage cloudStorage, EmailTemplateService emailTemplateService, MailService mailService, ITemplateEngine templateEngine, FormValidator formValidator, UserService userService) {
        this.coupleProfileService = coupleProfileService;
        this.cloudStorage = cloudStorage;
        this.emailTemplateService = emailTemplateService;
        this.mailService = mailService;
        this.templateEngine = templateEngine;
        this.formValidator = formValidator;
        this.userService = userService;
    }


    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public CouplesProfileDto signup(@RequestBody CoupleProfileRequest coupleProfileRequest) {

        List<ErrorMessage> errorMessages = formValidator.validate(coupleProfileRequest, new Class[] { UserRegister.class });

        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }

        return coupleProfileService.signup(coupleProfileRequest, UserUtil.INSTANCE.getCurrentUserId());
    }
    @RequestMapping(method = RequestMethod.GET)
    public CouplesProfileDto get() {

        return coupleProfileService.getCoupleProfile(UserUtil.INSTANCE.getCurrentUserId());
    }
    @RequestMapping(method = RequestMethod.PUT)
    public CouplesProfileDto update(@RequestBody CoupleProfileRequest coupleProfileRequest) {

        return coupleProfileService.update(coupleProfileRequest, UserUtil.INSTANCE.getCurrentUserId());
    }
    @RequestMapping(value = "/photo/upload", method = RequestMethod.POST)
    public ResponseEntity<CloudStorageFileDto> uploadImage(@RequestParam("coupleImage") MultipartFile coupleImage) {
        CloudStorageFileDto uploadedImage = null;

        if (coupleImage != null && !coupleImage.isEmpty()) {
            try {
                uploadedImage = cloudStorage.uploadAndPersistFile(coupleImage, imagesBucket,
                        FilenameUtils.getExtension(coupleImage.getOriginalFilename()), coupleImage.getContentType());
                coupleProfileService.uploadPhoto(uploadedImage, UserUtil.INSTANCE.getCurrentUserId());
            } catch (IOException e) {
                throw new GenericServiceException("Failed to upload Image");
            }
        }
        return ResponseEntity.ok().body(uploadedImage);
    }

    @RequestMapping(value = "/partner/email", method = RequestMethod.POST)
    public CouplesProfileDto registerPartner(@RequestParam("partnerEmail") String  partnerEmail, HttpServletRequest request) {
        if(!CommonUtils.isValidEmail(partnerEmail)) {
            throw new BadRequestException("Invalid Email");
        }

        CouplesProfileDto couplesProfileDto = coupleProfileService.getCoupleProfile(UserUtil.INSTANCE.getCurrentUserId());

        UserDto userDto = userService.getUser(partnerEmail);
        if(userDto == null) {

            userDto = coupleProfileService.registerPartner(UserUtil.INSTANCE.getCurrentUserId(), partnerEmail);

        }else{
            if (partnerEmail.equals(couplesProfileDto.getPartnerEmail()) &&
                    (couplesProfileDto.getStatus() == ApprovalStatus.APPROVED ||
                            DateUtils.daysAfter(couplesProfileDto.getLastUpdated(), new Date(), 1))) {
                return couplesProfileDto;

            } else {
                userDto = coupleProfileService.registerPartner(UserUtil.INSTANCE.getCurrentUserId(), partnerEmail);

            }
        }

        EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.COUPLE_APPROVAL, "EN");
        String verifyToken = null;
        if (userDto != null) {
            if (userDto.getPassword() == null) {
                verifyToken = new String(Base64.getEncoder().encode((UserUtil.INSTANCE.getCurrentUserId() + "_" + userDto.getEmail()).getBytes()));
            } else {
                verifyToken = new String(Base64.getEncoder().encode(("new_" + UserUtil.INSTANCE.getCurrentUserId() + "_" + userDto.getEmail()).getBytes()));
            }
            String url = CommonUtils.getBaseURL(request) + "/couples/verify/" + verifyToken;
            if (emailTemplate != null) {
                emailTemplate.getAdditionalInformation().put("url", url);
                emailTemplate.setBody(emailTemplate.getBody().replace("{{partnerEmail}}", UserUtil.INSTANCE.getCurrentUser().getEmail()));
                new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), partnerEmail, templateEngine,
                        mailService, emailTemplate).send();
            }
            couplesProfileDto.setPartnerEmail(partnerEmail);
            couplesProfileDto.setPartnerId(userDto.getID());
        }


        return couplesProfileDto;
    }
}



