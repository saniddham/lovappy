package com.realroofers.lovappy.web.config;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import com.realroofers.lovappy.service.couples.CoupleProfileService;
import com.realroofers.lovappy.service.couples.dto.CouplesProfileDto;
import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.survey.UserSurveyService;
import com.realroofers.lovappy.service.user.UserPreferenceService;
import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.MemberProfileDto;
import com.realroofers.lovappy.service.user.dto.UserAuthDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.dto.UserPreferenceDto;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.vendor.dto.VendorDto;
import com.realroofers.lovappy.service.vendor.model.VendorType;
import com.realroofers.lovappy.web.util.DatingPlacesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public class AuthenticationSuccessHandlerAndRegistrationFilter extends OncePerRequestFilter
        implements AuthenticationSuccessHandler, AuthenticationFailureHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationSuccessHandlerAndRegistrationFilter.class);
    private static final List<String> EXCLUDED_PATHS = Lists.newArrayList();

    static {
        EXCLUDED_PATHS.add("/favicon.ico");
        EXCLUDED_PATHS.add("/css");
        EXCLUDED_PATHS.add("/js");
        EXCLUDED_PATHS.add("/fonts");
        EXCLUDED_PATHS.add("/images");
        EXCLUDED_PATHS.add("/webjars");

        EXCLUDED_PATHS.add("/api");

        EXCLUDED_PATHS.add("/lovdrop");
        EXCLUDED_PATHS.add("/dating/places");
        EXCLUDED_PATHS.add("/user/profile/hands");
        EXCLUDED_PATHS.add("/user/profile/feet");
        EXCLUDED_PATHS.add("/user/profile/legs");

        EXCLUDED_PATHS.add("/v1/gallery/photo");

        EXCLUDED_PATHS.add("/api/v1/gallery/photo");
        EXCLUDED_PATHS.add("/gallery/photo");

        EXCLUDED_PATHS.add("/lovdrop/samples");
        EXCLUDED_PATHS.add("/register");
        EXCLUDED_PATHS.add("/users/nearby");

        EXCLUDED_PATHS.add("/events/browse");
        EXCLUDED_PATHS.add("/events");
        EXCLUDED_PATHS.add("/events/show/nearby/count");
        EXCLUDED_PATHS.add("/plans-and-pricing");
        EXCLUDED_PATHS.add("/features");
        EXCLUDED_PATHS.add("/mobile");
        EXCLUDED_PATHS.add("/about-us");
        EXCLUDED_PATHS.add("/contact-us");
        EXCLUDED_PATHS.add("/blog");

        EXCLUDED_PATHS.add("/account_not_verified");

        EXCLUDED_PATHS.add("/lox");
        EXCLUDED_PATHS.add("/lox/profile");

        EXCLUDED_PATHS.add("/couples/dashboard");

        EXCLUDED_PATHS.add("/user/registration/signup");
        EXCLUDED_PATHS.add("/user/registration/new/signup");
        EXCLUDED_PATHS.add("/user/registration/photo");

        EXCLUDED_PATHS.add("/vendor/signup");
        EXCLUDED_PATHS.add("/vendor/new/signup");
        EXCLUDED_PATHS.add("/vendor/mobile/**");
        EXCLUDED_PATHS.add("/vendor/mobile/verify");
        EXCLUDED_PATHS.add("/vendor/mobile/verification");
        EXCLUDED_PATHS.add("/developer");
        EXCLUDED_PATHS.add("/developer/**");
        EXCLUDED_PATHS.add("/careers");

        EXCLUDED_PATHS.add("/lox/news/admin");
        EXCLUDED_PATHS.add("/lox/news/admin/allNews");
        EXCLUDED_PATHS.add("/lox/news/admin/asynchronous/search");

        EXCLUDED_PATHS.add("/lox/news/admin/asynchronous/drop-downs");
        EXCLUDED_PATHS.add("/lox/news/admin/asynchronous/new");
        EXCLUDED_PATHS.add("/news/search");
        EXCLUDED_PATHS.add("/news/approved");
        EXCLUDED_PATHS.add("/news/latest");


    }

    private final UserSurveyService userSurveyService;
    private final UserService userService;
    private final UserProfileService userProfileService;
    private final UserPreferenceService userPreferenceService;
    private final LovstampService lovstampService;
    private final DatingPlacesUtil datingPlacesUtil;
    private final CoupleProfileService coupleProfileService;

    private final RequestCache requestCache;

    public AuthenticationSuccessHandlerAndRegistrationFilter(UserProfileService userProfileService,
                                                             UserPreferenceService userPreferenceService, LovstampService lovstampService, UserService userService, UserSurveyService userSurveyService, DatingPlacesUtil datingPlacesUtil,  CoupleProfileService coupleProfileService, RequestCache requestCache) {
        this.userService = userService;
        this.userProfileService = userProfileService;
        this.userPreferenceService = userPreferenceService;
        this.lovstampService = lovstampService;
        this.userSurveyService = userSurveyService;
        this.datingPlacesUtil = datingPlacesUtil;
        this.coupleProfileService = coupleProfileService;
        this.requestCache = requestCache;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request,
                                        HttpServletResponse response, AuthenticationException exception)
            throws IOException, ServletException {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        LOGGER.debug("[On Failure] UserId {} : " + userId);
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws
            IOException, ServletException {
        String targetUrl = onAuthenticationSuccess(authentication, request, response);
        if (!targetUrl.startsWith("http"))
            targetUrl = request.getContextPath() + targetUrl;

        response.sendRedirect(targetUrl);
    }

    public String onAuthenticationSuccess(Authentication authentication, HttpServletRequest request, HttpServletResponse response) throws
            IOException, ServletException {

        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        if (userId == null || StringUtils.isEmpty(UserUtil.INSTANCE.getCurrentUser().getEmail())) {
            return "/registration/email";
        }


        String targetUrl = determineTargetUrl(userId);
        if (Strings.isNullOrEmpty(targetUrl)) {
            SavedRequest savedRequest = requestCache.getRequest(request, response);
            // for some reason, in staging it goes to https://mycusto.com:8443/error
            if (savedRequest != null && savedRequest.getRedirectUrl() != null && !savedRequest.getRedirectUrl().contains("error") && !savedRequest.getRedirectUrl().contains("null")) {
                targetUrl = savedRequest.getRedirectUrl();
                LOGGER.debug("[On Login] Redirecting to DefaultSavedRequest Url: " + targetUrl);
            } else {
                targetUrl = redirectUrlBasedOnRole(userId);
                LOGGER.debug("[On Login] Redirecting to default target Url: " + targetUrl);
            }
        } else {
            targetUrl = targetUrl + (targetUrl.contains("/registration") ? "?back=1" : "");
            LOGGER.debug("[On Login] Redirecting to target Url: " + targetUrl);
        }
        userService.updateLastLogin(userId);

        HttpSession session = request.getSession(true);
        try {
            session.setAttribute("surveyStatus", userSurveyService.isAnswered(userId) ? "1" : "0");
            List<String> authorities = UserUtil.INSTANCE.getCurrentUser().getAuthorities().stream().map(Object::toString)
                    .collect(Collectors.toList());

            session.setAttribute("isOnlyAmbassador",
                    (authorities.contains("EVENT_AMBASSADOR") && !authorities.contains("USER")) ? "1" : "0");
        } catch (Exception ex) {
            LOGGER.error("error :", ex.getMessage());
            session.setAttribute("surveyStatus", "0");
        }
        UserAuthDto userAuthDto = UserUtil.INSTANCE.getCurrentUser();


        //TODO @Darrel, What is this code for?
        if (userAuthDto != null) {
            String url = datingPlacesUtil.addReview(request);
            if(!StringUtils.isEmpty(url)){
                targetUrl = url;
            }
        }
        //change oauth2 security to session based authentication
        if (authentication != null && authentication instanceof OAuth2Authentication) {
            OAuth2Authentication auth = (OAuth2Authentication) authentication;

            if (UserAuthDto.class.isAssignableFrom(authentication.getPrincipal().getClass())) {

                UsernamePasswordAuthenticationToken updatedAuth = new UsernamePasswordAuthenticationToken(userAuthDto, userAuthDto.getSocialId(),
                        userAuthDto.getAuthorities());

                updatedAuth.setDetails(auth.getPrincipal());

                SecurityContextHolder.getContext().setAuthentication(updatedAuth);
                // add authentication to the session
                request.getSession().setAttribute(
                        HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
                        SecurityContextHolder.getContext());
            }
        }
        return targetUrl;
    }

    private String redirectUrlBasedOnRole(Integer userId) {
        String result = "/radio";
        if (userId != null) {
            List<String> userRoles = userService.getRolesForUser(userId);

            if (userRoles.contains((Roles.COUPLE.getValue()))) {
                return "/couples/dashboard";
            }

            if (userRoles.contains((Roles.VENDOR.getValue()))) {
                result = "/vendor";
            }

            if (userRoles.contains(Roles.USER.getValue()))
                return result;
            if (userRoles.contains(Roles.MUSICIAN.getValue())) {
                result = "/music/my-music";
            } else if (userRoles.contains(Roles.EVENT_AMBASSADOR.getValue())) {
                result = "/events/browse";

            } else if (userRoles.contains((Roles.AUTHOR.getValue()))) {
                result = "/blog";

            }
        }
        return result;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws
            ServletException, IOException {

        Boolean isCouple = false;
        UserAuthDto currentUser = UserUtil.INSTANCE.getCurrentUser();
        if (currentUser != null) {

            if (request.getSession().getAttribute("INLOVE") != null && (Boolean) request.getSession().getAttribute("INLOVE")) {
                if (userService.assignRole(Roles.COUPLE.getValue(), currentUser.getUserId())) {
                    response.sendRedirect(request.getContextPath() + "/registration/couples");
                }
                isCouple = true;

            } else {
                isCouple = currentUser.getAuthorities() != null && currentUser.getAuthorities().stream() != null &&
                        currentUser.getAuthorities().stream().anyMatch(x -> x.getAuthority().equals("COUPLE"));
            }

        } else {
            // check if admin session is entered

            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
                User user = null;
                if (authentication.getDetails() instanceof User && !request.getRequestURI().contains("lox")) {

                    response.sendRedirect(request.getContextPath() + "/lox");
                    return;
                }
            }
        }
        if (isPathExcluded(request) || hasAllowedHeader(request) || isCouple) {
            // continue to the next filter
            //TODO check Internal server error after long time not loggedin
            try {
//                String currentUrl = UrlUtils.buildRequestUrl(request);
//                LOGGER.debug("[Filter] currentUrl: " + currentUrl);
//                request.setAttribute("role", currentUrl);
                filterChain.doFilter(request, response);
            } catch (Exception ex) {
                LOGGER.error("logout exception ", ex);
                return;
            }
        } else {
            Integer userId = UserUtil.INSTANCE.getCurrentUserId();
            if (userId == null) {
                String ajaxHeader = request.getHeader("X-Requested-With");
                boolean isAjax = StringUtils.hasText(ajaxHeader) && "XMLHttpRequest".equals(ajaxHeader);
                if (isAjax) {
                    // Ajax request and user is not authenticated (perhaps session timed-out), redirect to login
                    String scr = "<script>window.location=\"" + request.getContextPath() + "/login\"</script>";
                    response.getWriter().write(scr);
                } else {
                    try {
                        filterChain.doFilter(request, response);
                    } catch (IllegalStateException ex) {
                        LOGGER.error("error {}", ex);
                    }
                }
            } else {
                String currentUrl = UrlUtils.buildRequestUrl(request);
                LOGGER.debug("[Filter] currentUrl: " + currentUrl);
                if (currentUrl.startsWith("/registration") /*|| currentUrl.startsWith("/ambassador")*/) {
                    // Continue to next filter if url starts with registration
                    filterChain.doFilter(request, response);
                } else {
                    String targetUrl = determineTargetUrl(userId);
                    if (!Strings.isNullOrEmpty(targetUrl)) {
                        LOGGER.debug("[Filter] Redirecting to target Url: " + targetUrl);
                        if (currentUrl.equals(targetUrl)) {
                            // Continue to next filter if current url is the target url
                            filterChain.doFilter(request, response);
                        } else {
                            // Redirect to the target url
                            response.sendRedirect(request.getContextPath() + targetUrl);
                        }
                    } else {
                        // Continue to next filter if user shouldn't be redirected to any registration page
                        request.setAttribute("fullyRegistered", true);
                        filterChain.doFilter(request, response);
                    }
                }
            }
        }
    }

    private boolean hasAllowedHeader(HttpServletRequest request) {
        String contentType = request.getHeader("Content-Type");
        String accept = request.getHeader("Accept");
        boolean allowed = (StringUtils.hasText(contentType) && contentType.startsWith("multipart"))
                || (StringUtils.hasText(accept) && accept.startsWith("image"));
        return allowed;
    }

    private boolean isPathExcluded(HttpServletRequest request) {
        boolean excluded = false;
        String currentUrl = UrlUtils.buildRequestUrl(request);
        LOGGER.debug("current URL " + currentUrl);
        for (String path : EXCLUDED_PATHS) {
            if (currentUrl.startsWith(path)) {
                excluded = true;
                break;
            }
        }
        return excluded;
    }

    private String determineTargetUrl(Integer userId) {
        String result = "";
        if (userId != null) {
            UserDto user = userService.getUser(userId);
            List<String> userRoles = userService.getRolesForUser(userId);
            MemberProfileDto prof = userProfileService.findFullOne(userId);
            UserPreferenceDto pref = userPreferenceService.findOne(userId);
            if (user.getBanned() || user.getPaused()) {
                result = "/login?error=" + PageConstants.SUSPENDED_ERROR;
            } else if (userRoles.contains((Roles.COUPLE.getValue()))) {
                if (!hasCompletedCoupleProfileRegistration(userId)) {
                    result = "/registration/couples";
                } else {
                    return "";
                    // result = "/user/couples_dashboard";
                }
            } else if (!userRoles.contains(Roles.USER.getValue()) && userRoles.contains(Roles.EVENT_AMBASSADOR.getValue())) {
                result = "";
            } else if (!userRoles.contains(Roles.USER.getValue()) && userRoles.contains(Roles.MUSICIAN.getValue())) {
                return "";
                //result = "/music/upload";
            } else if (!userRoles.contains(Roles.USER.getValue()) && userRoles.contains(Roles.AUTHOR.getValue())) {

                return "";
            } else if (userRoles.contains(Roles.VENDOR.getValue())) {
                if (user.getMobileVerified()) {
                    VendorDto vendor = userService.findVendorByUserId(userId);
                    if (!vendor.getRegistrationStep1()) {
                        if (vendor.getVendorType().equals(VendorType.BRAND)) {
                            result = "/brand/register";
                        } else {
                            result = "/retailer/register";
                        }
                    } else if (!vendor.getRegistrationStep2()) {
                        result = "/vendor/product/submission/options";
                    } else {
                        return "";
                    }
                } else {
                    result = "/vendor/mobile/verification" + userId;
//                    return "";
                }
            } else if (prof == null && !userRoles.contains(Roles.COUPLE.getValue()) && !userRoles.contains(Roles.VENDOR.getValue())) {
                result = "/registration/gender";
            } else if (pref == null && !userRoles.contains(Roles.COUPLE.getValue()) && !userRoles.contains(Roles.VENDOR.getValue())) {
                result = "/registration/gender";
            } else if (!hasCompletedGenderRegistration(prof, pref) && !userRoles.contains(Roles.COUPLE.getValue()) && !userRoles.contains(Roles.VENDOR.getValue())) {
                result = "/registration/gender";
            } else if (userRoles.contains(Roles.USER.getValue())) {
                if (!hasCompletedAgeheightsizelangRegistration(prof, pref)) {
                    result = "/registration/ageheightsizelang";
                } else if (!hasCompletedPersonalitylifestyleRegistration(prof)) {
                    result = "/registration/personalitylifestyle";
                } else if (!hasCompletedPersonalitystatusRegistration(prof)) {
                    result = "/registration/personalitystatus";
                } else if (!hasCompletedProfilepicsRegistration(userId, prof)) {
                    result = "/registration/profilepics";
                } else if (prof.getSubscriptionType() == null) {
                    result = "/registration/subscription";
                }
//                else if (!user.getEmailVerified() && user.getSocialPlatform() == null) {
//                    result = "/radio";
//                } else if (!user.getEmailVerified() && user.getSocialPlatform() == null) {
//                    result = "/login?error=not_verified";
//                }
            }
        }
        return result;
    }

    private boolean hasCompletedGenderRegistration(MemberProfileDto prof, UserPreferenceDto pref) {
        return prof.getGender() != null && pref.getGender() != null && prof.getBirthDate() != null && prof.getAddress() != null
                && prof.getAddress().getLongitude() != null && prof.getAddress().getLongitude() != null;
    }

    private boolean hasCompletedAgeheightsizelangRegistration(MemberProfileDto prof, UserPreferenceDto pref) {
        return prof.getBirthDate() != null && pref.getHeight() != null && ( (pref.getGender() == Gender.MALE && pref.getPenisSizeMatter() != null) ||
                (pref.getGender() == Gender.FEMALE && pref.getBustSize() != null && pref.getButtSize() != null) ||
                (pref.getGender() == Gender.BOTH && pref.getBustSize() != null && pref.getButtSize() != null && pref.getPenisSizeMatter() != null)  )  ;
    }

    private boolean hasCompletedPersonalitylifestyleRegistration(MemberProfileDto prof) {
        return prof.getPersonalities().size() > 0 && prof.getLifestyles().size() > 0;
        // no longer the status for the new layout && prof.getStatuses().size() > 0; // TODO: Do the checking using the actual sizes
    }

    private boolean hasCompletedCoupleProfileRegistration(Integer userId) {
        CouplesProfileDto dto = coupleProfileService.getCoupleProfile(userId);

        return dto != null && dto.getAnniversaryDate() != null && dto.getBirthDate() != null && dto.getCoupleGender() != null
                && dto.getYearsTogether() > 0 && dto.getGender() != null;
    }

    private boolean hasCompletedPersonalitystatusRegistration(MemberProfileDto prof) {
        return prof.getStatuses().size() > 0; // TODO: Do the checking using the actual sizes
    }

    private boolean hasCompletedProfilepicsRegistration(Integer userId, MemberProfileDto prof) {
        logger.info(prof.toString());
        return lovstampService.getLovstampByUserID(userId) != null
                && ((prof.getHandsCloudFile() != null &&prof.getHandsCloudFile().getUrl() != null )|| (prof.isHandsFileSkipped()))
                && ((prof.getFeetCloudFile() != null &&prof.getFeetCloudFile().getUrl() != null) || (prof.isFeetFileSkipped()))
                && ((prof.getLegsCloudFile() != null && prof.getLegsCloudFile().getUrl() != null )|| (prof.isLegsFileSkipped()));
    }
}