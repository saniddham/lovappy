package com.realroofers.lovappy.web.email;


import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.util.InitialDataLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.ITemplateEngine;

/**
 * Created by Daoud Shaheen on 9/9/2017.
 */
public abstract class AbstractEmailTemplateHandler implements EmailTemplateHandler{

    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractEmailTemplateHandler.class);
    protected MailService mailService;
    protected ITemplateEngine templateEngine;
    protected EmailTemplateDto emailTemplateDto;
    protected String email;
    protected String baseUrl;

    public AbstractEmailTemplateHandler(String baseUrl, String email, MailService mailService, ITemplateEngine templateEngine, EmailTemplateDto emailTemplateDto) {
        this.mailService = mailService;
        this.templateEngine = templateEngine;
        this.emailTemplateDto = emailTemplateDto;
        this.email = email;
        this.baseUrl = baseUrl;
    }
}
