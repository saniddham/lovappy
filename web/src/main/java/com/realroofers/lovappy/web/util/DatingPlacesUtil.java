package com.realroofers.lovappy.web.util;

import com.realroofers.lovappy.service.datingPlaces.DatingPlaceService;
import com.realroofers.lovappy.service.datingPlaces.dto.DatingPlaceDto;
import com.realroofers.lovappy.service.datingPlaces.dto.DatingPlaceReviewDto;
import com.realroofers.lovappy.service.datingPlaces.support.google_places.DatingPlaceLocationUtil;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.order.dto.OrderDetailDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.UserAuthDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by daoud on 10/6/2018.
 */
@Component
public class DatingPlacesUtil {

    @Autowired
    private NotificationService notificationService;
    @Autowired
    private DatingPlaceService datingPlaceService;

    @Autowired
    private DatingPlaceLocationUtil datingPlaceLocationUtil;
    public String addReview(HttpServletRequest request) throws IOException {

        String targetUrl = "";
        UserAuthDto userAuthDto = UserUtil.INSTANCE.getCurrentUser();
        if (userAuthDto != null) {

            DatingPlaceDto datingPlaceDto = (DatingPlaceDto) request.getSession().getAttribute("datingPlace");
            DatingPlaceReviewDto datingPlaceReviewDto = (DatingPlaceReviewDto) request.getSession().getAttribute("datingPlaceReviewDto");

            NotificationDto notificationDto = new NotificationDto();
            if (datingPlaceDto != null) {
                datingPlaceDto.setCreateEmail(userAuthDto.getEmail());

                if (datingPlaceDto.getUserType().equalsIgnoreCase("reviewer")) {
                    datingPlaceDto.getDatingPlaceReviewDtos().get(0).setReviewerEmail(userAuthDto.getEmail());
                    DatingPlaceDto savedPlace = datingPlaceService.addDatingPlace(datingPlaceDto);
                    notificationDto.setContent(savedPlace.getPlaceName() + " is a new Dating Place in LovAppy");
                    notificationDto.setType(NotificationTypes.PLACE_APPROVAL);
                    notificationDto.setUserId(userAuthDto.getUserId());
                    notificationService.sendNotificationToAdmins(notificationDto);

                } else if (datingPlaceDto.getUserType().equalsIgnoreCase("manager")) {
                    //Review with Darry
                    OrderDto order = datingPlaceService.addFeaturedDatingPlace(datingPlaceDto, null, PaymentMethodType.SQUARE);
                    if (order != null) {
                        Integer productId = order.getOrderDetails().
                                stream().findFirst().orElse(new OrderDetailDto()).getOriginalId().intValue();
                        if (productId != null) {
                            DatingPlaceDto savedFeaturedPlace = datingPlaceService.findDatingPlaceById(productId);
                            if (savedFeaturedPlace != null) {
                                notificationDto.setContent(savedFeaturedPlace.getPlaceName() + " is a new Dating Place in LovAppy");
                                notificationDto.setType(NotificationTypes.PLACE_APPROVAL);
                                notificationDto.setUserId(userAuthDto.getUserId());
                                notificationDto.setSourceId(productId.longValue());
                                notificationService.sendNotificationToAdmins(notificationDto);
                            }
                        }
                    }
                }
            }

            if (datingPlaceReviewDto != null) {
                datingPlaceReviewDto.setReviewerEmail(userAuthDto.getEmail());

              if (datingPlaceReviewDto.getGooglePlaceId() != null) {
                    targetUrl = "/dating/places/" + datingPlaceReviewDto.getGooglePlaceId();
                    datingPlaceDto = datingPlaceLocationUtil.getGooglePlaceDetails(datingPlaceReviewDto.getGooglePlaceId());
                } else {
                    targetUrl = "/dating/places/" + datingPlaceReviewDto.getPlaceId().getDatingPlacesId();
                }
                DatingPlaceReviewDto savedReviewDto = datingPlaceService.addDatingPlaceReview(datingPlaceReviewDto, datingPlaceDto);

                if (savedReviewDto != null) {
                    notificationDto.setContent("A new review for " + savedReviewDto.getPlaceName() + " has been added");
                    notificationDto.setType(NotificationTypes.REVIEW_APPROVAL);
                    notificationDto.setUserId(userAuthDto.getUserId());
                    notificationService.sendNotificationToAdmins(notificationDto);
                }
            }
        }
        return targetUrl;
    }
}
