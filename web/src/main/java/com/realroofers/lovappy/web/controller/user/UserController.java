package com.realroofers.lovappy.web.controller.user;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.service.user.dto.UserLocationDto;
import com.realroofers.lovappy.service.user.dto.WithinRadiusDto;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@RestController
@RequestMapping("/users")
public class UserController {
    private UserProfileService service;

    @Autowired
    public UserController(UserProfileService userProfileService) {
        this.service = userProfileService;
    }

    @PostMapping("/nearby")
    public List<UserLocationDto> getNearbyUsers(@RequestBody WithinRadiusDto searchDto) {
        return service.getNearbyUsers(searchDto);
    }


}
