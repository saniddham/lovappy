package com.realroofers.lovappy.web.external;

/**
 * Created by Manoj on 07/01/2018.
 */
public interface MessageByLocaleService {
    public String getMessage(String id);
}
