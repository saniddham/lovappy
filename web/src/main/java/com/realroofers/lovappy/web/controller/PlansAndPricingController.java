package com.realroofers.lovappy.web.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.dto.RulePricePlanDTO;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import com.realroofers.lovappy.service.plan.PlansService;
import com.realroofers.lovappy.service.plan.dto.PlanDetailsDto;
import com.realroofers.lovappy.service.util.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Daoud Shaheen on 6/10/2017.
 */
@Controller
@RequestMapping("/plans-and-pricing")
public class PlansAndPricingController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlansAndPricingController.class);
    private PlansService plansService;
    private CMSService cmsService;

    public PlansAndPricingController(PlansService plansService, CMSService cmsService) {
        this.plansService = plansService;
        this.cmsService = cmsService;
    }

    @GetMapping
    public ModelAndView plans(ModelAndView model) {
        List<PlanDetailsDto> planDetailsDto = plansService.allDetailedPlans(true);
        model.addObject("plans", planDetailsDto);
        model.addObject(PageConstants.PLAN_PRICE_COVER_TEXT, cmsService.getTextByTagAndLanguage(PageConstants.PLAN_PRICE_COVER_TEXT, "EN"));
        model.addObject(PageConstants.PLAN_PRICE_COVER_DESCRIPTION, cmsService.getTextByTagAndLanguage(PageConstants.PLAN_PRICE_COVER_DESCRIPTION, "EN"));
        model.addObject(PageConstants.PLAN_PRICE_COVER_IMAGE, cmsService.getImageUrlByTag(PageConstants.PLAN_PRICE_COVER_IMAGE));

        model.setViewName("plans-and-pricing");
        return model;
    }

    public String aboutUs(Model model) {
        model.addAttribute(PageConstants.PLAN_PRICE_COVER_TEXT, cmsService.getTextByTagAndLanguage(PageConstants.PLAN_PRICE_COVER_TEXT, "EN"));
        model.addAttribute(PageConstants.PLAN_PRICE_COVER_IMAGE, cmsService.getImageUrlByTag(PageConstants.PLAN_PRICE_COVER_IMAGE));
        List<RulePricePlanDTO> rules = (List<RulePricePlanDTO>) CommonUtils.fromJSONArrayString(cmsService.getTextByTagAndLanguage(PageConstants.PLAN_PRICE_RULES, "EN"), new TypeReference<List<RulePricePlanDTO>>() {
        });
        rules.add(new RulePricePlanDTO("Feature", "Lite", "Prime", 0));
        Collections.sort(rules);
        model.addAttribute("rules", rules);
        return "plans-and-pricing";
    }
}
