package com.realroofers.lovappy.web.rest.v1;


import com.realroofers.lovappy.service.product.ProductTypeService;
import com.realroofers.lovappy.service.product.dto.ProductTypeDto;
import com.realroofers.lovappy.service.product.dto.mobile.CategoryItemDto;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.UserDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Manoj Janaka on 19/3/2018.
 */
@RestController
@RequestMapping({"/api/v1/product-type", "/api/v2/product-type"})
public class RestProductTypeController {

    private final ProductTypeService productTypeService;

    public RestProductTypeController(ProductTypeService productTypeService) {
        this.productTypeService = productTypeService;
    }


    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<CategoryItemDto>> getAll() {
        List<CategoryItemDto> brandDtos = productTypeService.findAllForMobile();
        return new ResponseEntity<>(brandDtos, HttpStatus.OK);
    }


    @PostMapping("/add")
    public ResponseEntity<ProductTypeDto> addBrand(@ModelAttribute @Valid ProductTypeDto productTypeDto) {
        try {

            ProductTypeDto brand = productTypeService.findByName(productTypeDto.getTypeName());
            if (brand != null) {
                return new ResponseEntity<>(HttpStatus.FOUND);
            }
            final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

            productTypeDto.setCreatedBy(new UserDto(userId));
            productTypeDto.setActive(true);
            ProductTypeDto createdBrand = productTypeService.create(productTypeDto);

            return new ResponseEntity<>(createdBrand, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
