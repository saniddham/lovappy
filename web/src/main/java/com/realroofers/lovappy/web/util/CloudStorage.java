package com.realroofers.lovappy.web.util;

import com.google.appengine.api.images.*;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.*;
import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.dto.ImageThumbnailDto;
import com.realroofers.lovappy.service.cloud.support.ImageSize;
import com.realroofers.lovappy.web.support.FileExtension;
//import net.bramp.ffmpeg.FFmpeg;
//import net.bramp.ffmpeg.FFmpegExecutor;
//import net.bramp.ffmpeg.FFprobe;
//import net.bramp.ffmpeg.builder.FFmpegBuilder;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;

/**
 * Created by Eias Altawil on 6/25/2017
 */

@Component
public class CloudStorage {

    private static Logger LOG = LoggerFactory.getLogger(CloudStorage.class);

    private Storage storage;

    @Value("${lovappy.cloud.ProjectId}")
    String ProjectId;

    @Value("${spring.profiles.active}")
    String activeProfile;

    private final CloudStorageService cloudStorageService;


    @Autowired
    public CloudStorage(CloudStorageService cloudStorageService) {

        if (Objects.equals(activeProfile, "production") || Objects.equals(activeProfile, "testing")) {
            storage = StorageOptions.getDefaultInstance().getService();

        } else {
            try {
                storage = StorageOptions.newBuilder()
                        .setProjectId(ProjectId)
                        .setCredentials(GoogleCredentials.fromStream(
                                getClass().getResourceAsStream("/Lovappy-gcloud-key.json")))
                        .build().getService();

            } catch (IOException e) {
                LOG.error(e.getMessage());
            }
        }
        this.cloudStorageService = cloudStorageService;
    }


    private String generateFileName(String extension) {
        return String.valueOf(new Date().getTime()) + "." + extension;
    }


    public CloudStorageFileDto uploadAndPersistImageWithThumbnails(MultipartFile filePart, final String bucketName, String extension,
                                                    String contentType) throws IOException{
        CloudStorageFileDto cloudStorageFileDto = uploadImageWithThumbnails(filePart, bucketName, extension, contentType);
        return cloudStorageService.add(cloudStorageFileDto);
    }

    public CloudStorageFileDto uploadAndPersistFile(MultipartFile filePart, final String bucketName, String extension,
                                                    String contentType) throws IOException{
        CloudStorageFileDto cloudStorageFileDto = uploadFile(filePart, bucketName, extension, contentType);
        return cloudStorageService.add(cloudStorageFileDto);
    }

    public CloudStorageFileDto uploadImageWithThumbnails(MultipartFile filePart, final String bucketName, String extension,
                                                         String contentType) throws IOException {

        return uploadImageWithThumbnails(filePart.getBytes(), bucketName, FilenameUtils.getExtension(filePart.getOriginalFilename()), contentType, true);
    }

    public CloudStorageFileDto uploadFile(MultipartFile filePart, final String bucketName, String extension,
                                                         String contentType) throws IOException {

        return uploadFile(filePart.getBytes(), bucketName, extension, contentType, true);
    }
    public CloudStorageFileDto uploadFile(byte[] bytesArray, final String bucketName, String extension,
                                          String contentType, Boolean isOriginal) throws IOException {

        String filename = generateFileName(extension);
//        if (extension == FileExtension.mp3.toString()){
//            String tempFilePrefix = "temp_alexa_formatted_";
//            final FFmpeg ffmpeg = new FFmpeg();
//            final FFprobe ffprobe = new FFprobe();
//
//            FileOutputStream tempOriginalMp3 = new FileOutputStream(filename);
//            tempOriginalMp3.write(bytesArray);
//            tempOriginalMp3.close();
//
//            final FFmpegBuilder builder = new FFmpegBuilder()
//                    .setInput(filename)
//                    .overrideOutputFiles(true)
//                    .addOutput(tempFilePrefix + filename)
//                    .setAudioCodec("libmp3lame")
//                    .setAudioChannels(FFmpeg.AUDIO_MONO)
//                    .setAudioBitRate(FFmpeg.AUDIO_SAMPLE_48000)
//                    .setAudioSampleRate(FFmpeg.AUDIO_SAMPLE_16000)
//                    .done();
//
//            new FFmpegExecutor(ffmpeg, ffprobe).createJob(builder).run();
//
//            File alexaFormatted = new File(tempFilePrefix + filename);
//            //init array with file length
//            bytesArray = new byte[(int) alexaFormatted.length()];
//
//            FileInputStream alexaFormattedStream = new FileInputStream(alexaFormatted);
//            alexaFormattedStream.read(bytesArray); //read file into bytes[]
//            alexaFormattedStream.close();
//            alexaFormatted.delete();
//            File originalFile= new File( filename);
//            originalFile.delete();
//
//        }

        BlobInfo blobInfo =
                storage.create(
                        BlobInfo
                                .newBuilder(bucketName, filename)
                                .setContentType(contentType)
                                // Modify access list to allow all users with link to read file
                                .setAcl(new ArrayList<>(
                                        Collections.singletonList(Acl.of(Acl.User.ofAllUsers(), Acl.Role.READER))))
                                .build(),
                        bytesArray);
        String url = blobInfo.getMediaLink();

        CloudStorageFileDto cloudStorageFileDto = new CloudStorageFileDto(filename, url, bucketName);

        return cloudStorageFileDto;
    }
    public CloudStorageFileDto uploadImageWithThumbnails(byte[] bytesArray, final String bucketName, String extension,
                                                         String contentType, Boolean isOriginal) throws IOException {

        byte[] bytesResized = bytesArray;

        if (contentType.startsWith("image/"))
            extension = "jpg";

        String filename = generateFileName(extension);

        if (isOriginal && contentType.startsWith("image/")) {
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(bytesArray));
            int width = img.getWidth() > 1000 ? 1000 : img.getWidth();
            int height = img.getHeight() > 1000 ? 1000 : img.getHeight();

            bytesResized = resizeAndConvertImage(bytesArray, width, height);
        }

        BlobInfo blobInfo =
                storage.create(
                        BlobInfo
                                .newBuilder(bucketName, filename)
                                .setContentType(contentType)
                                // Modify access list to allow all users with link to read file
                                .setAcl(new ArrayList<>(
                                        Collections.singletonList(Acl.of(Acl.User.ofAllUsers(), Acl.Role.READER))))
                                .build(),
                        new ByteArrayInputStream(bytesResized));
        // return the public download link
        String url = blobInfo.getMediaLink();

        CloudStorageFileDto cloudStorageFileDto = new CloudStorageFileDto(filename, url, bucketName);

        if (isOriginal && contentType.startsWith("image/")) {
            cloudStorageFileDto = addThumbnails(cloudStorageFileDto, bytesResized, bucketName, extension, contentType);
        }

        return cloudStorageFileDto;
    }


    private CloudStorageFileDto addThumbnails(CloudStorageFileDto cloudStorageFileDto, byte[] byteArray,
                                              String bucketName, String extension, String contentType){
        try {
            if(contentType.startsWith("image/")) {

                //SMALL
                byte[] bytes1 = resizeAndConvertImage(byteArray, 150, 150);
                CloudStorageFileDto thumbnailFile1 = uploadImageWithThumbnails(bytes1, bucketName, extension, contentType, false);
                ImageThumbnailDto thumbnail1 = new ImageThumbnailDto(null, thumbnailFile1, ImageSize.SMALL);

                //MEDIUM
                byte[] bytes2 = resizeAndConvertImage(byteArray, 300, 300);
                CloudStorageFileDto thumbnailFile2 = uploadImageWithThumbnails(bytes2, bucketName, extension, contentType, false);
                ImageThumbnailDto thumbnail2 = new ImageThumbnailDto(null, thumbnailFile2, ImageSize.MEDIUM);

                //LARGE
                byte[] bytes3 = resizeAndConvertImage(byteArray, 600, 600);
                CloudStorageFileDto thumbnailFile3 = uploadImageWithThumbnails(bytes3, bucketName, extension, contentType, false);
                ImageThumbnailDto thumbnail3 = new ImageThumbnailDto(null, thumbnailFile3, ImageSize.LARGE);

                if(cloudStorageFileDto.getThumbnails() == null)
                    cloudStorageFileDto.setThumbnails(new ArrayList<>());

                cloudStorageFileDto.getThumbnails().add(thumbnail1);
                cloudStorageFileDto.getThumbnails().add(thumbnail2);
                cloudStorageFileDto.getThumbnails().add(thumbnail3);
            }
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return cloudStorageFileDto;
    }

    private byte[] resizeAndConvertImage(byte[] bytesArray, Integer width, Integer height) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        BufferedImage bufferedImage = Thumbnails.of(new ByteArrayInputStream(bytesArray))
                .size(width, height)
                .asBufferedImage();
        ImageIO.write(bufferedImage, "jpg", os);

        return os.toByteArray();
    }

    private InputStream resizeImage(byte[] imageBytes, Integer width, Integer height) {

        ImagesService imagesService = ImagesServiceFactory.getImagesService();
        Image image = ImagesServiceFactory.makeImage(imageBytes);
        Transform resize = ImagesServiceFactory.makeResize(width, height);
        Image resizedImage = imagesService.applyTransform(resize, image);

        return new ByteArrayInputStream(resizedImage.getImageData());
    }

    public String createThumbnailUrl(String bucket, String filename, Integer imageSize){
        ImagesService imagesService = ImagesServiceFactory.getImagesService();

        // Create a fixed dedicated URL that points to the GCS hosted file
        ServingUrlOptions options = ServingUrlOptions.Builder
                .withGoogleStorageFileName("/gs/" + bucket + "/" + filename)
                .imageSize(imageSize)
                .crop(true)
                .secureUrl(true);
        return imagesService.getServingUrl(options);
    }

    public String getFileLink(final String bucketName, final String fileName){
        return storage.get(BlobId.of(bucketName, fileName)).getMediaLink();
    }
}