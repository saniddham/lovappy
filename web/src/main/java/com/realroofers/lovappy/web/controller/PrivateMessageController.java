package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.privatemessage.PrivateMessageService;
import com.realroofers.lovappy.service.privatemessage.dto.PrivateMessageDto;
import com.realroofers.lovappy.service.privatemessage.support.Status;
import com.realroofers.lovappy.service.system.LanguageService;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.MyAccountDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import com.realroofers.lovappy.web.util.PaymentUtil;
import com.squareup.connect.ApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by Eias Altawil on 5/9/17
 */

@Controller
@RequestMapping()
public class PrivateMessageController {

    @Value("${lovappy.cloud.bucket.privateMessage}")
    private String privateMessageBucket;

    private final CloudStorage cloudStorage;
    private final PrivateMessageService privateMessageService;
    private final LanguageService languageService;
    private final PaymentUtil paymentUtil;
    private final OrderService orderService;
    private final UserService userService;
    private final UserProfileService userProfileService;
    private String squareAppID;
    private final EmailTemplateService emailTemplateService;

    private ITemplateEngine templateEngine;
    private MailService mailService;

    private static Logger LOG = LoggerFactory.getLogger(PrivateMessageController.class);

    @Autowired
    public PrivateMessageController(PrivateMessageService privateMessageService,
                                    PaymentUtil paymentUtil, OrderService orderService,
                                    UserService userService,
                                    UserProfileService userProfileService, @Value("${square.applicationId}") String squareAppID,
                                    MailService mailService, List<ViewResolver> viewResolvers,
                                    LanguageService languageService, CloudStorage cloudStorage, EmailTemplateService emailTemplateService) {
        this.privateMessageService = privateMessageService;
        this.paymentUtil = paymentUtil;
        this.orderService = orderService;
        this.userService = userService;
        this.userProfileService = userProfileService;
        this.squareAppID = squareAppID;
        this.mailService = mailService;
        this.emailTemplateService = emailTemplateService;
        for (ViewResolver viewResolver : viewResolvers) {
            if (viewResolver instanceof ThymeleafViewResolver) {
                ThymeleafViewResolver thymeleafViewResolver = (ThymeleafViewResolver) viewResolver;
                this.templateEngine = thymeleafViewResolver.getTemplateEngine();
            }
        }
        if (this.templateEngine == null) {
            throw new BeanInitializationException("No view resolver of type ThymeleafViewResolver found.");
        }
        this.languageService = languageService;
        this.cloudStorage = cloudStorage;
    }

    @GetMapping("/findlove")
    public ModelAndView messages() {
        ModelAndView mav = new ModelAndView("private_message/messages_landing");
        return mav;
    }

    @GetMapping("/private_message/send/{id}")
    public ModelAndView sendPrivateMessagePage(@PathVariable("id") Integer userId) {
        ModelAndView mav = new ModelAndView("send_private_message");
        mav.addObject("languages", languageService.getAllLanguages(true));
        mav.addObject("userID", userId);
        mav.addObject("squareAppID", squareAppID);
        return mav;
    }

    @PostMapping("/private_message/send")
    public ResponseEntity<?> sendPrivateMessage(HttpServletRequest request,
                                                @RequestParam("file") MultipartFile file,
                                                @RequestParam("userID") Integer userID,
                                                @RequestParam("languageID") Integer languageID,
                                                @RequestParam("nonce") String nonce,
                                                @RequestParam("coupon") String coupon) {
        try {
            CloudStorageFileDto cloudStorageFileDto =
                    cloudStorage.uploadFile(file, privateMessageBucket, FileExtension.mp3.toString(), "audio/mp3");

            if (cloudStorageFileDto != null) {
                PrivateMessageDto privateMessageDto = new PrivateMessageDto();
                privateMessageDto.setFromUser(new UserDto(UserUtil.INSTANCE.getCurrentUserId()));
                privateMessageDto.setToUser(new UserDto(userID));
                privateMessageDto.setAudioFileCloud(cloudStorageFileDto);
                privateMessageDto.setLanguage(new LanguageDto(languageID));

                OrderDto orderDto = privateMessageService.sendPrivateMessage(privateMessageDto, coupon, PaymentMethodType.SQUARE);

                if (orderDto != null) {

                    PrivateMessageDto message = privateMessageService.getPrivateMessageByID(orderDto.getOrderDetails().stream().findFirst().get().getOriginalId().intValue());

                    CalculatePriceDto calculatePrice = privateMessageService.calculatePrice(coupon);
                    Boolean executed = null;
                    try {
                        executed = paymentUtil.executeTransaction(nonce, orderDto, null);
                    } catch (ApiException e) {
                        e.printStackTrace();
                        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
                    }
                    if (executed) {
                        privateMessageService.updatePrivateMessageStatus(message.getId(), Status.PAID);

                        sendReceiptEmail(message.getFromUser().getEmail(), orderDto.getTotalPrice(), orderDto.getDate());


                        MyAccountDto myAccountDto = userService.getMyAccount(userID);

                        if (myAccountDto.isNewMessagesNotifications()) {
                            //Send to the user who received the private message
                            sendToReceiptEmail(message.getToUser().getEmail(), message.getFromUser().getID(),
                                    CommonUtils.getBaseURL(request), "EN", cloudStorageFileDto.getUrl());
                        }

                        //saveTransactionu
                        if (userProfileService.saveTransaction(UserUtil.INSTANCE.getCurrentUserId(), userID)) {
                            //send email
                            //send  email to user
                            EmailTemplateDto unlockEmailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.PHOTO_UNLOCK, "EN");
                            unlockEmailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/member/" + UserUtil.INSTANCE.getCurrentUserId());
                            new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), myAccountDto.getEmail(), templateEngine,
                                    mailService, unlockEmailTemplate).send();
                        }

                        return new ResponseEntity<>(HttpStatus.OK);
                    } else {
                        orderService.revertOrder(orderDto.getId());
                    }
                }
            }
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private void sendReceiptEmail(String toUserEmail, Double price, Date sentAt) {
        Context context = new Context();
        context.setVariable("price", price);
        context.setVariable("sentAt", sentAt);
        String content = templateEngine.process("email/sent_message_email", context);
        mailService.sendMail(toUserEmail, content, "Private Message Sent");
    }

    /**
     * Send email to user who recived the song
     *
     * @param toUserEmail
     * @param baseUrl
     * @param language
     * @param link
     */
    private void sendToReceiptEmail(String toUserEmail, Integer fromUserId, String baseUrl, String language, String link) {
        EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.NEW_MESSAGES, language);
        emailTemplate.getAdditionalInformation().put("url", link);
        emailTemplate.getAdditionalInformation().put("userId", String.valueOf(fromUserId));
        new EmailTemplateFactory().build(baseUrl, toUserEmail, templateEngine,
                mailService, emailTemplate).send();
    }

    @GetMapping("/private_message/inbox")
    public ModelAndView inbox() {
        ModelAndView mav = new ModelAndView("user/private_message_inbox");
        Collection<PrivateMessageDto> receivedPrivateMessages =
                privateMessageService.getReceivedPrivateMessages(UserUtil.INSTANCE.getCurrentUserId());

        mav.addObject("messages", receivedPrivateMessages);
        return mav;
    }
}
