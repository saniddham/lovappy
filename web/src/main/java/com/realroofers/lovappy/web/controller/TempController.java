package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Daoud Shaheen on 6/10/2017.
 */
@Controller
@RequestMapping("/temp")
public class TempController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TempController.class);


    @GetMapping("/map")
    public String aboutUs(Model model) {
       return "fragments/map-fragment";
    }

    @GetMapping("/upload-voices")
    public String voices(Model model) {
        return "/views/upload-voices";
    }
}
