package com.realroofers.lovappy.web.config.social.instagram.api.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.realroofers.lovappy.web.config.social.instagram.api.InstagramProfile;
import com.realroofers.lovappy.web.config.social.instagram.api.UserOperations;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * Implementation of {@link UserOperations}, providing a binding to Instagram's user-oriented REST resources.
 */
public class UserTemplate extends AbstractInstagramOperations implements UserOperations {
	
	public UserTemplate(InstagramTemplate instagram, boolean isAuthorizedForUser) {
		super(instagram, isAuthorizedForUser);
	}

	public InstagramProfile getUser() {
		requireUserAuthorization();
		return get(buildUri(USERS_ENDPOINT + "self/"), InstagramProfileContainer.class).getObject();
	}

	public InstagramProfile getUser(long userId) {
		return get(buildUri(USERS_ENDPOINT + Long.toString(userId) + "/"), InstagramProfileContainer.class).getObject();
	}

}
