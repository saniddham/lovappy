package com.realroofers.lovappy.web.exception;

/**
 * Created by Daoud Shaheen on 6/9/2017.
 */
public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String message) {
        super(message);
    }
}

