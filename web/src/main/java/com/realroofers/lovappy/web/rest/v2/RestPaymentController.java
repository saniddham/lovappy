package com.realroofers.lovappy.web.rest.v2;

import com.realroofers.lovappy.service.credits.CreditService;
import com.realroofers.lovappy.service.datingPlaces.DatingPlaceService;
import com.realroofers.lovappy.service.event.EventService;
import com.realroofers.lovappy.service.exception.GenericServiceException;
import com.realroofers.lovappy.service.gift.GiftService;
import com.realroofers.lovappy.service.gift.dto.GiftDto;
import com.realroofers.lovappy.service.music.MusicService;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.PriceService;
import com.realroofers.lovappy.service.order.dto.*;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import com.realroofers.lovappy.service.privatemessage.PrivateMessageService;
import com.realroofers.lovappy.service.questions.DailyQuestionService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.web.exception.AccessDeniedException;
import com.realroofers.lovappy.web.exception.BadRequestException;
import com.realroofers.lovappy.web.rest.dto.PaymentMethod;
import com.realroofers.lovappy.service.order.support.ProductType;
import com.realroofers.lovappy.web.util.StripeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Daoud Shaheen on 7/9/2018.
 * only stripe payment method
 */
@RestController
@RequestMapping({"/api/v2/payments", "/api/v1/payments"})
public class RestPaymentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestPaymentController.class);

    @Autowired
    private MusicService musicService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private GiftService giftService;
    @Autowired
    private EventService eventService;
    @Autowired
    private DatingPlaceService datingPlaceService;
    @Autowired
    private PrivateMessageService privateMessageService;
    @Autowired
    private PriceService priceService;
    @Autowired
    private StripeUtil stripeUtil;
    @Autowired
    private CreditService creditService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private DailyQuestionService dailyQuestionService;



    @RequestMapping(value = "/prices", method = RequestMethod.GET)
    public Page<PriceDto> getPrices(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                    @RequestParam(value = "limit", defaultValue = "100") Integer limit
    ) {

        if (page < 1) {
            page = 1;
        }
        return priceService.getAll(new PageRequest(page - 1, limit));
    }

    @RequestMapping(value = "/{product}/price", method = RequestMethod.GET)
    public CalculatePriceDto calculatePrice(@PathVariable("product") ProductType productType,
                                            @RequestParam(value = "productId", required = false) Integer productId,
                                            @RequestParam(value = "coupon", required = false) String coupon,
                                            @RequestParam("quantity") Integer quantity) {
        LOGGER.info("Calculate productType = {} productId = {} coupon = {} ", productType, productId, coupon);
        switch (productType) {
            case music:
                CouponDto couponDto = orderService.getCoupon(coupon, CouponCategory.MUSIC);
                return musicService.calculatePrice(productId, couponDto);
            case gifts:
                GiftDto giftDto = giftService.getGiftByID(productId);
                return giftService.calculatePrice(giftDto, coupon);
            case messages:
                return privateMessageService.calculatePrice(coupon);
            case events:
                return eventService.calculatePrice(productId, coupon);
            case datingplaces:
                return datingPlaceService.calculatePrice(productId, coupon);
            case credits:
                CouponDto coupo = orderService.getCoupon(coupon, CouponCategory.CREDITS);
                return creditService.calculatePrice(coupo, quantity);
            case dailyquestion:
                return dailyQuestionService.calculatePrice(quantity, coupon);
        }

        throw new BadRequestException("you provide wrong type it should be : messages, music, gift, dating-places, events, dailyquestion");
    }



    @RequestMapping(value = "/methods/default", method = RequestMethod.GET)
    public PaymentMethod getDefaultMethod() {
        return stripeUtil.getDefaultCard(UserUtil.INSTANCE.getCurrentUserId());
    }

    @RequestMapping(value = "/methods/all", method = RequestMethod.GET)
    public List<PaymentMethod> getAll() {
        return stripeUtil.getAll(UserUtil.INSTANCE.getCurrentUserId());
    }

    @RequestMapping(value = "/methods/{cardId}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("cardId") String cardId) {
        stripeUtil.deleteMethod(UserUtil.INSTANCE.getCurrentUserId(), cardId);
    }

    @RequestMapping(value = "/methods", method = RequestMethod.POST)
    public PaymentMethod add(@RequestParam("email") String email,
                             @RequestParam("token") String token) throws AccessDeniedException {
        if (!StringUtils.isEmpty(email) && !UserUtil.INSTANCE.getCurrentUser().getEmail().equals(email)) {
            throw new AccessDeniedException("user email doesn't match the logged in user");
        }
        return stripeUtil.saveMethod(token, UserUtil.INSTANCE.getCurrentUser().getEmail(), UserUtil.INSTANCE.getCurrentUserId());
    }

    @RequestMapping(value = "/customer/registration", method = RequestMethod.POST)
    public Map<String, Object> createCustomer(@RequestParam("email") String email,
                                              @RequestParam("token") String token) throws AccessDeniedException {
//        if(!StringUtils.isEmpty(email) && !UserUtil.INSTANCE.getCurrentUser().getEmail().equals(email) ) {
//            throw new AccessDeniedException("user email doesn't match the logged in user");
//        }
        return stripeUtil.createCustomer(token, UserUtil.INSTANCE.getCurrentUser().getEmail(), UserUtil.INSTANCE.getCurrentUserId());
    }

    @RequestMapping(value = "/methods/{cardId}/default", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void setDefault(@PathVariable("cardId") String cardId) {

        stripeUtil.setDefaultCard(UserUtil.INSTANCE.getCurrentUserId(), cardId);
    }


    @RequestMapping(value = "/{productType}/{productId}/order", method = RequestMethod.POST)
    public OrderDto order(@PathVariable("productType") ProductType productType, @PathVariable("productId") Long productId,
                          @RequestBody ChargeRequest chargeRequest) throws AccessDeniedException {

//        if(!UserUtil.INSTANCE.getCurrentUser().getEmail().equals(chargeRequest.getEmail()) ) {
//            throw new AccessDeniedException("user email doesn't match the logged in user");
//        }
        Object product = getProduct(productType, productId.intValue());
        if (product != null) {

            OrderDto orderDto = stripeUtil.executeTransaction(UserUtil.INSTANCE.getCurrentUserId(), productId.intValue(), chargeRequest, UserUtil.INSTANCE.getCurrentUser().getEmail(), productType);

            if (orderDto != null) {
                registerAndSendNotification(productType, productId);
                return orderDto;
            }
        }

        throw new GenericServiceException("Something went wrong, please try again");
    }

    @RequestMapping(value = "/order", method = RequestMethod.POST)
    public OrderDto orders(@RequestBody ChargeRequest chargeRequests) throws AccessDeniedException {

        List<OrderDetailDto> orderDetailDtoList = new ArrayList<>();
        List<Long> responsesIds = new ArrayList<>();
        for (ChargeRequestDetails chargeRequest: chargeRequests.getChargeRequestDetailsList()) {
            //Object product = getProduct(chargeRequest.getProductType(), chargeRequest.getProductId());
                OrderDetailDto orderDetailDto = new OrderDetailDto();
                orderDetailDto.setType(chargeRequest.getProductType().toOrderType());
                orderDetailDto.setOriginalId(chargeRequest.getProductId());
                orderDetailDto.setQuantity(chargeRequest.getQuantity());
                orderDetailDtoList.add(orderDetailDto);
                if(chargeRequest.getProductType() == ProductType.dailyquestion){
                    responsesIds.add(chargeRequest.getProductId());
                }

        }

        if (orderDetailDtoList.size()>0) {

            OrderDto orderDto = stripeUtil.executeTransaction(UserUtil.INSTANCE.getCurrentUserId(),  chargeRequests, orderDetailDtoList, UserUtil.INSTANCE.getCurrentUser().getEmail());

            if (orderDto != null) {
                for (ChargeRequestDetails chargeRequest: chargeRequests.getChargeRequestDetailsList()) {

                    registerAndSendNotification(chargeRequest.getProductType(), chargeRequest.getProductId());
                }
                if(responsesIds.size()>0){
                    dailyQuestionService.addPurchaseRecord(responsesIds,UserUtil.INSTANCE.getCurrentUserId());
                }
                return orderDto;
            }
        }

        throw new GenericServiceException("Something went wrong, please try again");
    }

    @RequestMapping(value = "/dailyquestion/order", method = RequestMethod.POST)
    public OrderDto orderDailyQuestions(@RequestBody ChargeRequest chargeRequests) throws AccessDeniedException {

        List<OrderDetailDto> orderDetailDtoList = new ArrayList<>();
        List<Long> responsesIds = new ArrayList<>();
        for (ChargeRequestDetails chargeRequest: chargeRequests.getChargeRequestDetailsList()) {
                OrderDetailDto orderDetailDto = new OrderDetailDto();
                orderDetailDto.setType(chargeRequest.getProductType().toOrderType());
                orderDetailDto.setOriginalId(chargeRequest.getProductId());
                orderDetailDto.setQuantity(chargeRequest.getQuantity());
                orderDetailDtoList.add(orderDetailDto);
                responsesIds.add(chargeRequest.getProductId());
        }

        if (orderDetailDtoList.size()>0) {

            OrderDto orderDto = stripeUtil.executeTransaction(UserUtil.INSTANCE.getCurrentUserId(),  chargeRequests, orderDetailDtoList, UserUtil.INSTANCE.getCurrentUser().getEmail());

            if (orderDto != null) {
                dailyQuestionService.addPurchaseRecord(responsesIds,UserUtil.INSTANCE.getCurrentUserId());
                return orderDto;
            }
        }

        throw new GenericServiceException("Something went wrong, please try again");
    }

    @RequestMapping(value = "/credits/order", method = RequestMethod.POST)
    public OrderDto orderCredit(@RequestBody ChargeRequest chargeRequest) throws AccessDeniedException {
        LOGGER.info("orderCredit chargeRequest = {}  ", chargeRequest);
        if (!StringUtils.isEmpty(chargeRequest.getEmail()) && !UserUtil.INSTANCE.getCurrentUser().getEmail().equals(chargeRequest.getEmail())) {
            throw new AccessDeniedException("user email doesn't match the logged in user");
        }
        OrderDto orderDto = stripeUtil.executeTransaction(UserUtil.INSTANCE.getCurrentUserId(), UserUtil.INSTANCE.getCurrentUserId(), chargeRequest, UserUtil.INSTANCE.getCurrentUser().getEmail(), ProductType.credits);

        if (orderDto != null) {
            LOGGER.info("orderDto orderDto = {}  ", orderDto);
            creditService.addCredit(UserUtil.INSTANCE.getCurrentUserId(), chargeRequest.getQuantity());
            return orderDto;
        }

        throw new GenericServiceException("Something went wrong, please try again");
    }


    private void registerAndSendNotification(ProductType prodcutType, Long productId) {

        String content = null;
        NotificationTypes notificationType = null;
        switch (prodcutType) {
            case events:
                content = "New Event has been ordered";
                eventService.registerUserForEvent(productId.intValue(), UserUtil.INSTANCE.getCurrentUserId());
                break;
            case music:
                musicService.setPurchased(productId.intValue(), UserUtil.INSTANCE.getCurrentUserId());
                content = "New Music has been ordered";
                notificationType = NotificationTypes.MUSIC_ORDER;
                break;
            case messages:
                content = "New Message voice has been ordered";
                break;

            case gifts:
                content = "New Gift has been ordered";
                break;
            case datingplaces:
                content = "New Dating place has been ordered";
                break;
            case dailyquestion:
                content = "New Daily Question has been ordered";
                break;

        }
        //send notification to admin
        if (notificationType != null) {
            NotificationDto notificationDto = new NotificationDto();
            notificationDto.setContent(content);
            notificationDto.setType(notificationType);
            notificationDto.setUserId(UserUtil.INSTANCE.getCurrentUser().getUserId());
            notificationDto.setSourceId(productId);
            notificationService.sendNotificationToAdmins(notificationDto);
        }
    }

    private Object getProduct(ProductType prodcutType, Integer productId) {

        switch (prodcutType) {
            case events:
                return eventService.findEventById(productId);
            case music:
                return musicService.getMusic(productId);

            case messages:
                return privateMessageService.getPrivateMessageByID(productId);

            case gifts:
                return giftService.getGiftByID(productId);

            case datingplaces:
                return datingPlaceService.findDatingPlaceById(productId);

        }
        return null;
    }
}
