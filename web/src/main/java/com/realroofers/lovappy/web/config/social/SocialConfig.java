package com.realroofers.lovappy.web.config.social;

import com.realroofers.lovappy.service.user.SocialProfileService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.web.config.AuthenticationSuccessHandlerAndRegistrationFilter;
import com.realroofers.lovappy.web.config.security.UserAuthorizationService;
import com.realroofers.lovappy.web.config.social.instagram.connect.InstagramConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurer;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.mem.InMemoryUsersConnectionRepository;
import org.springframework.social.connect.web.ConnectController;
import org.springframework.social.connect.web.ProviderSignInController;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;

@Configuration
@EnableSocial
public class SocialConfig implements SocialConfigurer {

    @Autowired
    private  UserAuthorizationService userAuthorizationService;

    @Autowired
    private UserService userService;
    @Autowired
    private SocialProfileService socialProfileService;
    @Autowired
    private AuthenticationSuccessHandlerAndRegistrationFilter authenticationSuccessHandlerAndRegistrationFilter;

    @Override
    public void addConnectionFactories(ConnectionFactoryConfigurer cfConfig, Environment env) {
        cfConfig.addConnectionFactory(new FacebookConnectionFactory(
                env.getProperty("facebook.client.clientId"),
                env.getProperty("facebook.client.clientSecret")
        ));

        cfConfig.addConnectionFactory(new TwitterConnectionFactory(
                env.getProperty("twitter.consumer-key"),
                env.getProperty("twitter.consumer-secret")
        ));

        cfConfig.addConnectionFactory(new InstagramConnectionFactory(
                env.getProperty("instagram.client.clientId"),
                env.getProperty("instagram.client.clientSecret")
        ));
    }

    @Override
    public UserIdSource getUserIdSource() {
        return new UserIdSource() {
            @Override
            public String getUserId() {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (authentication == null) {
                    throw new IllegalStateException("Unable to get a ConnectionRepository: no user signed in");
                }
                return authentication.getName();
            }
        };
    }


    @Override
    public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
        InMemoryUsersConnectionRepository usersConnectionRepository = new InMemoryUsersConnectionRepository(

                connectionFactoryLocator
        );
        return usersConnectionRepository;
    }

    @Bean
    public ConnectController connectController(ConnectionFactoryLocator connectionFactoryLocator, ConnectionRepository connectionRepository) {
        ConnectController connectController = new ConnectController(connectionFactoryLocator, connectionRepository);
        connectController.setViewPath("redirect:/radio");
        return connectController;
    }

    @Autowired
    private SocialConnectionSignup twitterConnectionSignup;
    @Autowired
    private ConnectionFactoryLocator connectionFactoryLocator;

    @Autowired
    private UsersConnectionRepository usersConnectionRepository;
    @Bean
    public ProviderSignInController providerSignInController() {
        ((InMemoryUsersConnectionRepository) usersConnectionRepository)
                .setConnectionSignUp(twitterConnectionSignup);

        ProviderSignInController providerSignInController = new ProviderSignInController(
                connectionFactoryLocator,
                usersConnectionRepository,
                new SocialSignInAdapter(userAuthorizationService, authenticationSuccessHandlerAndRegistrationFilter, userService, socialProfileService));

        providerSignInController.addSignInInterceptor(new SocialProviderSignInInterceptor());
        return providerSignInController;
    }
}
