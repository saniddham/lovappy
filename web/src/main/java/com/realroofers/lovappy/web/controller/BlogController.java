package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.blog.CategoryService;
import com.realroofers.lovappy.service.blog.PostService;
import com.realroofers.lovappy.service.blog.VlogsService;
import com.realroofers.lovappy.service.blog.dto.CategoryDto;
import com.realroofers.lovappy.service.blog.dto.EmailShareDto;
import com.realroofers.lovappy.service.blog.dto.PostDto;
import com.realroofers.lovappy.service.blog.dto.VlogsDto;
import com.realroofers.lovappy.service.blog.model.BlogTypes;
import com.realroofers.lovappy.service.blog.model.CategoryPostCount;
import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import com.realroofers.lovappy.service.core.SocialType;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.AuthorDto;
import com.realroofers.lovappy.service.user.dto.RegisterUserDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.util.AddressUtil;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.external.MessageByLocaleService;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import com.realroofers.lovappy.web.util.FileUtil;
import com.realroofers.lovappy.web.util.Pager;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.social.twitter.api.TweetData;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daoud Shaheen on 7/10/2017.
 */
@Controller
@RequestMapping("/blog")
public class BlogController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlogController.class);
    @Autowired
    protected PostService postService;

    @Autowired
    protected CategoryService categoryService;

    @Autowired
    protected UserService userService;

    @Autowired
    private FileUtil fileUploadUtil;
    @Autowired
    private AddressUtil addressUtil;
    @Autowired
    private ITemplateEngine templateEngine;

    @Autowired
    private CMSService cmsService;

    @Autowired
    private MailService mailService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private VlogsService vlogsService;

    @Autowired
    private CloudStorage cloudStorage;

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @Autowired
    private TwitterTemplate twitterTemplate;

    @Autowired
    private EmailTemplateService emailTemplateService;

    @Autowired
    private CloudStorageService cloudStorageService;

    @Autowired
    private MessageByLocaleService messageByLocaleService;

    @GetMapping("/posts/{slugUrl}/content")
    @ResponseBody
    public String getBlogContent(@PathVariable(value = "slugUrl") String slugUrl) {
        LOGGER.debug("=====================getBlog=====================");
        PostDto postDto = postService.findBySlugUrl(slugUrl);

        return postDto.getContent();
    }

    @GetMapping
    public ModelAndView getBlog(ModelAndView model, @RequestParam(value = "approved", defaultValue = "false") Boolean approved) {
        LOGGER.debug("=====================getBlog=====================");

        // get  most recent 9 Posts
        List<PostDto> topRecentBlogs = postService.getTopRecentBlogs(9);
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();


        //get main blog
        if (topRecentBlogs.size() > 0) {
            PostDto mainPost = topRecentBlogs.get(0);

            model.addObject("mainPost", mainPost);
        }
        if (topRecentBlogs.size() > 1) {
            PostDto secondMainPost = topRecentBlogs.get(1);
            model.addObject("secondMainPost", secondMainPost);
        }
        if (topRecentBlogs.size() > 2) {
            PostDto thirdMainPost = topRecentBlogs.get(2);
            model.addObject("thirdMainPost", thirdMainPost);
        }

        List<PostDto> topViewBlogs = postService.findMostViewed(1, 4).getContent();

        // get  most viewed 9 Posts for slider
        model.addObject("sliders", topViewBlogs);

        Page<PostDto> postsByLovappy = postService.getAdminPosts(1, 2);

        model.addObject("lovappyPosts", postsByLovappy.getContent());
        //Popular Posts
        model.addObject("popularPosts", topViewBlogs);
        //recent Posts
        model.addObject("recentPosts", topRecentBlogs.subList(0, topRecentBlogs.size() > 4 ? 4 : topRecentBlogs.size()));

        //user roles of the current user
        model.addObject("userId", userId);
        model.addObject("userRoles", userService.getRolesForUser(userId));


        Page<CategoryPostCount> categoryPostCounts = categoryService.getCategoryPostsCount(1, 20);
        model.addObject("categoryPostCounts", categoryPostCounts);

        Page<VlogsDto> vlogsDtos = vlogsService.getActiveVlogsList(1, 3);
        model.addObject("videos", vlogsDtos);

        model.addObject("user", new UserDto());
        model.addObject("isMyBlog", UserUtil.INSTANCE.getCurrentUserId() != null && postService.getNumberOfPostByUserId(UserUtil.INSTANCE.getCurrentUserId()) > 0);

        model.addObject("searchText", "");
        model.addObject("emailShare", new EmailShareDto());
        model.addObject("galleries", postService.getPostByType(BlogTypes.GALLERY, 1, 6).getContent());
        model.addObject("isApproved", approved);
        setAuthorRegisterModel(model);
        model.setViewName("blog/blog");
        return model;
    }

    ModelAndView getBlogById(ModelAndView model, PostDto mainPost, boolean isFull) {
        //get main blog

        model.addObject("mainPost", mainPost);
        model.addObject("isEditable", false);
        //Check if we should show edit and delete icons
        if (isFull && mainPost.getAuthor() != null) {
            Integer currentUserId = UserUtil.INSTANCE.getCurrentUserId();
            if (currentUserId != null && currentUserId == mainPost.getAuthor().getId()) {
                model.addObject("isEditable", true);
            }
        }

        List<PostDto> topViewBlogs = postService.findMostViewed(1, 4).getContent();
        // get  most recent 9 Posts
        List<PostDto> topRecentBlogs = postService.getTopRecentBlogs(4);
        // get  most viewed 9 Posts for slider
        model.addObject("sliders", topViewBlogs);

        model.addObject("searchText", "");

        //Popular Posts
        model.addObject("popularPosts", topViewBlogs);
        //recent Posts
        model.addObject("recentPosts", topRecentBlogs);
        Page<CategoryPostCount> categoryPostCounts = categoryService.getCategoryPostsCount(1, 20);
        model.addObject("categoryPostCounts", categoryPostCounts);
        model.addObject("user", new UserDto());
        model.addObject("isMyBlog", UserUtil.INSTANCE.getCurrentUserId() != null && postService.getNumberOfPostByUserId(UserUtil.INSTANCE.getCurrentUserId()) > 0);

        model.addObject("searchText", "");
        model.addObject("emailShare", new EmailShareDto());
        return model;
    }

    @GetMapping("/posts/{slugUrl}")
    public ModelAndView viewArticleBySlug(ModelAndView model,
                                          @PathVariable String slugUrl) {
        PostDto mainPost =  postService.getAndUpdateViewCountBySlugUrl(1, slugUrl);
        if(mainPost == null) {
            model.setViewName("error/404");
            return model;
        }
        model = getBlogById(model, mainPost, true);
        setAuthorRegisterModel(model);
        model.addObject("searchText", "");

        model.setViewName("blog/blog-article");
        return model;
    }


    @GetMapping("/share/email")
    public ModelAndView shareEmail(HttpServletRequest request,
                                   ModelAndView model, EmailShareDto emailShareDto,
                                   @RequestParam(value = "postId", required = false) Integer postId,
                                   @RequestParam(value = "lang", defaultValue = "EN") String language) {

        EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.BLOG_SHARE, language);

        emailTemplate.getAdditionalInformation().put("recipientName", emailShareDto.getRecipientName());
        emailTemplate.getAdditionalInformation().put("name", emailShareDto.getName());
        emailTemplate.getAdditionalInformation().put("recipientEmail", emailShareDto.getShareWithEmail());
        emailTemplate.getAdditionalInformation().put("sharedBy", emailShareDto.getEmail());
        emailTemplate.getAdditionalInformation().put("shareLink", emailShareDto.getShareUrl());
        emailTemplate.getAdditionalInformation().put("buttonText", "LovBlog Posted");

        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), emailShareDto.getShareWithEmail(), templateEngine,
                mailService, emailTemplate).send();
        model.setViewName("redirect:" + emailShareDto.getShareUrl());

        if(postId!=null)
        postService.shareBySocialProvider(postId, SocialType.EMAIl);
        return model;
    }

    @GetMapping("/submit")
    public ModelAndView submitArticle(ModelAndView model) {
        model.addObject("submitBlogCoverImage", cmsService.getImageUrlByTag(PageConstants.SUBMIT_BLOG_COVER));
        model.addObject("action", "/blog/post/save");
        List<CategoryDto> categories = categoryService.findActive();
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setName(messageByLocaleService.getMessage("blogs.submit.placeholder.category"));
        categoryDto.setId(0);
        if(categories.isEmpty()) {
            categories.add(categoryDto);
        } else {
            categories.add(0, categoryDto);
        }
        model.addObject("categories", categories);
        AuthorDto author = userService.getAuthor(UserUtil.INSTANCE.getCurrentUserId());

        if (author!=null && !author.getTweeted()) {try {


            //Tweet new status on lovappy account about new registered user
            Integer numberOfMales = userService.countUsersByGender(Gender.MALE);
            Integer numberOfFemales = userService.countUsersByGender(Gender.FEMALE);
            String address = author.getAddress() != null ? "from #" + author.getAddress() : "";
            TweetData tweetData = new TweetData(String.format("%d members males %d female %d new member %s", (numberOfMales + numberOfFemales), numberOfMales, numberOfFemales, address));
            twitterTemplate.timelineOperations().updateStatus(tweetData);
            userService.setTweeted(true, author.getId());
        } catch (Exception ex){

        }
        }

        PostDto post = new PostDto();
        if (author != null)
            post.setAuthor(author);
        model.addObject("categoryName", "Select a Category");
        model.addObject("post", post);
        model.addObject("isEdit", false);
        model.addObject("isMyBlog", UserUtil.INSTANCE.getCurrentUserId() != null && postService.getNumberOfPostByUserId(UserUtil.INSTANCE.getCurrentUserId()) > 0);

        model.addObject("searchText", "");
        model.setViewName("blog/submit-article");
        return model;
    }

    @GetMapping("/post/{post-id}/edit")
    public ModelAndView editArticle(@PathVariable("post-id") Integer postId, ModelAndView model) {
        PostDto postDto = postService.findById(postId);

        if(postDto == null) {
            model.setViewName("error/404");
            return model;
        }
        model.addObject("post", postDto);
        model.addObject("action", "/blog/post/" + postId + "/save");
        List<CategoryDto> categories = categoryService.findActive();
        model.addObject("categories", categories);
        model.addObject("isEdit", true);
        model.addObject("isMyBlog", UserUtil.INSTANCE.getCurrentUserId() != null && postService.getNumberOfPostByUserId(UserUtil.INSTANCE.getCurrentUserId()) > 0);
        model.addObject("submitBlogCoverImage", cmsService.getImageUrlByTag(PageConstants.SUBMIT_BLOG_COVER));
        model.addObject("searchText", "");
        model.setViewName("blog/submit-article");
        return model;
    }

    @GetMapping("/post/{post-id}/delete")
    public ModelAndView deleteArticle(@PathVariable("post-id") Integer postId, ModelAndView model) {
        postService.delete(postId);
        model.setViewName("redirect:/blog");
        return model;
    }


    @PostMapping(value = "/post/save")
    public String postSubmit(@Valid PostDto postDto, BindingResult err, Model model, RedirectAttributes redirectAttributes) {


        if (err.getErrorCount() > 0 || err.hasErrors()) {
            LOGGER.debug("==========ERROR============");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.postDto", err);
            redirectAttributes.addFlashAttribute("postDto", postDto);
            return "redirect:/blog/submit";

        }
        LOGGER.debug("==========Create Post============ {}" , postDto.getHtmlContent());
       // updateAuthorDetails(postDto.getZipCode(), postDto.getAuthorName(), authorImage);
        //setImageFileId(file, postDto);
        postDto.setDraft(false);
        postDto.setContent(postDto.getHtmlContent());
        postDto = postService.save(postDto, UserUtil.INSTANCE.getCurrentUserId());

        //send notification to admin
        NotificationDto notificationDto = new NotificationDto();
        notificationDto.setContent(UserUtil.INSTANCE.getCurrentUser().getEmail() + " submited a new blog ");
        notificationDto.setType(NotificationTypes.USER_BLOGS);
        notificationDto.setUserId(UserUtil.INSTANCE.getCurrentUser().getUserId());
        notificationDto.setSourceId(postDto.getID().longValue());
        notificationService.sendNotificationToAdmins(notificationDto);

        return "redirect:/blog?approved=true";
    }


    @PostMapping("/post/{post-id}/save")
    public String editPost(@PathVariable("post-id") Integer postId,
                           @Valid PostDto postDto, BindingResult err, Model model, RedirectAttributes redirectAttributes) {

        if (err.getErrorCount() > 0 || err.hasErrors()) {
            LOGGER.debug("==========ERROR============");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.postDto", err);
            redirectAttributes.addFlashAttribute("postDto", postDto);
            return "redirect:/blog/posterror";

        }
        LOGGER.debug("==========Update Post============");
        updateAuthorDetails(postDto);
        postDto.setDraft(false);
        postDto.setHtmlContent(StringUtils.replaceAll(postDto.getHtmlContent(), "[^\u0000-\u007F]", "&nbsp;"));
        postDto.setContent(postDto.getHtmlContent());
        postDto = postService.update(postId, postDto);
        return "redirect:/blog/posts/" + postDto.getSlugUrl();
    }


    @PostMapping("/post/draft/save")
    @ResponseBody
    public String saveDraft(PostDto postDto) {

        LOGGER.debug("==========Save Draft============");
        updateAuthorDetails(postDto);
        postDto.setDraft(true);
        postDto.setContent(postDto.getHtmlContent());
        postService.save(postDto, UserUtil.INSTANCE.getCurrentUserId());
        return "success";
    }


    @PostMapping("/search")
    public ModelAndView search(String searchText) {
        if (searchText == null)
            searchText = "";
        return search(1, searchText);
    }

    @GetMapping("/search")
    public ModelAndView search(@RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "searchText", defaultValue = "") String searchText) {
        ModelAndView model = new ModelAndView();

        if (page < 1) {
            throw new RuntimeException("Invalid Page Number");
        }
        Page<PostDto> postPage = postService.findPostByKeyword(searchText, new PageRequest(page - 1, 4));
        model.addObject("keyword", searchText);
        model.addObject("searchText", searchText);
        model.addObject("currentPage", page);
        model.addObject("postPage", postPage);
        model.addObject("user", new UserDto());

        Pager pager = new Pager(postPage.getTotalPages(), postPage.getNumber(), 5);


        model.addObject("pager", pager);
        // get  most recent 9 Posts
        List<PostDto> topRecentBlogs = postService.getTopRecentBlogs(4);


        List<PostDto> topViewBlogs = postService.findMostViewed(1, 4).getContent();

        // get  most viewed 9 Posts for slider
        model.addObject("sliders", topViewBlogs);
        //Popular Posts
        model.addObject("popularPosts", topViewBlogs);
        //recent Posts
        model.addObject("recentPosts", topRecentBlogs);

        Page<CategoryPostCount> categoryPostCounts = categoryService.getCategoryPostsCount(1, 20);
        model.addObject("categoryPostCounts", categoryPostCounts);
        model.addObject("emailShare", new EmailShareDto());
        model.addObject("isMyBlog", UserUtil.INSTANCE.getCurrentUserId() != null && postService.getNumberOfPostByUserId(UserUtil.INSTANCE.getCurrentUserId()) < 0);
        model.addObject("actionUrl", "/blog/search?searchText=" + searchText + "&");
        setAuthorRegisterModel(model);
        model.setViewName("blog/search-blog");
        return model;
    }

    private void setAuthorRegisterModel(ModelAndView model) {
        RegisterUserDto registerUserDto = new RegisterUserDto();
        registerUserDto.setNotificationType(NotificationTypes.AUTHOR_REGISTRATION);
        registerUserDto.setRole(Roles.AUTHOR);

        model.addObject("authorRegister", registerUserDto);
    }
    @GetMapping("/posts/lovappy")
    public ModelAndView search(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        ModelAndView model = new ModelAndView();

        if (page < 1) {
            throw new RuntimeException("Invalid Page Number");
        }
        Page<PostDto> postPage = postService.getAdminPosts(page, 4);
        model.addObject("keyword", "Lovappy");
        model.addObject("searchText", "");
        model.addObject("currentPage", page);
        model.addObject("postPage", postPage);
        model.addObject("user", new UserDto());

        Pager pager = new Pager(postPage.getTotalPages(), postPage.getNumber(), 5);

//        if(page > pager.getEndPage()) {
//            return new ModelAndView("redirect:/blog/lovappy/posts?page=" + pager.getEndPage());
//        } else  if(page < pager.getStartPage()) {
//            return new ModelAndView("redirect:/blog/lovappy/posts?page=" + pager.getStartPage());
//        }
        model.addObject("pager", pager);

        Page<CategoryPostCount> categoryPostCounts = categoryService.getCategoryPostsCount(1, 20);
        model.addObject("categoryPostCounts", categoryPostCounts);
        // get  most recent 9 Posts
        List<PostDto> topRecentBlogs = postService.getTopRecentBlogs(4);

        List<PostDto> topViewBlogs = postService.findMostViewed(1, 4).getContent();

        // get  most viewed 9 Posts for slider
        model.addObject("sliders", topViewBlogs);
        //Popular Posts
        model.addObject("popularPosts", topViewBlogs);
        //recent Posts
        model.addObject("recentPosts", topRecentBlogs);
        model.addObject("emailShare", new EmailShareDto());
        model.addObject("actionUrl", "/blog/posts/lovappy?");
        model.addObject("isMyBlog", UserUtil.INSTANCE.getCurrentUserId() != null && postService.getNumberOfPostByUserId(UserUtil.INSTANCE.getCurrentUserId()) < 0);
        setAuthorRegisterModel(model);
        model.setViewName("blog/search-blog");
        return model;
    }

    @GetMapping("/category/{category-id}/posts")
    public ModelAndView search(@PathVariable("category-id") Integer categoryId,
                               @RequestParam(value = "page", defaultValue = "1") Integer page) {
        ModelAndView model = new ModelAndView();

        if (page < 1) {
            throw new RuntimeException("Invalid Page Number");
        }
        CategoryDto categoryDto = categoryService.getOne(categoryId);
        if (categoryDto == null) {
            throw new RuntimeException("Invalid Category");
        }
        Page<PostDto> postPage = postService.getPostsByCategory(categoryId, page, 4);
        model.addObject("keyword", categoryDto.getName());
        model.addObject("searchText", "");
        model.addObject("currentPage", page);
        model.addObject("postPage", postPage);
        model.addObject("user", new UserDto());

        Pager pager = new Pager(postPage.getTotalPages(), postPage.getNumber(), 5);

//        if(page > pager.getEndPage()) {
//            return new ModelAndView("redirect:/blog/category/"+ categoryId + "/posts?page=" + pager.getEndPage());
//        } else  if(page < pager.getStartPage()) {
//            return new ModelAndView("redirect:/blog/category/"+ categoryId + "/posts?page=" + pager.getStartPage());
//        }
        model.addObject("pager", pager);

        Page<CategoryPostCount> categoryPostCounts = categoryService.getCategoryPostsCount(1, 20);
        model.addObject("categoryPostCounts", categoryPostCounts);

        // get  most recent 9 Posts
        List<PostDto> topRecentBlogs = postService.getTopRecentBlogs(4);

        List<PostDto> topViewBlogs = postService.findMostViewed(1, 4).getContent();

        // get  most viewed 9 Posts for slider
        model.addObject("sliders", topViewBlogs);
        //Popular Posts
        model.addObject("popularPosts", topViewBlogs);
        //recent Posts
        model.addObject("recentPosts", topRecentBlogs);
        model.addObject("emailShare", new EmailShareDto());
        model.addObject("actionUrl", "/blog/category/" + categoryId + "/posts?");
        model.addObject("isMyBlog", UserUtil.INSTANCE.getCurrentUserId() != null && postService.getNumberOfPostByUserId(UserUtil.INSTANCE.getCurrentUserId()) < 0);
        setAuthorRegisterModel(model);
        model.setViewName("blog/blog-category");
        return model;
    }

    @GetMapping("/posts/gallery")
    public ModelAndView getGallery(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        ModelAndView model = new ModelAndView();

        if (page < 1) {
            throw new RuntimeException("Invalid Page Number");
        }
        Page<PostDto> postPage = postService.getPostByType(BlogTypes.GALLERY, page, 4);
        model.addObject("keyword", "Gallery");
        model.addObject("searchText", "");
        model.addObject("currentPage", page);
        model.addObject("postPage", postPage);
        model.addObject("user", new UserDto());


        Pager pager = new Pager(postPage.getTotalPages(), postPage.getNumber(), 5);

        model.addObject("pager", pager);


        Page<CategoryPostCount> categoryPostCounts = categoryService.getCategoryPostsCount(1, 20);
        model.addObject("categoryPostCounts", categoryPostCounts);

        // get  most recent 9 Posts
        List<PostDto> topRecentBlogs = postService.getTopRecentBlogs(4);

        List<PostDto> topViewBlogs = postService.findMostViewed(1, 4).getContent();

        // get  most viewed 9 Posts for slider
        model.addObject("sliders", topViewBlogs);
        //Popular Posts
        model.addObject("popularPosts", topViewBlogs);
        //recent Posts
        model.addObject("recentPosts", topRecentBlogs);
        model.addObject("emailShare", new EmailShareDto());

        model.addObject("isMyBlog", UserUtil.INSTANCE.getCurrentUserId() != null && postService.getNumberOfPostByUserId(UserUtil.INSTANCE.getCurrentUserId()) < 0);
        model.addObject("actionUrl", "/blog/posts/gallery?");
        setAuthorRegisterModel(model);
        model.setViewName("blog/blog-gallery");
        return model;
    }

    @GetMapping("/posts/featured")
    public ModelAndView getFeaturedPosts(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        ModelAndView model = new ModelAndView();

        if (page < 1) {
            throw new RuntimeException("Invalid Page Number");
        }
        Page<PostDto> postPage = postService.getFeaturedPosts(page, 4);
        model.addObject("keyword", "Featured");
        model.addObject("searchText", "");
        model.addObject("currentPage", page);
        model.addObject("postPage", postPage);
        model.addObject("user", new UserDto());

        Pager pager = new Pager(postPage.getTotalPages(), postPage.getNumber(), 5);

        model.addObject("pager", pager);

        Page<CategoryPostCount> categoryPostCounts = categoryService.getCategoryPostsCount(1, 20);
        model.addObject("categoryPostCounts", categoryPostCounts);

        // get  most recent 9 Posts
        List<PostDto> topRecentBlogs = postService.getTopRecentBlogs(4);

        List<PostDto> topViewBlogs = postService.findMostViewed(1, 4).getContent();

        // get  most viewed 9 Posts for slider
        model.addObject("sliders", topViewBlogs);
        //Popular Posts
        model.addObject("popularPosts", topViewBlogs);
        //recent Posts
        model.addObject("recentPosts", topRecentBlogs);

        model.addObject("emailShare", new EmailShareDto());
        model.addObject("isMyBlog", UserUtil.INSTANCE.getCurrentUserId() != null && postService.getNumberOfPostByUserId(UserUtil.INSTANCE.getCurrentUserId()) < 0);
        model.addObject("actionUrl", "/blog/posts/featured?");
        setAuthorRegisterModel(model);
        model.setViewName("blog/search-blog");
        return model;
    }


    @GetMapping("/vlogs")
    public ModelAndView getVlogs(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        ModelAndView model = new ModelAndView();

        if (page < 1) {
            page = 1;
        }
        Page<VlogsDto> vlogsPage = vlogsService.getActiveVlogsList(page, 4);
        model.addObject("keyword", "Video");
        model.addObject("searchText", "");
        model.addObject("currentPage", page);
        model.addObject("videoPage", vlogsPage);
        model.addObject("user", new UserDto());


        Pager pager = new Pager(vlogsPage.getTotalPages(), vlogsPage.getNumber(), 5);

        model.addObject("pager", pager);

        Page<CategoryPostCount> categoryPostCounts = categoryService.getCategoryPostsCount(1, 20);
        model.addObject("categoryPostCounts", categoryPostCounts);
        model.addObject("emailShare", new EmailShareDto());
        // get  most recent 9 Posts
        List<PostDto> topRecentBlogs = postService.getTopRecentBlogs(4);

        List<PostDto> topViewBlogs = postService.findMostViewed(1, 4).getContent();

        // get  most viewed 9 Posts for slider
        model.addObject("sliders", topViewBlogs);
        //Popular Posts
        model.addObject("popularPosts", topViewBlogs);
        //recent Posts
        model.addObject("recentPosts", topRecentBlogs);


        model.addObject("isMyBlog", UserUtil.INSTANCE.getCurrentUserId() != null && postService.getNumberOfPostByUserId(UserUtil.INSTANCE.getCurrentUserId()) < 0);
        model.addObject("actionUrl", "/blog/vlogs?");
        setAuthorRegisterModel(model);
        model.setViewName("blog/blog-video");
        return model;
    }

    @GetMapping("/vlogs/{id}")
    public ModelAndView viewVlogDetail(ModelAndView model, @PathVariable Integer id) {

        VlogsDto vlogs = vlogsService.getVlogsById(id);
        if(vlogs == null) {
            model.setViewName("error/404");
            return model;
        }
        vlogsService.updateViewCount(1, id);

        model.addObject("mainVlog", vlogs);
        model.addObject("user", new UserDto());

        Page<CategoryPostCount> categoryPostCounts = categoryService.getCategoryPostsCount(1, 20);
        model.addObject("categoryPostCounts", categoryPostCounts);

        model.addObject("searchText", "");

        // get  most recent 9 Posts
        List<PostDto> topRecentBlogs = postService.getTopRecentBlogs(4);

        List<PostDto> topViewBlogs = postService.findMostViewed(1, 4).getContent();
        model.addObject("emailShare", new EmailShareDto());
        // get  most viewed 9 Posts for slider
        model.addObject("sliders", topViewBlogs);
        //Popular Posts
        model.addObject("popularPosts", topViewBlogs);
        //recent Posts
        model.addObject("recentPosts", topRecentBlogs);
        setAuthorRegisterModel(model);
        model.setViewName("blog/blog-video-details");
        return model;
    }


    @GetMapping("/my-blogs")
    public ModelAndView myBlogs(@RequestParam(value = "page", defaultValue = "1") Integer page, ModelAndView model) {

        Page<PostDto> postPage = postService.findPostByUserId(UserUtil.INSTANCE.getCurrentUserId(), page);
        model.addObject("postPage", postPage);
        model.addObject("currentPage", page);
        model.addObject("searchText", "");

        Pager pager = new Pager(postPage.getTotalPages(), postPage.getNumber(), 5);

        model.addObject("pager", pager);
        model.addObject("actionUrl", "/blog/my-blogs?");
        model.setViewName("blog/my-blog");
        return model;
    }


    @GetMapping("/my-blogs/{post-id}/delete")
    public ModelAndView deleteMyArticle(@PathVariable("post-id") Integer postId, ModelAndView model) {
        postService.delete(postId);
        model.setViewName("redirect:/blog/my-blogs");
        return model;
    }


    @GetMapping("/my-blogs/{post-id}/edit")
    public ModelAndView editMyArticle(@PathVariable("post-id") Integer postId, ModelAndView model) {
        PostDto postDto = postService.findById(postId);
        if(postDto.getAuthor()!=null) {
            UserDto user = userService.getUser(postDto.getAuthor().getId());
            postDto.setAuthorProfilePhoto(user.getUserProfile().getProfilePhotoCloudFile());
        }
        model.addObject("submitBlogCoverImage", cmsService.getImageUrlByTag(PageConstants.SUBMIT_BLOG_COVER));
        model.addObject("post", postDto);
        model.addObject("action", "/blog/my-blogs/" + postId + "/save");
        List<CategoryDto> categories = categoryService.findActive();
        model.addObject("categories", categories);

        model.addObject("isEdit", true);
        model.addObject("searchText", "");
        model.setViewName("blog/submit-article");
        return model;
    }

    @PostMapping("/my-blogs/{post-id}/save")
    public String editMyPost(@PathVariable("post-id") Integer postId,
                             @Valid PostDto postDto, BindingResult err, Model model, RedirectAttributes redirectAttributes) {


        if (err.getErrorCount() > 0 || err.hasErrors()) {
            LOGGER.debug("==========ERROR============");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.postDto", err);
            redirectAttributes.addFlashAttribute("postDto", postDto);
            return "redirect:/blog/posterror";

        }
        LOGGER.debug("==========Update Post============");

        updateAuthorDetails(postDto);

        postDto.setDraft(false);
        postDto.setContent(postDto.getHtmlContent());
        postService.update(postId, postDto);

        return "redirect:/blog/my-blogs";
    }


    private void updateAuthorDetails(PostDto postDto) {

        if (postDto != null && postDto.getAuthorProfilePhoto() !=null) {
            try {
                CloudStorageFileDto cloudStorageFileDto =
                        new CloudStorageFileDto(cloudStorageService.findById(postDto.getAuthorProfilePhoto().getId()));
                userService.updateAuthorDetails(UserUtil.INSTANCE.getCurrentUserId(), cloudStorageFileDto, postDto.getZipCode(), postDto.getAuthorName());
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
            }
        }

    }

    @GetMapping("/lovblog")
    public ModelAndView landingPage(ModelAndView model) {

        List<PostDto> posts = postService.getTopRecentBlogs(3);
        model.addObject("posts", posts);
        model.addObject("user", new UserDto());
        model.setViewName("blog/blog-landing");
        return model;
    }

    @GetMapping("/help")
    public ModelAndView howTo(ModelAndView model) {

        model.setViewName("blog/submit-blog-info");
        return model;
    }
}
