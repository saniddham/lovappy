package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.lovstamps.LovstampSampleService;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampSampleDto;
import com.realroofers.lovappy.service.lovstamps.support.SampleTypes;
import com.realroofers.lovappy.service.system.LanguageService;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.web.util.CloudStorage;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.io.IOException;

@Controller
@RequestMapping("/lox")
public class SystemController {

    private final LanguageService languageService;
    private final LovstampSampleService lovstampSampleService;
    private final CloudStorage cloudStorage;

    @Value("${lovappy.cloud.bucket.lovdropSamples}")
    private String recordingSamplesBucket;

    @Autowired
    public SystemController(LanguageService languageService, LovstampSampleService lovstampSampleService, CloudStorage cloudStorage) {
        this.languageService = languageService;
        this.lovstampSampleService = lovstampSampleService;
        this.cloudStorage = cloudStorage;
    }

    @GetMapping("/languages")
    public ModelAndView languages() {
        ModelAndView mav = new ModelAndView("admin/system/languages");
        mav.addObject("languages", languageService.getAllLanguages(null));
        return mav;
    }

    @GetMapping("/languages/new")
    public ModelAndView addNewLanguage() {
        ModelAndView mav = new ModelAndView("admin/system/language_details");
        mav.addObject("language", new LanguageDto());
        return mav;
    }

    @GetMapping("/languages/{id}")
    public ModelAndView languageDetails(@PathVariable(value = "id") Integer id) {
        ModelAndView mav = new ModelAndView("admin/system/language_details");
        mav.addObject("language", languageService.getLanguage(id));
        return mav;
    }

    @PostMapping("/languages/save")
    public ResponseEntity saveLanguage(@ModelAttribute("language") @Valid LanguageDto language) {
        if (language.getId() == null) {
            LanguageDto addLanguage = languageService.addLanguage(language);
            return ResponseEntity.status(HttpStatus.OK).body(addLanguage.getId());
        } else {
            LanguageDto updateLanguage = languageService.updateLanguage(language);
            return ResponseEntity.status(HttpStatus.OK).body(updateLanguage.getId());
        }
    }

    @PostMapping("/languages/change_status")
    public ResponseEntity changeStatus(@RequestParam("language_id") Integer languageID,
                                       @RequestParam("enabled") Boolean enabled) {
        LanguageDto updateLanguage = languageService.changeStatus(languageID, enabled);
        return ResponseEntity.status(HttpStatus.OK).body(updateLanguage.getId());
    }


    @GetMapping("/recording_samples")
    public ModelAndView recordingSamples() {
        ModelAndView mav = new ModelAndView("admin/system/recording_samples");
        mav.addObject("samples", lovstampSampleService.getAllRecordingSamplesAndType(SampleTypes.SAMPLE, new PageRequest(0, 100)).getContent());
        return mav;
    }

    @GetMapping("/recording_samples/new")
    public ModelAndView addNewRecordingSample() {
        ModelAndView mav = new ModelAndView("admin/system/recording_samples_details");
        mav.addObject("sample", new LovstampSampleDto());
        mav.addObject("languages", languageService.getAllLanguages(true));
        return mav;
    }

    @GetMapping("/recording_samples/{id}")
    public ModelAndView recordingSampleDetails(@PathVariable(value = "id") Integer id) {
        ModelAndView mav = new ModelAndView("admin/system/recording_samples_details");
        mav.addObject("sample", lovstampSampleService.getRecordingSample(id));
        mav.addObject("languages", languageService.getAllLanguages(true));
        return mav;
    }

    @PostMapping("/recording_samples/save")
    public ResponseEntity saveRecordingSample(@RequestParam("id") Integer id,
                                              @RequestParam("file") MultipartFile file,
                                              @RequestParam("language") Integer languageID,
                                              @RequestParam("gender") Gender gender,
                                              @RequestParam(value = "type", defaultValue = "SAMPLE") SampleTypes type) {
        try {
            CloudStorageFileDto cloudStorageFile = null;

            if (file != null && file.getSize() > 0) {
                cloudStorageFile = cloudStorage.uploadAndPersistFile(file, recordingSamplesBucket,
                        FilenameUtils.getExtension(file.getOriginalFilename()), file.getContentType());
            }

            Long fileID = cloudStorageFile == null ? null : cloudStorageFile.getId();
            LovstampSampleDto recordingSample =
                    lovstampSampleService.saveRecordingSample(id, fileID, languageID, gender, type);

            return ResponseEntity.status(HttpStatus.OK).body(recordingSample.getId());

        } catch (IOException ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
        }
    }
}
