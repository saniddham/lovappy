package com.realroofers.lovappy.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.realroofers.lovappy.service.blog.CommentService;
import com.realroofers.lovappy.service.blog.PostService;
import com.realroofers.lovappy.service.blog.RelayCommentService;
import com.realroofers.lovappy.service.blog.model.Comment;
import com.realroofers.lovappy.service.blog.model.Post;
import com.realroofers.lovappy.service.blog.model.RelayComment;

@org.springframework.stereotype.Controller
public class RelayCommentController {
//	
//	@Autowired
//	protected RelayCommentService relayCommentService;
//	
//	@Autowired
//	protected CommentService commentService;
//	
//
//	@Autowired
//	protected PostService postService;
//	/**
//	 * BUILD RELAY COMMENT
//	 * @param blogID
//	 * @param relaycomment
//	 * @param request
//	 * @return
//	 */
//	@RequestMapping(value = "/blog/{id}/publishRelayComment", method = RequestMethod.POST)
//	public ModelAndView publishRelayComment(@PathVariable(value = "id") Integer blogID,
//			@ModelAttribute("relaycomment") RelayComment relaycomment,
//			HttpServletRequest request){
//		int commentID=Integer.parseInt(request.getParameter("commentID"));
//		/**
//		 * find the comment
//		 */
//		List<Comment> list=commentService.getAllComments();
//		Comment comment=null;
//		for(Comment i:list){
//			if(i.getId()==commentID){
//				comment=i;
//				break;
//			}
//		}
//		relaycomment.setDate(new Date());
//		relaycomment.setComment(comment);
//		relayCommentService.save(relaycomment);
//		commentService.update(comment);
//		List<Post> postlist = postService.getAllPosts();
//		Post post = postlist.get(blogID - 1);
////		byte[] image = post.getImage();
//		List<RelayComment> relaylist=relayCommentService.getAllRelayComments();
//		ModelAndView mav = new ModelAndView("each-blog");
//		mav.addObject("commentModel",new Comment());
//		mav.addObject("relayCommentModel",new RelayComment());
//		mav.addObject("blog",post);
////		mav.addObject("image", new String(image));
//		mav.addObject("relaylist",relaylist);
//		return mav;
//	}
//
}
