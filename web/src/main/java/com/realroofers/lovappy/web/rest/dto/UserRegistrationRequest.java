package com.realroofers.lovappy.web.rest.dto;

import com.realroofers.lovappy.service.validator.UserRegister;
import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 4/22/2018.
 */
@Data
public class UserRegistrationRequest implements Serializable {
    @NotEmpty(message = "You should provide your email address.", groups = {UserRegister.class})
    @NotNull(message = "You should provide your email address.", groups = {UserRegister.class})
    @Email(message = "This is not a valid email address.", groups = {UserRegister.class})
    private String email;

    @NotEmpty(message = "Password cannot be empty.", groups = {UserRegister.class})
    @NotNull(message = "Password cannot be empty.", groups = {UserRegister.class})
    @Size(min = 6, message = "Min password length is 6.", groups = {UserRegister.class})
    private String password;

    private String confirmPassword;
    private Double latitude;

    private Double longitude;
}
