package com.realroofers.lovappy.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Daoud Shaheen on 8/18/2017.
 */
@Controller
@RequestMapping("/following")
public class FollowingController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FollowingController.class);


    @GetMapping()
    public String following(){
        return "following/following";
    }

}
