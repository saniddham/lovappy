package com.realroofers.lovappy.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.user.dto.UserLifeStylesDto;
import com.realroofers.lovappy.service.user.dto.UserPersonalityDto;
import com.realroofers.lovappy.service.user.dto.UserPreferenceDto;
import com.realroofers.lovappy.service.user.dto.UserStatusesDto;
import lombok.Data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Created by Daoud Shaheen on 6/18/2018.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)

public class UserCompleteProfileDto implements Serializable {
    private Integer heightFt;
    private Integer heightIn;
    private Double heightCm;
    private UserPreferenceDto preference;
    private Set<String> seekingLanguages;
    private Set<String> speakinglanguages;
    private UserStatusesDto statuses;
    private UserPersonalityDto personalities;
    private UserLifeStylesDto lifestyles;

    @Override
    public String toString() {
        return "UserCompleteProfileDto{" +
                "heightFt=" + heightFt +
                ", heightIn=" + heightIn +
                ", heightCm=" + heightCm +
                ", preference=" + preference +
                ", seekingLanguages=" + seekingLanguages +
                ", speakinglanguages=" + speakinglanguages +
                ", statuses=" + statuses +
                ", personalities=" + personalities +
                ", lifestyles=" + lifestyles +
                '}';
    }
}
