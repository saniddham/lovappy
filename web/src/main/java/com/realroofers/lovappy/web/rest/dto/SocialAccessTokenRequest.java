package com.realroofers.lovappy.web.rest.dto;

import com.realroofers.lovappy.service.user.model.Roles;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;

@Data
public class SocialAccessTokenRequest {

    @NotNull
    private String accessToken;

    private String accessTokenSecret;

    @NotNull
    private Roles role;

    private String email ;


    @Override
    public String toString() {
        return new ToStringBuilder(this).
                append("accessToken", accessToken).
                append("accessTokenSecret", accessTokenSecret).
                append("email", email).
                toString();
    }
}
