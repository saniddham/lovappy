package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.dto.DeveloperRegistration;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.web.config.security.UserAuthorizationService;
import com.realroofers.lovappy.web.rest.dto.LoginRequest;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by Daoud Shaheen on 5/11/2018.
 */
@Slf4j
@Controller
@RequestMapping("/developer")
public class DeveloperController {


    @Autowired
    private UserService userService;
    @Autowired
    private UserAuthorizationService userAuthorizationService;

    @Autowired
    private CMSService cmsService;

    @GetMapping()
    public ModelAndView developer (ModelAndView modelAndView) throws IOException {

        modelAndView.setViewName("redirect:/developer/login");
        return modelAndView;
    }
    @GetMapping("/login")
    public ModelAndView login (ModelAndView modelAndView, @ModelAttribute("error") String error) {
        modelAndView.getModelMap().addAttribute("error", error);
        modelAndView.addObject("loginRequest", new LoginRequest());
        modelAndView.setViewName("developer/developer-login");
        return modelAndView;
    }

    @PostMapping("/login")
    public ModelAndView postLogin (LoginRequest loginRequest, HttpServletRequest request, ModelAndView modelAndView) throws IOException {

        String status = userAuthorizationService.developerLogin(loginRequest.getUsername(), loginRequest.getPassword());
        if ("success".equals(status)) {
            // add authentication to the session
            request.getSession(true).setAttribute(
                    HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
                    SecurityContextHolder.getContext());


            modelAndView.setViewName("redirect:/jsondoc-ui.html?url=/jsondoc#");
            return modelAndView;
        }
        return new ModelAndView("redirect:/developer/login?error=" + status);
    }
    @GetMapping("/access")
    public ModelAndView developerAccess () throws IOException {
        ModelAndView modelAndView = new ModelAndView();
        Locale locale = LocaleContextHolder.getLocale();
        String language = locale.getLanguage();
        modelAndView.addObject("submitRequest", new DeveloperRegistration());
        modelAndView.addObject("devTerms", cmsService.findPageByTagWithImagesAndTexts("dev-terms", "en").getTexts());
        modelAndView.setViewName("developer/developer-access");
        return modelAndView;
    }

    @PostMapping("/access")
    public ModelAndView request (DeveloperRegistration registration, ModelAndView modelAndView) throws IOException {

        String status = userService.registerDeveloper(registration);
        if ("success".equals(status)) {
            modelAndView.setViewName("redirect:/developer/login");
            return modelAndView;
        }
        return new ModelAndView("redirect:/developer/login?error=" + status);
    }
}
