package com.realroofers.lovappy.web.support;

public enum FileExtension {
    webm, wav, mp3, jpg, png
}
