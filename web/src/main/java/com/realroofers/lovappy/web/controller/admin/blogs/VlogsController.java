package com.realroofers.lovappy.web.controller.admin.blogs;

import com.realroofers.lovappy.service.blog.VlogsService;
import com.realroofers.lovappy.service.blog.dto.VlogsDto;
import com.realroofers.lovappy.service.blog.support.PostStatus;
import com.realroofers.lovappy.web.util.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

/**
 * Created by Daoud Shaheen on 8/9/2017.
 */
@Controller()
@RequestMapping("/lox/vlogs")
public class VlogsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(VlogsController.class);

    @Autowired
    private VlogsService vlogsService;

    @GetMapping()
    public ModelAndView getVlogsTable(ModelAndView model,
                                      @RequestParam("pageSize") Optional<Integer> pageSize,
                                      @RequestParam("page") Optional<Integer> pageNumber,
                                      @RequestParam(value = "filter", defaultValue = "") String filter,
                                      @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){

        PageRequest pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize));
        if(!StringUtils.isEmpty(filter)) {
            if(pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize), new Sort(sort, filter));
            }
        }
        model.addObject("filter", filter);
        model.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);

        Page<VlogsDto> vlogsDtoPage = vlogsService.getSortedVlogsList(pageable);
         model.addObject("vlogsPage", vlogsDtoPage);

        model.addObject("newVlogs", new VlogsDto());

        Pager pager = new Pager(vlogsDtoPage.getTotalPages(), vlogsDtoPage.getNumber(), Pager.BUTTONS_TO_SHOW);
        model.addObject("selectedPageSize", pageSize.orElse(Pager.INITIAL_PAGE_SIZE));
        model.addObject("pageSizes", Pager.PAGE_SIZES);
        model.addObject("pager", pager);
         model.setViewName("admin/blog/manage_vlogs");
         return model;
    }

    @GetMapping("/{id}/activate")
    public ModelAndView activate(@PathVariable("id") Integer vlogId, ModelAndView model) {
        vlogsService.updateStateById(PostStatus.ACTIVE, vlogId);
        model.setViewName("redirect:/lox/vlogs");
        return model;
    }


    @GetMapping("/{id}/deactivate")
    public ModelAndView deactivate(@PathVariable("id") Integer vlogId, ModelAndView model) {
        vlogsService.updateStateById(PostStatus.INACTIVE, vlogId);
        model.setViewName("redirect:/lox/vlogs");
        return model;
    }


    @PostMapping("/save")
    public ModelAndView saveVlogs(@Valid VlogsDto vlogsDto, BindingResult err, ModelAndView model, RedirectAttributes redirectAttributes) {
//        if (err.hasErrors()) {
//            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.vlogs", err);
//            redirectAttributes.addFlashAttribute("category", vlogsDto);
//            model.setViewName("admin/blog/manage_vlogs");
//            return model;
//        }

        vlogsService.createVlogs(vlogsDto);
        model.setViewName("redirect:/lox/vlogs");
        return model;
    }

    @PostMapping("/{id}/save")
    public ModelAndView updateVlogs(@PathVariable("id") Integer vlogsId,
                                       @Valid VlogsDto vlogsDto, BindingResult err, ModelAndView model, RedirectAttributes redirectAttributes, Pageable pageable) {
//        if (err.hasErrors()) {
//
//            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.category", err);
//            redirectAttributes.addFlashAttribute("vlogs", vlogsDto);
//            model.setViewName("admin/blog/manage_vlogs");
//            return model;
//        }

        vlogsDto.setId(vlogsId);
        vlogsService.updateVlogs(vlogsDto);
        model.setViewName("redirect:/lox/vlogs");
        return model;
    }

    @GetMapping("/{id}/delete")
    public ModelAndView deleteVlogs(@PathVariable("id") Integer vlogsId, RedirectAttributes redirectAttributes) {


        vlogsService.deleteVlogsById(vlogsId);
        return new ModelAndView("redirect:/lox/vlogs");
    }
}
