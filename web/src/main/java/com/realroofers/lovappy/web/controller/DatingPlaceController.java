package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.datingPlaces.DatingPlaceService;
import com.realroofers.lovappy.service.datingPlaces.dto.*;
import com.realroofers.lovappy.service.datingPlaces.dto.mobile.DatingPlaceDealsMobileDto;
import com.realroofers.lovappy.service.datingPlaces.support.DatingPlaceSupportService;
import com.realroofers.lovappy.service.datingPlaces.support.DatingPlaceUtil;
import com.realroofers.lovappy.service.datingPlaces.support.google_places.DatingPlaceLocationUtil;
import com.realroofers.lovappy.service.exception.EntityValidationException;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.service.validation.ResponseStatus;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.PaymentUtil;
import com.squareup.connect.ApiException;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Daoud Shaheen on 9/20/2017.
 */
@Controller
@RequestMapping("/dating/places")
public class DatingPlaceController {

    private static final Logger LOG = LoggerFactory.getLogger(DatingPlaceController.class);
    private static final String GOOGLE_KEY = "google_key";

    private final DatingPlaceService datingPlaceService;
    private final CloudStorage cloudStorage;
    private final CloudStorageService cloudStorageService;
    private final UserService userService;
    private final NotificationService notificationService;
    private final PaymentUtil paymentUtil;
    private final DatingPlaceSupportService datingPlaceSupportService;
    private final DatingPlaceLocationUtil datingPlaceLocationUtil;

    @Autowired
    private OrderService orderService;

    @Value("${google.maps.api.key}")
    private String googleKeyId;

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @Value("${square.applicationId}")
    private String squareAppID;

    @Autowired
    public DatingPlaceController(DatingPlaceService datingPlaceService, CloudStorage cloudStorage,
                                 CloudStorageService cloudStorageService, UserService userService,
                                 NotificationService notificationService, PaymentUtil paymentUtil, DatingPlaceSupportService datingPlaceSupportService, DatingPlaceLocationUtil datingPlaceLocationUtil) {
        this.datingPlaceService = datingPlaceService;
        this.cloudStorage = cloudStorage;
        this.cloudStorageService = cloudStorageService;
        this.userService = userService;
        this.notificationService = notificationService;
        this.paymentUtil = paymentUtil;
        this.datingPlaceSupportService = datingPlaceSupportService;
        this.datingPlaceLocationUtil = datingPlaceLocationUtil;
    }

    @GetMapping
    public ModelAndView main(ModelAndView model, @RequestParam(value = "page", defaultValue = "1") Integer page,
                             @ModelAttribute("searchPlace") DatingPlaceFilterDto datingPlaceFilterDto, @ModelAttribute("location") LocationDto locationDto, HttpServletRequest request) throws IOException {
        Page<DatingPlaceDto> datingPlaceDtos;
        StringBuilder BASE_URL_FILTERS = new StringBuilder("/dating/places?");
        if (locationDto.getLatitude() != null && locationDto.getLongitude() != null) {
            BASE_URL_FILTERS.append("latitude=");
            BASE_URL_FILTERS.append(locationDto.getLatitude());
            BASE_URL_FILTERS.append("&longitude=");
            BASE_URL_FILTERS.append(locationDto.getLongitude());
        }
        BASE_URL_FILTERS.append("&gender=");
        BASE_URL_FILTERS.append(datingPlaceFilterDto.getGender().name());
        BASE_URL_FILTERS.append("&pricerange=");
        BASE_URL_FILTERS.append(datingPlaceFilterDto.getPriceRange());
        BASE_URL_FILTERS.append("&ageRange=");
        BASE_URL_FILTERS.append(datingPlaceFilterDto.getAgeRange());
        BASE_URL_FILTERS.append("&security=");
        BASE_URL_FILTERS.append(datingPlaceFilterDto.getSecurity().toString());
        BASE_URL_FILTERS.append("&parking=");
        BASE_URL_FILTERS.append(datingPlaceFilterDto.getParking().toString());
        BASE_URL_FILTERS.append("&transport=");
        BASE_URL_FILTERS.append(datingPlaceFilterDto.getTransport().toString());

        if (datingPlaceFilterDto.getFiltered() != null) {
            BASE_URL_FILTERS.append("&filtered=");
            BASE_URL_FILTERS.append(datingPlaceFilterDto.getFiltered());
        }

        if (datingPlaceFilterDto.getQuickFiltered() != null) {
            BASE_URL_FILTERS.append("&quickFiltered=");
            BASE_URL_FILTERS.append(datingPlaceFilterDto.getQuickFiltered());
        }

        if (datingPlaceFilterDto.getWorldWide() != null) {
            BASE_URL_FILTERS.append("&worldWide=");
            BASE_URL_FILTERS.append(datingPlaceFilterDto.getWorldWide());
        }

        if (datingPlaceFilterDto.getLocal() != null) {
            BASE_URL_FILTERS.append("&local=");
            BASE_URL_FILTERS.append(datingPlaceFilterDto.getLocal());
        }

        if (datingPlaceFilterDto.getFoodTypes() != null) {
            BASE_URL_FILTERS.append("&foodType=");
            //BASE_URL_FILTERS.append(datingPlaceFilterDto.getFoodType().replace(",", "+").replace("", "+"));
        }
        if (locationDto.getRadius() != null) {
            BASE_URL_FILTERS.append("&radius=");
            BASE_URL_FILTERS.append(locationDto.getRadius().intValue());
        }
        if (locationDto.getPlaceName() != null) {
            BASE_URL_FILTERS.append("&placeName=");
            BASE_URL_FILTERS.append(locationDto.getPlaceName().replace(" ", "+"));
        }
        if (datingPlaceFilterDto.getPetsAllowed() != null) {
            BASE_URL_FILTERS.append("&petsAllowed=");
            BASE_URL_FILTERS.append(datingPlaceFilterDto.getPetsAllowed());
        }
        if (datingPlaceFilterDto.getGoodForCouple() != null) {
            BASE_URL_FILTERS.append("&goodForCouple=");
            BASE_URL_FILTERS.append(datingPlaceFilterDto.getGoodForCouple());
        }
        if (datingPlaceFilterDto.getDealsActive() != null) {
            BASE_URL_FILTERS.append("&dealsActive=");
            BASE_URL_FILTERS.append(datingPlaceFilterDto.getDealsActive());
            if (datingPlaceFilterDto.getDealsActive()) {
                BASE_URL_FILTERS.append("&currentDate=");
                BASE_URL_FILTERS.append(datingPlaceFilterDto.getCurrentDate());
            }
        }
        if (page < 1) {
            throw new RuntimeException("Invalid Page Number");
        }
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        model.addObject("actionUrl", BASE_URL_FILTERS + "&");
        if (datingPlaceFilterDto.getQuickFiltered() != null && datingPlaceFilterDto.getQuickFiltered()) {
            datingPlaceDtos = datingPlaceService.findPlacesByQuickFilters(page, 10, datingPlaceFilterDto, locationDto);
        } else {
            datingPlaceDtos = datingPlaceService.findPlacesByAllFilters(new PageRequest(page - 1, 10), datingPlaceFilterDto, locationDto);
        }
        model.setViewName("dating_places/temp/dating-place-list");
        model.addObject("datingPlaces", datingPlaceDtos);
        model.addObject("userRoles", userService.getRolesForUser(userId));
        model.addObject("featuredDatingPlaces", datingPlaceService.getFeaturedPlaces(locationDto));
        model.addObject("latitude", locationDto.getLatitude());
        model.addObject("longitude", locationDto.getLongitude());
        model.addObject("atmospheres", datingPlaceSupportService.getAllAtmospheresEnabled());
        model.addObject("foodTypes", datingPlaceSupportService.getAllFoodTypesEnabled());
        model.addObject("personTypes", datingPlaceSupportService.getAllPersonTypesEnabled());
        model.addObject("featuredReview", datingPlaceService.findFeaturedReview());
        model.addObject("currentPage", page);
        model.addObject(GOOGLE_KEY, googleKeyId);
        request.getSession(true).setAttribute("currentPlaceUrl", UrlUtils.buildRequestUrl(request));
        return model;
    }

    @GetMapping("/{id}")
    public ModelAndView locationDetails(HttpServletRequest request, ModelAndView model, @PathVariable(value = "id") String id, Pageable
            pageable) throws IOException {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        model.addObject("currentPlaceUrl", request.getSession().getAttribute("currentPlaceUrl"));
        request.getSession(true).setAttribute("currentPlaceUrl", "/dating/places");
        if (DatingPlaceUtil.isInteger(id)) {
            Integer placeId = Integer.valueOf(id);
            DatingPlaceDto datingPlaceDto = datingPlaceService.findDatingPlaceById(placeId);
            if (datingPlaceDto == null) {
                model.setViewName("error/404");
                return model;
            } else {
                Page<DatingPlaceReviewDto> datingPlaceReviewDtos = datingPlaceService.getReviewsByDatingPlace(pageable, placeId);
                model.setViewName("dating_places/temp/dating-place-details");
                model.addObject("userId", userId);
                model.addObject("placeReviewCreate", new DatingPlaceReviewDto());
                model.addObject("personTypes", datingPlaceSupportService.getAllPersonTypesEnabled());
                model.addObject("datingPlaceReviewDtos", datingPlaceReviewDtos);
                model.addObject("userRoles", userService.getRolesForUser(userId));
                model.addObject(GOOGLE_KEY, googleKeyId);
                model.addObject("googleRating", datingPlaceDto.getGoogleAverage());
                model.addObject("datingPlace", datingPlaceDto);
                model.addObject("activeDeals", datingPlaceService.getActiveDealsByPlaceIdAndDate(pageable, placeId, new Date()));
                return model;
            }
        } else {

            DatingPlaceDto datingPlaceDto = datingPlaceService.findGoogleDatingPlaceById(id);
            if (datingPlaceDto == null) {
                model.setViewName("error/404");
                return model;
            } else {
                Page<DatingPlaceReviewDto> datingPlaceReviewDtos = datingPlaceService.getReviewsByGoogleDatingPlace(pageable, datingPlaceDto);
                model.setViewName("dating_places/temp/dating-place-details");
                model.addObject("userId", userId);
                model.addObject("placeReviewCreate", new DatingPlaceReviewDto());
                model.addObject("datingPlaceReviewDtos", datingPlaceReviewDtos);
                model.addObject("averageRating", datingPlaceDto.getOverAllRating());
                model.addObject("userRoles", userService.getRolesForUser(userId));
                model.addObject("googleRating", datingPlaceDto.getOverAllRating());
                model.addObject("personTypes", datingPlaceSupportService.getAllPersonTypesEnabled());
                model.addObject("averageSecurity", datingPlaceDto.getSecurityAverage());
                model.addObject("overAllRating", datingPlaceDto.getOverAllRating());
                model.addObject("averageParking", datingPlaceDto.getParkingAverage());
                model.addObject("averageParkingAccess", datingPlaceDto.getTransportAverage());
                model.addObject("maleCount", datingPlaceDto.getMaleCount());
                model.addObject("femaleCount", datingPlaceDto.getFemaleCount());
                model.addObject("bothCount", datingPlaceDto.getBothCount());
                model.addObject(GOOGLE_KEY, googleKeyId);
                model.addObject("datingPlace", datingPlaceDto);
                return model;
            }
        }
    }

    @RequestMapping(value = "/deals/{id}/date={start}/page={page}", method = RequestMethod.GET)
    public ResponseEntity<Page<DatingPlaceDealsMobileDto>> getActivePlaceDeals(@PathVariable(value = "page") Integer page,
                                                                               @PathVariable(value = "id") Integer placeId,
                                                                               @PathVariable(value = "start") Long date) {
        if (page < 1) {
            throw new RuntimeException("Invalid Page Number");
        }
        try {
            Page<DatingPlaceDealsMobileDto> datingPlaceDealsDtos = datingPlaceService.getActiveDealsByPlaceIdAndDateMobile(new PageRequest(page - 1, 1), placeId, new Date(date));
            return new ResponseEntity<>(datingPlaceDealsDtos, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/add")
    public ModelAndView addLocationDetails(ModelAndView modelAndView,
                                           @RequestParam(value = "type", required = false) String type) {

        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        modelAndView.setViewName("dating_places/temp/add-dating-place");
        modelAndView.addObject("isFeatured", Objects.equals(type, "featured"));
        modelAndView.addObject("userId", userId);
        modelAndView.addObject("placeCreate", new DatingPlaceDto());
        modelAndView.addObject("userRoles", userService.getRolesForUser(userId));
        modelAndView.addObject("atmospheres", datingPlaceSupportService.getAllAtmospheresEnabled());
        modelAndView.addObject("foodTypes", datingPlaceSupportService.getAllFoodTypesEnabled());
        modelAndView.addObject(GOOGLE_KEY, googleKeyId);
        modelAndView.addObject("squareAppID", squareAppID);
        return modelAndView;
    }

    @GetMapping("/landing")
    public ModelAndView landingPage(ModelAndView modelAndView) {
        modelAndView.setViewName("dating_places/landing-date-places");
        modelAndView.addObject("place", datingPlaceService.findPlaceWithHighestRating());
        return modelAndView;
    }

    @PostMapping("/validate/create")
    public ResponseEntity validateAddDatingPlace(@ModelAttribute("placeCreate") DatingPlaceDto datingPlaceDto,
                                                 @RequestParam("placesAttachmentDtos[0].picture.id") Integer locationPictureId,
                                                 @RequestParam("datingPlaceReviewDtos[0].reviewerComment") String reviewComment,
                                                 @RequestParam("datingPlaceReviewDtos[0].securityRating") Double securityRating,
                                                 @RequestParam("datingPlaceReviewDtos[0].parkingRating") Double parkingRating,
                                                 @RequestParam("datingPlaceReviewDtos[0].parkingAccessRating") Double parkingAccessRating) throws IOException {
        HashMap<String, List<ErrorMessage>> messages = DatingPlaceUtil.isValidDatingPlace(
                datingPlaceDto, locationPictureId, userService);
        String searchString = datingPlaceDto.getPlaceName() + " " + datingPlaceDto.getAddressLine();
        DatingPlaceDto existingPlace = datingPlaceService.findGoogleDatingPlaceByQuery(searchString);
        List<ErrorMessage> errorMessages = messages.get("errorMessages");
        if (datingPlaceDto.getUserType().equalsIgnoreCase("reviewer")) {
            if (reviewComment.isEmpty() || securityRating == null ||
                    parkingAccessRating == null || parkingRating == null) {
                errorMessages.add(new ErrorMessage("reviewComment",
                        "Please enter a review & mention ratings"));
            }
        } else if (datingPlaceDto.getUserType().equalsIgnoreCase("manager")) {
            if (datingPlaceDto.getPlaceAtmospheres() == null) {
                errorMessages.add(new ErrorMessage("Atmosphere", "Select at least one Atmosphere"));
            }
            if (datingPlaceDto.getFoodTypes() == null) {
                errorMessages.add(new ErrorMessage("Food Type", "Select the Food types available"));
            }
            if (datingPlaceDto.getIsPetsAllowed() == null) {
                errorMessages.add(new ErrorMessage("Pets", "Select if pets are allowed"));
            }
            if (datingPlaceDto.getIsFeatured() == null) {
                errorMessages.add(new ErrorMessage("Featured", "Select if you want to feature your place"));
            } else {
                if (datingPlaceDto.getIsFeatured()) {
                    if (!datingPlaceDto.getIsMapFeatured() && !datingPlaceDto.getIsLovStampFeatured()) {
                        errorMessages.add(new ErrorMessage("manage", "Please select a Feature Type"));
                    }
                }
            }
        }
        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        } else if (existingPlace.getDatingPlacesId() != null) {
            return new ResponseEntity<>(existingPlace, HttpStatus.CONFLICT);
        } else {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @PostMapping("/create")
    public ResponseEntity addDatingPlace(@ModelAttribute("placeCreate") DatingPlaceDto datingPlaceDto,
                                         @RequestParam("placesAttachmentDtos[0].picture.id") Long locationPictureId,
                                         @RequestParam("placesAttachmentDtos[0].imageDescription") String imageDescription,
                                         @RequestParam("coverPicture.id") Long coverPictureId,
                                         @RequestParam("datingPlaceReviewDtos[0].reviewerComment") String reviewComment,
                                         @RequestParam("datingPlaceReviewDtos[0].securityRating") Double securityRating,
                                         @RequestParam("datingPlaceReviewDtos[0].parkingRating") Double parkingRating,
                                         @RequestParam("datingPlaceReviewDtos[0].parkingAccessRating") Double parkingAccessRating,
                                         @RequestParam("nonce") String nonce,
                                         @RequestParam("coupon") String coupon) throws IOException {
        JsonResponse response = new JsonResponse();
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        List<PlacesAttachmentDto> placesAttachmentDtoList = getLocationPictureList(locationPictureId, imageDescription);
        datingPlaceDto.setPlacesAttachmentDtos(placesAttachmentDtoList);
        datingPlaceDto.setCoverPicture(getImage(coverPictureId));
        if (userId != null) {
            datingPlaceDto.setCreateEmail(userService.getUser(userId).getEmail());
        } else {
            //User is logged out
            UserDto userDto = new UserDto();
            userDto.setEmail(datingPlaceDto.getCreateEmail());
            userDto.setPassword(datingPlaceDto.getPassword());
            userDto.setZipcode(datingPlaceDto.getZipCode());

            User newUser = userService.registerNewUser(userDto);
            userId = newUser.getUserId();
            NotificationDto notificationDto = new NotificationDto();
            notificationDto.setContent(newUser.getEmail() + " is new member in lovappy ");
            notificationDto.setType(NotificationTypes.USER_REGISTRATION);
            notificationDto.setUserId(newUser.getUserId());
            notificationService.sendNotificationToAdmins(notificationDto);
        }
        DatingPlaceDto savedDatingPlaceDto;
        List<DatingPlaceReviewDto> datingPlaceReviewDtoList = new ArrayList<>();
        DatingPlaceReviewDto datingPlaceReviewDto = new DatingPlaceReviewDto();
        datingPlaceReviewDto.setReviewerEmail(datingPlaceDto.getCreateEmail());
        datingPlaceReviewDto.setReviewerComment(reviewComment);
        datingPlaceReviewDto.setSecurityRating(securityRating);
        datingPlaceReviewDto.setParkingRating(parkingRating);
        datingPlaceReviewDto.setParkingAccessRating(parkingAccessRating);
        if (datingPlaceDto.getUserType().equalsIgnoreCase("reviewer")) {

            datingPlaceReviewDto.setReviewApprovalStatus(ApprovalStatus.PENDING);
            datingPlaceReviewDto.setReviewerType("user");
            datingPlaceReviewDtoList.add(datingPlaceReviewDto);
            datingPlaceDto.setDatingPlaceReviewDtos(datingPlaceReviewDtoList);
            savedDatingPlaceDto = datingPlaceService.addDatingPlace(datingPlaceDto);

            if (savedDatingPlaceDto != null) {
                NotificationDto notificationDto = new NotificationDto();
                notificationDto.setContent(savedDatingPlaceDto.getPlaceName() + " is a new Dating Place in LovAppy");
                notificationDto.setType(NotificationTypes.PLACE_APPROVAL);
                notificationDto.setUserId(userId);
                notificationService.sendNotificationToAdmins(notificationDto);
            }

        } else if (datingPlaceDto.getUserType().equalsIgnoreCase("manager")) {

            if (datingPlaceDto.getIsFeatured()) {
                OrderDto order = datingPlaceService.addFeaturedDatingPlace(datingPlaceDto, coupon, PaymentMethodType.SQUARE);
                if (order != null) {
                    Boolean executed = null;
                    try {
                        executed = paymentUtil.executeTransaction(nonce, order, null);
                    } catch (ApiException e) {
                        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
                    }
                    if (executed) {
                        NotificationDto notificationDto = new NotificationDto();
                        notificationDto.setContent(datingPlaceDto.getPlaceName() + " is a new Dating Place in LovAppy");
                        notificationDto.setType(NotificationTypes.PLACE_APPROVAL);
                        notificationDto.setUserId(userId);
                        notificationService.sendNotificationToAdmins(notificationDto);
                    } else {
                        orderService.revertOrder(order.getId());
                        response.setStatus(ResponseStatus.FAIL);
                        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
                    }
                }

            } else {
                datingPlaceReviewDto.setReviewApprovalStatus(ApprovalStatus.APPROVED);
                datingPlaceReviewDto.setReviewerType("owner");
                datingPlaceReviewDtoList.add(datingPlaceReviewDto);
                datingPlaceDto.setDatingPlaceReviewDtos(datingPlaceReviewDtoList);
                savedDatingPlaceDto = datingPlaceService.addDatingPlace(datingPlaceDto);
                if (savedDatingPlaceDto != null) {
                    NotificationDto notificationDto = new NotificationDto();
                    notificationDto.setContent(savedDatingPlaceDto.getPlaceName() + " is a new Dating Place in LovAppy");
                    notificationDto.setType(NotificationTypes.PLACE_APPROVAL);
                    notificationDto.setUserId(userId);
                    notificationService.sendNotificationToAdmins(notificationDto);
                }
            }
        }
        response.setStatus(ResponseStatus.SUCCESS);
        LOG.debug("Dating Place saved successfully");
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/show/markers/{latitude}/{longitude}/info")
    public ResponseEntity<List<LocationDto>> showDatingPlaces(@PathVariable("latitude") BigDecimal latitude, @PathVariable("longitude") BigDecimal longitude) {
        final List<LocationDto> locationDtoList = new ArrayList<>();

        List<DatingPlaceDto> datingPlaceDtos = datingPlaceService.getAllNearByPlaces(latitude.doubleValue(), longitude.doubleValue());
        datingPlaceDtos.stream().forEach(datingPlaceDto -> {
            LocationDto locationDto = new LocationDto(datingPlaceDto.getDatingPlacesId(), datingPlaceDto.getLatitude(), datingPlaceDto.getLongitude(), datingPlaceDto.getIsFeatured(), datingPlaceDto.getPlaceName());
            locationDtoList.add(locationDto);
        });
        return new ResponseEntity<List<LocationDto>>(locationDtoList, HttpStatus.OK);
    }

    @PostMapping("/show/markers/{latitude}/{longitude}/{placeName}/info")
    public ResponseEntity<List<LocationDto>> showDatingPlacesFilteredByName(@PathVariable("latitude") Double latitude, @PathVariable("longitude") Double longitude, @PathVariable("placeName") String placeName) {
        final List<LocationDto> locationDtoList = new ArrayList<>();

        List<DatingPlaceDto> datingPlaceDtos = datingPlaceService.getAllNearByPlacesByNameAndLocation(latitude, longitude, placeName);
        datingPlaceDtos.stream().forEach(datingPlaceDto -> {
            LocationDto locationDto1 = new LocationDto(datingPlaceDto.getDatingPlacesId(), datingPlaceDto.getLatitude(), datingPlaceDto.getLongitude(), datingPlaceDto.getIsFeatured(), datingPlaceDto.getPlaceName());
            locationDtoList.add(locationDto1);
        });
        return new ResponseEntity<List<LocationDto>>(locationDtoList, HttpStatus.OK);
    }

    @PostMapping("/show/markers/{radius}/{cityName}/{latitude}/{longitude}/info")
    public ResponseEntity<List<LocationDto>> showDatingPlacesFilteredByCityName(@PathVariable("radius") Double radius, @PathVariable("cityName") String cityName, @PathVariable("latitude") Double latitude, @PathVariable("longitude") Double longitude) {
        final List<LocationDto> locationDtoList = new ArrayList<>();

        List<DatingPlaceDto> datingPlaceDtos = datingPlaceService.getAllNearByPlacesByCityAndRadius(latitude, longitude, radius, cityName);
        datingPlaceDtos.stream().forEach(datingPlaceDto -> {
            LocationDto locationDto1 = new LocationDto(datingPlaceDto.getDatingPlacesId(), datingPlaceDto.getLatitude(), datingPlaceDto.getLongitude(), datingPlaceDto.getIsFeatured(), datingPlaceDto.getPlaceName());
            locationDtoList.add(locationDto1);
        });
        return new ResponseEntity<List<LocationDto>>(locationDtoList, HttpStatus.OK);
    }


    @PostMapping("/deals/active/{placeId}")
    public ResponseEntity<Integer> getActiveDealsByPlace(@PathVariable("placeId") String id, @RequestParam("today") Long date) {
        Date startDate = new Date(date);

        if (DatingPlaceUtil.isInteger(id)) {
            Integer dealsCount = datingPlaceService.getActiveDealsCountByPlaceIdAndDate(Integer.parseInt(id), startDate);
            return new ResponseEntity<Integer>(dealsCount, HttpStatus.OK);
        } else {
            return new ResponseEntity<Integer>(0, HttpStatus.OK);
        }
    }

    @PostMapping("/create/upload_dating-place-picture")
    public ResponseEntity uploadCoverImage(@RequestParam("dating_place_picture") MultipartFile picture, @RequestParam("image_description") String description) {
        List<ErrorMessage> errorMessages = new ArrayList<>();
        if (picture.isEmpty())
            errorMessages.add(new ErrorMessage("cover", "Add a location picture."));

        if (description == null || description.isEmpty()) {
            errorMessages.add(new ErrorMessage("imageDescription", "Add a image description"));
        }

        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        try {
            CloudStorageFileDto coverFileDto = cloudStorage.uploadFile(picture, imagesBucket, FilenameUtils.getExtension(picture.getOriginalFilename()), picture.getContentType());
            CloudStorageFileDto added = cloudStorageService.add(coverFileDto);
            return ResponseEntity.ok(added);

        } catch (IOException e) {
            LOG.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/upload/upload-dating-place-pictures")
    public ResponseEntity<Integer> uploadDatingPlacePictures(@RequestParam("files") MultipartFile[] pictures) {

        List<ErrorMessage> errorMessages = new ArrayList<>();
        if (pictures.length == 0)
            errorMessages.add(new ErrorMessage("uploadDatingPictures", "Add atleast ONE Location picture."));

        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        List<CloudStorageFileDto> list = new ArrayList<>();
        try {
            for (MultipartFile picture : pictures) {
                CloudStorageFileDto cloudStorageFileDto =
                        cloudStorage.uploadImageWithThumbnails(picture, imagesBucket, FileExtension.jpg.toString(), "image/jpeg");
                CloudStorageFileDto added = cloudStorageService.add(cloudStorageFileDto);
                list.add(added);
            }
            LOG.debug("Dating Location Photos saved to cloud storage");
            //eventService.addEventLocationPictures(UserUtil.INSTANCE.getCurrentUserId(), eventId, list);
            return ResponseEntity.ok(list.size());

        } catch (IOException e) {
            LOG.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/create/place-review/{id}")
    public ResponseEntity addDatingPlacereview(@ModelAttribute DatingPlaceReviewDto
                                                       datingPlaceReviewDto, @PathVariable("id") String placeId) throws IOException {
        JsonResponse response = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        DatingPlaceDto datingPlaceDto = new DatingPlaceDto();

        if (DatingPlaceUtil.isInteger(placeId)) {
            Integer id = Integer.parseInt(placeId);
            datingPlaceDto = datingPlaceService.findDatingPlaceById(id);
            datingPlaceReviewDto.setPlaceId(datingPlaceDto.getDatingPlace());
        } else {
            datingPlaceReviewDto.setGooglePlaceId(placeId);
            datingPlaceDto = datingPlaceLocationUtil.getGooglePlaceDetails(placeId);
        }

        if (userId != null) {
            datingPlaceReviewDto.setReviewerEmail(userService.getUser(userId).getEmail());
            datingPlaceReviewDto.setPassword("");
            if ((datingPlaceDto.getOwner() != null) && Objects.equals(userId, datingPlaceDto.getOwner().getUserId())) {
                errorMessages.add(new ErrorMessage("reviewerComment", "You cannot review your own place"));
            }
        } else {
            if (userService.emailExists(datingPlaceReviewDto.getReviewerEmail())) {
                errorMessages.add(new ErrorMessage("createEmail", "You are already registered."));
            } else {
                UserDto userDto = new UserDto();
                userDto.setEmail(datingPlaceReviewDto.getReviewerEmail());
                userDto.setPassword(datingPlaceReviewDto.getPassword());

                if (datingPlaceDto.getZipCode() != null) {
                    userDto.setZipcode(datingPlaceDto.getZipCode());
                } else {
                    userDto.setZipcode(datingPlaceReviewDto.getReviewZipCode());
                }

                User newUser = userService.registerNewUser(userDto);
                userId = newUser.getUserId();
                NotificationDto notificationDto = new NotificationDto();
                notificationDto.setContent(newUser.getEmail() + " is new member in lovappy ");
                notificationDto.setType(NotificationTypes.USER_REGISTRATION);
                notificationDto.setUserId(newUser.getUserId());
                notificationService.sendNotificationToAdmins(notificationDto);
            }
        }

        if (datingPlaceReviewDto.getReviewerEmail().isEmpty()) {
            errorMessages.add(new ErrorMessage("createEmail", "Provide your Email Address"));
        }
        if (!DatingPlaceUtil.isValidateEmail(datingPlaceReviewDto.getReviewerEmail())) {
            errorMessages.add(new ErrorMessage("createEmail", "Provide a valid Email Address"));
        }
        if (userId == null) {
            if (datingPlaceReviewDto.getPassword().isEmpty()) {
                errorMessages.add(new ErrorMessage("password", "Provide a password"));
            }
        }
        if (datingPlaceReviewDto.getReviewerComment().isEmpty()) {
            errorMessages.add(new ErrorMessage("reviewerComment", "Provide a review"));
        }
        if (datingPlaceReviewDto.getSecurityRating() == null || datingPlaceReviewDto.getParkingAccessRating() == null || datingPlaceReviewDto.getParkingRating() == null) {
            errorMessages.add(new ErrorMessage("rating", "Select a rating"));
        }

        if (datingPlaceReviewDto.getSuitableAgeRange() == null || datingPlaceReviewDto.getSuitableGender() == null || datingPlaceReviewDto.getSuitablePriceRange() == null) {
            errorMessages.add(new ErrorMessage("suitable", "Select suitable options"));
        }

        if (datingPlaceReviewDto.getOverAllRating() == null) {

            errorMessages.add(new ErrorMessage("overall", "Select an over all rating"));
        }
        if (errorMessages.size() > 0) {
            response.setErrorMessageList(errorMessages);
            response.setStatus(ResponseStatus.FAIL);
            throw new EntityValidationException(errorMessages);
        } else {
            DatingPlaceReviewDto datingPlaceReviewDto1 = datingPlaceService.addDatingPlaceReview(datingPlaceReviewDto, datingPlaceDto);
            if (datingPlaceReviewDto1 != null) {
                NotificationDto notificationDto = new NotificationDto();
                if (datingPlaceReviewDto1.getPlaceId() != null) {
                    notificationDto.setContent("A new review for " + datingPlaceReviewDto1.getPlaceId().getPlaceName() + " has been added");
                } else if (datingPlaceReviewDto1.getGooglePlaceId() != null) {
                    notificationDto.setContent("A new review for " + datingPlaceReviewDto1.getGooglePlaceId() + " has been added");
                }
                notificationDto.setType(NotificationTypes.REVIEW_APPROVAL);
                notificationDto.setUserId(userId);
                notificationService.sendNotificationToAdmins(notificationDto);
            }
            response.setStatus(ResponseStatus.SUCCESS);
            LOG.debug("Dating Place Review saved successfully");
            return ResponseEntity.ok().body(response);
        }
    }

    @PostMapping("/create/place-review/validation/{id}")
    public ResponseEntity addDatingPlaceReviewValidation(@ModelAttribute DatingPlaceReviewDto
                                                                 datingPlaceReviewDto, @PathVariable("id") String placeId) {
        JsonResponse response = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();
        DatingPlaceDto datingPlaceDto;

        if (StringUtils.isNumeric(placeId)) {
            Integer id = Integer.parseInt(placeId);
            datingPlaceDto = datingPlaceService.findDatingPlaceById(id);
            datingPlaceReviewDto.setPlaceId(datingPlaceDto.getDatingPlace());
        } else {
            datingPlaceReviewDto.setGooglePlaceId(placeId);
        }
        if (datingPlaceReviewDto.getReviewerComment().isEmpty()) {
            errorMessages.add(new ErrorMessage("reviewerComment", "Provide a review"));
        }
        if (datingPlaceReviewDto.getSecurityRating() == null || datingPlaceReviewDto.getParkingAccessRating() == null || datingPlaceReviewDto.getParkingRating() == null) {
            errorMessages.add(new ErrorMessage("rating", "Select a rating"));
        }
        if (datingPlaceReviewDto.getSuitableAgeRange() == null || datingPlaceReviewDto.getSuitableGender() == null || datingPlaceReviewDto.getSuitablePriceRange() == null) {
            errorMessages.add(new ErrorMessage("suitable", "Select suitable options"));
        }
        if (datingPlaceReviewDto.getOverAllRating() == null) {

            errorMessages.add(new ErrorMessage("overall", "Select an over all rating"));
        }
        if (errorMessages.size() > 0) {
            response.setErrorMessageList(errorMessages);
            response.setStatus(ResponseStatus.FAIL);
            throw new EntityValidationException(errorMessages);
        } else {
            response.setStatus(ResponseStatus.SUCCESS);
            return ResponseEntity.ok().body(response);
        }
    }

    @PostMapping("/create/place-review/sign/out/{id}")
    public ResponseEntity addDatingPlaceReviewLoggedOut(@ModelAttribute DatingPlaceReviewDto
                                                                datingPlaceReviewDto, @PathVariable("id") String placeId) throws IOException {
        JsonResponse response = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();
        DatingPlaceDto datingPlaceDto = new DatingPlaceDto();

        if (StringUtils.isNumeric(placeId)) {
            Integer id = Integer.parseInt(placeId);
            datingPlaceDto = datingPlaceService.findDatingPlaceById(id);
            datingPlaceReviewDto.setPlaceId(datingPlaceDto.getDatingPlace());
        } else {
            datingPlaceReviewDto.setGooglePlaceId(placeId);
            datingPlaceDto = datingPlaceLocationUtil.getGooglePlaceDetails(placeId);
        }

        if (datingPlaceReviewDto.getReviewerEmail() == null || datingPlaceReviewDto.getReviewerEmail().isEmpty()) {
            errorMessages.add(new ErrorMessage("createEmail", "Provide your Email Address"));
        } else {
            if (!DatingPlaceUtil.isValidateEmail(datingPlaceReviewDto.getReviewerEmail())) {
                errorMessages.add(new ErrorMessage("createEmail", "Provide a valid Email Address"));
            }
            if (userService.emailExists(datingPlaceReviewDto.getReviewerEmail())) {
                errorMessages.add(new ErrorMessage("createEmail", "You are already registered."));
            }
            if ((datingPlaceDto.getOwner() != null) && Objects.equals(datingPlaceReviewDto.getReviewerEmail(),
                    datingPlaceDto.getOwner().getEmail())) {
                errorMessages.add(new ErrorMessage("reviewerComment", "You cannot review your own place"));
            }
        }
        if (datingPlaceReviewDto.getPassword() == null || datingPlaceReviewDto.getPassword().isEmpty()) {
            errorMessages.add(new ErrorMessage("password", "Provide a password"));
        }

        if (errorMessages.size() > 0) {
            response.setErrorMessageList(errorMessages);
            response.setStatus(ResponseStatus.FAIL);
            throw new EntityValidationException(errorMessages);
            //return new ResponseEntity<>(new EntityValidationException(errorMessages), HttpStatus.INTERNAL_SERVER_ERROR);
        } else {

            //Create New User
            UserDto userDto = new UserDto();
            userDto.setEmail(datingPlaceReviewDto.getReviewerEmail());
            userDto.setPassword(datingPlaceReviewDto.getPassword());

            if (datingPlaceDto.getZipCode() != null) {
                userDto.setZipcode(datingPlaceDto.getZipCode());
            } else {
                userDto.setZipcode(datingPlaceReviewDto.getReviewZipCode());
            }
            User newUser = userService.registerNewUser(userDto);
            Integer userId = newUser.getUserId();
            NotificationDto notificationDto = new NotificationDto();
            notificationDto.setContent(newUser.getEmail() + " is new member in lovappy ");
            notificationDto.setType(NotificationTypes.USER_REGISTRATION);
            notificationDto.setUserId(newUser.getUserId());
            notificationService.sendNotificationToAdmins(notificationDto);

            //Add Place Review
            DatingPlaceReviewDto datingPlaceReviewDto1 = datingPlaceService.addDatingPlaceReview(datingPlaceReviewDto, datingPlaceDto);
            if (datingPlaceReviewDto1 != null) {
                NotificationDto newNotification = new NotificationDto();
                if (datingPlaceReviewDto1.getPlaceId() != null) {
                    newNotification.setContent("A new review for " + datingPlaceReviewDto1.getPlaceId().getPlaceName() + " has been added");
                } else if (datingPlaceReviewDto1.getGooglePlaceId() != null) {
                    newNotification.setContent("A new review for " + datingPlaceReviewDto1.getGooglePlaceId() + " has been added");
                }
                newNotification.setType(NotificationTypes.REVIEW_APPROVAL);
                newNotification.setUserId(userId);
                notificationService.sendNotificationToAdmins(newNotification);
            }
            response.setStatus(ResponseStatus.SUCCESS);
            return ResponseEntity.ok().body(response);
        }
    }

    @PostMapping("/social")
    @ResponseBody
    public ResponseEntity datingPlacesRegistration(HttpServletRequest
                                                           request, @ModelAttribute("placeCreate") DatingPlaceDto datingPlaceDto,
                                                   @RequestParam("placesAttachmentDtos[0].picture.id") Long locationPictureId,
                                                   @RequestParam("coverPicture.id") Long coverPictureId,
                                                   @RequestParam("datingPlaceReviewDtos[0].reviewerComment") String reviewComment,
                                                   @RequestParam("datingPlaceReviewDtos[0].securityRating") Double securityRating,
                                                   @RequestParam("datingPlaceReviewDtos[0].parkingRating") Double parkingRating,
                                                   @RequestParam("datingPlaceReviewDtos[0].parkingAccessRating") Double parkingAccessRating) {

        JsonResponse response = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();
        List<PlacesAttachmentDto> placesAttachmentDtoList = getLocationPictureList(locationPictureId, "");
        datingPlaceDto.setPlacesAttachmentDtos(placesAttachmentDtoList);
        datingPlaceDto.setCoverPicture(getImage(coverPictureId));
        if (datingPlaceDto.getUserType() == null) {

            errorMessages.add(new ErrorMessage("userType", "Select whether you want to Review Place/Own the Place"));
        }
        if (datingPlaceDto.getPlaceName().isEmpty()) {
            errorMessages.add(new ErrorMessage("placeName", "Provide a Name for the Dating Place"));
        }
        if (datingPlaceDto.getCity().isEmpty()) {
            errorMessages.add(new ErrorMessage("city", "Provide a city"));
        }
        if (datingPlaceDto.getAddressLine().isEmpty()) {
            errorMessages.add(new ErrorMessage("addressLine", "Provide an address"));
        }
        if (datingPlaceDto.getState().isEmpty()) {
            errorMessages.add(new ErrorMessage("state", "Provide a state"));
        }
        if (datingPlaceDto.getCountry().isEmpty()) {
            errorMessages.add(new ErrorMessage("country", "Select a Country"));
        }
        if (datingPlaceDto.getZipCode().isEmpty()) {
            errorMessages.add(new ErrorMessage("zipCode", "Provide a Zipcode"));
        }
        if (locationPictureId == null) {
            errorMessages.add(new ErrorMessage("datingLocationPhoto", "Upload an image"));
        }
        if (datingPlaceDto.getAgeRange() == null) {
            errorMessages.add(new ErrorMessage("ageRange", "Select an Age Range"));
        }
        if (datingPlaceDto.getGender() == null) {
            errorMessages.add(new ErrorMessage("gender", "Select Gender"));
        }
        if (datingPlaceDto.getPriceRange() == null) {
            errorMessages.add(new ErrorMessage("priceRange", "Select suitable Price Range"));
        }

        if (datingPlaceDto.getLatitude() == null || datingPlaceDto.getLongitude() == null) {
            errorMessages.add(new ErrorMessage("verify", "Please Verify the location you've entered"));
        }

        if (errorMessages.size() > 0) {
            response.setErrorMessageList(errorMessages);
            response.setStatus(ResponseStatus.FAIL);
            throw new EntityValidationException(errorMessages);
            //return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        } else {
            if (datingPlaceDto.getUserType().equalsIgnoreCase("reviewer")) {

                if (reviewComment.isEmpty() || securityRating == null || parkingAccessRating == null || parkingRating == null) {
                    errorMessages.add(new ErrorMessage("reviewComment", "Please enter a review & mention ratings"));
                    throw new EntityValidationException(errorMessages);
                }
                List<DatingPlaceReviewDto> datingPlaceReviewDtoList = new ArrayList<>();
                DatingPlaceReviewDto datingPlaceReviewDto = new DatingPlaceReviewDto();
                datingPlaceReviewDto.setReviewerComment(reviewComment);
                datingPlaceReviewDto.setSecurityRating(securityRating);
                datingPlaceReviewDto.setParkingRating(parkingRating);
                datingPlaceReviewDto.setParkingAccessRating(parkingAccessRating);
                datingPlaceReviewDto.setReviewApprovalStatus(ApprovalStatus.PENDING);
                datingPlaceReviewDtoList.add(datingPlaceReviewDto);
                datingPlaceDto.setDatingPlaceReviewDtos(datingPlaceReviewDtoList);

            } else if (datingPlaceDto.getUserType().equalsIgnoreCase("manager")) {
                if (!datingPlaceDto.getIsMapFeatured() && !datingPlaceDto.getIsLovStampFeatured()) {

                    errorMessages.add(new ErrorMessage("manage", "Please select a Feature Type"));
                    throw new EntityValidationException(errorMessages);
                }
            }
            response.setStatus(ResponseStatus.SUCCESS);
            request.getSession(true).setAttribute("datingPlace", datingPlaceDto);
            return ResponseEntity.ok().body(response);
        }

    }

    @PostMapping("/featured/social")
    @ResponseBody
    public ResponseEntity addFeaturedDatingPlaceSocial(HttpServletRequest request,
                                                       @ModelAttribute("placeCreate") DatingPlaceDto datingPlaceDto,
                                                       @RequestParam("placesAttachmentDtos[0].picture.id") Long locationPictureId,
                                                       @RequestParam("coverPicture.id") Long coverPictureId) {
        JsonResponse response = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();
        List<PlacesAttachmentDto> placesAttachmentDtoList = getLocationPictureList(locationPictureId, "");
        datingPlaceDto.setPlacesAttachmentDtos(placesAttachmentDtoList);
        datingPlaceDto.setCoverPicture(getImage(coverPictureId));

        if (datingPlaceDto.getPlaceName().isEmpty()) {
            errorMessages.add(new ErrorMessage("placeName", "Provide a Name for the Dating Place"));
        }
        if (datingPlaceDto.getCity().isEmpty()) {
            errorMessages.add(new ErrorMessage("city", "Provide a city"));
        }
        if (datingPlaceDto.getAddressLine().isEmpty()) {
            errorMessages.add(new ErrorMessage("addressLine", "Provide an address"));
        }
        if (datingPlaceDto.getState().isEmpty()) {
            errorMessages.add(new ErrorMessage("state", "Provide a state"));
        }
        if (datingPlaceDto.getCountry().isEmpty()) {
            errorMessages.add(new ErrorMessage("country", "Select a Country"));
        }
        if (datingPlaceDto.getZipCode().isEmpty()) {
            errorMessages.add(new ErrorMessage("zipCode", "Provide a Zipcode"));
        }
        if (locationPictureId == null) {
            errorMessages.add(new ErrorMessage("datingLocationPhoto", "Upload an image"));
        }
        if (datingPlaceDto.getAgeRange() == null) {
            errorMessages.add(new ErrorMessage("ageRange", "Select an Age Range"));
        }
        if (datingPlaceDto.getGender() == null) {
            errorMessages.add(new ErrorMessage("gender", "Select Gender"));
        }
        if (datingPlaceDto.getPriceRange() == null) {
            errorMessages.add(new ErrorMessage("priceRange", "Select suitable Price Range"));
        }

        if (datingPlaceDto.getLatitude() == null || datingPlaceDto.getLongitude() == null) {
            errorMessages.add(new ErrorMessage("verify", "Please Verify the location you've entered"));
        }

        if (BooleanUtils.isFalse(datingPlaceDto.getIsMapFeatured())
                && BooleanUtils.isFalse(datingPlaceDto.getIsLovStampFeatured())) {

            errorMessages.add(new ErrorMessage("feature", "Please select atleast one type of Featuring"));
        }

        if (errorMessages.size() > 0) {
            response.setErrorMessageList(errorMessages);
            response.setStatus(ResponseStatus.FAIL);
            throw new EntityValidationException(errorMessages);
            //return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        } else {

            request.getSession(true).setAttribute("datingPlace", datingPlaceDto);
            return ResponseEntity.ok().body(response);
        }

    }

    @PostMapping("/social/place-review/{id}")
    public ResponseEntity addDatingPlaceReviewSocial(HttpServletRequest
                                                             request, @ModelAttribute DatingPlaceReviewDto
                                                             datingPlaceReviewDto, @PathVariable("id") String placeId) {
        JsonResponse response = new JsonResponse();
        DatingPlaceDto datingPlaceDto;

        if (DatingPlaceUtil.isInteger(placeId)) {
            Integer id = Integer.parseInt(placeId);
            datingPlaceDto = datingPlaceService.findDatingPlaceById(id);
            datingPlaceReviewDto.setPlaceId(datingPlaceDto.getDatingPlace());
        } else {
            datingPlaceReviewDto.setGooglePlaceId(placeId);
        }
        response.setStatus(ResponseStatus.SUCCESS);
        request.getSession(true).setAttribute("datingPlaceReviewDto", datingPlaceReviewDto);
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/claim/{id}")
    public ModelAndView claim(ModelAndView model, @PathVariable("id") String id) throws IOException {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        if (DatingPlaceUtil.isInteger(id)) {
            DatingPlaceDto datingPlaceDto = datingPlaceService.findDatingPlaceById(Integer.parseInt(id));
            model.addObject("datingPlace", datingPlaceDto);
        } else {
            DatingPlaceDto datingPlaceDto = datingPlaceService.findGoogleDatingPlaceById(id);
            model.addObject("datingPlace", datingPlaceDto);
        }
        model.setViewName("dating_places/dating_place_claim");
        model.addObject("userId", userId);
        if (userId != null) {
            UserDto user = userService.getUser(userId);
            model.addObject("userEmail", user.getEmail());
        }
        return model;
    }

    @GetMapping("/deals/{id}/submit")
    public ModelAndView dealsSubmit(ModelAndView model, @PathVariable("id") String id) {
        model.addObject("datingPlace", datingPlaceService.findDatingPlaceById(Integer.parseInt(id)));
        model.addObject("datingPlaceDeal", new DatingPlaceDealsDto());
        model.setViewName("dating_places/dating-place-deal-submit");
        return model;
    }

    @GetMapping("/{id}/edit")
    public ModelAndView editDatingPlace(ModelAndView model, @PathVariable("id") String id) {
        model.addObject("datingPlace", datingPlaceService.findDatingPlaceById(Integer.parseInt(id)));
        model.addObject(GOOGLE_KEY, googleKeyId);
        model.addObject("atmospheres", datingPlaceSupportService.getAllAtmospheresEnabled());
        model.addObject("foodTypes", datingPlaceSupportService.getAllFoodTypesEnabled());
        model.setViewName("dating_places/edit_dating_place");
        return model;
    }

    @PostMapping("/deals/save")
    public ResponseEntity savePlaceDeal(@ModelAttribute("datingPlaceDeal") DatingPlaceDealsDto placeDealsDto) {
        JsonResponse response = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (placeDealsDto.getDealDescription() == null || StringUtils.isEmpty(placeDealsDto.getDealDescription())) {

            errorMessages.add(new ErrorMessage("dealDescription", "Please add a deal type"));
        }
        if (placeDealsDto.getFullAmount() == null) {

            errorMessages.add(new ErrorMessage("fullAmount", "Please add the full amount"));
        }
        if (placeDealsDto.getDiscountValue() == null) {

            errorMessages.add(new ErrorMessage("discount", "Please add a discount"));
        }
        if (placeDealsDto.getStartDate() == null) {

            errorMessages.add(new ErrorMessage("startDate", "Please add a start date"));
        }
        if (placeDealsDto.getExpiryDate() == null) {

            errorMessages.add(new ErrorMessage("expiryDate", "Please add an Expiry date"));
        }
        if (placeDealsDto.getDiscountValue() != null && placeDealsDto.getFullAmount() != null && (placeDealsDto.getDiscountValue() > placeDealsDto.getFullAmount())) {

            errorMessages.add(new ErrorMessage("discount", "Discount should be less than full value"));
        }
        if (placeDealsDto.getStartDate() != null && placeDealsDto.getExpiryDate() != null && placeDealsDto.getStartDate().after(placeDealsDto.getExpiryDate())) {

            errorMessages.add(new ErrorMessage("expiryDate", "Expiry date should be after the Start Date"));
        }
        //check if existing Deal
        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        DatingPlaceDealsDto placeDealsDto1 = datingPlaceService.addDatingDeal(placeDealsDto);
        response.setStatus(ResponseStatus.SUCCESS);
        return ResponseEntity.status(HttpStatus.OK).body(response);

    }

    private List<PlacesAttachmentDto> getLocationPictureList(Long pictureId, String description) {

        CloudStorageFileDto pic = new CloudStorageFileDto();
        pic.setId(pictureId);
        PlacesAttachmentDto picDto = new PlacesAttachmentDto();
        picDto.setPicture(pic);
        picDto.setImageDescription(description);
        List<PlacesAttachmentDto> list = new ArrayList<>();
        list.add(picDto);
        return list;
    }

    private CloudStorageFileDto getImage(Long coverPictureId) {
        CloudStorageFileDto pic = new CloudStorageFileDto();
        pic.setId(coverPictureId);
        return pic;
    }

}
