package com.realroofers.lovappy.web.rest.v1;

import com.google.common.base.Strings;
import com.realroofers.lovappy.service.ads.AdsService;
import com.realroofers.lovappy.service.ads.dto.AdDto;
import com.realroofers.lovappy.service.ads.dto.AdTargetAreaDto;
import com.realroofers.lovappy.service.ads.support.*;
import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.core.TextAlign;
import com.realroofers.lovappy.service.exception.EntityValidationException;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.AddressDto;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.FormValidator;
import com.realroofers.lovappy.service.validator.SubmitAdvertisement;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.FileUtil;
import com.realroofers.lovappy.web.util.PaymentUtil;
import com.squareup.connect.ApiException;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * @author Eias Altawil
 */

@RestController
@RequestMapping({"/api/v1/ads", "/api/v2/ads"})
public class RestAdsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestAdsController.class);

    private final FormValidator formValidator;
    private final AdsService adsService;
    private final CloudStorage cloudStorage;
    private final CloudStorageService cloudStorageService;
    private final NotificationService notificationService;
    private final PaymentUtil paymentUtil;
    private final OrderService orderService;
    private final FileUtil fileUtil;
    @Value("${lovappy.cloud.bucket.ads.audio}")
    private String audioAdsBucket;

    @Value("${lovappy.cloud.bucket.ads.video}")
    private String videoAdsBucket;

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 2;

    @Autowired
    public RestAdsController(AdsService adsService, FormValidator formValidator, CloudStorage cloudStorage,
                             CloudStorageService cloudStorageService, NotificationService notificationService,
                             PaymentUtil paymentUtil, OrderService orderService, FileUtil fileUtil) {
        this.formValidator = formValidator;
        this.adsService = adsService;
        this.cloudStorage = cloudStorage;
        this.cloudStorageService = cloudStorageService;
        this.notificationService = notificationService;
        this.paymentUtil = paymentUtil;
        this.orderService = orderService;
        this.fileUtil = fileUtil;
    }


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Page<AdDto>> loadAds(
            @RequestParam(value = "page_size", required = false) Optional<Integer> pageSize,
            @RequestParam(value = "page_number", required = false) Optional<Integer> pageNumber,
            @RequestParam(value = "excluded_ads", required = false) Integer[] excludedAds) {

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (pageNumber.orElse(0) < 1) ? INITIAL_PAGE : pageNumber.get() - 1;

        if (excludedAds == null)
            excludedAds = new Integer[0];

        Page<AdDto> ads;

        Integer userID = UserUtil.INSTANCE.getCurrentUserId();
        if (userID == null){
            ads = adsService.findAll(new PageRequest(evalPage, evalPageSize));
        }else {
            ads = adsService.getAds(userID, new ArrayList<>(Arrays.asList(excludedAds)),
                    new PageRequest(evalPage, evalPageSize));
        }

        return ResponseEntity.ok(ads);
    }

    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<?> addAdvertisement(
            @RequestParam("mediaType") AdMediaType mediaType,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "length", required = false) Integer length,
            @RequestParam(value ="coupon", required = false) String coupon,
            @RequestParam(value = "mediaFileCloud.id" , required = false) Long mediaFileId,
            @RequestParam(value = "audioType", required = false) AudioType audioType,
            @RequestParam("adType") AdType adType,
            @RequestParam(value = "gender", required = false) Gender gender,
            @RequestParam(value = "ageRange", required = false) AgeRange ageRange,
            @RequestParam("language.id") Integer language,
            @RequestParam("url") String url,
            @RequestParam(value = "featured", required = false) Boolean featured,

            @RequestParam(value = "text", required = false) String text,
            @RequestParam(value = "textAlign", required = false) TextAlign textAlign,
            @RequestParam(value = "mediaFileUrl", required = false) String mediaFileUrl,
            @RequestParam(value = "textColor", required = false) String textColor,
            @RequestParam(value = "backgroundColorID", required = false) Integer backgroundColorID,
            @RequestParam(value = "backgroundImageID", required = false) Long backgroundImageID,
            @RequestParam(value = "featuredAdBackgroundID", required = false) Long featuredAdBackgroundID,

            @RequestParam(value = "targetAreas", required = false) AddressDto[] targetAreas,
            @RequestParam(value = "preferences.catORdog", required = false) String catORdog,
            @RequestParam(value = "preferences.drinker", required = false) String drinker,
            @RequestParam(value = "preferences.smoker", required = false) String smoker,
            @RequestParam(value = "preferences.user", required = false) String user,
            @RequestParam(value = "preferences.no-habit", required = false) String nohabit,
            @RequestParam(value = "preferences.ruralORurbanORsuburban", required = false) String ruralORurbanORsuburban,
            @RequestParam(value = "preferences.politicalopinion", required = false) String politicalopinion,
            @RequestParam(value = "preferences.outdoorsyORindoorsy", required = false) String outdoorsyORindoorsy,
            @RequestParam(value = "preferences.frugalORbigspender", required = false) String frugalORbigspender,
            @RequestParam(value = "preferences.morningpersonORnightowl", required = false) String morningpersonORnightowl,
            @RequestParam(value = "preferences.extrovertORintrovert", required = false) String extrovertORintrovert,

            @RequestParam("nonce") String nonce) {

        Map<String, String> preferences = new HashMap<>();
        preferences.put("catORdog", catORdog);
        if (drinker != null) preferences.put("drinker", drinker);
        if (smoker != null) preferences.put("smoker", smoker);
        if (user != null) preferences.put("user", user);
        if (nohabit != null) preferences.put("no-habit", nohabit);
        preferences.put("ruralORurbanORsuburban", ruralORurbanORsuburban);
        preferences.put("politicalopinion", politicalopinion);
        preferences.put("outdoorsyORindoorsy", outdoorsyORindoorsy);
        preferences.put("frugalORbigspender", frugalORbigspender);
        preferences.put("morningpersonORnightowl", morningpersonORnightowl);
        preferences.put("extrovertORintrovert", extrovertORintrovert);

        List<AdTargetAreaDto> adTargetAreas = new ArrayList<>();

        if (targetAreas != null) {
            for (AddressDto addressDto : targetAreas) {
                adTargetAreas.add(new AdTargetAreaDto(null, addressDto));
            }
        }

        CloudStorageFileDto mediaFile = null;
        if(mediaFileId != null) {
            mediaFile = new CloudStorageFileDto();
            mediaFile.setId(mediaFileId);
        }

        CalculatePriceDto calculatePrice = adsService.calculatePrice(adType, mediaType, length, coupon);
        AdDto adDto = new AdDto(email, length, null, coupon, mediaFile, audioType, adType, adTargetAreas,
                gender, ageRange, new LanguageDto(language), preferences, url, mediaType, text, textColor,
                backgroundColorID, backgroundImageID, featured, calculatePrice.getPrice(), mediaFileUrl,featuredAdBackgroundID, textAlign);

        List<ErrorMessage> errorMessages = formValidator.validate(adDto, new Class[]{SubmitAdvertisement.class});

        if (preferences.get("catORdog") == null)
            errorMessages.add(new ErrorMessage("catORdog", "What do you prefer?"));

        if (preferences.get("drinker") == null && preferences.get("smoker") == null && preferences.get("user") == null && preferences.get("no-habit") == null)
            errorMessages.add(new ErrorMessage("habit", "What is your habit?"));

        if (preferences.get("ruralORurbanORsuburban") == null)
            errorMessages.add(new ErrorMessage("ruralORurbanORsuburban", "What is your ideal neighborhood"));

        if (preferences.get("outdoorsyORindoorsy") == null)
            errorMessages.add(new ErrorMessage("outdoorsyORindoorsy", "Are you outdoorsy or indoorsy?"));

        if (preferences.get("frugalORbigspender") == null)
            errorMessages.add(new ErrorMessage("frugalORbigspender", "Are you frugal or big spender?"));

        if (preferences.get("morningpersonORnightowl") == null)
            errorMessages.add(new ErrorMessage("morningpersonORnightowl", "Are you morning person or night owl?"));

        if (preferences.get("extrovertORintrovert") == null)
            errorMessages.add(new ErrorMessage("extrovertORintrovert", "Are you extrovert or introvert?"));


        if (mediaType == AdMediaType.TEXT || mediaType == AdMediaType.AUDIO){
            if (Strings.isNullOrEmpty(textColor))
                errorMessages.add(new ErrorMessage("textColor", "Select text color"));

            if (backgroundColorID == null && backgroundImageID == null)
                errorMessages.add(new ErrorMessage("backgroundColorID", "Select background color"));
        }

        if (mediaType.equals(AdMediaType.TEXT)) {
            if (Strings.isNullOrEmpty(text))
                errorMessages.add(new ErrorMessage("text", "You should add some text"));

        } else if (mediaType.equals(AdMediaType.AUDIO)) {
            if (StringUtils.isEmpty(mediaFileId))
                errorMessages.add(new ErrorMessage("audioFileId", "Upload or record audio"));
        } else if (mediaType.equals(AdMediaType.VIDEO)) {
            if (StringUtils.isEmpty(mediaFileId) && StringUtils.isEmpty(mediaFileUrl))
                errorMessages.add(new ErrorMessage("audioFileId", "Upload a video"));
        }

        //Validate url
        try {
            if(!url.startsWith("http://") || !url.startsWith("https://"))
                url = "http://" + url;

            new URL(url);
        }
        catch (MalformedURLException e) {
            errorMessages.add(new ErrorMessage("url", "Invalid url"));
        }

        if (errorMessages.size() > 0)
            throw new EntityValidationException(errorMessages);

        OrderDto order = adsService.addAd(adDto);
        if(order != null) {

            Boolean executed = null;
            try {
                executed = paymentUtil.executeTransaction(nonce, order, null);
            } catch (ApiException e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
            if (executed) {

                //send notification to admin
                NotificationDto notificationDto = new NotificationDto();
                notificationDto.setContent("New ad submitted");
                notificationDto.setType(NotificationTypes.USER_ADS);
                notificationDto.setSourceId(order.getId());//check it later
                notificationService.sendNotificationToAdmins(notificationDto);

                return ResponseEntity.ok().build();
            } else {
                orderService.revertOrder(order.getId());
            }
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }


    @RequestMapping(value = "/cover_image", method = RequestMethod.POST)
    public ResponseEntity<CloudStorageFileDto> uploadCoverImage(@RequestParam("cover_image") MultipartFile coverImage){
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (coverImage.isEmpty())
            errorMessages.add(new ErrorMessage("cover", "You should add cover image."));

        if (errorMessages.size() > 0)
            throw new EntityValidationException(errorMessages);

        try {
            CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadAndPersistFile(
                    coverImage, imagesBucket, FilenameUtils.getExtension(coverImage.getOriginalFilename()), coverImage.getContentType());

            return ResponseEntity.ok(cloudStorageFileDto);

        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @RequestMapping(value = "/audio", method = RequestMethod.POST)
    public ResponseEntity<CloudStorageFileDto> uploadAudio(@RequestParam("audio") MultipartFile audioFile){
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (audioFile.isEmpty())
            errorMessages.add(new ErrorMessage("audio", "You should upload a file."));

        if (errorMessages.size() > 0)
            throw new EntityValidationException(errorMessages);



        try {
            CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadAndPersistFile(
                    audioFile, audioAdsBucket, FilenameUtils.getExtension(audioFile.getOriginalFilename()), audioFile.getContentType());

            cloudStorageFileDto.setDuration(fileUtil.getAudioDuration(audioFile));

            return ResponseEntity.ok(cloudStorageFileDto);

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @RequestMapping(value = "/video", method = RequestMethod.POST)
    public ResponseEntity<CloudStorageFileDto> uploadVideo(@RequestParam("video") MultipartFile file){
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (file.isEmpty())
            errorMessages.add(new ErrorMessage("video", "You should upload a file."));

        if (errorMessages.size() > 0)
            throw new EntityValidationException(errorMessages);



        try {
            CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadAndPersistFile(
                    file, videoAdsBucket, FilenameUtils.getExtension(file.getOriginalFilename()), file.getContentType());

            return ResponseEntity.ok(cloudStorageFileDto);

        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @RequestMapping(value = "/calculate_price", method = RequestMethod.GET)
    public ResponseEntity<CalculatePriceDto> calculatePrice(@RequestParam("ad_type") AdType adType,
                                                            @RequestParam("media_type") AdMediaType mediaType,
                                                            @RequestParam("duration") Integer duration,
                                                            @RequestParam("coupon") String coupon) {

        LOGGER.info("Calculate Ad Proce adType = {} media_type = {} duration = {} coupon = {} ", adType, mediaType, duration, coupon);
        return ResponseEntity.ok(adsService.calculatePrice(adType, mediaType, duration, coupon));
    }


}