package com.realroofers.lovappy.web.dto;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public class IdDto {
    private String id;

    public IdDto(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
