package com.realroofers.lovappy.web.controller.admin.blogs;

import com.realroofers.lovappy.service.blog.dto.InvitationDto;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.dto.AuthorDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.util.PasswordUtils;
import com.realroofers.lovappy.service.validator.Invitation;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.util.CommonUtils;
import com.realroofers.lovappy.web.util.Pager;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.FormValidator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by Daoud Shaheen on 7/7/2017.
 */
@Controller
@RequestMapping("/lox/blog")
public class BlogUsersController  {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlogUsersController.class);

    @Autowired
    private FormValidator formValidator;

    @Autowired
    private MailService mailService;

    @Autowired
    private UserService userService;

    @Autowired
    protected ITemplateEngine templateEngine;

    @Autowired
    private EmailTemplateService emailTemplateService;

    //Users Managment
    @GetMapping("/users")
    public ModelAndView list(ModelAndView model, @RequestParam(value = "pageOne", defaultValue = "1") Integer page1,
                             @RequestParam(value = "pageTwo", defaultValue = "1") Integer page2,
                             @RequestParam(value = "sort",required = false) Sort.Direction sorting){

        int evalPage1 = (page1 < 1) ? Pager.INITIAL_PAGE + 1 : page1;
        int evalPage2 = (page2 < 1) ? Pager.INITIAL_PAGE + 1 : page2;
        Page<AuthorDto> activateUsers = userService.getAuthorsByActivated(true, evalPage1, Pager.INITIAL_PAGE_SIZE, sorting);
        Page<AuthorDto> deactivateUsers = userService.getAuthorsByActivated(false, evalPage2, Pager.INITIAL_PAGE_SIZE, sorting);
        Pager activatePager = new Pager(activateUsers.getTotalPages(), activateUsers.getNumber(), Pager.BUTTONS_TO_SHOW);
        Pager deactivatePager = new Pager(deactivateUsers.getTotalPages(), deactivateUsers.getNumber(), Pager.BUTTONS_TO_SHOW);

        model.addObject("activeUsers", activateUsers);
        model.addObject("deactiveUsers", deactivateUsers);
        model.addObject("sort", sorting == null? Sort.Direction.ASC : sorting.equals(Sort.Direction.ASC)?Sort.Direction.DESC:Sort.Direction.ASC);


        model.addObject("selectedPageSize",  Pager.INITIAL_PAGE_SIZE);
        model.addObject("pageSizes", Pager.PAGE_SIZES);
        model.addObject("activatePager", activatePager);
        model.addObject("deactivatePager", deactivatePager);
        model.setViewName("admin/blog/manage_blog_users");
        return model;
    }

    @GetMapping("/users/{user-id}/{activate}")
    public ModelAndView activate(@PathVariable("user-id") Integer userId,
                                 @PathVariable("activate") String activate,
                                 ModelAndView model) {

        userService.activateAuthor(userId, activate.equals("activate"));
        model.setViewName("redirect:/lox/blog/users");
        return model;
    }


    //Invitation CATEGORY
    @GetMapping("/invitation")
    public ModelAndView getInvitationForm(ModelAndView model, Pageable pageable) {
        if (!model.getModelMap().containsAttribute("invitation")) {
            //TODO @Mujoko, review this please
            model.addObject("invitation", new InvitationDto());
        }
        model.setViewName("admin/blog/manage_invitation");
        return model;
    }


    @PostMapping("/invitation/send")
    public ModelAndView sendInvitation(HttpServletRequest request,@Valid InvitationDto invitation, BindingResult err, ModelAndView model, RedirectAttributes redirectAttributes, Pageable pageable) {
        List<ErrorMessage> errorMessages = formValidator.validate(invitation, new Class[]{Invitation.class});
        User user = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
            user = (User) authentication.getDetails();
        }

        if (user.getEmail() == null) {
            redirectAttributes.addFlashAttribute("invitation", invitation);
            redirectAttributes.addFlashAttribute("errorMessage", "Please insert email addresses");
            model.setViewName("redirect:/lox/blog/invitation");
            return model;
        }
        if(!user.getEmail().contains(",")) {

            if(!CommonUtils.isValidEmail(user.getEmail())) {
                redirectAttributes.addFlashAttribute("invitation", invitation);
                redirectAttributes.addFlashAttribute("errorMessage", "Please insert email addresses");
                model.setViewName("redirect:/lox/blog/invitation");
                return model;
            }
        }
        String[] emails = org.springframework.util.StringUtils.delimitedListToStringArray(invitation.getEmail(), ",");

        StringBuilder errorEmails = new StringBuilder("");
        for (String email : emails) {
            if(!CommonUtils.isValidEmail(email)){
                errorEmails.append("\n" + email);
            }
        }
        if(!StringUtils.isEmpty(errorEmails)) {
            redirectAttributes.addFlashAttribute("invitation", invitation);
            redirectAttributes.addFlashAttribute("errorMessage", "invalid emails : " + errorEmails);
            model.setViewName("redirect:/lox/blog/invitation");
            return model;
        }
        for (String email : emails) {
            LOGGER.info("send invitations to email " + email);
            if(userService.inviteAuthorByEmail(email, user.getEmail())) {

                //send approval email
                EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.BLOG_INVITATION, "EN");
                emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/blog");

                if (!StringUtils.isEmpty(invitation.getContent())) {
                    emailTemplate.setBody(invitation.getContent());
                }

                new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), email, templateEngine,
                        mailService, emailTemplate).send();

            } else {

                String password = PasswordUtils.generatePassword();
                UserDto authorDto = new UserDto();
                authorDto.setEmail(email);
                authorDto.setFirstName("");
                authorDto.setLastName("");
                authorDto.setPassword(password);
                userService.createNewAuthor(authorDto);

                Context context = new Context();
                context.setVariable("email", authorDto.getEmail());
                context.setVariable("password", authorDto.getPassword());
                context.setVariable("emailBody", invitation.getContent());
                context.setVariable("url", CommonUtils.getBaseURL(request) + "/blog");
                String content = templateEngine.process("email/blog_invitation_anonymous", context);
                mailService.sendMail(email, content, "Blog Invitation");
            }
        }
        redirectAttributes.addFlashAttribute("successMessage", "Invitation is successfully sent");
        model.setViewName("redirect:/lox/blog/invitation");
        return model;
    }

}
