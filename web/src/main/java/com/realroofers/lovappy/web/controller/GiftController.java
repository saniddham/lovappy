package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.gift.GiftExchangeService;
import com.realroofers.lovappy.service.gift.GiftService;
import com.realroofers.lovappy.service.gift.PurchasedGiftService;
import com.realroofers.lovappy.service.gift.ViewedGiftService;
import com.realroofers.lovappy.service.gift.dto.GiftExchangeDto;
import com.realroofers.lovappy.service.gift.dto.PurchasedGiftDto;
import com.realroofers.lovappy.service.gift.dto.ViewedGiftDto;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.product.ProductService;
import com.realroofers.lovappy.service.product.dto.ProductDto;
import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.util.CommonUtils;
import com.realroofers.lovappy.web.util.Pager;
import com.realroofers.lovappy.web.util.PaymentUtil;
import com.squareup.connect.ApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.realroofers.lovappy.web.support.Util.distinctByKey;

/**
 * Created by Eias Altawil on 5/7/17
 */

@Controller
@RequestMapping("/gifts")
public class GiftController {

    private final GiftService giftService;
    private final UserService userService;
    private final UserProfileService userProfileService;
    private final GiftExchangeService giftExchangeService;
    private final OrderService orderService;
    private final PaymentUtil paymentUtil;
    private final ViewedGiftService viewedGiftService;
    private final PurchasedGiftService purchasedGiftService;
    private String squareAppID;

    private ITemplateEngine templateEngine;
    private MailService mailService;
    private final EmailTemplateService emailTemplateService;

    private final ProductService productService;

    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 20;
    private static final int[] PAGE_SIZES = {5, 10, 20};

    @Value("${email.basePath}")
    private String baseUrl;

    private static Logger LOG = LoggerFactory.getLogger(GiftController.class);

    @Autowired
    public GiftController(GiftService giftService, UserService userService, UserProfileService userProfileService,
                          GiftExchangeService giftExchangeService, OrderService orderService,
                          PaymentUtil paymentUtil, PurchasedGiftService purchasedGiftService, @Value("${square.applicationId}") String squareAppID,
                          MailService mailService, List<ViewResolver> viewResolvers, ViewedGiftService viewedGiftService, EmailTemplateService emailTemplateService,
                          ProductService productService) {
        this.giftService = giftService;
        this.userService = userService;
        this.userProfileService = userProfileService;
        this.giftExchangeService = giftExchangeService;
        this.orderService = orderService;
        this.paymentUtil = paymentUtil;
        this.purchasedGiftService = purchasedGiftService;
        this.squareAppID = squareAppID;
        this.mailService = mailService;
        this.viewedGiftService = viewedGiftService;
        this.emailTemplateService = emailTemplateService;
        this.productService = productService;
        for (ViewResolver viewResolver : viewResolvers) {
            if (viewResolver instanceof ThymeleafViewResolver) {
                ThymeleafViewResolver thymeleafViewResolver = (ThymeleafViewResolver) viewResolver;
                this.templateEngine = thymeleafViewResolver.getTemplateEngine();
            }
        }
        if (this.templateEngine == null) {
            throw new BeanInitializationException("No view resolver of type ThymeleafViewResolver found.");
        }
    }

    @GetMapping("/catalog/{userID}")
    public ModelAndView giftCatalog(@PathVariable Integer userID,
                                    @RequestParam("pageSize") Optional<Integer> pageSize,
                                    @RequestParam("page") Optional<Integer> page,
                                    @RequestParam(value = "search", defaultValue = "", required = false) String searchProductText) {
        ModelAndView mav = new ModelAndView("gift/gift_catalog");

        mav.addObject("toUser", userService.getUser(userID));
        mav.addObject("toUserProfile", userProfileService.findOne(userID));

        mav.addObject("searchProductText", searchProductText);
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        PageRequest pageable = new PageRequest(evalPage, evalPageSize);

        Page<ProductDto> pages = productService.getAllProductsByName(searchProductText, pageable);
        mav.addObject("HotGifts", pages);
        Pager pager = new Pager(pages.getTotalPages(), pages.getNumber(), BUTTONS_TO_SHOW);
        mav.addObject("selectedPageSize", evalPageSize);
        mav.addObject("pageSizes", PAGE_SIZES);
        mav.addObject("pager", pager);


        mav.addObject("SuggestedGifts", productService.getAllProductsByRecentlyViwed(UserUtil.INSTANCE.getCurrentUserId()));
        return mav;
    }

    @GetMapping("/viewed/{userID}")
    public ModelAndView giftViewed(@PathVariable Integer userID) {
        ModelAndView mav = new ModelAndView("gift/gift_recently_viewed");

        mav.addObject("toUser", userService.getUser(userID));
        mav.addObject("toUserProfile", userProfileService.findOne(userID));

        mav.addObject("HotGifts", productService.getAllProductsByRecentlyViwed(UserUtil.INSTANCE.getCurrentUserId()));
        return mav;
    }

    @GetMapping("/purchased/{userID}")
    public ModelAndView giftPurchased(@PathVariable Integer userID) {
        ModelAndView mav = new ModelAndView("gift/gift_recently_purchased");

        mav.addObject("toUser", userService.getUser(userID));
        mav.addObject("toUserProfile", userProfileService.findOne(userID));

        mav.addObject("HotGifts", productService.getAllProductsRecentlyPurchased(UserUtil.INSTANCE.getCurrentUserId()));
        return mav;
    }

    @GetMapping("/purchase/{userID}/{giftID}")
    public ModelAndView giftPurchase(@PathVariable Integer userID, @PathVariable Integer giftID) throws Exception {
        ModelAndView mav = new ModelAndView("gift/gift_purchase");
        mav.addObject("toUser", userService.getUser(userID));
        mav.addObject("toUserProfile", userProfileService.findOne(userID));


        mav.addObject("HotGifts", productService.getAllProductsByRecentlyViwed(UserUtil.INSTANCE.getCurrentUserId()));
        mav.addObject("forUser", userService.getUser(userID));

        ProductDto product = productService.findById(giftID);
        if(product == null) {
            mav.setViewName("error/404");
            return mav;
        }

        mav.addObject("gift", product);
        mav.addObject("squareAppID", squareAppID);

        ViewedGiftDto viewedGift = new ViewedGiftDto();
        viewedGift.setProduct(new Product(product));
        viewedGift.setUser(userService.getUser(UserUtil.INSTANCE.getCurrentUserId()));
        viewedGift.setViewedOn(new Date());

        viewedGiftService.create(viewedGift);
        return mav;
    }

    @PostMapping("/send")
    public ResponseEntity<?> purchaseGift(@RequestHeader String host, HttpServletRequest request,
                                          @RequestParam("to_user_id") Integer toUserID, @RequestParam("gift_id") Integer giftID,
                                          @RequestParam("quantity") Integer quantity, @RequestParam("nonce") String nonce,
                                          @RequestParam(value = "language", defaultValue = "EN") String language,
                                          @RequestParam("coupon") String coupon) {

        OrderDto order = giftService.purchaseGift(UserUtil.INSTANCE.getCurrentUserId(), toUserID, giftID, quantity, coupon, PaymentMethodType.SQUARE);
        if (order != null) {

            Boolean executed = null;
            try {
                executed = paymentUtil.executeTransaction(nonce, order, null);
            } catch (ApiException e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
            if (executed) {
                GiftExchangeDto giftExchange = giftExchangeService.sendGift(giftID, UserUtil.INSTANCE.getCurrentUserId(), toUserID, order);

                DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
                SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");

                EmailTemplateDto emailTemplate = emailTemplateService
                        .getByNameAndLanguage(EmailTemplateConstants.GIFT_SENT, language);
                emailTemplate.getAdditionalInformation().put("toUser", giftExchange.getToUser().getID()+"");
                emailTemplate.getAdditionalInformation().put("refNumber", giftExchange.getRefNumber());
                emailTemplate.getAdditionalInformation().put("productName", giftExchange.getGift().getProductName());
                emailTemplate.getAdditionalInformation().put("price", decimalFormat.format(giftExchange.getSoldPrice()));
                emailTemplate.getAdditionalInformation().put("date", dateFormatter.format(giftExchange.getSentAt()));
                emailTemplate.getAdditionalInformation().put("baseUrl", CommonUtils.getBaseURL(request));
                emailTemplate.getAdditionalInformation().put("url", baseUrl + "/my_account/gift/outbox");
                new EmailTemplateFactory().build(baseUrl, giftExchange.getFromUser().getEmail(), templateEngine,
                        mailService, emailTemplate).send();

                emailTemplate = emailTemplateService
                        .getByNameAndLanguage(EmailTemplateConstants.GIFT_RECEIVED, language);
                emailTemplate.getAdditionalInformation().put("fromUser", giftExchange.getToUser().getID()+"");
                emailTemplate.getAdditionalInformation().put("baseUrl", CommonUtils.getBaseURL(request));
                emailTemplate.getAdditionalInformation().put("url", baseUrl + "/my_account/gift/inbox");
                new EmailTemplateFactory().build(baseUrl, giftExchange.getToUser().getEmail(), templateEngine,
                        mailService, emailTemplate).send();

                emailTemplate = emailTemplateService
                        .getByNameAndLanguage(EmailTemplateConstants.GIFT_SOLD, language);
                emailTemplate.getAdditionalInformation().put("toUser", giftExchange.getToUser().getID()+"");
                emailTemplate.getAdditionalInformation().put("refNumber", giftExchange.getRefNumber());
                emailTemplate.getAdditionalInformation().put("productName", giftExchange.getGift().getProductName());
                emailTemplate.getAdditionalInformation().put("price", decimalFormat.format(giftExchange.getSoldPrice()));
                emailTemplate.getAdditionalInformation().put("date", dateFormatter.format(giftExchange.getSentAt()));
                emailTemplate.getAdditionalInformation().put("baseUrl", CommonUtils.getBaseURL(request));
                emailTemplate.getAdditionalInformation().put("url", baseUrl + "/vendor");
                new EmailTemplateFactory().build(baseUrl, giftExchange.getGift().getCreatedBy().getEmail(), templateEngine,
                        mailService, emailTemplate).send();


                PurchasedGiftDto purchasedGiftDto = new PurchasedGiftDto();
                purchasedGiftDto.setUser(userService.getUser(UserUtil.INSTANCE.getCurrentUserId()));
                purchasedGiftDto.setProduct(new Product(productService.findById(giftID)));
                purchasedGiftDto.setPurchasedOn(new Date());

                try {
                    purchasedGiftService.create(purchasedGiftDto);
                } catch (Exception e) {
                    e.printStackTrace();
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }

                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                orderService.revertOrder(order.getId());
            }
        }

        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping("/received")
    public ModelAndView giftsReceived() {
        ModelAndView mav = new ModelAndView("gift/gifts_received");
        mav.addObject("gifts",
                giftExchangeService.getAllReceivedGifts(UserUtil.INSTANCE.getCurrentUserId()));

        return mav;
    }

    @GetMapping("/sent")
    public ModelAndView giftsSent() {
        ModelAndView mav = new ModelAndView("gift/gifts_sent");
        Collection<GiftExchangeDto> allSentGifts = giftExchangeService.getAllSentGifts(UserUtil.INSTANCE.getCurrentUserId());
        mav.addObject("gifts", allSentGifts);
        mav.addObject("totalSpent",
                orderService.getTotalSpentByUserID(UserUtil.INSTANCE.getCurrentUserId()));

        Long totalUsers = allSentGifts.stream()
                .filter(distinctByKey(o -> o.getToUser().getID()))
                .count();
        mav.addObject("totalUsers", totalUsers);
        return mav;
    }

    @GetMapping
    public ModelAndView landingPage(@RequestParam("pageSize") Optional<Integer> pageSize, @RequestParam("page") Optional<Integer> page) {
        ModelAndView model = new ModelAndView();
        model.addObject("user", new UserDto());

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        PageRequest pageable = new PageRequest(evalPage, evalPageSize);

        Page<ProductDto> pages = productService.getAllProductsByName("", pageable);
        model.addObject("HotGifts", pages);
        Pager pager = new Pager(pages.getTotalPages(), pages.getNumber(), BUTTONS_TO_SHOW);
        model.addObject("selectedPageSize", evalPageSize);
        model.addObject("pageSizes", PAGE_SIZES);
        model.addObject("pager", pager);
        model.setViewName("gift/gift-landing");
        return model;
    }

    @GetMapping("/email/brand")
    public ModelAndView emailBrand() {
        ModelAndView model = new ModelAndView();
        model.setViewName("email/gift/gift_brand");
        return model;
    }

    @GetMapping("/email/sent")
    public ModelAndView emailSent() {
        ModelAndView model = new ModelAndView();
        model.setViewName("email/gift/gift_sent");
        return model;
    }

    @GetMapping("/email/received")
    public ModelAndView emailReceived() {
        ModelAndView model = new ModelAndView();
        model.setViewName("email/gift/gift_received");
        return model;
    }

}
