package com.realroofers.lovappy.web.rest.v1;

import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.product.BrandService;
import com.realroofers.lovappy.service.product.ProductService;
import com.realroofers.lovappy.service.product.ProductTypeService;
import com.realroofers.lovappy.service.product.dto.*;
import com.realroofers.lovappy.service.product.support.ProductAvailability;
import com.realroofers.lovappy.service.product.support.Upload;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.FormValidator;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.service.validation.ResponseStatus;
import com.realroofers.lovappy.service.vendor.VendorService;
import com.realroofers.lovappy.service.vendor.dto.VendorDto;
import com.realroofers.lovappy.service.vendor.model.VendorType;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.ExcelReader;
import com.realroofers.lovappy.web.util.HttpDownloadUtility;
import com.realroofers.lovappy.web.util.dto.DownloadFile;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Manoj on 03/02/2018.
 */
@RestController
@RequestMapping({"/api/v1/product", "/api/v2/product"})
public class RestProductController {

    private static String[] columns = {"Product Code/SKU", "Product Name", "Category",
            "Brand", "Product UPC/EAN", "Product Description", "Price", "Product Availability",
            "Product Image URL - 1", "Product Image URL - 2", "Product Image URL - 3"};

    private final ProductService productService;
    private final UserService userService;
    private final FormValidator formValidator;
    private final ProductTypeService productTypeService;
    private final BrandService brandService;
    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;
    private final CloudStorage cloudStorage;
    private final CloudStorageService cloudStorageService;
    private final VendorService vendorService;

    @Autowired
    public RestProductController(ProductService productService, UserService userService, FormValidator formValidator,
                                 ProductTypeService productTypeService, BrandService brandService, CloudStorage cloudStorage, CloudStorageService cloudStorageService, VendorService vendorService) {
        this.productService = productService;
        this.userService = userService;
        this.formValidator = formValidator;
        this.productTypeService = productTypeService;
        this.brandService = brandService;
        this.cloudStorage = cloudStorage;
        this.cloudStorageService = cloudStorageService;
        this.vendorService = vendorService;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity add(@ModelAttribute @Valid ProductRegDto productDto) {

        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = formValidator.validate(productDto, new Class[]{Upload.class});

        if (errorMessages.size() > 0) {
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(200).body(res);
        }
        try {
            final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
            VendorDto user = vendorService.findByUser(userId);
            String productCode = productDto.getProductCode();
            if (user.getVendorType().equals(VendorType.VENDOR)) {
                productDto.setProductCode("R" + user.getId() + "-" + productCode);
            } else {
                productDto.setProductCode("B" + user.getId() + "-" + productCode);
            }

            if (validateDuplicateCode(productDto.getProductCode(), res, errorMessages))
                return ResponseEntity.status(200).body(res);

            ProductDto product = productService.addProduct(productDto, userId);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseEntity update(@ModelAttribute @Valid ProductRegDto productDto) {

        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = formValidator.validate(productDto, new Class[]{Upload.class});

        if (errorMessages.size() > 0) {
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(200).body(res);
        }
        try {
            final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
            VendorDto user = vendorService.findByUser(userId);
            String productCode = productDto.getProductCode();
            if (user.getVendorType().equals(VendorType.VENDOR)) {
                productDto.setProductCode("R" + user.getId() + "-" + productCode);
            } else {
                productDto.setProductCode("B" + user.getId() + "-" + productCode);
            }

            if (validateDuplicateCodeOnUpdate(productDto.getProductCode(), productDto.getId(), res, errorMessages))
                return ResponseEntity.status(200).body(res);
            ProductDto product = productService.updateProduct(productDto, userId);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    private boolean validateDuplicateCode(String productCode, JsonResponse res, List<ErrorMessage> errorMessages) {
        if (productService.getAllProductsByCode(productCode).size() > 0) {
            ErrorMessage errorMessage = new ErrorMessage("productCode", "Duplicate Product Code");
            errorMessages.add(errorMessage);
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return true;
        }
        return false;
    }

    private boolean validateDuplicateCodeOnUpdate(String productCode, Integer id, JsonResponse res, List<ErrorMessage> errorMessages) {
        List<ProductDto> allProductsByCode = productService.getAllProductsByCode(productCode);
        if (allProductsByCode.size() >= 1
                && allProductsByCode.get(0).getId() != id) {
            ErrorMessage errorMessage = new ErrorMessage("productCode", "Duplicate Product Code");
            errorMessages.add(errorMessage);
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return true;
        }
        return false;
    }


    @RequestMapping(value = "/upload/excel", method = RequestMethod.POST)
    public ResponseEntity uploadExcel(@RequestParam("file") MultipartFile excelFile) {

        ExcelReader excelReader = new ExcelReader();
        try {
            List<ProductUploadDto> productList = excelReader.getProducts(excelFile.getInputStream());
            if (productList.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            JsonResponse res = new JsonResponse();
            List<ErrorMessage> errorMessages = new ArrayList<>();

            String code;

            final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
            final UserDto userDto = new UserDto(userId);
            VendorDto user = vendorService.findByUser(userId);
            if (user.getVendorType().equals(VendorType.VENDOR)) {
                code = "R" + user.getId() + "-";
            } else {
                code = "B" + user.getId() + "-";
            }

            for (ProductUploadDto product : productList) {
                errorMessages = formValidator.validateExcel(productList.indexOf(product), product, new Class[]{Upload.class});
                validateDuplicateCode(product.getProductCode(), res, errorMessages);
            }

            if (errorMessages.size() > 0) {
                res.setErrorMessageList(errorMessages);
                res.setStatus(ResponseStatus.FAIL);
                return ResponseEntity.status(500).body(res);
            }

            final List<ProductTypeDto> productTypeList = productTypeService.findAllActive();
            final List<BrandDto> brandList = brandService.findAllActive();

            productList.stream().forEach(p -> {

                ProductDto product = new ProductDto();
                product.setProductCode(code + p.getProductCode());
                product.setProductName(p.getProductName());

                ProductTypeDto productTypeDto = productTypeList.stream().filter(item -> Objects.equals(item.getTypeName(), p.getProductType())).findFirst().orElse(null);
                product.setProductType(productTypeDto);

                if (p.getBrand() != null) {
                    BrandDto brandDto = brandList.stream().filter(item -> Objects.equals(item.getBrandName(), p.getBrand())).findFirst().orElse(null);
                    product.setBrand(brandDto);
                }
                product.setProductEan(p.getProductEan());
                product.setProductDescription(p.getProductDescription());
                product.setPrice(new BigDecimal(p.getPrice()));
                if (p.getProductAvailability().equalsIgnoreCase("Available")) {
                    product.setProductAvailability(ProductAvailability.IN_STOCK);
                    product.setActive(true);
                } else {
                    product.setProductAvailability(ProductAvailability.OUT_OF_STOCK);
                    product.setActive(false);
                }
                product.setImage1(imageFile(p.getImage1()));

                if (p.getImage2() != null && !p.getImage2().equals("")) {
                    product.setImage2(imageFile(p.getImage2()));
                }
                if (p.getImage3() != null && !p.getImage3().equals("")) {
                    product.setImage3(imageFile(p.getImage3()));
                }
                try {
                    product.setCreatedBy(userDto);
                    productService.create(product);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/download", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Resource> downloadFile() throws IOException {

        Workbook excel = createExcel();

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        excel.write(buffer);
        byte[] bytes = buffer.toByteArray();
        InputStream inputStream = new ByteArrayInputStream(bytes);

        InputStreamResource resource = new InputStreamResource(inputStream);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=Lovappy-bulk-product-upload.xls");

        return ResponseEntity.ok()
                .headers(headers)
//                .contentLength(bytes.length())
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(resource);
    }

    public Workbook createExcel() {
        // Create a Workbook
        Workbook workbook = new HSSFWorkbook();

        // Create a Sheet
        Sheet sheet = workbook.createSheet("Products");

        // Create a Row
        Row headerRow = sheet.createRow(0);

        // Creating cells
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
        }

        String[] stringArray = productTypeService.getStringArray();
        String range = "$A$1:$A$";
        createDropDown(workbook, sheet, stringArray, range, 2, "type");

        stringArray = brandService.getStringArray();
        createDropDown(workbook, sheet, stringArray, range, 3, "brand");

        stringArray = new String[]{"Available", "Not-Available"};
        createDropDown(workbook, sheet, stringArray, range, 7, "availability");


        workbook.setSheetHidden(1, true);
        workbook.setSheetHidden(2, true);
        workbook.setSheetHidden(3, true);
        workbook.setSheetHidden(4, true);

        // Resize all columns to fit the content size
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        return workbook;
    }

    private void createDropDown(Workbook workbook, Sheet sheet, String[] stringArray, String range, int column, String name) {
        Sheet hiddenSheet = workbook.createSheet(name);
        for (int i = 0, length = stringArray.length; i < length; i++) {
            String cellVal = stringArray[i];
            Row row = hiddenSheet.createRow(i);
            Cell cell = row.createCell(0);
            cell.setCellValue(cellVal);
        }
        Name namedCell = workbook.createName();
        namedCell.setNameName(name);
        namedCell.setRefersToFormula(name + "!" + range + stringArray.length);
        DVConstraint constraint = DVConstraint.createFormulaListConstraint(name);
        CellRangeAddressList addressList = new CellRangeAddressList(1, 100000, column, column);
        DataValidation dataValidation = new HSSFDataValidation(addressList, constraint);
        dataValidation.setSuppressDropDownArrow(false);
        sheet.addValidationData(dataValidation);
    }

    private CloudStorageFileDto imageFile(String imageUrl) {
        CloudStorageFileDto added = null;
        try {
            DownloadFile downloadFile = HttpDownloadUtility.downloadFile(imageUrl);

            if (downloadFile.getImageBytes().length > -1) {
                CloudStorageFileDto coverFileDto = cloudStorage.uploadFile(downloadFile.getImageBytes(), imagesBucket, FilenameUtils.getExtension(downloadFile.getFileName()), downloadFile.getContentType(), true);
                added = cloudStorageService.add(coverFileDto);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return added;
    }


}
