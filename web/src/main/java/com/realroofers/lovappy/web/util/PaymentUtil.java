package com.realroofers.lovappy.web.util;

import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.squareup.connect.ApiClient;
import com.squareup.connect.ApiException;
import com.squareup.connect.Configuration;
import com.squareup.connect.api.TransactionsApi;
import com.squareup.connect.auth.OAuth;
import com.squareup.connect.models.ChargeRequest;
import com.squareup.connect.models.ChargeResponse;
import com.squareup.connect.models.Money;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component
public class PaymentUtil {

    @Value("${square.accessToken}")
    private String accessToken;

    @Value("${square.locationId}")
    private String locationId;

    @Value("${email.basePath}")
    private String baseUrl;

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentUtil.class);

    private final OrderService orderService;
    private final EmailTemplateService emailTemplateService;
    private final MailService mailService;
    private ITemplateEngine templateEngine;

    @Autowired
    public PaymentUtil(OrderService orderService, EmailTemplateService emailTemplateService, MailService mailService,
                       List<ViewResolver> viewResolvers) {
        this.orderService = orderService;
        this.emailTemplateService = emailTemplateService;
        this.mailService = mailService;

        for (ViewResolver viewResolver : viewResolvers) {
            if (viewResolver instanceof ThymeleafViewResolver) {
                ThymeleafViewResolver thymeleafViewResolver = (ThymeleafViewResolver) viewResolver;
                this.templateEngine = thymeleafViewResolver.getTemplateEngine();
            }
        }
        if (this.templateEngine == null) {
            throw new BeanInitializationException("No view resolver of type ThymeleafViewResolver found.");
        }
    }

    public Boolean executeTransaction(String nonce, OrderDto order, String userEmail) throws ApiException {
        ApiClient apiClient = Configuration.getDefaultApiClient();
        OAuth oauth2 = (OAuth) apiClient.getAuthentication("oauth2");
        oauth2.setAccessToken(accessToken);

        TransactionsApi api = new TransactionsApi(apiClient);
        String idempotencyKey = UUID.randomUUID().toString();
        ChargeRequest body = new ChargeRequest()
                .idempotencyKey(idempotencyKey)
                .amountMoney(new Money()
                        .amount(new Double(Math.ceil(order.getTotalPrice() * 100)).longValue())
                        .currency(Money.CurrencyEnum.USD))
                .referenceId(String.valueOf(order.getId()))
                .cardNonce(nonce);

            ChargeResponse response = api.charge(locationId, body);
            if (response.getErrors().size() > 0)
                return false;

            order = orderService.setOrderTransactionID(order.getId(), response.getTransaction().getId());
            if (order != null) {
                sendConfirmationEmail(order, userEmail);
                return true;
            }

        return false;
    }

    private void sendConfirmationEmail(OrderDto order, String userEmail){
        EmailTemplateDto emailTemplate =
                emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.PAYMENT_CONFIRMATION, "EN");

        emailTemplate.getVariables().put("##ORDER_NUMBER##", String.valueOf(order.getTransactionID()));
        emailTemplate.getVariables().put("##AMOUNT##", String.valueOf(order.getTotalPrice()));

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        emailTemplate.getVariables().put("##DATE##", dateFormat.format(new Date()));

        if(order.getUser() != null)
            userEmail = order.getUser().getEmail();

        new EmailTemplateFactory()
                .build(baseUrl, userEmail, templateEngine, mailService, emailTemplate)
                .send();
    }
}
