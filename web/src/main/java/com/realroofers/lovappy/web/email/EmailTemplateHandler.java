package com.realroofers.lovappy.web.email;

/**
 * Created by Daoud Shaheen on 9/9/2017.
 */
public interface EmailTemplateHandler {

    void send();
}
