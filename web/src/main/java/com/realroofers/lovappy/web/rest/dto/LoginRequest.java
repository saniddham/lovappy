package com.realroofers.lovappy.web.rest.dto;

import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Login Request object contain username and password
 *
 * @author Daoud Shaheen [03-01-2017]
 */
@EqualsAndHashCode
public class LoginRequest {

  @NotEmpty
  private String username;
  @NotEmpty
  private String password;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

}
