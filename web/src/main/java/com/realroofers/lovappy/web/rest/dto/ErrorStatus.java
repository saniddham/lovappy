package com.realroofers.lovappy.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.realroofers.lovappy.service.validation.ErrorMessage;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Daoud Shaheen on 9/3/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorStatus implements Serializable{

    private String error;
    @JsonProperty("error_description")
    private String message;
    @JsonProperty("error_messages")
    private List<ErrorMessage> errorMessages;
    public ErrorStatus(String error, String message) {
        this.error = error;
        this.message = message;
    }

    public ErrorStatus(String error, String message, List<ErrorMessage> errorMessages) {
        this.error = error;
        this.message = message;
        this.errorMessages = errorMessages;
    }
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ErrorMessage> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(List<ErrorMessage> errorMessages) {
        this.errorMessages = errorMessages;
    }
}
