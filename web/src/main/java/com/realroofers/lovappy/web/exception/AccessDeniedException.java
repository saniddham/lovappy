package com.realroofers.lovappy.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Daoud Shaheen on 12/9/2017.
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class AccessDeniedException extends Exception {
    public AccessDeniedException(String message) {
        super(message);
    }
}
