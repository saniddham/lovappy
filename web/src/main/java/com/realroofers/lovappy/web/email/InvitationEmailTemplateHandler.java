package com.realroofers.lovappy.web.email;

import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import org.thymeleaf.ITemplateEngine;

/**
 * Created by Daoud Shaheen on 9/11/2017.
 */
public class InvitationEmailTemplateHandler extends AbstractEmailTemplateHandler {


    public InvitationEmailTemplateHandler(String baseUrl, String email, MailService mailService, ITemplateEngine templateEngine, EmailTemplateDto emailTemplateDto) {
        super(baseUrl, email, mailService, templateEngine, emailTemplateDto);
    }

    @Override
    public void send() {

    }
}
