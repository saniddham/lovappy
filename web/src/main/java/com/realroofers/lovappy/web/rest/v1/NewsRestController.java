package com.realroofers.lovappy.web.rest.v1;

import com.realroofers.lovappy.service.news.NewsCategoryService;
import com.realroofers.lovappy.service.news.model.NewsCategory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Manoj
 */

@RestController
@RequestMapping({"/api/v1/news", "/api/v2/news"})
public class NewsRestController {

    private final NewsCategoryService newsCategoryService;

    public NewsRestController(NewsCategoryService newsCategoryService) {
        this.newsCategoryService = newsCategoryService;
    }

    @GetMapping("/categories")
    public ResponseEntity<List<NewsCategory>> getAllCategoris() {
        List<NewsCategory> list=  newsCategoryService.getAllNewsCategory();
        return  new ResponseEntity<List<NewsCategory>>(list, HttpStatus.OK);
    }
}
