package com.realroofers.lovappy.web.email;

import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import org.springframework.util.StringUtils;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Created by Daoud Shaheen on 9/10/2017.
 * handle  unblock picture
 */
public class UnLockProfilePictureEmailTemplateHandler extends AbstractEmailTemplateHandler {


    public UnLockProfilePictureEmailTemplateHandler(String baseUrl, String email, MailService mailService, ITemplateEngine templateEngine, EmailTemplateDto emailTemplateDto) {
        super(baseUrl, email, mailService, templateEngine, emailTemplateDto);
    }

    @Override
    public void send() {
        if (!emailTemplateDto.isEnabled())
            return;

        try {
            Context context = new Context();

            String body = emailTemplateDto.getBody();

            String userId = emailTemplateDto.getAdditionalInformation().get("userId");
            if (!StringUtils.isEmpty(userId)) {
                body.replace("NUMBER", userId);
            }
            context.setVariable("body", body);
            context.setVariable("approvalUrl", emailTemplateDto.getAdditionalInformation().get("approvalUrl"));
            context.setVariable("denialUrl", emailTemplateDto.getAdditionalInformation().get("denialUrl"));
            context.setVariable("baseUrl", baseUrl);
            context.setVariable("emailBG", emailTemplateDto.getImageUrl());
            String content = templateEngine.process(emailTemplateDto.getTemplateUrl(), context);
            mailService.sendMail(email, content, emailTemplateDto.getSubject(), emailTemplateDto.getFromEmail(), emailTemplateDto.getFromName());
        } catch (Exception ex) {
            LOGGER.error("Failed to send email: {}", ex);
        }
    }
}
