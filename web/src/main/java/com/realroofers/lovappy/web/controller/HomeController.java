package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.blog.PostService;
import com.realroofers.lovappy.service.blog.dto.PostDto;
import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.dto.VideoWidgetDTO;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import com.realroofers.lovappy.service.core.VideoProvider;
import com.realroofers.lovappy.service.radio.RadioService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@Controller
@RequestMapping("/")
public class HomeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);
    private final PostService postService;
    private final CMSService cmsService;
    private final RadioService radioService;

    @Value("${lovappy.cloud.bucket.lovdrop}")
    private String lovdropsBucket;

    @Autowired
    public HomeController(PostService postService, CMSService cmsService, RadioService radioService) {
        this.postService = postService;
        this.cmsService = cmsService;
        this.radioService = radioService;
    }

    @GetMapping
    public String swapHomePage(Model model, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        if (!model.containsAttribute("user")) {
            model.addAttribute("user", new UserDto());
        }

        if (UserUtil.INSTANCE.getCurrentUser() != null) {
            // In case another redirect with flash attributes send to this redirect
            Map<String, Object> asMap = model.asMap();
            redirectAttributes.addFlashAttribute("popup_msg", asMap.get("popup_msg"));
            redirectAttributes.addFlashAttribute("popup_msg_title", asMap.get("popup_msg_title"));
            redirectAttributes.addFlashAttribute("popup_msg_text", asMap.get("popup_msg_text"));

            return "redirect:/home";
        } else {
            model.addAttribute(PageConstants.VIDEO_HOMEPAGE_TITLE,
                    cmsService.getTextByTagAndLanguage(PageConstants.VIDEO_HOMEPAGE_TITLE, "en"));
            model.addAttribute(PageConstants.VIDEO_HOMEPAGE_LOVRADIO_TITLE,
                    cmsService.getTextByTagAndLanguage(PageConstants.VIDEO_HOMEPAGE_LOVRADIO_TITLE, "en"));
            model.addAttribute(PageConstants.VIDEO_HOMEPAGE_LOVRADIO_SUB_TITLE,
                    cmsService.getTextByTagAndLanguage(PageConstants.VIDEO_HOMEPAGE_LOVRADIO_SUB_TITLE, "en"));
            model.addAttribute(PageConstants.VIDEO_HOMEPAGE_LOVRADIO_EXAMPLE,
                    cmsService.getTextByTagAndLanguage(PageConstants.VIDEO_HOMEPAGE_LOVRADIO_EXAMPLE, "en"));

            VideoWidgetDTO videoWidgetDTO = cmsService.getVideosByKey(PageConstants.VIDEO_HOMEPAGE_VIDEO);
            VideoWidgetDTO mobileVideoWidgetDTO = cmsService.getVideosByKey(PageConstants.VIDEO_HOMEPAGE_MOBILE_VIDEO);

            if (videoWidgetDTO != null)
                model.addAttribute(PageConstants.VIDEO_HOMEPAGE_VIDEO,
                        videoWidgetDTO.getUrl());
            if (mobileVideoWidgetDTO  != null) {
                model.addAttribute(PageConstants.VIDEO_HOMEPAGE_MOBILE_VIDEO,
                        mobileVideoWidgetDTO.getProvider().equals(VideoProvider.YOUTUBE)?mobileVideoWidgetDTO.getProviderId() : mobileVideoWidgetDTO.getUrl());
            }
            model.addAttribute(PageConstants.VIDEO_HOMEPAGE_LOVRADIO_BG,
                    cmsService.getImageUrlByTag(PageConstants.VIDEO_HOMEPAGE_LOVRADIO_BG));
            model.addAttribute("register", request.getSession().getAttribute("register")==null? false : request.getSession().getAttribute("register"));
            model.addAttribute("publicLovstamp",
                    radioService.getHomePublicLovRadio(new PageRequest(0, 3)).getContent());
            model.addAttribute("rightPublicLovstamp",
                    radioService.getHomePublicLovRadio(new PageRequest(1, 3)).getContent());

            return "home";
        }
    }
    @GetMapping ("/home/register")
    public ModelAndView homeRegister(HttpServletRequest request){
        request.getSession(true).setAttribute("register", true);
        return new ModelAndView("redirect:/");
    }
    @GetMapping("/home")
    public String swapHomePage3(Model model) {

        model.addAttribute(PageConstants.ALT_HOMEPAGE_COVER_BG,
                cmsService.getImageUrlByTag(PageConstants.ALT_HOMEPAGE_COVER_BG));

        model.addAttribute(PageConstants.ALT_HOMEPAGE_COVER_HEADER,
                cmsService.getTextByTagAndLanguage(PageConstants.ALT_HOMEPAGE_COVER_HEADER, "en"));
        model.addAttribute(PageConstants.ALT_HOMEPAGE_COVER_SUB_HEADER,
                cmsService.getTextByTagAndLanguage(PageConstants.ALT_HOMEPAGE_COVER_SUB_HEADER, "en"));
        model.addAttribute(PageConstants.ALT_HOMEPAGE_COVER_LOVRADIO_EXAMPLE,
                cmsService.getTextByTagAndLanguage(PageConstants.ALT_HOMEPAGE_COVER_LOVRADIO_EXAMPLE, "en"));
        model.addAttribute(PageConstants.ALT_HOMEPAGE_LOVE_LISTENING_HEADER,
                cmsService.getTextByTagAndLanguage(PageConstants.ALT_HOMEPAGE_LOVE_LISTENING_HEADER, "en"));
        model.addAttribute(PageConstants.ALT_HOMEPAGE_LOVE_LISTENING_SUB_HEADER,
                cmsService.getTextByTagAndLanguage(PageConstants.ALT_HOMEPAGE_LOVE_LISTENING_SUB_HEADER, "en"));
        model.addAttribute(PageConstants.ALT_HOMEPAGE_LOVBLOG_TITLE,
                cmsService.getTextByTagAndLanguage(PageConstants.ALT_HOMEPAGE_LOVBLOG_TITLE, "en"));
        model.addAttribute(PageConstants.ALT_HOMEPAGE_LOVBLOG_SUB_TITLE,
                cmsService.getTextByTagAndLanguage(PageConstants.ALT_HOMEPAGE_LOVBLOG_SUB_TITLE, "en"));
        model.addAttribute(PageConstants.ALT_HOMEPAGE_LOVBLOG_FEATURED_VLOG,
                cmsService.getTextByTagAndLanguage(PageConstants.ALT_HOMEPAGE_LOVBLOG_FEATURED_VLOG, "en"));
        model.addAttribute(PageConstants.ALT_HOMEPAGE_LOVAPPY_CORPORATION,
                cmsService.getTextByTagAndLanguage(PageConstants.ALT_HOMEPAGE_LOVAPPY_CORPORATION, "en"));
        model.addAttribute(PageConstants.ALT_HOMEPAGE_LOVAPPY_CORPORATION_DESCRIPTION,
                cmsService.getTextByTagAndLanguage(PageConstants.ALT_HOMEPAGE_LOVAPPY_CORPORATION_DESCRIPTION, "en"));
        model.addAttribute(PageConstants.ALT_HOMEPAGE_LOVAPPY_APP_TITLE,
                cmsService.getTextByTagAndLanguage(PageConstants.ALT_HOMEPAGE_LOVAPPY_APP_TITLE, "en"));
        model.addAttribute(PageConstants.ALT_HOMEPAGE_LOVAPPY_APP_DESCRIPTION,
                cmsService.getTextByTagAndLanguage(PageConstants.ALT_HOMEPAGE_LOVAPPY_APP_DESCRIPTION, "en"));
        model.addAttribute(PageConstants.ALT_HOMEPAGE_LOVAPPY_APP_ANDROID_LINK,
                cmsService.getTextByTagAndLanguage(PageConstants.ALT_HOMEPAGE_LOVAPPY_APP_ANDROID_LINK, "en"));
        model.addAttribute(PageConstants.ALT_HOMEPAGE_LOVAPPY_APP_APPLE_LINK,
                cmsService.getTextByTagAndLanguage(PageConstants.ALT_HOMEPAGE_LOVAPPY_APP_APPLE_LINK, "en"));

        model.addAttribute("publicLovstamp",
                radioService.getHomePublicLovRadio(new PageRequest(0, 3)).getContent());
        model.addAttribute("rightPublicLovstamp",
                radioService.getHomePublicLovRadio(new PageRequest(1, 3)).getContent());

        List<PostDto> posts = postService.findMostViewed(1, 5).getContent();
        if(posts.size() > 0)
        model.addAttribute("mainPost", posts.get(0));

        if(posts.size()>1)
        model.addAttribute("posts", posts.subList(1, posts.size()));

        return "logged-home";
    }


    @GetMapping("/homenew")
    public String newHome() {
        return "home-new";
    }

    @GetMapping("/register-lite")
    public String registerLite(Model model) {
        if (!model.containsAttribute("user")) {
            model.addAttribute("user", new UserDto());
        }
        model.addAttribute("bannerImages", cmsService.getImagesByPageTagAndImageType("lite", "banner"));
        model.addAttribute(PageConstants.LITEPAGE_PARAGRAPH_H5, cmsService.getTextByTagAndLanguage(PageConstants.LITEPAGE_PARAGRAPH_H5, "EN"));

        return "register_lite";
    }

    @GetMapping("/register-prime")
    public String registerPrime(Model model) {
        if (!model.containsAttribute("user")) {
            model.addAttribute("user", new UserDto());
        }
        model.addAttribute("bannerImages", cmsService.getImagesByPageTagAndImageType("prime", "banner"));
        model.addAttribute(PageConstants.PRIMEPAGE_PARAGRAPH_H5, cmsService.getTextByTagAndLanguage(PageConstants.PRIMEPAGE_PARAGRAPH_H5, "EN"));

        return "register_prime";
    }

    @GetMapping("/account_not_verified")
    public String accountNotVerified(Model model) {
        model.addAttribute("popup_msg", "Email Verification");
        model.addAttribute("popup_msg_title", "Email Verification");
        model.addAttribute("popup_msg_text", "A verification email has been sent to you, please activate your account with the link attached.");

        return "blank";
    }

    @RequestMapping("/_ah/health")
    public ResponseEntity<String> healthCheck() {
        return new ResponseEntity<>("Healthy", HttpStatus.OK);
    }




}
