package com.realroofers.lovappy.web.rest.v2;

import com.realroofers.lovappy.service.credits.CreditService;
import com.realroofers.lovappy.service.credits.dto.CreditDto;
import com.realroofers.lovappy.service.user.UserUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Daoud Shaheen on 8/11/2018.
 */
@RestController
@RequestMapping({"/api/v2/credits", "/api/v1/credits"})
public class RestCreditsController {

    @Autowired
    private CreditService creditService;

    private static final Logger LOGGER = LoggerFactory.getLogger(RestCreditsController.class);
    @RequestMapping(method = RequestMethod.GET)
    public CreditDto get() {

        LOGGER.info("Get Credit {}", UserUtil.INSTANCE.getCurrentUserId());

        return creditService.getCredit(UserUtil.INSTANCE.getCurrentUserId());
    }
    @RequestMapping(method = RequestMethod.GET, value = "/{toUserId}")
    public CreditDto getByUser(@PathVariable("toUserId") Integer userId) {
        return creditService.getCreditByToUser(UserUtil.INSTANCE.getCurrentUserId(), userId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{toUserId}")
    public CreditDto send(@PathVariable("toUserId") Integer userId,
                          @RequestParam("balance") Integer balance) {
        return creditService.sendUser(UserUtil.INSTANCE.getCurrentUserId(), userId, balance);
    }


}
