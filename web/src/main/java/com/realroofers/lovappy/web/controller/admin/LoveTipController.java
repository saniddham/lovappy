package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.ads.AdsService;
import com.realroofers.lovappy.service.tips.TipsService;
import com.realroofers.lovappy.service.tips.dto.LoveTipDto;
import com.realroofers.lovappy.web.util.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Created by Daoud Shaheen on 1/1/2018.
 */
@Controller
@RequestMapping("/lox/tips")
public class LoveTipController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoveTipController.class);

    private final TipsService tipsService;
    private final AdsService adsService;

    public LoveTipController(TipsService tipsService, AdsService adsService) {
        this.tipsService = tipsService;
        this.adsService = adsService;
    }


    @GetMapping
    public ModelAndView getAll(ModelAndView modelAndView,
                              @RequestParam("pageSize") Optional<Integer> pageSize,
                              @RequestParam("page") Optional<Integer> pageNumber,
                              @RequestParam(value = "filter", defaultValue = "") String filter,
                              @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){

        PageRequest pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize));

        if(!StringUtils.isEmpty(filter)) {
            if(pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize), new Sort(sort, filter));
            }
        }
        modelAndView.addObject("filter", filter);
        modelAndView.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);

        Page<LoveTipDto> tipsPage = tipsService.getAll(pageable);
        modelAndView.addObject("backgroundImages", adsService.getAllBackgroundImages());
        modelAndView.addObject("backgroundColors", adsService.getAllBackgroundColors());
        modelAndView.addObject("tipsPage", tipsPage);
        Pager pager = new Pager(tipsPage.getTotalPages(), tipsPage.getNumber(), Pager.BUTTONS_TO_SHOW);
        modelAndView.addObject("newTip", new LoveTipDto());
        modelAndView.addObject("selectedPageSize", Pager.evalPageSize(pageSize));
        modelAndView.addObject("pageSizes", Pager.PAGE_SIZES);
        modelAndView.addObject("pager", pager);
        modelAndView.setViewName("admin/tips/tips-list");
        return modelAndView;
    }


    @PostMapping("/save")
    public ModelAndView save(LoveTipDto newTip, ModelAndView modelAndView) throws Exception {

        tipsService.create(newTip);
        modelAndView.setViewName("redirect:/lox/tips");
        return modelAndView;
    }

    @PostMapping("/{tip-id}/save")
    public ModelAndView update(@PathVariable("tip-id") Integer tipId, LoveTipDto tipDto, ModelAndView modelAndView) throws Exception {

        tipDto.setId(tipId);
        tipsService.update(tipDto);
        modelAndView.setViewName("redirect:/lox/tips");
        return modelAndView;
    }


    @PostMapping("/{tip-id}/active")
    public ResponseEntity showInHome(HttpServletRequest request, @PathVariable("tip-id") Integer id) {
        tipsService.setActive(id, true);
        return ResponseEntity.status(HttpStatus.OK).body("");
    }

    @DeleteMapping("/{tip-id}/active")
    public ResponseEntity hideFromHome(HttpServletRequest request, @PathVariable("tip-id") Integer id) {
        tipsService.setActive(id, false);
        return ResponseEntity.status(HttpStatus.OK).body("");
    }
}
