package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.datingPlaces.DatingPlaceService;
import com.realroofers.lovappy.service.datingPlaces.dto.*;
import com.realroofers.lovappy.service.datingPlaces.support.DatingPlaceSupportService;
import com.realroofers.lovappy.service.exception.EntityValidationException;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.service.validation.ResponseStatus;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darrel Rayen on 10/29/17.
 */
@Controller
@RequestMapping("/lox/dating/places")
public class DatingPlaceManagementController {

    private static final Logger LOG = LoggerFactory.getLogger(DatingPlaceManagementController.class);
    private static final String GOOGLE_KEY = "google_key";

    private final DatingPlaceService datingPlaceService;
    private final EmailTemplateService emailTemplateService;
    private final ITemplateEngine templateEngine;
    private final CloudStorage cloudStorage;
    private final CloudStorageService cloudStorageService;
    private final DatingPlaceSupportService datingPlaceSupportService;

    private MailService mailService;

    @Value("${google.maps.api.key}")
    private String googleKeyId;

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @Autowired
    public DatingPlaceManagementController(DatingPlaceService datingPlaceService, EmailTemplateService emailTemplateService, ITemplateEngine templateEngine, CloudStorage cloudStorage,
                                           CloudStorageService cloudStorageService, DatingPlaceSupportService datingPlaceSupportService, MailService mailService) {
        this.datingPlaceService = datingPlaceService;
        this.emailTemplateService = emailTemplateService;
        this.templateEngine = templateEngine;
        this.cloudStorage = cloudStorage;
        this.cloudStorageService = cloudStorageService;
        this.datingPlaceSupportService = datingPlaceSupportService;
        this.mailService = mailService;
    }

    @GetMapping
    public ModelAndView listPlaces(ModelAndView model, @RequestParam(value = "page", defaultValue = "1") Integer page) {
        if (page < 1) {
            throw new RuntimeException("Invalid Page Number");
        }
        Page<DatingPlaceDto> datingPlaceDtos = datingPlaceService.getAllPlaces(new PageRequest(page - 1, 10));
        model.setViewName("admin/dating_places/dating-places-list-admin");
        model.addObject("datingPlaces", datingPlaceDtos);
        model.addObject("actionUrl", "/lox/dating/places?");
        model.addObject("currentPage", page);
        model.addObject(GOOGLE_KEY, googleKeyId);
        return model;
    }

    @GetMapping("/reviews")
    public ModelAndView listPlacesReview(ModelAndView model, @RequestParam(value = "page", defaultValue = "1") Integer page) {
        if (page < 1) {
            throw new RuntimeException("Invalid Page Number");
        }
        Page<DatingPlaceReviewDto> datingPlaceReviews = datingPlaceService.getAllReviews(new PageRequest(page - 1, 10));
        model.setViewName("admin/dating_places/dating-places-review-list-admin");
        model.addObject("placeReviews", datingPlaceReviews);
        model.addObject("actionUrl", "/lox/dating/places/reviews?");
        model.addObject("currentPage", page);
        model.addObject(GOOGLE_KEY, googleKeyId);
        return model;
    }

    @GetMapping("/{id}")
    public ModelAndView locationDetailsById(ModelAndView model, @PathVariable(value = "id") Integer id) {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        DatingPlaceDto datingPlaceDto = datingPlaceService.findDatingPlaceById(id);
        if (datingPlaceDto == null) {
            model.setViewName("error/404");
            return model;
        }
        model.setViewName("admin/dating_places/dating-place-details-admin");
        model.addObject("userId", userId);
        model.addObject("placeReviewCreate", new DatingPlaceReviewDto());
        model.addObject(GOOGLE_KEY, googleKeyId);
        model.addObject("datingPlace", datingPlaceDto);
        return model;
    }

    @GetMapping("/activate/{id}")
    public ModelAndView approvePlace(HttpServletRequest request, ModelAndView modelAndView, @PathVariable("id") Integer placeId) {

        modelAndView.setViewName("redirect:/lox/dating/places/" + placeId);

        boolean isApproved = datingPlaceService.changePlaceApprovalStatus(placeId, ApprovalStatus.APPROVED);

        if (!isApproved) {
            DatingPlaceDto placeDto = datingPlaceService.findDatingPlaceById(placeId);

            //Send Approval email
            if (placeDto != null) {
                EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.SUBMITTED_PLACE_APPROVAL, "EN");
                emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/dating/places/" + placeDto.getDatingPlacesId());

                new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), placeDto.getCreateEmail(), templateEngine,
                        mailService, emailTemplate).send();
            }
        }
        return modelAndView;
    }

    @GetMapping("/feature/type/{featureType}/place/{id}")
    public ModelAndView featurePlace(ModelAndView modelAndView, @PathVariable("id") Integer placeId, @PathVariable("featureType") String featureType) {

        boolean isFeatured = datingPlaceService.featureDatingPlaceFromAdmin(placeId, featureType);
        if (isFeatured)
            modelAndView.setViewName("redirect:/lox/dating/places/" + placeId);
        else
            modelAndView.setViewName("redirect:/lox/dating/places/" + placeId);
        return modelAndView;
    }

    @GetMapping("/activate/deal/{id}")
    public ModelAndView approvePlaceDeal(HttpServletRequest request, ModelAndView modelAndView, @PathVariable("id") Integer datingDealId) {

        modelAndView.setViewName("redirect:/lox/dating/places/list/deals");

        boolean isApproved = datingPlaceService.changeDealApprovalStatus(datingDealId, ApprovalStatus.APPROVED);

        if (!isApproved) {
            DatingPlaceDealsDto dealsDto = datingPlaceService.findDatingDealById(datingDealId);

            //send Approval email
            if (dealsDto != null) {
                EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.SUBMITTED_PLACE_APPROVAL, "EN");
                //emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/dating/places/" + dealsDto.getDatingPlacesId());
                String emailId = dealsDto.getDatingPlace().getOwner().getEmail();
                new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), emailId, templateEngine,
                        mailService, emailTemplate).send();
            }
        }
        return modelAndView;
    }

    @GetMapping("/reject/deal/{id}")
    public ModelAndView rejectPlaceDeal(HttpServletRequest request, ModelAndView modelAndView, @PathVariable("id") Integer datingDealId) {

        modelAndView.setViewName("redirect:/lox/dating/places/list/deals");

        DatingPlaceDealsDto dealsDto = datingPlaceService.findDatingDealById(datingDealId);
        if (dealsDto != null && !dealsDto.getApproved().equals(ApprovalStatus.REJECTED.name())) {
            datingPlaceService.changeDealApprovalStatus(datingDealId, ApprovalStatus.REJECTED);

            //send denial email
            if (dealsDto.getPlaceDealId() != null) {
                EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.SUBMITTED_PLACE_DENIAL, "EN");
                //emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/dating/places/" + datingPlaceDto.getDatingPlacesId());
                String emailId = dealsDto.getDatingPlace().getOwner().getEmail();
                new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), emailId, templateEngine,
                        mailService, emailTemplate).send();
            }
        }
        return modelAndView;
    }

    @GetMapping("/reject/{id}")
    public ModelAndView rejectPlace(HttpServletRequest request, ModelAndView modelAndView, @PathVariable("id") Integer placeId) {

        modelAndView.setViewName("redirect:/lox/dating/places/" + placeId);
        DatingPlaceDto datingPlaceDto = datingPlaceService.findDatingPlaceById(placeId);
        if (datingPlaceDto != null && !datingPlaceDto.getApproved().equals(ApprovalStatus.REJECTED.name())) {
            datingPlaceService.changePlaceApprovalStatus(placeId, ApprovalStatus.REJECTED);

            //send denial email
            if (datingPlaceDto.getDatingPlacesId() != null) {
                EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.SUBMITTED_PLACE_DENIAL, "EN");
                emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/dating/places/" + datingPlaceDto.getDatingPlacesId());
                new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), datingPlaceDto.getCreateEmail(), templateEngine,
                        mailService, emailTemplate).send();
            }
        }
        return modelAndView;
    }

    @PostMapping("/update")
    public ResponseEntity updateDatingPlace(@ModelAttribute("datingPlace") DatingPlaceDto datingPlaceDto) {
        JsonResponse response = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (datingPlaceDto.getPlaceName().isEmpty()) {
            errorMessages.add(new ErrorMessage("placeName", "Provide a Name for the Dating Place"));
        }
        if (datingPlaceDto.getCity().isEmpty()) {
            errorMessages.add(new ErrorMessage("city", "Provide a city"));
        }
        if (datingPlaceDto.getAddressLine().isEmpty()) {
            errorMessages.add(new ErrorMessage("addressLine", "Provide an address"));
        }
        if (datingPlaceDto.getState().isEmpty()) {
            errorMessages.add(new ErrorMessage("state", "Provide a state"));
        }
        if (datingPlaceDto.getCountry().isEmpty()) {
            errorMessages.add(new ErrorMessage("country", "Select a Country"));
        }
        if (datingPlaceDto.getZipCode().isEmpty()) {
            errorMessages.add(new ErrorMessage("zipCode", "Provide a Zipcode"));
        }
/*        if (locationPictureId == null) {
            errorMessages.add(new ErrorMessage("datingLocationPhoto", "Upload an image"));
        }*/
        if (datingPlaceDto.getAgeRange() == null) {
            errorMessages.add(new ErrorMessage("ageRange", "Select an Age Range"));
        }
        if (datingPlaceDto.getGender() == null) {
            errorMessages.add(new ErrorMessage("gender", "Select Gender"));
        }
        if (datingPlaceDto.getPriceRange() == null) {
            errorMessages.add(new ErrorMessage("priceRange", "Select suitable Price Range"));
        }

        if (errorMessages.size() > 0) {
            response.setErrorMessageList(errorMessages);
            response.setStatus(com.realroofers.lovappy.service.validation.ResponseStatus.FAIL);
            throw new EntityValidationException(errorMessages);
        } else {

            DatingPlaceDto updatedPlace = datingPlaceService.updateDatingPlace(datingPlaceDto);
            if (updatedPlace != null) {
                response.setStatus(ResponseStatus.SUCCESS);
                LOG.debug("Dating Place updated successfully");
                return ResponseEntity.ok().body(response);
            }
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @GetMapping("reviews/activate/{id}")
    public ModelAndView approveReview(HttpServletRequest request, ModelAndView modelAndView, @PathVariable("id") Integer reviewId) {

        modelAndView.setViewName("redirect:/lox/dating/places/reviews");

        DatingPlaceReviewDto datingPlaceReviewDto = datingPlaceService.findReviewById(reviewId);
        boolean isApproved = datingPlaceService.changeReviewApprovalStatus(reviewId, ApprovalStatus.APPROVED);

        if (!isApproved) {
            DatingPlaceDto placeDto = datingPlaceService.getDatingPlaceByReviewId(reviewId);

            if (placeDto != null) {
                //send Approval email
                EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.SUBMITTED_REVIEW_APPROVAL, "EN");
                emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/dating/places/" + placeDto.getDatingPlacesId());

                new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), datingPlaceReviewDto.getReviewerEmail(), templateEngine,
                        mailService, emailTemplate).send();
            }
        }
        return modelAndView;
    }

    @GetMapping("reviews/reject/{id}")
    public ModelAndView rejectReview(HttpServletRequest request, ModelAndView modelAndView, @PathVariable("id") Integer reviewId) {

        modelAndView.setViewName("redirect:/lox/dating/places/reviews");

        DatingPlaceReviewDto datingPlaceReviewDtoDto = datingPlaceService.findReviewById(reviewId);

        if (datingPlaceReviewDtoDto != null && !datingPlaceReviewDtoDto.getApproved().equals(ApprovalStatus.REJECTED.name())) {
            datingPlaceService.changeReviewApprovalStatus(reviewId, ApprovalStatus.REJECTED);
            DatingPlaceDto placeDto = datingPlaceService.getDatingPlaceByReviewId(reviewId);

            //send denial email
            if (placeDto != null) {
                EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.SUBMITTED_REVIEW_DENIAL, "EN");
                emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/dating/places/" + placeDto.getDatingPlacesId());
                new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), datingPlaceReviewDtoDto.getReviewerEmail(), templateEngine,
                        mailService, emailTemplate).send();
            }
        }
        return modelAndView;
    }

    @GetMapping("reviews/feature/{id}")
    public ModelAndView featureReview(ModelAndView modelAndView, @PathVariable("id") Integer reviewId) {
        try {
            datingPlaceService.featurePlaceReview(reviewId);
            modelAndView.setViewName("redirect:/lox/dating/places/reviews");
        } catch (Exception e) {
            modelAndView.setViewName("error/404");
        }
        return modelAndView;
    }

    @GetMapping("/reviews/delete/{id}")
    public ModelAndView deletePlaceReview(ModelAndView modelAndView, @PathVariable("id") Integer reviewId) {

        boolean isDeleted = datingPlaceService.deletePlaceReview(reviewId);

        if (isDeleted) {
            modelAndView.setViewName("redirect:/lox/dating/places/reviews");
        } else {
            modelAndView.setViewName("error/404");
        }
        return modelAndView;

    }

    @GetMapping("/food/types")
    public ModelAndView listFoodTypes(ModelAndView model) {

        List<FoodTypeDto> foodTypes = datingPlaceSupportService.getAllFoodTypes();
        model.setViewName("admin/dating_places/food_types");
        model.addObject("foodTypes", foodTypes);
        return model;
    }

    @GetMapping("/food/types/{id}")
    public ModelAndView listFoodTypesById(ModelAndView model, @PathVariable("id") Integer foodTypeId) {
        model.setViewName("admin/dating_places/food_type_details");
        model.addObject("foodType", datingPlaceSupportService.getFoodTypeById(foodTypeId));
        return model;
    }

    @GetMapping("/food/types/add")
    public ModelAndView addFoodTypes(ModelAndView model) {
        model.setViewName("admin/dating_places/food_type_details");
        model.addObject("foodType", new FoodTypeDto());
        return model;
    }

    @PostMapping("/food/types/save")
    public ResponseEntity saveFoodType(@ModelAttribute("foodType") FoodTypeDto foodTypeDto) {
        if (foodTypeDto.getId() != null) {
            FoodTypeDto dto = datingPlaceSupportService.updateFoodType(foodTypeDto);
            return ResponseEntity.status(HttpStatus.OK).body(dto.getId());
        } else {
            FoodTypeDto dto = datingPlaceSupportService.addFoodType(foodTypeDto);
            return ResponseEntity.status(HttpStatus.OK).body(dto.getId());
        }
    }

    @GetMapping("/personTypes")
    public ModelAndView listPersonTypes(ModelAndView model) {
        List<PersonTypeDto> personTypeDtos = datingPlaceSupportService.getAllPersonTypes();
        model.setViewName("admin/dating_places/person_types");
        model.addObject("personTypes", personTypeDtos);
        return model;
    }

    @GetMapping("/personTypes/{id}")
    public ModelAndView listPersonTypesById(ModelAndView model, @PathVariable("id") Integer id) {
        model.setViewName("admin/dating_places/person_types_details");
        model.addObject("personType", datingPlaceSupportService.getPersonTypeById(id));
        return model;
    }

    @GetMapping("/personTypes/add")
    public ModelAndView addpersonTypes(ModelAndView model) {
        model.setViewName("admin/dating_places/person_types_details");
        model.addObject("personType", new PersonTypeDto());
        return model;
    }

    @PostMapping("/personTypes/save")
    public ResponseEntity savePersonType(@ModelAttribute("personType") PersonTypeDto personTypeDto) {
        if (personTypeDto.getId() != null) {
            PersonTypeDto dto = datingPlaceSupportService.updatePersonType(personTypeDto);
            return ResponseEntity.status(HttpStatus.OK).body(dto.getId());
        } else {
            PersonTypeDto dto = datingPlaceSupportService.addPersonType(personTypeDto);
            return ResponseEntity.status(HttpStatus.OK).body(dto.getId());
        }
    }

    @GetMapping("/list/deals")
    public ModelAndView listAllDeals(ModelAndView model, @RequestParam(value = "page", defaultValue = "1") Integer page) {
        if (page < 1) {
            throw new RuntimeException("Invalid Page Number");
        }
        Page<DatingPlaceDealsDto> datingPlaceDealsDtos = datingPlaceService.getAllPlaceDeals(new PageRequest(page - 1, 10));
        model.setViewName("admin/dating_places/deal-approval-list-admin");
        model.addObject("datingPlacesDeals", datingPlaceDealsDtos);
        model.addObject("actionUrl", "/lox/dating/places/list/deals?");
        model.addObject("currentPage", page);
        return model;
    }

    @GetMapping("/atmospheres")
    public ModelAndView listAtmosphere(ModelAndView model) {
        List<PlaceAtmosphereDto> placeAtmosphereDtoList = datingPlaceSupportService.getAllAtmospheres();
        model.setViewName("admin/dating_places/place_atmospheres");
        model.addObject("atmospheres", placeAtmosphereDtoList);
        return model;
    }

    @GetMapping("/atmospheres/{id}")
    public ModelAndView listAtmosphereById(ModelAndView model, @PathVariable("id") Integer id) {
        model.setViewName("admin/dating_places/place_atmosphere_details");
        model.addObject("atmosphere", datingPlaceSupportService.getAtmosphereById(id));
        return model;
    }

    @GetMapping("/atmospheres/add")
    public ModelAndView addPlaceAtmosphere(ModelAndView model) {
        model.setViewName("admin/dating_places/place_atmosphere_details");
        model.addObject("atmosphere", new PlaceAtmosphereDto());
        return model;
    }

    @PostMapping("/atmospheres/save")
    public ResponseEntity saveAtmosphere(@ModelAttribute("atmosphere") PlaceAtmosphereDto atmosphereDto) {
        if (atmosphereDto.getId() != null) {
            PlaceAtmosphereDto dto = datingPlaceSupportService.updatePlaceAtmosphere(atmosphereDto);
            return ResponseEntity.status(HttpStatus.OK).body(dto.getId());
        } else {
            PlaceAtmosphereDto dto = datingPlaceSupportService.addPlaceAtmosphere(atmosphereDto);
            return ResponseEntity.status(HttpStatus.OK).body(dto.getId());
        }
    }

    @GetMapping("/deals")
    public ModelAndView listPlacesPricing(ModelAndView model, @RequestParam(value = "page", defaultValue = "1") Integer page) {
        if (page < 1) {
            throw new RuntimeException("Invalid Page Number");
        }
        model.addObject("deals", datingPlaceService.getAllDealTypes(new PageRequest(page - 1, 10)));
        model.addObject("datingDeals", new DealsDto());
        model.addObject("actionUrl", "/lox/dating/places/deals?");
        model.addObject("currentPage", page);
        model.setViewName("admin/dating_places/dating-place-pricing");
        return model;
    }

    @PostMapping("/deals/save")
    public ResponseEntity saveDeal(@ModelAttribute("datingDeals") DealsDto dealsDto) {
        JsonResponse response = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();
        if (dealsDto.getStartPrice() == null) {
            errorMessages.add(new ErrorMessage("startPrice", "Please enter a start price"));
        }
        if (dealsDto.getEndPrice() == null) {
            errorMessages.add(new ErrorMessage("endPrice", "Please enter a end price"));
        }
        if (dealsDto.getFeePercentage() == null) {
            errorMessages.add(new ErrorMessage("fee", "Please enter a fee percentage"));
        }
        if (dealsDto.getStartPrice() != null && dealsDto.getEndPrice() != null) {
            if (dealsDto.getStartPrice() > dealsDto.getEndPrice()) {

                errorMessages.add(new ErrorMessage("startPrice", "start price should be less than end price"));
            }
        }
        if (dealsDto.getFeePercentage() != null && (dealsDto.getFeePercentage() > 100 || dealsDto.getFeePercentage() < 0)) {

            errorMessages.add(new ErrorMessage("fee", "The fee percentage should be within 0-100"));
        }
        if (datingPlaceService.isDealRangeExisting(dealsDto.getStartPrice())) {

            errorMessages.add(new ErrorMessage("startPrice", dealsDto.getStartPrice() + " is already included in a deal"));
        }
        if (datingPlaceService.isDealRangeExisting(dealsDto.getEndPrice())) {

            errorMessages.add(new ErrorMessage("endPrice", dealsDto.getEndPrice() + " is already included in a deal"));
        }
        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        } else {
            DealsDto savedDto = datingPlaceService.addDeal(dealsDto);
            response.setStatus(ResponseStatus.SUCCESS);
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }
    }

    @GetMapping("/deals/{id}")
    public ModelAndView findDealById(ModelAndView model, @PathVariable("id") Integer id) {
        model.setViewName("admin/dating_places/deal_details");
        model.addObject("placeDeal", datingPlaceService.getdealById(id));
        return model;
    }

    @PostMapping("/deals/update")
    public ResponseEntity updateDeal(@ModelAttribute("placeDeal") DealsDto dealsDto) {
        JsonResponse response = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();
        if (dealsDto.getStartPrice() == null) {
            errorMessages.add(new ErrorMessage("startPrice", "Please enter a start price"));
        }
        if (dealsDto.getEndPrice() == null) {
            errorMessages.add(new ErrorMessage("endPrice", "Please enter a end price"));
        }
        if (dealsDto.getFeePercentage() == null) {
            errorMessages.add(new ErrorMessage("fee", "Please enter a fee percentage"));
        }
        if (dealsDto.getStartPrice() != null && dealsDto.getEndPrice() != null) {
            if (dealsDto.getStartPrice() > dealsDto.getEndPrice()) {
                errorMessages.add(new ErrorMessage("startPrice1", "start price should be less than end price"));
            }
        }
        if (dealsDto.getFeePercentage() != null && (dealsDto.getFeePercentage() > 100 || dealsDto.getFeePercentage() < 0)) {

            errorMessages.add(new ErrorMessage("fee", "The fee percentage should be within 0-100"));
        }
        if (datingPlaceService.isDealRangeExistsFor(dealsDto.getStartPrice(), dealsDto.getDealId())) {

            errorMessages.add(new ErrorMessage("startPrice", dealsDto.getStartPrice() + " is already included in a deal"));
        }
        if (datingPlaceService.isDealRangeExistsFor(dealsDto.getEndPrice(), dealsDto.getDealId())) {

            errorMessages.add(new ErrorMessage("endPrice", dealsDto.getEndPrice() + " is already included in a deal"));
        }
        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        } else {
            DealsDto savedDto = datingPlaceService.updateDeal(dealsDto);
            response.setStatus(ResponseStatus.SUCCESS);
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }
    }
}
