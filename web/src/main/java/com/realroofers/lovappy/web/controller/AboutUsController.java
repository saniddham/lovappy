package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Locale;

/**
 * Created by Daoud Shaheen on 6/10/2017.
 */
@Controller
@RequestMapping("/about-us")
public class AboutUsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AboutUsController.class);

    private  CMSService cmsService;

    public AboutUsController(CMSService cmsService) {
        this.cmsService = cmsService;
    }

    @GetMapping
    public ModelAndView aboutUs(ModelAndView model, @RequestParam(value = "lang", defaultValue = "EN") String language) {
            Locale locale = LocaleContextHolder.getLocale();
            language = locale.getLanguage();
            model.addObject(PageConstants.ABOUT_US_DESCRIPTION, cmsService.getTextByTagAndLanguage(PageConstants.ABOUT_US_DESCRIPTION, language).replace("\n","<br>"));
            model.addObject(PageConstants.ABOUT_US_HEADER, cmsService.getTextByTagAndLanguage(PageConstants.ABOUT_US_HEADER, language));
            model.addObject(PageConstants.ABOUT_US_BG, cmsService.getImageUrlByTag(PageConstants.ABOUT_US_BG));
            model.setViewName("about-us");
            return model;
    }
}
