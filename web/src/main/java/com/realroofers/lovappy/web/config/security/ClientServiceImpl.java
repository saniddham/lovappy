package com.realroofers.lovappy.web.config.security;

import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;

public class ClientServiceImpl extends JdbcClientDetailsService implements ClientService {

  public ClientServiceImpl(DataSource dataSource) {
    super(dataSource);
  }
}
