package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.likes.LikeService;
import com.realroofers.lovappy.service.likes.dto.LikeDto;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.MyAccountDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.dto.UserFlagDto;
import com.realroofers.lovappy.service.user.dto.UserLikesDto;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.util.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author mwiyono
 */
@Controller
@RequestMapping("/like")
public class LikeController {

	private static final Logger LOGGER = LoggerFactory.getLogger(LikeController.class);

	private final LikeService likeService;
	private final UserService userService;
	private final EmailTemplateService emailTemplateService;
	private final MailService mailService;
	private final ITemplateEngine templateEngine;

	@Autowired
	public LikeController(LikeService likeService, UserService userService, EmailTemplateService emailTemplateService, MailService mailService, ITemplateEngine templateEngine) {
		super();
		this.likeService = likeService;
		this.userService = userService;
		this.emailTemplateService = emailTemplateService;
		this.mailService = mailService;
		this.templateEngine = templateEngine;
	}

	@PostMapping("/{user-id}")
	@ResponseBody
	public ResponseEntity<?> sendALike(@PathVariable("user-id") Integer userId,
									   HttpServletRequest request,
									   @RequestParam(value = "lang", defaultValue = "EN") String language) {
		LOGGER.info("POST sendALike To {}", userId);
		if (UserUtil.INSTANCE.getCurrentUserId().equals(userId)) {
			LOGGER.error("Can not like the same as login User :  {} ", UserUtil.INSTANCE.getCurrentUser().getEmail());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		//validate, if the user already like him/her, addLike should be skip or give error response
		try {
			LikeDto like = likeService.addLike(UserUtil.INSTANCE.getCurrentUserId(), userId);

			if(like != null) {
				//get Like acount
				MyAccountDto myAccountDto = userService.getMyAccount(userId);

					EmailTemplateDto emailTemplate = null;
					//send match email
					if (likeService.match(UserUtil.INSTANCE.getCurrentUserId(), userId)) {
						//send Likes email
						if (myAccountDto.isNewMatchesNotifications())
						emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.NEW_MATCHES, language);
					} else if (myAccountDto.isNewLikesNotifications()) {
						//send Likes email
						emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.NEW_LIKES, language);
					}

					if(emailTemplate != null) {
						emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/member/" + userId);
						new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), like.getLikeTo().getEmail(), templateEngine,
								mailService, emailTemplate).send();
					}
				}
//			}

		}catch (Exception ex) {
			LOGGER.error(" error add new like",ex);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@DeleteMapping("/{user-id}")
	@ResponseBody
	public ResponseEntity<?> unLike(@PathVariable("user-id") Integer userId) {
		LOGGER.info("DELETE sendALike To {}", userId);
		if (UserUtil.INSTANCE.getCurrentUserId().equals(userId)) {
			LOGGER.error("Can not like the same as login User :  {} ", UserUtil.INSTANCE.getCurrentUser().getEmail());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		//validate, if the user already like him/her, addLike should be skip or give error response
		 likeService.unLike(UserUtil.INSTANCE.getCurrentUserId(), userId);

		return new ResponseEntity<>(HttpStatus.OK);
	}
	/**
	 * Get Profile That Login User like
	 * @param model
	 * @return
	 */
	@GetMapping("/mylikes")
	public ModelAndView myLikes(@RequestParam(name = "page", required = true, defaultValue = "1") Integer page,
						  ModelAndView model) {

        Page<UserLikesDto> myLikes = likeService.getLikesByUserLikeBy(page, 8, UserUtil.INSTANCE.getCurrentUserId());
		model.addObject("pageTitle", "My Likes");
		return setLikeViews(model, myLikes, page, "/like/mylikes",
				"red-box", "green-box", "green-box");
	}

	@GetMapping("/likeme")
	public ModelAndView likeMe(@RequestParam(name = "page", required = true, defaultValue = "1") Integer page,
								ModelAndView model) {

		Page<UserLikesDto> likeme = likeService.getLikesByUserLikeTo(page, 8, UserUtil.INSTANCE.getCurrentUserId());

		model.addObject("pageTitle", "Like Me");
		return setLikeViews(model, likeme, page, "/like/likeme",
				"green-box", "red-box", "green-box");
	}

	@GetMapping("/matches")
	public ModelAndView matches(@RequestParam(name = "page", required = true, defaultValue = "1") Integer page,
							   ModelAndView model) {

		Page<UserLikesDto> matches = likeService.getMatchesForUser(page, 8, UserUtil.INSTANCE.getCurrentUserId());
		model.addObject("pageTitle", "Matches");
		return setLikeViews(model, matches, page, "/like/matches",
				"green-box", "green-box", "red-box");
	}
	ModelAndView setLikeViews(ModelAndView model, Page<UserLikesDto> likesPage, int page, String pageAction,
							String myLikesBoxColor, String likeMeBoxColor, String matchesBoxColor  ) {
		UserDto user = userService.getUser(UserUtil.INSTANCE.getCurrentUserId());
		model.addObject("mylikesCount", likeService.countMyLikesByUserId(UserUtil.INSTANCE.getCurrentUserId()));
		model.addObject("likeMeCount", likeService.countLikeMeByUserId(UserUtil.INSTANCE.getCurrentUserId()));
		model.addObject("matchLikesCount", likeService.countMatchesByUserId(user.getID()));
		model.addObject("likesPage", likesPage);
		model.addObject("currentPage", page);
		model.addObject("userFlag", new UserFlagDto());
		model.addObject("pageAction", pageAction );
		model.addObject("myLikesBoxColor", myLikesBoxColor );
		model.addObject("likemeBoxColor", likeMeBoxColor );
		model.addObject("matchesBoxColor", matchesBoxColor );
		model.setViewName("like/likes");

		return model;
	}

}
