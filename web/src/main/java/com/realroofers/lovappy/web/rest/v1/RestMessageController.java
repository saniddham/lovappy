package com.realroofers.lovappy.web.rest.v1;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.credits.CreditService;
import com.realroofers.lovappy.service.exception.GenericServiceException;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.privatemessage.PrivateMessageService;
import com.realroofers.lovappy.service.privatemessage.dto.PrivateMessageDto;
import com.realroofers.lovappy.service.privatemessage.support.Status;
import com.realroofers.lovappy.service.system.LanguageService;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.MyAccountDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import com.realroofers.lovappy.web.util.StripeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;

/**
 * @author Eias Altawil
 */

@RestController
@RequestMapping({"/api/v1/message", "/api/v2/message"})
public class RestMessageController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestMessageController.class);

    private final PrivateMessageService privateMessageService;


    @Value("${lovappy.cloud.bucket.privateMessage}")
    private String privateMessageBucket;

    private final CloudStorage cloudStorage;
    private final LanguageService languageService;
    private final OrderService orderService;
    private final UserService userService;
    private final UserProfileService userProfileService;
    private  final StripeUtil stripeUtil;
    private final EmailTemplateService emailTemplateService;
    private final CreditService creditService;

    private ITemplateEngine templateEngine;
    private MailService mailService;

    @Autowired
    public RestMessageController(PrivateMessageService privateMessageService,
                                 CloudStorage cloudStorage,
                                 LanguageService languageService,

                                 OrderService orderService,
                                 UserService userService,
                                 UserProfileService userProfileService,
                                 StripeUtil stripeUtil, EmailTemplateService emailTemplateService,
                                 CreditService creditService, ITemplateEngine templateEngine, MailService mailService) {
        this.privateMessageService = privateMessageService;
        this.cloudStorage = cloudStorage;
        this.languageService = languageService;

        this.orderService = orderService;
        this.userService = userService;
        this.userProfileService = userProfileService;
        this.stripeUtil = stripeUtil;
        this.emailTemplateService = emailTemplateService;
        this.creditService = creditService;
        this.templateEngine = templateEngine;
        this.mailService = mailService;
    }

    @RequestMapping(value = "/inbox", method = RequestMethod.GET)
    public ResponseEntity<Page<PrivateMessageDto>> getAllInbox(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                               @RequestParam(value = "sort") String sortOrder) {
        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        String sort = "sentAt";
        Sort.Direction sortDirection = Sort.Direction.DESC;

        if (sortOrder.equalsIgnoreCase("Date")) {
            sort = "sentAt";
            sortDirection = Sort.Direction.ASC;
        }else if (sortOrder.equalsIgnoreCase("Date_DESC")) {
            sort = "sentAt";
            sortDirection = Sort.Direction.DESC;
        }else if (sortOrder.equalsIgnoreCase("User")) {
            sort = "fromUser";
            sortDirection = Sort.Direction.ASC;
        }else if (sortOrder.equalsIgnoreCase("User_DESC")) {
            sort = "fromUser";
            sortDirection = Sort.Direction.DESC;
        }

        return ResponseEntity.ok(privateMessageService.getReceivedPrivateMessages(userId, new PageRequest(page - 1, 10, sortDirection, sort)));
    }

    @RequestMapping(value = "/inbox/count", method = RequestMethod.GET)
    public long getInboxCount() {
        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        return privateMessageService.countRecived(userId);
    }


    @RequestMapping(value = "/outbox", method = RequestMethod.GET)
    public ResponseEntity<Page<PrivateMessageDto>> getAllOutbox(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                                @RequestParam(value = "sort") String sortOrder) {
        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        String sort = "sentAt";
        Sort.Direction sortDirection = Sort.Direction.DESC;

        if (sortOrder.equalsIgnoreCase("Date")) {
            sort = "sentAt";
            sortDirection = Sort.Direction.ASC;
        }else if (sortOrder.equalsIgnoreCase("Date_DESC")) {
            sort = "sentAt";
            sortDirection = Sort.Direction.DESC;
        }else if (sortOrder.equalsIgnoreCase("User")) {
            sort = "toUser";
            sortDirection = Sort.Direction.ASC;
        }else if (sortOrder.equalsIgnoreCase("User_DESC")) {
            sort = "toUser";
            sortDirection = Sort.Direction.DESC;
        }
        return ResponseEntity.ok(privateMessageService.getSentPrivateMessages(userId, new PageRequest(page - 1, 10, sortDirection, sort)));
    }

    @RequestMapping(value = "/outbox/count", method = RequestMethod.GET)
    public long getOutboxCount() {
        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        return privateMessageService.countSent(userId);
    }


    @PostMapping("/send")
    public OrderDto sendPrivateMessage(HttpServletRequest request,
                                                @RequestParam("file") MultipartFile file,
                                                @RequestParam("userID") Integer userID,
                                                @RequestParam(value = "language", defaultValue = "EN") String language,
                                                @RequestParam("coupon") String coupon) {
        try {
            CloudStorageFileDto cloudStorageFileDto =
                    cloudStorage.uploadFile(file, privateMessageBucket, FileExtension.mp3.toString(), "audio/mp3");

            if (cloudStorageFileDto != null) {
                PrivateMessageDto privateMessageDto = new PrivateMessageDto();
                privateMessageDto.setFromUser(new UserDto(UserUtil.INSTANCE.getCurrentUserId()));
                privateMessageDto.setToUser(new UserDto(userID));
                privateMessageDto.setAudioFileCloud(cloudStorageFileDto);

                LanguageDto languageByCode = languageService.getLanguageByCode(language);
                privateMessageDto.setLanguage(languageByCode);

                OrderDto orderDto = privateMessageService.sendPrivateMessage(privateMessageDto, coupon, PaymentMethodType.STRIPE);
                if (stripeUtil.executeTransaction(UserUtil.INSTANCE.getCurrentUserId(), orderDto, UserUtil.INSTANCE.getCurrentUser().getEmail())) {

                    PrivateMessageDto message = privateMessageService.getPrivateMessageByID(orderDto.getOrderDetails().stream().findFirst().get().getOriginalId().intValue());

                    privateMessageService.updatePrivateMessageStatus(message.getId(), Status.PAID);

                    sendReceiptEmail(message.getFromUser().getEmail(), orderDto.getTotalPrice(), orderDto.getDate());


                    MyAccountDto myAccountDto = userService.getMyAccount(userID);

                    if (myAccountDto.isNewMessagesNotifications()) {
                        //Send to the user who received the private message
                        sendToReceiptEmail(message.getToUser().getEmail(), message.getFromUser().getID(),
                                CommonUtils.getBaseURL(request), "EN", cloudStorageFileDto.getUrl());
                    }

                    //saveTransaction
                    if (userProfileService.saveTransaction(UserUtil.INSTANCE.getCurrentUserId(), userID)) {
                        //send  email to user
                        EmailTemplateDto unlockEmailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.PHOTO_UNLOCK, "EN");
                        unlockEmailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/member/" + UserUtil.INSTANCE.getCurrentUserId());
                        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), myAccountDto.getEmail(), templateEngine,
                                mailService, unlockEmailTemplate).send();
                    }

                    return orderDto;
                } else {
                    orderService.revertOrder(orderDto.getId());
                }

            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

     throw new GenericServiceException("Something went wrong please try again");
    }
    @PostMapping("/credits/send")
    public ResponseEntity<?> sendPrivateMessageByCredits(HttpServletRequest request,
                                                @RequestParam("file") MultipartFile file,
                                                @RequestParam("userID") Integer userID,
                                                @RequestParam(value = "language", defaultValue = "EN") String language) {
        try {
            CloudStorageFileDto cloudStorageFileDto =
                    cloudStorage.uploadFile(file, privateMessageBucket, FileExtension.mp3.toString(), "audio/mp3");

            if (cloudStorageFileDto != null) {
                PrivateMessageDto privateMessageDto = new PrivateMessageDto();
                privateMessageDto.setFromUser(new UserDto(UserUtil.INSTANCE.getCurrentUserId()));
                privateMessageDto.setToUser(new UserDto(userID));
                privateMessageDto.setAudioFileCloud(cloudStorageFileDto);

                LanguageDto languageByCode = languageService.getLanguageByCode(language);
                privateMessageDto.setLanguage(languageByCode);

                PrivateMessageDto privateMessage = privateMessageService.sendPrivateMessageByCredit(privateMessageDto);
                if (privateMessage != null) {

                    privateMessageService.updatePrivateMessageStatus(privateMessage.getId(), Status.PAID);

                    MyAccountDto myAccountDto = userService.getMyAccount(userID);

                    if (myAccountDto.isNewMessagesNotifications()) {
                        //Send to the user who received the private message
                        sendToReceiptEmail(privateMessage.getToUser().getEmail(), privateMessage.getFromUser().getID(),
                                CommonUtils.getBaseURL(request), "EN", cloudStorageFileDto.getUrl());
                    }

                    //saveTransaction
                    if (userProfileService.saveTransaction(UserUtil.INSTANCE.getCurrentUserId(), userID)) {
                        //send  email to user
                        EmailTemplateDto unlockEmailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.PHOTO_UNLOCK, "EN");
                        unlockEmailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/member/" + UserUtil.INSTANCE.getCurrentUserId());
                        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), myAccountDto.getEmail(), templateEngine,
                                mailService, unlockEmailTemplate).send();
                    }

                    return new ResponseEntity<>(HttpStatus.OK);
                }

            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private void sendReceiptEmail(String toUserEmail, Double price, Date sentAt) {
        Context context = new Context();
        context.setVariable("price", price);
        context.setVariable("sentAt", sentAt);
        String content = templateEngine.process("email/sent_message_email", context);
        mailService.sendMail(toUserEmail, content, "Private Message Sent");
    }


    /**
     * Send email to user who recived the song
     *
     * @param toUserEmail
     * @param baseUrl
     * @param language
     * @param link
     */
    private void sendToReceiptEmail(String toUserEmail, Integer fromUserId, String baseUrl, String language, String link) {
        EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.NEW_MESSAGES, language);
        emailTemplate.getAdditionalInformation().put("url", link);
        emailTemplate.getAdditionalInformation().put("userId", String.valueOf(fromUserId));
        new EmailTemplateFactory().build(baseUrl, toUserEmail, templateEngine,
                mailService, emailTemplate).send();
    }

    @RequestMapping(value = "/calculate_price", method = RequestMethod.GET)
    public ResponseEntity<CalculatePriceDto> calculatePrice(@RequestParam("coupon") String coupon) {

        return ResponseEntity.ok(privateMessageService.calculatePrice(coupon));
    }
}
