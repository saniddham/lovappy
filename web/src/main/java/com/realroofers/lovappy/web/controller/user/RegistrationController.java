package com.realroofers.lovappy.web.controller.user;

import com.realroofers.lovappy.service.couples.CoupleProfileService;
import com.realroofers.lovappy.service.couples.dto.CouplesProfileDto;
import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.survey.UserSurveyService;
import com.realroofers.lovappy.service.survey.dto.UserSurveyDto;
import com.realroofers.lovappy.service.system.LanguageService;
import com.realroofers.lovappy.service.user.*;
import com.realroofers.lovappy.service.user.dto.*;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.validation.FormValidator;
import com.realroofers.lovappy.web.util.DatingPlacesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@Controller
@RequestMapping("/registration")
public class RegistrationController {

    private static Logger LOG = LoggerFactory.getLogger(RegistrationController.class);

    private static final String MODEL_ATTRIBUTE_NAME = "info";

    private static final String GOOGLE_KEY = "google_key";

    private static final String GOOGLE_MAP_URL = "https://maps.googleapis.com/maps/api/geocode/json?address";

    private final UserProfileAndPreferenceService service;
    private final CoupleProfileService  coupleProfileService;
    private final UserProfileService userProfileService;

    private final LovstampService lovstampService;

    private final UserService userService;

    private final LanguageService languageService;
    private final DatingPlacesUtil datingPlacesUtil;

    private final FormValidator formValidator;

    private final TwitterTemplate twitterTemplate;

    private ITemplateEngine templateEngine;
    private MailService mailService;

    private UserSurveyService userSurveyService;



    @Value("${google.maps.api.key}")
    private String googleKeyId;


    @Autowired
    public RegistrationController(UserProfileAndPreferenceService service, CoupleProfileService coupleProfileService, UserProfileService userProfileService,
                                  LovstampService lovstampService, UserService userService, LanguageService languageService,
                                  FormValidator formValidator, MailService mailService, List<ViewResolver> viewResolvers,
                                  DatingPlacesUtil datingPlacesUtil, TwitterTemplate twitterTemplate, UserSurveyService userSurveyService) {
        this.service = service;
        this.coupleProfileService = coupleProfileService;
        this.userProfileService = userProfileService;
        this.lovstampService = lovstampService;
        this.userService = userService;
        this.languageService = languageService;
        this.formValidator = formValidator;
        this.datingPlacesUtil = datingPlacesUtil;
        this.twitterTemplate = twitterTemplate;

        this.userSurveyService = userSurveyService;
        this.mailService = mailService;
        for (ViewResolver viewResolver : viewResolvers) {
            if (viewResolver instanceof ThymeleafViewResolver) {
                ThymeleafViewResolver thymeleafViewResolver = (ThymeleafViewResolver) viewResolver;
                this.templateEngine = thymeleafViewResolver.getTemplateEngine();
            }
        }
        if (this.templateEngine == null) {
            throw new BeanInitializationException("No view resolver of type ThymeleafViewResolver found.");
        }
    }


    @GetMapping("/gender")
    public String showGenderPage(Model model) {
        if(UserUtil.INSTANCE.getCurrentUser() == null)
            return "redirect:/login";
        GenderAndPrefGenderDto dto = service.getGenderAndPrefGender(UserUtil.INSTANCE.getCurrentUserId());

        if (dto != null) {
            model.addAttribute(MODEL_ATTRIBUTE_NAME, dto);
            model.addAttribute(GOOGLE_KEY, googleKeyId);
        } else {
            model.addAttribute(MODEL_ATTRIBUTE_NAME, new GenderAndPrefGenderDto());
        }
       model.addAttribute("isReg", true);
//        return userService.getUser(UserUtil.INSTANCE.getCurrentUserId()).getIsInLove() ?
//                "user/registration/in_love/couples_registration" : "user/registration/gender";
        return "user/registration/gender";
    }

    @GetMapping("/email")
    public String showEmailReg(Model model) {
        model.addAttribute("email", "");
        return "user/registration/email-registration";
    }

    @PostMapping("/email/save")
    public String saveEmail(@RequestParam("email") String email, Model model, HttpServletRequest request) {
        UserAuthDto user = UserUtil.INSTANCE.getCurrentUser();
        user.setEmail(email);
        user = userService.updateSocialMissingEmail(user);
        //update Authentiation
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            if (UserAuthDto.class.isAssignableFrom(authentication.getPrincipal().getClass())) {

                UsernamePasswordAuthenticationToken updatedAuth = new UsernamePasswordAuthenticationToken(user, user.getSocialId(),
                        user.getAuthorities());

                updatedAuth.setDetails(user);

                SecurityContextHolder.getContext().setAuthentication(updatedAuth);
                // add authentication to the session
                request.getSession().setAttribute(
                        HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
                        SecurityContextHolder.getContext());
            }
        }
        String targetUrl = null;
        try {
            targetUrl = datingPlacesUtil.addReview(request);
        } catch (IOException e) {

        }
        if(!StringUtils.isEmpty(targetUrl)){
            return "redirect:" + targetUrl;
        }
        return "redirect:/registration/gender";
    }

    @GetMapping("/ageheightsizelang")
    public String showAgeHeightLangAndPrefSizeAgeHeightLangPage(Model model) {
        model.addAttribute("isReg", true);
        AgeHeightLangAndPrefSizeAgeHeightLangDto dto = service
                .getAgeHeightLangAndPrefSizeAgeHeightLang(UserUtil.INSTANCE.getCurrentUser().getUserId());
        if (dto.getPrefGender() == null) {
            return "redirect:/registration/gender?fromageheightsizelang=true";
        } else {
            model.addAttribute(MODEL_ATTRIBUTE_NAME, dto);
            if (Gender.MALE.equals(dto.getPrefGender())) {
                return "user/registration/ageheightsizelang_male";
            } else if (Gender.FEMALE.equals(dto.getPrefGender())) {
                return "user/registration/ageheightsizelang_female";
            } else {
                // Not MALE, Not FEMALE then both
                return "user/registration/ageheightsizelang_both";
            }
        }
    }


    @GetMapping("/personalitylifestyle")
    public String showPersonalityLifstyle(Model model) {
        model.addAttribute("isReg", true);
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        PersonalityLifestyleDto info = userProfileService.getPersonalityLifestyle(userId);
        model.addAttribute("profile", info);

        return "user/registration/personalitylifestyle";
    }


    @GetMapping("/personalitystatus")
    public String showPersonalityStatus(Model model) {
        model.addAttribute("isReg", true);
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        StatusLangDto info = service.getStatusLang(userId);
        model.addAttribute("profile", info);
        model.addAttribute("languages", languageService.getAllLanguages());

        return "user/registration/personalitystatus";
    }


    @GetMapping("/profilepics")
    public String showprofilepics(Model model) {
        model.addAttribute("isReg", true);
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        if(userId == null)
            return "errors/404";
        model.addAttribute("profile", userProfileService.getProfilePicsDto(userId));
        model.addAttribute("lovstamp", lovstampService.getLovstampByUserID(userId));
        model.addAttribute("languages", languageService.getAllLanguages());
        return "user/registration/profilepics";
    }

    @GetMapping("/subscription")
    public String showSubscription(Model model) {
        model.addAttribute("isReg", true);
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        model.addAttribute("subscriptionType", userProfileService.getSubscriptionType(userId));
        return "user/registration/subscription";
    }

    @GetMapping("/survey")
    public String surveyUser(Model model) {

        model.addAttribute("userSurvey", new UserSurveyDto());
        model.addAttribute("questions", userSurveyService.getAllQuestions());

        return "user/registration/survey";
    }

    @PostMapping("/survey/add")
    public String addSurvey(@ModelAttribute("userSurvey") UserSurveyDto userSurveyDto, HttpServletRequest request) {
        userSurveyDto.setUserDto(userService.getUser(UserUtil.INSTANCE.getCurrentUserId()));
        userSurveyService.addUserSurvey(userSurveyDto);
        HttpSession session = request.getSession(true);
        session.setAttribute("surveyStatus", "1");

        return "redirect:/radio";
    }

    @PostMapping("/survey/dontShow")
    @ResponseBody
    public boolean dontShowSurvey(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        session.setAttribute("surveyStatus", "1");

        return userSurveyService.dontShowSurvey();
    }

    @GetMapping("/couples")
    public String registerCouples(Model model) {
        model.addAttribute("isReg", true);
        CouplesProfileDto dto = coupleProfileService.getCoupleProfile(UserUtil.INSTANCE.getCurrentUserId());

        if (dto != null) {
            model.addAttribute(MODEL_ATTRIBUTE_NAME, dto);
            model.addAttribute("isUploaded", dto.getCoupleImage()!=null&&dto.getCoupleImage().getUrl()!=null);
        } else {
            model.addAttribute(MODEL_ATTRIBUTE_NAME, new CouplesProfileDto());
            model.addAttribute("isUploaded", false);
        }

        return "couples/couples_registration";
    }
}
