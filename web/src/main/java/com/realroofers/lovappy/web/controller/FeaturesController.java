package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.dto.VideoWidgetDTO;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Daoud Shaheen on 6/10/2017.
 */
@Controller
@RequestMapping("/features")
public class FeaturesController {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeaturesController.class);

    private CMSService cmsService;

    public FeaturesController(CMSService cmsService) {
        this.cmsService = cmsService;
    }

    @GetMapping
    public String features(Model model) {
        model.addAttribute(PageConstants.FEATURES_RECORD_VOICE_DESCRIPTION, cmsService.getTextByTagAndLanguage(PageConstants.FEATURES_RECORD_VOICE_DESCRIPTION, "EN"));
        model.addAttribute(PageConstants.FEATURES_SEND_SONG_DESCRIPTION, cmsService.getTextByTagAndLanguage(PageConstants.FEATURES_SEND_SONG_DESCRIPTION, "EN"));
        model.addAttribute(PageConstants.FEATURES_SEND_GIFT_DESCRIPTION, cmsService.getTextByTagAndLanguage(PageConstants.FEATURES_SEND_GIFT_DESCRIPTION, "EN"));
        return "features";
    }

    @GetMapping("/record-voice")
    public ModelAndView recordVoice(ModelAndView modelAndView) {

        VideoWidgetDTO video = cmsService.getVideosByKey("record-voice-video");
        if (video != null) {
            modelAndView.addObject("record_voice_video", video.getUrl());
        }
        modelAndView.setViewName("record-voice");
        return modelAndView;
    }

    @GetMapping("/send-song")
    public String sendSong(){
        return "send-song";
    }

    @GetMapping("/send-gift")
    public String sendGift(){
        return "send-gift";
    }
}
