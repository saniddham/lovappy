package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.gift.GiftExchangeService;
import com.realroofers.lovappy.service.privatemessage.PrivateMessageService;
import com.realroofers.lovappy.service.user.UserUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

/**
 * Created by Eias Altawil on 5/13/17
 */

@Controller
@RequestMapping("/notifications")
public class NotificationsController {

    private GiftExchangeService giftExchangeService;
    private PrivateMessageService privateMessageService;

    private static Logger LOG = LoggerFactory.getLogger(NotificationsController.class);

    @Autowired
    public NotificationsController(GiftExchangeService giftExchangeService, PrivateMessageService privateMessageService) {
        this.giftExchangeService = giftExchangeService;
        this.privateMessageService = privateMessageService;
    }

    @GetMapping
    public ModelAndView notificationsPage() {
        ModelAndView mav = new ModelAndView("notifications");

        Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        mav.addObject("SongsReceived", new ArrayList<>());
        mav.addObject("PrivateMessagesReceived", privateMessageService.getReceivedPrivateMessages(userId));
        mav.addObject("GiftsReceived", giftExchangeService.getAllReceivedGifts(userId));
        return mav;
    }
}
