package com.realroofers.lovappy.web.config.social.instagram.api.impl;


import com.fasterxml.jackson.databind.module.SimpleModule;
import com.realroofers.lovappy.web.config.social.instagram.api.*;

public class InstagramModule extends SimpleModule {

    public InstagramModule() {
        super(InstagramModule.class.getName(), new com.fasterxml.jackson.core.Version(1, 0, 1, null, null, null));
    }

    @Override public void setupModule(SetupContext context) {
        context.setMixInAnnotations(InstagramProfile.class, InstagramProfileMixin.class);
    }

}
