package com.realroofers.lovappy.web.rest.v1;

import com.realroofers.lovappy.service.blog.PostService;
import com.realroofers.lovappy.service.blog.VlogsService;
import com.realroofers.lovappy.service.blog.dto.PostDto;
import com.realroofers.lovappy.service.blog.dto.VlogsDto;
import com.realroofers.lovappy.service.blog.model.BlogTypes;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.web.exception.BadRequestException;
import com.realroofers.lovappy.web.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Daoud Shaheen on 9/3/2017.
 */
@RestController
@RequestMapping({"/api/v1/vlogs", "/api/v2/vlogs"})
public class RestVlogsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestVlogsController.class);

    private final VlogsService vlogsService;

    @Autowired
    public RestVlogsController(VlogsService vlogsService) {

        this.vlogsService = vlogsService;
    }

    @RequestMapping(value = "/videos", method = RequestMethod.GET)
    public Page<VlogsDto> getVlogsList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                @RequestParam(value= "limit", defaultValue = "10") Integer limit){

        validatePageAndSize(page, limit);
        return vlogsService.getActiveVlogsList(page, limit);
    }

    @RequestMapping(value = "/videos/{video-id}", method = RequestMethod.GET)
    public VlogsDto getVlogsById(@PathVariable("video-id") Integer videoId){

        VlogsDto vlogsDto = vlogsService.getActiveVlogsById(videoId);
        if(vlogsDto == null) {
          throw new ResourceNotFoundException("Vlog with id " + videoId + " is not found");
        }

        return vlogsDto;
    }

    private void validatePageAndSize(Integer page, Integer limit) {
        if(page < 1) {
            throw new BadRequestException("Page should be more than 1");
        }
        if(limit < 1) {
            throw new BadRequestException("PageSize should be more than 1");
        }
    }
}
