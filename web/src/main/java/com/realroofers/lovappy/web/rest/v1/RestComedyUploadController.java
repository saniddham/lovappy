package com.realroofers.lovappy.web.rest.v1;

import com.realroofers.lovappy.service.product.support.Upload;
import com.realroofers.lovappy.service.upload.PodcastCategoryService;
import com.realroofers.lovappy.service.upload.dto.PodcastCategoryDto;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.FormValidator;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.service.validation.ResponseStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Created by Manoj
 */
@RestController
@RequestMapping({"/api/v1/comedy", "/api/v2/comedy"})
public class RestComedyUploadController {

    private final PodcastCategoryService uploadCategoryService;
    private final FormValidator formValidator;

    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 10;

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    public RestComedyUploadController(PodcastCategoryService uploadCategoryService,
                                      FormValidator formValidator) {
        this.uploadCategoryService = uploadCategoryService;
        this.formValidator = formValidator;
    }

    @GetMapping("/categories")
    public ResponseEntity<Page<PodcastCategoryDto>> getAllCategories(
            @RequestParam(value = "page_size", required = false) Optional<Integer> pageSize,
            @RequestParam(value = "page_number", required = false) Optional<Integer> pageNumber) {

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (pageNumber.orElse(0) < 1) ? INITIAL_PAGE : pageNumber.get() - 1;

        Page<PodcastCategoryDto> categoryDtos = uploadCategoryService.findAll(new PageRequest(evalPage, evalPageSize));

        return ResponseEntity.ok(categoryDtos);
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<PodcastCategoryDto> getCategoryById(@PathVariable("id") Integer id) {

        try {
            PodcastCategoryDto itemDto = uploadCategoryService.findById(id);
            return new ResponseEntity<>(itemDto, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/category/add")
    public ResponseEntity addCategory(@ModelAttribute @Valid PodcastCategoryDto uploadCategoryDto) {

        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = formValidator.validate(uploadCategoryDto, new Class[]{Upload.class});

        if (errorMessages.size() > 0) {
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(200).body(res);
        }
        try {
            uploadCategoryService.create(uploadCategoryDto);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/category/update")
    public ResponseEntity updateCategory(@ModelAttribute @Valid PodcastCategoryDto uploadCategoryDto) {

        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = formValidator.validate(uploadCategoryDto, new Class[]{Upload.class});

        if (errorMessages.size() > 0) {
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(200).body(res);
        }
        try {
            uploadCategoryService.update(uploadCategoryDto);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
