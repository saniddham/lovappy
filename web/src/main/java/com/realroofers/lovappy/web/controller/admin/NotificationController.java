package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.Notification;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.web.util.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.ws.rs.PathParam;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Daoud Shaheen on 6/24/2017.
 */
@Controller()
@RequestMapping("/lox/notifications")
public class NotificationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationController.class);
    @Autowired
    private NotificationService notificationService;

    @GetMapping
    public ModelAndView list(ModelAndView model, @RequestParam(value = "type", defaultValue = "ALL") String type, @RequestParam(value = "page", defaultValue = "1") int page) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
            User user = (User) authentication.getDetails();
            Page<NotificationDto> notificationPage;

            if ("USER_REGISTRATION".equals(type)) {
                notificationPage = notificationService.getPageOfNotificaionByUserAndType(user, NotificationTypes.USER_REGISTRATION, page, 10);
                notificationService.setAllNotificaitonAsSeenByUserAndType(user, NotificationTypes.USER_REGISTRATION);
            } else if ("USER_BLOGS".equals(type)) {
                notificationPage = notificationService.getPageOfNotificaionByUserAndType(user, NotificationTypes.USER_BLOGS, page, 10);
                notificationService.setAllNotificaitonAsSeenByUserAndType(user, NotificationTypes.USER_BLOGS);
            } else if ("OTHER".equals(type)) {
                notificationPage = notificationService.getPageOfNotificaionByUserExceptRegistartionAndBlog(user, page, 10);
                List<NotificationTypes> types = new ArrayList<>();
                for (NotificationTypes notifType : NotificationTypes.values()) {
                    types.add(notifType);

                }
                types.remove(NotificationTypes.USER_BLOGS);
                types.remove(NotificationTypes.USER_REGISTRATION);

                notificationService.setAllNotificaitonAsSeenByUserAndTypes(user, types);
            } else if ("EVENT_APPROVAL".equals(type)) {
                notificationPage = notificationService.getPageOfNotificaionByUserAndType(user, NotificationTypes.EVENT_APPROVAL, page, 10);
                notificationService.setAllNotificaitonAsSeenByUserAndType(user, NotificationTypes.EVENT_APPROVAL);
            } else if ("USER_ADS".equals(type)) {
                notificationPage = notificationService.getPageOfNotificaionByUserAndType(user, NotificationTypes.USER_ADS, page, 10);
                notificationService.setAllNotificaitonAsSeenByUserAndType(user, NotificationTypes.USER_ADS);
            } else if ("PLACE_APPROVAL".equals(type)) {
                notificationPage = notificationService.getPageOfNotificaionByUserAndType(user, NotificationTypes.PLACE_APPROVAL, page, 10);
                notificationService.setAllNotificaitonAsSeenByUserAndType(user, NotificationTypes.PLACE_APPROVAL);
            } else if ("REVIEW_APPROVAL".equals(type)) {
                notificationPage = notificationService.getPageOfNotificaionByUserAndType(user, NotificationTypes.REVIEW_APPROVAL, page, 10);
                notificationService.setAllNotificaitonAsSeenByUserAndType(user, NotificationTypes.REVIEW_APPROVAL);
            } else if ("MUSIC_ORDER".equals(type)) {
            notificationPage = notificationService.getPageOfNotificaionByUserAndType(user, NotificationTypes.MUSIC_ORDER, page, 10);
            notificationService.setAllNotificaitonAsSeenByUserAndType(user, NotificationTypes.MUSIC_ORDER);
        }
            else {
                notificationPage = notificationService.getPageOfNotificaionByUser(user, page, 10);
                notificationService.setAllNotificaitonAsSeenByUser(user);
            }
            model.addObject("notifications", notificationPage);

            Pager pager = new Pager(notificationPage.getTotalPages(), notificationPage.getNumber(), Pager.BUTTONS_TO_SHOW);

            model.addObject("selectedPageSize", 10);
            model.addObject("pageSizes", Pager.PAGE_SIZES);
            model.addObject("pager", pager);
            model.addObject("type", type);

        }
        model.setViewName("admin/notification/notification");
        return model;
    }

    @GetMapping("/{notification-id}")
    public ModelAndView showNotificaiton(ModelAndView modelAndView, @PathVariable("notification-id") Long notificationId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
            User user = (User) authentication.getDetails();
            notificationService.setNotificaitonAsSeenByUser(notificationId, user);
        }
        NotificationDto notificationDto = notificationService.getNotificationById(notificationId);
        if (notificationDto.getType().equals(NotificationTypes.USER_BLOGS)) {
            modelAndView.setViewName("redirect:/lox/blog/post/" + notificationDto.getSourceId());
        } else if (notificationDto.getType().equals(NotificationTypes.USER_FLAGS)) {
            modelAndView.setViewName("redirect:/lox/users/complaints");
        } else if (notificationDto.getType().equals(NotificationTypes.USER_REGISTRATION)) {
            modelAndView.setViewName("redirect:/lox/users/" + notificationDto.getUserId());
        } else if (notificationDto.getType().equals(NotificationTypes.AMBASSADOR_REGISTRATION)) {
            modelAndView.setViewName("redirect:/lox/users/" + notificationDto.getUserId());
        } else if (notificationDto.getType().equals(NotificationTypes.EVENT_APPROVAL)) {
            modelAndView.setViewName("redirect:/lox/events/");
        } else if (notificationDto.getType().equals(NotificationTypes.PLACE_APPROVAL)) {
            modelAndView.setViewName("redirect:/lox/dating/places/");
        } else if (notificationDto.getType().equals(NotificationTypes.REVIEW_APPROVAL)) {
            modelAndView.setViewName("redirect:/lox/dating/places/reviews");
        }  else if (notificationDto.getType().equals(NotificationTypes.MUSIC_ORDER)) {
        modelAndView.setViewName("redirect:/lox/music/"+ notificationDto.getSourceId());
    }

        return modelAndView;
    }

}
