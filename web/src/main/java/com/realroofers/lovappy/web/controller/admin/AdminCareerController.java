package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.career.VacancyService;
import com.realroofers.lovappy.service.career.dto.VacancyDto;
import com.realroofers.lovappy.service.core.VacancyStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by daoud on 8/22/2018.
 */
@Controller
@RequestMapping("/lox/careers")
public class AdminCareerController {

    private static Logger LOG = LoggerFactory.getLogger(AdminCareerController.class);

    @Autowired
    private VacancyService vacancyService;

    @GetMapping("/{page}")
    public ModelAndView getOpenVacancies(@PathVariable int page) {
        LOG.info("Getting all the open vacancies for admin");
        Page<VacancyDto> vacancies =  vacancyService.findByStatus(VacancyStatus.OPEN.toString(), new PageRequest(page, 5));
        ModelAndView model = new ModelAndView("admin/career/careers");
        model.addObject("vacancies", vacancies);
        model.addObject("vacancy", new VacancyDto());
        model.addObject("page", page);
        model.addObject("searchKey", "");

        return model;
    }

    @GetMapping("/search")
    public ModelAndView Search(@RequestParam(value = "searchKey") String searchKey) {
        LOG.info("Getting all the search open vacancies");
        Page<VacancyDto> vacancies =  vacancyService.search(searchKey, new PageRequest(0, 100));
        ModelAndView model = new ModelAndView("admin/career/careers");
        model.addObject("vacancies", vacancies);
        model.addObject("vacancy", new VacancyDto());
        model.addObject("page", 1);
        model.addObject("searchKey", "");
        return model;
    }

    @PostMapping("/addVacancy")
    public String addVacancy(@ModelAttribute VacancyDto vacancy) {
        LOG.info("Inserting a new opportunity");
        String status = vacancyService.insertVacancy(vacancy);

        return "redirect:/lox/careers/0";
    }

    @PostMapping("/saveVacancy")
    public String editVacancy(@ModelAttribute VacancyDto vacancy) {
        LOG.info("Saving the vacancy");
        String status = vacancyService.saveVacancy(vacancy);
        return "redirect:/lox/careers/0";
    }

    @PostMapping("/deleteVacancy")
    public String deleteVacancy(@RequestParam(value = "vacancyId") long vacancyId) {
        LOG.info("Closing the vacancy ", vacancyId);
        String status = vacancyService.closeVacancy(vacancyId, VacancyStatus.CLOSED.toString());
        return "redirect:/lox/careers/0";
    }

}