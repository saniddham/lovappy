package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Daoud Shaheen on 6/10/2017.
 */
@Controller
@RequestMapping("/mobile")
public class MobileController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MobileController.class);

    private CMSService cmsService;

    public MobileController(CMSService cmsService) {
        this.cmsService = cmsService;
    }

    @GetMapping
    public String contactUs(Model model) {
        model.addAttribute(PageConstants.MOBILE_HEADER_TEXT, cmsService.getTextByTagAndLanguage(PageConstants.MOBILE_HEADER_TEXT, "EN"));
        model.addAttribute(PageConstants.MOBILE_DESCRIPTION_TEXT, cmsService.getTextByTagAndLanguage(PageConstants.MOBILE_DESCRIPTION_TEXT, "EN"));
        model.addAttribute(PageConstants.LEFT_MOBILE_PAGE_IMAGE, cmsService.getImageUrlByTag(PageConstants.LEFT_MOBILE_PAGE_IMAGE));
        return "mobile";
    }
}
