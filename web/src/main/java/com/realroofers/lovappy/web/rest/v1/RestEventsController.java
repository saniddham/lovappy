package com.realroofers.lovappy.web.rest.v1;

import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.core.SocialType;
import com.realroofers.lovappy.service.event.EventService;
import com.realroofers.lovappy.service.event.dto.EventDto;
import com.realroofers.lovappy.service.event.dto.EventLocationDto;
import com.realroofers.lovappy.service.event.dto.EventLocationPictureDto;
import com.realroofers.lovappy.service.event.dto.EventSearchDto;
import com.realroofers.lovappy.service.event.dto.mobile.AddEventMobileDto;
import com.realroofers.lovappy.service.event.dto.mobile.EventMobileDto;
import com.realroofers.lovappy.service.event.support.EventDateUtil;
import com.realroofers.lovappy.service.event.support.EventStatus;
import com.realroofers.lovappy.service.exception.EntityValidationException;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.RegisterUserDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.service.validation.ResponseStatus;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import com.realroofers.lovappy.web.util.PaymentUtil;
import com.squareup.connect.ApiException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.*;

/**
 * @author Eias Altawil
 */

@RestController
@RequestMapping({"/api/v1/events", "/api/v2/events"})
public class RestEventsController {

    private static final Logger LOG = LoggerFactory.getLogger(RestEventsController.class);
    private final CloudStorage cloudStorage;
    private final CloudStorageService cloudStorageService;
    private final ITemplateEngine templateEngine;
    private final EmailTemplateService emailTemplateService;
    private final MailService mailService;
    private final PaymentUtil paymentUtil;
    private final OrderService orderService;
    private EventService eventService;
    private final UserService userService;
    private NotificationService notificationService;
    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @Autowired
    public RestEventsController(CloudStorage cloudStorage, CloudStorageService cloudStorageService,
                                EventService eventService, UserService userService, NotificationService notificationService,
                                ITemplateEngine templateEngine, EmailTemplateService emailTemplateService,
                                MailService mailService, PaymentUtil paymentUtil, OrderService orderService) {

        this.cloudStorage = cloudStorage;
        this.cloudStorageService = cloudStorageService;
        this.eventService = eventService;
        this.userService = userService;
        this.notificationService = notificationService;
        this.templateEngine = templateEngine;
        this.emailTemplateService = emailTemplateService;
        this.mailService = mailService;
        this.paymentUtil = paymentUtil;
        this.orderService = orderService;
    }

    @RequestMapping(value = "/show/nearby", method = RequestMethod.POST)
    public List<EventMobileDto> showNearByEvents(@RequestBody @Valid EventSearchDto eventSearchDto) {

        return eventService.getNearByEvents(eventSearchDto);
    }

    @RequestMapping(value = "/show/nearby/page", method = RequestMethod.POST)
    public Page<EventDto> showNearByEvents(@RequestBody @Valid EventSearchDto eventSearchDto, Pageable pageable) {
        return eventService.getNearByEventsPage(eventSearchDto, pageable);
    }

    @RequestMapping(value = "/show/nearby/count", method = RequestMethod.POST)
    public Integer showNearByEventsCount(@RequestBody @Valid EventSearchDto eventSearchDto) {
        return eventService.getNearByEventsCount(eventSearchDto);
    }

    @RequestMapping(value = "/create/location", method = RequestMethod.POST)
    public ResponseEntity<Void> addTitleAndLocation(@RequestParam("eventTitle") String eventTitle,
                                                    @RequestParam("eventLocation.longitude") Double longitude,
                                                    @RequestParam("eventLocation.floorRoom") String floorRoom,
                                                    @RequestParam("eventLocation.latitude") Double latitude) {
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (latitude == null || longitude == null)
            errorMessages.add(new ErrorMessage("locationValidation", "Select the event location"));

        if (StringUtils.isEmpty(eventTitle))
            errorMessages.add(new ErrorMessage("eventTitle", "Provide Title for the Event"));

        if (errorMessages.size() > 0)
            throw new EntityValidationException(errorMessages);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/create/upload_location-pictures", method = RequestMethod.POST)
    public ResponseEntity uploadCoverImage(@RequestParam("event_location_picture") MultipartFile picture) {
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (picture.isEmpty())
            errorMessages.add(new ErrorMessage("cover", "Add a location picture."));

        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        try {
            CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadFile(picture, imagesBucket, FileExtension.png.toString(), "image/png");
            CloudStorageFileDto added = cloudStorageService.add(cloudStorageFileDto);

            return ResponseEntity.ok(added);

        } catch (IOException e) {
            LOG.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @RequestMapping(value = "/create/others", method = RequestMethod.POST)
    public ResponseEntity<Void> addDescAndCategories(@RequestParam("eventDescription") String eventDescription,
                                                     @RequestParam("eventCategories") String eventCategories,
                                                     @RequestParam("locationPictures[0].picture.id") Double locationPicture
    ) {

        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (StringUtils.isEmpty(eventDescription)) {
            errorMessages.add(new ErrorMessage("eventDescription", "Provide Description for Event"));
        }
        if (StringUtils.isEmpty(eventCategories)) {
            errorMessages.add(new ErrorMessage("eventCategories", "Please Select at least one Category"));
        }
        if (locationPicture == null) {
            errorMessages.add(new ErrorMessage("locationPicture", "Please upload location picture"));
        }

        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/create/date", method = RequestMethod.POST)
    public ResponseEntity<Void> addDate(
            @RequestParam("eventTitle") String eventTitle,
            @RequestParam("eventLocation.longitude") Double longitude,
            @RequestParam("eventLocation.latitude") Double latitude,
            @RequestParam("eventLocation.zipCode") String zipCode,
            @RequestParam("eventLocation.floorRoom") String floorRoom,
            @RequestParam("eventLocation.country") String country,
            @RequestParam("eventLocation.state") String state,
            @RequestParam("eventLocation.province") String province,
            @RequestParam("eventLocation.city") String city,
            @RequestParam("eventLocation.streetNumber") String streetNumber,
            @RequestParam("eventLocation.stateShort") String stateShort,
            @RequestParam("eventLocation.fullAddress") String fullAddress,
            @RequestParam("eventLocation.premise") String premise,
            @RequestParam("eventLocation.route") String route,
            @RequestParam("eventDescription") String eventDescription,
            @RequestParam("eventCategories") String eventCategories,
            @RequestParam("admissionCost") Double admissionCost,
            @RequestParam("eventDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date eventDate,
            @RequestParam("timeZone") String timeZone,
            @RequestParam("finishTime") String finishTime,
            @RequestParam("startTime") String startTime,
            @RequestParam("outsiderCount") Integer outsiderCount,
            @RequestParam("locationPictures[0].picture.id") Long pictureId,
            @RequestParam(value = "targetedAudience.maleOrFemale", required = false) String gender,
            @RequestParam(value = "targetedAudience.ageRange", required = false) String ageRange,
            @RequestParam(value = "targetedAudience.languageSpoken", required = false) String languageSpoken,
            @RequestParam(value = "targetedAudience.catORdog", required = false) String catORdog,
            @RequestParam(value = "targetedAudience.drinker", required = false) String drinker,
            @RequestParam(value = "targetedAudience.smoker", required = false) String smoker,
            @RequestParam(value = "targetedAudience.user", required = false) String user,
            @RequestParam(value = "targetedAudience.no-habit", required = false) String nohabit,
            @RequestParam(value = "targetedAudience.ruralORurbanORsuburban", required = false) String ruralORurbanORsuburban,
            @RequestParam(value = "targetedAudience.politicalopinion", required = false) String politicalopinion,
            @RequestParam(value = "targetedAudience.outdoorsyORindoorsy", required = false) String outdoorsyORindoorsy,
            @RequestParam(value = "targetedAudience.frugalORbigspender", required = false) String frugalORbigspender,
            @RequestParam(value = "targetedAudience.morningpersonORnightowl", required = false) String morningpersonORnightowl,
            @RequestParam(value = "targetedAudience.extrovertORintrovert", required = false) String extrovertORintrovert) {

        List<ErrorMessage> errorMessages = new ArrayList<>();
        Double defaultCost = 10.0;

        if (eventDate == null || eventDate.compareTo(EventDateUtil.getDateOnly()) < 0) {
            errorMessages.add(new ErrorMessage("eventDate", "Select a valid date for the Event"));
        }
        if (StringUtils.isEmpty(startTime)) {
            errorMessages.add(new ErrorMessage("startTime", "Select start time"));
        }
        if (StringUtils.isEmpty(finishTime)) {
            errorMessages.add(new ErrorMessage("finishTime", "Select end time"));
        }
        if (!StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(finishTime)) {
            if (validateTimes(startTime, finishTime))
                errorMessages.add(new ErrorMessage("validTime", "Select start time before end time"));
        }
        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        if (admissionCost == null) {
            admissionCost = defaultCost;
        } else {

            admissionCost = defaultCost + admissionCost;
        }

        if (outsiderCount == null)
            outsiderCount = 0;

        Map<String, String> targetedAudience = new HashMap<>();
        if (gender != null) targetedAudience.put("maleOrFemale", gender);
        if (ageRange != null) targetedAudience.put("ageRange", ageRange);
        if (languageSpoken != null && !languageSpoken.equalsIgnoreCase("Select"))
            targetedAudience.put("languageSpoken", languageSpoken);
        if (catORdog != null) targetedAudience.put("catORdog", catORdog);
        if (drinker != null) targetedAudience.put("drinker", drinker);
        if (smoker != null) targetedAudience.put("smoker", smoker);
        if (user != null) targetedAudience.put("user", user);
        if (nohabit != null) targetedAudience.put("nohabit", nohabit);
        if (ruralORurbanORsuburban != null) targetedAudience.put("ruralORurbanORsuburban", ruralORurbanORsuburban);
        if (politicalopinion != null) targetedAudience.put("politicalopinion", politicalopinion);
        if (outdoorsyORindoorsy != null) targetedAudience.put("outdoorsyORindoorsy", outdoorsyORindoorsy);
        if (frugalORbigspender != null) targetedAudience.put("frugalORbigspender", frugalORbigspender);
        if (morningpersonORnightowl != null) targetedAudience.put("morningpersonORnightowl", morningpersonORnightowl);
        if (extrovertORintrovert != null) targetedAudience.put("extrovertORintrovert", extrovertORintrovert);


        EventLocationDto eventLocationDto = new EventLocationDto(zipCode, country, state, stateShort, province, city, streetNumber, fullAddress, latitude, longitude, premise, route, floorRoom);
        EventDto eventDto = new EventDto(eventTitle, eventDescription, eventDate, startTime, finishTime, outsiderCount, admissionCost, eventCategories);
        eventDto.setTimeZone(timeZone);
        Integer userId = null;

        if (UserUtil.INSTANCE.getCurrentUserId() != null) {
            userId = UserUtil.INSTANCE.getCurrentUserId();
        } else {
            User userNew = null;
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
                userNew = (User) authentication.getDetails();
            }
            userId = userNew.getUserId();
        }

        eventDto.setAmbassadorUserId(userId);
        eventDto.setEventLocation(eventLocationDto);
        eventDto.setLocationPictures(getPictureList(pictureId));
        eventDto.setTargetedAudience(targetedAudience);
        EventDto savedDto = eventService.addEvent(eventDto);

        NotificationDto notificationDto = new NotificationDto();
        UserDto ambassadorDto = userService.getUser(userId);
        notificationDto.setContent("A new event " + savedDto.getEventTitle() + " has been created by " + ambassadorDto.getFirstName());
        notificationDto.setType(NotificationTypes.EVENT_APPROVAL);
        notificationDto.setUserId(userId);
        notificationService.sendNotificationToAdmins(notificationDto);

        LOG.debug(savedDto.getEventId() != null ? "Event Saved successfully with id " + savedDto.getEventId() : "Error in creating event");
        if (savedDto.getEventId() != null) {
            return ResponseEntity.ok().build();
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/user/attend", method = RequestMethod.POST)
    public ResponseEntity<?> registerUserForEvent(@RequestParam("event_id") Integer eventId,
                                                  @RequestParam("nonce") String nonce,
                                                  @RequestParam("coupon") String coupon) {

        OrderDto order = eventService.registerUserForEvent(eventId, UserUtil.INSTANCE.getCurrentUserId(), coupon, PaymentMethodType.SQUARE);
        Boolean executed = null;
        try {
            executed = paymentUtil.executeTransaction(nonce, order, null);
        } catch (ApiException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
        if (executed) {
            Integer noOfAttendees = eventService.getNoOfUserAttendees(eventId);
            return ResponseEntity.ok(noOfAttendees);
        } else {
            orderService.revertOrder(order.getId());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @RequestMapping(value = "/location-pictures/delete/{id}", method = RequestMethod.POST)
    public ResponseEntity<HttpStatus> deleteLocationImage(@PathVariable("id") Integer pictureId) {
        Boolean isDeleted = eventService.deleteLocationImage(pictureId);

        if (isDeleted)
            return new ResponseEntity<>(HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @RequestMapping(value = "/calculate_price", method = RequestMethod.GET)
    public ResponseEntity<CalculatePriceDto> calculatePrice(@RequestParam("event_id") Integer eventID,
                                                            @RequestParam("coupon") String coupon) {
        return ResponseEntity.ok(eventService.calculatePrice(eventID, coupon));
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseEntity updateEvent(@ModelAttribute("event") EventDto eventDto) {
        JsonResponse response = new JsonResponse();

        List<ErrorMessage> errorMessages = new ArrayList<>();
        if (errorMessages.size() > 0) {
            response.setErrorMessageList(errorMessages);
            response.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } else {
            eventDto.setApproved(false);
            eventDto.setEventAprrovalStatus(ApprovalStatus.PENDING);
            eventDto.setEventStatus(EventStatus.INACTIVE);
            EventDto updatedEventDto = eventService.updateEvent(eventDto);

            Integer userId = UserUtil.INSTANCE.getCurrentUserId();
            RegisterUserDto ambassadorDto = userService.getAmbassador(userId);
            NotificationDto notificationDto = new NotificationDto();
            notificationDto.setContent("Event " + updatedEventDto.getEventTitle() + " has been updated by " + ambassadorDto.getName());
            notificationDto.setType(NotificationTypes.EVENT_APPROVAL);
            notificationDto.setUserId(userId);
            notificationService.sendNotificationToAdmins(notificationDto);
            response.setStatus(ResponseStatus.SUCCESS);
            return ResponseEntity.ok().body(response);
        }
    }

    @RequestMapping(value = "/cancel/{eventId}", method = RequestMethod.POST)
    public ResponseEntity cancelEvent(HttpServletRequest request, @PathVariable("eventId") Integer eventId, @RequestParam(value = "lang", defaultValue = "EN") String language) {
        try {
            List<Integer> attendeesList = new ArrayList<>();
            List<User> userList = new ArrayList<>();
            EventDto eventDto = eventService.findEventById(eventId);
            String eventTitle = eventDto.getEventTitle();
            if (eventDto == null) {
                LOG.info("Event id " + eventId + " not found");
            } else {

                attendeesList = eventService.cancelEvent(eventId);
            }
            if (attendeesList != null && !attendeesList.isEmpty()) {
                attendeesList.forEach(attendees -> {
                    userList.add(userService.getUserById(attendees));
                });
                userList.forEach(user -> {

                    EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.EVENT_CANCELLATION, language);
                    emailTemplate.getAdditionalInformation().put("attendeeName", user.getFirstName());
                    emailTemplate.getAdditionalInformation().put("eventTitle", eventDto.getEventTitle());
                    new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), user.getEmail(), templateEngine, mailService, emailTemplate).send();
                });
            }

        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);


    }

    @RequestMapping(value = "/upload/upload-event-pictures/{eventId}", method = RequestMethod.POST)
    public ResponseEntity<Integer> uploadEventPictures(@RequestParam("files") MultipartFile[] pictures,
                                                       @PathVariable("eventId") Integer eventId) {

        List<ErrorMessage> errorMessages = new ArrayList<>();
        if (pictures.length == 0)
            errorMessages.add(new ErrorMessage("uploadPictures", "Add atleast one event picture."));

        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        List<CloudStorageFileDto> list = new ArrayList<>();
        try {
            for (MultipartFile picture : pictures) {
                CloudStorageFileDto cloudStorageFileDto =
                        cloudStorage.uploadFile(picture, imagesBucket, FileExtension.jpg.toString(), "image/jpeg");
                CloudStorageFileDto added = cloudStorageService.add(cloudStorageFileDto);
                list.add(added);
            }
            Boolean aBoolean = eventService.addEventLocationPictures(UserUtil.INSTANCE.getCurrentUserId(), eventId, list);
            if (aBoolean) {
                return ResponseEntity.ok(list.size());
            } else return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @RequestMapping(value = "/update/upload_location-pictures/{eventId}", method = RequestMethod.POST)
    public ResponseEntity updateCoverImage(@RequestParam("event_location_picture") MultipartFile picture, @PathVariable("eventId") Integer eventId) {

        List<ErrorMessage> errorMessages = new ArrayList<>();
        EventDto eventDto = eventService.findEventById(eventId);
        if (picture.isEmpty())
            errorMessages.add(new ErrorMessage("cover", "Add a location picture."));

        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        try {
            CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadFile(picture, imagesBucket, FileExtension.png.toString(), "image/png");
            CloudStorageFileDto added = cloudStorageService.add(cloudStorageFileDto);
            EventLocationPictureDto eventLocationPictureDto = eventService.updateEventLocation(eventId, added);
            if (eventLocationPictureDto != null) {
                Integer userId = UserUtil.INSTANCE.getCurrentUserId();
                RegisterUserDto ambassadorDto = userService.getAmbassador(userId);
                NotificationDto notificationDto = new NotificationDto();
                notificationDto.setContent("Event " + eventDto.getEventTitle() + " has been updated by " + ambassadorDto.getName());
                notificationDto.setType(NotificationTypes.EVENT_APPROVAL);
                notificationDto.setUserId(userId);
                notificationService.sendNotificationToAdmins(notificationDto);
            }
            return ResponseEntity.ok(added);
        } catch (IOException e) {
            LOG.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    private List<EventLocationPictureDto> getPictureList(Long pictureId) {

        CloudStorageFileDto pic = new CloudStorageFileDto();
        pic.setId(pictureId);
        EventLocationPictureDto picDto = new EventLocationPictureDto();
        picDto.setPicture(pic);
        List<EventLocationPictureDto> list = new ArrayList<>();
        list.add(picDto);
        return list;
    }


    @PostMapping("/{event-id}/share/{social-provider}")
    public void shareEvent(@PathVariable("event-id") Integer postId,
                           @PathVariable("social-provider") String socialProvider) {
        eventService.shareBySocialProvider(postId, SocialType.fromString(socialProvider));
    }

    private boolean validateTimes(String startTime, String finishTime) {

        return EventDateUtil.convertToDate(startTime).after(EventDateUtil.convertToDate(finishTime));
    }

    /**
     * This api call returns a paginated list of Future active events objects
     *
     * @param page - parameter accepts the page number required by the user
     * @return paginated list of Events Object
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Page<EventMobileDto>> getMobileEventList(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        try {
            Page<EventMobileDto> mobileDtos = eventService.findMobileAllActiveEventsPaginated(new PageRequest(page - 1, 10), UserUtil.INSTANCE.getCurrentUserId());
            return new ResponseEntity<>(mobileDtos, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This api returns a paginated list of Active events in date descending order
     *
     * @param page page number
     * @return paginated list of Events
     */
    @RequestMapping(value = "/date", method = RequestMethod.GET)
    public ResponseEntity<Page<EventMobileDto>> getMobileEventListByDate(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        try {
            Page<EventMobileDto> mobileDtos = eventService.findMobileAllActiveEventsByDate(new PageRequest(page - 1, 10), UserUtil.INSTANCE.getCurrentUserId());
            return new ResponseEntity<>(mobileDtos, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This api returns a paginated list of Active events in distance descending order
     *
     * @param page page number
     * @return paginated list of Events
     */
    @RequestMapping(value = "/distance", method = RequestMethod.POST)
    public ResponseEntity<Page<EventMobileDto>> getMobileEventListByDistance(@RequestParam(value = "page", defaultValue = "1") Integer page, @RequestBody @Valid EventSearchDto eventSearchDto) {
        try {
            Page<EventMobileDto> mobileDtos = eventService.getNearByEventsMobile(eventSearchDto,
                    UserUtil.INSTANCE.getCurrentUserId(), new PageRequest(page - 1, 10));
            return new ResponseEntity<>(mobileDtos, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This api returns a paginated list of Active events depending on the search term
     *
     * @param page  parameter accepts the page number of the results
     * @param query parameter accepts the search term that should be matched
     * @return a paginated list of active Events
     */
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public ResponseEntity<Page<EventMobileDto>> getMobileEventListByQuery(@RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam("query") String query) {
        try {
            Page<EventMobileDto> mobileDtos = eventService.findAllMobileEventByQuery(new PageRequest(page - 1, 10), query, UserUtil.INSTANCE.getCurrentUserId());
            return new ResponseEntity<>(mobileDtos, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/me/recommended", method = RequestMethod.GET)
    public ResponseEntity<Page<EventMobileDto>> getUserSuitableEventPaginatedList(@RequestParam(value = "page", defaultValue = "1") Integer page) {

        Page<EventMobileDto> mobileDtos = eventService.findMobileAllActiveEventsPaginated(new PageRequest(page - 1, 10), UserUtil.INSTANCE.getCurrentUserId());
        return new ResponseEntity<>(mobileDtos, HttpStatus.OK);
    }

    /**
     * This api returns a paginated list of Events the user has paid
     *
     * @param page   parameter accepts the page number requested by the user
     * @param date   parameter accepts the date passed by the user
     * @param isPast parameter accepts a boolean which decides whether to return past Events or Future Events
     * @return paginated list of Events
     */
    @RequestMapping(value = "/me", method = RequestMethod.GET)
    public ResponseEntity<Page<EventMobileDto>> getMobileEventPaginatedListByUserId(@RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam("eventDate") String date, @RequestParam("past") Boolean isPast) {
        try {
            Page<EventMobileDto> mobileDtos;
            if (isPast) {
                mobileDtos = eventService.findPastEventsByUserId(date, UserUtil.INSTANCE.getCurrentUserId(), new PageRequest(page - 1, 10));
            } else {
                mobileDtos = eventService.findFutureEventsByUserId(date, UserUtil.INSTANCE.getCurrentUserId(), new PageRequest(page - 1, 10));
            }
            return new ResponseEntity<>(mobileDtos, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This api returns a list of Events the user has paid
     *
     * @param date   parameter accepts the date passed by the user
     * @param isPast parameter accepts a boolean which decides whether to return past Events or Future Events
     * @return paginated list of Events
     */
    @RequestMapping(value = "/me/list", method = RequestMethod.GET)
    public ResponseEntity<List<EventMobileDto>> getMobileEventListByUserId(@RequestParam("eventDate") String date, @RequestParam("past") Boolean isPast) {
        try {
            List<EventMobileDto> mobileDtos;
            if (isPast) {
                mobileDtos = eventService.findPastEventsListByUserId(date, UserUtil.INSTANCE.getCurrentUserId());
            } else {
                mobileDtos = eventService.findPastEventsListByUserId(date, UserUtil.INSTANCE.getCurrentUserId());
            }
            return new ResponseEntity<>(mobileDtos, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This api call returns a paginated list of Event which are not active or old
     *
     * @param page parameter accepts the page number requested by the user
     * @return paginated list of Old Events
     */
    @RequestMapping(value = "/previous", method = RequestMethod.GET)
    public ResponseEntity<Page<EventMobileDto>> getMobilePreviousEventsList(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        try {
            Page<EventMobileDto> mobileDtos = eventService.findMobilePreviousEvents(new PageRequest(page - 1, 10));
            return new ResponseEntity<>(mobileDtos, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This api call returns the list of Events which are active
     *
     * @return list of Events
     */
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<EventMobileDto>> getAllEventListMobile() {
        List<EventMobileDto> eventDtoList = eventService.findMobileAllActiveEvents();

        return new ResponseEntity<>(eventDtoList, HttpStatus.OK);
    }

    /**
     * This api call returns Event object by the event id
     *
     * @param id - parameter accepts the ID of an Event
     * @return Event Object depending on the id
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<EventMobileDto> getEventByIdMobile(@PathVariable("id") Integer id) {
        try {
            Integer userId = UserUtil.INSTANCE.getCurrentUserId();
            EventMobileDto eventDto = eventService.findMobileEventById(id, userId);
            return new ResponseEntity<>(eventDto, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    /**
     * This api is used to add an Event Entity
     *
     * @param event parameter accepts an Event Object
     * @return Event object which was saved
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addEvent(@RequestBody AddEventMobileDto event) {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        List<ErrorMessage> errorMessages = new ArrayList<>();
        Double defaultCost = 10.0;

        if (!eventService.isAmbassador(userId)) {
            errorMessages.add(new ErrorMessage("role", "you should be an ambassador to create an event"));
        }
        if (event.getEventDate() == null || event.getEventDate().compareTo(EventDateUtil.getDateOnly()) < 0) {
            errorMessages.add(new ErrorMessage("eventDate", "Select a valid date for the Event"));
        }
        if (StringUtils.isEmpty(event.getStartTime())) {
            errorMessages.add(new ErrorMessage("startTime", "Select start time"));
        }
        if (StringUtils.isEmpty(event.getFinishTime())) {
            errorMessages.add(new ErrorMessage("finishTime", "Select end time"));
        }
        if (!StringUtils.isEmpty(event.getStartTime()) && !StringUtils.isEmpty(event.getFinishTime())) {
            if (validateTimes(event.getStartTime(), event.getFinishTime()))
                errorMessages.add(new ErrorMessage("validTime", "Select start time before end time"));
        }
        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        if (event.getAdmissionCost() == null) {
            event.setAdmissionCost(defaultCost);
        } else {
            event.setAdmissionCost(defaultCost + event.getAdmissionCost());
        }
        try {
            event.setAmbassadorUserId(userId);
            EventMobileDto eventMobileDto = eventService.addMobileEvent(event);
            NotificationDto notificationDto = new NotificationDto();
            UserDto ambassadorDto = userService.getUser(userId);
            notificationDto.setContent("A new event " + eventMobileDto.getEventTitle() + " has been created by " + ambassadorDto.getFirstName());
            notificationDto.setType(NotificationTypes.EVENT_APPROVAL);
            notificationDto.setUserId(userId);
            notificationService.sendNotificationToAdmins(notificationDto);
            return new ResponseEntity<EventMobileDto>(eventMobileDto, HttpStatus.OK);
        } catch (Exception e) {
            errorMessages.add(new ErrorMessage("Error", e.getMessage()));
            return new ResponseEntity<>(errorMessages, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity updateEvent(@RequestBody AddEventMobileDto event) {
        List<ErrorMessage> errorMessages = new ArrayList<>();
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        Double defaultCost = 10.0;
        if (event.getEventId() == null) {
            errorMessages.add(new ErrorMessage("eventId", "Event id not found"));
        }
        if (event.getEventDate() == null || event.getEventDate().compareTo(EventDateUtil.getDateOnly()) < 0) {
            errorMessages.add(new ErrorMessage("eventDate", "Select a valid date for the Event"));
        }
        if (StringUtils.isEmpty(event.getStartTime())) {
            errorMessages.add(new ErrorMessage("startTime", "Select start time"));
        }
        if (StringUtils.isEmpty(event.getFinishTime())) {
            errorMessages.add(new ErrorMessage("finishTime", "Select end time"));
        }
        if (!StringUtils.isEmpty(event.getStartTime()) && !StringUtils.isEmpty(event.getFinishTime())) {
            if (validateTimes(event.getStartTime(), event.getFinishTime()))
                errorMessages.add(new ErrorMessage("validTime", "Select start time before end time"));
        }
        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        if (event.getAdmissionCost() == null) {
            event.setAdmissionCost(defaultCost);
        } else {
            event.setAdmissionCost(defaultCost + event.getAdmissionCost());
        }
        try {
            event.setAmbassadorUserId(userId);
            EventMobileDto eventMobileDto = eventService.addMobileEvent(event);
            NotificationDto notificationDto = new NotificationDto();
            UserDto ambassadorDto = userService.getUser(userId);
            notificationDto.setContent("Event " + eventMobileDto.getEventTitle() + " has been edited by " + ambassadorDto.getFirstName());
            notificationDto.setType(NotificationTypes.EVENT_APPROVAL);
            notificationDto.setUserId(userId);
            notificationService.sendNotificationToAdmins(notificationDto);
            return new ResponseEntity<EventMobileDto>(eventMobileDto, HttpStatus.OK);
        } catch (Exception e) {
            errorMessages.add(new ErrorMessage("Error", e.getMessage()));
            return new ResponseEntity<>(errorMessages, HttpStatus.BAD_REQUEST);
        }
    }

}
