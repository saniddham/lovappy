package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.couples.CoupleProfileService;
import com.realroofers.lovappy.service.couples.dto.CouplesProfileDto;
import com.realroofers.lovappy.service.credits.CreditService;
import com.realroofers.lovappy.service.credits.dto.CreditDto;
import com.realroofers.lovappy.service.likes.LikeService;
import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampDto;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampFilterDto;
import com.realroofers.lovappy.service.music.MusicService;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.order.PriceService;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.privatemessage.PrivateMessageService;
import com.realroofers.lovappy.service.questions.DailyQuestionService;
import com.realroofers.lovappy.service.questions.dto.DailyQuestionDto;
import com.realroofers.lovappy.service.questions.dto.UserResponseDto;
import com.realroofers.lovappy.service.system.LanguageService;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.user.*;
import com.realroofers.lovappy.service.user.dto.*;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.ProfilePictureAccess;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.service.validation.ResponseStatus;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

/**
 * @author mwiyono
 * @author Eias Altawil
 */
@Controller
@RequestMapping("/member")
public class MemberProfileController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemberProfileController.class);

    private final UserService userService;
    private final MusicService musicService;
    private final UserProfileService userProfileService;
    private final UserPreferenceService userPreferenceService;
    private final UserProfileAndPreferenceService userProfileAndPreferenceService;
    private final LikeService likeService;
    private final LovstampService lovstampService;
    private final PrivateMessageService privateMessageService;
    private final LanguageService languageService;
    private final NotificationService notificationService;
    private final CoupleProfileService coupleProfileService;
    private final PriceService priceService;
    private final CreditService creditService;
    private CloudStorage cloudStorage;
    private final DailyQuestionService dailyQuestionService;

    @Value("${square.applicationId}")
    private String squareAppID;
    @Value("${google.maps.api.key}")
    private String googleKeyId;

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @Value("${lovappy.cloud.bucket.lovdrop}")
    private String lovdropsBucket;

    @Autowired
    public MemberProfileController(UserService userService, MusicService musicService, UserProfileService userProfileService,
                                   UserPreferenceService userPreferenceService, LikeService likeService,
                                   LovstampService lovstampService, PrivateMessageService privateMessageService,
                                   LanguageService languageService, CloudStorage cloudStorage,
                                   UserProfileAndPreferenceService userProfileAndPreferenceService, NotificationService notificationService, CoupleProfileService coupleProfileService, PriceService priceService, CreditService creditService, DailyQuestionService dailyQuestionService) {
        this.userService = userService;
        this.musicService = musicService;
        this.userProfileService = userProfileService;
        this.userPreferenceService = userPreferenceService;
        this.likeService = likeService;
        this.lovstampService = lovstampService;
        this.privateMessageService = privateMessageService;
        this.languageService = languageService;
        this.cloudStorage = cloudStorage;
        this.userProfileAndPreferenceService = userProfileAndPreferenceService;
        this.notificationService = notificationService;
        this.coupleProfileService = coupleProfileService;
        this.priceService = priceService;
        this.creditService = creditService;
        this.dailyQuestionService = dailyQuestionService;
    }


    @GetMapping("/{id}")
    public String getMember(HttpServletRequest request, @PathVariable("id") Integer userId, Model model) {
        LOGGER.debug("Get Member {}:", userId);

        if (userId.equals(UserUtil.INSTANCE.getCurrentUserId()))
            return "redirect:/member";
        try {

            /**
             * User -->email, title, firstName, lastName, address, city, state, zipcode, country
             */
            UserDto userDto = userService.getUser(userId);
            if (userDto == null || !userDto.getActivated()) {
                return "error/404";
            }
            /**
             *Profile -->gender, birthDate, language, heightFt, heightIn
             */
            MemberProfileDto memberDTO = userProfileService.findFullOneWihtAllowedProfilePic(userId, UserUtil.INSTANCE.getCurrentUserId());
            if (memberDTO == null) {
                memberDTO = new MemberProfileDto();
            }

            if (userId != UserUtil.INSTANCE.getCurrentUserId()) {
                userProfileService.updateUserProfileViewsAndAvgDistance(UserUtil.INSTANCE.getCurrentUserId(), userId);
            }
            model.addAttribute("loginprofile", userId == UserUtil.INSTANCE.getCurrentUserId());


            List<Integer> userIds;
            if (request.getSession().getAttribute("userIds") != null) {
                userIds = (List<Integer>) request.getSession().getAttribute("userIds");
            } else {
                userIds = new ArrayList<>();
            }

            Integer previousUserId = null;
            Integer nextUserId = null;
            for (int i = 0; i < userIds.size(); i++) {
                //184
                LOGGER.error("User Id " + userIds.get(i));
                if (userIds.get(i).equals(userId)) {
                    LOGGER.error("User Id equal" + userIds.get(i));
                    previousUserId = i == 0 ? null : userIds.get(i - 1);
                    nextUserId = i == userIds.size() - 1 ? null : userIds.get(i + 1);
                    break;
                }
            }

            model.addAttribute("previousUserId", previousUserId);
            model.addAttribute("nextUserId", nextUserId);
            /**
             *Preference-->prefGender, penisSizeMatter, prefBustSize, prefButtSize, prefMinAge, prefMaxAge, prefHeight, prefLanguage
             */
            UserPreferenceDto userPre = userPreferenceService.findOne(userId);

            model.addAttribute("messagesPrice", priceService.getByType(OrderDetailType.PRIVATE_MESSAGE).getPrice());
            model.addAttribute("member", userDto);
            model.addAttribute("memberProfile", (memberDTO != null ? memberDTO : new MemberProfileDto()));
//		    new ProfilePicsDto(userProfileRepo.findOne(userId))
            model.addAttribute("memberPreference", (userPre != null ? userPre : new UserPreferenceDto()));
            // Does He/She block me????
            //check his profile
            model.addAttribute("blockStatus", (memberDTO != null ? memberDTO.getBlockStatus(UserUtil.INSTANCE.getCurrentUserId()) : false));
            //Let Me check If I blocked him
            Boolean blocked = (userProfileService.getBlockStatus(UserUtil.INSTANCE.getCurrentUserId(), userId));
            model.addAttribute("blocked", blocked);
            model.addAttribute("btnBlockImage", blocked ? "/images/icons/unblock.png" : "/images/icons/block.png");
            LovstampDto lovstampDto = lovstampService.getLovstampByUserID(userId);
            model.addAttribute("lovstamp", lovstampDto == null ? new LovstampDto() : lovstampDto);
            model.addAttribute("userFlag", new UserFlagDto());
            model.addAttribute("lovstamps", lovstampService.getUserProfileLovstamps(UserUtil.INSTANCE.getCurrentUserId(), userId));

            model.addAttribute("responseCount", dailyQuestionService.findUserResponseCountByUserId(userId));
            model.addAttribute("profilePicAccess", userProfileService.isProfilePicAccessed(UserUtil.INSTANCE.getCurrentUserId(), userId));
//
            model.addAttribute("profilePicPublic", userProfileService.lockedByTransactionAlgorithem(UserUtil.INSTANCE.getCurrentUserId(), userId) || userProfileService.isProfilePicAccessed(userId, UserUtil.INSTANCE.getCurrentUserId()));
//

            model.addAttribute("messagesReceived",
                    privateMessageService.getPrivateMessagesByFromUserAndToUser(userId, UserUtil.INSTANCE.getCurrentUserId()));

            model.addAttribute("messagesSent",
                    privateMessageService.getPrivateMessagesByFromUserAndToUser(UserUtil.INSTANCE.getCurrentUserId(), userId));


            model.addAttribute("musicReceivedCount",
                    musicService.countMusicSentBetweenFromAndToUser(userId, UserUtil.INSTANCE.getCurrentUserId()));

            model.addAttribute("musicSentCount",
                    musicService.countMusicSentBetweenFromAndToUser(UserUtil.INSTANCE.getCurrentUserId(), userId));
            // Set LIkE/UNLIKE
            boolean likeme = likeService.likeme(UserUtil.INSTANCE.getCurrentUserId(), userId);
            model.addAttribute("likeme", likeme);
            Boolean ilikeTo = likeService.liketo(UserUtil.INSTANCE.getCurrentUserId(), userId);
            model.addAttribute("liketo", ilikeTo == null ? false : ilikeTo);
            Boolean matches = likeService.match(UserUtil.INSTANCE.getCurrentUserId(), userId);
            model.addAttribute("matches", matches == null ? false : matches);
            LOGGER.info("Like me : {} , Ilike To : {} , Match {} ", likeme, ilikeTo, matches);

            if (matches) {
                model.addAttribute("btnLikeImage", "/images/icons/match_icon.png");
            } else if (likeme) {
                model.addAttribute("btnLikeImage", "/images/icons/like_you.png");
            } else {
                model.addAttribute("btnLikeImage", !ilikeTo ? "/images/icons/like.png" : "/images/icons/like_them.png");
            }

// if(matches){
//					memberDTO.setMatches((UserUtil.INSTANCE.getCurrentUserId()!=null && userId!=null)?(likeService.match(UserUtil.INSTANCE.getCurrentUserId(),userId )):null);
//			}			
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "member/view-profile-with-msg";
    }


    @GetMapping("/responses")
    public ModelAndView getPastResponsesOfUser(ModelAndView model) {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        model.setViewName("user/past-recordings");
        model.addObject("responses", dailyQuestionService.findUserResponseByUserId(userId));
        return model;
    }


    @GetMapping("/{id}/responses")
    public ModelAndView getPastResponsesOthers(@PathVariable("id") Integer userId, ModelAndView model) {
        MemberProfileDto memberDTO = userProfileService.findFullOneWihtAllowedProfilePic(userId, UserUtil.INSTANCE.getCurrentUserId());
        UserPreferenceDto userPre = userPreferenceService.findOne(userId);
        UserPreferenceAndStatusesDto userPreferences = new UserPreferenceAndStatusesDto(userPre, memberDTO.getStatuses());
        model.addObject("memberId", userId);
        model.addObject("memberProfile", memberDTO == null ? new MemberProfileDto() : memberDTO);
        model.addObject("memberPreference", userPreferences);
        model.addObject("responses", dailyQuestionService.findPaidUserResponseByUserId(userId, UserUtil.INSTANCE.getCurrentUserId()));
        model.addObject("profilePicPublic", userProfileService.lockedByTransactionAlgorithem(UserUtil.INSTANCE.getCurrentUserId(), userId) || userProfileService.isProfilePicAccessed(userId, UserUtil.INSTANCE.getCurrentUserId()));
        model.addObject("squareAppID", squareAppID);
        model.setViewName("user/past-questions");
        return model;
    }

    @PostMapping(value = "/{id}/response/upload")
    public ResponseEntity<Void> uploadResponse(@PathVariable("id") Long questionId,
                                               @RequestParam("data") MultipartFile uploadFile,
                                               @RequestParam("seconds") Integer seconds,
                                               @RequestParam("languageId") Integer languageCode,
                                               @RequestParam("today") Long currentDate) {
        try {
            CloudStorageFileDto cloudStorageFileDto =
                    cloudStorage.uploadFile(uploadFile, lovdropsBucket, FileExtension.mp3.toString(), "audio/mp3");

            if (cloudStorageFileDto != null) {
                UserResponseDto userResponse = new UserResponseDto();
                userResponse.setAudioResponse(cloudStorageFileDto);
                userResponse.setQuestionId(questionId);
                userResponse.setResponseDate(new Date(currentDate));
                userResponse.setUserId(UserUtil.INSTANCE.getCurrentUserId());
                userResponse.setLanguageId(languageCode);
                userResponse.setRecordSeconds(seconds);

                UserResponseDto userResponseDto = dailyQuestionService.addUserResponse(userResponse);
                if (userResponseDto != null) {
                    return ResponseEntity.ok().build();
                }
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @PutMapping(value = "/response/{id}")
    public ResponseEntity<Void> updateResponse(@PathVariable("id") Long responseId,
                                               @RequestParam("data") MultipartFile uploadFile,
                                               @RequestParam("seconds") Integer seconds) {
        try {
            CloudStorageFileDto cloudStorageFileDto =
                    cloudStorage.uploadFile(uploadFile, lovdropsBucket, FileExtension.mp3.toString(), "audio/mp3");

            if (cloudStorageFileDto != null) {
                UserResponseDto userResponse = new UserResponseDto();
                userResponse.setAudioResponse(cloudStorageFileDto);
                userResponse.setResponseId(responseId);
                userResponse.setUserId(UserUtil.INSTANCE.getCurrentUserId());
                userResponse.setRecordSeconds(seconds);

                UserResponseDto userResponseDto = dailyQuestionService.updateUserResponse(userResponse);
                if (userResponseDto != null) {
                    return ResponseEntity.ok().build();
                }
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @GetMapping("/response/delete/{id}")
    public ModelAndView deleteResponseOfUser(ModelAndView modelAndView, @PathVariable("id") Long responseId) {
        modelAndView.setViewName("redirect:/member/responses");
        Boolean isDeleted = dailyQuestionService.deleteResponse(responseId);
        return modelAndView;
    }

    @GetMapping()
    public String getMe(Model model) {
        LOGGER.debug("Get Member {}:", UserUtil.INSTANCE.getCurrentUserId());
        try {

            /**
             * User -->email, title, firstName, lastName, address, city, state, zipcode, country
             */
            UserDto userDto = userService.getUser(UserUtil.INSTANCE.getCurrentUserId());
            boolean isUser = false;
            for (RoleDto roleDto : userDto.getRoles()) {
                if (roleDto.getName().equals(Roles.USER.getValue())) {
                    isUser = true;
                }

            }
            if (!isUser) {
                model.addAttribute("user", new ProfileDto(userDto));
                return "user/profiles/user-profile";
            }

            /**
             *Profile -->gender, birthDate, language, heightFt, heightIn
             */
            MemberProfileDto memberDTO = userProfileService.findFullOne(UserUtil.INSTANCE.getCurrentUserId());
            if (memberDTO == null) {
                memberDTO = new MemberProfileDto();
            }

            boolean seekingLove = !userDto.getRoles().contains(new RoleDto(Roles.COUPLE.getValue()));
            /**
             *Preference-->prefGender, penisSizeMatter, prefBustSize, prefButtSize, prefMinAge, prefMaxAge, prefHeight, prefLanguage
             */
            UserPreferenceDto userPre = userPreferenceService.findOne(UserUtil.INSTANCE.getCurrentUserId());
            userPre.setSeekingLove(seekingLove);
            UserPreferenceAndStatusesDto userPreferenceAndStatusesDto = new UserPreferenceAndStatusesDto(userPre, memberDTO.getStatuses());

            model.addAttribute("seekingLove", seekingLove ? "I'm seeking love" : "I want to Continue dating my love");
            model.addAttribute("member", userDto);
            model.addAttribute("userProfile", userDto.getUserProfile());
            model.addAttribute("memberProfile", (memberDTO != null ? memberDTO : new MemberProfileDto()));
            model.addAttribute("memberPreference", (userPreferenceAndStatusesDto != null ? userPreferenceAndStatusesDto : new UserPreferenceAndStatusesDto()));
            model.addAttribute("memberSeekingInfo", (userPre != null ? userPre : new UserPreferenceDto()));
            model.addAttribute("responseCount", dailyQuestionService.findUserResponseCountByUserId(UserUtil.INSTANCE.getCurrentUserId()));
            PersonalityLifestyleDto info = userProfileService.getPersonalityLifestyle(UserUtil.INSTANCE.getCurrentUserId());
            model.addAttribute("PersonalityLifestyle", info);
            DailyQuestionDto questionDto = dailyQuestionService.findLatestQuestion();
            model.addAttribute("question", questionDto == null ? new DailyQuestionDto() : questionDto);
            model.addAttribute("languages", languageService.getAllLanguages(true));
            model.addAttribute("lovstamp", lovstampService.getLovstampByUserID(UserUtil.INSTANCE.getCurrentUserId()));
            model.addAttribute("google_key", googleKeyId);

            // Does He/She block me????
            CouplesProfileDto couplesProfileDto = coupleProfileService.getCoupleProfile(UserUtil.INSTANCE.getCurrentUserId());

            if (couplesProfileDto == null) {
                couplesProfileDto = new CouplesProfileDto();
                couplesProfileDto.setGender(memberDTO.getGender());
                couplesProfileDto.setBirthDate(memberDTO.getBirthDate());
                couplesProfileDto.setCoupleGender(Gender.MALE);
            }

            model.addAttribute("coupleRequest", couplesProfileDto);

            model.addAttribute("musicReceivedCount",
                    musicService.countMusicReceived(UserUtil.INSTANCE.getCurrentUserId()));

            model.addAttribute("musicSentCount",
                    musicService.countMusicSent(UserUtil.INSTANCE.getCurrentUserId()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "member/my-profile";
    }

    @GetMapping("/{user-id}/block")
    public String blockUser(@PathVariable("user-id") Integer userId,
                            @RequestParam(value = "redirectUrl", required = false) String redirectUrl) {
        LOGGER.info("blockUser {}", userId);
        if (UserUtil.INSTANCE.getCurrentUserId().equals(userId)) {
            LOGGER.error("Can not blockUser the same as login User :  {} ", UserUtil.INSTANCE.getCurrentUser().getEmail());
        } else {
            boolean blocked = userProfileService.getBlockStatus(UserUtil.INSTANCE.getCurrentUser().getUserId(), userId);
            if (!blocked) {
                int blockedSuccess = userProfileService.blockUser(UserUtil.INSTANCE.getCurrentUserId(), userId);
                LOGGER.debug("Block State update result :  {} ", blockedSuccess);

            } else {
                LOGGER.warn("Should come to this block codde, user block state:  {} ", blocked);
            }
        }
        return "redirect:/member/" + userId;
    }

    @PostMapping("/{user-id}/block")
    @ResponseBody
    public String blockUser(@PathVariable("user-id") Integer userId) {
        LOGGER.info("blockUser {}", userId);
        if (UserUtil.INSTANCE.getCurrentUserId().equals(userId)) {
            LOGGER.error("Can not blockUser the same as login User :  {} ", UserUtil.INSTANCE.getCurrentUser().getEmail());
        } else {
            boolean blocked = userProfileService.getBlockStatus(UserUtil.INSTANCE.getCurrentUser().getUserId(), userId);
            if (!blocked) {
                int blockedSuccess = userProfileService.blockUser(UserUtil.INSTANCE.getCurrentUserId(), userId);
                LOGGER.debug("Block State update result :  {} ", blockedSuccess);

            } else {
                LOGGER.warn("Should come to this block codde, user block state:  {} ", blocked);
            }
        }
        return "success";
    }

    @PostMapping("/{user-id}/report")
    public String flagUser(@PathVariable("user-id") Integer userId, @Valid UserFlagDto userFlagDto) {
        //TODO Use API later
        userFlagDto = flagUserAPI(userId, userFlagDto);
        return "redirect:" + (StringUtils.isEmpty(userFlagDto.getRedirectUrl()) ? "/member/" + userId : userFlagDto.getRedirectUrl());
    }


    @PostMapping("/flag/{user-id}")
    @ResponseBody
    public UserFlagDto flagUserAPI(@PathVariable("user-id") Integer userId, UserFlagDto userFlagDto) {
        try {
            LOGGER.info("flagUser {}", userId);
            if (UserUtil.INSTANCE.getCurrentUserId().equals(userId)) {
                LOGGER.error("Can not flag the same as login User :  {} ", UserUtil.INSTANCE.getCurrentUser().getEmail());
            } else {
                boolean blocked = userProfileService.getBlockStatus(UserUtil.INSTANCE.getCurrentUser().getUserId(), userId);
                if (!blocked) {
                    userFlagDto.setFlagBy(UserUtil.INSTANCE.getCurrentUserId());
                    userFlagDto.setFlagTo(userId);
                    userFlagDto.setOffensive("offensive".equals(userFlagDto.getFormInput()));
                    userFlagDto.setDiscriminatory("discriminatory".equals(userFlagDto.getFormInput()));
                    userFlagDto.setHarrassment("harrassment".equals(userFlagDto.getFormInput()));
                    userFlagDto.setNotRelevant("notRelevant".equals(userFlagDto.getFormInput()));
                    userFlagDto.setUncivil("uncivil".equals(userFlagDto.getFormInput()));

                    String email = userService.reportUser(userFlagDto);

                    NotificationDto notificationDto = new NotificationDto();
                    notificationDto.setContent(email + " was Flagged ");
                    notificationDto.setType(NotificationTypes.USER_FLAGS);
                    notificationDto.setUserId(userFlagDto.getFlagTo());
                    notificationService.sendNotificationToAdmins(notificationDto);

                    LOGGER.debug("Flag State update result :  ");
                } else {
                    LOGGER.warn("Should not come to this flag code, user flag state:  {} ", blocked);
                }
            }
        } catch (Exception ex) {
            LOGGER.warn("this user is already flagged");
        }
        return userFlagDto;
    }

    @GetMapping("/unblock")
    public String unblockUser(Integer userId) {
        LOGGER.info("unblockUser {}", userId);
        if (UserUtil.INSTANCE.getCurrentUserId().equals(userId)) {
            LOGGER.error("Can not unblockUser the same as login User :  {} ", UserUtil.INSTANCE.getCurrentUser().getEmail());
        } else {
            int blockedSuccess = userProfileService.unblockUser(UserUtil.INSTANCE.getCurrentUserId(), userId);
        }
        return "redirect:/member/" + userId;
    }


    //Added two new methods for block and unblock with POST request

    @PostMapping("/block")
    public ResponseEntity blockUserNew(Integer userId) {
        if (UserUtil.INSTANCE.getCurrentUserId().equals(userId)) {
            return new ResponseEntity<>("Can not blockUser the same as login User.", HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            userProfileService.blockUser(UserUtil.INSTANCE.getCurrentUserId(), userId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @PostMapping("/unblock")
    public ResponseEntity unblockUserNew(Integer userId) {
        if (UserUtil.INSTANCE.getCurrentUserId().equals(userId)) {
            return new ResponseEntity<>("Can not unblockUser the same as login User.", HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            boolean blocked = userProfileService.getBlockStatus(UserUtil.INSTANCE.getCurrentUser().getUserId(), userId);
            if (blocked) {
                userProfileService.unblockUser(UserUtil.INSTANCE.getCurrentUserId(), userId);
            }
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @PostMapping("/save_preferences")
    public ResponseEntity savePreferences(@ModelAttribute("PersonalityLifestyle") PersonalityLifestyleStatusLocationDto personalityLifeStyle,
                                          @ModelAttribute("memberPreference") UserPreferenceAndStatusesDto userPreferences) {

        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();

        Map<String, String> lifestyles = personalityLifeStyle.getLifestyles();
        Map<String, String> personalities = personalityLifeStyle.getPersonalities();

        if (personalityLifeStyle.getLifestyles() == null || personalityLifeStyle.getLifestyles().get("catORdog") == null)
            errorMessages.add(new ErrorMessage("catORdog", "What do you prefer?"));

        if (personalityLifeStyle.getLifestyles() == null || personalityLifeStyle.getLifestyles().get("drinker") == null && personalityLifeStyle.getLifestyles().get("smoker") == null &&
                personalityLifeStyle.getLifestyles().get("user") == null && personalityLifeStyle.getLifestyles().get("no-habit") == null)
            errorMessages.add(new ErrorMessage("habit", "What is your habit?"));

        if (personalityLifeStyle.getLifestyles() == null || personalityLifeStyle.getLifestyles().get("ruralORurbanORsuburban") == null)
            errorMessages.add(new ErrorMessage("ruralORurbanORsuburban", "What is your ideal neighborhood"));

        if (personalityLifeStyle.getLifestyles() == null || lifestyles.get("politicalopinion") == null)
            errorMessages.add(new ErrorMessage("politicalopinion", "What is your political opinion?"));

        if (personalities == null || personalities.get("outdoorsyORindoorsy") == null)
            errorMessages.add(new ErrorMessage("outdoorsyORindoorsy", "Are you outdoorsy or indoorsy?"));

        if (personalities == null || personalities.get("frugalORbigspender") == null)
            errorMessages.add(new ErrorMessage("frugalORbigspender", "Are you frugal or big spender?"));

        if (personalities == null || personalities.get("morningpersonORnightowl") == null)
            errorMessages.add(new ErrorMessage("morningpersonORnightowl", "Are you morning person or night owl?"));

        if (personalities == null || personalities.get("extrovertORintrovert") == null)
            errorMessages.add(new ErrorMessage("extrovertORintrovert", "Are you extrovert or introvert?"));

        if (errorMessages.size() > 0) {
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(HttpStatus.OK).body(res);
        } else {
            personalityLifeStyle.setStatuses(userPreferences.getStatuses());
            userProfileService.saveOrUpdate(UserUtil.INSTANCE.getCurrentUserId(), personalityLifeStyle);
            userPreferenceService.update(UserUtil.INSTANCE.getCurrentUserId(), userPreferences.getPenisSizeMatter(),
                    userPreferences.getBustSize(), userPreferences.getButtSize());


            res.setStatus(ResponseStatus.SUCCESS);
            return ResponseEntity.ok().body(res);
        }
    }


    @PostMapping("/save_seeking_info")
    public ResponseEntity savePreferences(@ModelAttribute("memberSeekingInfo") UserPreferenceDto dto) {
        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();

        //TODO Add validation

        if (errorMessages.size() > 0) {
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(HttpStatus.OK).body(res);
        } else {
            userPreferenceService.update(UserUtil.INSTANCE.getCurrentUserId(), dto.getMinAge(), dto.getMaxAge(),
                    dto.getHeight(), dto.getGender());

            if (dto.getSeekingLove()) {
                userProfileAndPreferenceService.setSeekingLove(UserUtil.INSTANCE.getCurrentUserId());
            }
            res.setStatus(ResponseStatus.SUCCESS);
            return ResponseEntity.ok().body(res);
        }
    }

    @PostMapping("/save_age_lang_location_info")
    public ResponseEntity savePreferences(@ModelAttribute("userProfile") UserProfileDto dto) {
        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();

        String[] languagesIDs1 = dto.getSelectedLanguages().split(",");
        Set<LanguageDto> list = new HashSet<>();
        for (String s : languagesIDs1) {
            list.add(new LanguageDto(Integer.valueOf(s.trim())));
        }
        dto.setSpeakingLanguages(list);

        //TODO Add validation

        if (errorMessages.size() > 0) {
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(HttpStatus.OK).body(res);
        } else {
            userProfileService.update(UserUtil.INSTANCE.getCurrentUserId(), dto.getSpeakingLanguages(), dto.getAddress());
            res.setStatus(ResponseStatus.SUCCESS);
            return ResponseEntity.ok().body(res);
        }
    }

    @GetMapping("/unlock_profile/{id}")
    public String unlockProfile(Model model, @PathVariable("id") Integer userId) {
        model.addAttribute("memberProfile", userProfileService.findFullOne(userId));
        return "member/unlock-profile-pic";
    }


    @GetMapping("/myMap")
    public ModelAndView myMaps(Model model) {
        ModelAndView mav = new ModelAndView("member/my-map");

        GenderAndPrefGenderDto info = userProfileAndPreferenceService.getGenderAndPrefGender(UserUtil.INSTANCE.getCurrentUserId());
        model.addAttribute("info", info);
        model.addAttribute("google_key", googleKeyId);
        mav.addObject("filter", new LovstampFilterDto());

        return mav;
    }

    @GetMapping("/profiles/access")
    public ModelAndView profilePicAccessPage(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                             @RequestParam(value = "filter", defaultValue = "private") String filter,
                                             @RequestParam(value = "photo-access", defaultValue = "") String photoAccess) {
        ModelAndView mav = new ModelAndView("member/access-profile-pic-list");

        page = page <= 0 ? 1 : page;

        UserProfileDto userProfileDto = userProfileService.findByUserId(UserUtil.INSTANCE.getCurrentUserId());
        if (StringUtils.isEmpty(photoAccess)) {
            mav.addObject("profileAccess", userProfileDto.getAllowedProfilePic() ? ProfilePictureAccess.PUBLIC.name() : userProfileDto.getMaxTransactions() > 0 ? ProfilePictureAccess.MANAGED.name() : ProfilePictureAccess.PRIVATE.name());
        } else {
            mav.addObject("profileAccess", photoAccess.toUpperCase());
        }

        mav.addObject("allowedProfilePic", !userProfileDto.getAllowedProfilePic());

        mav.addObject("maxTransactions", userProfileDto.getMaxTransactions());

        mav.addObject("filter", filter);
        Page<UserProfilePictureAccessDto> userProfiles = new PageImpl<>(new ArrayList<>());
        if (filter.equals("private")) {
            userProfileService.setTransactionAsSeen(UserUtil.INSTANCE.getCurrentUserId());
            userProfiles = userProfileService.getUserProfileImageAccess(UserUtil.INSTANCE.getCurrentUserId(), page, 5);
        } else if (filter.equals("pending")) {
            userProfiles = userProfileService.getPendingUserProfileImageAccess(UserUtil.INSTANCE.getCurrentUserId(), userProfileDto.getMaxTransactions(), new PageRequest(page - 1, 5));
        }

        mav.addObject("userProfiles", userProfiles);
        mav.addObject("pendingCount", userProfileService.countPendingProfileAccess(userProfileDto.getUserId(), userProfileDto.getMaxTransactions()));
        mav.addObject("privateHasAccessCount", userProfileService.countUserProfileImageAccess(userProfileDto.getUserId()));
        Pager pager = new Pager(userProfiles.getTotalPages(), userProfiles.getNumber(), 5);


        mav.addObject("pager", pager);
        return mav;
    }

    @GetMapping("/profiles/{profile-id}/disable")
    public ModelAndView disableProfileAccess(@PathVariable("profile-id") Integer profileId) {
        ModelAndView mav = new ModelAndView("redirect:member/profiles/access");

        userProfileService.disableProfileAccessView(profileId, UserUtil.INSTANCE.getCurrentUserId());
        return mav;
    }

    @GetMapping("/profile-landing")
    public ModelAndView profileLanding(Model model) {
        ModelAndView mav = new ModelAndView("member/profile-landing");

        return mav;
    }


    @GetMapping("{userId}/credits/send")
    public ModelAndView sendCredit(@PathVariable("userId") Integer userId) {

        ModelAndView modelAndView = new ModelAndView("user/send_credit");
        modelAndView.addObject("squareAppID", squareAppID);
        CreditDto creditDto = creditService.getCreditByToUser(UserUtil.INSTANCE.getCurrentUserId(), userId);
        if (creditDto.getRecievedBalance() != null)
            creditDto.setBalance(creditDto.getBalance() + creditDto.getRecievedBalance());
        modelAndView.addObject("credit", creditDto);
        modelAndView.addObject("user", userService.getToUser(userId));
        return modelAndView;
    }


}
