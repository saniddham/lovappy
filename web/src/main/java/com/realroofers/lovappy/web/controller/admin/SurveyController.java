package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.music.MusicService;
import com.realroofers.lovappy.service.music.dto.GenreDto;
import com.realroofers.lovappy.service.product.ProductTypeService;
import com.realroofers.lovappy.service.product.dto.ProductTypeDto;
import com.realroofers.lovappy.service.survey.UserSurveyService;
import com.realroofers.lovappy.service.survey.dto.SurveyAnswerDto;
import com.realroofers.lovappy.service.survey.dto.SurveyQuestionDto;
import com.realroofers.lovappy.service.survey.dto.UserSurveyDto;
import com.realroofers.lovappy.web.util.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;


@Controller
@RequestMapping("/lox/survey")
public class SurveyController {

    private final UserSurveyService userSurveyService;
    private final ProductTypeService productTypeService;
    private final MusicService musicService;

    @Autowired
    public SurveyController(UserSurveyService userSurveyService, ProductTypeService productTypeService, MusicService musicService) {
        this.userSurveyService = userSurveyService;
        this.productTypeService = productTypeService;
        this.musicService = musicService;
    }

    @GetMapping("/user_answers")
    public ModelAndView survey(@RequestParam("pageSize") Optional<Integer> pageSize,
                               @RequestParam("page") Optional<Integer> pageNumber,
                               @RequestParam(value = "filter", defaultValue = "") String filter,
                               @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort) {
        ModelAndView mav = new ModelAndView("admin/survey/survey_answers");

        Pageable pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize));

        if (!StringUtils.isEmpty(filter)) {
            if (pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize), new Sort(sort, filter));
            }
        }


        mav.addObject("filter", filter);
        mav.addObject("sort", sort.equals(Sort.Direction.ASC) ? Sort.Direction.DESC : Sort.Direction.ASC);

        Page<UserSurveyDto> surveyPage = userSurveyService.getUserSurvey(pageable);
        mav.addObject("surveyPage", surveyPage);
        Pager pager = new Pager(surveyPage.getTotalPages(), surveyPage.getNumber(), Pager.BUTTONS_TO_SHOW);

        mav.addObject("selectedPageSize", pageSize);
        mav.addObject("pageSizes", Pager.PAGE_SIZES);
        mav.addObject("pager", pager);

        return mav;
    }

    @GetMapping("/questions")
    public ModelAndView surveyQuestions(@RequestParam("pageSize") Optional<Integer> pageSize,
                                        @RequestParam("page") Optional<Integer> pageNumber,
                                        @RequestParam(value = "filter", defaultValue = "") String filter,
                                        @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort) {

        ModelAndView mav = new ModelAndView("admin/survey/questions");

        PageRequest pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize));

        if (!StringUtils.isEmpty(filter)) {
            if (pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize), new Sort(sort, filter));
            }
        }


        mav.addObject("filter", filter);
        mav.addObject("sort", sort.equals(Sort.Direction.ASC) ? Sort.Direction.DESC : Sort.Direction.ASC);

        Page<SurveyQuestionDto> questions = userSurveyService.getAllQuestions(pageable);

        List<ProductTypeDto> typeDtoList = productTypeService.findByAddedToSurveyIsFalse();

        if (!typeDtoList.isEmpty()) {
            typeDtoList.stream().forEach(productTypeDto -> {
                productTypeDto.setAddedToSurvey(true);
                try {
                    productTypeService.update(productTypeDto);
                    userSurveyService.addAnswer(2, productTypeDto.getTypeName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }

        List<GenreDto> genreDtos = musicService.findByAddedToSurveyIsFalse();

        if(!genreDtos.isEmpty()){
            genreDtos.stream().forEach(genreDto  -> {
                genreDto.setAddedToSurvey(true);
                try {
                    musicService.updateGenre(genreDto);
                    userSurveyService.addAnswer(1, genreDto.getTitle());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }

        mav.addObject("questions", questions);
        Pager pager = new Pager(questions.getTotalPages(), questions.getNumber(), Pager.BUTTONS_TO_SHOW);

        mav.addObject("selectedPageSize", pageSize);
        mav.addObject("pageSizes", Pager.PAGE_SIZES);
        mav.addObject("pager", pager);
        return mav;
    }

    @PostMapping("/questions/add")
    public ResponseEntity addQuestion(@RequestParam("title") String title){
        SurveyQuestionDto surveyQuestion = userSurveyService.addQuestion(title);

        return ResponseEntity.status(HttpStatus.OK).body(surveyQuestion.getId());
    }

    @GetMapping("/questions/{id}")
    public ModelAndView questionDetails(@PathVariable(value = "id") Integer id) {
        ModelAndView mav = new ModelAndView("admin/survey/question_details");
        mav.addObject("question", userSurveyService.getQuestion(id));

        return mav;
    }

    @PostMapping("/questions/update")
    public ResponseEntity updateQuestions(@RequestParam("question_id") Integer questionID,
                                       @RequestParam("title") String title){
        SurveyQuestionDto surveyQuestion = userSurveyService.updateQuestion(questionID, title);
        return ResponseEntity.status(HttpStatus.OK).body(surveyQuestion.getId());
    }

    @GetMapping("/answer/{id}")
    public ResponseEntity getAnswer(@PathVariable(value = "id") Integer id){
        SurveyAnswerDto surveyAnswer = userSurveyService.getAnswer(id);
        return ResponseEntity.status(HttpStatus.OK).body(surveyAnswer);
    }

    @PostMapping("/answer/add")
    public ResponseEntity addAnswer(@RequestParam("question_id") Integer questionID,
                                    @RequestParam("title") String title){
        SurveyAnswerDto surveyAnswer = userSurveyService.addAnswer(questionID, title);
        return ResponseEntity.status(HttpStatus.OK).body(surveyAnswer.getId());
    }

    @PostMapping("/answer/update")
    public ResponseEntity updateAnswer(@RequestParam("answer_id") Integer answerID,
                                       @RequestParam("title") String title){
        SurveyAnswerDto surveyAnswer = userSurveyService.updateAnswer(answerID, title);
        return ResponseEntity.status(HttpStatus.OK).body(surveyAnswer.getId());
    }

    @PostMapping("/answer/delete")
    public ResponseEntity deleteAnswer(@RequestParam("id") Integer id){
        userSurveyService.deleteAnswer(id);
        return ResponseEntity.status(HttpStatus.OK).body("");
    }
}
