package com.realroofers.lovappy.web.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.realroofers.lovappy.service.user.dto.AddressDto;
import com.realroofers.lovappy.web.rest.support.AddressListConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.realroofers.lovappy.service.exception.GenericServiceException;
import com.realroofers.lovappy.web.exception.DuplicateRecordException;
import com.realroofers.lovappy.web.support.DatePropertyEditorSupport;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@ControllerAdvice
public class GlobalControllerAdvice {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalControllerAdvice.class);

    @InitBinder
    public void registerCustomEditors(WebDataBinder binder, WebRequest request) {
        binder.registerCustomEditor(Date.class, new DatePropertyEditorSupport());
        binder.registerCustomEditor(AddressDto[].class, new AddressListConverter());
    }
}
