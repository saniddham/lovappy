package com.realroofers.lovappy.web.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Used to calculate span of buttons which will be displayed on a page
 *
 * @author Branislav Lazic
 * @author Bruno Raljic
 */
public class Pager {
    private static final Logger LOGGER = LoggerFactory.getLogger(Pager.class);
    public static final int BUTTONS_TO_SHOW = 5;
    public static final int INITIAL_PAGE = 0;
    public static final int INITIAL_PAGE_SIZE = 10;
    public static final int[] PAGE_SIZES = { 5, 10, 20 };

    private int buttonsToShow = 5;

    private int startPage;

    private int endPage;

    public Pager(int totalPages, int currentPage, int buttonsToShow) {

        setButtonsToShow(buttonsToShow);

        int halfPagesToShow = getButtonsToShow() / 2;

        if (totalPages <= getButtonsToShow()) {
            setStartPage(1);
            setEndPage(totalPages);

        } else if (currentPage - halfPagesToShow <= 0) {
            setStartPage(1);
            setEndPage(getButtonsToShow());

        } else if (currentPage + halfPagesToShow == totalPages) {
            setStartPage(currentPage - halfPagesToShow);
            setEndPage(totalPages);

        } else if (currentPage + halfPagesToShow > totalPages) {
            setStartPage(totalPages - getButtonsToShow() + 1);
            setEndPage(totalPages);

        } else {
            setStartPage(currentPage - halfPagesToShow);
            setEndPage(currentPage + halfPagesToShow);
        }

    }

    public int getButtonsToShow() {
        return buttonsToShow;
    }

    public void setButtonsToShow(int buttonsToShow) {
        if (buttonsToShow % 2 != 0) {
            this.buttonsToShow = buttonsToShow;
        } else {
            throw new IllegalArgumentException("Must be an odd value!");
        }
    }

    public static int evalPageNumber(Optional<Integer> pageNumber){
        return (pageNumber.orElse(0) < 1) ? Pager.INITIAL_PAGE : pageNumber.get() - 1;
    }
    public static int evalPageInteger(Integer pageNumber){
        return (pageNumber < 1) ? Pager.INITIAL_PAGE : pageNumber - 1;
    }
    public static int evalPageSize(Optional<Integer> pageSize){
        return pageSize.orElse(Pager.INITIAL_PAGE_SIZE);
    }

    public static String getURL(HttpServletRequest req) {

        String scheme = req.getScheme();             // http
        String serverName = req.getServerName();     // hostname.com
        int serverPort = req.getServerPort();        // 80
        String contextPath = req.getContextPath();   // /mywebapp
        String servletPath = req.getServletPath();   // /servlet/MyServlet
        String pathInfo = req.getPathInfo();         // /a/b;c=123
        String queryString = req.getQueryString();          // d=789

        // Reconstruct original requesting URL
        StringBuilder url = new StringBuilder();
        url.append(scheme).append("://").append(serverName);

        if (serverPort != 80 && serverPort != 443) {
            url.append(":").append(serverPort);
        }

        url.append(contextPath).append(servletPath);

        if (pathInfo != null) {
            url.append(pathInfo);
        }
        if (queryString != null) {
            String queries[] = queryString.split("&");

            for (int i = 0; i < queries.length; i++) {
                if (queries[i].isEmpty() || queries[i].startsWith("page") || queries[i].startsWith("pageSize"))
                    continue;
                if(i==0)
                    url.append("?");

                url.append(queries[i]);
                if(i < queries.length - 1) {
                    url.append("&");
                }
            }
        }
        return url.toString();
    }
    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getEndPage() {
        return endPage;
    }

    public void setEndPage(int endPage) {
        this.endPage = endPage;
    }

    @Override
    public String toString() {
        return "Pager [startPage=" + startPage + ", endPage=" + endPage + "]";
    }

}