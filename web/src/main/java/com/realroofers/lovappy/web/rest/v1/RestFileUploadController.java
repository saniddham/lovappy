package com.realroofers.lovappy.web.rest.v1;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.product.support.Upload;
import com.realroofers.lovappy.service.upload.FileUploadService;
import com.realroofers.lovappy.service.upload.PodcastCategoryService;
import com.realroofers.lovappy.service.upload.dto.FileUploadDto;
import com.realroofers.lovappy.service.upload.dto.PodcastCategoryDto;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.FormValidator;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.service.validation.ResponseStatus;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Created by Manoj
 */
@RestController
@RequestMapping({"/api/v1/upload", "/api/v2/upload"})
public class RestFileUploadController {

    private final FileUploadService fileUploadService;
    private final PodcastCategoryService uploadCategoryService;
    private final FormValidator formValidator;
    private final CloudStorage cloudStorage;

    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 10;

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    public RestFileUploadController(FileUploadService fileUploadService,
                                    PodcastCategoryService uploadCategoryService,
                                    FormValidator formValidator, CloudStorage cloudStorage) {
        this.fileUploadService = fileUploadService;
        this.uploadCategoryService = uploadCategoryService;
        this.formValidator = formValidator;
        this.cloudStorage = cloudStorage;
    }

    @GetMapping("/categories")
    public ResponseEntity<Page<PodcastCategoryDto>> getAllCategories(
            @RequestParam(value = "page_size", required = false) Optional<Integer> pageSize,
            @RequestParam(value = "page_number", required = false) Optional<Integer> pageNumber) {

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (pageNumber.orElse(0) < 1) ? INITIAL_PAGE : pageNumber.get() - 1;

        Page<PodcastCategoryDto> categoryDtos = uploadCategoryService.findAll(new PageRequest(evalPage, evalPageSize));

        return ResponseEntity.ok(categoryDtos);
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<PodcastCategoryDto> getCategoryById(@PathVariable("id") Integer id) {

        try {
            PodcastCategoryDto itemDto = uploadCategoryService.findById(id);
            return new ResponseEntity<>(itemDto, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/category/add")
    public ResponseEntity addCategory(@ModelAttribute @Valid PodcastCategoryDto uploadCategoryDto) {

        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = formValidator.validate(uploadCategoryDto, new Class[]{Upload.class});

        if (errorMessages.size() > 0) {
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(200).body(res);
        }
        try {
            uploadCategoryService.create(uploadCategoryDto);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/category/update")
    public ResponseEntity updateCategory(@ModelAttribute @Valid PodcastCategoryDto uploadCategoryDto) {

        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = formValidator.validate(uploadCategoryDto, new Class[]{Upload.class});

        if (errorMessages.size() > 0) {
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(200).body(res);
        }
        try {
            uploadCategoryService.update(uploadCategoryDto);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<Page<FileUploadDto>> getAll(@RequestParam(value = "page", required = false) Optional<Integer> pageNumber) {

        int evalPage = (pageNumber.orElse(0) < 1) ? INITIAL_PAGE : pageNumber.get() - 1;

        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        Page<FileUploadDto> fileUploadDtos = fileUploadService.findAll(new PageRequest(evalPage, INITIAL_PAGE_SIZE), userId);

        return ResponseEntity.ok(fileUploadDtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<FileUploadDto> getById(@PathVariable("id") Integer id) {

        try {
            final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
            FileUploadDto itemDto = fileUploadService.findById(id, userId);
            return new ResponseEntity<>(itemDto, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


    @PostMapping("/add")
    public ResponseEntity add(@ModelAttribute @Valid FileUploadDto fileUploadDto,
                              @RequestParam("imageFile") MultipartFile imageFile,
                              @RequestParam("file") MultipartFile file) {

        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = formValidator.validate(fileUploadDto, new Class[]{Upload.class});

        if (errorMessages.size() > 0) {
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(500).body(res);
        }


        CloudStorageFileDto imageFileDto = null;
        CloudStorageFileDto fileDto = null;
        try {

            if (imageFile != null)
                imageFileDto = cloudStorage.uploadAndPersistFile(imageFile, imagesBucket, FileExtension.jpg.toString(), imageFile.getContentType());
            fileUploadDto.setImageFile(imageFileDto);
            if (file != null) {
                fileDto = cloudStorage.uploadAndPersistFile(file, imagesBucket, FileExtension.jpg.toString(), file.getContentType());
                fileUploadDto.setFile(fileDto);
            }
            fileUploadService.create(fileUploadDto);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity update(@ModelAttribute @Valid FileUploadDto fileUploadDto,
                                 @RequestParam(value = "imageFile", required = false) MultipartFile imageFile,
                                 @RequestParam(value = "file", required = false) MultipartFile file) {

        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = formValidator.validate(fileUploadDto, new Class[]{Upload.class});

        if (errorMessages.size() > 0) {
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(500).body(res);
        }

        CloudStorageFileDto imageFileDto = null;
        CloudStorageFileDto fileDto = null;
        try {

            if (imageFile != null)
                imageFileDto = cloudStorage.uploadAndPersistFile(imageFile, imagesBucket, FileExtension.jpg.toString(), imageFile.getContentType());
                fileUploadDto.setImageFile(imageFileDto);
            if (file != null) {
                fileDto = cloudStorage.uploadAndPersistFile(file, imagesBucket, FileExtension.jpg.toString(), file.getContentType());
                fileUploadDto.setFile(fileDto);
            }
            fileUploadService.update(fileUploadDto);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
