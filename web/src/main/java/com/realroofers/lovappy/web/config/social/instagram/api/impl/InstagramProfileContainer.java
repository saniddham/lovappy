package com.realroofers.lovappy.web.config.social.instagram.api.impl;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.realroofers.lovappy.web.config.social.instagram.api.InstagramProfile;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

@JsonDeserialize(using=InstagramProfileContainer.InstagramProfileContainerDeserializer.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class InstagramProfileContainer extends AbstractInstagramResponseContainer<InstagramProfile>{
	
	public InstagramProfileContainer(InstagramProfile profile) {
		super(profile);
	}

	public InstagramProfileContainer() {
		super();
	}

	@JsonProperty("data")
	@Override
    public void setObject(InstagramProfile profile){
		super.setObject(profile);
	}

	public static class InstagramProfileContainerDeserializer extends AbstractInstagramDeserializer<InstagramProfileContainer> {

	    @Override
	    public InstagramProfileContainer deserialize(JsonParser jp, DeserializationContext ctxt) 
	            throws IOException {
	        return deserializeResponseObject(jp, InstagramProfileContainer.class, InstagramProfile.class);
	    }

	}
}
