package com.realroofers.lovappy.web.email;

import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import org.thymeleaf.ITemplateEngine;

/**
 * Created by Daoud Shaheen on 9/9/2017.
 */
public class EmailTemplateFactory {


    public EmailTemplateHandler build(String baseUrl, String email, ITemplateEngine templateEngine, MailService mailService, EmailTemplateDto emailTemplateDto) {

        switch (emailTemplateDto.getType()) {

            case DEACTIVATION:
                return new DeactivateEmailTemplateHandler(baseUrl, email, mailService, templateEngine, emailTemplateDto);

            case VERFICATION:
                return new DefaultWithBtnEmailTemplateHandler(baseUrl, email, mailService, templateEngine, emailTemplateDto);
            case RESET_PASSWORD:
                return new ResetPasswordEmailTemplateHandler(baseUrl, email, mailService, templateEngine, emailTemplateDto);
            case EMAIL_SUBMITION:
                return new SubmitEmailTemplateHandler(baseUrl, email, mailService, templateEngine, emailTemplateDto);
            case EMAIL_INTERACTION:
                return new InteractionEmailTemplateHandler(baseUrl, email, mailService, templateEngine, emailTemplateDto);
            case EMAIL_APPROVAL:
                return new DefaultWithBtnEmailTemplateHandler(baseUrl, email, mailService, templateEngine, emailTemplateDto);
            case EMAIL_ACCEPT_DENY:
                return new UnLockProfilePictureEmailTemplateHandler(baseUrl, email, mailService, templateEngine, emailTemplateDto);
            case EMAIL_SHARE:
                return new ShareEmailTemplateHandler(baseUrl, email, mailService, templateEngine, emailTemplateDto);

            case EMAIL_INVITATION:
                return new DefaultWithBtnEmailTemplateHandler(baseUrl, email, mailService, templateEngine, emailTemplateDto);

            case GIFT_SENT:
                return new GiftSentEmailTemplateHandler(baseUrl, email, mailService, templateEngine, emailTemplateDto);

            case GIFT_REDEEM:
                return new GiftRedeemEmailTemplateHandler(baseUrl, email, mailService, templateEngine, emailTemplateDto);

            case GIFT_RECEIVED:
                return new GiftReceivedEmailTemplateHandler(baseUrl, email, mailService, templateEngine, emailTemplateDto);

            case GIFT_SOLD:
                return new GiftSentEmailTemplateHandler(baseUrl, email, mailService, templateEngine, emailTemplateDto);

        }
        return new DefaultEmailTemplateHandler(baseUrl, email, mailService, templateEngine, emailTemplateDto);

    }

}
