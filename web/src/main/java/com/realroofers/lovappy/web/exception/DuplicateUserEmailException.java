package com.realroofers.lovappy.web.exception;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public class DuplicateUserEmailException extends RuntimeException {
    public DuplicateUserEmailException(String message) {
        super(message);
    }
}
