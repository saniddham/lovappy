package com.realroofers.lovappy.web.email;

import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by Daoud Shaheen on 9/9/2017.
 */
public class DefaultEmailTemplateHandler extends AbstractEmailTemplateHandler {


    public DefaultEmailTemplateHandler(String baseUrl, String email, MailService mailService, ITemplateEngine templateEngine, EmailTemplateDto emailTemplateDto) {
        super(baseUrl, email, mailService, templateEngine, emailTemplateDto);
    }

    @Override
    public void send() {
        if (!emailTemplateDto.isEnabled())
            return;

        String body = emailTemplateDto.getBody();

        Iterator it = emailTemplateDto.getVariables().entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            body = body.replace(String.valueOf(pair.getKey()), String.valueOf(pair.getValue()));
            it.remove(); // avoids a ConcurrentModificationException
        }

        try {
            Context context = new Context();
            context.setVariable("body", body);
            context.setVariable("attendeeName", emailTemplateDto.getAdditionalInformation().get("attendeeName"));
            context.setVariable("eventTitle", emailTemplateDto.getAdditionalInformation().get("eventTitle"));
            context.setVariable("baseUrl", baseUrl);
            context.setVariable("emailBG", emailTemplateDto.getImageUrl());
            context.setVariable("title", emailTemplateDto.getName());
            String content = templateEngine.process(emailTemplateDto.getTemplateUrl(), context);
            mailService.sendMail(email, content, emailTemplateDto.getSubject(), emailTemplateDto.getFromEmail(), emailTemplateDto.getFromName());
        } catch (Exception ex) {
            LOGGER.error("Failed to send email: {}", ex);
        }
    }

}
