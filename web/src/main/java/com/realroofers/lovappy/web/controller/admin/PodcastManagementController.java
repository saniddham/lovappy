package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.news.dto.NewsCategoryDto;
import com.realroofers.lovappy.service.upload.PodcastCategoryService;
import com.realroofers.lovappy.service.upload.dto.PodcastCategoryDto;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.web.util.Pager;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Optional;

/**
 * @author Manoj Senevirathne
 */

@Controller
@RequestMapping("/lox/podcast")
public class PodcastManagementController {

    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 20;
    private static final int[] PAGE_SIZES = {5, 10, 20};

    private final PodcastCategoryService podcastCategoryService;

    public PodcastManagementController(PodcastCategoryService podcastCategoryService) {
        this.podcastCategoryService = podcastCategoryService;
    }

    @GetMapping
    public ModelAndView list(ModelAndView model) {
        model.setViewName("admin/news/news");
        return model;
    }

    @PostMapping("/save")
    public ResponseEntity saveGenre(@ModelAttribute("podcastCategoryDto") @Valid PodcastCategoryDto podcastCategoryDto) {
        if (podcastCategoryDto.getId() == null) {
            try {
                PodcastCategoryDto save = podcastCategoryService.create(podcastCategoryDto);
                return ResponseEntity.status(HttpStatus.OK).body(save.getId());
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.fillInStackTrace());
            }
        } else {
            try {
                PodcastCategoryDto save = podcastCategoryService.update(podcastCategoryDto);
                return ResponseEntity.status(HttpStatus.OK).body(save.getId());
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.fillInStackTrace());
            }
        }
    }


    @RequestMapping("/")
    public ModelAndView getNewsCategoryPage(@RequestParam("pageSize") Optional<Integer> pageSize,
                                            @RequestParam("page") Optional<Integer> page,
                                            @RequestParam(value = "searchText", defaultValue = "", required = false) String searchText) {

        ModelAndView modelAndView = new ModelAndView("admin/upload/news-category");

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        PageRequest pageable = new PageRequest(evalPage, evalPageSize);

        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        modelAndView.addObject("searchText", searchText);
        org.springframework.data.domain.Page<PodcastCategoryDto> pages = podcastCategoryService.findAll(pageable);
        Pager pager = new Pager(pages.getTotalPages(), pages.getNumber(), BUTTONS_TO_SHOW);
        modelAndView.addObject("pages", pages);

        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pageSizes", PAGE_SIZES);
        modelAndView.addObject("pager", pager);
        return modelAndView;
    }

    @GetMapping("/new")
    public ModelAndView addNewCategory() {
        ModelAndView mav = new ModelAndView("admin/upload/news_category_details");
        mav.addObject("news", new NewsCategoryDto());
        return mav;
    }

    @GetMapping("/{id}")
    public ModelAndView categoryDetails(@PathVariable(value = "id") Integer id) {
        ModelAndView mav = new ModelAndView("admin/upload/news_category_details");
        PodcastCategoryDto one = podcastCategoryService.findById(id);
        mav.addObject("news", one);
        return mav;
    }

}
