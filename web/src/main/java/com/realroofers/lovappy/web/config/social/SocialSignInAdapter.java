package com.realroofers.lovappy.web.config.social;

import com.realroofers.lovappy.service.user.SocialProfileService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.dto.SocialProfileDto;
import com.realroofers.lovappy.service.user.dto.UserAuthDto;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.web.config.AuthenticationSuccessHandlerAndRegistrationFilter;
import com.realroofers.lovappy.web.config.security.UserAuthorizationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.NativeWebRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class SocialSignInAdapter implements SignInAdapter {
    private UserAuthorizationService userAuthorizationService;
   private AuthenticationSuccessHandlerAndRegistrationFilter authenticationSuccessHandlerAndRegistrationFilter;
    private UserService userService;
    private SocialProfileService socialProfileService;
    @Autowired
    private ConnectionRepository connectionRepository;
    public SocialSignInAdapter(UserAuthorizationService userAuthorizationService, AuthenticationSuccessHandlerAndRegistrationFilter authenticationSuccessHandlerAndRegistrationFilter, UserService userService, SocialProfileService socialProfileService) {
        this.userAuthorizationService = userAuthorizationService;
        this.authenticationSuccessHandlerAndRegistrationFilter = authenticationSuccessHandlerAndRegistrationFilter;
        this.userService = userService;
        this.socialProfileService = socialProfileService;
    }

    @Override
    public String signIn(String localUserId, Connection<?> connection, NativeWebRequest webRequest) {

        log.debug(" Email {}", localUserId);
        UserAuthDto userAuthDto = null;
        try {
             userAuthDto = (UserAuthDto) userAuthorizationService.loadUserByUsername(localUserId);

        } catch (UsernameNotFoundException ex){
            userAuthDto = userAuthorizationService.socialLogin(connection, connection.getKey().getProviderId(), null);
        }


        HttpServletRequest servletRequest = webRequest.getNativeRequest(HttpServletRequest.class);
        HttpServletResponse servletResponse = webRequest.getNativeResponse(HttpServletResponse.class);

        log.info(" ROLE {}", servletRequest.getSession().getAttribute("role"));

        Roles role = Roles.USER;
        if(servletRequest.getSession().getAttribute("role") != null) {
            String roleStr = servletRequest.getSession().getAttribute("role").toString();
            switch (roleStr) {
                case "musician":
                    role = Roles.MUSICIAN;
                    break;
                case "ambassador":
                    role = Roles.EVENT_AMBASSADOR;
                    break;
                case "author":
                    role = Roles.AUTHOR;
                    break;
                default:
                    role = Roles.USER;
                    break;
            }
        }
      //  UserAuthDto user = userAuthorizationService.socialLogin(connection, connection.getKey().getProviderId(), null);
        String email = userAuthDto.getEmail();
        if(StringUtils.isEmpty(email)){
          return "/login";
        }
        if (userService.emailExists(email)) {
            User userByEmail = userService.getUserByEmail(email);
            userAuthDto.setUserId(userByEmail.getUserId());
            SocialProfileDto socialProfileDto = new SocialProfileDto();
            socialProfileDto.setFirstName(userAuthDto.getFirstName());
            socialProfileDto.setLastName(userAuthDto.getLastName());
            socialProfileDto.setSocialId(userAuthDto.getSocialId());
            socialProfileDto.setSocialPlatform(connection.getKey().getProviderId());
            socialProfileDto.setImageUrl(userAuthDto.getImageUrl());
            socialProfileDto.setUserId(userByEmail.getUserId());
            try {
                socialProfileService.update(socialProfileDto);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            userAuthDto.setUserId(socialProfileService.saveSocial(userAuthDto, role));
        }

        UsernamePasswordAuthenticationToken updatedAuth = new UsernamePasswordAuthenticationToken(userAuthDto, userAuthDto.getSocialId(),
                userAuthDto.getAuthorities());


        SecurityContextHolder.getContext().setAuthentication(updatedAuth);
        // add authentication to the session
        servletRequest.getSession().setAttribute(
                HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
                SecurityContextHolder.getContext());

        try {
            return authenticationSuccessHandlerAndRegistrationFilter.onAuthenticationSuccess(updatedAuth, servletRequest, servletResponse);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ServletException e) {
            e.printStackTrace();
        }
        connectionRepository.removeConnection(connection.getKey());
        return "/radio";
    }
}