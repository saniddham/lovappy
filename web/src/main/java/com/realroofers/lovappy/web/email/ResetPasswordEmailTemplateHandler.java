package com.realroofers.lovappy.web.email;

import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Created by Daoud Shaheen on 9/9/2017.
 */
public class ResetPasswordEmailTemplateHandler extends AbstractEmailTemplateHandler {


    public ResetPasswordEmailTemplateHandler(String baseUrl, String email, MailService mailService, ITemplateEngine templateEngine, EmailTemplateDto emailTemplateDto) {
        super(baseUrl, email, mailService, templateEngine, emailTemplateDto);
    }

    @Override
    public void send() {
        if (!emailTemplateDto.isEnabled())
            return;

        try {
            mailService.sendResetEmail(email, emailTemplateDto.getSubject(), (user, passwordResetRequest) -> {
                String name = user.getFirstName() + " " + user.getLastName();
                Context context = new Context();
                context.setVariable("name", name);
                context.setVariable("title", emailTemplateDto.getName());
                context.setVariable("body", emailTemplateDto.getBody());
                context.setVariable("baseUrl", baseUrl);
                context.setVariable("buttonText", emailTemplateDto.getAdditionalInformation().get("buttonText") == null ? "Reset Password" : emailTemplateDto.getAdditionalInformation().get("buttonText"));
                context.setVariable("emailBG", emailTemplateDto.getImageUrl());
                context.setVariable("link", baseUrl + "/set_new_password/" + passwordResetRequest.getToken());
                return templateEngine.process(emailTemplateDto.getTemplateUrl(), context);
            });

        } catch (Exception ex) {
            LOGGER.error("Failed to send email: {}", ex);
        }
    }
}
