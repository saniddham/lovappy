package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.product.BrandService;
import com.realroofers.lovappy.service.product.ProductTypeService;
import com.realroofers.lovappy.service.user.CountryService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.FormValidator;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.service.validation.ResponseStatus;
import com.realroofers.lovappy.service.validator.UserRegister;
import com.realroofers.lovappy.service.vendor.VendorService;
import com.realroofers.lovappy.service.vendor.dto.RetailerRegistrationDto;
import com.realroofers.lovappy.service.vendor.dto.VendorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Manoj on 03/02/2018.
 */
@Controller
@RequestMapping("/retailer")
public class RetailerController {

    private final CountryService countryService;
    private final VendorService vendorService;
    private final BrandService brandService;
    private final ProductTypeService productTypeService;
    private final FormValidator formValidator;

    @Autowired
    public RetailerController(CountryService countryService, VendorService vendorService,
                              BrandService brandService, ProductTypeService productTypeService, FormValidator formValidator) {
        this.countryService = countryService;
        this.vendorService = vendorService;
        this.brandService = brandService;
        this.productTypeService = productTypeService;
        this.formValidator = formValidator;
    }

    @GetMapping("/register")
    public String registrationRetailerPage(Model model) {

        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        model.addAttribute("userId", userId);
        model.addAttribute("countries", countryService.findAllActive());

        model.addAttribute("info", new RetailerRegistrationDto());

        return "vendor/registration/retailer";
    }


    @PostMapping("/register")
    public ResponseEntity registration(@ModelAttribute @Valid RetailerRegistrationDto retailerRegistrationDto) {
        JsonResponse res = new JsonResponse();
        try {
            List<ErrorMessage> errorMessages = formValidator.validate(retailerRegistrationDto, new Class[]{UserRegister.class});
            if (errorMessages.size() > 0) {
                res.setErrorMessageList(errorMessages);
                res.setStatus(ResponseStatus.FAIL);
                return ResponseEntity.ok().body(res);
            }

            final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
            VendorDto vendor = vendorService.findByUser(userId);
            vendor.setAddressLine1(retailerRegistrationDto.getAddressLine1());
            vendor.setAddressLine2(retailerRegistrationDto.getAddressLine2());
            vendor.setCity(retailerRegistrationDto.getCity());
            vendor.setZipCode(retailerRegistrationDto.getZipCode());
            vendor.setCountry(retailerRegistrationDto.getCountry());
            vendor.setRegistrationStep1(true);

            vendorService.update(vendor);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body(res);
        }
        return ResponseEntity.ok().body(res);

    }

}
