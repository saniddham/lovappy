package com.realroofers.lovappy.web.util;

import com.realroofers.lovappy.service.product.dto.ProductUploadDto;
import com.realroofers.lovappy.service.vendor.model.VendorLocation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manoj on 03/02/2018.
 */
public class ExcelReader {

    public synchronized List<ProductUploadDto> getProducts(InputStream inputStream) throws IOException {

        List<ProductUploadDto> productList = new ArrayList<>();

        read(inputStream).forEach(row -> {
            if (row.getRowNum() > 0) {

                String firstRecord = getStringValue(row.getCell(0));
                if (firstRecord != null && !firstRecord.equalsIgnoreCase("")) {
                    productList.add(extractProduct(row));
                }
            }
        });

        return productList;
    }

    public synchronized List<VendorLocation> getVendorLocation(InputStream inputStream) throws IOException {

        List<VendorLocation> vendorLocationList = new ArrayList<>();

        readXSSF(inputStream).forEach(row -> {
            if (row.getRowNum() > 0) {
                String firstRecord = getStringValue(row.getCell(0));
                if (firstRecord != null && !firstRecord.equalsIgnoreCase("")) {
                    vendorLocationList.add(extractVendorLocation(row));
                }
            }
        });

        return vendorLocationList;
    }

    private synchronized Sheet read(InputStream inputStream) throws IOException {

        // Creating a Workbook from an Excel file (.xls or .xlsx)
        Workbook workbook = new HSSFWorkbook(inputStream);

        // Getting the Sheet at index zero
        Sheet sheet = workbook.getSheetAt(0);

        // Closing the workbook
        workbook.close();

        return sheet;
    }

    private synchronized Sheet readXSSF(InputStream inputStream) throws IOException {

        // Creating a Workbook from an Excel file (.xls or .xlsx)
        Workbook workbook = new XSSFWorkbook(inputStream);

        // Getting the Sheet at index zero
        Sheet sheet = workbook.getSheetAt(0);

        // Closing the workbook
        workbook.close();

        return sheet;
    }

    private VendorLocation extractVendorLocation(Row row) {
        VendorLocation vendorLocation = new VendorLocation();
        vendorLocation.setCompanyName(getStringValue(row.getCell(0)));
        vendorLocation.setStoreId(getStringValue(row.getCell(1)));
        vendorLocation.setStoreManager(getStringValue(row.getCell(2)));
        vendorLocation.setStoreManagerContact(getStringValue(row.getCell(3)));
        vendorLocation.setAssistantManager(getStringValue(row.getCell(4)));
        vendorLocation.setAssistantManagerContact(getStringValue(row.getCell(5)));
        vendorLocation.setAddressLine1(getStringValue(row.getCell(6)));
        vendorLocation.setAddressLine2(getStringValue(row.getCell(7)));
        vendorLocation.setCity(getStringValue(row.getCell(8)));
        vendorLocation.setState(getStringValue(row.getCell(9)));
        vendorLocation.setZipCode(getStringValue(row.getCell(10)));
        vendorLocation.setCountry(getStringValue(row.getCell(11)));
        vendorLocation.setLocationContact(getStringValue(row.getCell(12)));
        vendorLocation.setOpenFrom(getStringValue(row.getCell(13)));
        vendorLocation.setOpenTo(getStringValue(row.getCell(14)));

        return vendorLocation;
    }

    private ProductUploadDto extractProduct(Row row) {
        ProductUploadDto product = new ProductUploadDto();
        product.setProductCode(getStringValue(row.getCell(0)));
        product.setProductName(getStringValue(row.getCell(1)));
        product.setProductType(getStringValue(row.getCell(2)));
        product.setBrand(getStringValue(row.getCell(3)));
        product.setProductEan(getStringValue(row.getCell(4)));
        product.setProductDescription(getStringValue(row.getCell(5)));
        product.setPrice(getDoubleValue(row.getCell(6)));
        product.setProductAvailability(getStringValue(row.getCell(7)));
        product.setImage1(getStringValue(row.getCell(8)));
        product.setImage2(getStringValue(row.getCell(9)));
        product.setImage3(getStringValue(row.getCell(10)));

//        product.setCommissionEarned(getDoubleValue(row.getCell(25)));

        return product;
    }

    private String getStringValue(Cell cell) {
        if (cell != null) {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            return cell.toString();
        }
        return null;
    }

    private Double getDoubleValue(Cell cell) {
        if (cell != null) {
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
            return cell.getNumericCellValue();
        }
        return null;
    }

}
