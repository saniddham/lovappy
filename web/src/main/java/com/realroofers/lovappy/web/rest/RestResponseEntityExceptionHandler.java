package com.realroofers.lovappy.web.rest;

import com.realroofers.lovappy.service.exception.EntityValidationException;
import com.realroofers.lovappy.service.exception.GenericServiceException;
import com.realroofers.lovappy.service.exception.UnauthorizedUserException;
import com.realroofers.lovappy.service.validation.ResponseStatus;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.web.exception.*;
import com.realroofers.lovappy.web.rest.dto.ErrorStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author Eias Altawil
 */

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

    @ExceptionHandler(value = { UnauthorizedUserException.class})
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected ResponseEntity<Object> handleUnauthorizedUserException(RuntimeException ex, WebRequest request) {

        JsonResponse res = new JsonResponse();
        res.setCause(ex.getMessage());
        res.setStatus(ResponseStatus.FAIL);

        return handleExceptionInternal(ex, res, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }

    @ExceptionHandler(value = { AuthenticationException.class})
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ResponseEntity<Object> handleAuthenticationException(AuthenticationException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorStatus(HttpStatus.UNAUTHORIZED.toString(), ex.getLocalizedMessage()), new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }


    @ExceptionHandler(value = { ValidationException.class})
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handleEntityValidationException(ValidationException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorStatus(HttpStatus.BAD_REQUEST.toString(), ex.getLocalizedMessage(), ex.getErrorMessages()), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
    @ExceptionHandler(value = { EntityValidationException.class})
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handleEntityValidationException(EntityValidationException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getErrorMessages(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
    @ExceptionHandler(value = { GenericServiceException.class})
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<Object> handleGenericServiceException(GenericServiceException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString(), ex.getLocalizedMessage()), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
    @ExceptionHandler(value = { DuplicateUserEmailException.class})
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<Object> handleDuplicateUserEmailException(DuplicateUserEmailException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorStatus(HttpStatus.CONFLICT.toString(), ex.getLocalizedMessage()), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @ExceptionHandler(value = { InvalidGrantException.class})
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handleInvalidGrantException(DuplicateUserEmailException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorStatus(HttpStatus.BAD_REQUEST.toString(), ex.getLocalizedMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = { ResourceNotFoundException.class})
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<Object> handleResourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorStatus(HttpStatus.NOT_FOUND.toString(), ex.getLocalizedMessage()), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = { BadRequestException.class})
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handleBadRequestException(BadRequestException ex, WebRequest request) {
        LOGGER.error("bad request exception " + ex.getLocalizedMessage());
        return handleExceptionInternal(ex, new ErrorStatus(HttpStatus.BAD_REQUEST.toString(), ex.getLocalizedMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = { AccessDeniedException.class})
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.FORBIDDEN)
    public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorStatus(HttpStatus.FORBIDDEN.toString(), ex.getLocalizedMessage()), new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

}