package com.realroofers.lovappy.web.controller.news;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.news.NewsCategoryService;
import com.realroofers.lovappy.service.news.NewsStoryService;
import com.realroofers.lovappy.service.news.dto.NewsDto;
import com.realroofers.lovappy.service.news.dto.NewsStoryAdminDto;
import com.realroofers.lovappy.service.news.dto.NewsStoryDto;
import com.realroofers.lovappy.service.news.model.NewsCategory;
import com.realroofers.lovappy.service.news.model.NewsStory;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/lox/news/admin")
public class NewsAdminRestController {
    private static Logger LOG = LoggerFactory.getLogger(NewsSubmitController.class);

    @Autowired
    private CloudStorage cloudStorage;

    @Autowired
    private NewsStoryService newsStoryService;

    @Autowired
    private NewsCategoryService newsCategoryService;


    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;
//
//    @GetMapping(value = "/allNews")
//    public List<NewsStoryAdminDto> getAllNewsStories(){
//        return newsStoryService.getAllNewsStoryAdminDto();
//    }
//

    @GetMapping(value = "/allNews")
    public ResponseEntity<List<NewsDto>> getAllNewsStories(){
        return new ResponseEntity<>(newsStoryService.getAllNewsStories(),HttpStatus.OK);
    }

    @GetMapping(value = "/get/{id}")
    public NewsStoryAdminDto getNewsStoryById(@PathVariable Long id){

        return newsStoryService.getNewsStoryAdminDtoById(id);

    }


    @RequestMapping(value = { "/search" }, method = RequestMethod.GET)
    public Map<String, Object> getSearchedNewsStories(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        String txtSearch = request.getParameter("txtSearch");
        resultMap.put("newsStories", newsStoryService.getAllNewsStoryAdminDtoByTitleAndContain(txtSearch));
        return resultMap;
    }
//
//    @RequestMapping(value = { "/asynchronous/search" }, method = RequestMethod.GET)
//    public Map<String, Object> getSearchedNewsStories(HttpServletRequest request) {
//        Map<String, Object> resultMap = new HashMap<String, Object>();
//        String txtSearch = request.getParameter("txtSearch");
//        resultMap.put("newsStories", newsStoryService.getAllNewsStoryAdminDtoByTitleAndContain(txtSearch));
//        return resultMap;
//    }

    @GetMapping(value = "/asynchronous/drop-downs")
    public Map<String, Object> getDropDowns() {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("newsCategories", newsCategoryService.getActiveNewsCategory());
        return resultMap;
    }

    @GetMapping(value = "/approval/{id}/{status}")
    public void newsApproval(@PathVariable long id,@PathVariable boolean status) {
        newsStoryService.newsApproval(id,status);
    }


    @RequestMapping(value = { "/asynchronous/new" }, method = RequestMethod.POST ,consumes = {"multipart/form-data"})
    public ResponseEntity<Void>  createNews(@RequestPart("dto")  String json, BindingResult bindingResult,
                                            @RequestParam(value = "videoUploadFile", required = false) MultipartFile videoUploadFile,@RequestParam("newsPicture") MultipartFile newsPicture,@RequestParam(value = "newsAuthorPicture", required = false) MultipartFile newsAuthorPicture) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        NewsStoryDto newsStoryDto=objectMapper.readValue(json, NewsStoryDto.class);
        //    newsStoryDto.setNewsPicture(newsPicture);
        //   newsStoryDto.setNewsAuthorPicture(newsAuthorPicture);
        //NewsStoryDto newsStoryDto=new NewsStoryDto();

        newsStoryDto.setSource("lovappy news submits");

        CloudStorageFileDto newsImage = null;
        CloudStorageFileDto userImage = null;
        CloudStorageFileDto video = null;
        try {
            if (videoUploadFile != null) {
                video = cloudStorage.uploadAndPersistFile(videoUploadFile, imagesBucket, FileExtension.wav.toString(), videoUploadFile.getContentType());
                newsStoryDto.setVideoUploadFile(videoUploadFile);
                newsStoryDto.setNewsStoryAudioVideoUrl(videoUploadFile.getOriginalFilename());
                newsStoryDto.setVideoName(videoUploadFile.getOriginalFilename());
            }

            if (newsAuthorPicture != null)
                userImage = cloudStorage.uploadAndPersistFile(newsAuthorPicture, imagesBucket, FileExtension.jpg.toString(), newsAuthorPicture.getContentType());
            if (newsPicture != null) {
                newsImage = cloudStorage.uploadAndPersistFile(newsPicture, imagesBucket, FileExtension.jpg.toString(), newsPicture.getContentType());
            }

            NewsStory newsStory = newsStoryService.saveNewsStory(newsStoryDto);
            if (newsImage != null) {
                newsStory.setNewsImage(new CloudStorageFile(newsImage.getId()));
            }
            if (userImage != null) {
                newsStory.setNewsUserIdentification(new CloudStorageFile(userImage.getId()));
            }
            if (video != null) {
                newsStory.setNewsVideo(new CloudStorageFile(video.getId()));
            }

            newsStoryService.saveNewsStory(newsStory);

        } catch (IOException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }


     /*   try {
            newsStoryService.saveNewsStory(newsStoryDto);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        /*
        try {
            String[] output = newsStoryDto.getAdvancedOptionKeywordPhrases().split("\\,");
            if (output.length > 2) {
                bindingResult.rejectValue("advancedOptionKeywordPhrases", "error.newsStoryDto", "Max keyword phrases are 2");
            }
            if (null != newsStoryService.getNewsStoryByTitle(newsStoryDto.getNewsStoryTitle())) {
                //System.out.println("title error--");
               // bindingResult.rejectValue("newsStoryTitle", "error.newsStoryDto", "Change the title , Same Title founded");
            }
        } catch (NullPointerException ee) {
//            LOG.error(ee.getMessage());
        }
        if (bindingResult.hasErrors()) {
            bindingResult.rejectValue("newsPicture", "error.newsStoryDto", "upload an image with your post");
            bindingResult.rejectValue("newsAuthorPicture", "error.newsStoryDto", "upload an identification image");
            Map<String, String> errors = bindingResult.getFieldErrors().stream()
                    .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
            resultMap.put("errors", errors);
        } else {
            newsStoryDto.setVideoName(null);
            newsStoryDto.setSource("lovappy news submits");
            try {
                newsStoryService.saveNewsStory(newsStoryDto);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //System.out.println(newsStoryDto);
        }*/
        return  new ResponseEntity<Void>(HttpStatus.OK);
    }
    @RequestMapping(value = { "/category/save" }, method = RequestMethod.POST,consumes = "application/json")
    public ResponseEntity<NewsCategory> save(@RequestBody NewsCategory category) {
        NewsCategory savedCategory=newsCategoryService.save(category);
        return  new ResponseEntity<NewsCategory>(savedCategory, HttpStatus.OK);
    }
    @RequestMapping(value = { "/category/check/{name}" }, method = RequestMethod.GET,produces = "application/json")
    public ResponseEntity<NewsCategory> checkExistCategory(@PathVariable String name) {
        NewsCategory savedCategory=newsCategoryService.getNewsCategoryByName(name);
        return  new ResponseEntity<NewsCategory>(savedCategory, HttpStatus.OK);
    }


    @RequestMapping(value = { "/category/all" }, method = RequestMethod.GET,produces = "application/json")
    public ResponseEntity<List<NewsCategory>> getAllCategoris() {
        List<NewsCategory> list=  newsCategoryService.getAllNewsCategory();
        return  new ResponseEntity<List<NewsCategory>>(list, HttpStatus.OK);
    }

    @RequestMapping(value = { "/category/status/{id}/{status}" }, method = RequestMethod.GET)
    public ResponseEntity<NewsCategory> updateStatus(@PathVariable Long id,@PathVariable int status) {

        NewsCategory category=newsCategoryService.getOne(id);
        category.setStatus(status);
        NewsCategory savedCategory=newsCategoryService.save(category);
        return  new ResponseEntity<NewsCategory>(savedCategory, HttpStatus.OK);
    }
    @RequestMapping(value = { "/title/check/{title}" }, method = RequestMethod.GET,produces = "application/json")
    public ResponseEntity<Boolean> checkExistNewsTitle(@PathVariable String title) {
        NewsStory story=newsStoryService.getNewsStoryByTitle(title);
        Boolean result=false;
        if(story != null){
            result =true;
        }
        return  new ResponseEntity<Boolean>(result, HttpStatus.OK);
    }

}