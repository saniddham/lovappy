package com.realroofers.lovappy.web.rest.v1;

import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.product.ProductService;
import com.realroofers.lovappy.service.product.dto.ProductDto;
import com.realroofers.lovappy.service.product.support.Upload;
import com.realroofers.lovappy.service.user.CountryService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.ChangePasswordRequest;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.FormValidator;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.service.validation.ResponseStatus;
import com.realroofers.lovappy.service.vendor.VendorLocationService;
import com.realroofers.lovappy.service.vendor.VendorService;
import com.realroofers.lovappy.service.vendor.dto.VendorDto;
import com.realroofers.lovappy.service.vendor.model.VendorLocation;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.ExcelReader;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Manoj on 04/02/2018.
 */
@RestController
@RequestMapping({"/api/v1/vendor", "/api/v2/vendor"})
public class RestVendorController {

    private static String[] locationColumns = {"Store ID", "Store Manager", "Store Manager Contact Phone",
            "Assistant Manager", "Assistant Manager Contact Phone", "Address Line 1", "Address Line 2", "City	State",
            "Zip Code", "Country", "Location Phone", "Open From", "Open To"};

    private final VendorLocationService vendorLocationService;
    private final UserService userService;
    private final FormValidator formValidator;
    private final CountryService countryService;
    private final VendorService vendorService;
    private final CloudStorage cloudStorage;
    private final CloudStorageService cloudStorageService;
    private final ProductService giftService;


    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @Autowired
    public RestVendorController(VendorLocationService vendorLocationService,
                                UserService userService, FormValidator formValidator,
                                CountryService countryService, VendorService vendorService, CloudStorage cloudStorage, CloudStorageService cloudStorageService, ProductService giftService) {
        this.vendorLocationService = vendorLocationService;
        this.userService = userService;
        this.formValidator = formValidator;
        this.countryService = countryService;
        this.vendorService = vendorService;
        this.cloudStorage = cloudStorage;
        this.cloudStorageService = cloudStorageService;
        this.giftService = giftService;
    }


    @RequestMapping(value = "/add/location", method = RequestMethod.POST)
    public ResponseEntity<Object> add(@ModelAttribute @Valid VendorLocation vendorLocation) {

        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = formValidator.validate(vendorLocation, new Class[]{Upload.class});
        VendorLocation vendorLocation1;
        if (errorMessages.size() > 0) {
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(500).body(res);
        }
        try {
            final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

            vendorLocation.setCreatedBy(new User(userId));
            vendorLocation1 = vendorLocationService.create(vendorLocation);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.status(200).body(vendorLocation1);
    }


    @RequestMapping(value = "/update/location", method = RequestMethod.POST)
    public ResponseEntity<Object> update(@ModelAttribute @Valid VendorLocation vendorLocation) {

        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = formValidator.validate(vendorLocation, new Class[]{Upload.class});
        VendorLocation vendorLocation1;
        if (errorMessages.size() > 0) {
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(500).body(res);
        }
        try {
            vendorLocation.setUpdated(new Date());
            vendorLocation1 = vendorLocationService.update(vendorLocation);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.status(200).body(vendorLocation1);
    }


    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseEntity<Object> update(@Valid VendorDto vendor) {

        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = formValidator.validate(vendor, new Class[]{Upload.class});
        VendorDto vendorDto;
        if (errorMessages.size() > 0) {
            res.setErrorMessageList(errorMessages);
            res.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(500).body(res);
        }
        try {
            VendorDto vendorDto1 = vendorService.findById(vendor.getId());
            if (vendorDto1 != null) {
                vendor.setUser(vendorDto1.getUser());
                vendorDto = vendorService.update(vendor);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.status(200).body(vendorDto);
    }

    @RequestMapping(value = "/upload/location", method = RequestMethod.POST)
    public ResponseEntity uploadExcel(@RequestParam("file") MultipartFile excelFile) {

        ExcelReader excelReader = new ExcelReader();
        try {
            List<VendorLocation> vendorLocationList = excelReader.getVendorLocation(excelFile.getInputStream());
            if (vendorLocationList.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            JsonResponse res = new JsonResponse();
            List<ErrorMessage> errorMessages = new ArrayList<>();

            for (VendorLocation vendorLocation : vendorLocationList) {
                errorMessages = formValidator.validateExcel(vendorLocationList.indexOf(vendorLocation), vendorLocation, new Class[]{Upload.class});
            }

            if (errorMessages.size() > 0) {
                res.setErrorMessageList(errorMessages);
                res.setStatus(ResponseStatus.FAIL);
                return ResponseEntity.status(500).body(res);
            }

            final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
            final User user = userService.getUserById(userId);

            vendorLocationList.stream().forEach(p -> {
                p.setCreatedBy(user);
                try {
                    vendorLocationService.create(p);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/download/location", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Resource> downloadFile() throws IOException {

        Workbook excel = createLocationExcel();

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        excel.write(buffer);
        byte[] bytes = buffer.toByteArray();
        InputStream inputStream = new ByteArrayInputStream(bytes);
        InputStreamResource resource = new InputStreamResource(inputStream);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=Lovappy-Vendor-Location-Upload.xlsx");

        return ResponseEntity.ok()
                .headers(headers)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(resource);
    }

    public Workbook createLocationExcel() {
        // Create a Workbook
        Workbook workbook = new HSSFWorkbook();

        // Create a Sheet
        Sheet sheet = workbook.createSheet("Location");

        // Create a Row
        Row headerRow = sheet.createRow(0);

        // Creating cells
        for (int i = 0; i < locationColumns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(locationColumns[i]);
        }

        String[] countryNameList = countryService.getCountryNameList();
        Sheet hidden = workbook.createSheet("hidden");
        for (int i = 0, length = countryNameList.length; i < length; i++) {
            String name = countryNameList[i];
            Row row = hidden.createRow(i);
            Cell cell = row.createCell(11);
            cell.setCellValue(name);
        }
        Name namedCell = workbook.createName();
        namedCell.setNameName("hidden");
        namedCell.setRefersToFormula("hidden!A1:A" + countryNameList.length);
        DVConstraint constraint = DVConstraint.createFormulaListConstraint("hidden");
        CellRangeAddressList addressList = new CellRangeAddressList(1, 100000, 11, 11);
        DataValidation dataValidation = new HSSFDataValidation(addressList, constraint);
        dataValidation.setSuppressDropDownArrow(false);
        workbook.setSheetHidden(1, true);
        sheet.addValidationData(dataValidation);

        // Resize all columns to fit the content size
        for (int i = 0; i < locationColumns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        return workbook;
    }

    @RequestMapping(value = "/photo/upload", method = RequestMethod.POST)
    public ResponseEntity<Void> uploadProfilePhotoWithoutThumbnails(@RequestParam("file") MultipartFile imageFile) {
        try {
            CloudStorageFileDto coverFileDto = cloudStorage.uploadFile(imageFile, imagesBucket, FilenameUtils.getExtension(imageFile.getOriginalFilename()), imageFile.getContentType());
            CloudStorageFileDto added = cloudStorageService.add(coverFileDto);


            final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

            VendorDto vendor = vendorService.findByUser(userId);
            vendor.setCompanyLogo(added);
            vendorService.update(vendor);

            return ResponseEntity.ok().build();

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @GetMapping("/location/deactivate{id}")
    public ResponseEntity<VendorLocation> deactivate(@PathVariable("id") Integer id) {
        try {
            VendorLocation location = vendorLocationService.findById(id);
            if (location == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            location.setActive(false);
            VendorLocation updatedLocation = vendorLocationService.update(location);

            return new ResponseEntity<>(updatedLocation, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/location/activate{id}")
    public ResponseEntity<VendorLocation> activate(@PathVariable("id") Integer id) {
        try {
            VendorLocation location = vendorLocationService.findById(id);
            if (location == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            location.setActive(true);
            VendorLocation updatedLocation = vendorLocationService.update(location);

            return new ResponseEntity<>(updatedLocation, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/profile/password/update")
    public ResponseEntity<String> save(ChangePasswordRequest changePasswordRequest) {
        boolean isValid = false;

        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        if (userId != null) {
            isValid = userService.updateVendorPassword(userId, changePasswordRequest.getCurrentPassword(), changePasswordRequest.getNewPassword());
        }
        return isValid ? ResponseEntity.ok().body("Success") : ResponseEntity.badRequest().body("Current Password is not valid");
    }


    @RequestMapping(value = "/locations", method = RequestMethod.GET)
    public ResponseEntity<List<VendorLocation>> getRetailerLocations(@RequestParam("vendorId") Integer vendorId) {
        VendorDto vendorDto = vendorService.findById(vendorId);
        List<VendorLocation> giftLocations = vendorLocationService.findAllVendorLocations(vendorDto.getUser().getID());
        return new ResponseEntity<>(giftLocations, HttpStatus.OK);
    }


    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public ResponseEntity<VendorDto> mainRetailer(@RequestParam("gift_id") Integer giftId) {
        ProductDto byId = giftService.findById(giftId);
        if (byId != null) {
            VendorDto vendorDto = vendorService.findByUser(byId.getCreatedBy().getID());

            if (vendorDto != null) {
                return ResponseEntity.ok(vendorDto);
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }


    @RequestMapping(value = "/others", method = RequestMethod.GET)
    public ResponseEntity<List<VendorDto>> otherRetailers(@RequestParam("gift_id") Integer giftId) {

        return ResponseEntity.ok(vendorService.findByGiftId(giftId));
    }


}
