package com.realroofers.lovappy.web.util;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.imgscalr.Scalr;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.TagException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Eias Altawil on 5/2/2017
 */

@Component
public class FileUtil {

    @Value("${max.image.width:640}")
    private int maxImageWidth;
    @Value("${max.image.height:480}")
    private int maxImageHeight;

    // MIMETYPES
    public static final String MIMETYPE_BPM = "image/bpm";
    public static final String MIMETYPE_GIF = "image/gif";
    public static final String MIMETYPE_JPEG = "image/jpeg";
    public static final String MIMETYPE_PNG = "image/png";

    public static Map<String, String> MIMETYPE_EXTENSION_MAPPING;
    static {
        Map<String, String> mapping = new HashMap<String, String>();
        mapping.put(MIMETYPE_BPM, "bpm");
        mapping.put(MIMETYPE_GIF, "gif");
        mapping.put(MIMETYPE_JPEG, "jpg");
        mapping.put(MIMETYPE_PNG, "png");
        MIMETYPE_EXTENSION_MAPPING = Collections.unmodifiableMap(mapping);
    }

    private static Logger LOG = LoggerFactory.getLogger(FileUtil.class);

    public enum FileExtension {webm,wav}


    /**
     * Resizes the given image (passed as {@link InputStream}.
     * If the image is smaller then the given maximum width or height, the image
     * will be proportionally resized.
     */
    public byte [] smallify(InputStream imageInputStream, String mimeType, int maxWidth, int maxHeight) {
        try {
            BufferedImage image = ImageIO.read(imageInputStream);

            int width = Math.min(image.getWidth(), maxWidth);
            int height = Math.min(image.getHeight(), maxHeight);

            Scalr.Mode mode = Scalr.Mode.AUTOMATIC;
            if (image.getHeight() > maxHeight) {
                mode = Scalr.Mode.FIT_TO_HEIGHT;
            }

            if (width != image.getWidth() || height != image.getHeight()) {
                image = Scalr.resize(image, mode, width, height);
            }

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(image, MIMETYPE_EXTENSION_MAPPING.get(mimeType), bos);
            return bos.toByteArray();
        } catch (IOException e) {
            LOG.error("Exception while resizing image", e);
            throw new RuntimeException(e);
        }
    }

    private boolean checkDirectory(String directory){
        File theDir = new File(directory);

        // if the directory does not exist, create it
        if (theDir.exists()) {
            return true;
        } else {
            try {
                return theDir.mkdirs();
            }
            catch(SecurityException se){
                return false;
            }
        }
    }

    public byte[] spliteFile (MultipartFile musicFile){
        try     {

            FileInputStream in= (FileInputStream) musicFile.getInputStream();
            long i=musicFile.getSize();
            Integer songLength = getAudioDuration(musicFile);
            if(songLength > 60) {
                long j = (long) ((double) i * (60.0 / (double) songLength));
                return IOUtils.toByteArray(in, j);
            } else return musicFile.getBytes();
        }
        catch (IOException e)
        {

        }
      return null;
    }


    public Integer getAudioDuration(MultipartFile audioFile) {
        try {
            File file = File.createTempFile("lovappy-",
                    "." + FilenameUtils.getExtension(audioFile.getOriginalFilename()));

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(audioFile.getBytes());
            fos.close();

            AudioFile audio = AudioFileIO.read(file);
            LOG.error("length " + audio.getAudioHeader().getTrackLength());
            return audio.getAudioHeader().getTrackLength();

        } catch (CannotReadException | IOException | ReadOnlyFileException | TagException | InvalidAudioFrameException e) {
            LOG.error(e.getMessage());
        }

        return 0;
    }
}
