package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.music.MusicService;
import com.realroofers.lovappy.service.music.dto.GenreDto;
import com.realroofers.lovappy.service.music.dto.MusicDto;
import com.realroofers.lovappy.service.music.dto.MusicExchangeDto;
import com.realroofers.lovappy.service.music.support.MusicStatus;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.util.CommonUtils;
import com.realroofers.lovappy.web.util.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

/**
 * Created by Eias Altawil on 8/26/2017
 */

@Controller
@RequestMapping("/lox/music")
public class MusicAdminController {

    private final MusicService musicService;
    private final EmailTemplateService emailTemplateService;
    private final MailService mailService;
    private final ITemplateEngine templateEngine;

    @Autowired
    public MusicAdminController(MusicService musicService, EmailTemplateService emailTemplateService, MailService mailService, ITemplateEngine templateEngine) {
        this.musicService = musicService;
        this.emailTemplateService = emailTemplateService;
        this.mailService = mailService;
        this.templateEngine = templateEngine;
    }

    @GetMapping
    public ModelAndView music(ModelAndView modelAndView,
                              @RequestParam("pageSize") Optional<Integer> pageSize,
                              @RequestParam("page") Optional<Integer> pageNumber,
                              @RequestParam(value = "filter", defaultValue = "") String filter,
                              @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){

        PageRequest pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize));

        if(!StringUtils.isEmpty(filter)) {
            if(pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize), new Sort(sort, filter));
            }
        }
        modelAndView.addObject("filter", filter);
        modelAndView.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);

        Page<MusicDto> musicPage = musicService.getAll(pageable);
        modelAndView.addObject("musicPage", musicPage);
        Pager pager = new Pager(musicPage.getTotalPages(), musicPage.getNumber(), Pager.BUTTONS_TO_SHOW);

        modelAndView.addObject("selectedPageSize", Pager.evalPageSize(pageSize));
        modelAndView.addObject("pageSizes", Pager.PAGE_SIZES);
        modelAndView.addObject("pager", pager);
        modelAndView.setViewName("admin/music/music_list");
        return modelAndView;
    }

    @GetMapping("/{id}")
    public ModelAndView musicDetails(ModelAndView model, @PathVariable(value = "id") Integer id){
        model.addObject("music",  musicService.getMusic(id));
        model.setViewName("admin/music/music_details");
        return model;
    }

    @GetMapping("/exchange")
    public ModelAndView musicExchange(ModelAndView modelAndView,
                                      @RequestParam("pageSize") Optional<Integer> pageSize,
                                      @RequestParam("page") Optional<Integer> pageNumber,
                                      @RequestParam(value = "filter", defaultValue = "") String filter,
                                      @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){

        PageRequest pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize));

        if(!StringUtils.isEmpty(filter)) {
            if(pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize), new Sort(sort, filter));
            }
        }
        modelAndView.addObject("filter", filter);
        modelAndView.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);


        Page<MusicExchangeDto> musicExchangePage = musicService.getMusicExchange(pageable);
        modelAndView.addObject("musicExchangePage", musicExchangePage);
        Pager pager = new Pager(musicExchangePage.getTotalPages(), musicExchangePage.getNumber(), Pager.BUTTONS_TO_SHOW);

        modelAndView.addObject("selectedPageSize", Pager.evalPageSize(pageSize));
        modelAndView.addObject("pageSizes", Pager.PAGE_SIZES);
        modelAndView.addObject("pager", pager);
        modelAndView.setViewName("admin/music/music_exchange");
        return modelAndView;
    }

    @GetMapping("/genres")
    public ModelAndView genres(ModelAndView modelAndView){
        modelAndView.addObject("genres",  musicService.getAllGenres());
        modelAndView.setViewName("admin/music/genres");
        return modelAndView;
    }

    @GetMapping("/genres/new")
    public ModelAndView addNewGenre() {
        ModelAndView mav = new ModelAndView("admin/music/genre_details");
        mav.addObject("genre", new GenreDto());
        return mav;
    }

    @GetMapping("/genres/{id}")
    public ModelAndView genreDetails(@PathVariable(value = "id") Integer id) {
        ModelAndView mav = new ModelAndView("admin/music/genre_details");
        mav.addObject("genre", musicService.getGenre(id));
        return mav;
    }

    @PostMapping("/genres/save")
    public ResponseEntity saveGenre(@ModelAttribute("genre") @Valid GenreDto genre) {
        if(genre.getId() == null){
            GenreDto addGenre = musicService.addGenre(genre);
            return ResponseEntity.status(HttpStatus.OK).body(addGenre.getId());
        } else {
            GenreDto updateGenre = musicService.updateGenre(genre);
            return ResponseEntity.status(HttpStatus.OK).body(updateGenre.getId());
        }
    }

    @PostMapping("/{music-id}/approve")
    @RequestMapping(value = "/{music-id}/approve", method = RequestMethod.POST)
    public ResponseEntity approveMusic(HttpServletRequest request, @PathVariable("music-id") Integer id){
        MusicDto musicDto = musicService.updateStatus(id, MusicStatus.APPROVED);

        //send approval email
        EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.SUBMITTED_MUSIC_APPROVAL, "EN");
        emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/music/" + id+"/details");
        emailTemplate.getAdditionalInformation().put("title", musicDto.getTitle());
        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), musicDto.getAuthor().getEmail(), templateEngine,
                mailService, emailTemplate).send();
        return ResponseEntity.status(HttpStatus.OK).body("");
    }

    @RequestMapping(value = "/{music-id}/reject", method = RequestMethod.POST)
    public ResponseEntity rejectMusic(HttpServletRequest request, @PathVariable("music-id") Integer id){
        MusicDto musicDto = musicService.updateStatus(id, MusicStatus.REJECTED);

        //send denial email
        EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.SUBMITTED_MUSIC_DENIAL, "EN");
        emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request)+ "/music/" + id+"/details");
        emailTemplate.getAdditionalInformation().put("title", musicDto.getTitle());
        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), musicDto.getAuthor().getEmail(), templateEngine,
                mailService, emailTemplate).send();
        return ResponseEntity.status(HttpStatus.OK).body("");
    }


}
