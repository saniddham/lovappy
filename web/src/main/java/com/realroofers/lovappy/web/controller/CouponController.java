package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import com.realroofers.lovappy.service.order.OrderService;

import com.realroofers.lovappy.service.order.dto.CouponDto;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Daoud Shaheen on 6/10/2017.
 */
@Controller
@RequestMapping("/coupons")
public class CouponController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CouponController.class);

    private CMSService cmsService;
    private OrderService orderService;

    @Autowired
    public CouponController(CMSService cmsService, OrderService orderService) {
        this.cmsService = cmsService;
        this.orderService = orderService;
    }

    @GetMapping
    public String coupon(Model model) {
         return "coupon/more-coupons";
    }

    @GetMapping("/types")
    public ModelAndView types(ModelAndView model) {
    	//Get Coupons
        model.setViewName("coupon/coupon-types");
        List<CouponDto> allCoupons = orderService.getAllCoupons();
        String s =CouponCategory.GIFT.name();
        String ss =CouponCategory.GIFT.toString();

        model.addObject("coupons_gift", allCoupons.stream().filter(x -> x.getCategory().name().equals(CouponCategory.GIFT.name())).collect(Collectors.toList()));
        model.addObject("coupons_music", allCoupons.stream().filter(x -> x.getCategory().name().equals(CouponCategory.MUSIC.name())).collect(Collectors.toList()));
        model.addObject("coupons_message", allCoupons.stream().filter(x -> x.getCategory().name().equals(CouponCategory.MESSAGE.name())).collect(Collectors.toList()));
        model.addObject("coupons_ad", allCoupons.stream().filter(x -> x.getCategory().name().equals(CouponCategory.AD.name())).collect(Collectors.toList()));
        model.addObject("coupons_event", allCoupons.stream().filter(x -> x.getCategory().name().equals(CouponCategory.EVENT_ATTEND.name())).collect(Collectors.toList()));
       	model.addObject("listCoupons", allCoupons);
        return model;
    }
}
