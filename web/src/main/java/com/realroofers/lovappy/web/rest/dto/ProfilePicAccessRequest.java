package com.realroofers.lovappy.web.rest.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 1/19/2018.
 */
@Data
public class ProfilePicAccessRequest implements Serializable {
    private String access;
    private Integer maxTransactions;
}
