package com.realroofers.lovappy.web.rest.v1;

import java.io.IOException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.social.twitter.api.TweetData;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.gallery.GalleryStorageService;
import com.realroofers.lovappy.service.gallery.dto.GalleryStorageFileDto;
import com.realroofers.lovappy.service.user.model.embeddable.Address;
import com.realroofers.lovappy.service.util.AddressUtil;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;

/****
 * @author Japheth Odonya
 * */
@Controller
@RequestMapping({"/api/v1/gallery", "/api/v2/gallery"})
public class RestGalleryController {
	
    private static Logger LOG = LoggerFactory.getLogger(RestGalleryController.class);
	
    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    private final GalleryStorageService galleryStorageService;
    private final CloudStorage cloudStorage;
    private final TwitterTemplate twitterTemplate;
    private final AddressUtil addressUtil;

    @Autowired
    public RestGalleryController(GalleryStorageService galleryStorageService, CloudStorage cloudStorage, TwitterTemplate twitterTemplate, AddressUtil addressUtil) {
    	this.galleryStorageService = galleryStorageService;
        this.cloudStorage = cloudStorage;
        this.twitterTemplate  = twitterTemplate;
        this.addressUtil = addressUtil;
    }

	
    @RequestMapping(value = "/photo", method = RequestMethod.POST)
    public ResponseEntity<Void> saveCouplePhoto(@RequestParam("file") MultipartFile multipartFile, @RequestParam("zipCode") String zipCode, @RequestParam("photoCaption") String photoCaption) {
    	try {
            CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadImageWithThumbnails(multipartFile, imagesBucket,
                    FileExtension.jpg.toString(), "image/jpeg");
            Address address = addressUtil.extractAddress(zipCode);
            GalleryStorageFileDto  galleryStorageFileDto = new GalleryStorageFileDto(cloudStorageFileDto.getName(), cloudStorageFileDto.getUrl(), cloudStorageFileDto.getBucket(), photoCaption, zipCode, address);
            galleryStorageService.add(galleryStorageFileDto, address);
            UrlResource imageUrlResource = new UrlResource(cloudStorageFileDto.getUrl());
            
            if (address.getCity() == null) address.setCity("");
            if (address.getState() == null) address.setState("");
            if (address.getCountry() == null) address.setCountry("");
            if (photoCaption == null) photoCaption = "";
            
            TweetData tweetData = new TweetData(String.format("\"%s\", %s , %s , %s", photoCaption, address.getCity(), address.getState(), address.getCountry())).withMedia(imageUrlResource);
            twitterTemplate.timelineOperations().updateStatus(tweetData);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    	
    }

}
