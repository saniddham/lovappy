package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.gallery.GalleryStorageService;
import com.realroofers.lovappy.service.gallery.dto.GalleryStorageFileDto;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.web.util.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Created by Daoud Shaheen on 2/5/2018.
 */
@Controller
@RequestMapping("/lox/happy-couples")
public class GalleryController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GalleryController.class);

    private final GalleryStorageService galleryStorageService;

    public GalleryController(GalleryStorageService galleryStorageService) {
        this.galleryStorageService = galleryStorageService;
    }

    @GetMapping
    public ModelAndView getAll(ModelAndView modelAndView,
                               @RequestParam("pageSize") Optional<Integer> pageSize,
                               @RequestParam("page") Optional<Integer> pageNumber,
                               @RequestParam(value = "filter", defaultValue = "") String filter,
                               @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){

        PageRequest pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize));

        if(!StringUtils.isEmpty(filter)) {
            if(pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize), new Sort(sort, filter));
            }
        }
        modelAndView.addObject("filter", filter);
        modelAndView.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);

        Page<GalleryStorageFileDto> galleryPage = galleryStorageService.getAllPhotos(pageable);
        modelAndView.addObject("galleryPage", galleryPage);
        Pager pager = new Pager(galleryPage.getTotalPages(), galleryPage.getNumber(), Pager.BUTTONS_TO_SHOW);
        modelAndView.addObject("selectedPageSize", Pager.evalPageSize(pageSize));
        modelAndView.addObject("pageSizes", Pager.PAGE_SIZES);
        modelAndView.addObject("pager", pager);
        modelAndView.setViewName("admin/happy_couples/happy-couples-list");
        return modelAndView;
    }


    @PostMapping("/{fileId}/active")
    public ResponseEntity showInHome(HttpServletRequest request, @PathVariable("fileId") Integer id) {
        galleryStorageService.setActive(id, ApprovalStatus.APPROVED);
        return ResponseEntity.status(HttpStatus.OK).body("");
    }

    @DeleteMapping("/{fileId}/active")
    public ResponseEntity hideFromHome(HttpServletRequest request, @PathVariable("fileId") Integer id) {
        galleryStorageService.setActive(id, ApprovalStatus.REJECTED);
        return ResponseEntity.status(HttpStatus.OK).body("");
    }
}
