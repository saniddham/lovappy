package com.realroofers.lovappy.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.model.TextTranslation;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.util.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionKey;
import org.springframework.social.oauth1.AuthorizedRequestToken;
import org.springframework.social.oauth1.OAuth1Operations;
import org.springframework.social.oauth1.OAuth1Parameters;
import org.springframework.social.oauth1.OAuthToken;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.TwitterProfile;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;

import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.dto.SetNewPasswordForm;

@Controller
public class LoginController {
    private UserService userService;
    private ITemplateEngine templateEngine;
    private MailService mailService;
    private CMSService cmsService;
    private EmailTemplateService emailTemplateService;
    private static Logger LOG = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    public LoginController(EmailTemplateService emailTemplateService, UserService userService, MailService mailService, List<ViewResolver> viewResolvers, CMSService cmsService) {
        this.userService = userService;
        this.mailService = mailService;
        this.emailTemplateService = emailTemplateService;
        for (ViewResolver viewResolver : viewResolvers) {
            if (viewResolver instanceof ThymeleafViewResolver) {
                ThymeleafViewResolver thymeleafViewResolver = (ThymeleafViewResolver) viewResolver;
                this.templateEngine = thymeleafViewResolver.getTemplateEngine();
            }
        }
        if (this.templateEngine == null) {
            throw new BeanInitializationException("No view resolver of type ThymeleafViewResolver found.");
        }
        this.cmsService = cmsService;
    }

    @GetMapping("/login")
    public ModelAndView login(@RequestParam(value = "error", defaultValue = "") String error) {
      ModelAndView model = new ModelAndView("login");
        if(!StringUtils.isEmpty(error)) {
            model.addObject("error", true);
            TextTranslation errorDetails = cmsService.getTextTranslationByTagAndLanguage(error, "EN");
            model.addObject("error_title", errorDetails.getTitle());
            model.addObject("error_text", errorDetails.getContent());
        }

        model.addObject(PageConstants.MAIN_LOGIN_BG, cmsService.getImageUrlByTag(PageConstants.MAIN_LOGIN_BG));
        return model;
    }

    @GetMapping("/reset")
    public String passwordReset(Model model) {
        model.addAttribute(PageConstants.FORGOT_PASSWORD_DESCRIPTION, cmsService.getTextByTagAndLanguage(PageConstants.FORGOT_PASSWORD_DESCRIPTION, "EN"));
        return "reset";
    }

    @GetMapping("/couples_reset_password")
    public String couplesPasswordReset(Model model) {
        return "couples_reset_password";
    }


    @PostMapping("/send_reset_email")
    public ModelAndView passwordReset( HttpServletRequest request, String email, RedirectAttributes redirectAttributes,
                                      @RequestParam(value = "lang", defaultValue = "EN") String language) {

        UserDto user = userService.getUser(email);
        if(user != null) {
            EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.RESET_PASSWORD, language);
            new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), email, templateEngine,
                    mailService, emailTemplate).send();

            redirectAttributes.addFlashAttribute("popup_msg", "FORGOT PASSWORD");
            redirectAttributes.addFlashAttribute("popup_msg_title", "Password Reset");
            redirectAttributes.addFlashAttribute("popup_msg_text", "A password reset email has been sent, check your email please.");
            return new ModelAndView("redirect:/");
        } else {
            ModelAndView modelAndView = new ModelAndView("reset");
            modelAndView.addObject("error", user!=null ? "User is registered using social media" : "User is not found");
            modelAndView.addObject(PageConstants.FORGOT_PASSWORD_DESCRIPTION, cmsService.getTextByTagAndLanguage(PageConstants.FORGOT_PASSWORD_DESCRIPTION, "EN"));

            return modelAndView;
        }
    }

    @GetMapping("/set_new_password/{token}")
    public ModelAndView setNewPasswordPage(@PathVariable("token") String token) {
        ModelAndView mav = new ModelAndView();
        mav.addObject("setNewPasswordForm", new SetNewPasswordForm(token));
        mav.setViewName("set_new_password");
        return mav;
    }

    @PostMapping("/set_new_password")
    public ModelAndView setNewPassword(HttpServletRequest request,
            @Valid SetNewPasswordForm setNewPasswordForm, BindingResult bindingResult
       ,  @RequestParam(value = "lang", defaultValue = "EN") String language){
        if (bindingResult.hasErrors())
            return new ModelAndView("set_new_password");

        UserDto user = userService.setNewPassword(setNewPasswordForm.getToken(), setNewPasswordForm.getPassword());
        EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.NEW_PASSWORD_CONFIRMATION, language);
        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), user.getEmail(), templateEngine,
                mailService, emailTemplate).send();

       return new ModelAndView("redirect:/login");
    }

    @GetMapping("/error/403")
    public String notAuthorized() {
        return "/error/403";
    }

}
