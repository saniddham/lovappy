package com.realroofers.lovappy.web.rest.dto;

import com.realroofers.lovappy.service.radio.dto.RadioDto;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 1/12/2018.
 */
@Data
public class RadioResponse implements Serializable{
    private String message;

    private Boolean emptyResult;

    private Boolean showMessage;

    private Page<RadioDto> radioPage;
}
