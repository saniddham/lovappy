package com.realroofers.lovappy.web.config.social.instagram.api;

import java.util.List;


/**
 * Interface defining the operations for retrieving information about Instagram users and
 * managing relationships amongst the authenticated user.
 */
public interface UserOperations {
	
	/**
	 * Get basic information about the authenticated user
	 * @return Instagram profile
	 */
	InstagramProfile getUser();
	
	/**
	 * Get basic information about a specific user
	 * @param userId
	 * @return	Instagram profile
	 */
	InstagramProfile getUser(long userId);
	

	String USERS_ENDPOINT = "users/";
}
