package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import com.realroofers.lovappy.service.music.MusicService;
import com.realroofers.lovappy.service.music.dto.*;
import com.realroofers.lovappy.service.music.model.MusicExchange;
import com.realroofers.lovappy.service.music.support.MusicStatus;
import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.ProfilePicsDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.web.util.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

/**
 * Created by Eias Altawil on 8/25/17
 */

@Controller
@RequestMapping("/music")
public class MusicController {
    private static Logger LOGGER = LoggerFactory.getLogger(MusicController.class);
    @Autowired
    private RequestCache requestCache;
    private final MusicService musicService;
    private final UserProfileService userProfileService;
    private CMSService cmsService;

    @Value("${square.applicationId}")
    private String squareAppID;

    @Autowired
    public MusicController(MusicService musicService, UserProfileService userProfileService, CMSService cmsService) {
        this.musicService = musicService;
        this.userProfileService = userProfileService;
        this.cmsService = cmsService;
    }

    @GetMapping
    public ModelAndView browse(ModelAndView mav, @RequestParam("page") Optional<Integer> pageNumber,
                               @RequestParam(value = "lang", defaultValue = "EN") String language,
                               @RequestParam(value = "genreId", defaultValue = "0") Integer genreId) {
        mav.setViewName("music/browse");

        Locale locale = LocaleContextHolder.getLocale();
        language = locale.getLanguage().toUpperCase();
        PageRequest pageRequest = new PageRequest(Pager.evalPageNumber(pageNumber), 12);

        mav.addObject(PageConstants.MUSIC_PAGE_TITLE, cmsService.getTextByTagAndLanguage(PageConstants.MUSIC_PAGE_TITLE, language).replace("\\n", "<br>"));
        mav.addObject(PageConstants.MUSIC_PAGE_SUB_TITLE_A, cmsService.getTextByTagAndLanguage(PageConstants.MUSIC_PAGE_SUB_TITLE_A, language).replace("\\n", "<br>"));
        mav.addObject(PageConstants.MUSIC_PAGE_SUB_TITLE_B, cmsService.getTextByTagAndLanguage(PageConstants.MUSIC_PAGE_SUB_TITLE_B, language).replace("\\n", "<br>"));
        mav.addObject("Music", musicService.getAllApproved(pageRequest));

        mav.addObject("genreId", genreId);

        mav.addObject("MostPurchased", musicService.getMostPopularPurchased(pageRequest));
        mav.addObject("RecentlyPurchased", musicService.getRecentlyPurchased(pageRequest));
        List<GenreDto> genres = musicService.getAllEnabledGenres();
        GenreDto genreDto = new GenreDto();
        genreDto.setTitle("Select a Genre");
        genreDto.setId(0);
        if (genres.isEmpty()) {
            genres.add(genreDto);
        } else {
            genres.add(0, genreDto);
        }
        mav.addObject("genres", genres);
        mav.addObject("searchText", "");
        mav.addObject("filter", "");

        return mav;
    }

    @GetMapping("/{id}/details")
    public ModelAndView details(@PathVariable(value = "id") Integer id,
                                HttpServletRequest request,
                                HttpServletResponse response,
                                ModelAndView mav,
                                @RequestParam(value = "lang", defaultValue = "EN") String language) {

        Locale locale = LocaleContextHolder.getLocale();
        language = locale.getLanguage().toUpperCase();
        MusicDto music = musicService.getMusic(id);

        if (music == null) {
            mav.setViewName("error/404");
            return mav;
        }

        if (!music.getAuthor().getID().equals(UserUtil.INSTANCE.getCurrentUserId()) && !music.getStatus().equals(MusicStatus.APPROVED)) {
            mav.setViewName("error/403");
            return mav;
        }

        boolean hideDownload = true;
        boolean showSendMusic = false;
        boolean showNoteLimit = false;
        boolean showBuyLink = true;
        boolean showRecievedSong = false;
        boolean owner = false;
        if (UserUtil.INSTANCE.getCurrentUser() != null) {
            MusicUserDto musicUserDto = musicService.getMusicUserDetails(id, UserUtil.INSTANCE.getCurrentUserId());
            if (musicUserDto != null) {
                showRecievedSong = musicUserDto.getReceived();
            }
            if (music.getAuthor().getID().equals(UserUtil.INSTANCE.getCurrentUserId())) {
                owner = true;
                showSendMusic = true;
                showBuyLink = false;
                hideDownload = false;

            } else {

                if (musicUserDto != null) {

                    music.setAlreadyRated(musicUserDto.getRating() > -1);
                    //Has user bought this song already?
                    if (musicUserDto.getPurchased() || musicUserDto.getReceived()) {
                        showSendMusic = true;
                        showBuyLink = false;
                        //Has user reached download limit? 2 times
                        //later add it for mobile also
                        if (musicUserDto.getWebDownloads() + musicUserDto.getMobileDownloads() < 2) {
                            hideDownload = false;
                        } else {
                            showNoteLimit = true;
                            showBuyLink = true;
                        }

                    }

                }
            }

            if (showRecievedSong) {
                Page<MusicExchangeDto> musicExchangeDtos = musicService.getMusicExchangeByToUser(UserUtil.INSTANCE.getCurrentUserId(), new PageRequest(0, 1, Sort.Direction.DESC, "exchangeDate"));
                if (musicExchangeDtos.getContent().size() > 0)
                    mav.addObject("lyricsReceived", musicExchangeDtos.getContent().get(0).getRevceivedLyrics());
                LOGGER.info("Received " + musicExchangeDtos.getContent().get(0).getRevceivedLyrics());
            }

        } else { //cache the request
            requestCache.saveRequest(request, response);
        }

        Integer musicExchange = -1;
        Object sendToUserSession = request.getSession().getAttribute("send-music-to");
        if (sendToUserSession != null) {
            Integer sendToUser = Integer.parseInt(String.valueOf(request.getSession().getAttribute("send-music-to")));
            musicExchange = musicService.countMusicSentBetweenFromAndToUserAndMusic(UserUtil.INSTANCE.getCurrentUserId(), sendToUser, id);
            LOGGER.debug("send music to user {} music exchange {}", sendToUserSession, musicExchange);
            showBuyLink = showBuyLink && showSendMusic && musicExchange == 0;
            hideDownload = true;
        }

        mav.addObject("sendToUser", sendToUserSession);
        mav.addObject("music", music);
        mav.addObject("isOwner", owner);
        mav.addObject("squareAppID", squareAppID);
        mav.addObject("hideDownload", hideDownload);
        mav.addObject("showBuyLink", showBuyLink);
        mav.addObject("showSendBuyLink", !showSendMusic && musicExchange == 0);
        mav.addObject("showAlreadySendMsg", musicExchange > 0);
        mav.addObject("showSendMusic", showSendMusic && musicExchange == 0);
        mav.addObject("showNoteLimit", showNoteLimit);
        mav.addObject("showReceivedMusic", showRecievedSong);

        mav.addObject("searchText", "");
        mav.addObject("filter", "");
        mav.setViewName("music/details");
        mav.addObject("music", music);

        return mav;
    }

    @GetMapping("/{id}/inbox-details")
    public ModelAndView receivedDetails(@PathVariable(value = "id") Integer musicExchangeId,
                                        @RequestParam("userId") Integer sendBy,
                                        HttpServletRequest request,
                                        HttpServletResponse response,
                                        ModelAndView mav,
                                        @RequestParam(value = "lang", defaultValue = "EN") String language) {

        return getMusicTransactions(musicExchangeId, sendBy, request, response, mav, true);
    }

    @GetMapping("/{id}/outbox-details")
    public ModelAndView sentDetails(@PathVariable(value = "id") Integer musicExchangeId,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    ModelAndView mav,
                                    @RequestParam(value = "lang", defaultValue = "EN") String language) {

        return getMusicTransactions(musicExchangeId, null, request, response, mav, false);
    }

    private ModelAndView getMusicTransactions(Integer musicExchangeId,
                                              Integer sendBy, HttpServletRequest request,
                                              HttpServletResponse response, ModelAndView mav, boolean isInbox) {
        MusicExchange musicExchangeObj = musicService.getMusicExchange(musicExchangeId);

        if (musicExchangeObj == null) {
            mav.setViewName("error/404");
            return mav;
        }
        Integer musicId = musicExchangeObj.getId();
        if (sendBy != null)
            musicService.setMusicNotificationSeen(UserUtil.INSTANCE.getCurrentUserId(), sendBy, musicId);

        MusicDto music = musicService.getMusic(musicId);

        if (music == null) {
            mav.setViewName("error/404");
            return mav;
        }

        boolean hideDownload = true;
        boolean showSendMusic = false;
        boolean showNoteLimit = false;
        boolean showBuyLink = true;
        boolean showRecievedSong = false;
        boolean owner = false;
        if (UserUtil.INSTANCE.getCurrentUser() != null) {
            MusicUserDto musicUserDto = musicService.getMusicUserDetails(musicId, UserUtil.INSTANCE.getCurrentUserId());
            if (musicUserDto != null) {
                showRecievedSong = musicUserDto.getReceived();
            }
            if (music.getAuthor().getID().equals(UserUtil.INSTANCE.getCurrentUserId())) {
                owner = true;
                showSendMusic = true;
                showBuyLink = false;
                hideDownload = false;

            } else {
                if (isInbox) {
                    if (musicExchangeObj.getReceiverWebDownloads() == 0) {
                        hideDownload = false;
                    } else {
                        showNoteLimit = true;
                        showBuyLink = true;
                    }
                } else {
                    if (musicExchangeObj.getSenderWebDownloads() == 0) {
                        hideDownload = false;
                    } else {
                        showNoteLimit = true;
                        showBuyLink = true;
                    }
                }
            }

            if (showRecievedSong) {
                Page<MusicExchangeDto> musicExchangeDtos = musicService.getMusicExchangeByToUser(UserUtil.INSTANCE.getCurrentUserId(), new PageRequest(0, 1, Sort.Direction.DESC, "exchangeDate"));
                if (musicExchangeDtos.getContent().size() > 0)
                    mav.addObject("lyricsReceived", musicExchangeDtos.getContent().get(0).getRevceivedLyrics());
                LOGGER.info("Received " + musicExchangeDtos.getContent().get(0).getRevceivedLyrics());
            }

        } else { //cache the request
            requestCache.saveRequest(request, response);
        }

        Integer musicExchange = -1;
        Object sendToUserSession = request.getSession().getAttribute("send-music-to");
        if (sendToUserSession != null) {
            Integer sendToUser = Integer.parseInt(String.valueOf(request.getSession().getAttribute("send-music-to")));
            musicExchange = musicService.countMusicSentBetweenFromAndToUserAndMusic(UserUtil.INSTANCE.getCurrentUserId(), sendToUser, musicId);
            LOGGER.debug("send music to user {} music exchange {}", sendToUserSession, musicExchange);
            showBuyLink = showBuyLink && showSendMusic && musicExchange == 0;
            hideDownload = true;
        }

        mav.addObject("sendToUser", sendToUserSession);
        mav.addObject("music", music);
        mav.addObject("isOwner", owner);
        mav.addObject("squareAppID", squareAppID);
        mav.addObject("hideDownload", hideDownload);
        mav.addObject("showBuyLink", showBuyLink);
        mav.addObject("showSendBuyLink", !showSendMusic && musicExchange == 0);
        mav.addObject("showAlreadySendMsg", musicExchange > 0);
        mav.addObject("showSendMusic", showSendMusic && musicExchange == 0);
        mav.addObject("showNoteLimit", showNoteLimit);
        mav.addObject("showReceivedMusic", showRecievedSong);
        mav.addObject("musicExchange", musicExchangeObj);

        mav.addObject("searchText", "");
        mav.addObject("filter", "");
        if (isInbox) {
            mav.setViewName("user/music-inbox-details");
        } else {
            mav.setViewName("user/music-outbox-details");
        }
        mav.addObject("music", music);

        return mav;
    }
//
//    @GetMapping("/search")
//    public ModelAndView search(
//                               @RequestParam(value = "filter", defaultValue = "artist") String filter,
//                               @RequestParam(value = "searchText", defaultValue = "") String searchText,
//                               @RequestParam(value = "genreId", defaultValue = "0") Integer genreId,
//                               @RequestParam(value = "page", defaultValue = "1") Integer page ,
//                               @RequestParam(value = "lang", defaultValue = "EN") String language) {
//
//        Locale locale = LocaleContextHolder.getLocale();
//        language = locale.getLanguage().toUpperCase();
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("music/search");
//        modelAndView.addObject("searchText", searchText);
//        modelAndView.addObject("filter", filter);
//        modelAndView.addObject("genreId", genreId);
//        List<GenreDto> genres = musicService.getAllEnabledGenres();
//        GenreDto genreDto = new GenreDto();
//        genreDto.setTitle("Select a Genre");
//        genreDto.setId(0);
//        if(genres.isEmpty()) {
//            genres.add(genreDto);
//        } else {
//            genres.add(0, genreDto);
//        }
//        modelAndView.addObject("genres", genres);
//        Page<MusicDto> musicPage = musicService.findByFilterAndKeyword(filter, searchText, new PageRequest(Pager.evalPageInteger(page), 12));
//        modelAndView.addObject("musicPage", musicPage);
//        Pager pager = new Pager(musicPage.getTotalPages(), musicPage.getNumber(), 5);
//
//
//        modelAndView.addObject("pager", pager);
//        return modelAndView;
//    }


    @GetMapping("/sell-your-music")
    public ModelAndView musicianLanding(ModelAndView model,
                                        @RequestParam(value = "lang", defaultValue = "EN") String language) {
        if (!model.getModelMap().containsAttribute("user")) {
            model.addObject("user", new UserDto());
        }
        Locale locale = LocaleContextHolder.getLocale();
        language = locale.getLanguage().toUpperCase();
        model.addObject(PageConstants.MUSICIAN_LANDING_PAGE_DESCRIPTION, cmsService.getTextByTagAndLanguage(PageConstants.MUSICIAN_LANDING_PAGE_DESCRIPTION, language).replace("\\n", "<br>"));
        model.addObject(PageConstants.MUSICIAN_LANDING_PAGE_TITLE, cmsService.getTextByTagAndLanguage(PageConstants.MUSICIAN_LANDING_PAGE_TITLE, language).replace("\\n", "<br>"));
        model.addObject(PageConstants.MUSICIAN_LANDING_PAGE_IMAGE, cmsService.getTextByTagAndLanguage(PageConstants.MUSICIAN_LANDING_PAGE_IMAGE, language).replace("\\n", "<br>"));
        model.addObject("mostRecentMusic", musicService.getAllApproved(new PageRequest(0, 9, Sort.Direction.DESC, "created")).getContent());
        model.setViewName("music/musician-landing");
        return model;
    }

    @GetMapping("/send-music")
    public ModelAndView musicUsersLanding(ModelAndView model,
                                          @RequestParam(value = "lang", defaultValue = "EN") String language) {
        if (!model.getModelMap().containsAttribute("user")) {
            model.addObject("user", new UserDto());
        }
        Locale locale = LocaleContextHolder.getLocale();
        language = locale.getLanguage().toUpperCase();
        model.addObject(PageConstants.MUSIC_USER_LANDING_PAGE_TITLE, cmsService.getTextByTagAndLanguage(PageConstants.MUSIC_USER_LANDING_PAGE_TITLE, language).replace("\\n", "<br>"));
        model.addObject(PageConstants.MUSIC_USER_LANDING_PAGE_DESCRIPTION, cmsService.getTextByTagAndLanguage(PageConstants.MUSIC_USER_LANDING_PAGE_DESCRIPTION, language).replace("\\n", "<br>"));
        model.addObject(PageConstants.MUSIC_USER_LANDING_PAGE_IMAGE, cmsService.getTextByTagAndLanguage(PageConstants.MUSIC_USER_LANDING_PAGE_IMAGE, language).replace("\\n", "<br>"));


        model.addObject("mostGiftedList", musicService.getMostPopularPurchased(new PageRequest(0, 9)).getContent());
        model.addObject("lovappyList", musicService.getTopByLovappyUploader(new PageRequest(0, 9)).getContent());
        model.setViewName("music/music-users-landing");
        return model;
    }


    @GetMapping("/my-music")
    public ModelAndView musicianMusic(ModelAndView model,
                                      @RequestParam(value = "searchText", defaultValue = "") String searchText,
                                      @RequestParam(value = "page", defaultValue = "1") Integer page,
                                      @RequestParam(value = "lang", defaultValue = "EN") String language) {

        Locale locale = LocaleContextHolder.getLocale();
        language = locale.getLanguage().toUpperCase();
        Page<MusicDto> musicPage = null;
        if (searchText.isEmpty()) {
            musicPage = musicService.getAllByAuthor(UserUtil.INSTANCE.getCurrentUserId(), new PageRequest(Pager.evalPageInteger(page), 12, Sort.Direction.DESC, "created"));

        } else {
            musicPage = musicService.getAllByKeywordAndAuthor(searchText, UserUtil.INSTANCE.getCurrentUser().getEmail(), new PageRequest(Pager.evalPageInteger(page), 12, Sort.Direction.DESC, "created"));
        }
        model.addObject("musicPage", musicPage);
        Pager pager = new Pager(musicPage.getTotalPages(), musicPage.getNumber(), 5);

        ProfilePicsDto profilePicsDto = userProfileService.getProfilePicsDto(UserUtil.INSTANCE.getCurrentUserId());
        String musicianName = "";
        if (StringUtils.isEmpty(UserUtil.INSTANCE.getCurrentUser().getFirstName()))
            musicianName = UserUtil.INSTANCE.getCurrentUser().getEmail();
        else {
            musicianName = StringUtils.isEmpty(UserUtil.INSTANCE.getCurrentUser().getLastName()) ? UserUtil.INSTANCE.getCurrentUser().getFirstName() : UserUtil.INSTANCE.getCurrentUser().getFirstName() + " " +
                    UserUtil.INSTANCE.getCurrentUser().getLastName();
        }
        model.addObject("musicianName", musicianName);
        model.addObject("musicianPhoto", profilePicsDto.getPhotoIdentificationCloudFile());
        model.addObject("searchText", searchText);
        model.addObject("pager", pager);
        model.setViewName("music/musician_music");
        return model;
    }

    @GetMapping("/upload")
    public ModelAndView uploadMusic(ModelAndView model,
                                    @RequestParam(value = "lang", defaultValue = "EN") String language) {
        List<GenreDto> genres = musicService.getAllEnabledGenres();
        GenreDto genreDto = new GenreDto();
        genreDto.setTitle("Select a Genre");
        genreDto.setId(0);
        if (genres.isEmpty()) {
            genres.add(genreDto);
        } else {
            genres.add(0, genreDto);
        }
        Locale locale = LocaleContextHolder.getLocale();
        language = locale.getLanguage();
        model.addObject("genres", genres);
        model.addObject("musicSubmit", new MusicSubmitDto());
        model.addObject("musicParameters", cmsService.findPageByTagWithImagesAndTexts("music-uploads", language).getTexts());

        model.setViewName("music/upload-music");
        return model;
    }


    @GetMapping("/{music-id}/edit")
    public ModelAndView editMusic(@PathVariable("music-id") Integer musicId, ModelAndView model,
                                  @RequestParam(value = "lang", defaultValue = "EN") String language) {

        Locale locale = LocaleContextHolder.getLocale();
        language = locale.getLanguage().toUpperCase();
        List<GenreDto> genres = musicService.getAllEnabledGenres();
        MusicDto musicDto = musicService.getMusic(musicId);
        GenreDto genreDto = new GenreDto();
        genreDto.setTitle("Select a Genre");
        genreDto.setId(0);
        if (genres.isEmpty()) {
            genres.add(genreDto);
        } else {
            genres.add(0, genreDto);
        }
        model.addObject("genres", genres);

        MusicSubmitDto musicSubmitDto = new MusicSubmitDto();
        musicSubmitDto.setAdvertised(musicDto.getAdvertised());
        musicSubmitDto.setConfirmed(true);
        musicSubmitDto.setGenreId(musicDto.getGenre().getId());
        musicSubmitDto.setLyrics(musicDto.getLyrics());
        musicSubmitDto.setArtistName(musicDto.getArtistName());
        musicSubmitDto.setTitle(musicDto.getTitle());
        model.addObject("musicSubmit", musicSubmitDto);
        model.addObject("musicCoverPhoto", musicDto.getCoverImageUrl());
        model.addObject("musicId", musicId);
        model.setViewName("music/edit-music");
        return model;
    }

    @GetMapping("/send/user/{userId}")
    public ModelAndView sendSongToUser(@PathVariable("userId") Integer userId, HttpServletRequest request,
                                       @RequestParam(value = "lang", defaultValue = "EN") String language) {

        Locale locale = LocaleContextHolder.getLocale();
        language = locale.getLanguage().toUpperCase();
        request.getSession().setAttribute("send-music-to", userId);
        ModelAndView model = new ModelAndView();
        model.setViewName("redirect:/music");
        return model;
    }

    @GetMapping("/musician/help")
    public ModelAndView purchaseInfo(ModelAndView mav,
                                     @RequestParam(value = "lang", defaultValue = "EN") String language) {
        Locale locale = LocaleContextHolder.getLocale();
        language = locale.getLanguage().toUpperCase();
        mav.addObject("musicianTerms", cmsService.findPageByTagWithImagesAndTexts("musician-terms", language).getTexts());
        mav.setViewName("music/songwriters_info");
        return mav;
    }

    @GetMapping("/purchase/terms")
    public ModelAndView purchaseTerms(ModelAndView mav,
                                      @RequestParam(value = "lang", defaultValue = "EN") String language) {

        Locale locale = LocaleContextHolder.getLocale();
        language = locale.getLanguage().toUpperCase();
        mav.setViewName("music/songwriters_terms");
        return mav;
    }


    @GetMapping("/signup")
    public ModelAndView signup(ModelAndView mav,
                               @RequestParam(value = "lang", defaultValue = "EN") String language) {

        Locale locale = LocaleContextHolder.getLocale();
        language = locale.getLanguage().toUpperCase();
        mav.setViewName("music/musician-registration");
        return mav;
    }
}
