package com.realroofers.lovappy.web.email;

import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import org.springframework.util.StringUtils;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Created by Daoud Shaheen on 9/10/2017.
 * handle new likes, matches, songs, gifts
 */
public class InteractionEmailTemplateHandler extends AbstractEmailTemplateHandler {


    public InteractionEmailTemplateHandler(String baseUrl, String email, MailService mailService, ITemplateEngine templateEngine, EmailTemplateDto emailTemplateDto) {
        super(baseUrl, email, mailService, templateEngine, emailTemplateDto);
    }

    @Override
    public void send() {
        if (!emailTemplateDto.isEnabled())
            return;

        try {
            Context context = new Context();

            String body = emailTemplateDto.getBody();

            String userId = emailTemplateDto.getAdditionalInformation().get("userId");

            if (!StringUtils.isEmpty(userId)) {
                body = body.replace("NUMBER", userId);
            }
            context.setVariable("body", body);
            context.setVariable("link", emailTemplateDto.getAdditionalInformation().get("url"));
            context.setVariable("baseUrl", baseUrl);
            context.setVariable("emailBG", emailTemplateDto.getImageUrl());
            context.setVariable("buttonText", emailTemplateDto.getAdditionalInformation().get("buttonText") == null ? "Click Here" : emailTemplateDto.getAdditionalInformation().get("buttonText"));
            String content = templateEngine.process(emailTemplateDto.getTemplateUrl(), context);
            mailService.sendMail(email, content, emailTemplateDto.getSubject(), emailTemplateDto.getFromEmail(), emailTemplateDto.getFromName());
        } catch (Exception ex) {
            LOGGER.error("Failed to send email: {}", ex);
        }
    }
}
