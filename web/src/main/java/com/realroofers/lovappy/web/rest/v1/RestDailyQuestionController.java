package com.realroofers.lovappy.web.rest.v1;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.exception.GenericServiceException;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.OrderDetailDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.questions.DailyQuestionService;
import com.realroofers.lovappy.service.questions.dto.DailyQuestionDto;
import com.realroofers.lovappy.service.questions.dto.UserResponseDto;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.web.exception.ResourceNotFoundException;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.PaymentUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Darrel Rayen on 10/19/18.
 */
@RestController
@RequestMapping({"/api/v1/questions", "/api/v2/questions"})
public class RestDailyQuestionController {

    private static final Logger LOG = LoggerFactory.getLogger(RestDailyQuestionController.class);
    private final DailyQuestionService dailyQuestionService;
    private final CloudStorage cloudStorage;
    private final PaymentUtil paymentUtil;
    private final OrderService orderService;

    @Value("${lovappy.cloud.bucket.lovdrop}")
    private String lovdropsBucket;

    @Autowired
    public RestDailyQuestionController(DailyQuestionService dailyQuestionService, CloudStorage cloudStorage, PaymentUtil paymentUtil, OrderService orderService) {
        this.dailyQuestionService = dailyQuestionService;
        this.cloudStorage = cloudStorage;
        this.paymentUtil = paymentUtil;
        this.orderService = orderService;
    }

    /**
     * This api returns the user response depending on the response id
     *
     * @param responseId - parameter accepts the place id of a Dating Place
     * @return a UserResponseDto object
     */
    @RequestMapping(value = "/responses/{id}", method = RequestMethod.GET)
    public ResponseEntity<UserResponseDto> getUserResponse(@PathVariable("id") Long responseId) {
        UserResponseDto userResponseDto = dailyQuestionService.findByResponseId(responseId);
        if (userResponseDto != null) {
            return new ResponseEntity<>(userResponseDto, HttpStatus.OK);
        }
        throw new ResourceNotFoundException("No responses yet");
    }

    /**
     * This api returns the latest question added by the admin
     *
     * @return a DailyQuestionDto object
     */
    @RequestMapping(value = "/latest", method = RequestMethod.GET)
    public ResponseEntity<DailyQuestionDto> getLatestQuestion() {
        DailyQuestionDto questionDto = dailyQuestionService.findLatestQuestion();
        if (questionDto != null) {
            return new ResponseEntity<>(questionDto, HttpStatus.OK);
        }
       throw new ResourceNotFoundException("No questions yet");
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Page<DailyQuestionDto>> getAllQuestions(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        Page<DailyQuestionDto> questionDtos = dailyQuestionService.getAllQuestionsWithUserId(UserUtil.INSTANCE.getCurrentUserId(),
                new PageRequest(page - 1, 10));
        if (questionDtos != null) {
            return new ResponseEntity<>(questionDtos, HttpStatus.OK);
        }
        throw new GenericServiceException("Internal Server error");
    }

    /**
     * This api returns a paginated list of User Responses of the user logged in
     *
     * @param page - parameter accepts the page number of results requested
     * @return a paginated list of UserResponse entity
     */
    @RequestMapping(value = "/responses/me", method = RequestMethod.GET)
    public ResponseEntity<Page<UserResponseDto>> getMyResponses(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        try {
            Page<UserResponseDto> userResponseDtos = dailyQuestionService.findUserResponseByUserId(UserUtil.INSTANCE.getCurrentUserId(),
                    new PageRequest(page - 1, 10));

            return new ResponseEntity<>(userResponseDtos, HttpStatus.OK);
        } catch (Exception ex) {
            throw new GenericServiceException("Internal Server error");
        }
    }

    /**
     * This api returns a list of User Responses of a user
     *
     * @param userId - parameter accepts the user id of the user
     * @return a paginated list of UserResponse entity
     */
    @RequestMapping(value = "/responses/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<UserResponseDto>> getUserResponsesByUserId(@PathVariable("id") Integer userId) {
        try {
            List<UserResponseDto> userResponseDtos = dailyQuestionService.findPaidUserResponseByUserId(userId, UserUtil.INSTANCE.getCurrentUserId());

            return new ResponseEntity<>(userResponseDtos, HttpStatus.OK);
        } catch (Exception ex) {
            throw new GenericServiceException("Internal Server error");
        }
    }

    /**
     * This api is used to save response for a question
     *
     * @param questionId   - parameter accepts the id of the question
     * @param uploadFile   - parameter accepts the question response recording in mp3 format
     * @param seconds      - parameter accepts the length of the recording in seconds
     * @param languageCode - parameter accepts the language id of the response language
     * @param currentDate  - parameter accepts the current date in milliseconds
     * @return the saved UserResponseDto object
     */
    @RequestMapping(value = "/{id}/response", method = RequestMethod.POST)
    public ResponseEntity<UserResponseDto> uploadResponse(@PathVariable("id") Long questionId,
                                                          @RequestParam("data") MultipartFile uploadFile,
                                                          @RequestParam("seconds") Integer seconds,
                                                          @RequestParam("languageId") Integer languageCode,
                                                          @RequestParam("today") Long currentDate) {
        try {
            CloudStorageFileDto cloudStorageFileDto =
                    cloudStorage.uploadFile(uploadFile, lovdropsBucket, FileExtension.mp3.toString(), "audio/mp3");

            if (cloudStorageFileDto != null) {
                UserResponseDto userResponse = new UserResponseDto();
                userResponse.setAudioResponse(cloudStorageFileDto);
                userResponse.setQuestionId(questionId);
                Date date = new Date(currentDate * 1000L);
                userResponse.setResponseDate(date);
                userResponse.setUserId(UserUtil.INSTANCE.getCurrentUserId());
                userResponse.setLanguageId(languageCode);
                userResponse.setRecordSeconds(seconds);

                UserResponseDto userResponseDto = dailyQuestionService.addUserResponse(userResponse);
                if (userResponseDto != null) {
                    return new ResponseEntity<>(userResponseDto, HttpStatus.OK);
                }
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * This api is used to edit the response added to a question
     *
     * @param responseId - parameter accepts the id of the response
     * @param uploadFile - parameter accepts the question response recording in mp3 format
     * @param seconds    - parameter accepts the length of the recording in seconds
     * @return the saved UserResponseDto object
     */
    @RequestMapping(value = "/{questionId}/response/{id}", method = RequestMethod.PUT)
    public ResponseEntity<UserResponseDto> updateResponse(@PathVariable("questionId") Long questionId,
                                                          @PathVariable("id") Long responseId,
                                                          @RequestParam("data") MultipartFile uploadFile,
                                                          @RequestParam("seconds") Integer seconds) {
        try {
            CloudStorageFileDto cloudStorageFileDto =
                    cloudStorage.uploadFile(uploadFile, lovdropsBucket, FileExtension.mp3.toString(), "audio/mp3");

            if (cloudStorageFileDto != null) {
                UserResponseDto userResponse = new UserResponseDto();
                userResponse.setAudioResponse(cloudStorageFileDto);
                userResponse.setResponseId(responseId);
                userResponse.setQuestionId(questionId);
                userResponse.setRecordSeconds(seconds);
                UserResponseDto updated = dailyQuestionService.updateUserResponseMobile(userResponse, UserUtil.INSTANCE.getCurrentUserId());
                return updated != null ? new ResponseEntity<>(updated, HttpStatus.OK) : new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
            }
        } catch (Exception e) {
            throw new GenericServiceException("Internal Server error");
        }
        throw new GenericServiceException("Internal Server error");
    }

    /**
     * This api is used to delete a response added by the user
     *
     * @param questionId - parameter accepts the id of the question
     * @param responseId - parameter accepts the id the response that should be deleted
     * @return the delete status of the response
     */
    @RequestMapping(value = "/{questionId}/response/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteUserResponse(@PathVariable("questionId") Long questionId,
                                             @PathVariable("id") Long responseId) {
        Boolean isDeleted = dailyQuestionService.deleteResponseMobile(responseId, questionId, UserUtil.INSTANCE.getCurrentUserId());
        if (isDeleted) {
            return new ResponseEntity(HttpStatus.OK);
        }
        throw new GenericServiceException("Internal Server error");
    }

    //Todo Common method to be implemented
    @RequestMapping(value = "/order", method = RequestMethod.POST)
    public ResponseEntity<OrderDto> purchaseUserResponse(@RequestParam("responses") List<String> responseIds,
                                                         @RequestParam("coupon") String coupon,
                                                         @RequestParam("nonce") String nonce) {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        CalculatePriceDto calculatePriceDto = dailyQuestionService.calculatePrice(responseIds.size(), coupon);
        List<OrderDetailDto> orderDetailDtos = new ArrayList<>();
        List<Long> orderIds = new ArrayList<>();
        for (String responsId : responseIds) {
            OrderDetailDto orderDetailDto = new OrderDetailDto();
            Long productId = Long.parseLong(responsId);
            orderIds.add(productId);
            orderDetailDto.setOriginalId(productId);
            orderDetailDto.setQuantity(1);
            orderDetailDto.setType(OrderDetailType.DAILY_QUESTION);
            orderDetailDtos.add(orderDetailDto);
        }
        OrderDto order = orderService.addOrder(orderDetailDtos, calculatePriceDto.getTotalPrice(),
                userId, coupon, null, PaymentMethodType.SQUARE);
        if (order != null) {

            Boolean executed = null;
            try {
                executed = paymentUtil.executeTransaction(nonce, order, null);
                if (executed) {
                    dailyQuestionService.addPurchaseRecord(orderIds, UserUtil.INSTANCE.getCurrentUserId());
                    return new ResponseEntity<>(order, HttpStatus.OK);
                } else {
                    orderService.revertOrder(order.getId());
                }
            } catch (Exception e) {
                orderService.revertOrder(order.getId());
            }

        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
