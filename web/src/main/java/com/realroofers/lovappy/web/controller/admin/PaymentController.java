package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CouponDto;
import com.realroofers.lovappy.service.order.dto.CouponRuleDto;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import com.realroofers.lovappy.service.order.support.RuleActionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

@Controller
@RequestMapping("/lox/payment")
public class PaymentController {

    private final OrderService orderService;

    @Autowired
    public PaymentController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/coupons")
    public ModelAndView coupons(ModelAndView model) {
        model.addObject("coupons", orderService.getAllCoupons());
        model.setViewName("admin/payment/coupons");
        return model;
    }

    @GetMapping("/coupons/new")
    public ModelAndView addCoupon(ModelAndView model) {
        model.addObject("coupon", new CouponDto());
        model.setViewName("admin/payment/coupon_details");
        return model;
    }

    @GetMapping("/coupons/{id}")
    public ModelAndView couponDetails(ModelAndView model, @PathVariable(value = "id") Integer id) {
        model.addObject("coupon", orderService.getCoupon(id));
        model.setViewName("admin/payment/coupon_details");
        return model;
    }

    @PostMapping("/coupons")
    public ResponseEntity saveCoupon(@RequestParam("id") Integer id,
                                     @RequestParam("couponCode") String code,
                                     @RequestParam("category") CouponCategory category,
                                     @RequestParam("discountPercent") Integer discountPercent,
                                     @RequestParam("expiryDate") Date expiryDate,
                                     @RequestParam("description") String description) {

        CouponDto coupon = orderService.saveCoupon(new CouponDto(id, code, category, discountPercent, expiryDate, description));
        return ResponseEntity.status(HttpStatus.OK).body(coupon);
    }

    @GetMapping("/coupon_rules")
    public ModelAndView couponRules(ModelAndView model) {
        model.addObject("coupon_rules", orderService.getAllCouponRules());
        model.setViewName("admin/payment/coupon_rules");
        return model;
    }

    @GetMapping("/coupon_rules/{id}")
    public ModelAndView couponRulesDetails(ModelAndView modelAndView, @PathVariable(value = "id") Integer id) {
        modelAndView.addObject("rule", orderService.getCouponRule(id));
        modelAndView.setViewName("admin/payment/coupon_rule_details");
        return modelAndView;
    }

    @GetMapping("/coupon_rules/new")
    public ModelAndView newCouponRules(ModelAndView modelAndView) {
        modelAndView.addObject("rule", new CouponRuleDto());
        modelAndView.setViewName("admin/payment/coupon_rule_details");
        return modelAndView;
    }

    @PostMapping("coupon_rule/save")
    public ResponseEntity saveCouponRule(
            @RequestParam("id") Integer id,
            @RequestParam("ruleDescription") String ruleDescription,
            @RequestParam("affectedCategory") String affectedCategory,
            @RequestParam("actionType") String actionType,
            @RequestParam("discountValue") Double discountValue,
            @RequestParam("expiryDate") Date expiryDate
    ) {
        CouponRuleDto couponRuleDto = new CouponRuleDto(id, ruleDescription, CouponCategory.valueOf(affectedCategory),
                RuleActionType.valueOf(actionType), discountValue, new Date(), expiryDate);
        couponRuleDto = orderService.saveRule(couponRuleDto);
        return ResponseEntity.status(HttpStatus.OK).body(couponRuleDto.getId());
    }
}
