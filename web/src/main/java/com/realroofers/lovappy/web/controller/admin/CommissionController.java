package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.product.BrandCommissionService;
import com.realroofers.lovappy.service.product.RetailerCommissionService;
import com.realroofers.lovappy.service.product.dto.BrandCommissionDto;
import com.realroofers.lovappy.service.product.dto.RetailerCommissionDto;
import com.realroofers.lovappy.service.user.CountryService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.model.Country;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/lox/commission")
public class CommissionController {

    private final RetailerCommissionService retailerCommissionService;
    private final BrandCommissionService brandCommissionService;
    private final UserService userService;
    private final CountryService countryService;

    public CommissionController(RetailerCommissionService retailerCommissionService, BrandCommissionService brandCommissionService, UserService userService, CountryService countryService) {
        this.retailerCommissionService = retailerCommissionService;
        this.brandCommissionService = brandCommissionService;
        this.userService = userService;
        this.countryService = countryService;
    }

    @GetMapping("/brand")
    public String brandPage(ModelMap model, @SortDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable,
                            @RequestParam(value = "searchText", defaultValue = "", required = false) String searchText) {

        model.addAttribute("searchText", searchText);
        model.addAttribute("page", brandCommissionService.findAll(pageable));
        model.addAttribute("commission", new BrandCommissionDto());

        return "admin/product/brand-commission";
    }

    @PostMapping("/brand/save")
    public ResponseEntity saveBrand(@ModelAttribute("brandCommissionDto") @Valid BrandCommissionDto brandCommissionDto) {

        try {
            brandCommissionDto.setAffectiveDate(getDate(brandCommissionDto.getEffectiveDate()));
        } catch (ParseException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

        Integer userId = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
            User user = (User) authentication.getDetails();
            userId = user.getUserId();
        }

        if (brandCommissionDto.getId() == null) {
            BrandCommissionDto addBrandCommissionDto = null;
            try {
                if (userId != null)
                    brandCommissionDto.setCreatedBy(userService.getUser(userId));
                brandCommissionDto.setActive(true);
                addBrandCommissionDto = brandCommissionService.create(brandCommissionDto);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
            }
            return ResponseEntity.status(HttpStatus.OK).body(addBrandCommissionDto.getId());
        } else {
            BrandCommissionDto updateBrandCommissionDto = null;
            try {
                brandCommissionDto.setUpdated(new Date());
                if (userId != null)
                    brandCommissionDto.setModifiedBy(userService.getUser(userId));
                updateBrandCommissionDto = brandCommissionService.update(brandCommissionDto);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
            }
            return ResponseEntity.status(HttpStatus.OK).body(updateBrandCommissionDto.getId());
        }
    }

    @GetMapping("/brand/delete{id}")
    public ModelAndView deactivateBrand(@PathVariable("id") Integer id) throws Exception {
        BrandCommissionDto brandCommissionDto = brandCommissionService.findById(id);

        Integer userId = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
            User user = (User) authentication.getDetails();
            userId = user.getUserId();
        }
        brandCommissionDto.setActive(false);
        brandCommissionDto.setUpdated(new Date());
        if (userId != null)
            brandCommissionDto.setModifiedBy(userService.getUser(userId));
        brandCommissionService.update(brandCommissionDto);


        return new ModelAndView("redirect:/lox/commission/brand");
    }

    @GetMapping("/retailer")
    public String retailerPage(ModelMap model, @SortDefault("id") Pageable pageable,
                               @RequestParam(value = "searchText", defaultValue = "", required = false) String searchText) {

        model.addAttribute("searchText", searchText);
        model.addAttribute("page", retailerCommissionService.findAll(pageable));
        model.addAttribute("commission", new RetailerCommissionDto());
        model.addAttribute("countryList", countryService.findAllActive());

        return "admin/product/retailer-commission";
    }

    @PostMapping("/retailer/save")
    public ResponseEntity saveRetailer(@ModelAttribute("retailerCommissionDto") @Valid RetailerCommissionDto retailerCommissionDto) {

        try {
            retailerCommissionDto.setAffectiveDate(getDate(retailerCommissionDto.getEffectiveDate()));
        } catch (ParseException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

        Country country = countryService.findById(retailerCommissionDto.getCountryId());

        if (country == null) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        } else {
            retailerCommissionDto.setCountry(country);
        }
        Integer userId = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
            User user = (User) authentication.getDetails();
            userId = user.getUserId();
        }

        if (retailerCommissionDto.getId() == null) {
            RetailerCommissionDto addBrandCommissionDto = null;
            try {
                if (userId != null)
                    retailerCommissionDto.setCreatedBy(userService.getUser(userId));
                retailerCommissionDto.setActive(true);
                addBrandCommissionDto = retailerCommissionService.create(retailerCommissionDto);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
            }
            return ResponseEntity.status(HttpStatus.OK).body(addBrandCommissionDto.getId());
        } else {
            RetailerCommissionDto updateBrandCommissionDto = null;
            try {
                retailerCommissionDto.setUpdated(new Date());
                if (userId != null)
                    retailerCommissionDto.setModifiedBy(userService.getUser(userId));
                updateBrandCommissionDto = retailerCommissionService.update(retailerCommissionDto);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
            }
            return ResponseEntity.status(HttpStatus.OK).body(updateBrandCommissionDto.getId());
        }
    }

    @GetMapping("/retailer/delete{id}")
    public ModelAndView deactivateRetailer(@PathVariable("id") Integer id) throws Exception {
        RetailerCommissionDto retailerCommissionDto = retailerCommissionService.findById(id);

        Integer userId = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
            User user = (User) authentication.getDetails();
            userId = user.getUserId();
        }
        retailerCommissionDto.setActive(false);
        retailerCommissionDto.setUpdated(new Date());
        if (userId != null)
            retailerCommissionDto.setModifiedBy(userService.getUser(userId));
        retailerCommissionService.update(retailerCommissionDto);


        return new ModelAndView("redirect:/lox/commission/retailer");
    }

    private Date getDate(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        return (Date) format.parse(date);
    }
}
