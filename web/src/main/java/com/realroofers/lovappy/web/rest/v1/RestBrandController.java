package com.realroofers.lovappy.web.rest.v1;


import com.realroofers.lovappy.service.product.BrandService;
import com.realroofers.lovappy.service.product.dto.BrandDto;
import com.realroofers.lovappy.service.product.dto.mobile.BrandItemDto;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.service.vendor.VendorService;
import com.realroofers.lovappy.service.vendor.dto.VendorDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Manoj Janaka on 19/3/2018.
 */
@RestController
@RequestMapping({"/api/v1/brand", "/api/v2/brand"})
public class RestBrandController {

    private final BrandService brandService;
    private final VendorService vendorService;

    public RestBrandController(BrandService brandService, VendorService vendorService) {
        this.brandService = brandService;
        this.vendorService = vendorService;
    }


    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<BrandItemDto>> getAll() {
        List<BrandItemDto> brandDtos = brandService.findAllForMobile();
        return new ResponseEntity<>(brandDtos, HttpStatus.OK);
    }


    @PostMapping("/add")
    public ResponseEntity<BrandDto> addBrand(@ModelAttribute @Valid BrandDto brandDto) {
        try {

            BrandDto brand = brandService.findByName(brandDto.getBrandName());
            if (brand != null) {
                return new ResponseEntity<>(HttpStatus.FOUND);
            }
            final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

            brandDto.setCreatedBy(new UserDto(userId));
            brandDto.setActive(true);
            BrandDto createdBrand = brandService.create(brandDto);

            return new ResponseEntity<>(createdBrand, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping("/skip-register")
    public ResponseEntity skipUpload() {
        JsonResponse res = new JsonResponse();
        try {
            Integer userId = UserUtil.INSTANCE.getCurrentUserId();
            VendorDto vendor = vendorService.findByUser(userId);
            vendor.setRegistrationStep2(true);

            vendorService.update(vendor);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body(res);
        }
        return ResponseEntity.ok().body(res);
    }

    @GetMapping("/deactivate{id}")
    public ResponseEntity<BrandDto> deactivate(@PathVariable("id") Integer id) {
        try {
            BrandDto brand = brandService.findById(id);
            if (brand == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            brand.setActive(false);
            BrandDto updatedBrand = brandService.update(brand);

            return new ResponseEntity<>(updatedBrand, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/activate{id}")
    public ResponseEntity<BrandDto> activate(@PathVariable("id") Integer id) {
        try {
            BrandDto brand = brandService.findById(id);
            if (brand == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            brand.setActive(true);
            BrandDto updatedBrand = brandService.update(brand);

            return new ResponseEntity<>(updatedBrand, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
