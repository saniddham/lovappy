package com.realroofers.lovappy.web.rest.v1;


import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.support.PhotoNames;
import com.realroofers.lovappy.service.couples.CoupleProfileService;
import com.realroofers.lovappy.service.couples.dto.CouplesProfileDto;
import com.realroofers.lovappy.service.exception.GenericServiceException;
import com.realroofers.lovappy.service.exception.ResourceNotFoundException;
import com.realroofers.lovappy.service.likes.LikeService;
import com.realroofers.lovappy.service.lovstamps.LovstampSampleService;
import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampDto;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampSampleDto;
import com.realroofers.lovappy.service.lovstamps.support.SampleTypes;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.user.*;
import com.realroofers.lovappy.service.user.dto.*;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.rest.dto.ProfilePicAccessRequest;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * @author Eias Altawil
 */

@Slf4j
@RestController
@RequestMapping({"/api/v1/user/profile", "/api/v2/user/profile"})
public class RestUserProfileController {

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    private final UserProfileService userProfileService;
    private final UserService userService;
    private final CloudStorage cloudStorage;
    private final EmailTemplateService emailTemplateService;
    private final LovstampService lovstampService;
    private final LovstampSampleService lovstampSampleService;
    private final MailService mailService;
    private final CoupleProfileService coupleProfileService;
    private final ITemplateEngine templateEngine;

    private final LikeService likeService;

    @Autowired
    public RestUserProfileController(UserProfileService userProfileService, UserService userService, CloudStorage cloudStorage, EmailTemplateService emailTemplateService, LovstampService lovstampService, LovstampSampleService lovstampSampleService, MailService mailService, CoupleProfileService coupleProfileService, ITemplateEngine templateEngine, LikeService likeService) {
        this.userProfileService = userProfileService;
        this.userService = userService;
        this.cloudStorage = cloudStorage;
        this.emailTemplateService = emailTemplateService;
        this.lovstampService = lovstampService;
        this.lovstampSampleService = lovstampSampleService;
        this.mailService = mailService;
        this.coupleProfileService = coupleProfileService;
        this.templateEngine = templateEngine;

        this.likeService = likeService;
    }


    @RequestMapping(value = "/voice/skip", method = RequestMethod.POST)
    public ResponseEntity<Void> skipVoice() {

        UserProfileDto userProfileDto = userProfileService.findOne(UserUtil.INSTANCE.getCurrentUserId());
        Page<LovstampSampleDto> lovstampSampleDtoPage = lovstampSampleService.getLovstampSamplesByLanguageAndGenderAndType(1, userProfileDto.getGender(), SampleTypes.DEFAULT, new PageRequest(0, 1));

        if (lovstampSampleDtoPage.getContent().size() > 0) {
            LovstampSampleDto lovstampSampleDto = lovstampSampleDtoPage.getContent().get(0);
            LovstampDto lovstamp = new LovstampDto();
            lovstamp.setUser(new UserDto(UserUtil.INSTANCE.getCurrentUserId()));
            lovstamp.setAudioFileCloud(lovstampSampleDto.getAudioFileCloud());
            lovstamp.setRecordSeconds(lovstampSampleDto.getDuration());
            lovstamp.setLanguage(lovstampSampleDto.getLanguage());

            if (lovstampService.create(lovstamp, true) != null)
                return ResponseEntity.ok().build();
        }

        userProfileService.skipVoice();
        //    sendUserMail(userService.getUserById(UserUtil.INSTANCE.getCurrentUserId()).getEmail(), PhotoNames.HANDSPHOTO, EmailTemplateConstants.PHOTO_SKIPPED);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/hands/upload", method = RequestMethod.POST)
    public ResponseEntity<CloudStorageFileDto> saveHandsPhoto(@RequestParam("file") MultipartFile multipartFile) {
        try {
            CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadFile(multipartFile, imagesBucket,
                    FileExtension.png.toString(), "image/png");
            userProfileService.saveHandsPhoto(cloudStorageFileDto);
            return ResponseEntity.ok().body(cloudStorageFileDto);

        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @RequestMapping(value = "/hands/skip", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> skipHandsPhoto(HttpServletRequest request) {
        userProfileService.skipHandsPhoto();
        sendUserMail(CommonUtils.getBaseURL(request), userService.getUserById(UserUtil.INSTANCE.getCurrentUserId()).getEmail(), PhotoNames.HANDSPHOTO, EmailTemplateConstants.PHOTO_SKIPPED);
        return ResponseEntity.noContent().build();
    }
    @RequestMapping(value = "/photos/skip", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> skipAllPhoto() {
        userProfileService.skipAllPhotos();
        return ResponseEntity.noContent().build();
    }

    private void sendUserMail(String baseUrl, String userEmail, PhotoNames photoNames, String emailTemplateConstant) {
        EmailTemplateDto emailTemplate =
                emailTemplateService.getByNameAndLanguage(emailTemplateConstant, "EN");
        emailTemplate.getVariables().put("##PHOTO##", photoNames.getValue());
        new EmailTemplateFactory()
                .build(baseUrl, userEmail, templateEngine, mailService, emailTemplate)
                .send();
    }


    @RequestMapping(value = "/feet/upload", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<CloudStorageFileDto> saveFeetPhoto(@RequestParam("file") MultipartFile multipartFile) {
        try {
            CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadFile(multipartFile, imagesBucket,
                    FileExtension.png.toString(), "image/png");
            userProfileService.saveFeetPhoto(cloudStorageFileDto);
            return ResponseEntity.ok().body(cloudStorageFileDto);

        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @RequestMapping(value = "/feet/skip", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> skipFeetPhoto(HttpServletRequest request) {
        userProfileService.skipFeetPhoto();
        sendUserMail(CommonUtils.getBaseURL(request), userService.getUserById(UserUtil.INSTANCE.getCurrentUserId()).getEmail(), PhotoNames.FEETPHOTO, EmailTemplateConstants.PHOTO_SKIPPED);
        return ResponseEntity.noContent().build();
    }


    @RequestMapping(value = "/legs/upload", method = RequestMethod.POST)
    public ResponseEntity<CloudStorageFileDto> saveLegsPhoto(@RequestParam("file") MultipartFile multipartFile) {
        try {
            CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadFile(multipartFile, imagesBucket,
                    FileExtension.png.toString(), "image/png");
            userProfileService.saveLegsPhoto(cloudStorageFileDto);
            return ResponseEntity.ok().body(cloudStorageFileDto);

        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @RequestMapping(value = "/legs/skip", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> skipLegsPhoto(HttpServletRequest request) {
        userProfileService.skipLegsPhoto();
        sendUserMail(CommonUtils.getBaseURL(request),userService.getUserById(UserUtil.INSTANCE.getCurrentUserId()).getEmail(), PhotoNames.LEGSPHOTO, EmailTemplateConstants.PHOTO_SKIPPED);
        return ResponseEntity.noContent().build();
    }


    @RequestMapping(value = "/photo", method = RequestMethod.POST)
    public ResponseEntity<CloudStorageFileDto> saveProfilePhoto(@RequestParam("file") MultipartFile multipartFile) {
        try {
            CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadFile(multipartFile, imagesBucket,
                    FileExtension.png.toString(), "image/png");
            Integer userId = UserUtil.INSTANCE.getCurrentUserId();
            userProfileService.saveProfilePhoto(userId, cloudStorageFileDto);
            return ResponseEntity.ok().body(cloudStorageFileDto);

        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @RequestMapping(value = "/photo/upload", method = RequestMethod.POST)
    public ResponseEntity<Object> uploadProfilePhotoWithoutThumbnails(@RequestParam("file") MultipartFile multipartFile) {
        try {
            CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadFile(multipartFile, imagesBucket,
                    FileExtension.png.toString(), "image/png");
            userProfileService.saveProfilePhoto(UserUtil.INSTANCE.getCurrentUserId(), cloudStorageFileDto);
            return ResponseEntity.ok().body(cloudStorageFileDto);

        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @RequestMapping(value = "/{userId}/block", method = RequestMethod.POST)
    public ResponseEntity<Void> blockUser(@PathVariable("userId") Integer userId) {

        if (Objects.equals(UserUtil.INSTANCE.getCurrentUserId(), userId))
            throw new GenericServiceException("Can not block the same as login User.");

        userProfileService.blockUser(UserUtil.INSTANCE.getCurrentUserId(), userId);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{userId}/unblock", method = RequestMethod.POST)
    public ResponseEntity<Void> unblockUser(@PathVariable("userId") Integer userId) {

        if (Objects.equals(UserUtil.INSTANCE.getCurrentUserId(), userId))
            throw new GenericServiceException("Can not unblock the same as login User.");

        boolean blocked = userProfileService.getBlockStatus(UserUtil.INSTANCE.getCurrentUser().getUserId(), userId);
        if (blocked)
            userProfileService.unblockUser(UserUtil.INSTANCE.getCurrentUserId(), userId);

        return ResponseEntity.ok().build();
    }


    @RequestMapping(value = "/nearby", method = RequestMethod.POST)
    public ResponseEntity<List<UserLocationDto>> getNearbyUsers(@RequestParam("latitude") Double latitude,
                                                                @RequestParam("longitude") Double longitude,
                                                                @RequestParam("radius") Integer radius,
                                                                @RequestParam(value = "pref_gender", defaultValue = "BOTH") Gender prefGender) {

        WithinRadiusDto searchDto = new WithinRadiusDto(latitude, longitude, radius, prefGender);
        List<UserLocationDto> nearbyUsers = userProfileService.getNearbyUsers(searchDto);

        return ResponseEntity.ok(nearbyUsers);
    }

    @RequestMapping(value = "/{userId}/photo/access", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void enableProfilePictureAccess(HttpServletRequest request,
                                           @PathVariable("userId") Integer viewerId,
                                           @RequestParam(value = "lang", defaultValue = "EN") String language) {
        userProfileService.enableProfileAccessView(UserUtil.INSTANCE.getCurrentUserId(), viewerId);
    }

    @RequestMapping(value = "/{userId}/photo/access", method = RequestMethod.DELETE)
    public void disableProfilePictureAccess(HttpServletRequest request,
                                            @PathVariable("userId") Integer viewerId,
                                            @RequestParam(value = "lang", defaultValue = "EN") String language) {
        userProfileService.disableProfileAccessView(UserUtil.INSTANCE.getCurrentUserId(), viewerId);
    }

    @RequestMapping(value = "/photo/access", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void enableProfilePictureAccess(@RequestBody ProfilePicAccessRequest request,
                                           @RequestParam(value = "lang", defaultValue = "EN") String language) {

        int maxTransaction = 0;
        if (request.getAccess().equals("MANAGED")) {
            maxTransaction = request.getMaxTransactions();
        }
        userProfileService.setProfileImageAccess(request.getAccess().equals("PUBLIC"), maxTransaction, UserUtil.INSTANCE.getCurrentUserId());
    }


    @RequestMapping(value = "/me", method = RequestMethod.GET)
    public MemberFullProfileDto getMemberProfile(@RequestParam(value = "lang", defaultValue = "EN") String language) {

        log.debug("Get Member {}:", UserUtil.INSTANCE.getCurrentUserId());

       MemberFullProfileDto memberFullProfileDto = userProfileService.getFullProfile(UserUtil.INSTANCE.getCurrentUserId());
        // Does He/She block me????
        CouplesProfileDto couplesProfileDto = coupleProfileService.getCoupleProfile(UserUtil.INSTANCE.getCurrentUserId());


        memberFullProfileDto.setCoupleProfile(couplesProfileDto);
//        response.put("musicReceivedCount",
//                musicService.countMusicReceived(UserUtil.INSTANCE.getCurrentUserId()));
//
//        response.put("musicSentCount",
//                musicService.countMusicSent(UserUtil.INSTANCE.getCurrentUserId()));
//        return response;
        log.debug("GET MyProfile {}", memberFullProfileDto);
        return memberFullProfileDto;

    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public MemberFullProfileDto getMember(@PathVariable("id") Integer userId, HttpServletRequest request) {
        log.debug("Get Member {}:", userId);

        MemberFullProfileDto memberFullProfileDto = userProfileService.getFullProfile(userId);
        if(!memberFullProfileDto.getActivated())
            throw new ResourceNotFoundException("User with id " + userId + " is not found");
        memberFullProfileDto.getAdditionalInfo().put("profilePicAccess", userProfileService.isProfilePicAccessed(UserUtil.INSTANCE.getCurrentUserId(), userId));
//
        boolean profilePicPublic = userProfileService.lockedByTransactionAlgorithem(UserUtil.INSTANCE.getCurrentUserId(), userId) || userProfileService.isProfilePicAccessed(userId, UserUtil.INSTANCE.getCurrentUserId());
        memberFullProfileDto.getAdditionalInfo().put("profilePicPublic", profilePicPublic);
        memberFullProfileDto.setIliked(likeService.liketo(UserUtil.INSTANCE.getCurrentUserId(), userId));
        memberFullProfileDto.setBlocked(userProfileService.getBlockStatus(UserUtil.INSTANCE.getCurrentUserId(), userId));

       String baseUrl = CommonUtils.getBaseURL(request);

        memberFullProfileDto.setFeetCloudFileUrl(baseUrl);
        memberFullProfileDto.setHandsCloudFileUrl(baseUrl);
        memberFullProfileDto.setLegsCloudFileUrl(baseUrl);
        memberFullProfileDto.setProfilePhotoCloudFileUrl(profilePicPublic, baseUrl);
        // Does He/She block me????

//        response.put("musicReceivedCount",
//                musicService.countMusicReceived(UserUtil.INSTANCE.getCurrentUserId()));
//
//        response.put("musicSentCount",
//                musicService.countMusicSent(UserUtil.INSTANCE.getCurrentUserId()));
//        return response;
        log.debug("GET MyProfile {}", memberFullProfileDto);
        return memberFullProfileDto;
    }

}
