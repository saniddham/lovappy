package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailDto;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.dto.LovappyEmailDto;
import com.realroofers.lovappy.service.mail.support.EmailTypes;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import com.realroofers.lovappy.web.util.Pager;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.ITemplateEngine;
import sun.misc.BASE64Decoder;

import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Optional;

/**
 * Created by Daoud Shaheen on 9/7/2017.
 */
@Controller
@RequestMapping("/lox/emails")
public class EmailTemplateController {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailTemplateController.class);

    private final MailService mailService;
    private final ITemplateEngine templateEngine;
    private final EmailTemplateService emailTemplateService;
    private final CloudStorage cloudStorage;

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @Autowired
    public EmailTemplateController(MailService mailService, ITemplateEngine templateEngine, EmailTemplateService emailTemplateService, CloudStorage cloudStorage) {
        this.mailService = mailService;
        this.templateEngine = templateEngine;
        this.emailTemplateService = emailTemplateService;
        this.cloudStorage = cloudStorage;
    }

    @GetMapping("/templates/{id}")
    public ModelAndView emailTemplateDetails(ModelAndView model, @PathVariable(value = "id") Integer id, @RequestParam(value = "lang", defaultValue = "EN") String language ){
        model.addObject("emailTemplate",  emailTemplateService.getByIdAndLanguage(id, language));
        model.setViewName("admin/email/email-template-details");
        return model;
    }

    @GetMapping("/templates/create")
    public ModelAndView emailTemplateCreate(ModelAndView model, @RequestParam(value = "lang", defaultValue = "EN") String language ){
        model.addObject("emailTemplate",  new EmailTemplateDto());
        model.setViewName("admin/email/email-template-create");
        return model;
    }
    @PostMapping("/templates/save")
    public ModelAndView createEmailTemplate(ModelAndView model,
                                            EmailTemplateDto emailTemplate,
                                            @RequestParam("file") MultipartFile file,
                                            @RequestParam(value = "lang", defaultValue = "EN") String language){

        EmailTemplateDto emailTemplateDto = emailTemplateService.getByNameAndLanguage(emailTemplate.getName(), language);
        if (emailTemplateDto == null) {
            emailTemplate.setTemplateUrl("email/default_email");
            emailTemplate.setType(EmailTypes.DEFAULT_EMAIL);
            emailTemplate.setLanguage(language);
            if (file != null && file.getSize() > 0) {
                try {
                    emailTemplate.setImageUrl(uploadEmailImage(file.getInputStream()));
                } catch (IOException ex) {

                }
            }
            emailTemplateService.create(emailTemplate);
        } else {
            saveEmailTemplateDetails(model, emailTemplate, file, emailTemplateDto.getId(), language);
        }
        model.setViewName("redirect:/lox/emails/templates");
        return model;
    }


    @PostMapping("/templates/test")
    @ResponseBody
    public ResponseEntity<?> testTemplate(@RequestBody EmailDto emailRequest,
                                               @RequestParam(value = "lang", defaultValue = "EN") String language,
                                               HttpServletRequest request){
        EmailTemplateDto emailTemplateDto = new EmailTemplateDto();
        emailTemplateDto.setBody(emailRequest.getBody());
        emailTemplateDto.setSubject(emailRequest.getSubject());
        emailTemplateDto.setType(EmailTypes.DEFAULT_EMAIL);
        emailTemplateDto.setTemplateUrl("email/default_email");
        emailTemplateDto.setEnabled(false);
        emailTemplateDto.setFromEmail(emailRequest.getFromEmail());
        if (emailRequest.getImageData() != null && emailRequest.getImageData().startsWith("data")) {
            // tokenize the data
            try {


                String parts[] = emailRequest.getImageData().split(",");
                String imageString = parts[1];

// create a buffered image
                BufferedImage image = null;
                byte[] imageByte;

                BASE64Decoder decoder = new BASE64Decoder();
                imageByte = decoder.decodeBuffer(imageString);
                ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
                bis.close();
                emailTemplateDto.setImageUrl(uploadEmailImage(bis));
            } catch (IOException ex) {
                LOGGER.error("Failed to test email image {}", ex);
            }
        }
        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), emailRequest.getEmail(), templateEngine,
                mailService, emailTemplateDto).send();
        return ResponseEntity.ok("{status:success}");
    }

    @PostMapping("/templates/{id}/save")
    public ModelAndView saveEmailTemplateDetails(ModelAndView model,
                                                 EmailTemplateDto emailTemplate,
                                                 @RequestParam("file") MultipartFile file,
                                                 @PathVariable(value = "id") Integer id,
                                                 @RequestParam(value = "lang", defaultValue = "EN") String language){

        LOGGER.debug("save template changes {}", emailTemplate);
        if (file != null && file.getSize() > 0) {
                try {
                    emailTemplate.setImageUrl(uploadEmailImage(file.getInputStream()));
                } catch (IOException ex) {

                }
        }

        emailTemplateService.update(id, emailTemplate);
        model.setViewName("redirect:/lox/emails/templates");
        return model;
    }

    @PostMapping("/templates/{id}/test")
    @ResponseBody
    public ResponseEntity<?> testEmailTemplate(@RequestBody EmailDto emailRequest,
                                               @PathVariable(value = "id") Integer id,
                                               @RequestParam(value = "lang", defaultValue = "EN") String language,
                                               HttpServletRequest request){
        LOGGER.debug("test email template {} ", id);
        EmailTemplateDto emailTemplateDto = emailTemplateService.getByIdAndLanguage(id, language);
        emailTemplateDto.setBody(emailRequest.getBody());
        emailTemplateDto.setSubject(emailRequest.getSubject());

        if (emailRequest.getImageData() != null && emailRequest.getImageData().startsWith("data")) {
            // tokenize the data
            try {


            String parts[] = emailRequest.getImageData().split(",");
            String imageString = parts[1];

// create a buffered image
            BufferedImage image = null;
            byte[] imageByte;

            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            bis.close();
            emailTemplateDto.setImageUrl(uploadEmailImage(bis));
            } catch (IOException ex) {
                LOGGER.error("Failed to test email image {}", ex);
            }
        }
        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), emailRequest.getEmail(), templateEngine,
                mailService, emailTemplateDto).send();
        return ResponseEntity.ok("{status:success}");
    }


    @GetMapping("/templates")
    public ModelAndView getEmailTemplates(ModelAndView model,
                                          @RequestParam("pageSize") Optional<Integer> pageSize,
                                          @RequestParam("page") Optional<Integer> pageNumber,
                                          @RequestParam(value = "lang", defaultValue = "EN") String language,
                                          @RequestParam(value = "filter", defaultValue = "") String filter,
                                          @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){

        PageRequest pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize));
        if(!StringUtils.isEmpty(filter)) {
            if(pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize), new Sort(sort, filter));
            }
        }
        model.addObject("filter", filter);
        model.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);


        Page<EmailTemplateDto> emailTemplateDtos = emailTemplateService.getListByLanguage(language, pageable);
        model.addObject("templates", emailTemplateDtos);

        Page<LovappyEmailDto> lovappyEmailDtoPage = emailTemplateService.getLovappyEmails(1, Integer.MAX_VALUE);
        model.addObject("emails", lovappyEmailDtoPage.getContent());

        Pager pager = new Pager(emailTemplateDtos.getTotalPages(), emailTemplateDtos.getNumber(), Pager.BUTTONS_TO_SHOW);

        model.addObject("selectedPageSize", pageSize.orElse(Pager.INITIAL_PAGE_SIZE));
        model.addObject("pageSizes", Pager.PAGE_SIZES);
        model.addObject("pager", pager);
        model.setViewName("admin/email/manage-templates");
        return model;
    }

    private String uploadEmailImage(InputStream inputStream){
        if (inputStream != null) {
            try {
                CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadImageWithThumbnails(IOUtils.toByteArray(inputStream),
                        imagesBucket, FileExtension.jpg.toString(), "image/jpeg", false);

                return cloudStorageFileDto.getUrl();
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
        }
        return "";
    }

}
