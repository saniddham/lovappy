package com.realroofers.lovappy.web.rest.v1;

import com.realroofers.lovappy.service.gift.GiftExchangeService;
import com.realroofers.lovappy.service.gift.GiftService;
import com.realroofers.lovappy.service.gift.PurchasedGiftService;
import com.realroofers.lovappy.service.gift.dto.GiftExchangeDto;
import com.realroofers.lovappy.service.gift.dto.PurchasedGiftDto;
import com.realroofers.lovappy.service.gift.dto.mobile.GiftExchangeInboxDto;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.product.ProductService;
import com.realroofers.lovappy.service.product.ProductTypeService;
import com.realroofers.lovappy.service.product.dto.GiftResponsePage;
import com.realroofers.lovappy.service.product.dto.ProductTypeDto;
import com.realroofers.lovappy.service.product.dto.mobile.ItemDto;
import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.survey.UserSurveyService;
import com.realroofers.lovappy.service.survey.dto.SurveyQuestionAnswerDto;
import com.realroofers.lovappy.service.survey.dto.UserSurveyDto;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.ToUser;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.dto.UserProfileDto;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.util.CommonUtils;
import com.realroofers.lovappy.web.util.PaymentUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author Manoj Senevirathne
 */

@RestController
@RequestMapping({"/api/v1/gift", "/api/v2/gift"})
public class RestGiftController {

    private static final Logger LOG = LoggerFactory.getLogger(RestGiftController.class);
    private final ProductService giftService;
    private final GiftService giftItemService;
    private final GiftExchangeService giftExchangeService;
    private final UserService userService;
    private final OrderService orderService;
    private final PaymentUtil paymentUtil;
    private final PurchasedGiftService purchasedGiftService;
    private final UserSurveyService userSurveyService;
    private final ProductTypeService productTypeService;

    private ITemplateEngine templateEngine;
    private MailService mailService;
    private final EmailTemplateService emailTemplateService;


    @Value("${email.basePath}")
    private String baseUrl;

    @Autowired
    public RestGiftController(ProductService giftService, GiftService giftItemService, GiftExchangeService giftExchangeService, UserService userService, OrderService orderService, PaymentUtil paymentUtil, PurchasedGiftService purchasedGiftService, UserSurveyService userSurveyService, ProductTypeService productTypeService, ITemplateEngine templateEngine, MailService mailService, EmailTemplateService emailTemplateService) {
        this.giftService = giftService;
        this.giftItemService = giftItemService;
        this.giftExchangeService = giftExchangeService;
        this.userService = userService;
        this.orderService = orderService;
        this.paymentUtil = paymentUtil;
        this.purchasedGiftService = purchasedGiftService;
        this.userSurveyService = userSurveyService;
        this.productTypeService = productTypeService;
        this.templateEngine = templateEngine;
        this.mailService = mailService;
        this.emailTemplateService = emailTemplateService;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<Page<ItemDto>> getAll(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                @RequestParam(value = "search") String searchText,
                                                @RequestParam(value = "brandId") int brandId,
                                                @RequestParam(value = "categoryId") int categoryId) {
        Page<ItemDto> mobileDtos = giftService.getProductForMobile(new PageRequest(page - 1, 10), searchText, brandId, categoryId);
        return new ResponseEntity<>(mobileDtos, HttpStatus.OK);
    }

    @RequestMapping(value = "/all-by-survey", method = RequestMethod.GET)
    public ResponseEntity<GiftResponsePage> getAllBySurvey(@RequestParam(value = "page", defaultValue = "1") Integer page) {

        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        UserSurveyDto userSurvey = userSurveyService.getUserSurvey(userId);
        int categoryId = 0;
        String categoryName = "All";
        if (userSurvey != null) {
            List<SurveyQuestionAnswerDto> surveyQuestionAnswer = userSurvey.getSurveyQuestionAnswer();

            if (surveyQuestionAnswer != null && !surveyQuestionAnswer.isEmpty()) {

                Optional<SurveyQuestionAnswerDto> first = surveyQuestionAnswer.stream().filter(surveyQuestionAnswerDto -> surveyQuestionAnswerDto.getQuestion().getId() == 2).findFirst();
                if (first.isPresent()) {
                    ProductTypeDto category = productTypeService.findByName(first.get().getAnswer().getTitle());
                    if (category != null) {
                        categoryName = category.getTypeName();
                        categoryId = category.getId();
                    }
                }
            }
        }

        Page<ItemDto> mobileDtos = giftService.getProductForMobile(new PageRequest(page - 1, 10), categoryId);

        if (mobileDtos.getTotalElements() <= 0) {
            categoryName = "All";
            mobileDtos = giftService.getProductForMobile(new PageRequest(page - 1, 10), 0);
        }

        GiftResponsePage giftResponsePage = new GiftResponsePage();
        giftResponsePage.setContent(mobileDtos.getContent());
        giftResponsePage.setTotalElements(mobileDtos.getTotalElements());
        giftResponsePage.setTotalPages(mobileDtos.getTotalPages());
        giftResponsePage.setNumberOfElements(mobileDtos.getNumberOfElements());
        giftResponsePage.setSize(mobileDtos.getSize());
        giftResponsePage.setNumber(mobileDtos.getNumber());
        giftResponsePage.setCategory(categoryName);

        return new ResponseEntity<>(giftResponsePage, HttpStatus.OK);
    }


    @RequestMapping(value = "/inbox/count", method = RequestMethod.GET)
    public long getInboxCount() {
        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        return giftExchangeService.getGiftExchangeInboxCount(userId);
    }

    @RequestMapping(value = "/outbox/count", method = RequestMethod.GET)
    public long getOutboxCount() {
        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        return giftExchangeService.getGiftExchangeOutboxCount(userId);
    }

    @RequestMapping(value = "/outbox/cost", method = RequestMethod.GET)
    public Double getOutboxCost() {
        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        return giftExchangeService.getTotalCostForUser(userId);
    }

    @RequestMapping(value = "/inbox", method = RequestMethod.GET)
    public ResponseEntity<Page<GiftExchangeInboxDto>> getAllInbox(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                                  @RequestParam(value = "sort") String sortOrder) {
        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        String sort = "sentAt";
        Sort.Direction sortDirection = Sort.Direction.DESC;

        if (sortOrder.equalsIgnoreCase("Date")) {
            sort = "sentAt";
            sortDirection = Sort.Direction.ASC;
        }else if (sortOrder.equalsIgnoreCase("Date_DESC")) {
            sort = "sentAt";
            sortDirection = Sort.Direction.DESC;
        }else if (sortOrder.equalsIgnoreCase("User")) {
            sort = "fromUser";
            sortDirection = Sort.Direction.ASC;
        }else if (sortOrder.equalsIgnoreCase("User_DESC")) {
            sort = "fromUser";
            sortDirection = Sort.Direction.DESC;
        }

        return ResponseEntity.ok(giftExchangeService.getGiftExchangeInboxMobile(userId, new PageRequest(page - 1, 10, sortDirection, sort)));
    }

    @RequestMapping(value = "/inbox/details", method = RequestMethod.GET)
    public ResponseEntity<GiftExchangeInboxDto> getInboxDetails(@RequestParam(value = "id") Integer id) {

        return ResponseEntity.ok(giftExchangeService.getGiftExchangeInboxMobile(id));
    }

    @RequestMapping(value = "/outbox", method = RequestMethod.GET)
    public ResponseEntity<Page<GiftExchangeInboxDto>> getAllOutbox(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                                   @RequestParam(value = "sort") String sortOrder) {
        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        String sort = "sentAt";
        Sort.Direction sortDirection = Sort.Direction.DESC;

        if (sortOrder.equalsIgnoreCase("Date")) {
            sort = "sentAt";
            sortDirection = Sort.Direction.ASC;
        }else if (sortOrder.equalsIgnoreCase("Date_DESC")) {
            sort = "sentAt";
            sortDirection = Sort.Direction.DESC;
        }else if (sortOrder.equalsIgnoreCase("User")) {
            sort = "toUser";
            sortDirection = Sort.Direction.ASC;
        }else if (sortOrder.equalsIgnoreCase("User_DESC")) {
            sort = "toUser";
            sortDirection = Sort.Direction.DESC;
        }
        return ResponseEntity.ok(giftExchangeService.getGiftExchangeOutboxMobile(userId, new PageRequest(page - 1, 10, sortDirection, sort)));
    }


    /**
     * This api call returns Gift object by the  id
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<ItemDto> getEventByIdMobile(@PathVariable("id") Integer id) {

        try {
            ItemDto itemDto = giftService.getProductForMobileById(id);
            return new ResponseEntity<>(itemDto, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<ToUser> getUser(@PathVariable("id") Integer id) {

        try {
            UserDto user = userService.getUser(id);

            UserProfileDto userProfile = user.getUserProfile();

            ToUser toUser = new ToUser();
            toUser.setId(user.getID());
            toUser.setAge(userProfile.getAge());
            toUser.setGender(userProfile.getGender().toString());
            toUser.setLanguage(userProfile.getSpeakingLanguagesAbb());
            toUser.setState(userProfile.getAddress().getStateShort());
            toUser.setUserImage(userProfile.getProfilePhotoCloudFile().getUrl());

            return new ResponseEntity<>(toUser, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


    @RequestMapping(value = "/calculate_price", method = RequestMethod.GET)
    public ResponseEntity<CalculatePriceDto> calculatePrice(@RequestParam("gift_id") Integer giftId,
                                                            @RequestParam("coupon") String coupon) {
        return ResponseEntity.ok(giftService.calculatePrice(giftId, coupon));
    }

    @PostMapping("/send")
    public ResponseEntity<?> purchaseGift(HttpServletRequest request,
                                          @RequestParam("to_user_id") Integer toUserID, @RequestParam("gift_id") Integer giftID,
                                          @RequestParam("orderId") Long orderId,
                                          @RequestParam(value = "language", defaultValue = "EN") String language) {

        OrderDto order = orderService.getOrderByID(orderId);
        if (order != null) {

            GiftExchangeDto giftExchange = giftExchangeService.sendGift(giftID, UserUtil.INSTANCE.getCurrentUserId(), toUserID, order);

            DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
            SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");

            EmailTemplateDto emailTemplate = emailTemplateService
                    .getByNameAndLanguage(EmailTemplateConstants.GIFT_SENT, language);
            emailTemplate.getAdditionalInformation().put("toUser", giftExchange.getToUser().getID() + "");
            emailTemplate.getAdditionalInformation().put("refNumber", giftExchange.getRefNumber());
            emailTemplate.getAdditionalInformation().put("productName", giftExchange.getGift().getProductName());
            emailTemplate.getAdditionalInformation().put("price", decimalFormat.format(giftExchange.getSoldPrice()));
            emailTemplate.getAdditionalInformation().put("date", dateFormatter.format(giftExchange.getSentAt()));
            emailTemplate.getAdditionalInformation().put("baseUrl", CommonUtils.getBaseURL(request));
            emailTemplate.getAdditionalInformation().put("url", baseUrl + "/my_account/gift/outbox");
            new EmailTemplateFactory().build(baseUrl, giftExchange.getFromUser().getEmail(), templateEngine,
                    mailService, emailTemplate).send();

            emailTemplate = emailTemplateService
                    .getByNameAndLanguage(EmailTemplateConstants.GIFT_RECEIVED, language);
            emailTemplate.getAdditionalInformation().put("fromUser", giftExchange.getToUser().getID() + "");
            emailTemplate.getAdditionalInformation().put("baseUrl", CommonUtils.getBaseURL(request));
            emailTemplate.getAdditionalInformation().put("url", baseUrl + "/my_account/gifts/inbox");
            new EmailTemplateFactory().build(baseUrl, giftExchange.getToUser().getEmail(), templateEngine,
                    mailService, emailTemplate).send();

            emailTemplate = emailTemplateService
                    .getByNameAndLanguage(EmailTemplateConstants.GIFT_SOLD, language);
            emailTemplate.getAdditionalInformation().put("toUser", giftExchange.getToUser().getID() + "");
            emailTemplate.getAdditionalInformation().put("refNumber", giftExchange.getRefNumber());
            emailTemplate.getAdditionalInformation().put("productName", giftExchange.getGift().getProductName());
            emailTemplate.getAdditionalInformation().put("price", decimalFormat.format(giftExchange.getSoldPrice()));
            emailTemplate.getAdditionalInformation().put("date", dateFormatter.format(giftExchange.getSentAt()));
            emailTemplate.getAdditionalInformation().put("baseUrl", CommonUtils.getBaseURL(request));
            emailTemplate.getAdditionalInformation().put("url", baseUrl + "/vendor");
            new EmailTemplateFactory().build(baseUrl, giftExchange.getGift().getCreatedBy().getEmail(), templateEngine,
                    mailService, emailTemplate).send();


            PurchasedGiftDto purchasedGiftDto = new PurchasedGiftDto();
            purchasedGiftDto.setUser(userService.getUser(UserUtil.INSTANCE.getCurrentUserId()));
            purchasedGiftDto.setProduct(new Product(giftService.findById(giftID)));
            purchasedGiftDto.setPurchasedOn(new Date());

            try {
                purchasedGiftService.create(purchasedGiftDto);
            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            orderService.revertOrder(order.getId());
        }

        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/redeem", method = RequestMethod.POST)
    public ResponseEntity<Void> redeemGift(@RequestParam("gift_id") Integer giftExchangeId,
                                           @RequestParam("location") Integer location) {

        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        try {
            giftExchangeService.redeem(giftExchangeId, userId, location);
            GiftExchangeDto giftExchange = giftExchangeService.findById(giftExchangeId);

            EmailTemplateDto emailTemplate = emailTemplateService
                    .getByNameAndLanguage(EmailTemplateConstants.GIFT_REDEEM, "EN");
            emailTemplate.getAdditionalInformation().put("toUser", giftExchange.getToUser().getID() + "");
            emailTemplate.getAdditionalInformation().put("refNumber", giftExchange.getRefNumber());
            emailTemplate.getAdditionalInformation().put("productName", giftExchange.getGift().getProductName());
            emailTemplate.getAdditionalInformation().put("price", decimalFormat.format(giftExchange.getSoldPrice()));
            emailTemplate.getAdditionalInformation().put("date", dateFormatter.format(giftExchange.getSentAt()));
            emailTemplate.getAdditionalInformation().put("redeemCode", giftExchange.getRedeemCode());
            emailTemplate.getAdditionalInformation().put("baseUrl", baseUrl);
            emailTemplate.getAdditionalInformation().put("url", baseUrl + "/vendor/sales/details");
            new EmailTemplateFactory().build(baseUrl, giftExchange.getGift().getCreatedBy().getEmail(), templateEngine,
                    mailService, emailTemplate).send();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok().build();
    }

}
