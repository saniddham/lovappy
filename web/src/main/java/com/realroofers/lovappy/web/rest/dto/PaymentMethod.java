package com.realroofers.lovappy.web.rest.dto;

import com.stripe.model.Card;
import lombok.Data;

import java.io.Serializable;
@Data
public class PaymentMethod implements Serializable {
    private String id;
    private String customerId;
    private String cardNumber;
    private Long cardExpMonth;
    private Long cardExpYear;
    private String cardCVC;
    private String methodType;
    private Boolean selected;
    public PaymentMethod(Card card) {
        this.id = card.getId();
        this.customerId= card.getCustomer();
        this.cardNumber = card.getLast4();
        this.cardExpMonth= card.getExpMonth();
        this.cardExpYear= card.getExpYear();
        this.cardCVC= card.getCvcCheck();
        this.methodType= card.getBrand();
        this.selected= false;
    }
    public PaymentMethod() {
    }
}