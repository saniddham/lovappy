package com.realroofers.lovappy.web.util;

import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.OrderDetailDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.dto.ChargeRequest;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.exception.BadRequestException;
import com.realroofers.lovappy.web.exception.ResourceNotFoundException;
import com.realroofers.lovappy.web.rest.dto.PaymentMethod;
import com.realroofers.lovappy.service.order.support.ProductType;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;

import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class StripeUtil {

    @Value("${stripe.publishable.key}")
    private String publishableKey;

    @Value("${stripe.secret.key}")
    private String secretKey;

    @Value("${email.basePath}")
    private String baseUrl;
    @Autowired
    private UserProfileService userProfileService;

    private static final Logger LOGGER = LoggerFactory.getLogger(StripeUtil.class);

    private final OrderService orderService;
    private final EmailTemplateService emailTemplateService;
    private final MailService mailService;
    private ITemplateEngine templateEngine;

    @Autowired
    public StripeUtil(OrderService orderService, EmailTemplateService emailTemplateService, MailService mailService,
                      List<ViewResolver> viewResolvers) {
        this.orderService = orderService;
        this.emailTemplateService = emailTemplateService;
        this.mailService = mailService;

        for (ViewResolver viewResolver : viewResolvers) {
            if (viewResolver instanceof ThymeleafViewResolver) {
                ThymeleafViewResolver thymeleafViewResolver = (ThymeleafViewResolver) viewResolver;
                this.templateEngine = thymeleafViewResolver.getTemplateEngine();
            }
        }
        if (this.templateEngine == null) {
            throw new BeanInitializationException("No view resolver of type ThymeleafViewResolver found.");
        }

    }

    public Boolean executeTransaction(Integer userId, OrderDto order, String userEmail) {
        String customerId = userProfileService.getStripeIdByUserId(userId);
        if(customerId == null) {
            throw new ResourceNotFoundException("Customer Id doesn't exist");
        }

        Map<String, Object> chargeParams = new HashMap<>();
        chargeParams.put("amount", order.getTotalPrice().intValue()*100);
        chargeParams.put("currency", ChargeRequest.Currency.USD);
        chargeParams.put("customer", customerId);
        try {
            Stripe.apiKey = secretKey;
            Charge charge = Charge.create(chargeParams);

            order =   orderService.setOrderTransactionID(order.getId(), charge.getBalanceTransaction());
            if (order != null) {
                sendConfirmationEmail(order, userEmail);
                return true;
            }
        } catch (StripeException e) {
            LOGGER.error("Error during charging {}", e);
        }
        return false;
    }

    public Map<String, Object> createCustomer(String token, String email, Integer userId){

        try {
            Stripe.apiKey = secretKey;
            Customer customer;
            boolean exists = false;
            String customerId = userProfileService.getStripeIdByUserId(userId);
            if(customerId == null) {
                Map<String, Object> customerParams = new HashMap<String, Object>();
                customerParams.put("email", email);
                customerParams.put("source", token);

                customer = Customer.create(customerParams);
                userProfileService.saveStripeCustomerId(userId, customer.getId());
            } else {
                exists = true;
                customer = Customer.retrieve(customerId);
            }

            Map<String, Object> map = new HashMap<>();
            map.put("customerId", customer.getId());
            map.put("email", email);
            map.put("exists", exists);
            return map;
        } catch (StripeException e) {
            LOGGER.error("Error during charging {}", e);
            throw new BadRequestException("Invalid email address");
        }
    }

    public PaymentMethod saveMethod(String token, String email, Integer userId){

        try {
            Stripe.apiKey = secretKey;
            Customer customer;
            String customerId = userProfileService.getStripeIdByUserId(userId);
            if(customerId == null) {
                throw new ResourceNotFoundException("Customer Id doesn't exist " + customerId);
            }
                customer = Customer.retrieve(customerId);

                Map<String, Object> params = new HashMap<>();
                params.put("source", token);
                Card source = (Card) customer.getSources().create(params);
                if(customer.getDefaultSourceObject() == null) {
                    setDefaultCard(userId, source.getId());
                }

                return new PaymentMethod(source);

        } catch (StripeException e) {
            LOGGER.error("Error during charging {}", e);
            return null;
        }
    }

    public PaymentMethod getDefaultCard(Integer userId){

        try {
            Stripe.apiKey = secretKey;
            String customerId = userProfileService.getStripeIdByUserId(userId);
            if(customerId != null) {
                Customer customer = Customer.retrieve(customerId);
                if(customer.getDefaultSource() != null) {
                    PaymentMethod paymentMethod =  new PaymentMethod((Card) customer.getSources().retrieve(customer.getDefaultSource()));
                paymentMethod.setSelected(true);
                return paymentMethod;
                }
                throw new ResourceNotFoundException("No default Payment Method");
            }

            throw new ResourceNotFoundException("Customer Id doesn't exist " + customerId);
        } catch (StripeException e) {
            LOGGER.error("Error during charging {}", e);
            throw new ResourceNotFoundException(e.getMessage());
        }

    }
    public void setDefaultCard(Integer userId, String cardId){

        try {
            Stripe.apiKey = secretKey;
            String customerId = userProfileService.getStripeIdByUserId(userId);

            if(customerId != null) {
                Customer customer = Customer.retrieve(customerId);
                customer.setDefaultSource(cardId);
                Map<String, Object> updateParams = new HashMap<>();
                updateParams.put("default_source", cardId);

                customer.update(updateParams);
                return;
            }

            throw new ResourceNotFoundException("Customer Id doesn't exist");
        } catch (StripeException e) {
            LOGGER.error("Error during charging {}", e);
            throw new ResourceNotFoundException(e.getMessage());
        }

    }
    public void deleteMethod(Integer userId, String cardId){

        try {
            Stripe.apiKey = secretKey;
            String customerId = userProfileService.getStripeIdByUserId(userId);
            if(customerId != null) {
                Customer customer = Customer.retrieve(customerId);
                customer.getSources().retrieve(cardId).delete();
                return;
            }

            throw new ResourceNotFoundException("Customer Id doesn't exist");
        } catch (StripeException e) {
            LOGGER.error("Error during charging {}", e);
            throw new ResourceNotFoundException(e.getMessage());
        }
    }
    public List<PaymentMethod> getAll(Integer userId){

        try {
            Stripe.apiKey = secretKey;

            String customerId = userProfileService.getStripeIdByUserId(userId);
            if(customerId == null) {
              return new ArrayList<>();
            }
            Map<String, Object> cardParams = new HashMap<String, Object>();
            cardParams.put("limit", 10);
            cardParams.put("object", "card");
            Customer customer = Customer.retrieve(customerId);
            ExternalAccountCollection list = customer.getSources().list(cardParams);
            List<PaymentMethod> methods = new ArrayList<>();
            String defaultCardId = customer.getDefaultSource();
            for (ExternalAccount card : list.getData()) {
                if(card instanceof Card) {
                    PaymentMethod paymentMethod = new PaymentMethod((Card) card);
                    paymentMethod.setSelected(card.getId().equals(defaultCardId));
                    methods.add(paymentMethod);
                }
            }
            return methods;
        } catch (StripeException e) {
            LOGGER.error("Error during charging {}", e);
        }
        return new ArrayList<>();
    }
    public OrderDto executeTransaction(Integer userid, Integer product, ChargeRequest chargeRequest, String userEmail, ProductType productType) {
        String customerId = userProfileService.getStripeIdByUserId(userid);
        if(customerId == null) {
            throw new ResourceNotFoundException("Customer Id doesn't exist");
        }

            Map<String, Object> chargeParams = new HashMap<>();
            chargeParams.put("amount", chargeRequest.getAmount().intValue()*100);
            chargeParams.put("currency", ChargeRequest.Currency.USD);
            chargeParams.put("customer", customerId);
        try {
            Stripe.apiKey = secretKey;
            Charge charge = Charge.create(chargeParams);

            PaymentMethod defaultCard = getDefaultCard(userid);

            OrderDto order =   orderService.addOrder(product, productType.toOrderType() , chargeRequest.getAmount(), chargeRequest.getQuantity(),
                userid, chargeRequest.getCoupon(), charge.getBalanceTransaction(), PaymentMethodType.STRIPE,defaultCard.getCardNumber(),defaultCard.getMethodType());
        if (order != null) {
            sendConfirmationEmail(order, userEmail);
            return order;
        }
        return order;
        } catch (StripeException e) {
            LOGGER.error("Error during charging {}", e);
            return null;
        }
    }


    public OrderDto executeTransaction(Integer userid, ChargeRequest chargeRequest, List<OrderDetailDto> orderDetailDtos, String userEmail) {
        String customerId = userProfileService.getStripeIdByUserId(userid);
        if(customerId == null) {
            throw new ResourceNotFoundException("Customer Id doesn't exist");
        }

        Map<String, Object> chargeParams = new HashMap<>();
        chargeParams.put("amount", chargeRequest.getAmount().intValue()*100);
        chargeParams.put("currency", ChargeRequest.Currency.USD);
        chargeParams.put("customer", customerId);
        try {
            Stripe.apiKey = secretKey;
            Charge charge = Charge.create(chargeParams);
                OrderDto order =   orderService.addOrder(orderDetailDtos,  chargeRequest.getAmount(), userid,
                        chargeRequest.getCoupon(), charge.getBalanceTransaction(), PaymentMethodType.STRIPE);
            if (order != null) {
                sendConfirmationEmail(order, userEmail);
                return order;
            }
            return order;
        } catch (StripeException e) {
            LOGGER.error("Error during charging {}", e);
            return null;
        }
    }
    private void sendConfirmationEmail(OrderDto order, String userEmail){
        try {

        EmailTemplateDto emailTemplate =
                emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.PAYMENT_CONFIRMATION, "EN");

        emailTemplate.getVariables().put("##ORDER_NUMBER##", String.valueOf(order.getTransactionID()));
        emailTemplate.getVariables().put("##AMOUNT##", String.valueOf(order.getTotalPrice()));

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        emailTemplate.getVariables().put("##DATE##", dateFormat.format(new Date()));

        if(order.getUser() != null)
            userEmail = order.getUser().getEmail();

        new EmailTemplateFactory()
                .build(baseUrl, userEmail, templateEngine, mailService, emailTemplate)
                .send();

        }catch (Exception ex) {

        }
    }

}
