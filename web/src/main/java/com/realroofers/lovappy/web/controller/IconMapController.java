package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by hasan on 24/7/2017.
 */

@Controller
@RequestMapping("/icons")
public class IconMapController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MobileController.class);
    private CMSService cmsService;
    public IconMapController(CMSService cmsService) {
        this.cmsService = cmsService;
    }

    @GetMapping
    public String iconMap(Model model) {
        return "icon-map";
    }

}
