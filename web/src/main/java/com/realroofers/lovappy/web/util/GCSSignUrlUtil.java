package com.realroofers.lovappy.web.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

@Component
public class GCSSignUrlUtil {

    public String createSignedUrl(String bucketName, String objectName) throws Exception {
        String OBJECT_PATH = "/" + bucketName + "/" + objectName;
        String FULL_OBJECT_URL = "https://storage.googleapis.com" + OBJECT_PATH;

        TypeReference<GoogleKeyFile> mapType = new TypeReference<GoogleKeyFile>() {};
        InputStream is = getClass().getResourceAsStream("/Lovappy-gcloud-key.json");
        GoogleKeyFile key = new ObjectMapper().readValue(is, mapType);

        // Set Url expiry to one minute from now
        String expiryTime = getExpiryTimeInEpoch();

        String stringToSign = "GET" + "\n"
                + "" + "\n"
                + "" + "\n"
                + expiryTime + "\n"
                + OBJECT_PATH;
        PrivateKey pk = getPrivateKey(key);
        String signedString = getSignedString(stringToSign, pk);

        // URL encode the signed string so that we can add this URL
        signedString = URLEncoder.encode(signedString, "UTF-8");

        return FULL_OBJECT_URL
                + "?GoogleAccessId=" + key.getClient_email()
                + "&Expires=" + expiryTime
                + "&Signature=" + signedString;
    }


    // Represented as the epoch time (seconds since 1st January 1970)
    private String getExpiryTimeInEpoch() {
        long now = System.currentTimeMillis();
        // expire in a minute!
        // note the conversion to seconds as needed by GCS.
        long expiredTimeInSeconds = (now + 60 * 1000L) / 1000;

        return expiredTimeInSeconds + "";
    }


    // Use SHA256withRSA to sign the request
    private String getSignedString(String input, PrivateKey pk) throws Exception {
        Signature privateSignature = Signature.getInstance("SHA256withRSA");
        privateSignature.initSign(pk);
        privateSignature.update(input.getBytes("UTF-8"));
        byte[] s = privateSignature.sign();
        return Base64.getEncoder().encodeToString(s);
    }


    // Get private key object from unencrypted PKCS#8 file content
    private PrivateKey getPrivateKey(GoogleKeyFile key) throws Exception {
        // Remove extra characters in private key.
        String realPK = key.getPrivate_key().replaceAll("-----END PRIVATE KEY-----", "")
                .replaceAll("-----BEGIN PRIVATE KEY-----", "").replaceAll("\n", "");
        byte[] b1 = Base64.getDecoder().decode(realPK);
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(b1);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }

}
