package com.realroofers.lovappy.web.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.context.annotation.*;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationManager;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.InMemoryAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.expression.OAuth2MethodSecurityExpressionHandler;
import org.springframework.security.oauth2.provider.password.ResourceOwnerPasswordTokenGranter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import com.realroofers.lovappy.web.config.SecurityConfig;
import javax.sql.DataSource;


/**
 * Created by Daoud Shaheen on 02/09/2018.
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class OAuth2ServerConfiguration extends GlobalMethodSecurityConfiguration {
//
  private final static String RESOURCE_ID = "lovappy";

  protected UserAuthorizationService userAuthorizationService;


  public OAuth2ServerConfiguration(@Qualifier("userAuthorizationService") UserAuthorizationService userAuthorizationService) {
    this.userAuthorizationService = userAuthorizationService;
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userAuthorizationService);
  }

  @Override
  protected MethodSecurityExpressionHandler createExpressionHandler() {
    OAuth2MethodSecurityExpressionHandler handler = new OAuth2MethodSecurityExpressionHandler();

    /**
     * override spring default Role prefix {@link: DefaultMethodSecurityExpressionHandler}
     */
    handler.setDefaultRolePrefix("");
    return handler;
  }
  @Configuration
  @EnableResourceServer
  @AutoConfigureBefore(SecurityConfig.class)
  protected static class ResourceServer extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
      http
              .antMatcher("/api/v2/**")
              .authorizeRequests()
              .antMatchers("/api/v2/login","/api/v2/user/password/reset","/api/v2/radio").permitAll()
              .antMatchers("/api/v2/social/**/login","/api/v2/signup").permitAll()
              .anyRequest().authenticated()
             .and()
    .exceptionHandling().authenticationEntryPoint(new CustomAuthenticationEntryPoint())
          .accessDeniedHandler(new CustomAccessDeniedHandler());
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
      resources.resourceId(RESOURCE_ID);
      resources.authenticationManager(new OAuth2AuthenticationManager());
    }
  }

  @Configuration
  @EnableAuthorizationServer
  protected static class AuthorizationConfig extends AuthorizationServerConfigurerAdapter {

    private AuthenticationManager authenticationManager;
    private DataSource dataSource;
    private UserAuthorizationService userAuthorizationService;

    AuthorizationConfig(@Qualifier("userAuthorizationService")UserAuthorizationService userAuthorizationService,
        AuthenticationManager authenticationManager, DataSource dataSource) {
      this.authenticationManager = authenticationManager;
      this.dataSource = dataSource;
      this.userAuthorizationService = userAuthorizationService;
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
      clients.jdbc(dataSource);

    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
      endpoints.tokenStore(tokenStore())
          .allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST)
          .authenticationManager(authenticationManager)

           .authorizationCodeServices(authorizationCodeServices())
              .tokenStore(tokenStore())
          .userDetailsService(userAuthorizationService);
    }

    @Bean
    protected AuthorizationCodeServices authorizationCodeServices() {
      return new JdbcAuthorizationCodeServices(dataSource);
    }

    @Bean
    public TokenStore tokenStore() {

      return new JdbcTokenStore(dataSource);
    }

    @Bean
    @Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
    @Primary
    public DefaultTokenServices tokenServices() {
      DefaultTokenServices tokenServices = new DefaultTokenServices();
      tokenServices.setTokenStore(tokenStore());
      tokenServices.setSupportRefreshToken(true);
      tokenServices.setReuseRefreshToken(false);

      return tokenServices;
    }

    @Bean
    public ClientService clientService() {
      return new ClientServiceImpl(dataSource);
    }

  }
}