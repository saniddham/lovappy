package com.realroofers.lovappy.web.util;

import com.realroofers.lovappy.web.util.dto.DownloadFile;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.clapper.util.misc.MIMETypeUtil;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

/**
 * A utility that downloads a file from a URL.
 * Created by Manoj on 01/04/2018.
 */
@Slf4j
public class HttpDownloadUtility {

    /**
     * Downloads a file from a URL
     *
     * @param fileURL HTTP URL of the file to be downloaded
     * @throws IOException
     */
    public static synchronized DownloadFile downloadFile(String fileURL) throws IOException {
        URL url = new URL(fileURL);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        int responseCode = httpConn.getResponseCode();
        DownloadFile downloadFile = new DownloadFile();

        // always check HTTP response code first
        if (responseCode == HttpURLConnection.HTTP_OK) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");
            String contentType = httpConn.getContentType();
            int contentLength = httpConn.getContentLength();

            if (disposition != null) {
                // extracts file name from header field
                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10,
                            disposition.length() - 1);
                }
            } else {
                // extracts file name from URL
                String extension = MIMETypeUtil.fileExtensionForMIMEType(contentType);
                fileName = new Date().getTime() + "." + extension;
            }

            // opens input stream from the HTTP connection
            InputStream inputStream = httpConn.getInputStream();
            byte[] imageBytes = IOUtils.toByteArray(inputStream);

            downloadFile.setFileName(fileName);
            downloadFile.setContentLength(contentLength);
            downloadFile.setContentType(contentType);
            downloadFile.setImageBytes(imageBytes);

            inputStream.close();

            log.debug(fileName + " :: File downloaded");
        } else {
            log.debug("No file to download. Server replied HTTP code: " + responseCode);
        }
        httpConn.disconnect();
        return downloadFile;
    }
}