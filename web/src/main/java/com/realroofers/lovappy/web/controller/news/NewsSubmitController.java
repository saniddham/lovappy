package com.realroofers.lovappy.web.controller.news;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.news.NewsCategoryService;
import com.realroofers.lovappy.service.news.NewsCrawlerService;
import com.realroofers.lovappy.service.news.NewsStoryService;
import com.realroofers.lovappy.service.news.dto.NewsStoryDto;
import com.realroofers.lovappy.service.news.model.NewsCategory;
import com.realroofers.lovappy.service.news.model.NewsStory;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

/**
 * @author Shehan Wijesinghe (shehansrw@gmail.com)
 */

@Controller
@RequestMapping("/news")
public class NewsSubmitController {
    private static Logger LOG = LoggerFactory.getLogger(NewsSubmitController.class);

    @Autowired
    private CloudStorage cloudStorage;

    @Autowired
    NewsCategoryService newsCategoryService;
    @Autowired
    NewsCrawlerService newsCrawlerService;

    @Autowired
    private NewsStoryService newsStoryService;


    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @GetMapping("/submit")
    public String initialNewsSubmitPage(Model model,
                                        @ModelAttribute("message") String flashAttr) {

        List<NewsCategory> newsCategories = newsCategoryService.getActiveNewsCategory();
        model.addAttribute("newsCategories", newsCategories);
        model.addAttribute("newsStoryDto", new NewsStoryDto());
        model.addAttribute("message", flashAttr);
        return "news/submit";
    }

    @PostMapping("/submit")
    public ModelAndView initialNewsSubmit(@Valid NewsStoryDto newsStoryDto, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        ModelAndView model = new ModelAndView();
        List<NewsCategory> newsCategories = newsCategoryService.getAllNewsCategory();
        model.addObject("newsCategories", newsCategories);
        try {
            String[] output = newsStoryDto.getAdvancedOptionKeywordPhrases().split("\\,");
            if (output.length > 2) {
                bindingResult.rejectValue("advancedOptionKeywordPhrases", "error.newsStoryDto", "Max keyword phrases are 2");
            }
            if (null != newsStoryService.getNewsStoryByTitle(newsStoryDto.getNewsStoryTitle())) {
                bindingResult.rejectValue("newsStoryTitle", "error.newsStoryDto", "Change the title , Same Title founded");
            }
        } catch (NullPointerException ee) {
            ee.printStackTrace();
            LOG.error(ee.getMessage());
        }
        newsStoryDto.setSource("lovappy news submits");

        if (bindingResult.hasErrors()) {
            if (newsStoryDto.getNewsPicture() == null || newsStoryDto.getNewsPicture().getId()==null)
                bindingResult.rejectValue("newsPicture", "error.newsStoryDto", "upload an image with your post");

            if (newsStoryDto.getNewsAuthorPicture() == null || newsStoryDto.getNewsAuthorPicture().getId()==null)
            bindingResult.rejectValue("newsAuthorPicture", "error.newsStoryDto", "upload an identification image");
            model.setViewName("news/submit");
        } else {

            CloudStorageFileDto vedio = null;
            try {
                if (newsStoryDto.getVideoUploadFile() != null && !newsStoryDto.getVideoUploadFile().isEmpty()) {
                    vedio = cloudStorage.uploadFile(newsStoryDto.getVideoUploadFile(), imagesBucket, FileExtension.png.toString(), newsStoryDto.getVideoUploadFile().getContentType());
                }

                NewsStory newsStory = newsStoryService.saveNewsStory(newsStoryDto);

                if (vedio != null) {
                    newsStory.setNewsVideo(new CloudStorageFile(vedio.getId()));
                }

                newsStoryService.saveNewsStory(newsStory);
                redirectAttributes.addFlashAttribute("message", "Success");
            } catch (IOException e) {
                e.printStackTrace();
                LOG.error(e.getMessage());
            } catch (Exception e) {
                e.printStackTrace();
                LOG.error(e.getMessage());
            }
            model.setViewName("redirect:/news/submit");
        }
        return model;
    }

    @GetMapping("/news")
    public String getNews() {
        newsCrawlerService.getNewsFromOtherSources();

        return "redirect:/submit";
    }

}