package com.realroofers.lovappy.web.support;

import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * Converts yyyy-MM-dd string to and from java.util.Date. Compatible to forms with <input type="date" />
 *
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public class DatePropertyEditorSupport extends PropertyEditorSupport {
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
    private static final Pattern PATTERN = Pattern.compile("^\\d{2}/\\d{2}/\\d{4}$");

    public String getJavaInitializationString() {
        Object var1 = this.getValue();
        return var1 != null ? var1.toString() : "null";
    }

    public void setAsText(String value) {
        Object result = null;
        if (StringUtils.isNotBlank(value)) {
            value = value.trim();
            if (PATTERN.matcher(value).matches()) {
                try {
                    result = DATE_FORMAT.parse(value.trim());
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
        setValue(result);
    }

    public String getAsText() {
        Object var1 = this.getValue();
        return var1 instanceof Date ? DATE_FORMAT.format((Date) var1) : null;
    }
}
