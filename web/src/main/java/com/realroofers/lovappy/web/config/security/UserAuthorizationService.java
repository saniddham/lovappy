package com.realroofers.lovappy.web.config.security;

import com.realroofers.lovappy.service.core.SocialType;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.dto.RoleDto;
import com.realroofers.lovappy.service.user.dto.UserAuthDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.*;
import com.realroofers.lovappy.service.user.repo.RoleRepo;
import com.realroofers.lovappy.service.user.repo.SocialProfileRepo;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import com.realroofers.lovappy.service.user.repo.UserRolesRepo;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedClientException;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.UserProfileBuilder;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestOperations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Daoud Shaheen on 02/09/2017.
 */
@Service("userAuthorizationService")
public class UserAuthorizationService implements UserDetailsService {

  private static final Logger logger = LoggerFactory.getLogger(UserAuthorizationService.class);
  @Autowired
  private  UserRepo userRepo;
  @Autowired
  private RoleRepo roleRepo;
  @Autowired
  private UserRolesRepo userRolesRepo;
  @Autowired
  private PasswordEncoder passwordEncoder;
  @Autowired
  private SocialProfileRepo socialProfileRepo;
  @Transactional(readOnly = true)
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
     User user = userRepo.findOneByEmail(username);

    if (user == null) {
      throw new UsernameNotFoundException("User '" + username + "' is not found");
    }

    return new UserAuthDto(user.getUserId(), user.getEmail(), user.getPassword(), user.getEmailVerified(), true, true, true, getGrantedAuthorities(user.getRoles()));

  }

  /**
   * Returns granted authorities from a given role list
   *
   * @param roles user roles
   * @return List<GrantedAuthority>
   */
  public List<GrantedAuthority> getGrantedAuthorities(Collection<UserRoles> roles) {
    List<GrantedAuthority> authorities = new ArrayList<>();

    for (UserRoles role : roles) {
      //if(role.getApprovalStatus()== ApprovalStatus.APPROVED)
      authorities.add(new SimpleGrantedAuthority(role.getId().getRole().getName()));
    }
    return authorities;
  }
  public UserDetails getCurrentLoginUser() {
    String username = SecurityContextHolder.getContext().getAuthentication().getName();
    return loadUserByUsername(username);
  }


  @Transactional(readOnly = true)
  public UserDto login(String username, String password) throws UsernameNotFoundException {
    User user = userRepo.findOneByEmail(username);

    if (user == null) {
      throw new UsernameNotFoundException("User '" + username + "' is not found");
    }

    UserAuthDto userAuth =  new UserAuthDto(user.getUserId(), user.getEmail(), user.getPassword(), user.getEmailVerified(), true, true, true, getGrantedAuthorities(user.getRoles()));

    if (userAuth != null) {
//      if (!user.getPassword().equals(password)) {
//        logger.error("User {} used a wrong password", username);
////        throw new UserBadCredentialsException("Incorrect password");
//      }
    } else {
      logger.error("User {} email dose not exist", username);
//      throw new UserBadCredentialsException("email dose not exist");
    }

    //create user authentication object used later for authentication
    Authentication authentication = new UsernamePasswordAuthenticationToken(username,
            userAuth.getPassword(), userAuth.getAuthorities());
    SecurityContextHolder.getContext().setAuthentication(authentication);

    return new UserDto(user);
  }

  @Transactional(readOnly = true)
  public UserAuthDto socialLogin(Connection<?> connection, String socialPlatform, String email) {
    String socialId = connection.getKey().getProviderUserId();
    if (connection != null) {
      UserProfile socialMediaProfile = null;
      if(socialPlatform.equals(SocialType.FACEBOOK.name().toLowerCase())) {
        socialMediaProfile = facebookUser(connection);
      } else  if(socialPlatform.equals(SocialType.TWITTER.name().toLowerCase())) {
        socialMediaProfile = twitterUser(connection);
      } else if(socialPlatform.equals(SocialType.INSTAGRAM.name().toLowerCase())){
        socialMediaProfile = connection.fetchUserProfile();
      }



      return fromSocialToAuthUser(socialMediaProfile, socialPlatform, email, socialId);
    }
    return null;
  }

  public UserAuthDto fromSocialToAuthUser(UserProfile socialMediaProfile, String socialPlatform, String emailIn, String socialId){
    Object emailO = socialMediaProfile.getEmail();
    logger.info("email  [{}]", emailO);
    if (emailO == null ) {
      if(emailIn == null) {
        throw new UnauthorizedClientException("User did not allow accessing " + socialPlatform + " email.");
      }
      emailO = emailIn;
    }
    String email = emailO.toString();
    String firstName;
    String lastName = null;

    firstName = socialMediaProfile.getFirstName();
    lastName = socialMediaProfile.getLastName();

    SocialProfile socialProfile = socialProfileRepo.findBySocialIdAndSocialPlatform(socialMediaProfile.getId(), SocialType.fromString(socialPlatform));

    User user = null;

    UserAuthDto dto = null;
    if(socialProfile != null) {
      user = socialProfile.getUser();
    }
    if (user == null) {
      dto = new UserAuthDto();
      dto.setPassword(socialId);
    } else {
      dto = new UserAuthDto(user.getUserId(),
              user.getEmail(), user.getPassword(), user.isEnabled(), true, true,
              true, getGrantedAuthorities(user.getRoles()));
      if(StringUtils.isEmpty(user.getPassword())){
        dto.setPassword(socialId);
      }
    }
    dto.setEmail(email);
    dto.setSocialId(socialId);
    dto.setSocialPlatform(socialPlatform);
    dto.setFirstName(firstName);
    dto.setLastName(lastName);
    return dto;


  }
  private UserProfile facebookUser(Connection<?> connection) {
    Facebook facebook = (Facebook) connection.getApi();
    String[] fields = {"id", "email", "first_name", "last_name"};
    org.springframework.social.facebook.api.User profile = facebook.fetchObject("me", org.springframework.social.facebook.api.User.class, fields);
    return (new UserProfileBuilder()).setId(profile.getId()).setName(profile.getName()).setFirstName(profile.getFirstName()).setLastName(profile.getLastName()).setEmail(profile.getEmail()).build();
  }

  private UserProfile twitterUser(Connection<?> connection) {
    Twitter twitter = (Twitter) connection.getApi();

    RestOperations restTemplate = twitter.restOperations();
    HashMap<String, Object> response = restTemplate.getForObject("https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true", HashMap.class);

    logger.debug("response [{}]", response);
    String socialId = response.get("id_str").toString();

    String firstName;
    String lastName = null;

    String fullName = response.get("name").toString();
    String[] names = fullName.split(" ");
    if (names.length == 3) {
      firstName = names[0];
      lastName = names[2];
    } else if (names.length == 2) {
      firstName = names[0];
      lastName = names[1];
    } else {
      firstName = fullName;
    }
   return (new UserProfileBuilder()).setId(socialId).setName(fullName)
            .setFirstName(firstName).setLastName(lastName).setEmail(response.get("email").toString()).build();
  }


  /**
   * Login service for admin
   *
   * @param username
   * @param password
   * @return
   */
  @Transactional(readOnly = true)
  public String developerLogin(String username, String password) {
    try {
      User user = userRepo.findOneByEmail(username);

      if (user == null) {
        throw new UsernameNotFoundException("User '" + username + "' is not found");
      }

      UserRoles userRole = userRolesRepo.findOne(new UserRolePK(user, roleRepo.findByName(Roles.DEVELOPER.getValue())));

      if(userRole == null) {
        return "Access Denied";
      }
      if (user != null) {
        logger.info("password [{}], [{}],[{}]",userRole.getPassphrase(),  passwordEncoder.encode(password), passwordEncoder.matches(password, userRole.getPassphrase()));
        if (true) {


          UserAuthDto userDetails = new UserAuthDto(user.getUserId(), user.getEmail(), user.getPassword(), true, true, true, true, getGrantedAuthorities(user.getRoles()));
          //create user authentication object used later for authentication
          UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails,
                  user.getPassword(), userDetails.getAuthorities());

          SecurityContextHolder.getContext().setAuthentication(authentication);
          return "success";
        }else {
          logger.error("Invalid Password " + password);
          return "Invalid Password";


        }
      }
    } catch (UsernameNotFoundException ex) {
      return "User Not Found";
    }
    return "failed";
  }
}