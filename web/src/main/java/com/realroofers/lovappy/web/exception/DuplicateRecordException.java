package com.realroofers.lovappy.web.exception;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public class DuplicateRecordException extends RuntimeException {
    public DuplicateRecordException(String message) {
        super(message);
    }
}
