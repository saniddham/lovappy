package com.realroofers.lovappy.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Daoud Shaheen on 6/10/2017.
 */
@Controller
@RequestMapping("/infographics")
public class InfographicController {

    private static final Logger LOGGER = LoggerFactory.getLogger(InfographicController.class);

    @GetMapping
    public String infographic(Model model) {
        return "info/infographic";
    }


    
    //Gift
    @GetMapping("/gifts")
    public ModelAndView giftinfographic(ModelAndView model) {
        model.setViewName("info/giftinfographic");
        return model;
    }
    
    //advertise
    @GetMapping("/advertising")
    public ModelAndView eventsinfographic(ModelAndView model) {
        model.setViewName("info/advertise-infographic");
        return model;
    }
    
    //lovappy
    @GetMapping("/dating")
    public ModelAndView lovebloginfographic(ModelAndView model) {
        model.setViewName("info/lovappy-infographic");
        return model;
    }
    
    //married
    @GetMapping("/couples")
    public ModelAndView datingplacesinfographic(ModelAndView model) {
        model.setViewName("info/married-infographic");
        return model;
    }
    
    //DatingPlaces
    @GetMapping("/music")
    public ModelAndView musicinfographic(ModelAndView model) {
        model.setViewName("info/musicinfographic");
        return model;
    }

}
