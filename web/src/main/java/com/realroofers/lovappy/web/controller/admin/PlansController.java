package com.realroofers.lovappy.web.controller.admin;

import com.realroofers.lovappy.service.order.PriceService;
import com.realroofers.lovappy.service.order.dto.PriceDto;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.plan.PlansService;
import com.realroofers.lovappy.service.plan.dto.FeatureDetailsDto;
import com.realroofers.lovappy.service.plan.dto.PlanDetailsDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by hasan on 7/11/2017.
 */
@Controller
@RequestMapping("/lox")
public class PlansController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PageController.class);
    private PlansService plansService;
    private PriceService priceService;

    public PlansController(PlansService plansService,
                           PriceService priceService) {
        this.plansService = plansService;
        this.priceService = priceService;
    }

    @GetMapping("/plans")
    public ModelAndView pricingPage(ModelAndView model) {
        model.setViewName("admin/plans_pricing/plans_pricing");
        model.addObject("plans", plansService.allPlans());
        model.addObject("prices", priceService.getAll(new PageRequest(0, 100)));
        return model;
    }

    @GetMapping("/plans/{id}")
    public ModelAndView planDetails(ModelAndView modelAndView, @PathVariable(value = "id") Integer id) {
        modelAndView.setViewName("admin/plans_pricing/plan_details");
        modelAndView.addObject("plan", plansService.getPlanDetails(id, true));

        return modelAndView;
    }


    @GetMapping("/prices/{id}")
    public ModelAndView priceDetails(ModelAndView modelAndView, @PathVariable(value = "id") Integer id) {
        modelAndView.setViewName("admin/plans_pricing/price_details");
        modelAndView.addObject("price", priceService.get(id));

        return modelAndView;
    }

    @GetMapping("/prices/{id}/save")
    public ModelAndView savePrice(ModelAndView modelAndView, @PathVariable(value = "id") Integer id,
                                  @RequestParam(value = "price", required = false) double price,
                              @RequestParam(value = "type", required = false) OrderDetailType type) {

        PriceDto priceDto = new PriceDto();
        priceDto.setId(id);
        priceDto.setPrice(price);
        priceDto.setType(type);
        priceService.update(priceDto);
        modelAndView.setViewName("redirect:/lox/plans");
        return modelAndView;
    }

    @GetMapping("/plans/new")
    public ModelAndView addPlan(ModelAndView model) {
        model.addObject("plan", new PlanDetailsDto());
        model.setViewName("admin/plans_pricing/plan_details");
        return model;
    }

    @PostMapping("/plans/save")
    public ResponseEntity savePlan(@RequestParam("planID") Integer id,
                                   @RequestParam("planName") String planTitle,
                                   @RequestParam("subTitle") String planSubtitle,
                                   @RequestParam("colorScheme") String colorScheme,
                                   @RequestParam("cost") Integer cost,
                                   @RequestParam("costDescription") String costDescription,
                                   @RequestParam("isValid") Boolean isValid) {

        PlanDetailsDto planDetailsDto = plansService.savePlan(new PlanDetailsDto(id, planTitle, planSubtitle, colorScheme, null, isValid,
                cost, costDescription));
        return ResponseEntity.status(HttpStatus.OK).body(planDetailsDto.getPlanID());
    }


    @GetMapping("/plans/feature/new/{planID}")
    public ModelAndView newFeature(ModelAndView model, @PathVariable(value = "planID") Integer planID) {
        model.setViewName("admin/plans_pricing/feature_details");
        FeatureDetailsDto featureDetailsDto = new FeatureDetailsDto();
        featureDetailsDto.setPlanId(planID);
        model.addObject("planName", plansService.getPlanDetails(planID, true).getPlanName());
        model.addObject("feature", featureDetailsDto);

        return model;
    }

    @GetMapping("/plans/feature/{id}")
    public ModelAndView updateFeature(ModelAndView model, @PathVariable(value = "id") Integer id) {
        model.setViewName("admin/plans_pricing/feature_details");
        FeatureDetailsDto featureDetailsDto = plansService.getFeatureByID(id);
        model.addObject("planName", plansService.getPlanDetails(featureDetailsDto.getPlanId(),true).getPlanName());
        model.addObject("feature", featureDetailsDto);

        return model;
    }

    @PostMapping("/plans/feature/save")
    public ResponseEntity saveFeature(@RequestParam("featureID") Integer featureId,
                                      @RequestParam("planId") Integer planId,
                                      @RequestParam("featureDetails") String featureDetails,
                                      @RequestParam("featureNote") String featureNote,
                                      @RequestParam("noteColor") String noteColor,
                                      @RequestParam("isValid") Boolean isValid) {

        FeatureDetailsDto featureDetailsDto = plansService.saveFeature(new FeatureDetailsDto(featureId, planId, featureDetails, featureNote, noteColor, isValid));
        return ResponseEntity.status(HttpStatus.OK).body(featureDetailsDto.getFeatureID());
    }
}
