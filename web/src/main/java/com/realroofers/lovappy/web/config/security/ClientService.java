package com.realroofers.lovappy.web.config.security;

import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationService;

public interface ClientService extends ClientDetailsService, ClientRegistrationService {

}
