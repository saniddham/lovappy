package com.realroofers.lovappy.web.config;

import com.realroofers.lovappy.service.cms.utils.PageConstants;
import com.realroofers.lovappy.service.couples.CoupleProfileService;
import com.realroofers.lovappy.service.datingPlaces.DatingPlaceService;
import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.survey.UserSurveyService;
import com.realroofers.lovappy.service.user.*;
import com.realroofers.lovappy.service.user.dto.SocialProfileDto;
import com.realroofers.lovappy.service.user.dto.UserAuthDto;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.vendor.VendorService;
import com.realroofers.lovappy.web.config.security.AuthenticationFilter;
import com.realroofers.lovappy.web.config.security.UserAuthorizationService;
import com.realroofers.lovappy.web.util.DatingPlacesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.CompositeFilter;

import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Configuration
@EnableWebSecurity
@EnableOAuth2Client
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityConfig.class);

    @Qualifier("oauth2ClientContext")
    @Autowired
    private OAuth2ClientContext oauth2ClientContext;
    @Autowired
    private UserService userService;
    @Autowired
    private CoupleProfileService coupleProfileService;
    @Qualifier("userAuthorizationService")
    @Autowired
    private UserAuthorizationService userAuthorizationService;
    @Autowired
    private UserProfileService userProfileService;
    @Autowired
    private UserPreferenceService userPreferenceService;
    @Autowired
    private LovstampService lovstampService;
    @Autowired
    private UserSurveyService userSurveyService;
    @Autowired
    private UserProfileAndPreferenceService userProfileAndPreferenceService;

    @Autowired
    private RequestCache requestCache;
    @Autowired
    private AuthenticationFilter authenticationFilter;

    @Autowired
    private DatingPlaceService datingPlaceService;
    @Autowired
    private DatingPlacesUtil datingPlacesUtil;
    @Autowired
    private NotificationService notificationService;

    @Autowired
    protected TwitterTemplate twitterTemplate;

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private SocialProfileService socialProfileService;
    @Autowired
    private AuthenticationSuccessHandlerAndRegistrationFilter authenticationSuccessHandlerAndRegistrationFilter;

    @Autowired
    private VendorService vendorService;

    @Value("${spring.profiles.active}")
    private String activeProfile;

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/favicon.ico","/.well-known/**", "/upload/**", "/css/**", "/js/**", "/fonts/**", "/images/**", "/webjars/**", "**/gallery/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.antMatcher("/**").authorizeRequests()
                .antMatchers("/", "/googleabeef86397d0e2ef.html", "/login**", "/register**", "/register/**", "/reset", "/register/verify/**",
                        "/radio", "/radio/new", "/radio/profiles", "/api/v1/lovappy_radio", "/api/v1/radio", "/api/v1/ads", "/api/v1/ads/calculate_price",
                        "/blog**",  "/blog/posts/**", "/blog/search**", "/share/post/facebook",
                        "/send_reset_email**", "/set_new_password/**", "/set_new_password**",
                        "/features", "/features/**", "/plans-and-pricing", "/mobile", "/about-us", "/contact-us", "/advertise/review",
                        "/advertise/audience", "/ad/add", "/ad/upload_cover_image", "/ad/upload_audio", "/blog/lovappy/posts**", "/blog/category/**",
                        "/blog/vlogs**", "/blog/vlogs/**", "/blog/posts/gallery", "/blog/posts/featured", "/blog/share/**", "/blog/posts/**/content", "/radio/share/**",
                        "/pages/**", "/lox", "/lox/**", "/following", "/how-this-works","/vendor/mobile/verification",
                        "/user/registration/musician/**", "/user/registration/photo",
                        "/user/registration/ambassador/**", "/user/registration/signup", "/user/registration/new/signup",
                        "/user/registration/author/**","/vendor/mobile/**","/vendor/mobile/verify",
                         "/terms",
                        "/privacy",
                        "/api/v1/user/login**",
                        "/register/how-this-works",
                        "/api/v1/blogs/**/share/**", "/infographic", "/events/**", "/dating/places/**",
                        "/api/v1/ads/add**", "/api/v1/ads/cover_image**", "/api/v1/ads/audio**", "/api/v1/ads/video**",
                        "/gifts",
                        "/music/infographic", "/blog/lovblog", "/events", "/team", "/icons", "/email",
                        "/music/sell-your-music", "/music/send-music",
                        "/blog/help", "/couples_reset_password", "/music",
                        "/music/search",
                        "/music/**/details","/video_blogger/registration",
                        "/infographics/gifts",
                        "/signin/twitter",
                        "/infographics/advertising",
                        "/infographics/dating",
                        "/advertise/**",
                        "/faq",
                        "/affiliate",
                        "/careers","/careers/**",
                         "/voice-dating/**",
                        "/user/registration/blogger",
                        "/ambassador/info",
                        "/patent",
                        "/musician/profile",
                        "/musician/settings",
                        "/blogger/profile",
                        "/blogger/settings",
                        "/ambassador/profile",
                        "/ambassador/settings",
                        "/findlove",
                        "/sitemap",
                        "/radio/**",
                        "/faq/**",
                         "/couples/password/**",
                        "/couples/verify/**",
                        "/developer/**",
                        "/ambassadors",
                        "/infographics/couples",
                        "/music/signup",
                        "/dating/place/verification",
                        "/api/v2/login",
                        "api/v2/music/inbox/responses",
                        "/connect/twitter",
                        "/signin/social/twitter",
                        "/signin/instagram",
                        "/home/register",
                        "/api/v2/social/**/login","/news/press",
                        "/dating/place/deals/submit","/homenew",
                        "/temp/**", "/error/**", "/vendor/registration", "/vendor/register/brand", "/vendor/signup",
                        "/infographic-library", "/happy-couples", "/api/v1/gallery/photo", "/music/purchase/terms",
                        "/music/musician/help","/news/approved","/news/latest",
                        "/.well-known/pki-validation/6e70612b67ca4dbc9d784bef314cd705.txt").permitAll()
                .antMatchers("/jsondoc-ui.html").hasAuthority("DEVELOPER")
                .anyRequest().authenticated()
                //.and().exceptionHandling().accessDeniedPage("/error/403")
                .and().formLogin().loginPage("/login").loginProcessingUrl("/login").successHandler(authenticationSuccessHandlerAndRegistrationFilter)
                .failureUrl("/login?error=" + PageConstants.INVALID_PASSWORD_ERROR)
                //.and().exceptionHandling().authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/login"))
                .and().logout().logoutSuccessUrl("/").permitAll()

                .and().addFilterBefore(ssoFilter(), BasicAuthenticationFilter.class)
                .addFilterAfter(authenticationSuccessHandlerAndRegistrationFilter, BasicAuthenticationFilter.class)
                .addFilterAfter(authenticationFilter, authenticationFilter.getClass());
        //walk around for the http error: 403 - Could not verify the provided CSRF token because your session was not found
        //when using non browser clients like Postman
        http.csrf().disable();
        http.headers().frameOptions().sameOrigin();

        if(!StringUtils.isEmpty(activeProfile) && (activeProfile.equals("production") || activeProfile.equals("testing"))) {
            http.requiresChannel().anyRequest().requiresSecure();
        }
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
        auth.userDetailsService(userAuthorizationService);
    }

    @Bean
    public FilterRegistrationBean oauth2ClientFilterRegistration(OAuth2ClientContextFilter filter) {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(filter);

        registration.setOrder(-100);
        return registration;
    }

    private SocialProfileDto toSocialUser(UserAuthDto dto, String name){
        SocialProfileDto socialProfileDto = new SocialProfileDto();
        socialProfileDto.setFirstName(dto.getFirstName());
        socialProfileDto.setLastName(dto.getLastName());
        socialProfileDto.setSocialId(dto.getSocialId());
        socialProfileDto.setSocialPlatform(dto.getSocialPlatform());
        socialProfileDto.setImageUrl(dto.getImageUrl());
        socialProfileDto.setName(name);
        return socialProfileDto;
    }
    private Filter ssoFilter() {
        CompositeFilter filter = new CompositeFilter();
        List<Filter> filters = new ArrayList<>();

        Function<String, PrincipalExtractor> createSocialExtractor = (socialPlatform) -> (map) -> {

            if(socialPlatform.equals("instagram")) {
                map = (Map<String, Object>) map.get("data");
//                map.put("email", socialPlatform+map.get("id"));
                map.put("name", map.get("full_name"));

            }
            UserAuthDto dto = toUser(map, socialPlatform);
            LOGGER.info("Login SSO result  {}", dto.toString());
            String email = dto.getEmail();
            if(StringUtils.isEmpty(email)){
                SocialProfileDto socialProfileDto = toSocialUser(dto, map.get("name").toString());
                try {
                    socialProfileDto = socialProfileService.create(socialProfileDto);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dto.setUserId(socialProfileDto.getUserId());
            }else {
                User userByEmail = userService.getUserByEmail(email);
                if (userByEmail != null) {
                    dto.setUserId(userByEmail.getUserId());
                    SocialProfileDto socialProfileDto = toSocialUser(dto, map.get("name").toString());
                    socialProfileDto.setUserId(userByEmail.getUserId());
                    try {
                        socialProfileService.update(socialProfileDto);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    userService.update(dto);
                } else {
                    dto.setUserId(socialProfileService.saveSocial(dto, Roles.USER));
                }
            }
            return dto;
        };


        // This Method is used Registration Musician, Author and Ambassador
        Function<String, PrincipalExtractor> createSocialExtractorForRegistration = (socialPlatform) -> (map) -> {

            String social = socialPlatform;
            String socialPlatformArr[] = social.split("-");

            if(social.equals("instagram")) {
                map = (Map<String, Object>) map.get("data");
                map.put("name", map.get("full_name"));
            }
            UserAuthDto dto = toUser(map, socialPlatformArr[1]);

            Roles role;

            switch (socialPlatformArr[0]) {
                case "musician":
                    role = Roles.MUSICIAN;
                    break;
                case "ambassador":
                    role = Roles.EVENT_AMBASSADOR;
                    break;
                case "author":
                    role = Roles.AUTHOR;
                    break;
                default:
                    role = Roles.USER;
                    break;
            }

            LOGGER.debug("Login SSO result  {}", dto.toString());
            String email = dto.getEmail();

            if(StringUtils.isEmpty(email)){
                SocialProfileDto socialProfileDto = toSocialUser(dto, map.get("name").toString());
                try {
                    socialProfileDto = socialProfileService.create(socialProfileDto);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dto.setUserId(socialProfileDto.getUserId());
            }else {
                User userByEmail = userService.getUserByEmail(email);
                if (userByEmail != null) {
                    dto.setUserId(userByEmail.getUserId());
                    SocialProfileDto socialProfileDto = toSocialUser(dto, map.get("name").toString());
                    socialProfileDto.setUserId(userByEmail.getUserId());
                    try {
                        socialProfileService.update(socialProfileDto);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    userService.update(dto, role);
                } else {
                    dto.setUserId(socialProfileService.saveSocial(dto, role));
                }
            }

            return dto;
        };

        filters.add(ssoFacebookFilter("/login/facebook", createSocialExtractor.apply("facebook")));
        filters.add(ssoFacebookFilter("/register/facebook", createSocialExtractor.apply("facebook")));

        filters.add(ssoFacebookFilter("/register/musician/facebook", createSocialExtractorForRegistration.apply("musician-facebook")));
        filters.add(ssoFacebookFilter("/register/ambassador/facebook", createSocialExtractorForRegistration.apply("ambassador-facebook")));
        filters.add(ssoFacebookFilter("/register/author/facebook", createSocialExtractorForRegistration.apply("author-facebook")));

        filters.add(ssoGoogleFilter("/register/musician/google", createSocialExtractorForRegistration.apply("musician-google")));
        filters.add(ssoGoogleFilter("/register/ambassador/google", createSocialExtractorForRegistration.apply("ambassador-google")));
        filters.add(ssoGoogleFilter("/register/author/google", createSocialExtractorForRegistration.apply("author-google")));

        filters.add(ssoGoogleFilter("/login/google", createSocialExtractor.apply("google")));
        filters.add(ssoGoogleFilter("/register/google", createSocialExtractor.apply("google")));
//
        filters.add(ssoInstagramFilter("/login/instagram", createSocialExtractor.apply("instagram")));
        filters.add(ssoInstagramFilter("/register/instagram", createSocialExtractor.apply("instagram")));

        filter.setFilters(filters);

        return filter;
    }

    @Autowired
    private AuthenticationManager authenticationManager;

    private Filter ssoFacebookFilter(String processingUrl, PrincipalExtractor principalExtractor) {
        OAuth2ClientAuthenticationProcessingFilter facebookFilter = new OAuth2ClientAuthenticationProcessingFilter(
                processingUrl);

        facebookFilter.setAuthenticationSuccessHandler(authenticationSuccessHandlerAndRegistrationFilter);
        facebookFilter.setAuthenticationManager(authenticationManager);
        OAuth2RestTemplate facebookTemplate = new OAuth2RestTemplate(facebook(), oauth2ClientContext);
        facebookFilter.setRestTemplate(facebookTemplate);

        UserInfoTokenServices tokenServices = new UserInfoTokenServices(facebookResource().getUserInfoUri(),
                facebook().getClientId());
        tokenServices.setRestTemplate(facebookTemplate);
        tokenServices.setPrincipalExtractor(principalExtractor);
        facebookFilter.setTokenServices(tokenServices);
        return facebookFilter;
    }

    private Filter ssoGoogleFilter(String processingUrl, PrincipalExtractor principalExtractor) {
        OAuth2ClientAuthenticationProcessingFilter googleFilter = new OAuth2ClientAuthenticationProcessingFilter(
                processingUrl);
        LOGGER.debug("processingUrl :{} ", processingUrl);

        googleFilter.setAuthenticationSuccessHandler(authenticationSuccessHandlerAndRegistrationFilter);

        OAuth2RestTemplate googleTemplate = new OAuth2RestTemplate(google(), oauth2ClientContext);
        googleFilter.setRestTemplate(googleTemplate);

        UserInfoTokenServices tokenServices = new UserInfoTokenServices(googleResource().getUserInfoUri(),
                google().getClientId());
        tokenServices.setRestTemplate(googleTemplate);
        tokenServices.setPrincipalExtractor(principalExtractor);
        googleFilter.setTokenServices(tokenServices);

        return googleFilter;
    }

    private Filter ssoInstagramFilter(String processingUrl, PrincipalExtractor principalExtractor) {
        OAuth2ClientAuthenticationProcessingFilter instagramFilter = new OAuth2ClientAuthenticationProcessingFilter(
                processingUrl);
        LOGGER.info("processingUrl :{} ", processingUrl);

        instagramFilter.setAuthenticationSuccessHandler(authenticationSuccessHandlerAndRegistrationFilter);

        OAuth2RestTemplate instagramTemplate = new OAuth2RestTemplate(instagram(), oauth2ClientContext);
        instagramFilter.setRestTemplate(instagramTemplate);

        UserInfoTokenServices tokenServices = new UserInfoTokenServices(instagramResource().getUserInfoUri(),
                instagram().getClientId());
        tokenServices.setRestTemplate(instagramTemplate);
        tokenServices.setPrincipalExtractor(principalExtractor);
        instagramFilter.setTokenServices(tokenServices);

        return instagramFilter;
    }

    @Bean
    @ConfigurationProperties("facebook.client")
    public AuthorizationCodeResourceDetails facebook() {
        return new AuthorizationCodeResourceDetails();
    }

    @Bean
    @ConfigurationProperties("facebook.resource")
    public ResourceServerProperties facebookResource() {
        return new ResourceServerProperties();
    }

    @Bean
    @ConfigurationProperties("google.client")
    public AuthorizationCodeResourceDetails google() {
        return new AuthorizationCodeResourceDetails();
    }

    @Bean
    @ConfigurationProperties("google.resource")
    public ResourceServerProperties googleResource() {
        return new ResourceServerProperties();
    }

    @Bean
    @ConfigurationProperties("instagram.client")
    public AuthorizationCodeResourceDetails instagram() {
        return new AuthorizationCodeResourceDetails();
    }

    @Bean
    @ConfigurationProperties("instagram.resource")
    public ResourceServerProperties instagramResource() {
        return new ResourceServerProperties();
    }


    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();

        authProvider.setUserDetailsService(userAuthorizationService);
        authProvider.setPasswordEncoder(passwordEncoder);

        return authProvider;
    }



    @Bean
    public AuthenticationSuccessHandlerAndRegistrationFilter authenticationSuccessHandlerAndRegistrationFilter() {
           return new AuthenticationSuccessHandlerAndRegistrationFilter(userProfileService, userPreferenceService,
                            lovstampService, userService, userSurveyService,  datingPlacesUtil, coupleProfileService, requestCache);
    }

    private UserAuthDto toUser(Map<String, Object> map, String socialPlatform) {
        String socialId = map.get("id").toString();
        Object emailO = map.get("email");
        String email = null;
        if (emailO != null) {
            email = emailO.toString();
        }

        String firstName;
        String lastName = null;

        String fullName = map.get("name").toString();
        String[] names = fullName.split(" ");
        if (names.length == 3) {
            firstName = names[0];
            lastName = names[2];
        } else if (names.length == 2) {
            firstName = names[0];
            lastName = names[1];
        } else {
            firstName = fullName;
        }

        UserAuthDto dto = userService.findOne(socialId, socialPlatform);
        if (dto == null) {
            dto = new UserAuthDto();
        }
        dto.setEmail(email);
        dto.setSocialId(socialId);
        dto.setSocialPlatform(socialPlatform);
        dto.setFirstName(firstName);
        dto.setLastName(lastName);
//        dto.setImageUrl(map.get(""));
        return dto;
    }

    @Bean
    public LogoutHandler logoutHandler() {
        return new SecurityContextLogoutHandler();
    }

}
