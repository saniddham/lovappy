package com.realroofers.lovappy.web.controller.admin.blogs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.realroofers.lovappy.service.blog.dto.PostShareStatisticsDto;
import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.exception.EntityValidationException;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import com.realroofers.lovappy.web.util.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.twitter.api.TweetData;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;
import org.thymeleaf.ITemplateEngine;

import com.realroofers.lovappy.service.blog.CategoryService;
import com.realroofers.lovappy.service.blog.PostService;
import com.realroofers.lovappy.service.blog.dto.PostDto;
import com.realroofers.lovappy.service.blog.support.PostStatus;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.validation.FormValidator;

/**
 * @author mwiyono
 */
@Controller()
@RequestMapping("/lox/blog")
public class BlogManagementController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlogManagementController.class);

    @Autowired
    protected FormValidator formValidator;

    @Autowired
    protected CategoryService categoryService;

    @Autowired
    protected PostService postService;

    @Autowired
    protected UserService userService;
    @Autowired
    protected CloudStorageService cloudStorageService;

    @Autowired
    protected CloudStorage cloudStorage;

    @Value("${lovappy.cloud.bucket.images}")
    protected String imagesBucket;

    @Autowired
    protected TwitterTemplate twitterTemplate;

    @Autowired
    private EmailTemplateService emailTemplateService;

    @Autowired
    private ITemplateEngine templateEngine;

    @Autowired
    private MailService mailService;

    //ADMIN POST
    @GetMapping("/post/{postId}")
    public ModelAndView postList(@PathVariable Integer postId){

        ModelAndView modelAndView = new ModelAndView("admin/blog/blog_details");
        modelAndView.getModelMap().addAttribute("categories",categoryService.getAllCategories());
        modelAndView.addObject("post", postService.getPost(postId));
        return modelAndView;
    }
    //ADMIN POST
    @GetMapping("/post")
    public ModelAndView postList(ModelAndView model,
                                 @RequestParam("pageSize") Optional<Integer> pageSize,
                                 @RequestParam("page") Optional<Integer> pageNumber,
                                 @RequestParam(value = "filter", defaultValue = "") String filter,
                                 @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){

        PageRequest pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize));
        if(!StringUtils.isEmpty(filter)) {
            if(pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize), new Sort(sort, filter));
            }
        }
        model.addObject("filter", filter);
        model.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);

        Page<PostDto> posts = postService.findAll(pageable);

        model.addObject("posts", posts);

        model.getModelMap().addAttribute("post", new PostDto());
        model.getModelMap().addAttribute("categories",categoryService.getAllCategories());
        model.getModelMap().addAttribute("newPost", new PostDto());

        Pager pager = new Pager(posts.getTotalPages(), posts.getNumber(), Pager.BUTTONS_TO_SHOW);

        model.addObject("selectedPageSize", pageSize.orElse(Pager.INITIAL_PAGE_SIZE));
        model.addObject("pageSizes", Pager.PAGE_SIZES);
        model.addObject("pager", pager);
        model.setViewName("admin/blog/manage_blogs");
        return model;
    }


    @GetMapping("/post/activate/{id}")
    public ModelAndView activatePost(HttpServletRequest request,
                                     UriComponentsBuilder uriBuilder,
                                     @PathVariable("id") Integer postId) {

        boolean isApproved = postService.updateStatus(postId, PostStatus.ACTIVE);

        if(!isApproved) {
            PostDto postDto = postService.getPost(postId);
            try {
                TweetData tweetData = new TweetData(postDto.getShortContent() + " " + uriBuilder.build().toString() + "/blog/posts/" + postDto.getSlugUrl());
                twitterTemplate.timelineOperations().updateStatus(tweetData);
            } catch (Exception ex) {
                LOGGER.error(ex.getLocalizedMessage());
            }
            //send approval email
            EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.SUBMITTED_BLOG_APPROVAL, "EN");
            emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/blog/posts/" + postDto.getSlugUrl());
            emailTemplate.getAdditionalInformation().put("title", postDto.getTitle());
            new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), postDto.getAuthor().getEmail(), templateEngine,
                    mailService, emailTemplate).send();
        }
        return new ModelAndView("redirect:/lox/blog/post");
    }


    @GetMapping("/post/reject/{id}")
    public ModelAndView hide(HttpServletRequest request, @PathVariable("id") Integer postId) {

        PostDto postDto = postService.getPost(postId);
        if(postDto != null && !postDto.getState().equals(PostStatus.DENIED.name())) {
            postService.updateStatus(postId, PostStatus.DENIED);
            //send approval email
            EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.SUBMITTED_BLOG_DENIAL, "EN");
            emailTemplate.getAdditionalInformation().put("url", CommonUtils.getBaseURL(request) + "/blog/post/" + postDto.getID() + "/edit" );
            new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), postDto.getAuthor().getEmail(), templateEngine,
                    mailService, emailTemplate).send();
        }
        return new ModelAndView("redirect:/lox/blog/post");
    }


    @GetMapping("/post/deactivate/{id}")
    public ModelAndView deactivate(@PathVariable("id") Integer postId) {
        postService.updateStatus(postId, PostStatus.INACTIVE);
        return new ModelAndView("redirect:/lox/blog/post");
    }

    @GetMapping("/post/featured/{id}")
    public ModelAndView featured(@PathVariable("id") Integer postId) {
        postService.setFeatured(postId);
        return new ModelAndView("redirect:/lox/blog/post");
    }

    @GetMapping("/post/delete/{id}")
    public ModelAndView deletePost(@PathVariable("id") Integer postId) {
        postService.delete(postId);
        return new ModelAndView("redirect:/lox/blog/post");
    }

    /**
     * @param postDto
     * @return
     */
    @PostMapping("/post/{post-id}/update")
    public ModelAndView updatePostSubmit(@PathVariable("post-id") Integer postId,
                                   @Valid PostDto postDto, BindingResult err, ModelAndView model, RedirectAttributes redirectAttributes,
                                   @RequestParam(name = "file", required = false) MultipartFile uploadfile) {

//		if (err.getErrorCount()>0||err.hasErrors()) {
//			LOGGER.debug("==========ERROR============");
//			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.postDto", err);
//			redirectAttributes.addFlashAttribute("postDto", postDto);
//			return "redirect:/blog/posterror";
//
//
        User user = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
            user = (User) authentication.getDetails();
            setImageFileId(uploadfile, postDto);
            postDto.setContent(postDto.getHtmlContent());
            postService.updateByAdmin(postId, postDto);
        }

        model.setViewName("redirect:/lox/blog/post");
        return model;
    }

    @PostMapping("/post/save")
    public ModelAndView postSubmit(@Valid PostDto postDto, BindingResult err, ModelAndView model, RedirectAttributes redirectAttributes,
                             @RequestParam("file") MultipartFile file) {


        if (err.getErrorCount() > 0 || err.hasErrors()) {
            LOGGER.debug("==========ERROR============");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.postDto", err);
            redirectAttributes.addFlashAttribute("postDto", postDto);
            return new ModelAndView("redirect:/blog/posterror");

        }
        LOGGER.debug("==========Create Post============");
        User user = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
            user = (User) authentication.getDetails();
            postDto.setWrittenByAdmin(true);
            postDto.setContent(postDto.getHtmlContent());
            setImageFileId(file, postDto);

            postService.save(postDto, user.getUserId());
        }


        model.setViewName("redirect:/lox/blog/post");
        return model;
    }


    private void setImageFileId(MultipartFile file, PostDto targetPost) {
        if (file != null && file.getSize() > 0) {
            try {
                CloudStorageFileDto cloudStorageFileDto =
                        cloudStorage.uploadImageWithThumbnails(file, imagesBucket, FileExtension.jpg.toString(), "image/jpeg");
                targetPost.setImage(cloudStorageFileDto);
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
        }
    }


    //ADMIN Blog Statistics
    @GetMapping("/statistics")
    public ModelAndView getStats(ModelAndView model,
                                 @RequestParam("pageSize") Optional<Integer> pageSize,
                                 @RequestParam("page") Optional<Integer> pageNumber,
                                 @RequestParam(value = "filter", defaultValue = "") String filter,
                                 @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort){

        LOGGER.debug("==========Get Statistics============");

        PageRequest pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize));
        if(!StringUtils.isEmpty(filter)) {
            if(pageable.getSort() != null) {
                pageable.getSort().and(new Sort(sort, filter));
            } else {
                pageable = new PageRequest(Pager.evalPageNumber(pageNumber), Pager.evalPageSize(pageSize), new Sort(sort, filter));
            }
        }
        model.addObject("filter", filter);
        model.addObject("sort", sort.equals(Sort.Direction.ASC)? Sort.Direction.DESC : Sort.Direction.ASC);

        Page<PostShareStatisticsDto> statistics = postService.getPostsStatestics(pageable);
        Pager pager = new Pager(statistics.getTotalPages(), statistics.getNumber(), Pager.BUTTONS_TO_SHOW);

        model.addObject("selectedPageSize", pageSize.orElse(Pager.INITIAL_PAGE_SIZE));
        model.addObject("pageSizes", Pager.PAGE_SIZES);
        model.addObject("pager", pager);
        model.getModelMap().addAttribute("statistics", statistics);
        model.setViewName("admin/blog/statistics");
        return model;
    }
    @RequestMapping(value = "/picture/upload", method = RequestMethod.POST)
    public ResponseEntity uploadCoverImage(@RequestParam("image_file") MultipartFile picture) {

        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (picture.isEmpty())
            errorMessages.add(new ErrorMessage("cover", "Add a location picture."));

        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        try {
            CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadFile(picture, imagesBucket, FileExtension.png.toString(), "image/png");
            CloudStorageFileDto added = cloudStorageService.add(cloudStorageFileDto);

            return ResponseEntity.ok(added);

        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}