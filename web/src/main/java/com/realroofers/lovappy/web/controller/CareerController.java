package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.career.CareerUserService;
import com.realroofers.lovappy.service.career.VacancyService;
import com.realroofers.lovappy.service.career.dto.CareerUserDto;
import com.realroofers.lovappy.service.career.dto.VacancyDto;
import com.realroofers.lovappy.service.core.VacancyStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.ws.rs.QueryParam;

import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * Created by daoud on 8/22/2018.
 */
@Controller
@RequestMapping("/careers")
public class CareerController {

    private static Logger LOG = LoggerFactory.getLogger(CareerController.class);

    @Autowired
    private VacancyService vacancyService;
    @Autowired
    private CareerUserService careerUserService;

    @GetMapping("/affiliate")
    public String affiliate(Model model) {
        return "career/affiliate";
    }

    @GetMapping
    public ModelAndView career() {
        LOG.info("Getting all the open vacancies");
        Page<VacancyDto> vacancies = vacancyService.findByStatus(VacancyStatus.OPEN.toString(), new PageRequest(0, 100));
        ModelAndView model = new ModelAndView("career/careers");
        model.addObject("vacancies", vacancies);
        return model;
    }

    @GetMapping("/details/{id}")
    public ModelAndView getVacancyDetails(@PathVariable long id) {
        LOG.info("Getting vacancy additional information");
        VacancyDto vacancy = vacancyService.findById(id);

        ModelAndView model = new ModelAndView("career/career-detail");
        model.addObject("vacancy", vacancy);
        return model;
    }

    @GetMapping("/apply")
    public ModelAndView getDetails(@QueryParam("vacancyId") Long vacancyId) {
        LOG.info("Getting vacancy additional information");
        ModelAndView model = new ModelAndView("career/career-apply");
        model.addObject("user", new CareerUserDto());
        model.addObject("vacancyId", vacancyId);
        return model;
    }

    @PostMapping("/apply")
    public RedirectView applyVacancy(@ModelAttribute CareerUserDto user, @ModelAttribute("vacancyId") Long vacancyId) {
        LOG.info("Posting the user information ");
        careerUserService.addUser(user, vacancyId);

        return new RedirectView("/lovappy-web/careers");
    }
}
