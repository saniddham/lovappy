package com.realroofers.lovappy.web.rest.v1;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.exception.UnauthorizedUserException;
import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampDto;
import com.realroofers.lovappy.service.radio.RadioService;
import com.realroofers.lovappy.service.radio.dto.RadioDto;
import com.realroofers.lovappy.service.radio.dto.RadioFilterDto;
import com.realroofers.lovappy.service.system.LanguageService;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.user.UserPreferenceService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.dto.UserPreferenceDto;
import com.realroofers.lovappy.service.user.dto.WithinRadiusDto;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.PrefHeight;
import com.realroofers.lovappy.service.user.support.Size;
import com.realroofers.lovappy.service.user.support.ZodiacSigns;
import com.realroofers.lovappy.web.rest.dto.RadioResponse;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import org.jsondoc.core.annotation.ApiBodyObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/**
 * @author Eias Altawil
 */

@RestController
@RequestMapping({"/api/v1", "/api/v2"})
public class RestLovappyRadioController {

    private final CloudStorage cloudStorage;
    private final LovstampService lovstampService;
    private final RadioService radioService;
    private final LanguageService languageService;
    private final UserPreferenceService userPreferenceService;
    @Value("${lovappy.cloud.bucket.lovdrop}")
    private String lovdropsBucket;

    private static Logger LOG = LoggerFactory.getLogger(RestLovappyRadioController.class);

    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 4;

    @Autowired
    public RestLovappyRadioController(RadioService radioService, CloudStorage cloudStorage, LovstampService lovstampService, LanguageService languageService, UserPreferenceService userPreferenceService) {
        this.cloudStorage = cloudStorage;
        this.lovstampService = lovstampService;
        this.radioService = radioService;
        this.languageService = languageService;
        this.userPreferenceService = userPreferenceService;
    }

    @RequestMapping(value = "/lovappy_radio/upload", method = RequestMethod.POST)
    public ResponseEntity<Void> uploadRecord(@RequestParam("data") MultipartFile uploadfile,
                                             @RequestParam("seconds") Integer seconds,
                                             @RequestParam("languageID") Integer languageID) {
        try {
            CloudStorageFileDto cloudStorageFileDto =
                    cloudStorage.uploadFile(uploadfile, lovdropsBucket, FileExtension.mp3.toString(), "audio/mp3");

            if (cloudStorageFileDto != null) {
                LovstampDto lovstamp = new LovstampDto();
                lovstamp.setUser(new UserDto(UserUtil.INSTANCE.getCurrentUserId()));
                lovstamp.setAudioFileCloud(cloudStorageFileDto);
                lovstamp.setRecordSeconds(seconds);
                lovstamp.setLanguage(new LanguageDto(languageID));

                if (lovstampService.create(lovstamp, false) != null)
                    return ResponseEntity.ok().build();
            }
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @RequestMapping(value = "/lovappy_radio/upload", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateRecord(@RequestParam("data") MultipartFile uploadfile,
                                             @RequestParam("seconds") Integer seconds,
                                             @RequestParam("languageID") Integer languageID) {
        try {
            CloudStorageFileDto cloudStorageFileDto =
                    cloudStorage.uploadFile(uploadfile, lovdropsBucket, FileExtension.mp3.toString(), "audio/mp3");

            if (cloudStorageFileDto != null) {
                LovstampDto lovstamp = new LovstampDto();
                lovstamp.setUser(new UserDto(UserUtil.INSTANCE.getCurrentUserId()));
                lovstamp.setAudioFileCloud(cloudStorageFileDto);
                lovstamp.setRecordSeconds(seconds);
                lovstamp.setLanguage(new LanguageDto(languageID));

                if (lovstampService.update(lovstamp) != null)
                    return ResponseEntity.ok().build();
            }
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @RequestMapping(value = "/radio/upload", method = RequestMethod.POST)
    public ResponseEntity<Void> uploadRecord(@RequestParam("data") MultipartFile uploadfile,
                                             @RequestParam("seconds") Integer seconds,
                                             @RequestParam("language") String languagecode) {
        try {
            CloudStorageFileDto cloudStorageFileDto =
                    cloudStorage.uploadFile(uploadfile, lovdropsBucket, FileExtension.mp3.toString(), "audio/mp3");

            if (cloudStorageFileDto != null) {
                LovstampDto lovstamp = new LovstampDto();
                lovstamp.setUser(new UserDto(UserUtil.INSTANCE.getCurrentUserId()));
                lovstamp.setAudioFileCloud(cloudStorageFileDto);
                lovstamp.setRecordSeconds(seconds);
                lovstamp.setLanguage(languageService.getLanguageByCode(languagecode));


                if (lovstampService.create(lovstamp, false) != null)
                    return ResponseEntity.ok().build();
            }
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @RequestMapping(value = "/radio", method = RequestMethod.GET)
    public ResponseEntity<RadioResponse> radioPage(
            HttpServletRequest request,
            @RequestParam(value = "limit", required = false) Optional<Integer> pageSize,
            @RequestParam(value = "page", required = false) Optional<Integer> pageNumber,
            @RequestParam(value = "use_filter", required = false, defaultValue = "false") Boolean useFilter,
            @RequestParam(value = "userId", required = false) Integer userID,
            @RequestParam(value = "ageFrom", required = false) Integer ageFrom,
            @RequestParam(value = "ageTo", required = false) Integer ageTo,
            @RequestParam(value = "gender", required = false) Gender gender,
            @RequestParam(value = "prefHeight", required = false) PrefHeight height,
            @RequestParam(value = "languages", required = false) String languages,
            @RequestParam(value = "penisSizeMatter", required = false) Boolean penisSizeMatter,
            @RequestParam(value = "breastsSize", required = false) Size breastsSize,
            @RequestParam(value = "buttsSize", required = false) Size buttsSize,
            @RequestParam(value = "personalities", required = false) HashMap<String, String> personalities,
            @RequestParam(value = "latitude", required = false) Double latitude,
            @RequestParam(value = "longitude", required = false) Double longitude,
            @RequestParam(value = "radius", required = false) Integer radius,
            @RequestParam(value = "lifestyles", required = false) HashMap<String, String> lifestyles,
            @RequestParam(value = "statuses", required = false) HashMap<String, String> statuses,
            @RequestParam(value = "zodiacsigns", required = false) ZodiacSigns[] zodiacSigns) {

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (pageNumber.orElse(0) < 1) ? INITIAL_PAGE : pageNumber.get() - 1;

        Integer currentUserId = UserUtil.INSTANCE.getCurrentUserId();

        Page<RadioDto> radioList;

        RadioResponse radioResponse = new RadioResponse();
        if (currentUserId == null&&!useFilter) {

            if (Gender.MALE.equals(gender) || Gender.FEMALE.equals(gender)) {
                radioList = radioService.getRadioItemsByGender(gender, evalPage + 1, evalPageSize);
            } else {
                radioList = radioService.getAllRadioItems(evalPage + 1, evalPageSize);
            }
            radioResponse.setRadioPage(radioList);
            radioResponse.setMessage("");
            radioResponse.setEmptyResult(false);
            radioResponse.setShowMessage(false);
            request.getSession().setAttribute("filter", gender);

        } else  {
            RadioFilterDto filter = new RadioFilterDto(userID, ageFrom, ageTo, gender, height, languages,
                    penisSizeMatter, breastsSize, buttsSize, personalities, lifestyles, statuses,
                    new WithinRadiusDto(latitude, longitude, radius, null), zodiacSigns);
            radioList = radioService.getFilteredRadioItems(currentUserId, useFilter, filter, evalPage + 1, evalPageSize);

            if (useFilter && filter.getUserId() != null && radioList.getContent().size() > 0) {
                radioResponse.setMessage("");
                radioResponse.setEmptyResult(false);
                radioResponse.setShowMessage(false);
            } else if (evalPage < 1 && radioList.getContent().size() <= 6) {
                UserPreferenceDto userPreferenceDto = userPreferenceService.findOne(UserUtil.INSTANCE.getCurrentUserId());
                if (Gender.MALE.equals(userPreferenceDto.getGender()) || Gender.FEMALE.equals(userPreferenceDto.getGender())) {
                    radioList = radioService.getRadioItemsByGender(userPreferenceDto.getGender(), evalPage + 1, evalPageSize);
                } else {
                    radioList = radioService.getAllRadioItems(evalPage + 1, evalPageSize);
                }
                radioResponse.setMessage("");
                radioResponse.setEmptyResult(useFilter);
                radioResponse.setShowMessage(useFilter);
            } else {
                radioResponse.setMessage("");
                radioResponse.setEmptyResult(false);
                radioResponse.setShowMessage(false);
            }
            radioResponse.setRadioPage(radioList);
            request.getSession().setAttribute("filter", filter);

        }
        request.getSession().setAttribute("use_filter", useFilter);
        request.getSession().setAttribute("page", evalPage);
        request.getSession().setAttribute("limit", evalPageSize);
        List<Integer> userIds;
        if(evalPage > 0) {
            if (request.getSession().getAttribute("userIds")!= null) {
                userIds = (List<Integer>) request.getSession().getAttribute("userIds");
            }else {
                userIds = new ArrayList<>();
            }
        } else {
            userIds = new ArrayList<>();
        }

        for (RadioDto radioDto:radioList) {
            userIds.add(radioDto.getUserId());
        }
        request.getSession().setAttribute("userIds", userIds);
        return ResponseEntity.ok(radioResponse);
    }


    @RequestMapping(value = "/radio", method = RequestMethod.POST)
    public ResponseEntity<RadioResponse> radioPage(
            @RequestParam(value = "limit", required = false) Optional<Integer> pageSize,
            @RequestParam(value = "page", required = false) Optional<Integer> pageNumber,
            @RequestParam(value = "use_filter", required = false, defaultValue = "false") Boolean useFilter,
            @ApiBodyObject @RequestBody(required = false) RadioFilterDto radioFilterDto
    ) {
        LOG.info("radio request " + radioFilterDto.toString());
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (pageNumber.orElse(0) < 1) ? INITIAL_PAGE : pageNumber.get() - 1;

        Integer currentUserId = UserUtil.INSTANCE.getCurrentUserId();

        Page<RadioDto> radioList;

        RadioResponse radioResponse = new RadioResponse();
        if (currentUserId == null&&!useFilter) {

            if (Gender.MALE.equals(radioFilterDto.getGender()) || Gender.FEMALE.equals(radioFilterDto.getGender())) {
                radioList = radioService.getRadioItemsByGender(radioFilterDto.getGender(), evalPage + 1, evalPageSize);
            } else {
                radioList = radioService.getAllRadioItems(evalPage + 1, evalPageSize);
            }
            radioResponse.setRadioPage(radioList);
            radioResponse.setMessage("");
            radioResponse.setEmptyResult(false);
            radioResponse.setShowMessage(false);
        } else {

            radioList = radioService.getFilteredRadioItems(currentUserId, useFilter, radioFilterDto, evalPage + 1, evalPageSize);
            if (useFilter && radioFilterDto.getUserId() != null && radioList.getContent().size() > 0) {
                radioResponse.setMessage("");
                radioResponse.setEmptyResult(false);
                radioResponse.setShowMessage(false);
            } else if (evalPage < 1 && radioList.getContent().size() == 0) {
                UserPreferenceDto userPreferenceDto = userPreferenceService.findOne(UserUtil.INSTANCE.getCurrentUserId());
                if (Gender.MALE.equals(userPreferenceDto.getGender()) || Gender.FEMALE.equals(userPreferenceDto.getGender())) {
                    radioList = radioService.getRadioItemsByGender(userPreferenceDto.getGender(), evalPage + 1, evalPageSize);
                } else {
                    radioList = radioService.getAllRadioItems(evalPage + 1, evalPageSize);
                }
                radioResponse.setMessage("");
                radioResponse.setEmptyResult(useFilter);
                radioResponse.setShowMessage(useFilter);
            } else {
                radioResponse.setMessage("");
                radioResponse.setEmptyResult(false);
                radioResponse.setShowMessage(false);
            }
            radioResponse.setRadioPage(radioList);


        }

        return ResponseEntity.ok(radioResponse);
    }


}
