package com.realroofers.lovappy.web.controller.admin;

import com.fasterxml.jackson.core.type.TypeReference;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.dto.*;
import com.realroofers.lovappy.service.cms.model.ImageWidget;
import com.realroofers.lovappy.service.cms.model.Page;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import com.realroofers.lovappy.service.core.VideoProvider;
import com.realroofers.lovappy.service.system.LanguageService;
import com.realroofers.lovappy.service.util.CommonUtils;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by Daoud Shaheen on 6/7/2017.
 */
@Controller
@RequestMapping("/lox/pages")
public class PageController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PageController.class);

    private CMSService cmsService;
    private final CloudStorage cloudStorage;
    private final LanguageService languageService;

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 20;
    private static final int[] PAGE_SIZES = {5, 10, 20};

    public PageController(CMSService cmsService, CloudStorage cloudStorage, LanguageService languageService) {
        this.cmsService = cmsService;
        this.cloudStorage = cloudStorage;
        this.languageService = languageService;
    }

    @PostMapping(value = {"/{page-tag}/images/{image-id}/upload"})
    public ModelAndView uploadExistingImage(@RequestParam("file") MultipartFile uploadfile,
                                            @PathVariable("page-tag") String pageTag,
                                            @PathVariable("image-id") Integer imageId) {
        try {
            CloudStorageFileDto cloudStorageFileDto =
                    cloudStorage.uploadImageWithThumbnails(uploadfile, imagesBucket, FileExtension.jpg.toString(), "image/jpeg");
            ImageWidgetDTO imageWidgetDTO = new ImageWidgetDTO(imageId, cloudStorageFileDto.getName(), cloudStorageFileDto.getUrl(), null);

            cmsService.updatePageImage(pageTag, imageId, imageWidgetDTO);
            return new ModelAndView("redirect:/lox/pages/" + pageTag);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

        throw new RuntimeException("Upload failed");

    }

    @PostMapping(value = {"/{page-tag}/images/types/{image-type}/upload"})
    public ModelAndView uploadImageByType(@RequestParam("file") MultipartFile uploadfile,
                                          @PathVariable("page-tag") String pageTag,
                                          @PathVariable("image-type") String type) {
        try {
            CloudStorageFileDto cloudStorageFileDto =
                    cloudStorage.uploadImageWithThumbnails(uploadfile, imagesBucket, FileExtension.jpg.toString(), "image/jpeg");
            ImageWidget imageWidget = new ImageWidget(cloudStorageFileDto.getName(), cloudStorageFileDto.getUrl(), type);
            cmsService.savePageImage(pageTag, imageWidget);
            return new ModelAndView("redirect:/lox/pages/" + pageTag);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

        throw new RuntimeException("Upload failed");
    }

    @GetMapping("/{page-tag}")
    public ModelAndView getPage(@PathVariable("page-tag") String pageTag, ModelAndView model,
                                @RequestParam(value = "language", defaultValue = "EN") String language) {

        model.addObject("languages", languageService.getAllLanguages(true));

        model.addObject("language", language);

        PageDTO page = cmsService.findPageByTagWithImagesAndTexts(pageTag, language);

        model.addObject("page", page);
        model.addObject("pageName", page.getName());
        model.addObject("pageTag", page.getTag());
        model.addObject("pageMeta", page.getPageMetaList());
        model.addObject("backgroundImages", page.getBackgroundImages());
        model.addObject("banners", page.getBannersImages());
        model.addObject("texts", page.getTexts());
        model.addObject("videos", page.getVideos());
        model.addObject("video", new VideoWidgetDTO());
        model.addObject("text", new TextWidgetDTO());
        model.addObject("meta", new PageMetaDTO());

        model.setViewName("admin/cms/content_managment");
        return model;
    }

    @GetMapping("/page/manager")
    public ModelAndView getPageMagement(@RequestParam("pageSize") Optional<Integer> pageSize,
                                        @RequestParam("page") Optional<Integer> page,
                                        @RequestParam(value = "searchText", defaultValue = "", required = false) String searchText) {

        ModelAndView modelAndView = new ModelAndView("admin/cms/page-management");

        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        PageRequest pageable = new PageRequest(evalPage, evalPageSize);

        modelAndView.addObject("searchText", searchText);
        org.springframework.data.domain.Page<com.realroofers.lovappy.service.cms.model.Page> pages = cmsService.getAllPagesByName(searchText, pageable);
        Pager pager = new Pager(pages.getTotalPages(), pages.getNumber(), BUTTONS_TO_SHOW);
        modelAndView.addObject("pages", pages);

        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pageSizes", PAGE_SIZES);
        modelAndView.addObject("pager", pager);
        return modelAndView;
    }

    @GetMapping("/page/manager/updatePageStatus{id}")
    public ModelAndView updatePageStatus(@PathVariable("id") Integer id) {

        Page page =cmsService.updatePageStatus(id);

        return new ModelAndView("redirect:/lox/pages/" + page.getTag());
    }

    @GetMapping("/errors")
    public ModelAndView getErrorPage(ModelAndView model,
                                     @RequestParam(value = "language", defaultValue = "EN") String language) {
        PageDTO page = cmsService.findPageByTagWithImagesAndTexts("errors", language);
        model.addObject("pageName", page.getName());
        model.addObject("pageTag", page.getTag());

        model.addObject("texts", page.getTexts());
        model.addObject("text", new TextWidgetDTO());
        model.addObject("videos", page.getVideos());
        model.addObject("video", new VideoWidgetDTO());
        model.setViewName("admin/cms/errors");
        return model;
    }

    @GetMapping("/{page-tag}/terms")
    public ModelAndView getTermPage(@PathVariable("page-tag") String pageTag, ModelAndView model,
                                    @RequestParam(value = "language", defaultValue = "EN") String language) {
        PageDTO page = cmsService.findPageByTagWithImagesAndTexts(pageTag, language);
        model.addObject("pageName", page.getName());
        model.addObject("pageTag", page.getTag());
        model.addObject("terms", page.getTexts());
        model.addObject("text", new TextWidgetDTO());
        model.addObject("videos", page.getVideos());
        model.addObject("video", new VideoWidgetDTO());
        model.setViewName("admin/cms/terms");
        return model;
    }


    @PostMapping(value = {"/{page-tag}/meta/{meta-id}"})
    public ModelAndView updateText(
            @PathVariable("page-tag") String pageTag,
            @PathVariable("meta-id") Integer metaId,
            @RequestParam(value = "language", defaultValue = "EN") String language,
            @ModelAttribute(value = "meta") PageMetaDTO content) {

        LOGGER.info("PageMetaData {}", content);
        cmsService.updateMetaTags(content, metaId);
        return new ModelAndView("redirect:/lox/pages/" + pageTag+"?language=" + language);
    }

    @PostMapping(value = {"/{page-tag}/terms/save"})
    public ModelAndView updateText(
            @PathVariable("page-tag") String pageTag,
            @RequestParam(value = "language", defaultValue = "EN") String language,
            @ModelAttribute(value = "text") TextWidgetDTO content) {

        LOGGER.info("textDTO {}", content);
        content.setLanguage(language);
        cmsService.savePageText(pageTag, content);
        return new ModelAndView("redirect:/lox/pages/" + pageTag + "/terms?language=" + language);
    }


    @PostMapping(value = {"/{page-tag}/texts/{text-id}"})
    public ModelAndView updateText(
            @PathVariable("page-tag") String pageTag,
            @PathVariable("text-id") Integer textId,
            @RequestParam(value = "language", defaultValue = "EN") String language,
            @ModelAttribute(value = "text") TextWidgetDTO content) {

        LOGGER.info("textDTO {}", content);
        cmsService.updateTextByIdAndLanguage(textId, language, content.getContent());
        return new ModelAndView("redirect:/lox/pages/" + pageTag+"?language=" + language);
    }

    @PostMapping(value = {"/{page-tag}/videos/{video-id}"})
    public ModelAndView updateVideo(
            @PathVariable("page-tag") String pageTag,
            @PathVariable("video-id") Integer videoId,
            @ModelAttribute(value = "video") VideoWidgetDTO videoWidgetDTO) {

        LOGGER.info("video {}", videoWidgetDTO);
        if (videoWidgetDTO != null && videoWidgetDTO.getUrl().contains("youtube")) {
            String youtubeId = CommonUtils.findYoutubeVideoId(videoWidgetDTO.getUrl());
            videoWidgetDTO.setUrl("https://www.youtube.com/embed/" + youtubeId);
            videoWidgetDTO.setProvider(VideoProvider.YOUTUBE);
            videoWidgetDTO.setProviderId(youtubeId);
        } else {
            videoWidgetDTO.setProvider(VideoProvider.OTHER);
            videoWidgetDTO.setProviderId("");
        }
        cmsService.updatePageVideo(pageTag, videoId, videoWidgetDTO);
        return new ModelAndView("redirect:/lox/pages/" + pageTag);
    }

    @PostMapping(value = {"/plans-pricing/rules"})
    public ModelAndView updateRules(
            @RequestParam(value = "language", defaultValue = "EN") String language,
            @RequestParam(value = "index", defaultValue = "") Integer ruleIndex,
            @ModelAttribute(value = "rule") RulePricePlanDTO updateRule) {

        LOGGER.info("RulePricePlanDTO {}", updateRule);
        List<RulePricePlanDTO> rules = (List<RulePricePlanDTO>) CommonUtils.fromJSONArrayString(cmsService.getTextByTagAndLanguage(PageConstants.PLAN_PRICE_RULES, "EN"), new TypeReference<List<RulePricePlanDTO>>() {
        });
        Collections.sort(rules);

        if (ruleIndex != null) {
            updateRule.setIndex(ruleIndex);
            for (RulePricePlanDTO rule : rules) {
                if (rule.getIndex() == updateRule.getIndex()) {
                    if (!StringUtils.isEmpty(updateRule.getLite()))
                        rule.setLite(updateRule.getLite());
                    if (!StringUtils.isEmpty(updateRule.getPrime()))
                        rule.setPrime(updateRule.getPrime());
                    if (!StringUtils.isEmpty(updateRule.getName()))
                        rule.setName(updateRule.getName());
                }
            }
        } else {
            updateRule.setIndex(rules.get(rules.size() - 1).getIndex() + 1);
            rules.add(updateRule);
        }

        cmsService.updateTextByTagAndLanguage(PageConstants.PLAN_PRICE_RULES, language, CommonUtils.convertObjectToJsonString(rules));
        return new ModelAndView("redirect:/lox/pages/plans-pricing");
    }


}
