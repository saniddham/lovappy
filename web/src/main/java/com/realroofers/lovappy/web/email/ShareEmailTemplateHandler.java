package com.realroofers.lovappy.web.email;

import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import org.springframework.util.StringUtils;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Created by Daoud Shaheen on 9/10/2017.
 */
public class ShareEmailTemplateHandler extends AbstractEmailTemplateHandler {


    public ShareEmailTemplateHandler(String baseUrl, String email, MailService mailService, ITemplateEngine templateEngine, EmailTemplateDto emailTemplateDto) {
        super(baseUrl, email, mailService, templateEngine, emailTemplateDto);
    }

    @Override
    public void send() {
        if (!emailTemplateDto.isEnabled())
            return;

        try {
            Context context = new Context();

            context.setVariable("recipientName", emailTemplateDto.getAdditionalInformation().get("recipientName"));
            context.setVariable("name", emailTemplateDto.getAdditionalInformation().get("name"));
            context.setVariable("recipientEmail", emailTemplateDto.getAdditionalInformation().get("recipientEmail"));
            context.setVariable("sharedBy", emailTemplateDto.getAdditionalInformation().get("sharedBy"));
            context.setVariable("shareLink", emailTemplateDto.getAdditionalInformation().get("shareLink"));

            context.setVariable("baseUrl", baseUrl);
            context.setVariable("body", emailTemplateDto.getBody());
            context.setVariable("emailBG", emailTemplateDto.getImageUrl());
            context.setVariable("buttonText", emailTemplateDto.getAdditionalInformation().get("buttonText") == null ? "Click Here" : emailTemplateDto.getAdditionalInformation().get("buttonText"));
            String content = templateEngine.process(emailTemplateDto.getTemplateUrl(), context);
            mailService.sendMail(email, content, emailTemplateDto.getSubject(), emailTemplateDto.getFromEmail(), emailTemplateDto.getFromName());
        } catch (Exception ex) {
            LOGGER.error("Failed to send email: {}", ex);
        }
    }

}
