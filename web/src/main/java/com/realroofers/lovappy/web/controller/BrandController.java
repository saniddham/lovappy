package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.product.BrandService;
import com.realroofers.lovappy.service.product.ProductTypeService;
import com.realroofers.lovappy.service.product.dto.BrandDto;
import com.realroofers.lovappy.service.product.dto.ProductTypeDto;
import com.realroofers.lovappy.service.user.CountryService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.FormValidator;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.service.validation.ResponseStatus;
import com.realroofers.lovappy.service.validator.UserRegister;
import com.realroofers.lovappy.service.vendor.VendorService;
import com.realroofers.lovappy.service.vendor.dto.BrandRegistrationDto;
import com.realroofers.lovappy.service.vendor.dto.VendorDto;
import com.realroofers.lovappy.service.vendor.model.VendorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manoj on 03/02/2018.
 */
@Controller
@RequestMapping("/brand")
public class BrandController {

    private final CountryService countryService;
    private final VendorService vendorService;
    private final BrandService brandService;
    private final ProductTypeService productTypeService;
    private final FormValidator formValidator;

    @Autowired
    public BrandController(CountryService countryService, VendorService vendorService,
                           BrandService brandService, ProductTypeService productTypeService, FormValidator formValidator) {
        this.countryService = countryService;
        this.vendorService = vendorService;
        this.brandService = brandService;
        this.productTypeService = productTypeService;
        this.formValidator = formValidator;
    }

    @GetMapping("/register")
    public String registrationBrandPage(Model model) {

        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        model.addAttribute("userId", userId);
        model.addAttribute("countries", countryService.findAllActive());

        VendorDto user = vendorService.findByUser(userId);
        List<BrandDto> brandList;

        if(user.getVendorType().equals(VendorType.BRAND)){
            brandList = new ArrayList<>();
            BrandDto brandDto = brandService.findByName(user.getCompanyName());
            if (brandDto!=null) {
                brandList.add(brandDto);
            }else{
                BrandDto branddto=new BrandDto();
                branddto.setBrandName(user.getCompanyName());
                branddto.setActive(true);
                branddto.setCreatedBy(user.getUser());
                try {
                    BrandDto brandDto1 = brandService.create(branddto);
                    brandList.add(brandDto1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }else {
            brandList = brandService.findAllActive();
        }

        model.addAttribute("info", new BrandRegistrationDto());
        model.addAttribute("brand", new BrandDto());
        model.addAttribute("brandList", brandList);
        model.addAttribute("productType", new ProductTypeDto());
        model.addAttribute("productTypeList", productTypeService.findAllActive());
        model.addAttribute("vendorType", user.getVendorType().toString());

        return "vendor/registration/brand";
    }

    @PostMapping("/register")
    public ResponseEntity registration(@ModelAttribute @Valid BrandRegistrationDto brandRegistrationDto) {
        JsonResponse res = new JsonResponse();
        try {
            List<ErrorMessage> errorMessages = formValidator.validate(brandRegistrationDto, new Class[]{UserRegister.class});
            if (errorMessages.size() > 0) {
                res.setErrorMessageList(errorMessages);
                res.setStatus(ResponseStatus.FAIL);
                return ResponseEntity.ok().body(res);
            }

            final Integer userId = UserUtil.INSTANCE.getCurrentUserId();
            vendorService.registerBrand(userId, brandRegistrationDto);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body(res);
        }
        return ResponseEntity.ok().body(res);

    }

    @GetMapping("/deactivate{id}")
    public String deactivate(@PathVariable("id") Integer id) throws Exception {
        BrandDto brand = brandService.findById(id);
        if (brand != null) {
            brand.setActive(false);
            brandService.update(brand);
        }

        return "redirect:/vendor/brand";
    }

    @GetMapping("/activate{id}")
    public String activate(@PathVariable("id") Integer id) throws Exception {
        BrandDto brand = brandService.findById(id);
        if (brand != null) {
            brand.setActive(true);
            brandService.update(brand);
        }

        return "redirect:/vendor/brand";
    }

}
