package com.realroofers.lovappy.web.controller;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.couples.CoupleProfileService;
import com.realroofers.lovappy.service.couples.dto.CoupleProfileRequest;
import com.realroofers.lovappy.service.couples.dto.CouplesProfileDto;
import com.realroofers.lovappy.service.exception.GenericServiceException;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.model.EmailTemplate;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.order.PriceService;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.SetNewPasswordForm;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.exception.BadRequestException;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.realroofers.lovappy.service.gallery.GalleryStorageService;
import org.thymeleaf.TemplateEngine;


import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 6/10/2017.
 */
@Controller
public class CouplesController {

	private static final String GOOGLE_KEY = "google_key";

	@Value("${google.maps.api.key}")
	private String googleKeyId;

	private static final Logger LOGGER = LoggerFactory.getLogger(CouplesController.class);
	@Autowired
	private GalleryStorageService galleryStorageService;
	@Autowired
	private CoupleProfileService coupleProfileService;
	@Autowired
	private UserService userService;
	@Autowired
	private MailService mailService;
	@Autowired
	private EmailTemplateService emailTemplateService;
	@Autowired
	private TemplateEngine templateEngine;
	@Autowired
	private PriceService priceService;
	@Autowired
	private  CloudStorage cloudStorage;
	@Value("${lovappy.cloud.bucket.images}")
	private String imagesBucket;
	@GetMapping("/happy-couples")
	public ModelAndView happyCouples(ModelAndView model) {
		model.setViewName("happy-couples");
		model.addObject("listFiles", galleryStorageService.getAllPhotosByStatus(new PageRequest(0, 20)));
		model.addObject(GOOGLE_KEY, googleKeyId);
		return model;
	}

	@GetMapping("/couples/verify/{token}")
	public ModelAndView verify(@PathVariable("token") String token, ModelAndView model) {

		String decodedToken = new String(Base64.getDecoder().decode(token));
		String parties[] = decodedToken.split("_");
		String partnerEmail = null;
		String redirectUrl = "redirect:/couples/password/rest/"+token;
		if(parties[0].equals("new")){
			partnerEmail=parties[2];
		} else {
			partnerEmail=parties[1];
			 redirectUrl = "redirect:/couples/dashboard";
		}
		if(coupleProfileService.partnerApprove(partnerEmail) == null){
			redirectUrl = "redirect:/error/404";
		}
		model.setViewName(redirectUrl);
		return model;
	}


	@GetMapping("/couples/password/rest/{token}")
	public ModelAndView restPassword(@PathVariable("token") String token, ModelAndView model) {

		model.addObject("setNewPasswordForm", new SetNewPasswordForm(token));
		model.setViewName("couples/couples_rest_password");
		return model;
	}


	@PostMapping("/couples/password/rest")
	public ModelAndView setNewPassword(HttpServletRequest request,
									   @Valid SetNewPasswordForm setNewPasswordForm, BindingResult bindingResult
			, @RequestParam(value = "lang", defaultValue = "EN") String language){
		if (bindingResult.hasErrors())
			return new ModelAndView("couples/couples_rest_password");

		UserDto user = userService.restNewPassword(setNewPasswordForm.getToken(), setNewPasswordForm.getPassword());
		if(user!=null) {
			EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.NEW_PASSWORD_CONFIRMATION, language);
			new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), user.getEmail(), templateEngine,
					mailService, emailTemplate).send();

			return new ModelAndView("redirect:/couples/dashboard");
		}
		return new ModelAndView("redirect:/error/404");
	}
	@GetMapping("/couples/dashboard")
	public String couplesDashboard(Model model) {
		CouplesProfileDto dto = coupleProfileService.getCoupleProfile(UserUtil.INSTANCE.getCurrentUserId());
		if (dto != null) {
			model.addAttribute("couplesProfile", dto);
		} else {
			model.addAttribute("couplesProfile", new CouplesProfileDto());
		}
		model.addAttribute("messagesPrice",  priceService.getByType(OrderDetailType.PRIVATE_MESSAGE).getPrice());
		return "couples/couples_dashboard";
	}

	@RequestMapping(value = "/couples/partner/email/save", method = RequestMethod.POST)
	public ModelAndView registerPartner(ModelAndView model,@RequestParam("partnerEmail") String  partnerEmail, HttpServletRequest request) {

		registerPartner(partnerEmail, request);

		model.setViewName("redirect:/couples/dashboard");

		return model;
	}

	void registerPartner(String  partnerEmail, HttpServletRequest request) {
		if(!CommonUtils.isValidEmail(partnerEmail)) {
			throw new BadRequestException("Invalid Email");
		}

		UserDto userDto = coupleProfileService.registerPartner(UserUtil.INSTANCE.getCurrentUserId(), partnerEmail);
		EmailTemplateDto  emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.COUPLE_APPROVAL, "EN");
		String verifyToken = null;
		if(userDto!=null) {
			if (userDto.getPassword() == null) {
				verifyToken = new String(Base64.getEncoder().encode((UserUtil.INSTANCE.getCurrentUserId() + "_" + userDto.getEmail()).getBytes()));
			} else {
				verifyToken = new String(Base64.getEncoder().encode(("new_" + UserUtil.INSTANCE.getCurrentUserId() + "_" + userDto.getEmail()).getBytes()));
			}
			String url = CommonUtils.getBaseURL(request) + "/couples/verify/" + verifyToken;
			if (emailTemplate != null) {
				emailTemplate.getAdditionalInformation().put("url", url);
				emailTemplate.setBody(emailTemplate.getBody().replace("{{partnerEmail}}", UserUtil.INSTANCE.getCurrentUser().getEmail()));
				new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), partnerEmail, templateEngine,
						mailService, emailTemplate).send();
			}
		}
	}

	@RequestMapping(value = "/couples/signup", method = RequestMethod.POST)
	public ResponseEntity<Void> couplesPage(@RequestParam(value = "gender", required = false) Gender gender,
											@RequestParam("coupleGender") Gender coupleGender,
											@RequestParam(value = "partnerEmail", required = false) String partnerEmail,
											@RequestParam(value = "birthDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date birthDate,
											@RequestParam("anniversaryDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date anniversaryDate,
											@RequestParam("yearsTogether") int yearsTogether, @RequestParam("coupleImage") MultipartFile coupleImage,
											HttpServletRequest request
											) {

		CoupleProfileRequest coupleProfileRequest = new CoupleProfileRequest();
		coupleProfileRequest.setGender(gender);
		coupleProfileRequest.setCoupleGender(coupleGender);
		coupleProfileRequest.setBirthDate(birthDate);
		coupleProfileRequest.setAnniversaryDate(anniversaryDate);
		coupleProfileRequest.setYearsTogether(yearsTogether);
		CouplesProfileDto couplesProfileDto = coupleProfileService.getCoupleProfile(UserUtil.INSTANCE.getCurrentUserId());
		if(couplesProfileDto == null) {
			couplesProfileDto =	coupleProfileService.signup(coupleProfileRequest, UserUtil.INSTANCE.getCurrentUserId());

		} else {
			couplesProfileDto = coupleProfileService.update(coupleProfileRequest, UserUtil.INSTANCE.getCurrentUserId());
		}


		if(!StringUtils.isEmpty(partnerEmail) && !partnerEmail.equals(couplesProfileDto.getPartnerEmail())){
			registerPartner(partnerEmail, request);
		}
		CloudStorageFileDto uploadedImage = null;

		if (coupleImage != null && !coupleImage.isEmpty()) {
			try {
				uploadedImage = cloudStorage.uploadAndPersistFile(coupleImage, imagesBucket,
						FilenameUtils.getExtension(coupleImage.getOriginalFilename()), coupleImage.getContentType());
				coupleProfileService.uploadPhoto(uploadedImage,  UserUtil.INSTANCE.getCurrentUserId());
			} catch (IOException e) {
				throw new GenericServiceException("Something went wrong");
			}
		}

		return ResponseEntity.ok().build();
	}
}
