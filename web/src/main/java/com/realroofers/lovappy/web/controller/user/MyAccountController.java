package com.realroofers.lovappy.web.controller.user;

import com.realroofers.lovappy.service.credits.CreditService;
import com.realroofers.lovappy.service.gift.GiftExchangeService;
import com.realroofers.lovappy.service.gift.dto.GiftExchangeDto;
import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.music.MusicService;
import com.realroofers.lovappy.service.music.dto.MusicExchangeDto;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.MyAccountDto;
import com.realroofers.lovappy.service.user.dto.RoleDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.vendor.VendorLocationService;
import com.realroofers.lovappy.service.vendor.VendorService;
import com.realroofers.lovappy.service.vendor.dto.VendorDto;
import com.realroofers.lovappy.service.vendor.model.VendorLocation;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.util.CommonUtils;
import com.realroofers.lovappy.web.util.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

import static com.realroofers.lovappy.web.support.Util.distinctByKey;

/**
 * Created by Eias Altawil on 5/6/17
 */

@Controller
@RequestMapping("/my_account")
public class MyAccountController {
    private static Logger LOG = LoggerFactory.getLogger(MyAccountController.class);
    private final UserService userService;
    private final UserProfileService userProfileService;
    private final LovstampService lovstampService;
    private final MailService mailService;
    private ITemplateEngine templateEngine;
    private final EmailTemplateService emailTemplateService;
    private final MusicService musicService;
    private final GiftExchangeService giftExchangeService;
    private final OrderService orderService;
    private final CreditService creditService;
    private final VendorService vendorService;
    private final VendorLocationService vendorLocationService;
    @Value("${square.applicationId}")
    private String squareAppID;
    @Autowired
    public MyAccountController(UserService userService, UserProfileService userProfileService,
                               LovstampService lovstampService, MailService mailService, List<ViewResolver> viewResolvers, EmailTemplateService emailTemplateService, MusicService musicService, GiftExchangeService giftExchangeService, OrderService orderService, CreditService creditService, VendorService vendorService, VendorLocationService vendorLocationService) {
        this.userService = userService;
        this.userProfileService = userProfileService;
        this.lovstampService = lovstampService;
        this.mailService = mailService;
        this.emailTemplateService = emailTemplateService;
        this.musicService = musicService;
        this.giftExchangeService = giftExchangeService;
        this.orderService = orderService;
        this.creditService = creditService;
        this.vendorService = vendorService;
        this.vendorLocationService = vendorLocationService;
        for (ViewResolver viewResolver : viewResolvers) {
            if (viewResolver instanceof ThymeleafViewResolver) {
                ThymeleafViewResolver thymeleafViewResolver = (ThymeleafViewResolver) viewResolver;
                this.templateEngine = thymeleafViewResolver.getTemplateEngine();
            }
        }
        if (this.templateEngine == null) {
            throw new BeanInitializationException("No view resolver of type ThymeleafViewResolver found.");
        }
    }


    @GetMapping
    public ModelAndView myAccount() {
        ModelAndView mav = new ModelAndView("user/my_account");
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        MyAccountDto myAccountDto = userService.getMyAccount(userId);
        mav.addObject("myAccount", myAccountDto);
        mav.addObject("profile", userProfileService.getProfilePicsDto(userId));
        return mav;
    }

    @GetMapping("/deactivate")
    public ModelAndView deactivate() {

        userService.activateUser(UserUtil.INSTANCE.getCurrentUserId(), false);
        return new ModelAndView("redirect:/my_account");
    }

    @GetMapping("/songs/inbox")
    public ModelAndView receivedSongs(@RequestParam(value = "limit", defaultValue = "10") Integer pageSize,
                                      @RequestParam(value = "page", defaultValue = "1") Integer page) {

    	ModelAndView modelAndView = new ModelAndView("user/my_received_songs");
    	Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        modelAndView.addObject("isInbox", true);
    	modelAndView.addObject("sentCount", musicService.countMusicSent(userId));

        modelAndView.addObject("recievedCount", musicService.countMusicReceived(userId));

        Page<MusicExchangeDto> musicExchangeDtoPage = musicService.getMusicExchangeByToUser(userId, new PageRequest(page - 1, 10, Sort.Direction.DESC, "exchangeDate"));
        modelAndView.addObject("musicExchangedPage", musicExchangeDtoPage);
        Pager pager = new Pager(musicExchangeDtoPage.getTotalPages(), musicExchangeDtoPage.getNumber(), 5);


        modelAndView.addObject("pager", pager);
    	return modelAndView;
    }

    @GetMapping("/songs/sent")
    public ModelAndView sentSongs(@RequestParam(value = "page", defaultValue = "1") Integer page) {
        ModelAndView modelAndView = new ModelAndView("user/my_sent_songs");
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        modelAndView.addObject("isInbox", false);
        modelAndView.addObject("sentCount", musicService.countMusicSent(userId));
        Page<MusicExchangeDto> musicExchangeDtoPage = musicService.getMusicExchangeByFromUser(userId, new PageRequest(page - 1, 6, Sort.Direction.DESC, "exchangeDate"));
        modelAndView.addObject("musicExchangedPage", musicExchangeDtoPage);
        modelAndView.addObject("recievedCount", musicService.countMusicReceived(userId));

        Pager pager = new Pager(musicExchangeDtoPage.getTotalPages(), musicExchangeDtoPage.getNumber(), 5);


        modelAndView.addObject("pager", pager);
        return modelAndView;
    }

    //TODO Please use pagination this critical
    @GetMapping("/gift/inbox")
    public ModelAndView giftInbox(){
    	ModelAndView mav = new ModelAndView("user/gift-inbox");
        Collection<GiftExchangeDto> allReceivedGifts = giftExchangeService.getAllReceivedGifts(UserUtil.INSTANCE.getCurrentUserId());
        mav.addObject("gifts", allReceivedGifts);
        Long totalUsers = allReceivedGifts.stream()
                .filter(distinctByKey(o -> o.getToUser().getID()))
                .count();
        mav.addObject("totalUsers", totalUsers);

        Collection<GiftExchangeDto> allSentGifts = giftExchangeService.getAllSentGifts(UserUtil.INSTANCE.getCurrentUserId());
        mav.addObject("totalSent", allSentGifts.size());
    	return mav;
    }

    @GetMapping("/gift/inbox/details{id}")
    public ModelAndView giftInboxDetails(@PathVariable("id") Integer id){
    	ModelAndView mav = new ModelAndView("user/gift-inbox-details");

        GiftExchangeDto gift = giftExchangeService.findById(id);

        if(gift==null){
            return new ModelAndView("redirect:/my_account/gift/inbox");
        }

        Integer vendorId=gift.getGift().getCreatedBy().getID();

        VendorDto vendorDto = vendorService.findByUser(vendorId);

        List<VendorLocation> locations = vendorLocationService.findLocationsByLoginUser(vendorId);

        mav.addObject("gift", gift);
        mav.addObject("product", gift.getGift());
        mav.addObject("vendor", vendorDto);
        mav.addObject("locations", locations);
    	return mav;
    }

    @GetMapping("/gift/inbox/redeem{id}")
    public ModelAndView giftInboxRedeem(@PathVariable("id") Integer id){
    	ModelAndView mav = new ModelAndView("user/gift-inbox-redeem");

        GiftExchangeDto gift = giftExchangeService.findById(id);

        if(gift==null || gift.getRedeemed()){
            return new ModelAndView("redirect:/my_account/gift/inbox");
        }

        Integer vendorId=gift.getGift().getCreatedBy().getID();
        List<VendorLocation> locations = vendorLocationService.findLocationsByLoginUser(vendorId);

        mav.addObject("gift", gift);
        mav.addObject("product", gift.getGift());
        mav.addObject("refNumber", gift.getRefNumber());
        mav.addObject("locations", locations);
    	return mav;
    }

    @GetMapping("/gift/outbox")
    public ModelAndView giftOutBox(){
    	ModelAndView mav = new ModelAndView("user/gift-outbox");
    	//please don't load all data
        Collection<GiftExchangeDto> allSentGifts = giftExchangeService.getAllSentGifts(UserUtil.INSTANCE.getCurrentUserId());
        mav.addObject("gifts", allSentGifts);
        mav.addObject("totalSpent",
                giftExchangeService.getTotalCostForUser(UserUtil.INSTANCE.getCurrentUserId()));

        Long totalUsers = allSentGifts.stream()
                .filter(distinctByKey(o -> o.getToUser().getID()))
                .count();
        mav.addObject("totalUsers", totalUsers);
        Collection<GiftExchangeDto> allReceivedGifts = giftExchangeService.getAllReceivedGifts(UserUtil.INSTANCE.getCurrentUserId());
        mav.addObject("totalReceived", allReceivedGifts.size());
    	return mav;
    }

    @GetMapping("/gift/outbox/details{id}")
    public ModelAndView giftOutBoxDetails(@PathVariable("id") Integer id){
    	ModelAndView mav = new ModelAndView("user/gift-outbox-details");
        GiftExchangeDto gift = giftExchangeService.findById(id);

        if(gift==null){
            return new ModelAndView("redirect:/my_account/gift/outbox");
        }

        Integer vendorId=gift.getGift().getCreatedBy().getID();

        VendorDto vendorDto = vendorService.findByUser(vendorId);

        mav.addObject("product", gift.getGift());
        mav.addObject("vendor", vendorDto);
    	return mav;
    }

    @PostMapping(value = {"/save"})
    public ModelAndView save(@Valid @ModelAttribute("myAccount") MyAccountDto myAccount, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return new ModelAndView("user/my_account");

        ModelAndView mav = new ModelAndView("redirect:/my_account");
        MyAccountDto myAccountDto = userService.updateMyAccount(UserUtil.INSTANCE.getCurrentUserId(), myAccount);

        if (myAccountDto == null)
            mav.addObject("message", "Error while updating account.");

        return mav;
    }

    @PostMapping("/send_reset_email")
    public ResponseEntity<?> passwordReset(HttpServletRequest request, @RequestParam(value = "lang", defaultValue = "EN") String language) {
        String email = UserUtil.INSTANCE.getCurrentUser().getEmail();

        EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.RESET_PASSWORD, language);
        String baseUrl = CommonUtils.getBaseURL(request);
        emailTemplate.getAdditionalInformation().put("baseUrl", baseUrl);
        new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), email, templateEngine,
                mailService, emailTemplate).send();

        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/credit/add")
    public ModelAndView addCretedit() {
        ModelAndView modelAndView = new ModelAndView("user/add_credit");
        modelAndView.addObject("squareAppID", squareAppID);
        modelAndView.addObject("credit", creditService.getCredit(UserUtil.INSTANCE.getCurrentUserId()));
        return modelAndView;
    }
}
