package com.realroofers.lovappy.web.controller.user;

import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import com.realroofers.lovappy.service.exception.EntityValidationException;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.RegisterExistingUserDto;
import com.realroofers.lovappy.service.user.dto.RegisterUserDto;
import com.realroofers.lovappy.service.user.dto.UserAuthDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.model.UserRoles;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.FormValidator;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.service.validation.ResponseStatus;
import com.realroofers.lovappy.service.validator.UserRegister;
import com.realroofers.lovappy.web.email.EmailTemplateFactory;
import com.realroofers.lovappy.web.external.MessageByLocaleService;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.realroofers.lovappy.web.util.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.social.twitter.api.TweetData;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/user/registration")
public class UserRegistrationController {

    private static Logger LOG = LoggerFactory.getLogger(UserRegistrationController.class);

    private static final String MODEL_ATTRIBUTE_NAME = "info";

    private final UserProfileService userProfileService;

    private final UserService userService;

    private final FormValidator formValidator;

    private ITemplateEngine templateEngine;

    private LogoutHandler logoutHandler;

    private NotificationService notificationService;

    private final CloudStorage cloudStorage;

    private final CloudStorageService cloudStorageService;

    private final MessageByLocaleService messageByLocaleService;
    private final EmailTemplateService emailTemplateService;
    private final MailService mailService;
    private final DaoAuthenticationProvider authenticationProvider;

    @Autowired
    private CMSService cmsService;

    @Autowired
    protected TwitterTemplate twitterTemplate;

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @Autowired
    public UserRegistrationController(UserProfileService userProfileService, UserService userService, CloudStorage cloudStorage, CloudStorageService cloudStorageService,
                                      FormValidator formValidator, NotificationService notificationService, List<ViewResolver> viewResolvers, LogoutHandler logoutHandler, MessageByLocaleService messageByLocaleService, EmailTemplateService emailTemplateService, MailService mailService, DaoAuthenticationProvider authenticationProvider) {

        this.userProfileService = userProfileService;
        this.userService = userService;
        this.cloudStorage = cloudStorage;
        this.cloudStorageService = cloudStorageService;
        this.formValidator = formValidator;
        this.notificationService = notificationService;
        this.logoutHandler = logoutHandler;
        this.messageByLocaleService = messageByLocaleService;
        this.emailTemplateService = emailTemplateService;
        this.mailService = mailService;
        this.authenticationProvider = authenticationProvider;
        for (ViewResolver viewResolver : viewResolvers) {
            if (viewResolver instanceof ThymeleafViewResolver) {
                ThymeleafViewResolver thymeleafViewResolver = (ThymeleafViewResolver) viewResolver;
                this.templateEngine = thymeleafViewResolver.getTemplateEngine();
            }
        }
        if (this.templateEngine == null) {
            throw new BeanInitializationException("No view resolver of type ThymeleafViewResolver found.");
        }
    }

    @PostMapping("/photo")
    public ResponseEntity saveIdentificationProfilePhoto(@RequestParam("photo-id") MultipartFile picture) {

        JsonResponse res = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (picture.isEmpty())
            errorMessages.add(new ErrorMessage("cover", "Add a photo identification picture."));

        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }

        try {
            CloudStorageFileDto cloudStorageFileDto =
                    cloudStorage.uploadFile(picture, imagesBucket, FileExtension.png.toString(), "image/png");
            CloudStorageFileDto added = cloudStorageService.add(cloudStorageFileDto);

            return ResponseEntity.status(HttpStatus.OK).body(added);

        } catch (IOException e) {
            LOG.error(e.getMessage());
            res.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(res);
        }
    }


    @GetMapping("/ambassador/signup")
    public String showAmbassadorRegisterPage(Model model) {

        UserAuthDto currentUser = UserUtil.INSTANCE.getCurrentUser();
        Integer userId = (currentUser != null) ? currentUser.getUserId() : null;
        model.addAttribute("userId", userId);

        int alreadyMember = 0;

        if (currentUser != null) {
            User user = userService.getUserByEmail(currentUser.getEmail());
            if (user != null) {
                ApprovalStatus approvalStatus = null;
                for (UserRoles userRole : user.getRoles()) {
                    if (userRole.getId().getRole().getName().equals(Roles.EVENT_AMBASSADOR)) {
                        approvalStatus = userRole.getApprovalStatus();
                    }
                }
                if (approvalStatus != null) {
                    switch (approvalStatus) {
                        case PENDING:
                            alreadyMember = 1;
                            break;
                        case APPROVED:
                            alreadyMember = 2;
                            break;
                        default:
                            alreadyMember = 0;
                            break;
                    }
                }
            }
        }

        if (alreadyMember==2) {
            return "redirect:/events/browse";
        }

        model.addAttribute("already_member", alreadyMember);

        RegisterUserDto registerUserDto = new RegisterUserDto();
        registerUserDto.setNotificationType(NotificationTypes.AMBASSADOR_REGISTRATION);
        registerUserDto.setRole(Roles.EVENT_AMBASSADOR);

        model.addAttribute(MODEL_ATTRIBUTE_NAME, registerUserDto);
        return "registration/registration-ambassador";
    }

    @GetMapping("/musician/signup")
    public String showMusicianRegisterPage(Model model) {
        Locale locale = LocaleContextHolder.getLocale();
        String language = locale.getLanguage();
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();

        if (userId != null && userService.isMusicianUser(userId)) {
            return "redirect:/music/my-music";
        }
        model.addAttribute("userId", userId);

        model.addAttribute("registered", userId != null && userService.isMusicianUser(userId));

        RegisterUserDto registerUserDto = new RegisterUserDto();
        registerUserDto.setNotificationType(NotificationTypes.MUSICIAN_REGISTRATION);
        registerUserDto.setRole(Roles.MUSICIAN);
        model.addAttribute("musicianTerms", cmsService.findPageByTagWithImagesAndTexts("musician_registration", language).getTexts().stream().filter(textWidgetDTO -> !textWidgetDTO.getTag().equals("RoyaltyMSG")).collect(Collectors.toList()));
        model.addAttribute("musicTerm", cmsService.getTextByTagAndLanguage("RoyaltyMSG", language));

        model.addAttribute(MODEL_ATTRIBUTE_NAME, registerUserDto);
        return "registration/registration-musician";
    }

    @GetMapping("/blogger")
    public String showAuthorRegisterPage(Model model) {

        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        model.addAttribute("userId", userId);

        if (userId != null && userService.isAuthor(userId)) {
            return "redirect:/blog";
        }

        model.addAttribute("registered", userId != null && userService.isAuthor(userId));

        RegisterUserDto registerUserDto = new RegisterUserDto();
        registerUserDto.setNotificationType(NotificationTypes.AUTHOR_REGISTRATION);
        registerUserDto.setRole(Roles.AUTHOR);

        model.addAttribute("authorRegister", registerUserDto);
        return "registration/registration-author";
    }

    @PostMapping("/new/signup")
    public ResponseEntity postAmbassadorRegisterPage(@ModelAttribute @Valid RegisterUserDto dto, @RequestParam(value = "language", defaultValue = "EN") String language,
                                                     HttpServletRequest request) {

        JsonResponse res = new JsonResponse();
        try {
            List<ErrorMessage> errorMessages = new ArrayList<>();
            String name = dto.getName();
            String phoneNumber = dto.getPhoneNumber();

            Integer userId = UserUtil.INSTANCE.getCurrentUserId();

            if (userId == null) {
                User userByEmail = userService.getUserByEmail(dto.getEmail());
                if (userByEmail != null) {
                    errorMessages.add(new ErrorMessage("name", messageByLocaleService.getMessage("message.email-in-use")));
                    res.setErrorMessageList(errorMessages);
                    res.setStatus(ResponseStatus.FAIL);
                    return ResponseEntity.ok().body(res);
                }
            }

            int age = 0;
            if (dto.getBirthDate() != null && dto.getBirthDate().toInstant() != null) {
                LocalDate birthDateLocal = dto.getBirthDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                age = Period.between(birthDateLocal, LocalDate.now()).getYears();
            }
            if (age < 18)
                errorMessages.add(new ErrorMessage("birthDate", messageByLocaleService.getMessage("message.invalid-birthday")));

            if (!dto.getRole().equals(Roles.AUTHOR)) {
                errorMessages = formValidator.validate(dto, new Class[]{UserRegister.class});
                if (!name.matches("[-a-zA-Z \'\\.]+")) {
                    errorMessages.add(new ErrorMessage("name", messageByLocaleService.getMessage("message.characters-only-name")));
                }
                if (!validatePhoneNumber(phoneNumber)) {
                    errorMessages.add(new ErrorMessage("phoneNumber", messageByLocaleService.getMessage("message.incorrect-phone-number")));
                }
                if (dto.getPhotoIdentificationCloudFile().getId() == null) {
                    errorMessages.add(new ErrorMessage("photoIdentificationCloudFile", messageByLocaleService.getMessage("message.no-identification-file")));
                }
            }


            if (errorMessages.size() > 0) {
                res.setErrorMessageList(errorMessages);
                res.setStatus(ResponseStatus.FAIL);
                return ResponseEntity.ok().body(res);
            }
            User ambassador = userService.createNewRegistration(userId, dto);
            userId = ambassador.getUserId();

            String key = UUID.randomUUID().toString();
            UserDto newUser = userService.updateVerifyEmailKey(userId, key);
            if (newUser != null) {
                EmailTemplateDto emailTemplate = emailTemplateService
                        .getByNameAndLanguage(EmailTemplateConstants.ACCOUNT_VERIFICATION, language);
                String baseUrl = CommonUtils.getBaseURL(request);
                emailTemplate.getAdditionalInformation().put("url", baseUrl + "/register/verify/" + key);
                new EmailTemplateFactory().build(CommonUtils.getBaseURL(request), ambassador.getEmail(), templateEngine,
                        mailService, emailTemplate).send();

            }

            NotificationDto notificationDto = new NotificationDto();
            if (dto.getRole().equals(Roles.EVENT_AMBASSADOR)) {
                notificationDto.setContent(dto.getName() + " is registered as an Ambassador in lovappy");
            } else if (dto.getRole().equals(Roles.MUSICIAN)) {
                notificationDto.setContent(dto.getName() + " is registered as a Musician in lovappy");
            } else if (dto.getRole().equals(Roles.AUTHOR)) {
                notificationDto.setContent(dto.getName() + " is registered as an Author in lovappy");
            } else {
                notificationDto.setContent(dto.getName() + " is registered in lovappy");
            }
            notificationDto.setType(dto.getNotificationType());
            notificationDto.setUserId(userId);
            notificationService.sendNotificationToAdmins(notificationDto);

            res.setStatus(ResponseStatus.SUCCESS);

            Integer numberOfMales = userService.countUsersByGender(Gender.MALE);
            Integer numberOfFemales = userService.countUsersByGender(Gender.FEMALE);
            try {
                TweetData tweetData = new TweetData(String.format("%d members males %d female %d new member", (numberOfMales + numberOfFemales), numberOfMales, numberOfFemales));
                twitterTemplate.timelineOperations().updateStatus(tweetData);
            } catch (Exception e) {
                e.printStackTrace();
            }

            authenticateUserAndSetSession(ambassador, request);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body(res);
        }
        return ResponseEntity.ok().body(res);

    }

    @PostMapping("/signup")
    public ResponseEntity postAmbassadorRegisterPage(@ModelAttribute @Valid RegisterExistingUserDto dto, HttpServletRequest request) {

        JsonResponse res = new JsonResponse();
        try {
            List<ErrorMessage> errorMessages = new ArrayList<>();
            String name = dto.getName();
            String phoneNumber = dto.getPhoneNumber();

            if (!dto.getRole().equals(Roles.AUTHOR)) {
                errorMessages = formValidator.validate(dto, new Class[]{UserRegister.class});
                if (!name.matches("[-a-zA-Z \'\\.]+")) {
                    errorMessages.add(new ErrorMessage("name", messageByLocaleService.getMessage("message.characters-only-name")));
                }
                if (!validatePhoneNumber(phoneNumber)) {
                    errorMessages.add(new ErrorMessage("phoneNumber", messageByLocaleService.getMessage("message.incorrect-phone-number")));
                }
                if (dto.getPhotoIdentificationCloudFile().getId() == null) {
                    errorMessages.add(new ErrorMessage("photoIdentificationCloudFile", messageByLocaleService.getMessage("message.no-identification-file")));
                }
            }

            int age = 0;
            if (dto.getBirthDate() != null && dto.getBirthDate().toInstant() != null) {
                LocalDate birthDateLocal = dto.getBirthDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                age = Period.between(birthDateLocal, LocalDate.now()).getYears();
            }
            if (age < 18)
                errorMessages.add(new ErrorMessage("birthDate", messageByLocaleService.getMessage("message.invalid-birthday")));


            if (errorMessages.size() > 0) {
                res.setErrorMessageList(errorMessages);
                res.setStatus(ResponseStatus.FAIL);
                return ResponseEntity.ok().body(res);
            }

            Integer userId = UserUtil.INSTANCE.getCurrentUserId();
            User ambassador = userService.createRegistration(userId, dto);
            userId = ambassador.getUserId();

            NotificationDto notificationDto = new NotificationDto();
            if (dto.getRole().equals(Roles.EVENT_AMBASSADOR)) {
                notificationDto.setContent(dto.getName() + " is registered as an Ambassador in lovappy");
            } else if (dto.getRole().equals(Roles.MUSICIAN)) {
                notificationDto.setContent(dto.getName() + " is registered as an Musician in lovappy");
            } else if (dto.getRole().equals(Roles.AUTHOR)) {
                notificationDto.setContent(dto.getName() + " is registered as an Author in lovappy");
            } else {
                notificationDto.setContent(dto.getName() + " is registered in lovappy");
            }
            notificationDto.setType(dto.getNotificationType());
            notificationDto.setUserId(userId);
            notificationService.sendNotificationToAdmins(notificationDto);

            res.setStatus(ResponseStatus.SUCCESS);

            Integer numberOfMales = userService.countUsersByGender(Gender.MALE);
            Integer numberOfFemales = userService.countUsersByGender(Gender.FEMALE);
            try {
                TweetData tweetData = new TweetData(String.format("%d members males %d female %d new member", (numberOfMales + numberOfFemales), numberOfMales, numberOfFemales));
                twitterTemplate.timelineOperations().updateStatus(tweetData);
            } catch (Exception e) {
                e.printStackTrace();
            }

            authenticateUserAndSetSession(ambassador, request);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body(res);
        }

        return ResponseEntity.ok().body(res);

    }

    @GetMapping("/info")
    public ModelAndView info(ModelAndView modelAndView) throws IOException {

        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        modelAndView.setViewName("user/ambassador-info");
        modelAndView.addObject("userId", userId);
        modelAndView.addObject("userRoles", userService.getRolesForUser(userId));
        modelAndView.addObject("eventAmbassadorInfo", cmsService.getTextByTagAndLanguage(PageConstants.EVENT_AMBASSADOR_INFO, "EN").replace("\n", "<br>"));
        return modelAndView;
    }

    private boolean validatePhoneNumber(String phoneNumber) {

        return !(phoneNumber.length() < 6 || phoneNumber.length() > 15);
    }

    //Login the user after register
    private void authenticateUserAndSetSession(User user, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword());


        SecurityContextHolder.getContext().setAuthentication(token);
        // add authentication to the session
        request.getSession().setAttribute(
                HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
                SecurityContextHolder.getContext());


    }
}
