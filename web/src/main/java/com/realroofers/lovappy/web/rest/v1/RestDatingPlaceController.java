package com.realroofers.lovappy.web.rest.v1;

import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.datingPlaces.DatingPlaceService;
import com.realroofers.lovappy.service.datingPlaces.dto.*;
import com.realroofers.lovappy.service.datingPlaces.dto.mobile.*;
import com.realroofers.lovappy.service.datingPlaces.support.DatingPlaceSupportService;
import com.realroofers.lovappy.service.datingPlaces.support.DatingPlaceUtil;
import com.realroofers.lovappy.service.datingPlaces.support.VerificationType;
import com.realroofers.lovappy.service.datingPlaces.support.google_places.DatingPlaceLocationUtil;
import com.realroofers.lovappy.service.exception.EntityValidationException;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.validation.ErrorMessage;
import com.realroofers.lovappy.service.validation.JsonResponse;
import com.realroofers.lovappy.service.validation.ResponseStatus;
import com.realroofers.lovappy.web.exception.BadRequestException;
import com.realroofers.lovappy.web.support.FileExtension;
import com.realroofers.lovappy.web.util.CloudStorage;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Call;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Darrel Rayen on 10/19/17.
 */
@RestController
@RequestMapping({"/api/v1/dating/places", "/api/v2/dating/places"})
public class RestDatingPlaceController {

    private static final Logger LOG = LoggerFactory.getLogger(RestDatingPlaceController.class);
    private static final String GOOGLE_KEY = "google_key";
    private final CloudStorage cloudStorage;
    private final CloudStorageService cloudStorageService;
    private final DatingPlaceService datingPlaceService;
    private final DatingPlaceSupportService datingPlaceSupportService;
    private final UserService userService;
    private final NotificationService notificationService;
    private final DatingPlaceLocationUtil datingPlaceLocationUtil;
    private String testAccountid = "AC320cae3847a539cae7169df57300ed32";
    private String testAuthToken = "7fc806ef06843975a74f350555a60836";
    private String testSenderNumber = "+15005550006";

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @Value("${twilio.account.id}")
    private String twilioAccountId;

    @Value("${twilio.authentication.token}")
    private String twilioAuthToken;

    @Value("${twilio.phone.number}")
    private String twilioSenderNumber;

    @Value("${google.maps.api.key}")
    private String googleKeyId;

    @Value("${square.applicationId}")
    private String squareAppID;

    private Integer claimId;

    @Autowired
    public RestDatingPlaceController(CloudStorage cloudStorage, CloudStorageService cloudStorageService, DatingPlaceService datingPlaceService, DatingPlaceSupportService datingPlaceSupportService, UserService userService, NotificationService notificationService, DatingPlaceLocationUtil datingPlaceLocationUtil) {
        this.cloudStorage = cloudStorage;
        this.cloudStorageService = cloudStorageService;
        this.datingPlaceService = datingPlaceService;
        this.datingPlaceSupportService = datingPlaceSupportService;
        this.userService = userService;
        this.notificationService = notificationService;
        this.datingPlaceLocationUtil = datingPlaceLocationUtil;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity addDatingPlace(@ModelAttribute("placeCreate") DatingPlaceDto datingPlaceDto) {
        JsonResponse response = new JsonResponse();

        List<ErrorMessage> errorMessages = new ArrayList<>();
        if (errorMessages.size() > 0) {
            response.setErrorMessageList(errorMessages);
            response.setStatus(ResponseStatus.FAIL);
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } else {
            datingPlaceService.addDatingPlace(datingPlaceDto);
            response.setStatus(ResponseStatus.SUCCESS);
            return ResponseEntity.ok().body(response);
        }
    }

    @RequestMapping(value = "/verification/{id}", method = RequestMethod.GET)
    public ModelAndView verification(ModelAndView model, @PathVariable("id") String id) {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        model.setViewName("dating_places/dating-place-owner-verfication");
        model.addObject("placeid", id);
        model.addObject("placeClaim", new DatingPlaceClaimDto());
        model.addObject("verifyPlace", new DatingPlaceClaimDto());
        model.addObject("userId", userId);
        return model;
    }

    @RequestMapping(value = "/claim/save", method = RequestMethod.POST)
    public ResponseEntity saveClaim(@ModelAttribute DatingPlaceClaimDto claimDto) {
        JsonResponse response = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (claimDto.getSendTo().isEmpty() || claimDto.getSendTo() == null) {
            errorMessages.add(new ErrorMessage("phoneNumber", "Mobile Number cannot be empty"));
        }
        if (claimDto.getVerificationType() == null) {
            errorMessages.add(new ErrorMessage("verificationType", "Please select a verification type"));
        }
        if (errorMessages.size() > 0) {
            response.setErrorMessageList(errorMessages);
            response.setStatus(ResponseStatus.FAIL);
            throw new EntityValidationException(errorMessages);
        } else {
            DatingPlaceClaimDto savedClaim = datingPlaceService.addClaim(claimDto);
            Twilio.init(twilioAccountId, twilioAuthToken);
            if (claimDto.getVerificationType().equals(VerificationType.SMS)) {
                Message message = Message.creator(new PhoneNumber(claimDto.getSendTo()),
                        new PhoneNumber(twilioSenderNumber), "Thanks for registering your business with Lovappy Dating Places. Your verification code is " + savedClaim.getVerificationCode() + " Please use this code to complete registration.").create();
                savedClaim.setContent(message.getSid());
                datingPlaceService.updateClaim(savedClaim);
                response.setResult(message.getSid());
            } else if (claimDto.getVerificationType().equals(VerificationType.CALL)) {
                try {
                    Call call = Call.creator(new PhoneNumber(claimDto.getSendTo()), new PhoneNumber(twilioSenderNumber),
                            new URI("https://handler.twilio.com/twiml/EHb45bfbb40cd012384a37bd12c2c83e68?verificationCode=" + savedClaim.getVerificationCode())).create();
                    savedClaim.setContent(call.getSid());
                    datingPlaceService.updateClaim(savedClaim);
                    response.setResult(call.getSid());
                } catch (URISyntaxException e) {
                    //e.printStackTrace();
                }
                claimId = savedClaim.getClaimId();
            }

            response.setStatus(ResponseStatus.SUCCESS);
            return ResponseEntity.ok().body(response);
        }
    }

    @RequestMapping(value = "/claim/resend/{messageId}", method = RequestMethod.POST)
    public ResponseEntity resendVerification(@PathVariable("messageId") String messageId) {
        JsonResponse response = new JsonResponse();
        if (messageId != null) {
            DatingPlaceClaimDto datingPlaceClaimDto = datingPlaceService.findPlaceClaimById(messageId);
            Twilio.init(twilioAccountId, twilioAuthToken);
            if (datingPlaceClaimDto != null) {
                if (datingPlaceClaimDto.getVerificationType().equals(VerificationType.SMS)) {
                    Message message = Message.creator(new PhoneNumber(datingPlaceClaimDto.getSendTo()),
                            new PhoneNumber(twilioSenderNumber), "Your verification code is " + datingPlaceClaimDto.getVerificationCode()).create();
                    datingPlaceClaimDto.setContent(message.getSid());
                } else if (datingPlaceClaimDto.getVerificationType().equals(VerificationType.CALL)) {
                    try {
                        Call call = Call.creator(new PhoneNumber(datingPlaceClaimDto.getSendTo()), new PhoneNumber(twilioSenderNumber),
                                new URI("https://handler.twilio.com/twiml/EHb45bfbb40cd012384a37bd12c2c83e68?verificationCode=" + datingPlaceClaimDto.getVerificationCode())).create();
                    } catch (URISyntaxException e) {
                        //e.printStackTrace();
                    }
                }
                response.setStatus(ResponseStatus.SUCCESS);
                return ResponseEntity.ok().body(response);
            }
        }
        response.setStatus(ResponseStatus.FAIL);
        return ResponseEntity.ok().body(response);
    }

    @RequestMapping(value = "/claim/verify", method = RequestMethod.POST)
    public ResponseEntity verifyOwner(@ModelAttribute("verifyPlace") DatingPlaceClaimDto claimDto) throws IOException {
        JsonResponse response = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (DatingPlaceUtil.isInteger(claimDto.getPlaceId()) && datingPlaceService.isAlreadyClaimed(Integer.parseInt(claimDto.getPlaceId()))) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
        if (claimDto.getVerificationCode().isEmpty() || claimDto.getVerificationCode() == null) {
            errorMessages.add(new ErrorMessage("verificationCode", "Please enter the verification code you received"));

        } else if (!datingPlaceService.isOwnerVerified(claimDto.getContent(), claimDto.getVerificationCode())) {
            errorMessages.add(new ErrorMessage("verificationCode", "The verification code you have entered is incorrect"));
        }
        if (errorMessages.size() > 0) {
            response.setErrorMessageList(errorMessages);
            response.setStatus(ResponseStatus.FAIL);
            throw new EntityValidationException(errorMessages);
        } else {
            if (DatingPlaceUtil.isInteger(claimDto.getPlaceId())) {
                datingPlaceService.addOwnerToExistingPlace(Integer.parseInt(claimDto.getPlaceId()), claimDto.getClaimerId());
                return ResponseEntity.ok().body(response);
            } else {
                DatingPlaceDto datingGooglePlaceDto = datingPlaceLocationUtil.getGooglePlaceDetails(claimDto.getPlaceId());
                DatingPlaceDto datingPlaceDto = datingPlaceService.addOwnerToGooglePlace(claimDto, datingGooglePlaceDto);
                response.setResult(datingPlaceDto.getDatingPlacesId());
                return ResponseEntity.ok().body(response);
            }
        }
    }


    @RequestMapping(value = "/claim/google/{id}", method = RequestMethod.GET)
    public ModelAndView completeGoogleVerification(ModelAndView modelAndView, @PathVariable("id") String id, @RequestParam(value = "type", required = false) String type) {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        modelAndView.setViewName("dating_places/add-missing-info-dating-place");
        modelAndView.addObject("userId", userId);
        modelAndView.addObject("isFeatured", Objects.equals(type, "featured"));
        modelAndView.addObject("userId", userId);
        modelAndView.addObject("datingPlace", datingPlaceService.findDatingPlaceById(Integer.parseInt(id)));
        modelAndView.addObject("userRoles", userService.getRolesForUser(userId));
        modelAndView.addObject("atmospheres", datingPlaceSupportService.getAllAtmospheresEnabled());
        modelAndView.addObject("foodTypes", datingPlaceSupportService.getAllFoodTypesEnabled());
        modelAndView.addObject(GOOGLE_KEY, googleKeyId);
        modelAndView.addObject("squareAppID", squareAppID);
        return modelAndView;
    }

    @RequestMapping(value = "/google/complete", method = RequestMethod.POST)
    public ResponseEntity completeDatingPlace(@ModelAttribute("datingPlace") DatingPlaceDto datingPlaceDto) {
        JsonResponse response = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        if (datingPlaceDto.getPlaceName().isEmpty()) {
            errorMessages.add(new ErrorMessage("placeName", "Provide a Name for the Dating Place"));
        }
        if (datingPlaceDto.getCity().isEmpty()) {
            errorMessages.add(new ErrorMessage("city", "Provide a city"));
        }
        if (datingPlaceDto.getAddressLine().isEmpty()) {
            errorMessages.add(new ErrorMessage("addressLine", "Provide an address"));
        }
        if (datingPlaceDto.getState().isEmpty()) {
            errorMessages.add(new ErrorMessage("state", "Provide a state"));
        }
        if (datingPlaceDto.getCountry().isEmpty()) {
            errorMessages.add(new ErrorMessage("country", "Select a Country"));
        }
        if (datingPlaceDto.getZipCode().isEmpty()) {
            errorMessages.add(new ErrorMessage("zipCode", "Provide a Zipcode"));
        }
        if (datingPlaceDto.getAgeRange() == null) {
            errorMessages.add(new ErrorMessage("ageRange", "Select an Age Range"));
        }
        if (datingPlaceDto.getGender() == null) {
            errorMessages.add(new ErrorMessage("gender", "Select Gender"));
        }
        if (datingPlaceDto.getPriceRange() == null) {
            errorMessages.add(new ErrorMessage("priceRange", "Select suitable Price Range"));
        }
        if (datingPlaceDto.getPlaceDescription() == null) {
            errorMessages.add(new ErrorMessage("placeDescription", "Please enter a place description"));
        }
        if (datingPlaceDto.getPlaceAtmospheres() == null) {
            errorMessages.add(new ErrorMessage("Atmosphere", "Select atleast one Atmosphere"));
        }
        if (datingPlaceDto.getFoodTypes() == null) {
            errorMessages.add(new ErrorMessage("FoodType", "Select the Food types available"));
        }
        if (datingPlaceDto.getIsPetsAllowed() == null) {
            errorMessages.add(new ErrorMessage("Pets", "Select if pets are allowed"));
        }
        if (datingPlaceDto.getNeighborHood() == null) {
            errorMessages.add(new ErrorMessage("neighborhood", "Select a suitable neighborhood"));
        }
        if (datingPlaceDto.getIsFeatured() == null) {
            errorMessages.add(new ErrorMessage("Featured", "Select if you want to feature your place"));
        }

        if (errorMessages.size() > 0) {
            response.setErrorMessageList(errorMessages);
            response.setStatus(ResponseStatus.FAIL);
            throw new EntityValidationException(errorMessages);
        } else {

            DatingPlaceDto updatedPlace = datingPlaceService.completeGoogleClaiming(datingPlaceDto);
            if (updatedPlace != null) {
                response.setStatus(ResponseStatus.SUCCESS);
                return ResponseEntity.ok().body(response);
            }
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @RequestMapping(value = "/owner/update", method = RequestMethod.POST)
    public ResponseEntity updateDatingPlace(@ModelAttribute("datingPlace") DatingPlaceDto datingPlaceDto) {
        JsonResponse response = new JsonResponse();
        List<ErrorMessage> errorMessages = new ArrayList<>();
        if (datingPlaceDto.getPlaceName().isEmpty()) {
            errorMessages.add(new ErrorMessage("placeName", "Provide a Name for the Dating Place"));
        }
        if (datingPlaceDto.getCity().isEmpty()) {
            errorMessages.add(new ErrorMessage("city", "Provide a city"));
        }
        if (datingPlaceDto.getAddressLine().isEmpty()) {
            errorMessages.add(new ErrorMessage("addressLine", "Provide an address"));
        }
        if (datingPlaceDto.getState().isEmpty()) {
            errorMessages.add(new ErrorMessage("state", "Provide a state"));
        }
        if (datingPlaceDto.getCountry().isEmpty()) {
            errorMessages.add(new ErrorMessage("country", "Select a Country"));
        }
        if (datingPlaceDto.getZipCode().isEmpty()) {
            errorMessages.add(new ErrorMessage("zipCode", "Provide a Zipcode"));
        }
        if (datingPlaceDto.getAgeRange() == null) {
            errorMessages.add(new ErrorMessage("ageRange", "Select an Age Range"));
        }
        if (datingPlaceDto.getGender() == null) {
            errorMessages.add(new ErrorMessage("gender", "Select Gender"));
        }
        if (datingPlaceDto.getPriceRange() == null) {
            errorMessages.add(new ErrorMessage("priceRange", "Select suitable Price Range"));
        }
        if (datingPlaceDto.getPlaceDescription() == null) {
            errorMessages.add(new ErrorMessage("placeDescription", "Please enter a place description"));
        }
/*        if (datingPlaceDto.getPlaceAtmospheres() == null) {
            errorMessages.add(new ErrorMessage("Atmosphere", "Select atleast one Atmosphere"));
        }
        if (datingPlaceDto.getFoodTypes() == null) {
            errorMessages.add(new ErrorMessage("FoodType", "Select the Food types available"));
        }*/
        if (datingPlaceDto.getIsPetsAllowed() == null) {
            errorMessages.add(new ErrorMessage("Pets", "Select if pets are allowed"));
        }
        if (datingPlaceDto.getNeighborHood() == null) {
            errorMessages.add(new ErrorMessage("neighborhood", "Select a suitable neighborhood"));
        }
/*        if (datingPlaceDto.getFeatured() == null) {
            errorMessages.add(new ErrorMessage("Featured", "Select if you want to feature your place"));
        }*/
        if (errorMessages.size() > 0) {
            response.setErrorMessageList(errorMessages);
            response.setStatus(ResponseStatus.FAIL);
            throw new EntityValidationException(errorMessages);
        } else {

            DatingPlaceDto updatedPlace = datingPlaceService.updateDatingPlaceByOwner(datingPlaceDto);
            if (updatedPlace != null) {
                response.setStatus(ResponseStatus.SUCCESS);
                return ResponseEntity.ok().body(response);
            }
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @RequestMapping(value = "/upload/upload-dating-place-pictures/{datingPlaceId}", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> uploadDatingPlacePictures(@RequestParam("files[]") MultipartFile[] pictures, @PathVariable("datingPlaceId") String placeId) {

        List<ErrorMessage> errorMessages = new ArrayList<>();
        List<CloudStorageFileDto> list = new ArrayList<>();
        DatingPlaceDto datingPlaceDto = datingPlaceService.findDatingPlaceById(Integer.parseInt(placeId));
        List<PlacesAttachmentDto> placesAttachmentDtoList = new ArrayList<>();

        if (pictures.length == 0)
            errorMessages.add(new ErrorMessage("uploadDatingPictures", "Add atleast ONE Location picture."));

        if (errorMessages.size() > 0) {
            throw new EntityValidationException(errorMessages);
        }
        try {

            List<ImageDto> fileList = new ArrayList<>();
            for (MultipartFile picture : pictures) {
                PlacesAttachmentDto attachmentDto = new PlacesAttachmentDto();
                CloudStorageFileDto cloudStorageFileDto = cloudStorage.uploadFile(picture, imagesBucket, FileExtension.jpg.toString(), "image/jpeg");
                CloudStorageFileDto added = cloudStorageService.add(cloudStorageFileDto);
                attachmentDto.setPicture(added);
                attachmentDto.setPlaceId(datingPlaceDto.getDatingPlace());
                attachmentDto.setUrl(added.getUrl());
                placesAttachmentDtoList.add(attachmentDto);
                list.add(added);
                ImageDto imageDto = new ImageDto(added.getUrl(), added.getName(), added.getDuration());
                fileList.add(imageDto);
            }
            DatingPlaceDto datingPlaceDto1 = datingPlaceService.addImagesToPlace(placesAttachmentDtoList);
            LOG.debug("Dating Location Photos saved to cloud storage");
            Map<String, Object> files = new HashedMap();
            files.put("files", fileList);
            return ResponseEntity.ok(files);

        } catch (IOException e) {
            LOG.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * This api returns a paginated list of places depending on the user location
     *
     * @param page      - parameter accepts the page number of results requested
     * @param latitude  - parameter accepts the users latitude
     * @param longitude - parameter accepts the users longitude
     * @return a paginated list of Dating Places
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Page<DatingPlaceMobileDto>> getAllPlaces(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "latitude", required = false) Double latitude,
            @RequestParam(value = "longitude", required = false) Double longitude) {
        if (page < 1) {
            throw new RuntimeException("Invalid Page Number");
        }
        LocationDto locationDto = new LocationDto();
        if(latitude ==null || longitude == null){
            latitude =  UserUtil.INSTANCE.getCurrentUser().getLatitude();
            longitude = UserUtil.INSTANCE.getCurrentUser().getLongitude();
        }
        locationDto.setLatitude(latitude);
        locationDto.setLongitude(longitude);
        Page<DatingPlaceMobileDto> datingPlaceDtos;
       /* try {

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }*/
        datingPlaceDtos = datingPlaceService.findPlacesByNewFilters(
                new PageRequest(page - 1, 10), locationDto).map(DatingPlaceMobileDto::new);
        return new ResponseEntity<>(datingPlaceDtos, HttpStatus.OK);
    }

    /**
     * This api returns a paginated list of places depending on the Dating Place Rating
     *
     * @param page         - parameter accepts the page number of results requested
     * @param latitude     - parameter accepts the users latitude
     * @param longitude    - parameter accepts the users longitude
     * @param isDescending - boolean parameter accepts whether the list is sorted in Ascending/Descending order
     * @return a paginated list of Dating Places
     */
    @RequestMapping(value = "/rating", method = RequestMethod.GET)
    public ResponseEntity<Page<DatingPlaceMobileDto>> getAllPlacesByRating(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam("latitude") Double latitude, @RequestParam("longitude") Double longitude,
            @RequestParam("isDescending") Boolean isDescending) {
        if (page < 1) {
            throw new RuntimeException("Invalid Page Number");
        }
        Page<DatingPlaceMobileDto> datingPlaceDtos;
        try {
            datingPlaceDtos = datingPlaceService.getAllPlacesByRating(
                    isDescending, latitude, longitude, new PageRequest(page - 1, 10)).map(DatingPlaceMobileDto::new);
            return new ResponseEntity<>(datingPlaceDtos, HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * This api returns a List of places depending on the Dating Place Rating
     *
     * @param latitude     - parameter accepts the users latitude
     * @param longitude    - parameter accepts the users longitude
     * @param isDescending - boolean parameter accepts whether the list is sorted in Ascending/Descending order
     * @return a paginated List of Dating Places
     */
    @RequestMapping(value = "/rating/list", method = RequestMethod.GET)
    public ResponseEntity<List<DatingPlaceMobileDto>> getSortedPlacesList(@RequestParam("latitude") Double latitude,
                                                                          @RequestParam("longitude") Double longitude,
                                                                          @RequestParam("isDescending") Boolean isDescending) {
        List<DatingPlaceMobileDto> mobileDtoList = new ArrayList<>();
        try {
            List<DatingPlaceDto> datingPlaceDtos = datingPlaceService.getAllPlacesListByRating(isDescending, latitude, longitude);
            datingPlaceDtos.forEach(datingPlaceDto -> {
                mobileDtoList.add(new DatingPlaceMobileDto(datingPlaceDto));
            });
            return new ResponseEntity<>(mobileDtoList, HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This api returns a LIST of places depending on the user location
     *
     * @param latitude  - parameter accepts the users latitude
     * @param longitude - parameter accepts the users longitude
     * @return a list of Dating Places
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<DatingPlaceMobileDto>> getAllPlacesList(@RequestParam("latitude") Double latitude, @RequestParam("longitude") Double longitude) {
        List<DatingPlaceMobileDto> datingPlaceDtos = null;
        try {
            datingPlaceDtos = datingPlaceService.getAllMobileNearByPlaces(latitude, longitude);
            return new ResponseEntity<>(datingPlaceDtos, HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This api returns the Dating Place depending on the placeId
     *
     * @param id - parameter accepts the place id of a Dating Place
     * @return a Dating place Object
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<DatingPlaceMobileDto> getPlaceDetails(@PathVariable("id") String id) {
        if (StringUtils.isNumeric(id)) {
            DatingPlaceMobileDto datingPlaceDto = datingPlaceService.findMobilePlaceById(Integer.parseInt(id));
            return new ResponseEntity<>(datingPlaceDto, HttpStatus.OK);
        } else {
            try {
                DatingPlaceMobileDto datingPlaceDto = datingPlaceService.findMobileGooglePlaceById(id);
                return new ResponseEntity<>(datingPlaceDto, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * This api returns a LIST of FEATURED places depending on the user location
     *
     * @param latitude  - parameter accepts the users latitude
     * @param longitude - parameter accepts the users longitude
     * @return a list of FEATURED Dating Places
     */
    @RequestMapping(value = "/featured", method = RequestMethod.GET)
    public ResponseEntity<List<DatingPlaceDto>> getFeaturedPlacesList(
            @RequestParam(value =  "latitude", required = false) Double latitude, @RequestParam(value = "longitude", required = false) Double longitude) {
        if(latitude ==null || longitude == null){
            latitude =  UserUtil.INSTANCE.getCurrentUser().getLatitude();
            longitude = UserUtil.INSTANCE.getCurrentUser().getLongitude();
        }
        LocationDto locationDto = new LocationDto();
        locationDto.setLatitude(latitude);
        locationDto.setLongitude(longitude);
        List<DatingPlaceDto> datingPlaceDtos = datingPlaceService.getFeaturedPlaces(locationDto);
        return new ResponseEntity<>(datingPlaceDtos, HttpStatus.OK);
    }
    @RequestMapping(value = "/featured/paginated", method = RequestMethod.GET)
    public ResponseEntity<Page<DatingPlaceDto>> getFeaturedPlacesPage(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                                      @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                                                      @RequestParam(value =  "latitude", required = false) Double latitude,
                                                                      @RequestParam(value = "longitude", required = false) Double longitude) {
        if(latitude ==null || longitude == null){
            latitude =  UserUtil.INSTANCE.getCurrentUser().getLatitude();
            longitude = UserUtil.INSTANCE.getCurrentUser().getLongitude();
        }
        LocationDto locationDto = new LocationDto();
        locationDto.setLatitude(latitude);
        locationDto.setLongitude(longitude);
        locationDto.setRadius(20D);
        Page<DatingPlaceDto> datingPlaceDtos = datingPlaceService.getAllNearByFeaturedPlaces(new PageRequest(page-1, limit),locationDto);
        return new ResponseEntity<>(datingPlaceDtos, HttpStatus.OK);
    }
    @RequestMapping(value = "/deals", method = RequestMethod.GET)
    public ResponseEntity<Page<DatingPlaceDealsMobileDto>> getActivePlaceDeals(@RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "id") Integer placeId, @RequestParam(value = "start") @DateTimeFormat(pattern = "yyyy-MM-dd") DateTime date) {
        if (page < 1) {
            throw new RuntimeException("Invalid Page Number");
        }
        Page<DatingPlaceDealsMobileDto> datingPlaceDealsDtos = datingPlaceService.getActiveDealsByPlaceIdAndDateMobile(new PageRequest(page - 1, 10), placeId, date.toDate());
        return new ResponseEntity<>(datingPlaceDealsDtos, HttpStatus.OK);

    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public Page<DatingPlaceMobileDto> getPlaceSearchResults(@RequestParam(value = "page", defaultValue = "1") Integer page, @RequestBody DatingPlaceMobileFilterDto datingPlaceMobileFilterDto) {

            DatingPlaceFilterDto datingPlaceFilterDto = new DatingPlaceFilterDto();
            LocationDto locationDto = new LocationDto(datingPlaceMobileFilterDto.getLatitude(),
                    datingPlaceMobileFilterDto.getLongitude());
            datingPlaceFilterDto.setGender(datingPlaceMobileFilterDto.getGender());
            datingPlaceFilterDto.setAgeRange(datingPlaceMobileFilterDto.getAgeRange());
            datingPlaceFilterDto.setPetsAllowed(datingPlaceMobileFilterDto.getPetsAllowed());
            datingPlaceFilterDto.setGoodForCouple(datingPlaceMobileFilterDto.getGoodForCouple());
            datingPlaceFilterDto.setPlaceAtmosphere(datingPlaceMobileFilterDto.getPlaceAtmosphere());
            datingPlaceFilterDto.setPersonTypes(datingPlaceMobileFilterDto.getPersonTypes());
            datingPlaceFilterDto.setFoodTypes(datingPlaceMobileFilterDto.getFoodType());
            datingPlaceFilterDto.setNeighborhood(datingPlaceMobileFilterDto.getNeighborhood());
            datingPlaceFilterDto.setSecurity(datingPlaceMobileFilterDto.getSecurity());
            datingPlaceFilterDto.setParking(datingPlaceMobileFilterDto.getParking());
            datingPlaceFilterDto.setTransport(datingPlaceMobileFilterDto.getTransport());
            datingPlaceFilterDto.setCurrentDate(datingPlaceMobileFilterDto.getCurrentDate());
            datingPlaceFilterDto.setDealsActive(datingPlaceMobileFilterDto.getDealsActive());
             LOG.error("Daoud datingPlaceFilterDto {}", datingPlaceMobileFilterDto.toString());
            Page<DatingPlaceMobileDto> datingPlaceMobileDtos = datingPlaceService.findPlacesByAllFiltersMobile(new PageRequest(page - 1, 10), datingPlaceFilterDto, locationDto);
        LOG.error("Daoud result {}", datingPlaceMobileDtos.getNumberOfElements());
            return datingPlaceMobileDtos;
    }

    @RequestMapping(value = "/search/list", method = RequestMethod.POST)
    public ResponseEntity<List<DatingPlaceMobileDto>> getPlaceSearchResultsList(@RequestBody DatingPlaceMobileFilterDto datingPlaceMobileFilterDto) {
        try {
            DatingPlaceFilterDto datingPlaceFilterDto = new DatingPlaceFilterDto();
            LocationDto locationDto = new LocationDto(datingPlaceMobileFilterDto.getLatitude(),
                    datingPlaceMobileFilterDto.getLongitude());
            datingPlaceFilterDto.setGender(datingPlaceMobileFilterDto.getGender());
            datingPlaceFilterDto.setAgeRange(datingPlaceMobileFilterDto.getAgeRange());
            datingPlaceFilterDto.setPetsAllowed(datingPlaceMobileFilterDto.getPetsAllowed());
            datingPlaceFilterDto.setGoodForCouple(datingPlaceMobileFilterDto.getGoodForCouple());
            datingPlaceFilterDto.setPlaceAtmosphere(datingPlaceMobileFilterDto.getPlaceAtmosphere());
            datingPlaceFilterDto.setPersonTypes(datingPlaceMobileFilterDto.getPersonTypes());
            datingPlaceFilterDto.setFoodTypes(datingPlaceMobileFilterDto.getFoodType());
            datingPlaceFilterDto.setNeighborhood(datingPlaceMobileFilterDto.getNeighborhood());
            datingPlaceFilterDto.setSecurity(datingPlaceMobileFilterDto.getSecurity());
            datingPlaceFilterDto.setParking(datingPlaceMobileFilterDto.getParking());
            datingPlaceFilterDto.setTransport(datingPlaceMobileFilterDto.getTransport());
            datingPlaceFilterDto.setCurrentDate(datingPlaceMobileFilterDto.getCurrentDate());
            datingPlaceFilterDto.setDealsActive(datingPlaceMobileFilterDto.getDealsActive());
            List<DatingPlaceMobileDto> datingPlaceMobileDtos = datingPlaceService.findPlacesByAllFiltersMobile(datingPlaceFilterDto, locationDto);
            return new ResponseEntity<>(datingPlaceMobileDtos, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addNewDatingPlace(@RequestBody AddDatingPlaceMobileDto datingPlaceMobileDto) {
        List<ErrorMessage> errorMessages = new ArrayList<>();
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        if (datingPlaceMobileDto.getUserType() == null) {

            errorMessages.add(new ErrorMessage("userType", "Select whether you want to Review Place/Own the Place"));
        }
        if (datingPlaceMobileDto.getPlaceName().isEmpty()) {
            errorMessages.add(new ErrorMessage("Place name", "Provide a Name for the Dating Place"));
        }
        if (datingPlaceMobileDto.getCity().isEmpty()) {
            errorMessages.add(new ErrorMessage("City", "Provide a city"));
        }
        if (datingPlaceMobileDto.getAddressLine().isEmpty()) {
            errorMessages.add(new ErrorMessage("AddressLine", "Provide an address"));
        }
        if (datingPlaceMobileDto.getState().isEmpty()) {
            errorMessages.add(new ErrorMessage("State", "Provide a state"));
        }
        if (datingPlaceMobileDto.getCountry().isEmpty()) {
            errorMessages.add(new ErrorMessage("Country", "Select a Country"));
        }
        if (datingPlaceMobileDto.getZipCode().isEmpty()) {
            errorMessages.add(new ErrorMessage("Zipcode", "Provide a Zipcode"));
        }
/*        if (locationPictureId == null) {
            errorMessages.add(new ErrorMessage("Image", "Upload an image"));
        }*/
        if (datingPlaceMobileDto.getAgeRange() == null) {
            errorMessages.add(new ErrorMessage("Age Range", "Select an Age Range"));
        }
        if (datingPlaceMobileDto.getGender() == null) {
            errorMessages.add(new ErrorMessage("Gender", "Select Gender"));
        }
        if (datingPlaceMobileDto.getPriceRange() == null) {
            errorMessages.add(new ErrorMessage("Price Range", "Select suitable Price Range"));
        }
        if (datingPlaceMobileDto.getLatitude() == null || datingPlaceMobileDto.getLongitude() == null) {
            errorMessages.add(new ErrorMessage("verify", "Please Verify the location you've entered"));
        }

        if (errorMessages.size() > 0) {
            return new ResponseEntity<>(errorMessages, HttpStatus.BAD_REQUEST);
        } else {
            try {
                DatingPlaceMobileDto mobileDto = datingPlaceService.addDatingPlaceMobile(datingPlaceMobileDto);
                if (mobileDto != null) {
                    NotificationDto notificationDto = new NotificationDto();
                    notificationDto.setContent(mobileDto.getPlaceName() + " is a new Dating Place in LovAppy");
                    notificationDto.setType(NotificationTypes.PLACE_APPROVAL);
                    notificationDto.setUserId(userId);
                    notificationService.sendNotificationToAdmins(notificationDto);
                }
                return new ResponseEntity<DatingPlaceMobileDto>(mobileDto, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<DatingPlaceMobileDto>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }


    /**
     * This api is used to Add review to a Dating Place
     *
     * @param id                         - parameter accepts the place id of the Dating Place
     * @param datingPlaceReviewMobileDto - accepts the request body of the PlaceReview Model Object
     */
    @RequestMapping(value = "/reviews/{placeId}", method = RequestMethod.POST)
    ResponseEntity addPlaceReview(@PathVariable("placeId") String id, @RequestBody AddDatingPlaceReviewMobileDto datingPlaceReviewMobileDto) throws IOException {
        List<ErrorMessage> errorMessages = new ArrayList<>();
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        DatingPlaceDto datingPlaceDto = null;
        if (StringUtils.isNumeric(id)) {
            datingPlaceDto = datingPlaceService.findDatingPlaceById(Integer.parseInt(id));
            if (datingPlaceDto == null) {
                errorMessages.add(new ErrorMessage("Place", "Place not found"));
            } else {
                datingPlaceReviewMobileDto.setPlaceId(id);
            }
        } else {
            datingPlaceReviewMobileDto.setGooglePlaceId(id);
            datingPlaceDto = datingPlaceLocationUtil.getGooglePlaceDetails(id);
        }
        if (datingPlaceReviewMobileDto.getReviewerEmail().isEmpty()) {
            errorMessages.add(new ErrorMessage("createEmail", "Provide your Email Address"));
        }
//        if (!DatingPlaceUtil.isValidateEmail(datingPlaceReviewMobileDto.getReviewerEmail())) {
//            errorMessages.add(new ErrorMessage("createEmail", "Provide a valid Email Address"));
//        }
       /* if (datingPlaceReviewMobileDto.getReviewerComment().isEmpty()) {
            errorMessages.add(new ErrorMessage("reviewerComment", "Provide a review"));
        }*/
        if (datingPlaceReviewMobileDto.getSecurityRating() == null || datingPlaceReviewMobileDto.getParkingAccessRating() == null || datingPlaceReviewMobileDto.getParkingRating() == null) {
            errorMessages.add(new ErrorMessage("rating", "Select a rating"));
        }

        if (datingPlaceReviewMobileDto.getSuitableAgeRange() == null || datingPlaceReviewMobileDto.getSuitableGender() == null || datingPlaceReviewMobileDto.getSuitablePriceRange() == null) {
            errorMessages.add(new ErrorMessage("suitable", "Select suitable options"));
        }

        if (datingPlaceReviewMobileDto.getOverAllRating() == null) {

            errorMessages.add(new ErrorMessage("overall", "Select an over all rating"));
        }
        if (errorMessages.size() > 0) {
            return new ResponseEntity<>(errorMessages, HttpStatus.BAD_REQUEST);
        }
        datingPlaceDto.setCreateEmail(datingPlaceReviewMobileDto.getReviewerEmail());
        DatingPlaceReviewMobileDto placeReviewMobileDto = datingPlaceService.addDatingPlaceReviewMobile(datingPlaceReviewMobileDto, datingPlaceDto);
        if (placeReviewMobileDto != null) {
            NotificationDto notificationDto = new NotificationDto();
            if (placeReviewMobileDto.getPlaceId() != null) {
                notificationDto.setContent("A new review for " + placeReviewMobileDto.getPlaceId() + " has been added");
            } else if (placeReviewMobileDto.getGooglePlaceId() != null) {
                notificationDto.setContent("A new review for " + placeReviewMobileDto.getGooglePlaceId() + " has been added");
            }
            notificationDto.setType(NotificationTypes.REVIEW_APPROVAL);
            notificationDto.setUserId(userId);
            notificationService.sendNotificationToAdmins(notificationDto);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * This api is used to return a list of Food Types available
     *
     * @return a list of enabled FoodType
     */
    @RequestMapping(value = "/foodTypes", method = RequestMethod.GET)
    public ResponseEntity<List<FoodTypeDto>> getActiveFoodTypes() {
        List<FoodTypeDto> foodTypeList = datingPlaceSupportService.getAllFoodTypesEnabled();
        return new ResponseEntity<>(foodTypeList, HttpStatus.OK);
    }

    /**
     * This api is used to return a list of Person Types available
     *
     * @return a list of enabled PersonType
     */
    @RequestMapping(value = "/personTypes", method = RequestMethod.GET)
    public ResponseEntity<List<PersonTypeDto>> getActivePersonTypes() {
        List<PersonTypeDto> personTypeDtos = datingPlaceSupportService.getAllPersonTypesEnabled();
        return new ResponseEntity<>(personTypeDtos, HttpStatus.OK);
    }

    /**
     * This api is used to return a list of Atmosphere Types available
     *
     * @return a list of enabled AtmosphereType
     */
    @RequestMapping(value = "/atmospheres", method = RequestMethod.GET)
    public ResponseEntity<List<PlaceAtmosphereDto>> getActivePlaceAtmospheres() {
        List<PlaceAtmosphereDto> placeAtmosphereDtos = datingPlaceSupportService.getAllAtmospheresEnabled();
        return new ResponseEntity<>(placeAtmosphereDtos, HttpStatus.OK);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<Page<DatingPlaceMobileDto>> getPlacesBySearchTerm(@RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam("radius") Double radius, @RequestParam("searchTerm") String searchTerm, @RequestParam("latitude") Double latitude, @RequestParam("longitude") Double longitude) {
        try {
            Page<DatingPlaceMobileDto> placeMobileDtos = datingPlaceService.findPlacesBySearchTerm(new PageRequest(page - 1, 10), searchTerm, latitude, longitude, radius);
            return new ResponseEntity<>(placeMobileDtos, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This api returns a paginated list of Dating Places
     *
     * @param page        - parameter accepts the page number of results requested
     * @param radius      - parameter accepts the radius limit requested by user
     * @param latitude    - parameter accepts the users latitude
     * @param longitude   - parameter accepts the users longitude
     * @param gender      - parameter accepts the gender requested by the user
     * @param isWorldWide - parameter accepts boolean whether results requires world wide coverage
     * @param isLocal     - parameter accepts boolean whether results requires only user location based coverage
     * @return a paginated list of filtered Dating Places
     */
    @RequestMapping(value = "/quickFilter", method = RequestMethod.GET)
    public Page<DatingPlaceMobileDto> getPlacesByFilter(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "limit", defaultValue = "10", required = false) Integer limit,
            @RequestParam(value = "radius", required = false) Double radius,
            @RequestParam(value = "latitude", required = false) Double latitude,
            @RequestParam(value = "longitude", required = false) Double longitude,
            @RequestParam(value = "gender", required = false) Gender gender,
            @RequestParam(value = "worldWide", required = false) Boolean isWorldWide,
            @RequestParam(value = "featured", required = false) Boolean featured,
            @RequestParam(value = "local", required = false) Boolean isLocal) {

        if (isLocal != null && isLocal) {
            if(latitude ==null || longitude == null){
                latitude =  UserUtil.INSTANCE.getCurrentUser().getLatitude();
                longitude = UserUtil.INSTANCE.getCurrentUser().getLongitude();
            }
        }
        LocationDto locationDto = new LocationDto(latitude, longitude, radius);
        DatingPlaceFilterDto filterDto = new DatingPlaceFilterDto();
        filterDto.setGender(gender);
        filterDto.setWorldWide(isWorldWide);
        filterDto.setLocal(isLocal);
        filterDto.setFeatured(featured);
        Page<DatingPlaceMobileDto> placeMobileDtos = datingPlaceService.findPlacesByQuickFilters(
               page , limit, filterDto, locationDto).map(DatingPlaceMobileDto::new);

        return placeMobileDtos;
    }

    /**
     * This api returns a Filtered list of Dating Places
     *
     * @param radius      - parameter accepts the radius limit requested by user
     * @param latitude    - parameter accepts the users latitude
     * @param longitude   - parameter accepts the users longitude
     * @param gender      - parameter accepts the gender requested by the user
     * @param isWorldWide - parameter accepts boolean whether results requires world wide coverage
     * @param isLocal     - parameter accepts boolean whether results requires only user location based coverage
     * @return a paginated list of filtered Dating Places
     */
    @RequestMapping(value = "/quickFilter/list", method = RequestMethod.GET)
    public List<DatingPlaceMobileDto> getPlacesByFilterList(
            @RequestParam(value = "radius", required = false) Double radius,
            @RequestParam(value = "latitude", required = false) Double latitude,
            @RequestParam(value = "longitude", required = false) Double longitude,
            @RequestParam(value = "gender", required = false) Gender gender,
            @RequestParam(value = "worldWide", required = false) Boolean isWorldWide,
            @RequestParam(value = "local", required = false) Boolean isLocal,
               @RequestParam(value = "featured", required = false) Boolean featured) {
        return getPlacesByFilter(1, 100, radius, latitude, longitude, gender, isWorldWide, isLocal, featured).getContent();
    }

    /**
     * this api returns a paginated list of place reviews for a Dating Place
     *
     * @param page - parameter accepts the page number of results requested
     * @param id   - parameter accepts the place id
     * @return a paginated list place reviews for a Specific place ID
     */
    @RequestMapping(value = "{id}/reviews", method = RequestMethod.GET)
    public ResponseEntity<Page<DatingPlaceReviewMobileDto>> getPlacesReviewsByPlaceId(
            @RequestParam(value = "page", defaultValue = "1") Integer page, @PathVariable("id") String id) {
        if (StringUtils.isNumeric(id)) {
            Page<DatingPlaceReviewMobileDto> reviews = datingPlaceService.getReviewsByDatingPlace(
                    new PageRequest(page - 1, 30), Integer.parseInt(id)).map(DatingPlaceReviewMobileDto::new);
            return new ResponseEntity<>(reviews, HttpStatus.OK);
        } else {
            try {
                DatingPlaceDto datingPlaceDto = datingPlaceService.findGoogleDatingPlaceById(id);
                Page<DatingPlaceReviewMobileDto> reviews = datingPlaceService.getReviewsByGoogleDatingPlace(
                        new PageRequest(page - 1, 30), datingPlaceDto).map(DatingPlaceReviewMobileDto::new);
                return new ResponseEntity<>(reviews, HttpStatus.OK);
            } catch (IOException e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (JSONException | NullPointerException ex) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }
    }
}
