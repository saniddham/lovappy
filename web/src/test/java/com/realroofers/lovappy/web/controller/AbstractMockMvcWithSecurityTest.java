package com.realroofers.lovappy.web.controller;

import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.junit4.SpringRunner;

import com.realroofers.lovappy.service.user.dto.UserAuthDto;
import com.realroofers.lovappy.web.config.ApplicationConfigTest;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringRunner.class)
@Import({ApplicationConfigTest.class})
public abstract class AbstractMockMvcWithSecurityTest extends AbstractMockMvcTest {
    protected static final Integer USER_ID = 1;

    @Before
    public final void setup() throws Exception {
        SecurityContext securityContext = new SecurityContextImpl();
        UserAuthDto user = new UserAuthDto(USER_ID);
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, null);
        securityContext.setAuthentication(authentication);
        PowerMockito.mockStatic(SecurityContextHolder.class);
        when(SecurityContextHolder.getContext()).thenReturn(securityContext);
        doSetup();
    }

    public void doSetup() throws Exception {

    }
}
