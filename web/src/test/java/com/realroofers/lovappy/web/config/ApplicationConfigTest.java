package com.realroofers.lovappy.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafProperties;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@Configuration
public class ApplicationConfigTest extends ApplicationConfig {

    @MockBean
    private AdminInterceptor adminInterceptor;

    @Autowired
    public ApplicationConfigTest(ThymeleafProperties thymeleafProperties) {
        super(thymeleafProperties);
    }

    /**
     * Should be able to resolve templates added in src/main/webapp since the directory is added in classpath during maven test
     * <pre>
     * {@code
     * <testResources>
     * <testResource>
     * <directory>src/main/webapp</directory>
     * </testResource>
     * </testResources>
     * </pre>
     *
     * @return
     */
    protected String getTemplatePrefix() {
        String prefix = thymeleafProperties.getPrefix();
        if (!prefix.startsWith("classpath:")) {
            prefix = "classpath:" + prefix;
        }
        return prefix;
    }
}
