package com.realroofers.lovappy.web.controller;

import java.lang.reflect.Field;
import java.util.Date;

import com.realroofers.lovappy.service.lovstamps.LovstampService;
import org.assertj.core.util.Lists;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.web.config.ApplicationConfigTest;
import com.realroofers.lovappy.web.support.DatePropertyEditorSupport;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@RunWith(SpringRunner.class)
@Import({ApplicationConfigTest.class})
public abstract class AbstractMockMvcTest {
    @Autowired
    protected ObjectMapper mapper;
    @Autowired
    protected MockMvc mockMvc;
    @MockBean
    private UserProfileService userProfileService;
    @MockBean
    private LovstampService lovstampService;

    protected MultiValueMap<String, String> toMultiValueMap(Object o) throws Exception {
        Class clazz = o.getClass();
        MultiValueMap<String, String> result = new LinkedMultiValueMap<>();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            Object val = field.get(o);
            String sval = null;
            if (val != null) {
                if (val instanceof Date) {
                    sval = DatePropertyEditorSupport.DATE_FORMAT.format((Date) val);
                } else {
                    sval = val.toString();
                }
            }
            result.put(field.getName(), Lists.newArrayList(sval));
        }
        return result;
    }
}
