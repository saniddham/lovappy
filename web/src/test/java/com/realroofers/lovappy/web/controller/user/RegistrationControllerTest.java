package com.realroofers.lovappy.web.controller.user;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Date;

import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.system.LanguageService;
import com.realroofers.lovappy.service.survey.UserSurveyService;
import com.realroofers.lovappy.service.validation.FormValidator;
import org.junit.Ignore;
import org.junit.Test;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;

import com.realroofers.lovappy.service.user.UserProfileAndPreferenceService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.dto.AddressDto;
import com.realroofers.lovappy.service.user.dto.AgeHeightLangAndPrefSizeAgeHeightLangDto;
import com.realroofers.lovappy.service.user.dto.GenderAndPrefGenderDto;
import com.realroofers.lovappy.service.user.model.UserPreference;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.Size;
import com.realroofers.lovappy.web.config.ApplicationConfig;
import com.realroofers.lovappy.web.config.SecurityConfig;
import com.realroofers.lovappy.web.controller.AbstractMockMvcWithSecurityTest;
import org.springframework.social.twitter.api.impl.TwitterTemplate;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@WebMvcTest(controllers = RegistrationController.class, secure = false, excludeFilters = {
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = {
        ApplicationConfig.class, SecurityConfig.class
    })
})
@PrepareForTest(SecurityContextHolder.class)
@Ignore
public class RegistrationControllerTest extends AbstractMockMvcWithSecurityTest {
    private final String rootUrl = "/registration";
    @MockBean
    private UserProfileAndPreferenceService service;

    @MockBean
    private UserService userService;

    @MockBean
    private FormValidator formValidator;


    @MockBean
    private MailService mailService;

    @MockBean
    private LanguageService languageService;

    @MockBean
    private NotificationService notificationService;

    @MockBean
    private TwitterTemplate twitterTemplate;

    @MockBean
    private UserSurveyService userSurveyService;
    @Test
    public void showGenderPage() throws Exception {
        GenderAndPrefGenderDto actual = new GenderAndPrefGenderDto(Gender.MALE, Gender.FEMALE);
        actual.setBirthDate(new Date());
        actual.setLocation(new AddressDto());
        when(service.getGenderAndPrefGender(USER_ID)).thenReturn(actual);
        mockMvc.perform(get(rootUrl + "/gender"))
            .andExpect(status().isOk())
            .andExpect(view().name("user/registration/gender"))
            .andExpect(model().attribute("info", actual));
    }

//    @Test
    public void postGenderPage() throws Exception {
        // Saving null genders
        GenderAndPrefGenderDto dto = new GenderAndPrefGenderDto();
        mockMvc.perform(post(rootUrl + "/gender")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED))
            .andExpect(status().isOk())
            .andExpect(view().name("user/registration/gender"))
            .andExpect(model().attribute("info", dto))
            .andExpect(model().errorCount(3))
            .andExpect(model().attributeHasErrors("info"))
            .andExpect(model().attributeHasFieldErrors("info", "gender", "prefGender"))
            .andExpect(model().attributeHasFieldErrorCode("info", "gender", "NotNull"))
            .andExpect(model().attributeHasFieldErrorCode("info", "prefGender", "NotNull"));

        // Saving valid parameters
        dto = new GenderAndPrefGenderDto(Gender.MALE, Gender.FEMALE);
        dto.setBirthDate(new Date());
        dto.setLocation(new AddressDto());
        dto.getLocation().setLastMileRadiusSearch(200);
        doNothing().when(service).saveOrUpdate(USER_ID, dto);
        mockMvc.perform(post(rootUrl + "/gender")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .params(toMultiValueMap(dto)))
            .andExpect(model().errorCount(1))
            .andExpect(status().is2xxSuccessful())
            .andExpect(view().name("redirect:/registration/ageheightsizelang"));
    }

    @Test
    public void showAgeHeightLangAndPrefSizeAgeHeightLangPage() throws Exception {
        UserProfile profile = new UserProfile();
        //profile.setBirthDate(new Date());
        UserPreference pref = new UserPreference();
        pref.setGender(Gender.FEMALE);
        pref.setBustSize(Size.SMALL);
        //pref.setButtSize(Size.DOESNTMATTER);
        //pref.setHeight(PrefHeight.DOESNTMATTER);
        AgeHeightLangAndPrefSizeAgeHeightLangDto actual = new AgeHeightLangAndPrefSizeAgeHeightLangDto(profile, pref);
        when(service.getAgeHeightLangAndPrefSizeAgeHeightLang(USER_ID)).thenReturn(actual);
        mockMvc.perform(get(rootUrl + "/ageheightsizelang"))
            .andExpect(status().isOk())
            .andExpect(view().name("user/registration/ageheightsizelang_female"))
            .andExpect(model().attribute("info", actual));
    }
}
