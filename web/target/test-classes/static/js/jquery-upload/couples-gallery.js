$(function() {

    var $uploadCouplePhotoDiv = $('#couplesphotoupload');

    var createOptions = function(url, $uploadCouplePhotoDiv) {
        return {
            url: url,
            dataType: 'json',
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            previewCrop: true,
            add: function(e, data) {
                // Replace image in preview container
               // var $previewFileDiv = $('.preview-file', $uploadContainer);
                if(data.files && data.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('img', $uploadCouplePhotoDiv).attr('src', e.target.result);
                    };
                    reader.readAsDataURL(data.files[0]);
                }
                // Hook to on click of of do-upload-button so that it'll submit the data on click.
                var $doUploadButton = $('.do-upload-button');
                $doUploadButton.off('click'); // Remove previous click event
                $doUploadButton.click(function() { // Register new click event
                    var $progressDiv = $('.progress', $uploadCouplePhotoDiv);
                    //$progressDiv.css('width', 0);
                    $progressDiv.css('visibility', 'visible');
                                        
                    // data.context = $('<p/>').text('Uploading...').replaceAll($(this)); // No loader yet
                   $zipCode = $('#zip-code').val();
                   $photoCaption = $('#photo-caption').val();
                   data.formData = {zipCode: $zipCode, photoCaption: $photoCaption};
                   //data.photoCaption = $('#photo-caption').val();
                   data.submit();
                });
            },
            progress: function(e, data) {
                /*var progress = parseInt(data.loaded / data.total * 100, 10);
                var $progressDiv = $('.progress', $uploadContainer);
                $progressDiv.css('width', progress + '%');*/
            },
            done: function(e, data) {
                /*var $progressDiv = $('.progress', $uploadContainer);
                console.log("Generated file id: " + data.result.id);
                $progressDiv.css('visibility', 'hidden');
                if(data.files && data.files[0]) {
                    $uploadedContainer.find('.upload_hidden_message').hide();
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('img', $uploadedContainer).attr('src', e.target.result);
                        $uploadedContainer.find("p").hide();
                    };
                    reader.readAsDataURL(data.files[0]);
                }*/
            },
            stop: function (e) {
                location.reload();
            }
        }
    };
    
    $('input[type="file"]', $uploadCouplePhotoDiv).fileupload(createOptions(baseUrl + 'api/v1/gallery/photo', $uploadCouplePhotoDiv));
});

   function callAccessProfilePicAPI(access) {
            $.ajax({
                url: baseUrl + "/api/v1/user/profile/photo/access",
                type: "POST",

                data:  JSON.stringify({"access":access}),
                contentType: "application/json; charset=utf-8",
                success: function () {
               return true;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                               if(jqXHR.status === 400)
                                   showValidationMessages(jqXHR.responseJSON);
                               else if(jqXHR.status === 401)
                                   window.location.href = baseUrl + "login";
                           }
            });
    }