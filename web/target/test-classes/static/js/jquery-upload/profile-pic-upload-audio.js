window.AudioContext = window.AudioContext || window.webkitAudioContext;

var audioInput;
var audioContext = new AudioContext();
var realAudioInput = null,
    inputPoint = null;
var recorder;
var file;
var recordedAudio;
var recordButtonText;
var buttonCircle;
var saveBtn;

var selectLangMsg = "Select a language first.";


$(document).ready(function () {

    initAudio();

    $('#language').prepend('<option disabled="disabled" selected="selected">Select Language</option>');

    $("#samples-list").find("a").each(function (index, element) {
        (this).onclick = function () {
            if ($("#language").val() === null) {
                showSelectLanguageMessage();
            }
        }
    });

    /**
     * Player class containing the state of our playlist and where we are in it.
     * Includes all methods for playing, skipping, updating the display, etc.
     * @param {Array} playlist Array of objects with playlist song details ({title, file, howl}).
     */
    var Player = function (playlist) {
        this.playlist = playlist;
        this.index = 0;

        $("#samples-list").find("a").each(function (index, element) {
            if (playlist.length > 0) {
                if (index <= (playlist.length - 1)) {
                    $(this).attr("id", playlist[index].id);
                } else {
                    $(this).removeAttr("id");
                    $(this).off('click');
                }
            }
        });

        // Setup the playlist display.
        playlist.forEach(function (song) {
            $('#' + song.id).click(
                //document.getElementById(song.title).onclick =
                function () {
                    if ($("#language").val() === null) {
                        alert(selectLangMsg);
                    } else {
                        player.skipTo(playlist.indexOf(song));
                        //$(this).find("img").attr("src", baseUrl + "/images/pause_btn_side.png");
                    }
                })
        });
    };

    Player.prototype = {
        /**
         * Play a song in the playlist.
         * @param  {Number} index Index of the song in the playlist (leave empty to play the first or current).
         */
        play: function (index) {
            var self = this;
            var sound;

            index = typeof index === 'number' ? index : self.index;
            var data = self.playlist[index];

            if (data.howl) {
                sound = data.howl;
            } else {
                sound = data.howl = new Howl({
                    src: [data.file],
                    html5: true, // Force to HTML5 so that the audio can stream in (best for large files).
                    onplay: function () {

                        // Start updating the progress of the track.
                        //requestAnimationFrame(self.step.bind(self));

                        stopAll(data.id);

                        //$("#" + data.title).find("img").attr("src", baseUrl + "/images/pause_btn_side.png");
                        //document.getElementById(data.title).src = baseUrl + "/images/pause_btn_side.png";
                    },
                    onload: function () {
                    },
                    onend: function () {
                        stopAll(null);
                    },
                    onpause: function () {
                        stopAll(null);
                    },
                    onstop: function () {
                        stopAll(null);
                    }
                });
            }

            // Begin playing the sound.
            sound.play();

            // Show the pause button.
            if (sound.state() === 'loaded') {
                stopAll(data.id);
                //$("#" + data.title).find("img").attr("src", baseUrl + "/images/pause_btn_side.png");
            } else {
                stopAll(null);
            }

            // Keep track of the index we are currently playing.
            self.index = index;
        },

        /**
         * Pause the currently playing track.
         */
        pause: function () {
            var self = this;

            // Get the Howl we want to manipulate.
            var sound = self.playlist[self.index].howl;

            // Puase the sound.
            sound.pause();

            stopAll(null);
        },

        /**
         * Stop the currently playing track.
         */
        stop: function () {
            var self = this;

            // Get the Howl we want to manipulate.
            var sound = self.playlist[self.index].howl;

            // Stop the sound.
            sound.stop();

            stopAll(null);
        },

        /**
         * Skip to a specific track based on its playlist index.
         * @param  {Number} index Index in the playlist.
         */
        skipTo: function (index) {
            var self = this;

            // Get the Howl we want to manipulate.
            var sound = self.playlist[self.index].howl;

            // Stop the current track.
            if (sound && sound.playing() && index === self.index) {
                self.stop();
            } else if (sound && sound.playing() && index !== self.index) {
                self.stop();
                self.play(index);
            } else {
                self.play(index);
            }
        }
    };


    var player;

    $("#language").change(function () {
        var lang = $(this).val();
        $.ajax({
            url: baseUrl + "lovdrop/samples",
            data: {language: lang},
            type: "GET",
            success: function (data) {
                // Setup our new audio player class and pass it the playlist.
                var playlistArray = [];
                for (var i = 0; i < data.length; i++) {
                    var title = data[i].audioFileCloud.url;
                    var file = data[i].audioFileCloud.url;
                    playlistArray.push({
                        id: data[i].id,
                        title: title,
                        file: file,
                        howl: null
                    });
                }
                player = new Player(playlistArray)
            },
            error: function () {
                alert("failed.");
            }
        });
    });


});

function stopAll(playID) {
    $("#samples-list").find("img").each(function () {
        $(this).attr("src", baseUrl + "/images/play_btn_side.png");

        if (playID !== null)
            $("#" + playID).find("img").attr("src", baseUrl + "/images/pause_btn_side.png");
    });
}


$(document).ready(function () {

    recordedAudio = document.querySelector('audio#recorded');

    recordButtonText = document.querySelector('#record-btn-text');
    buttonCircle = document.querySelector('#round-button-circle');
    saveBtn = $('#save-btn');

    // window.isSecureContext could be used for Chrome
    var isSecureOrigin = location.protocol === 'https:' ||
        location.hostname === 'localhost';
    if (!isSecureOrigin) {
        /*alert('getUserMedia() must be run from a secure origin: HTTPS or localhost.' +
         '\n\nChanging protocol to HTTPS');*/
        location.protocol = 'HTTPS';
    }

    recordedAudio.addEventListener('ended', function () {
        applyPlayStyle();
    });
});

function toggleRecording() {
    if (recordButtonText.dataset.action === 'Record') {
        if ($("#language").val() === null) {
            showSelectLanguageMessage();
        } else {
            startRecording();
            startTime(false);
        }
    } else if (recordButtonText.dataset.action === 'StopRecording') {
        finishRecording();
    } else if (recordButtonText.dataset.action === 'Play') {
        play();
        startTime(true);
        recordButtonText.textContent = 'Stop';
        recordButtonText.dataset.action = 'StopPlaying';
        buttonCircle.classList.add('round-button-circle-red');
        buttonCircle.classList.remove('round-button-circle-green');
    } else if (recordButtonText.dataset.action === 'StopPlaying') {
        recordedAudio.pause();
        applyPlayStyle();
    }
}

function applyPlayStyle() {
    stopTime();
    recordButtonText.textContent = 'Play';
    recordButtonText.dataset.action = 'Play';
    buttonCircle.classList.add('round-button-circle-green');
    buttonCircle.classList.remove('round-button-circle-red');
}

function finishRecording() {
    recordedSeconds = seconds;
    recordedMinutes = minutes;
    stopRecording();
    applyPlayStyle();
    //document.getElementById("timer").style.display = "none";
    $('#save-btn').prop('disabled', false);
}

function startRecording() {
    if (recorder == null) {
        $("#errMicModal").modal('show');
        return;
    }
    recorder.startRecording();
    console.log('Recording...');

    recordButtonText.textContent = 'Stop';
    recordButtonText.dataset.action = 'StopRecording';
}

function stopRecording() {
    recorder.finishRecording();
    console.log('Stopped recording.');
}

function play() {
    if (recordFile) recordedAudio.src = recordFile;

    recordedAudio.play();
}

function upload() {
    if ($("#language").val() === null) {
        showSelectLanguageMessage();
        return;
    }
   if(recordedSeconds < 30) {
            //$errorContainer.text('LovDrop recording is required.\nYou should record at least 10 seconds to continue.');
            //$errorContainer.show();
            $("#popup-message-title").text('Voice Recording is required');
            $("#popup-message-text").html('Voice Recording is required. You should record at least 30 seconds to continue.');
            $("#message-popup").modal('show');
   return;
           }
    saveBtn.prop('disabled', true);
    saveBtn.find('i').show();

    var fd = new FormData();
    fd.append('data', file);
    fd.append('seconds', recordedSeconds);
    fd.append('languageID', $("#selected-language-id").val());

    $.ajax({
        url: baseUrl + "/api/v1/lovappy_radio/upload",
        type: "POST",
        data: fd,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        success: function () {
            location.reload();
        },
        error: function () {
            saveBtn.find('i').hide();
            saveBtn.prop('disabled', false);
            alert("Upload failed.");
        }
    });
}

function uploadQuestionResponse() {
    if ($("#language").val() === null) {
        showSelectLanguageMessage();
        return;
    }

    saveBtn.prop('disabled', true);
    saveBtn.find('i').show();
    var questionId = $('#question-id').val();
    var currentDate = new Date();
    var fd = new FormData();
    fd.append('data', file);
    fd.append('seconds', recordedSeconds);
    fd.append('languageId', $("#selected-language-id").val());
    fd.append('today', currentDate.getTime());

    $.ajax({
        url: baseUrl + '/member/' + questionId + '/response/upload',
        type: "POST",
        data: fd,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        success: function () {
            saveBtn.find('i').hide();
            saveBtn.prop('disabled', true);
            location.reload();
        },
        error: function () {
            saveBtn.find('i').hide();
            saveBtn.prop('disabled', false);
            alert("Upload failed.");
        }
    });

}

function updateQuestionResponse() {
    saveBtn.prop('disabled', true);
    saveBtn.find('i').show();
    var responseId = $('#response-id').val();
    var fd = new FormData();
    fd.append('data', file);
    fd.append('seconds', recordedSeconds);

    $.ajax({
        url: baseUrl + '/member/response/' + responseId,
        type: "PUT",
        data: fd,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        success: function () {
            saveBtn.find('i').hide();
            saveBtn.prop('disabled', true);
            location.reload();
        },
        error: function () {
            saveBtn.find('i').hide();
            saveBtn.prop('disabled', false);
            toastr.error('Upload Failed');
        }
    });

}

function reset() {
    $.ajax({
        url: baseUrl + "lovdrop/delete_recording",
        data: {id: id},
        type: "POST",
        success: function () {
            location.reload();
        },
        error: function () {
            alert("failed.")
        }
    });
}

function skipVoice() {
    $.ajax({
        url: baseUrl + "api/v1/user/profile/voice/skip",
        data: {},
        type: "POST",
        success: function () {
            location.reload();
        },
        error: function () {
            $("#popup-message-title").text('Something went wrong');
            $("#popup-message-text").text('Please try again');
            $("#message-popup").modal('show');
        }
    });
}

function resetRecorder() {
    recordFile = null;
    recordButtonText.textContent = 'Record';
    recordButtonText.dataset.action = 'Record';
    buttonCircle.classList.remove('round-button-circle-green');
    buttonCircle.classList.add('round-button-circle-red');
    recordedSeconds = 0;
    seconds = 0;
    minutes = 0;
    document.getElementById("timer").innerHTML = formatTime(0, false) + formatTime(0, true);
    stopTime();
    saveBtn.show();
    $(".language").show();
}

function showSelectLanguageMessage() {
    $("#popup-message-title").text('Select Language');
    $("#popup-message-text").text('You should select a language for your Voice Recording.');
    $("#message-popup").modal('show');
}

// initialize your variables outside the function
var count = 0;
var clearTime;
var seconds = recordedSeconds != null ? recordedSeconds : 0,
    minutes = 0,
    hours = 0;
var clearState;
var secs = formatTime(seconds, true);
var mins = formatTime(minutes, false);
var gethours;

$(document).ready(function () {
    document.getElementById("timer").innerHTML = mins + secs;
});

function startWatch(reverse) {
    /* check if seconds is equal to 60 and add a +1 to minutes, and set seconds to 0 */
    if (seconds === 60 && !reverse) {
        seconds = 0;
        minutes = minutes + 1;
    }

    if (seconds < 0 && reverse) {
        seconds = 59;
        minutes = minutes - 1;
    }

    /* you use the javascript tenary operator to format how the minutes should look and add 0 to minutes if less than 10 */
    mins = formatTime(minutes, false);
    //mins = (minutes < 10) ? ('0' + minutes + ': ') : (minutes + ': ');

    /* check if minutes is equal to 60 and add a +1 to hours set minutes to 0 */
    if (minutes === 60 && !reverse) {
        minutes = 0;
        hours = hours + 1;
    }

    if (minutes < 0 && reverse) {
        minutes = 59;
        hours = hours - 1;
    }

    /* you use the javascript tenary operator to format how the hours should look and add 0 to hours if less than 10 */
    gethours = formatTime(hours, false);
    //gethours = (hours < 10) ? ('0' + hours + ': ') : (hours + ': ');
    secs = formatTime(seconds, true);
    //secs = (seconds < 10) ? ('0' + seconds) : (seconds);

    // display the stopwatch
    var x = document.getElementById("timer");
    x.innerHTML = /*gethours +*/ mins + secs;

    /* call the seconds counter after displaying the stop watch and increment seconds by +1 to keep it counting */
    if (reverse) {
        seconds--;
    } else {
        seconds++;
    }

    if (reverse && seconds < 0) {
        seconds = 0;
        stopTime();
        return;
    }

    if (!reverse && seconds > 300) {
        finishRecording();
        return;
    }

    /* call the setTimeout( ) to keep the stop watch alive ! */
    clearTime = setTimeout("startWatch(" + reverse + ")", 1000);
}

//create a function to start the stop watch
function startTime(reverse) {

    /* check if seconds, minutes, and hours are equal to zero and start the stop watch */
    //if (seconds === 0 && minutes === 0 && hours === 0) {

    /* hide the start button if the stop watch is running */
    //this.style.display = "none";

    /* call the startWatch( ) function to execute the stop watch whenever the startTime( ) is triggered */
    startWatch(reverse);
    //}
} // startwatch.js end

//create a function to stop the time
function stopTime() {

    /* check if seconds, minutes and hours are not equal to 0 */
    //if (seconds !== 0 || minutes !== 0 || hours !== 0) {

    // reset the stop watch
    seconds = recordedSeconds;
    minutes = recordedMinutes;
    //hours = 0;
    secs = (seconds < 10) ? ('0' + seconds) : (seconds);
    mins = (minutes < 10) ? ('0' + minutes + ': ') : (minutes + ': ');
    //gethours = '0' + hours + ': ';

    /* display the stopwatch after it's been stopped */
    var x = document.getElementById("timer");
    var stopTime = /*gethours +*/ mins + secs;
    x.innerHTML = stopTime;

    /* clear the stop watch using the setTimeout( )
     return value 'clearTime' as ID */
    clearTimeout(clearTime);
    //}
} // stopwatch.js end

function formatTime(time, isSeconds) {
    if (isSeconds)
        return (time < 10) ? ('0' + time) : (time);
    else
        return (time < 10) ? ('0' + time + ': ') : (time + ': ');
}


function gotStream(stream) {
    inputPoint = audioContext.createGain();

    // Create an AudioNode from the stream.
    realAudioInput = audioContext.createMediaStreamSource(stream);
    audioInput = realAudioInput;
    audioInput.connect(inputPoint);

    recorder = new WebAudioRecorder(inputPoint, {
        workerDir: baseUrl + "js/WebAudioRecorder/",     // must end with slash
        encoding: "mp3",
        options: {
            timeLimit: 30,
            mp3: {bitRate: 64}
        }
    });

    recorder.onComplete = function (rec, blob) {
        applyPlayStyle();

        recordedAudio.src = window.URL.createObjectURL(blob);

        var filename = (new Date).toISOString().replace(/:|\./g, '-') + '.mp3';
        file = new File([blob], filename);

        console.log('Recording Completed.');
    };

    recorder.onError = function (recorder, message) {
        console.log(message);
    };
}

function initAudio() {
    if (!navigator.getUserMedia)
        navigator.getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
    if (!navigator.cancelAnimationFrame)
        navigator.cancelAnimationFrame = navigator.webkitCancelAnimationFrame || navigator.mozCancelAnimationFrame;
    if (!navigator.requestAnimationFrame)
        navigator.requestAnimationFrame = navigator.webkitRequestAnimationFrame || navigator.mozRequestAnimationFrame;

    navigator.getUserMedia(
        {
            "audio": {
                "mandatory": {
                    "googEchoCancellation": "false",
                    "googAutoGainControl": "false",
                    "googNoiseSuppression": "false",
                    "googHighpassFilter": "false"
                },
                "optional": []
            },
        }, gotStream, function (e) {
            //alert('Error getting audio');
            console.log(e);
        });
}