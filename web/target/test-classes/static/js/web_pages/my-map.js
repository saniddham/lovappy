/**
 * Created by Eias Altawil on 7/1/2017.
 */
$(document).ready(function () {

    var elementA = document.createElement('script');
    elementA.type = 'text/javascript';
    elementA.async = true;
    elementA.defer = true;
    elementA.src = google_url;
    var elementScript = document.getElementsByTagName('script')[0];
    elementScript.parentNode.insertBefore(elementA, elementScript);

    //region Filter options selectors
    $('.filter-checkbox input[type=checkbox]').on("change", function () {
        if ($(this).is(':checked')) {
            $('.filter-options').append(
                '<span class="option-slice" data-text="'+$(this).data('text')+'">'+ $(this).data('text') +' <span class="filter-close">x</span></span>'
            );
        }
        else {
            $(".option-slice[data-text='"+$(this).data('text')+"']").remove();
        }
    });

    $('.filter-area select').on("change", function () {
        $(".option-slice[data-text='"+$(this).data('text')+"']").remove();
        if ($(this).val() != "") {
            var value = $(this).data('text') + ': ' + $(this).children('option:selected').text();
            $('.filter-options').append(
                '<span class="option-slice" data-text="'+$(this).data('text')+'">'+ value +'</span>'
            );
        }
    });

    $('.filter-options').on("click", ".option-slice",function () {
        $(".filter-checkbox input[data-text='"+$(this).data('text')+"']").click();
        $(".filter-area select[data-text='"+$(this).data('text')+"']").val("");
        $(this).remove();
    });
    //endregion

});