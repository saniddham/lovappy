/**
 * Created by hasan on 19/7/2017.
 */
var adType = "ad";
var adTypeParam;
var map;
var marker;
var selectedCountry, selectedState, selectedStateShort, selectedCity, selectedRadius;
var addressesJson = [];

var recordFile;
var mapItemsIndex = 0;
var geocoder;

var circles = [];
var addedAreas = new Array();
var selectedPoint;

var sound;
var audioBtn;
var playHtml = "<i class=\"fa fa-play\" aria-hidden=\"true\"></i>";
var stopHtml = "<i class=\"fa fa-stop\" aria-hidden=\"true\"></i>";
var loadingHtml = "<span class=\"ui button loading\" style='background: transparent;'></span>";
var uploadAudioHtml = "<img src=\"" + baseUrl + "images/advertise/audio-upload.png\"\">";

var adMediaType = "TEXT"; //text, audio, video
var selectedTxtColor = "white",
    selectedTxtBgColorID=null, selectedTxtBgImgID=null;

var selectedTxt = "";
var selectedTxtAlign = "CENTER";
var submitBtn;

var notifyOptions = {
    globalPosition: 'bottom right',
    className: "error",
    autoHide: true
};

$(document).ready(function() {

    $("#tabs").show();
    $("#tabs").tabs({
        activate: function(event, ui) {
            var active = $('#tabs').tabs('option', 'active');
            $("#tabid").html('the tab id is ' + $("#tabs ul>li a").eq(active).attr("href"));
            $(".ads-middle-box").css("background-image", "none");
            $(".box1").find("p").css("background-image", "none");
        }
    });

    $('input').on('keydown', function(e) {
        if (e.keyCode == 13 || e.which == 13) {
            e.preventDefault();
        }
    });
    $('input').on('keypress', function(e) {
        if (e.keyCode == 13 || e.which == 13) {
            e.preventDefault();
        }
    });

    $(".textAlign").on("click", function(){

    selectedTxtAlign = $(this).attr("data-align");
    if(selectedTxtAlign === "CENTER"){
    $(".ads-middle-box").find("p").css("vertical-align", "middle");
     $(".box1").find("p").css("vertical-align", "middle");
     }
    else   if(selectedTxtAlign === "BOTTOM"){
     $(".ads-middle-box").find("p").css("vertical-align", "bottom");
       $(".box1").find("p").css("vertical-align", "bottom");
      } else{
          $(".ads-middle-box").find("p").css("vertical-align", "top");
            $(".box1").find("p").css("vertical-align", "top");
          }
    });

    var elementA = document.createElement('script');
    elementA.type = 'text/javascript';
    elementA.async = true;
    elementA.defer = true;
    elementA.src = google_url;
    var elementScript = document.getElementsByTagName('script')[0];
    elementScript.parentNode.insertBefore(elementA, elementScript);

    $('.bg-color-first span').click(function() {
        $('.bg-color-first span').removeClass('active');
        $(this).addClass('active');
        if ($('.bg-color-first span:first-child').hasClass("active")) {
            $('.bg-color-list').css('display', 'block');
            $('.bg-image-list').css('display', 'none');
        } else {
            $('.bg-color-list').css('display', 'none');
            $('.bg-image-list').css('display', 'block');
        }
    });
    $('.bg-color-second span').click(function() {
        $('.bg-color-second span').removeClass('active');
        $(this).addClass('active');
        if ($('.bg-color-second span:first-child').hasClass("active")) {
            $('.bg-color-list2').addClass('important');
            $('.bg-image-list2').removeClass('important');
        } else {
            $('.bg-color-list2').removeClass('important');
            $('.bg-image-list2').addClass('important');
        }
    });
    var text_max = 120;
    $('#textarea_feedback').html(text_max + ' characters remaining');

    $('#advertise-text').keyup(function() {
        var text_length = $('#advertise-text').val().length;
        var text_remaining = text_max - text_length;

        $('#textarea_feedback').html(text_remaining + ' characters remaining');
    });

    $("#audio-preview-featured-stamp .ads-middle-box.tabs1-first").hide();
      $("#video-preview-featured-stamp .ads-middle-box.tabs1-first").hide();
    $('body').on('click', '#audio-featured-stamp', function() {
        if (this.checked) {
            $("#audio-preview-stamp").hide();

//            $("#featured-img").css("background-image", "url(" + $('#large-image-url').val() + ")");
            $("#audio-preview-featured-stamp .ads-middle-box.tabs1-first").show("table");
            $("#audio-preview-featured-stamp .ads-middle-box.tabs1-first").css("background-size", "100% 100%;");


           $("#audio-preview-featured-stamp .ads-middle-box.tabs1-first").css("background-repeat", "no-repeat;");
            $("#audio-preview-featured-stamp .ads-middle-box.tabs1-first").css("background-position", "top");
        } else {
            $("#audio-preview-featured-stamp .ads-middle-box.tabs1-first").hide();
            $("#audio-preview-stamp").show();
            $("#audio-preview-stamp").css("background-size", "100% 60%;");

            $("#audio-preview-stamp").css("background-repeat", "no-repeat;");
            $("#audio-preview-stamp").css("background-position", "top");

        }
    });

    $('body').on('click', '#video-featured-stamp', function() {
        if (this.checked) {
            $("#video-preview-stamp").hide();
            $("#video-preview-featured-stamp .ads-middle-box.tabs1-first").show("table");
            $("#featured-vedio-img").css("background-image", "url(" + $('#large-image-url').val() + ")");
        } else {
            $("#video-preview-featured-stamp .ads-middle-box.tabs1-first").hide();
            $("#video-preview-stamp").show();

        }
    });


    $("#video-recorder-btn").on('click', function() {
        $("#video-recorder-modal").modal("show");
    });

    submitBtn = $("#submit-advertise, #submit-audio-advertise");

    var uploadImageContainer = $("#image-upload-container");
    var uploadImageBtn = $("#upload-image-btn");

    var uploadAudioBtn = $("#upload-audio-btn");
    var uploadRecordingBtn = $("#upload-recording-btn");


    constructSound();
    audioBtn = $("#audio-btn").find("span");

    $('body').on('click', '#audio-btn', function() {
        if (sound.playing()) {
            sound.stop();
        } else {
            sound.play();
        }
    });

    $('#target-area, #audio-target-area, #video-target-area').find('span').show();
    $('#target-area, #audio-target-area, #video-target-area').find('i').hide();
    $('#audience-area, #audio-audience-area, #video-audience-area').find('span').show();
    $('#audience-area, #audio-audience-area, #video-audience-area').find('i').hide();

    //UPLOAD CUSTOM BACKGROUND

    $('body').on('change', '#bg-image-file-1, #bg-image-file-2 , #upload-imgg, #upload-img-audio, #upload-img-video', function() {
        var $this = $(this);
        $this.siblings("i").hide();
        $this.siblings(".loading").show();

        var formData = new FormData();
        formData.append('cover_image', this.files[0]);

        $.ajax({
            url: baseUrl + "api/v1/ads/cover_image",
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function(data) {
                $this.siblings("i").show();
                $this.siblings(".loading").hide();

                    selectedTxtBgImgID = data.id;
                    //selectedTxtBgColorID = null;
                    $(".tabs1-first").css("background-image", "url(" + data.url + ")");
                    $(".ads-middle-box").css("background-image", "url(" + data.url + ")");


            },
            error: function(jqXHR, textStatus, errorThrown) {
                $this.siblings("i").show();
                $this.siblings(".loading").hide();

                if (jqXHR.status === 400)
                    showNotifyErrorMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });
    });

    //Remove CUSTOM BACKGROUND
    $('body').on('click', '#removeCoverImage, #audioRemoveCoverImage', function() {
        $(".ads-middle-box").css("background-image", "none");
        $(".box1").find("p").css("background-image", "none");
    });

    $("#videoRemoveCoverImage").on('click', function() {
        $(".ads-middle-box").css("background-image", "url(" + baseUrl + "/images/advertise/Ad-Video-Icon.png)");
        $('#large-image-url').val(baseUrl + "/images/advertise/Ad-Video-Icon-lg.png");
        $(".box1").find("p").css("background-image", "none");
    });
    //UPLOAD COVER IMAGE
    $("#cover-image-file").on('change', function() {
        uploadImageContainer.find(".loading").show();
        uploadImageContainer.find(".upload-area").hide();
        uploadImageBtn.addClass("disabled");


        var formData = new FormData();
        formData.append('cover_image', this.files[0]);

        $.ajax({
            url: baseUrl + "api/v1/ads/cover_image",
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function(data) {
                uploadImageContainer.find(".loading").hide();
                uploadImageContainer.find(".upload-area").html("<img src=\"" + data.url + "\"/>");
                uploadImageContainer.find(".upload-area").show();
                $("#cover-image-input").val(data.id);
                uploadImageBtn.removeClass("disabled");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                uploadImageContainer.find(".loading").hide();
                uploadImageContainer.find(".upload-area").show();
                uploadImageBtn.removeClass("disabled");

                if (jqXHR.status === 400)
                    showNotifyErrorMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });
    });

    //UPLOAD AUDIO
    $("#ad-audio").on('change', function() {
        var audioFileInput = document.getElementById("ad-audio");
        if ('files' in audioFileInput && audioFileInput.files.length === 1) {
            audioBtn.html(loadingHtml);
            uploadAudioBtn.addClass("disabled");

            var formData = new FormData();
            formData.append('audio', this.files[0]);

            var objectUrl = window.URL.createObjectURL(this.files[0]);
            $("#uploaded").prop("src", objectUrl);

            $.ajax({
                url: baseUrl + "api/v1/ads/audio",
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function(data) {
                    $("#audio-input").val(data.id);
                    constructSound(data.url);
                    var duration = document.getElementById("uploaded").duration;
                    //$("#length-input").val(parseInt(duration, 10));
                    $("#length-input").val(data.duration);
                    window.URL.revokeObjectURL(objectUrl);
                    uploadAudioBtn.attr("audio-uploaded", true);
                    $("#audio-play-icon").show();
                    //  uploadAudioBtn.removeClass("disabled");
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    audioBtn.html(uploadAudioHtml);
                    uploadAudioBtn.removeClass("disabled");

                    if (jqXHR.status === 400)
                        showNotifyErrorMessages(jqXHR.responseJSON);
                    else if (jqXHR.status === 401)
                        window.location.href = baseUrl + "login";
                }
            });
        }
    });

    uploadRecordingBtn.attr("recording-uploaded", false);
    uploadAudioBtn.attr("audio-uploaded", false);
    //UPLOAD RECORDING
    uploadRecordingBtn.on('click', function() {
        uploadRecordingBtn.addClass("disabled");
        uploadRecordingBtn.addClass("loading");

        var formData = new FormData();
        formData.append('audio', file);

        $.ajax({
            url: baseUrl + "api/v1/ads/audio",
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function(data) {
                if (containsValidationErrors(data)) {} else {
                    $("#audio-input").val(data.id);
                    //$("#length-input").val(recordedSeconds);
                    $("#length-input").val(data.duration);
                    $.notify(
                        notifAudioReccorderUploaded, {
                            globalPosition: 'bottom right',
                            className: "success",
                            autoHide: true
                        }
                    );
                }
                //uploadRecordingBtn.removeClass("disabled");
                uploadRecordingBtn.removeClass("loading");
                uploadRecordingBtn.attr("recording-uploaded", true);
                $("#audio-play-icon").show();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                uploadRecordingBtn.removeClass("disabled");
                uploadRecordingBtn.removeClass("loading");

                if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });
    });


    // uploadAudioBtn.attr("audio-uploaded", false);


    $("#video-upload").attr("video-uploaded", false);
    //UPLOAD VIDEO
    $("#video-upload").on('change', function() {
        var videoFileInput = document.getElementById("video-upload");
        if ('files' in videoFileInput && videoFileInput.files.length === 1) {
            $("#video-upload-container").find(".loading").show();
            $("#video-upload-container").find(".upload-area").hide();
            $("#upload-video-btn").addClass("disabled");

            var formData = new FormData();
            formData.append('video', this.files[0]);

            $.ajax({
                url: baseUrl + "api/v1/ads/video",
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function(data) {
                    $("#video-upload-container").find(".loading").hide();
                    $("#video-upload-container").find(".upload-area").show();
                    $("#upload-video-btn").removeClass("disabled");
                    $("#audio-input").val(data.id);
                    vidPreview.src=data.url;
                    //videojs(document.querySelector('#my-video')).src(data.url);
                    $("#video-play-icon").show();
                    $("#video-upload").attr("video-uploaded", true);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#video-upload-container").find(".loading").hide();
                    $("#video-upload-container").find(".upload-area").show();
                    $("#upload-video-btn").removeClass("disabled");

                    if (jqXHR.status === 400)
                        showNotifyErrorMessages(jqXHR.responseJSON);
                    else if (jqXHR.status === 401)
                        window.location.href = baseUrl + "login";
                }
            });
        }
    });


    //SUBMIT Text AD
    $('#submit-advertise').on('click', function(e) {
        e.preventDefault();
        submitBtn = $(this);
        var isAgreed = $("#acknowledge-check")[0].checked;
        var isAudienceSelected = $("#audience-area").find("i").is(":hidden");
         var emailProvided = $("#email-text").val();
        if (!isAgreed)
            $.notify(notifTermsRequired, notifyOptions);

        if (!selectedRadius)
            $.notify(notifAreaRequired, notifyOptions);

        if (isAudienceSelected)
            $.notify(notifAudienceRequired, notifyOptions);

        if(emailProvided === ""){
          $.notify(notifEmailRequired, notifyOptions);
          emailProvided = null;
} else if(!validateEmail(emailProvided)){
          $.notify(notifEmailValid, notifyOptions);
            emailProvided = null;
          }
        if (isAgreed && selectedRadius && !isAudienceSelected && emailProvided) {
            calculatePrice(getCalcPriceData());
            $('#square-payment-form').modal('show');
        }
    });

    //SUBMIT Audio AD
    $('#submit-audio-advertise').on('click', function(e) {
        e.preventDefault();
        submitBtn = $(this);
        var isAgreed = $("#acknowledge-audio-check")[0].checked;
        var isAudienceSelected = $("#audio-audience-area").find("i").is(":hidden");
        var emailProvided = $("#email-audio").val();
        if (!isAgreed)
            $.notify(notifTermsRequired, notifyOptions);

        if (!selectedRadius)
            $.notify(notifAreaRequired, notifyOptions);

        if (isAudienceSelected)
            $.notify(notifAudienceRequired, notifyOptions);

         if(emailProvided === ""){
               $.notify(notifEmailRequired, notifyOptions);
               emailProvided = null;
     } else if(!validateEmail(emailProvided)){
               $.notify(notifEmailValid, notifyOptions);
                 emailProvided = null;
               }
        if (isAgreed && selectedRadius && !isAudienceSelected && emailProvided) {
            calculatePrice(getCalcPriceData());
            $('#square-payment-form').modal('show');
        }
    });

    //SUBMIT Video AD
    $('#submit-video-advertise').on('click', function(e) {
        e.preventDefault();
        submitBtn = $(this);

        var isAgreed = $("#acknowledge-video-check")[0].checked;
        var isAudienceSelected = $("#video-audience-area").find("i").is(":hidden");
        var emailProvided = $("#email-vd").val();
        if (!isAgreed)
            $.notify(notifTermsRequired, notifyOptions);

        if (!selectedRadius)
            $.notify(notifAreaRequired, notifyOptions);

        if (isAudienceSelected)
            $.notify(notifAudienceRequired, notifyOptions);

          if(emailProvided === ""){
                $.notify(notifEmailRequired, notifyOptions);
                emailProvided = null;
      } else if(!validateEmail(emailProvided)){
                $.notify(notifEmailValid, notifyOptions);
                  emailProvided = null;
                }
        if (isAgreed && selectedRadius && !isAudienceSelected && emailProvided) {
        if(!isNaN(vidPreview.duration)){
            $("#length-input").val(parseInt(vidPreview.duration, 10));
           } else {
            $("#length-input").val(null);
            }
            calculatePrice(getCalcPriceData());
            $('#square-payment-form').modal('show');
        }
    });

    $("#message-popup").find("div.ok").on("click", function() {
        window.location.href = baseUrl;
    });

    //ADD TARGET AREA
    $("#btn-add-area").on("click", function(e) {
        e.preventDefault();
        var radius = $("#mileRadius").val();
        selectedRadius = radius;

        var obj = {
            r: radius,
            s: selectedPoint
        };
        if (!isAreaSelected(obj)) {
            addedAreas.push(obj);
            var newCircle = new google.maps.Circle({
                strokeColor: '#9c0000',
                strokeOpacity: 0.8,
                strokeWeight: 1,
                fillColor: '#AA0000',
                fillOpacity: 0.35,
                map: map,
                center: selectedPoint,
                radius: 0
            });
            circles.push(newCircle);
            $('#areas-table tr:last').after(
                '<tr>' +
                '   <td>' + selectedStateShort + ', ' + selectedCity +
                '       <input type="hidden" name="targetAreas[' + mapItemsIndex + '].address.latitude" value="' + selectedPoint.lat + '" />' +
                '       <input type="hidden" name="targetAreas[' + mapItemsIndex + '].address.longitude" value="' + selectedPoint.lng + '" />' +
                '       <input type="hidden" name="targetAreas[' + mapItemsIndex + '].address.country" value="' + selectedCountry + '" />' +
                '       <input type="hidden" name="targetAreas[' + mapItemsIndex + '].address.state" value="' + selectedState + '" />' +
                '       <input type="hidden" name="targetAreas[' + mapItemsIndex + '].address.stateShort" value="' + selectedStateShort + '" />' +
                '       <input type="hidden" name="targetAreas[' + mapItemsIndex + '].address.city" value="' + selectedCity + '" />' +
                '       <input type="hidden" name="targetAreas[' + mapItemsIndex + '].address.lastMileRadiusSearch" value="' + selectedRadius + '" />' +
                '   </td>' +
                '   <td>' + radius + '<input type="hidden" name="targetAreas[' + mapItemsIndex + '].mileRadius" value="' + radius + '"/></td>' +
                '   <td>' + '<a href="javascript:void(0)" onclick="removeTargetArea(this)" data-id="' + mapItemsIndex + '">' +
                '               <i class="fa fa-times" aria-hidden="true"></i>' +
                '               </a>' +
                '   </td>' +
                '</tr>');

            addressesJson.push({
                "id": mapItemsIndex,
                "latitude": selectedPoint.lat,
                "longitude": selectedPoint.lng,
                "country": selectedCountry,
                "state": selectedState,
                "stateShort": selectedStateShort,
                "city": selectedCity,
                "lastMileRadiusSearch": selectedRadius,
                "mileRadius": radius
            });

            mapItemsIndex++;
        }
    });


    $('#go-location').on("click", function() {
        var address = $('#zip-code').val();
        var radius = $('#mileRadius').val();
        changeLocation(address, radius);
    });

    $('#zip-code').keyup(function(e) {
        if (e.which === 13) {
            var address = $('#zip-code').val();
            var radius = $('#mileRadius').val();
            changeLocation(address, radius);
        }
    });

    // select the ad type
    $('.ad-sample-area  .ad-sample').on("click", function() {
        adType = $(this).data('ad');
        if (!$(this).hasClass('active-ad')) {
            $('.ad-sample-area .ad-sample').addClass('inactive-ad');
            $('.ad-sample-area .ad-sample').removeClass('active-ad');
            $(this).removeClass('inactive-ad');
            $(this).addClass('active-ad');
        }

        setAdInfo(adType);
    });

    //region Target Area & Users Preferences popups
    $('#select-ad-areas, #select-audio-ad-areas').on('click', function(e) {
        e.preventDefault();
        $('#target-area-map').modal('show');
        google.maps.event.trigger(map, "resize");
        map.setCenter(marker.position);
    });

    $('body').on('click', '#save-target-area', function(e) {
        if (!selectedRadius) {
            $.notify(notifAreaRequired, notifyOptions);
        } else {
            $('#target-area, #audio-target-area, #video-target-area').find('span').hide();
            $('#target-area, #audio-target-area, #video-target-area').find('i').show();
            $('#target-area-map').modal('hide');
        }
    });

    $('body').on('click', '#audience-ad-pref, #audience-audio-ad-pref', function(e) {
        e.preventDefault();
        $('#audience-preferences').modal('show');
    });
    $('#save-audience-pref').on("click", function(e) {
        e.preventDefault();
        //            $("#ad-audience-preferences-form").validate();

        var isvalid = true;
        if (!$('input[name=gender]:checked').val()) {
            isvalid = false;
            $.notify(notifGenderRequired, notifyOptions);
        }
        if (!$('input[name=ageRange]:checked').val()) {
            isvalid = false;
            $.notify(notifAgeGroupRequired, notifyOptions);
        }

        if (!$("input[name='preferences.catORdog']:checked").val()) {
            isvalid = false;
            $.notify(notifPetsRequired, notifyOptions);
        }

        if (!$("input[name='preferences.drinker']:checked").val() && !$("input[name='preferences.smoker']:checked").val() &&
            !$("input[name='preferences.user']:checked").val() && !$("input[name='preferences.no-habit']:checked").val()) {
            isvalid = false;
            $.notify(notifHabitsRequired, notifyOptions);
        }
        if (!$("input[name='preferences.ruralORurbanORsuburban']:checked").val()) {
            isvalid = false;
            $.notify(notifNeighborhoodRequired, notifyOptions);
        }

        if (!$("input[name='preferences.outdoorsyORindoorsy']:checked").val()) {
            isvalid = false;
            $.notify(notifHangingRequired, notifyOptions);
        }
        if (!$("input[name='preferences.frugalORbigspender']:checked").val()) {
            isvalid = false;
            $.notify(notifMoneySpendingRequired, notifyOptions);
        }

        if (!$("input[name='preferences.morningpersonORnightowl']:checked").val()) {
            isvalid = false;
            $.notify(notifWakingTimeRequired, notifyOptions);
        }

        if (!$("input[name='preferences.extrovertORintrovert']:checked").val()) {
            isvalid = false;
            $.notify(notifPersonalityRequired, notifyOptions);
        }
        if (isvalid) {
            $('#audience-area, #audio-audience-area, #video-audience-area').find('span').hide();
            $('#audience-area, #audio-audience-area, #video-audience-area').find('i').show();
            $('#audience-preferences').modal('hide');


        }
    });

    //endregion

    $(' #upload-image-btn, #upload-custom-bg-1, #upload-custom-bg-2').on("click", function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        $('#upload-modal').modal('show');
        openTab('tab1');
    });

    $('#upload-audio-btn, #video-upload-button').on("click", function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        $(this).siblings('input').click();
    });


    // change audio method
    $('#audio-method').on("change", function() {
        if ($(this).val() === 'RECORD') {
            $('#record-audio').show();
            $('#upload-audio').hide();
        } else {
            $('#record-audio').hide();
            $('#upload-audio').show();
        }
    });

    $("#ad-type-audio-btn").on("click", function(e) {
        e.stopPropagation();
        e.preventDefault();
        $('#record-audio').hide();
        $('#upload-audio').show();
        $("#ad-audio-type").val("UPLOAD");
    });
    $("#ad-type-record-btn").on("click", function(e) {
        e.stopPropagation();
        e.preventDefault();
        $('#record-audio').show();
        $('#upload-audio').hide();
        $("#ad-audio-type").val("RECORD");
    });

    //open terms popup
    $('.terms-link').on("click", function() {

        switch (adType) {
            case 'ad':
                $('#ad-terms .header').text(adType + ' terms');
                $('#ad-terms').modal('show');
                break;

            case 'comedy':
                $('#comedy-terms .header').text(adType + ' terms');
                $('#comedy-terms').modal('show');
                break;

            case 'news':
                $('#news-terms .header').text(adType + ' terms');
                $('#news-terms').modal('show');
                break;

            case 'podcast':
                $('#podcast-terms .header').text(adType + ' terms');
                $('#podcast-terms').modal('show');
                break;

            case 'music':
                $('#music-terms .header').text(adType + ' terms');
                $('#music-terms').modal('show');
                break;

            case 'audiobook':
                $('#audiobook-terms .header').text(adType + ' terms');
                $('#audiobook-terms').modal('show');
                break;
        }

    });

    $(".ad-sample[data-ad='ad']").trigger("click");

    $('#mileRadius').numeric();
    $("#mileRadius").on("change", function(e) {
        var max = parseInt($(this).attr('max'));
        var min = parseInt($(this).attr('min'));
        if ($(this).val() > max) {
            $(this).val(max);
        } else if ($(this).val() < min) {
            $(this).val(min);
        }
    });
    $('#mileRadius').focusout(function() {
        changeRadius($(this).val());
    });

    $("#ad-type-audio-btn").click();

    $("#textarea-text").bind('input propertychange', function() {
        var text = $(this).val();
        $(".ads-middle-box").find("p").text(text);
        $("#advertise-text").val(text);
    });

    $("#advertise-text, #advertise-text-audio").bind('input propertychange', function() {
        var text = $(this).val();
        $(".ads-middle-box").find("p").text(text);
    });

//change text color
    $('input[type=radio][name=textColor]').on("change", function() {
        $(".ads-middle-box").find("p").css("color", this.value);
        $(".box1").find("p").css("color", this.value);
        selectedTxtColor = this.value;
    });

    $('input[type=radio][name=audioTextColor]').on("change", function() {
        $(".ads-middle-box").find("p").css("color", this.value);
        $(".box1").find("p").css("color", this.value);
        selectedTxtColor = this.value;
    });

    //change background color
    $('input[type=radio][name=backgroundColor1]').on("change", function() {
        $(".ads-middle-box").css("background-color", this.value);
        //$(".ads-middle-box").css("background-image", "none");
        //$(".box1").find("p").css("background-image", "none");
        $(".box1").find("p").css("background-color", this.value);

        selectedTxtBgColorID = $(this).data("id");
        //selectedTxtBgImgID = null;
    });


    $(".advertise-list").find("a").on("click", function() {
        adMediaType = $(this).data("type");
    });

    $(".images-list1").find("li").on("click", function() {
        selectedTxtBgImgID = $(this).data("id");
       $(".ads-middle-box").css("background-color", 'none');
        var imgUrl = $(this).find("img").attr("src");

        var fImgUrlId = $(this).find("div").attr("id");
        $('#large-image-id').val(fImgUrlId);
        $('#cover-image-input-large').val(fImgUrlId);

        var fImgUrl = $(this).find("input").attr("value");
        $('#large-image-url').val(fImgUrl);
        $("#large-img-url", "#featured-img").css("background-image", "url(" + fImgUrl + ")");
      $("#audio-preview-featured-stamp .ads-middle-box.tabs1-first").css("background-image", "url(" + fImgUrl + ")");

         $("#checkboxFourInput" + selectedTxtBgColorID).attr('checked', false);
        $(".ads-middle-box").css("background-image", "url(" + imgUrl + ")");
    });

    //Select first color and set background
    var firstColor = $('input[type=radio][name=backgroundColor1]:last');
    firstColor.attr('checked', true);
    $(".ads-middle-box").css("background-color", firstColor.val());
    selectedTxtBgColorID = firstColor.data("id");



    $('.text-step1 .next1 .nextto1').on("click", function(e) {
        e.preventDefault();
        $(".advertise-list").hide();
        $('.text-step1').hide();

        $('.text-step2').show();
        $('.text-step3').hide();
    });
    $('.text-step2 .next2 .nextto3').on("click", function(e) {
        e.preventDefault();
        if (selectedTxtBgImgID === null && $("#advertise-text").val().trim() == '') {
            $.notify(notifTextArea, notifyOptions);
            return;
        }

        if (!checkAndNotifyValidUrl($(".adUrl").val().trim(), $(".adUrl"))) {
            return;
        }

        $('.text-step1').hide();
        $('.text-step2').hide();

          $(".ads-middle-box").find("p").text($("#advertise-text").val());
        $('.text-step3').show();

    });
    $('.text-step2 .next2 .back2').on("click", function(e) {
        e.preventDefault();

        $(".advertise-list").show();
        $('.text-step1').show();
        $('.text-step2').hide();
        $('.text-step3').hide();
    });
    $('.text-step3 .next3 .back3').on("click", function(e) {
        e.preventDefault();
        $('.text-step1').hide();
        $('.text-step2').show();
        $('.text-step3').hide();
    });

    //audio flow
    $('.audio-step1 .next1 .nextto1').on("click", function(e) {
        e.preventDefault();
        // $('#tabs-text').hide();
        $(".advertise-list").hide();
        $('.audio-step1').hide();


        $('.audio-step2').show();
        $('.audio-step3').hide();
        $('.audio-step4').hide();
    });

    $('.audio-step2 .next2 .nextto2').on("click", function(e) {
        e.preventDefault();
        if (selectedTxtBgImgID === null && $("#advertise-text-audio").val().trim() == '') {
            $.notify(notifTextArea, notifyOptions);
            return;
        }

        if (!checkAndNotifyValidUrl($("#ad-url-audio").val().trim(), $("#ad-url-audio"))) {
            return;
        }
        $('.audio-step1').hide();
        $('.audio-step2').hide();

         $(".ads-middle-box").find("p").text($("#advertise-text-audio").val());
        $('.audio-step3').show();
        $('.audio-step4').hide();
    });

    $('.audio-step3 .next3 .nextTo3').on("click", function(e) {
        e.preventDefault();

        var isAudioUploaded = uploadAudioBtn.attr("audio-uploaded");
        var isRecordingUploaded = uploadRecordingBtn.attr("recording-uploaded");
        if (isAudioUploaded === "true" || isRecordingUploaded === "true") {
            $('.audio-step1').hide();
            $('.audio-step2').hide();
            $('.audio-step3').hide();
            $('.audio-step4').show();

        } else {
            $.notify(notifUploadRecord, notifyOptions);

        }

    });


    $('.audio-step4 .next4 .back3').on("click", function(e) {
        e.preventDefault();
        $('.audio-step1').hide();
        $('.audio-step2').hide();
        $('.audio-step3').show();
        $('.audio-step4').hide();
    });

    $('.audio-step3 .next3 .back3').on("click", function(e) {
        e.preventDefault();
        $('.audio-step1').hide();
        $('.audio-step2').show();
        $('.audio-step3').hide();
        $('.audio-step4').hide();
    });
    $('.audio-step2 .next2 .back2').on("click", function(e) {
        e.preventDefault();
        $(".advertise-list").show();
        $('.audio-step1').show();
        $('.audio-step2').hide();
        $('.audio-step3').hide();
        $('.audio-step4').hide();
    });

    // end of audio flow

    $('.video-step1 .nextto1').on("click", function(e) {
        e.preventDefault();
        $(".ads-middle-box").css("background-color", "transparent");
        $(".advertise-list").hide();
        $('.video-step1').hide();
        $('.video-step2').show();
        $('.video-step3').hide();
        $('.video-step4').hide();
    });

    $('.video-step2 .back2').on("click", function(e) {
        e.preventDefault();
        $(".advertise-list").show();
        $('.video-step1').show();
        $('.video-step2').hide();
        $('.video-step3').hide();
        $('.video-step4').hide();
    });

    $('.video-step2 .next2 .nextto2').on("click", function(e) {
        e.preventDefault();

        var url = $("#ad-url-video").val().trim();
        if (!checkAndNotifyValidUrl(url, $("#ad-url-video"))) {
            return;
        }

        if ($(".ads-middle-box").css("background-image") === null ||
            $(".ads-middle-box").css("background-image") === 'none') {
            $.notify(notifUploadOrSkip, notifyOptions);
            return;
        }

        $('.video-step1').hide();
        $('.video-step2').hide();
        $('.video-step3').show();
        $('.video-step4').hide();
    });

    $('.video-step3 .next4 .submit').on("click", function(e) {
        e.preventDefault();

        //add validations
        var url = $("#video-ad-url").val().trim();
        var isRecordingUploaded = uploadVideoRecordingBtn.attr("recording-uploaded");
        if (url != '' ) {
        if(!url.startsWith("http")){
            url = "http://" + url;
            $("#video-ad-url").val(url);
            }
            vidPreview.src=url;
        }
        if ((url != '' && validURL(url)) || $("#video-upload").attr("video-uploaded") === "true" || isRecordingUploaded === "true") {

            $("#video-play-icon").show();
            $('.video-step1').hide();
            $('.video-step2').hide();
            $('.video-step3').hide();
            $('.video-step4').show();
        } else {
            $.notify(notifUploadOrRecordOrUrl, notifyOptions);
            return;
        }
    });



    $('.video-step3 .next4 .back3').on("click", function(e) {
        e.preventDefault();
        $('.video-step1').hide();
        $('.video-step2').show();
        $('.video-step3').hide();
        $('.video-step4').hide();
    });
    $('.video-step4 .next4 .back3').on("click", function(e) {
        e.preventDefault();
        $('.video-step1').hide();
        $('.video-step2').hide();
        $('.video-step3').show();
        $('.video-step4').hide();
    });


    //end of loading
    $.LoadingOverlay("hide", true);
});


function checkAndNotifyValidUrl(url, selector) {
    if (url == '') {
        $.notify(notifUrlRequired, notifyOptions);
        return false;
    }
    if (!url.startsWith("http")) {
        url = "http://" + url;

    }
    if (!validURL(url)) {
        $.notify(notifUrlValid, notifyOptions);
        return false;
    }
    selector.val(url);
    return true;
}

function removeTargetArea(elem) {
    var index = $(elem).data("id");
    findAndRemove(addressesJson, 'id', index);
    $(elem).closest("tr").remove();
}

function findAndRemove(array, property, value) {
    array.forEach(function(result, index) {
        if (result[property] === value) {
            //Remove from array
            array.splice(index, 1);
        }
    });
}


function initMap() {
    console.log('init');
    var geocoder = new google.maps.Geocoder(); // Geocoding
    var uluru = {
        lat: -25.363,
        lng: 131.044
    };
    map = new google.maps.Map(document.getElementById('target-map'), {
        zoom: 4,
        center: uluru
    });
    marker = new google.maps.Marker({
        position: uluru,
        draggable: true,
        map: map
    });
    google.maps.event.addListener(marker, "dragend", updateMap);
    google.maps.event.addListener(map, 'click', updateMap);

    changeLocation("10022", 300);
}

function changeLocation(zipCode, radius) {
    deleteCircles();

    geocoder = new google.maps.Geocoder();
    if (zipCode !== null)
        geocoder.geocode({
            'address': zipCode
        }, function(results, status) {
            if (status === 'OK') {
                var location = results[0].geometry.location;
                var result = results[0];

                getLocationName(result);

                map.setCenter(location);

                var point = {};
                point.lat = location.lat();
                point.lng = location.lng();
                selectedPoint = point;

                marker = new google.maps.Marker({
                    position: {
                        lat: point.lat,
                        lng: point.lng
                    },
                    draggable: true,
                    map: map
                });

                var circle = new google.maps.Circle({
                    strokeColor: '#9c0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 1,
                    fillColor: '#AA0000',
                    fillOpacity: 0.35,
                    map: map,
                    center: point,
                    radius: radius * 1609.34
                });
                circles.push(circle);
                google.maps.event.addListener(marker, "dragend", updateMap);
            } else {
                console.log('Geocode was not successful for the following reason: ' + status);
            }
        });
}


var updateMap = function(e) {
    deleteCircles();
    if (geocoder === null || geocoder === 'undefined')
        geocoder = new google.maps.Geocoder();

    if (marker !== null) {
        marker.setMap(null);
    }
    var point = {};
    point.lat = isFunction(e.latLng.lat) ? e.latLng.lat() : e.latLng.lat;
    point.lng = isFunction(e.latLng.lng) ? e.latLng.lng() : e.latLng.lng;
    selectedPoint = point;

    geocoder.geocode({
        'location': point
    }, function(results, status) {
        if (status === 'OK') {
            if (results.length > 0) {
                var result = results[0];
                var location = results[0].geometry.location;

                getLocationName(result);
                var radius = $('#mileRadius').val();

                var circle = new google.maps.Circle({
                    strokeColor: '#9c0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 1,
                    fillColor: '#AA0000',
                    fillOpacity: 0.35,
                    map: map,
                    center: point,
                    radius: radius * 1609.34
                });
                circles.push(circle);

                console.log('Setting user location lat:lng to ' + point.lat + ':' + point.lng);

                map.setCenter(location);
                marker = new google.maps.Marker({
                    position: {
                        lat: point.lat,
                        lng: point.lng
                    },
                    draggable: true,
                    map: map
                });
                google.maps.event.addListener(marker, "dragend", updateMap);

            } else {
                console.log('No results found');
            }
        } else {
            console.log('Geocoder failed due to: ' + status);
        }
    });
}

function getLocationName(result) {
    var addressComponents = result.address_components;
    for (var i = 0; i < addressComponents.length; i++) {
        var addressComponent = addressComponents[i];
        var addressComponentTypes = addressComponent.types;
        for (var j = 0; j < addressComponentTypes.length; j++) {
            if (addressComponentTypes[j] === 'postal_code') {
                //$zipCode.val(addressComponent.long_name);
                break;
            } else if (addressComponentTypes[j] === 'country') {
                selectedCountry = addressComponent.long_name
                //$country.val(addressComponent.long_name);
                break;
            } else if (addressComponentTypes[j] === 'administrative_area_level_1') {
                selectedStateShort = addressComponent.short_name;
                selectedState = addressComponent.long_name;
                //$state.val(addressComponent.long_name);
                break;
            } else if (addressComponentTypes[j] === 'administrative_area_level_2') {
                //$province.val(addressComponent.long_name);
                break;
            } else if (addressComponentTypes[j] === 'locality') {
                selectedCity = addressComponent.short_name;
                //$city.val(addressComponent.long_name);
                break;
            } else if (addressComponentTypes[j] === 'street_number') {
                //$streetNumber.val(addressComponent.long_name);
                break;
            }
        }
    }
}

function changeRadius(radius) {
    circles[circles.length - 1].setRadius(radius * 1609.34);
}

function deleteCircles() {
    for (var i in circles) {
        if (!isSelected(circles[i]))
            circles[i].setMap(null);
    }
    circles = [];
}

function isSelected(circle) {
    for (var areaCircle in addedAreas) {
        console.log((addedAreas[areaCircle].r * 1609.34) + ', ' + circle.radius);
        if (addedAreas[areaCircle].s.lat == circle.center.lat() && addedAreas[areaCircle].s.lng == circle.center.lng() &&
            (addedAreas[areaCircle].r * 1609.34) == circle.radius)
            return true;
    }
    return false;
}

function isAreaSelected(obj) {
    for (area in addedAreas) {
        if (addedAreas[area].r == obj.r && addedAreas[area].s.lat == obj.s.lat &&
            addedAreas[area].s.lng == obj.s.lng)
            return true;
    }
    return false;
}


function isFunction(functionToCheck) {
    var getType = {};
    return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}

function constructSound(url) {
    sound = new Howl({
        src: [url],
        html5: true, // Force to HTML5 so that the audio can stream in (best for large files).
        onplay: function() {
            audioBtn.html(stopHtml);
        },
        onload: function() {
            audioBtn.html(playHtml);
        },
        onend: function() {
            audioBtn.html(playHtml);
        },
        onpause: function() {
            audioBtn.html(playHtml);
        },
        onstop: function() {
            audioBtn.html(playHtml);
        }
    });

    if (typeof url !== 'undefined') {
        audioBtn.html(playHtml);
    }
}

function containsValidationErrors(response) {
    /*$('.error-msg').each(function () {
     $(this).text('');
     $(this).hide();
     });

     if (response !== null && response.errorMessageList !== null && typeof response.errorMessageList !== 'undefined') {
     for (var i = 0; i < response.errorMessageList.length; i++) {
     var item = response.errorMessageList[i];
     var $controlGroup = $('#' + item.fieldName + 'FormGroup');
     var $errorMsg = $controlGroup.find('.error-msg');
     $errorMsg.text(item.message);
     $errorMsg.show();
     }*/

    if (response !== null && response.errorMessageList !== null && typeof response.errorMessageList !== 'undefined') {
        for (var i = 0; i < response.errorMessageList.length; i++) {
            var item = response.errorMessageList[i];
            $.notify(item.fieldName + ": " + item.message, notifyOptions);
        }
        return true;
    }
    return false;
}

function setAdInfo(adType) {
    $('#ad-length-info').text(adType + ' length:');
    $('#ad-title').text(adType);
    $(".advertise-list").show();
    $('.terms-link').attr("data-ad", adType);
    $(".ads-middle-box").css("background-image", "none");
    $(".box1").find("p").css("background-image", "none");
    switch (adType) {
        case 'ad':
            displayTextTab(true);
            displayAudioTab(true);
            displayVideoTab(true);
            adTypeParam = "ADVERTISEMENT";
            $("a[data-type='TEXT']").click();
            break;

        case 'comedy':
            displayTextTab(false);
            displayAudioTab(true);
            displayVideoTab(true);
            adTypeParam = "COMEDY";
            $("a[data-type='AUDIO']").click();
            break;

        case 'news':
            displayTextTab(true);
            displayAudioTab(true);
            displayVideoTab(true);
            adTypeParam = "NEWS";
            $("a[data-type='TEXT']").click();
            break;

        case 'podcast':
            displayTextTab(false);
            displayAudioTab(true);
            displayVideoTab(false);
            adTypeParam = "PODCAST";
            $("a[data-type='AUDIO']").click();
            break;

        case 'music':
            displayTextTab(false);
            displayAudioTab(true);
            displayVideoTab(true);
            adTypeParam = "MUSIC";
            $("a[data-type='AUDIO']").click();
            break;

        case 'audiobook':
            displayTextTab(false);
            displayAudioTab(true);
            displayVideoTab(false);
            adTypeParam = "AUDIOBOOK";
            $("a[data-type='AUDIO']").click();
            break;
    }
}

function displayTextTab(show) {
    if (show) {
        $("li[data-type='TEXT']").show();
        $('.text-step1').show();
        $('.text-step2').hide();
        $('.text-step3').hide();
        $('.text-step4').hide();
    } else {
        $("li[data-type='TEXT']").hide();
    }
}

function displayAudioTab(show) {
    if (show) {
        $("li[data-type='AUDIO']").show();
        $('.audio-step1').show();
        $('.audio-step2').hide();
        $('.audio-step3').hide();
        $('.audio-step4').hide();
    } else {
        $("li[data-type='AUDIO']").hide();
    }
}

function displayVideoTab(show) {
    if (show) {
        $("li[data-type='VIDEO']").show();
        $('.video-step1').show();
        $('.video-step2').hide();
        $('.video-step3').hide();
        $('.video-step4').hide();
    } else {
        $("li[data-type='VIDEO']").hide();
    }
}

function processPaymentForm(nonce) {
    var advertiseForm = $("#ad-form");
    var audienceForm = $("#ad-audience-preferences-form");

    submitBtn.addClass("loading");
    submitBtn.prop('disabled', true);

    var jsonData = {};

    jsonData.adType = adTypeParam;

    var poData1 = advertiseForm.serializeArray();
    for (var i = 0; i < poData1.length; i++) {
        jsonData[poData1[i].name] = poData1[i].value;
    }

    var poData2 = audienceForm.serializeArray();
    for (var j = 0; j < poData2.length; j++) {
        jsonData[poData2[j].name] = poData2[j].value;
    }

    var poData3 = $("#ad-form1").serializeArray();
    for (var x = 0; x < poData3.length; x++) {
        jsonData[poData3[x].name] = poData3[x].value;
    }

    jsonData.targetAreas = JSON.stringify(addressesJson);
    jsonData.mediaType = adMediaType;

    var featuredAdBackgroundID = $("#cover-image-input-large").val();

    if (adMediaType === "TEXT") {
        jsonData.textColor = selectedTxtColor;
        jsonData.textAlign = selectedTxtAlign;
        jsonData.backgroundColorID = selectedTxtBgColorID;
        jsonData.backgroundImageID = selectedTxtBgImgID;
        jsonData.featuredAdBackgroundID = featuredAdBackgroundID;
        jsonData.text = $('#advertise-text').val();
        jsonData.url = $('.adUrl').val();
        jsonData.email = $('#email-text').val();
    } else if (adMediaType === "AUDIO") {
        jsonData.textColor = selectedTxtColor;
             jsonData.textAlign = selectedTxtAlign;
        jsonData.backgroundColorID = selectedTxtBgColorID;
        jsonData.backgroundImageID = selectedTxtBgImgID;
        jsonData.featuredAdBackgroundID = featuredAdBackgroundID;
        jsonData.text = $('#advertise-text-audio').val();
        jsonData.url = $('#ad-url-audio').val();
         jsonData.email = $('#email-audio').val();
    } else if (adMediaType === "VIDEO") {
        jsonData.backgroundImageID = selectedTxtBgImgID;
        jsonData.featuredAdBackgroundID = featuredAdBackgroundID;
        jsonData.url = $('#ad-url-video').val();
        jsonData.mediaFileUrl = $("#video-ad-url").val().trim();
         jsonData.email = $('#email-vd').val();

    }

    jsonData.nonce = nonce;
    jsonData.coupon = $("#coupon-code").val();

    $.ajax({
        url: baseUrl + "api/v1/ads/add",
        type: "POST",
        data: jsonData,
        success: function(data) {
            $("#popup-message-title").text(payPopTitle);
            $("#popup-message-text").text(payPopText);
            $("#message-popup").modal({
                closable: false
            }).modal('show');
            submitBtn.removeClass("loading");
            submitBtn.prop('disabled', false);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            submitBtn.removeClass("loading");
            submitBtn.prop('disabled', false);

            if (jqXHR.status === 400)
                showNotifyErrorMessages(jqXHR.responseJSON);
            else if (jqXHR.status === 401)
                window.location.href = baseUrl + "login";
        },
        complete: function(jqXHR, textStatus) {
            $requestNonceBtn.removeClass("loading disabled");
        }
    });
}


function getCalcPriceData() {
    return {
        ad_type: adTypeParam,
        media_type: adMediaType,
        duration: $("#length-input").val(),
        coupon: $("#coupon-code").val()
    };
}