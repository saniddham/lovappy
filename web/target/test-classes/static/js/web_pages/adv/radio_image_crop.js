$(document).ready(function () {

    $('.upload-photo-id').on('click', function (e) {
        e.preventDefault();
        $('#upload-modal').modal('show');
        openTab('tab1');
    });

    $('#upload-button').click(function () {
        $('#inputImage').click();
    });

    $('#change-image').click(function () {
        $('#inputImage2').click();
    });


    $('.upload-single-photo,#upload-custom-bg-3').on('click', function (e) {
        e.preventDefault();
        $('#single-upload-modal').modal('show');
    });

    $('#upload-single-image-button').click(function () {
        $('#inputSingleImage').click();
    });

});

function uploadFile() {
    var uploadImageContainer = $("#image-upload-container");
    var uploadImageBtn = $("#upload-image-btn");
    var selectedTxtFeBgImgID = $("#cover-image-input-large");

    uploadImageContainer.find(".loading").show();
    uploadImageContainer.find(".upload-area").hide();
    uploadImageBtn.addClass("disabled");

    $('#image1').cropper('getCroppedCanvas',{ fillColor: '#FFFFFF' }).toBlob(function (blob) {
        var formData = new FormData();

        formData.append('cover_image', blob);

        $.ajax({
            url: baseUrl + "api/v1/ads/cover_image",
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function(data) {
                selectedTxtBgImgID = data.id;
                selectedTxtBgColorID = null;
                $(".tabs1-first").css("background-image", "url(" + data.url + ")");
                $(".ads-middle-box").css("background-image", "url(" + data.url + ")");
                $('#upload-modal').modal('hide');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $this.siblings("i").show();
                $this.siblings(".loading").hide();

                if (jqXHR.status === 400)
                    showNotifyErrorMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });
    });

    $('#image2').cropper('getCroppedCanvas',{ fillColor: '#FFFFFF' }).toBlob(function (blob) {
        var formData = new FormData();

        formData.append('cover_image', blob);

        $.ajax({
            url: baseUrl + "api/v1/ads/cover_image",
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function(data) {
                selectedTxtFeBgImgID.val(data.id);
                $('#large-image-id').val(data.id);
                $('#large-image-url').val(data.url);
                $("#large-img-url").css("background-image", "url(" + data.url + ")");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 400)
                    showNotifyErrorMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });
    });
}


function uploadSingleFile() {
    var uploadImageContainer = $("#image-upload-container");
    var uploadImageBtn = $("#upload-image-btn");

    uploadImageContainer.find(".loading").show();
    uploadImageContainer.find(".upload-area").hide();
    uploadImageBtn.addClass("disabled");

    $('#single-image').cropper('getCroppedCanvas',{ fillColor: '#FFFFFF' }).toBlob(function (blob) {
        var formData = new FormData();

        formData.append('cover_image', blob);

        $.ajax({
            url: baseUrl + "api/v1/ads/cover_image",
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function(data) {
                selectedTxtBgImgID = data.id;
                selectedTxtBgColorID = null;
                $(".tabs1-first").css("background-image", "url(" + data.url + ")");
                $(".ads-middle-box").css("background-image", "url(" + data.url + ")");
                $('#single-upload-modal').modal('hide');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $this.siblings("i").show();
                $this.siblings(".loading").hide();

                if (jqXHR.status === 400)
                    showNotifyErrorMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });
    });
}