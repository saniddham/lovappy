/**
 * Created by Eias Altawil on 7/10/2017
 */
window.AudioContext = window.AudioContext || window.webkitAudioContext;

var audioInput;
var audioContext = new AudioContext();
var realAudioInput = null, inputPoint = null;
var recorder;
var file;

var recordedAudio;
var recordButton;

var recordedSeconds = 0;
var recordedMinutes = 0;

//Timer Vars
var count = 0;
var clearTime;
var seconds = recordedSeconds,
    minutes = 0,
    hours = 0;
var clearState;
var secs = formatTime(seconds, true);
var mins = formatTime(minutes, false);
var gethours;

$( document ).ready(function() {

    initAudio();

    recordedAudio = document.querySelector('audio#recorded');
    recordButton = document.querySelector('#record-btn');

    $("#record-btn").on("click",function (e) {
        e.preventDefault();
        toggleRecording();
    });

    // window.isSecureContext could be used for Chrome
    var isSecureOrigin = location.protocol === 'https:' ||
        location.hostname === 'localhost';
    if (!isSecureOrigin) {
        //alert('getUserMedia() must be run from a secure origin: HTTPS or localhost.' +
          //  '\n\nChanging protocol to HTTPS');
        location.protocol = 'HTTPS';
    }

    recordedAudio.addEventListener('ended',function() {
        applyPlayStyle();
    });


    document.getElementById("timer").innerHTML = mins + secs;


});

function toggleRecording() {
    if (recordButton.dataset.action === 'Record') {
        startRecording();
        startTime(false);
    } else if (recordButton.dataset.action === 'StopRecording') {
        finishRecording();
    } else if (recordButton.dataset.action === 'Play') {
        play();
        startTime(true);
        recordButton.textContent = 'Stop';
        recordButton.dataset.action = 'StopPlaying';
        recordButton.style.background = "red";
    } else if (recordButton.dataset.action === 'StopPlaying') {
        recordedAudio.pause();
        applyPlayStyle();
    }
}

function applyPlayStyle() {
    stopTime();
    recordButton.textContent = advertiseRecordAudioPlay;
    recordButton.dataset.action = 'Play';
    recordButton.style.background = "green";
}

function finishRecording() {
    recordedSeconds = seconds;
    recordedMinutes = minutes;
    stopRecording();
    stopTime();
    recordButton.textContent = advertiseRecordAudioPlay;
    recordButton.dataset.action = 'Play';
    recordButton.style.background = "green";
    $('#record-btn').addClass("play-recording");
    //document.getElementById("timer").style.display = "none";
    $('#send_pm').prop('disabled', false);
}

function startRecording() {
    recorder.startRecording();
    console.log('Recording...');

    $('#record-btn').addClass("is-recording");
    recordButton.dataset.action = 'StopRecording';
}

function stopRecording() {
    recorder.finishRecording();

    console.log('Stopped recording.');
}

function resetRecording() {
    stopRecording();
    recordedSeconds = 0;
    recordedMinutes = 0;
    stopTime();
    recordButton.textContent = 'Record';
    recordButton.dataset.action = 'Record';
    recordButton.style.background = "red";
}

function play() {
    if(recordFile){
        recordedAudio.src = recordFile;
    }else{
        recordedAudio.play();
    }
}

function gotStream(stream) {
    inputPoint = audioContext.createGain();

    // Create an AudioNode from the stream.
    realAudioInput = audioContext.createMediaStreamSource(stream);
    audioInput = realAudioInput;
    audioInput.connect(inputPoint);

    recorder = new WebAudioRecorder(inputPoint, {
        workerDir: baseUrl + "js/WebAudioRecorder/",     // must end with slash
        encoding: "mp3",
        options: {
            timeLimit: 30,
            mp3: {bitRate: 64}
        }
    });

    recorder.onComplete = function(rec, blob) {
        applyPlayStyle();

        recordedAudio.src = window.URL.createObjectURL(blob);

        var filename = (new Date).toISOString().replace(/:|\./g, '-') + '.mp3';
        file = new File([blob], filename);
     $("#upload-recording-btn").prop('disabled', false);
        console.log('Recording Completed.');
    };

    recorder.onError = function(recorder, message) {
        console.log(message);
    };
}

function initAudio() {
    if (!navigator.getUserMedia)
        navigator.getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
    if (!navigator.cancelAnimationFrame)
        navigator.cancelAnimationFrame = navigator.webkitCancelAnimationFrame || navigator.mozCancelAnimationFrame;
    if (!navigator.requestAnimationFrame)
        navigator.requestAnimationFrame = navigator.webkitRequestAnimationFrame || navigator.mozRequestAnimationFrame;

    navigator.getUserMedia(
        {
            "audio": {
                "mandatory": {
                    "googEchoCancellation": "false",
                    "googAutoGainControl": "false",
                    "googNoiseSuppression": "false",
                    "googHighpassFilter": "false"
                },
                "optional": []
            },
        }, gotStream, function (e) {
            //alert('Error getting audio');
            console.log(e);
        });
}


/*
 *Recorder Timer Script
 */

function startWatch(reverse) {
    /* check if seconds is equal to 60 and add a +1 to minutes, and set seconds to 0 */
    if (seconds === 60 && !reverse) {
        seconds = 0;
        minutes = minutes + 1;
    }

    if (seconds < 0 && reverse) {
        seconds = 59;
        minutes = minutes - 1;
    }

    /* you use the javascript tenary operator to format how the minutes should look and add 0 to minutes if less than 10 */
    mins = formatTime(minutes, false);
    //mins = (minutes < 10) ? ('0' + minutes + ': ') : (minutes + ': ');

    /* check if minutes is equal to 60 and add a +1 to hours set minutes to 0 */
    if (minutes === 60 && !reverse) {
        minutes = 0;
        hours = hours + 1;
    }

    if (minutes < 0 && reverse) {
        minutes = 59;
        hours = hours - 1;
    }

    /* you use the javascript tenary operator to format how the hours should look and add 0 to hours if less than 10 */
    gethours = formatTime(hours, false);
    //gethours = (hours < 10) ? ('0' + hours + ': ') : (hours + ': ');
    secs = formatTime(seconds, true);
    //secs = (seconds < 10) ? ('0' + seconds) : (seconds);

    // display the stopwatch
    var x = document.getElementById("timer");
    x.innerHTML = /*gethours +*/ mins + secs;

    /* call the seconds counter after displaying the stop watch and increment seconds by +1 to keep it counting */
    if(reverse){
        seconds--;
    }else {
        seconds++;
    }

    if(reverse && seconds < 0){
        seconds = 0;
        stopTime();
        return;
    }

    if(!reverse && seconds > 30){
        finishRecording();
        return;
    }

    /* call the setTimeout( ) to keep the stop watch alive ! */
    clearTime = setTimeout("startWatch(" + reverse + ")", 1000);
}

//create a function to start the stop watch
function startTime(reverse) {

    /* check if seconds, minutes, and hours are equal to zero and start the stop watch */
    //if (seconds === 0 && minutes === 0 && hours === 0) {

    /* hide the start button if the stop watch is running */
    //this.style.display = "none";

    /* call the startWatch( ) function to execute the stop watch whenever the startTime( ) is triggered */
    startWatch(reverse);
    //}
} // startwatch.js end

//create a function to stop the time
function stopTime() {

    /* check if seconds, minutes and hours are not equal to 0 */
    //if (seconds !== 0 || minutes !== 0 || hours !== 0) {

    // reset the stop watch
    seconds = recordedSeconds;
    minutes = recordedMinutes;
    //hours = 0;
    secs = (seconds < 10) ? ('0' + seconds) : (seconds);
    mins = (minutes < 10) ? ('0' + minutes + ': ') : (minutes + ': ');
    //gethours = '0' + hours + ': ';

    /* display the stopwatch after it's been stopped */
    var x = document.getElementById("timer");
    var stopTime = /*gethours +*/ mins + secs;
    x.innerHTML = stopTime;

    /* clear the stop watch using the setTimeout( )
     return value 'clearTime' as ID */
    clearTimeout(clearTime);
    //}
} // stopwatch.js end

function formatTime(time, isSeconds) {
    if(isSeconds)
        return (time < 10) ? ('0' + time) : (time);
    else
        return (time < 10) ? ('0' + time + ': ') : (time + ': ');
}