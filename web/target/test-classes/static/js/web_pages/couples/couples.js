
$(document).ready(function () {


    /*
     *Send Private Message
     */
    $("#send-private-message-btn").click(function () {
        $(".modal[data-edit='private-message']").modal("show");
    });

      $(".sendBtn").click(function () {
            toastr.error('Partner email is required !');
        });
           $(".sendPendingBtn").click(function () {
                    toastr.error('Waiting approval from your partner !');
      });


         $("#emailSubmit").on('click', function (e) {
              e.preventDefault();

             if(saveCoupleProfileEmail()){
               location.reload();
              }

          });
});

function saveCoupleProfileEmail(){
 if ($("input[name='partnerEmail']").val() == "") {

              toastr.error('Partner email is required !');
        return;
    }


  $("#submitForm").ajaxSubmit({
            type: 'POST',
            async: false,
            crossDomain: true,
            headers: {
                Accept: "application/json; charset=utf-8"
            },

            url: baseUrl + "/couples/partner/email/save",
            success: function(data) {

              return true;
            },
         error: function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 400)
                        showValidationMessages(jqXHR.responseJSON);
                      toastr.error('Partner email is invalid !');
                        return false;

                    }



        });

        return true;

}