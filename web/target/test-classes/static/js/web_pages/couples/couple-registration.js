  $(document).ready(function () {


  $(".choose-options").click(function () {
        $('.choose-options').removeClass('active');
        $(this).addClass('active');
    });

 $("#coupleImage").on('change', function() {
          readURL(this, $('#couple-img'));
      });

    $('#birthdate').dcalendar(); // display calendar
    /*Select date on date changed*/
    $('#birthdate').dcalendarpicker({
        dateFormat: 'dd-mm-yyyy'
    }).on('selectdate', function (e) {

        console.log("The date is " + e.date);
        $('.date-value').val(e.date);
    });

    $('#anniversary').dcalendar(); // display calendar
    /*Select date on date changed*/
    $('#anniversary').dcalendarpicker({
        dateFormat: 'dd-mm-yyyy'
    }).on('selectdate', function (e) {

        console.log("The date is " + e.date);
        $('.anniver-date').val(e.date);
    });

    // This button will increment the value
    $('.qtyplus').click(function (e) {
        // Stop acting like a button
        e.preventDefault();

        var currentValue = parseInt($('#attendees-limit').val());

        if (isNaN(currentValue)) {
            currentValue = 0;
        }

        if (!isNaN(currentValue)) {

            $('#attendees-limit').val(currentValue + 1);

        }

    });
    // This button will decrement the value till 0
    $(".qtyminus").click(function (e) {

        // Stop acting like a button
        e.preventDefault();

        var currentValue = parseInt($('#attendees-limit').val());
        if (isNaN(currentValue)) {
            currentValue = 0;
        }
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentValue) && currentValue > 0) {
            // Decrement one
            $('#attendees-limit').val(currentValue - 1);
        } else {
            // Otherwise put a 0 there
            $('#attendees-limit').val(0);
        }
    });

    $("#couple-submit").on('click', function (e) {
        e.preventDefault();

       if(createCoupleProfile()){
        window.location.href = baseUrl + 'couples/dashboard';
        }

    });
   var $coupleseekingFormLoader = $('#couple-update i');
    $("#couple-update").on('click', function (e) {
        e.preventDefault();
  $coupleseekingFormLoader.show();
  $(this).text("Saving");
        var success = saveCoupleProfile();
        console.log("success" + success);
        if(success){
              $coupleseekingFormLoader.hide();
              $("#popup-message-title").text("Done");
                           $("#popup-message-text").text("Seeking information saved success.");
                           $('#message-popup').modal({
                               closable  : false,
                               onApprove : function() {
                                   location.reload();
                               }
                           }).modal('show');

              }

    });

    });



function createCoupleProfile(){
 if (!$("#gender-male").prop("checked") && !$("#gender-female").prop("checked")) {
        toastr.error('Gender required !');
        return;
    }

       if ($("input[name='birthDate']").val() == "") {
            toastr.error('Date of Birth required !');
            return false;
        }else if (!isDate($("input[name='birthDate']").val())){
        toastr.error('Invalid Date of Birth !');
            return false;
           }else if (!isValidBirthDate($("input[name='birthDate']").val())){
            toastr.error('You should be above 18 !');
                return false;
            }
    return saveCoupleProfile();
}
function saveCoupleProfile(){

    if (!$("#partner-male").prop("checked")&&!$("#partner-female").prop("checked")) {
        toastr.error('Pref Gender required !');
        return;
    }

     if ($("input[name='anniversaryDate']").val() == "") {
            toastr.error('Anniversary Date required !');
            return false;
        }else if (!isDate($("input[name='anniversaryDate']").val())){
        toastr.error('Invalid Anniversary Date !');
            return false;
        }

      var yearsTogether = $("#attendees-limit").val();
            if ($("#attendees-limit").val() == "" || $("#attendees-limit").val() == "0") {
                    toastr.error('Years together required !');
                    return false;
                }

     var formData = new FormData($('#couples-form')[0]);

        var fileInput = document.getElementById("coupleImage");
        if ('files' in fileInput && fileInput.files.length === 1) {
            formData.append('coupleImage', fileInput.files[0]);
        } else {
          formData.append('coupleImage', null);
           if($("#uploaded_img").val()=='false'){
                 toastr.error('Photo required !');
                                 return false;
             }

        }

        $.ajax({
            url: baseUrl + "couples/signup",
            type: "POST",
            async:false,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,

            success: function (data) {
            return true;

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 400)
                showValidationMessages(jqXHR.responseJSON);


            }
        });

        return true;

}