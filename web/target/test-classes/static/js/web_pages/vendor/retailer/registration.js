$(document).ready(function () {

    $('#country').dropdown();
});

var submitBtn = $("#register-vendor");

submitBtn.on('click', function (e) {
    e.preventDefault();
    submitBtn.addClass("disabled");
    submitBtn.addClass("loading");

    var x = $('#addressLine1').validate()
        & $('#countryInput').validateCountry()
        & $('#city').validate()
        & $('#state').validate()
        & $('#zipCode').validate();

    if (x) {
        var url = "/retailer/register";
        $.ajax({
            url: baseUrl + url,
            type: "POST",
            data: $('#reg-form').serialize(),
            success: function (data) {
                if (containsValidationErrors(data)) {
                    submitBtn.removeClass("disabled");
                    submitBtn.removeClass("loading");
                } else {
                    submitBtn.removeClass("disabled");
                    submitBtn.removeClass("loading");
                    location.reload();
                }
            },
            error: function (data) {
                submitBtn.removeClass("disabled");
                submitBtn.removeClass("loading");
                toastr.error('Failed to save !!');
            }
        });
    } else {
        toastr.error($('#fill-all-fields').val());
        submitBtn.removeClass("disabled");
        submitBtn.removeClass("loading");
    }

});


(function ($) {
    $.fn.validate = function () {
        var success = true;
        if ($(this).val() == '') {
            $(this).css('border', '1px solid red');
            success = false;
        } else
            $(this).css('border', '1px solid #898989');

        return success;
    }

}(jQuery));

(function ($) {
    $.fn.validateCountry = function () {
        var success = true;
        if ($(this).val() == '') {
            $('#country').css('border', '1px solid red');
            success = false;
        } else
            $('#country').css('border', '1px solid #898989');

        return success;
    }

}(jQuery));

function containsValidationErrors(response) {
    $('.error-msg').each(function () {
        $(this).text('');
        $(this).hide();
    });

    if (response !== null && response.errorMessageList !== null && typeof response.errorMessageList !== 'undefined') {
        var errorMessages;
        for (var i = 0; i < response.errorMessageList.length; i++) {

            if (!!errorMessages) {
                errorMessages += response.errorMessageList[i].message + "</br>";
            } else {
                errorMessages = response.errorMessageList[i].message + "</br>";
            }
            toastr.error(response.errorMessageList[i].message);

        }
        if (!!errorMessages) {
            $('#confirmTerms').html(errorMessages);
            $('#confirmTerms').show();
        }
        return true;
    }
    return false;
}