/**
 * Created by Darrel Rayen on 10/20/17.
 */

var datingLocationSubmit;

$(document).ready(function () {

    //Calculate Rating Overall
    $('#overall-rating i').on('click', function () {
        var onStar = parseInt($(this).data('value'), 10); // The star currently selected
        var stars = $(this).parent().children('i.star');
        for (i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass('active');
        }
        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass('active');
        }
        var ratingValue = parseInt($('#overall-rating i.active').last().data('value'), 10);

        $('#overall-rating-star').val(ratingValue);

    });


    //Calculate Rating Security
    $('#security-rating i').on('click', function () {
        var onStar = parseInt($(this).data('value'), 10); // The star currently selected
        var stars = $(this).parent().children('i.star');
        for (i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass('active');
        }
        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass('active');
        }
        var ratingValue = parseInt($('#security-rating i.active').last().data('value'), 10);

        $('#security-rating-star').val(ratingValue);

    });
    //Calculate Rating Parking
    $('#parking-rating i').on('click', function () {
        var onStar = parseInt($(this).data('value'), 10); // The star currently selected
        var stars = $(this).parent().children('i');
        for (i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass('active');
        }
        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass('active');
        }
        var ratingValue = parseInt($('#parking-rating i.active').last().data('value'), 10);

        $('#parking-rating-star').val(ratingValue);

    });

    //Calculate Transportation Rating
    $('#transportation-rating i').on('click', function () {
        var onStar = parseInt($(this).data('value'), 10); // The star currently selected
        var stars = $(this).parent().children('i');
        for (i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass('active');
        }
        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass('active');
        }

        var ratingValue = parseInt($('#transportation-rating i.active').last().data('value'), 10);

        $('#transportation-rating-star').val(ratingValue);

    });

    datingLocationSubmit = $('#submit-dating-place-btn');
    datingLocationSubmit.css('cursor', 'pointer');

    //Validate dating places form values
    datingLocationSubmit.on('click', function (e) {
        e.preventDefault();

        datingLocationSubmit.addClass("loading");
        datingLocationSubmit.prop('disabled', true);

        var formData = new FormData($('#dating-place-form')[0]);
        formData.append('nonce', "");
        formData.append('coupon', $("#coupon-code").val());

        $.ajax({
            url: baseUrl + 'dating/places/validate/create',
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,

            success: function (data) {
                datingLocationSubmit.removeClass("loading");
                datingLocationSubmit.prop('disabled', false);
                //Add Dating Place
                if (isFeatured)
                    $('#square-payment-form').modal('show');
                else
                    processPaymentForm("");

            },
            error: function (jqXHR, textStatus, errorThrown) {
                hideErrorMessages();
                if (jqXHR.status === 400) {
                    showValidationMessages(jqXHR.responseJSON);
                    showNotifyErrorMessages(jqXHR.responseJSON);
                } else if (jqXHR.status === 401) {
                    window.location.href = baseUrl + "login";
                } else if (jqXHR.status === 409) {
                    $('#existing-place-name').text(jqXHR.responseJSON.placeName);
                    $('#existing-place-address').text(jqXHR.responseJSON.addressLine);
                    $('#add-dating-modal').modal('show');
                    $('#details-page').attr("href", baseUrl + '/dating/places/' + jqXHR.responseJSON.datingPlacesId);

                    $('#confirm-place').on('click', function (e) {
                        if (isFeatured)
                            $('#square-payment-form').modal('show');
                        else
                            processPaymentForm("");
                    });
                }
            },
            complete: function (jqXHR, textStatus) {
                datingLocationSubmit.removeClass("loading");
                datingLocationSubmit.prop('disabled', false);
                $requestNonceBtn.removeClass("loading disabled");
            }
        });
    });

    var uploadImageContainer = $("#image-upload-container").find("span");
    var loadingHtml = "<span class=\"ui button loading\"></span>";


    $("#dating-location-photo").on('change', function () {
        uploadImageContainer.html(loadingHtml);

        var formData = new FormData();
        formData.append('dating_place_picture', this.files[0]);

        $.ajax({
            url: baseUrl + "dating/places/create/upload_dating-place-picture",
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (data) {
                uploadImageContainer.html("<img src=\"" + data.url + "\"/>");
                $("#dating-place-picture").val(data.id);
                $("#dating-place-cover-picture").val(data.id);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 400)
                    showNotifyErrorMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";

            }
        });
    });

    //Submit Dating Place Review when Logged in
    var locationReviewSubmit = $('#submit-review-btn');
    locationReviewSubmit.css('cursor', 'pointer');
    var datingPlaceId = $('#place-id').val();

    locationReviewSubmit.on('click', function (e) {
        e.preventDefault();
        locationReviewSubmit.addClass("loading");
        locationReviewSubmit.prop('disabled', true);

        $.ajax({
            url: baseUrl + 'dating/places/create/place-review/' + datingPlaceId,
            type: "POST",
            data: $('#add-dating-place-review-form').serialize(),

            success: function (data) {
                locationReviewSubmit.removeClass("loading");
                locationReviewSubmit.prop('disabled', false);
                $("#popup-message-title").text("Done");
                $("#popup-message-text").text("Thank you for submitting a Review. You will be notified by email when it is approved.");
                $('#message-popup').modal({
                    closable: false,
                    onApprove: function () {
                        window.location.href = baseUrl + 'dating/places/' + datingPlaceId;
                    }
                }).modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                locationReviewSubmit.removeClass("loading");
                locationReviewSubmit.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401) {
                    //window.location.href = baseUrl + "login";
                }
            }
        });
    });

    var googleReview = $('#social-login-google');
    var facebookReview = $('#social-login-facebook');
    var twitterReview = $('#social-login-twitter');
    var instagramReview = $('#social-login-instagram');

    //Add review via google
    googleReview.on('click', function (e) {
        e.preventDefault();
        googleReview.addClass("loading");
        googleReview.prop('disabled', true);

        $.ajax({
            url: baseUrl + 'dating/places/social/place-review/' + datingPlaceId,
            type: "POST",
            data: $('#add-dating-place-review-form').serialize(),

            success: function (data) {
                window.location.href = baseUrl + '/login/google';
            },
            error: function (jqXHR, textStatus, errorThrown) {
                googleReview.removeClass("loading");
                googleReview.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
            }
        });
    });

    //Add review via google
    googleReview.on('click', function (e) {
        e.preventDefault();
        googleReview.addClass("loading");
        googleReview.prop('disabled', true);

        $.ajax({
            url: baseUrl + 'dating/places/social/place-review/' + datingPlaceId,
            type: "POST",
            data: $('#add-dating-place-review-form').serialize(),

            success: function (data) {
                window.location.href = baseUrl + '/login/google';
            },
            error: function (jqXHR, textStatus, errorThrown) {
                googleReview.removeClass("loading");
                googleReview.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
            }
        });
    });

    //Add review via Facebook
    facebookReview.on('click', function (e) {
        e.preventDefault();
        facebookReview.addClass("loading");
        facebookReview.prop('disabled', true);

        $.ajax({
            url: baseUrl + 'dating/places/social/place-review/' + datingPlaceId,
            type: "POST",
            data: $('#add-dating-place-review-form').serialize(),

            success: function (data) {
                window.location.href = baseUrl + '/login/facebook';
            },
            error: function (jqXHR, textStatus, errorThrown) {
                facebookReview.removeClass("loading");
                facebookReview.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
            }
        });
    });

    //Add review via Twitter
    twitterReview.on('click', function (e) {
        e.preventDefault();
        twitterReview.addClass("loading");
        twitterReview.prop('disabled', true);

        $.ajax({
            url: baseUrl + 'dating/places/social/place-review/' + datingPlaceId,
            type: "POST",
            data: $('#add-dating-place-review-form').serialize(),

            success: function (data) {
                $("#twitterFrom").submit();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                twitterReview.removeClass("loading");
                twitterReview.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
            }
        });
    });

    //Add review via Instagram
    instagramReview.on('click', function (e) {
        e.preventDefault();
        instagramReview.addClass("loading");
        instagramReview.prop('disabled', true);

        $.ajax({
            url: baseUrl + 'dating/places/social/place-review/' + datingPlaceId,
            type: "POST",
            data: $('#add-dating-place-review-form').serialize(),

            success: function (data) {
                window.location.href = baseUrl + '/login/instagram';
            },
            error: function (jqXHR, textStatus, errorThrown) {
                instagramReview.removeClass("loading");
                instagramReview.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
            }
        });
    });

    //field validations when user is logged out
    var reviewSubmitLoggedOut = $('#submit-review-btn2');
    reviewSubmitLoggedOut.on('click', function (e) {
        e.preventDefault();
        reviewSubmitLoggedOut.addClass("loading");
        reviewSubmitLoggedOut.prop('disabled', true);

        $.ajax({
            url: baseUrl + 'dating/places/create/place-review/validation/' + datingPlaceId,
            type: "POST",
            data: $('#add-dating-place-review-form').serialize(),

            success: function (data) {
                reviewSubmitLoggedOut.removeClass("loading");
                reviewSubmitLoggedOut.prop('disabled', false);
                $('.ui.modal.dating-login').modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                reviewSubmitLoggedOut.removeClass("loading");
                reviewSubmitLoggedOut.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401) {
                    //window.location.href = baseUrl + "login";
                }
            }
        });
    });

    //Add place review when logged out
    var addReviewLoggedOut = $('#logout-submit-btn');
    addReviewLoggedOut.on('click', function (e) {
        var userEmail = $('#reviewer-email').val();
        var reviewerPassword = $('#reviewer-password').val();
        e.preventDefault();
        addReviewLoggedOut.addClass("loading");
        addReviewLoggedOut.prop('disabled', true);
        console.log(userEmail + ' is the email');
        $('#reviewer-email-form').val(userEmail);
        $('#reviewer-password-form').val(reviewerPassword);
        var formData = $('#add-dating-place-review-form').serialize();

        $.ajax({
            url: baseUrl + 'dating/places/create/place-review/sign/out/' + datingPlaceId,
            type: "POST",
            data: formData,
            processData: false,
            success: function (data) {
                addReviewLoggedOut.removeClass("loading");
                addReviewLoggedOut.prop('disabled', false);
                $("#popup-message-title").text("Done");
                $("#popup-message-text").text("Thank you for submitting a Review. You will be notified by email when it is approved.");
                $('#message-popup').modal({
                    closable: false,
                    onApprove: function () {
                        window.location.href = baseUrl + 'dating/places/' + datingPlaceId;
                    }
                }).modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                addReviewLoggedOut.removeClass("loading");
                addReviewLoggedOut.prop('disabled', false);
                hideErrorMessages();
                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401) {
                    //window.location.href = baseUrl + "login";
                }
            }
        });
    });

    $(".social-login").on('click', function (e) {
        e.preventDefault();
        var provider = $(this).attr("provider");
        $.ajax({
            url: baseUrl + "dating/places/social",
            type: "POST",
            data: $('#dating-place-form').serialize(),
            success: function () {
                console.log("success");
                window.location.href = baseUrl + 'login/' + provider;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                hideErrorMessages();
                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401) {
                }
                console.log("unsuccessful");
            }
        });
    });

    $(".social-login-review").on('click', function (e) {
        e.preventDefault();
        var provider = $(this).attr("provider");
        var datingPlaceId = $('#place-id').val();
        $.ajax({
            url: baseUrl + "dating/places/social/place-review/" + datingPlaceId,
            type: "POST",
            data: $('#add-dating-place-review-form').serialize(),
            success: function () {
                console.log("success");
                window.location.href = baseUrl + 'login/' + provider;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                hideErrorMessages();
                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401) {
                }
                console.log("unsuccessful");
            }
        });
    });

    // changing nav elements to show pop up for EVENT_AMBASSADOR only role
    var isAmbassadorOnly = $('#isAmbassadorOnly').val();
    if (isAmbassadorOnly === '1') {
        $('.main-nav ul li').each(function () {
            var field = $(this).find('a').text();
            if (field.toLowerCase() === 'likes' || field.toLowerCase() === 'my map') {
                $(this).find('a').attr('class', 'ambassadorOnlyPopup');
            }
        });
        $('.responsive-nav ul li').each(function () {
            var field = $(this).find('a').text();
            if (field.toLowerCase() === 'likes' || field.toLowerCase() === 'my map') {
                $(this).find('a').attr('class', 'ambassadorOnlyPopup');
            }
        });
    }

    $('.ambassadorOnlyPopup').on('click', function (e) {
        e.preventDefault();
        $('#ambassador-modal').modal('show');
    });

    var modal = document.getElementById('pictModal');

    var slideIndex = 1;

    $("#cover-image").click(function () {
        modal.style.display = "block";
        showSlides(1);
    });

    $("#cover-image1").click(function () {
        modal.style.display = "block";
        showSlides(1);
    });

    $('.prev').click(function () {
        plusSlides(-1);
    });

    $('.next').click(function () {
        plusSlides(1);
    });

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function getdealsCount() {
        var date1 = new Date();
        var dealCountSpan = $('#active-deal-count-span');
        var dealCountSpan2 = $('#active-deal-count-span2');
        $.ajax({
            type: "POST",
            url: baseUrl + 'dating/places/deals/active/' + datingPlaceId,
            data: {today: date1.getTime()},
            success: function (results) {
                console.log(results)
                if (results > 1) {
                    dealCountSpan.text(results + " Active Deals");
                    dealCountSpan2.text(results + " Active Deals");
                } else {
                    dealCountSpan.text(results + " Active Deal");
                    dealCountSpan2.text(results + " Active Deals");
                }
            }
        });
    }

    getdealsCount();

    function getActiveDeals() {
        var date1 = new Date();
        var dealDescription = $('#deal-description');
        var dealExpiryDate = $('#deal-expiry-date');
        var dealPrice = $('#deal-price');
        var dealDetailsDiv = $('#deal-details-div');
        var noDealsDiv = $('#no-deals-available');
        $.ajax({
            type: "GET",
            url: baseUrl + 'dating/places/deals/' + datingPlaceId + '/date=' + date1.getTime() + '/page=' + currentPageNumber,
            success: function (results) {
                totalPages = results.totalPages;
                isLast = results.last;
                if (totalPages > 0) {
                    dealDetailsDiv.show();
                    nextDealButton.show();
                    getDealButton.show();
                    selectedDealId = results.content[0].placeDealId;
                    dealDescription.text(results.content[0].dealDescription);
                    var expiryDate = new Date(results.content[0].expiryDate);
                    var month = expiryDate.getMonth() + 1;
                    dealExpiryDate.text(month + '-' + expiryDate.getDate() + '-' + expiryDate.getFullYear());
                    dealPrice.text(results.content[0].dealPrice);
                } else {
                    dealDetailsDiv.hide();
                    noDealsDiv.show();
                    nextDealButton.hide();
                    getDealButton.hide();
                }
                if (isLast) {
                    nextDealButton.css("pointer-events", "none");
                } else {
                    nextDealButton.css("cursor", "pointer");
                }
            }
        });
    }

    var nextDealButton = $('#next-btn');
    var getDealButton = $('#get-deal-btn');
    nextDealButton.on('click', function (e) {
        e.preventDefault();
        console.log("I am clicked next deal");
        if (currentPageNumber < totalPages) {
            currentPageNumber = currentPageNumber + 1;
            getActiveDeals();
        }

    });

    getDealButton.on('click', function (e) {
        e.preventDefault();

    });
    getActiveDeals();

});
var currentPageNumber = 1;
var isFirst;
var isLast;
var totalPages;
var selectedDealId;

function showSlides(n) {
    console.log('showSlides' + n);
    slideIndex = n;
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active1", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active1";
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}
function hideErrorMessages() {
    $('.error-msg').each(function () {
        $(this).text('');
        $(this).hide();
    });
}

function processPaymentForm(nonce) {
    datingLocationSubmit.addClass("loading");
    datingLocationSubmit.prop('disabled', true);

    var formData = new FormData($('#dating-place-form')[0]);
    formData.append('nonce', nonce);
    formData.append('coupon', $("#coupon-code").val());

    $.ajax({
        url: baseUrl + 'dating/places/create',
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,

        success: function (data) {
            datingLocationSubmit.removeClass("loading");
            datingLocationSubmit.prop('disabled', false);
            $("#popup-message-title").text("Done");
            $("#popup-message-text").text("Thank you for submitting a Dating Place. You will be notified by email when it is approved.");
            $('#message-popup').modal({
                closable: false,
                onApprove: function () {
                    window.location.href = baseUrl + 'dating/places';
                }
            }).modal('show');

        },
        error: function (jqXHR, textStatus, errorThrown) {
            hideErrorMessages();

            if (jqXHR.status === 400) {
                showValidationMessages(jqXHR.responseJSON);
                showNotifyErrorMessages(jqXHR.responseJSON);
            } else if (jqXHR.status === 401) {
                window.location.href = baseUrl + "login";
            } else if (jqXHR.status === 409) {
                console.log(jqXHR.responseJSON);
            }
        },
        complete: function (jqXHR, textStatus) {
            datingLocationSubmit.removeClass("loading");
            datingLocationSubmit.prop('disabled', false);
            $requestNonceBtn.removeClass("loading disabled");
        }
    });
}

function getCalcPriceData() {
    return {coupon: $("#coupon-code").val()};
}