/**
 * Created by Eias Altawil on 7/6/2017
 */

$(document).ready(function () {
    /*
     *Send Private Message
     */
    $("#send-private-message-btn").click(function () {
        $(".modal[data-edit='private-message']").modal("show");
    });
});