/**
 * Created by Eias Altawil on 6/17/2017
 */

$(document).ready(function () {

    var elementA = document.createElement('script');
    elementA.type = 'text/javascript';
    elementA.async = true;
    elementA.defer = true;
    elementA.src = google_url;
    var elementScript = document.getElementsByTagName('script')[0];
    elementScript.parentNode.insertBefore(elementA, elementScript);
    var isCouple=false;
     var isCoupleAlreadySelected=$("#coupleRadio").prop("checked");
        $(document).on("change", "#coupleRadio", function () {
               if(this.checked) {
               if(isCoupleAlreadySelected === false)
                isCouple = true;
               }
           });

    $('.language').on("change", '#language',function () {
        $("#selected-language-id").val($(this).val());
    });

    $('.numeric').numeric(null, ',');
    <!--region Open and Close modals-->
    $('.edit-icon, .lov-panel-header a img').click(function () {
        var selectedArea = $(this).data("edit");
        $(".modal[data-edit='" + selectedArea + "']").modal({
            autofocus: false,
        }).modal('show');
        google.maps.event.trigger(map, "resize");
        map.setCenter(new google.maps.LatLng($('#latitude').val(),$('#longitude').val()));
    });
    <!--endregion-->

    $('#record_new_lovdrop').click(function () {
        $("#record-lovdrop").modal("show");
    });

    $('.hover-upload').click(function () {
        $("input[name='image-type']").val($(this).data('image-type'));
        var imageUrl = $(this).data('image-url');
        if($(this).data('image-type') != 'photo') {
        $("#modalPhoto").attr('src', imageUrl);
        $('.access-profile').hide();
        } else {
        $('.access-profile').show();

          if($(this).data('image-type') != 'hands') {
             $("#modalPhoto").attr('src', imageUrl);
          }else  if($(this).data('image-type') != 'feet') {
             $("#modalPhoto").attr('src', imageUrl);
          } else  if($(this).data('image-type') != 'legs') {
             $("#modalPhoto").attr('src', imageUrl);
          }
        }
        $(".modal[data-edit='upload-image']").modal("show");
    });

    $('.dropdown')
        .dropdown({
            maxSelections: 7
        });
    /*$('.btn-refresh').click(function () {
        geocoder = new google.maps.Geocoder();
        var lat = '';
        var lng = '';
        var address = $('.zip-code').val();
        geocoder.geocode({'address': address}, function (results, status) {
            console.log(results);
            lat = (results[0].geometry.location.lat());
            lng = (results[0].geometry.location.lng());
            map.setCenter(new google.maps.LatLng(lat,
                lng), 13);
            map.setZoom(12);
        });
    });*/

    $("#birthdate").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0",
    });

    $("#btn-save-image").click(function () {
        $(this).text('Saving');
        if($("#image-type").val() === 'photo')
        callAccessProfilePicAPI($('input[name=choosePicAccess]:checked').val());
        uploadFile();
    });

    var $profileFormLoader = $('#btn-save-locationAndLang i');

    $("#btn-save-locationAndLang").click(function() {
        $profileFormLoader.show();

        $.ajax({
            url: baseUrl + "member/save_age_lang_location_info",
            type: "POST",
            data: $('#location-languages-form').serialize(),
            success: function (data) {
                if(containsValidationErrors(data)) {
                    $profileFormLoader.hide();
                } else {
                    $profileFormLoader.hide();
                    $("#popup-message-title").text("Done");
                    $("#popup-message-text").text("Profile information saved success.");
                    $('#message-popup').modal({
                        closable  : false,
                        onApprove : function() {
                            location.reload();
                        }
                    }).modal('show');
                }
            },
            error: function (data) {
                $profileFormLoader.hide();
                console.log(data);
            }
        });
    });


    var $seekingFormLoader = $('#btn-save-seeking i');

    $("#btn-save-seeking").click(function() {
        $seekingFormLoader.show();

        $.ajax({
            url: baseUrl + "member/save_seeking_info",
            type: "POST",
            data: $('#seeking-form').serialize(),
            success: function (data) {
                if(containsValidationErrors(data)) {
                    $seekingFormLoader.hide();
                } else {
                    $seekingFormLoader.hide();

                    if(isCouple){
                     $(".modal[data-edit='edit-couple-modal']").modal("show");
                    } else {
                    $("#popup-message-title").text("Done");
                    $("#popup-message-text").text("Seeking information saved success.");
                    $('#message-popup').modal({
                        closable  : false,
                        onApprove : function() {
                            location.reload();
                        }
                    }).modal('show');
                    }
                }
            },
            error: function (data) {
                $seekingFormLoader.hide();
                console.log(data);
            }
        });
    });


    var $registerLoader = $('#btn-save-preferences i');

    $("#btn-save-preferences").click(function() {
        $registerLoader.show();

        var ddd = $('#edit-preferences-form, #size-prefers-form').serialize();
        console.log("ddd == " + ddd);
        $.ajax({
            url: baseUrl + "member/save_preferences",
            type: "POST",
            data: ddd,
            success: function (data) {
                if(containsValidationErrors(data)) {
                    $registerLoader.hide();
                } else {
                    $registerLoader.hide();
                    $("#popup-message-title").text("Done");
                    $("#popup-message-text").text("Profile preferences saved success.");
                    $('#message-popup').modal({
                        closable  : false,
                        onApprove : function() {
                            location.reload();
                        }
                    }).modal('show');
                }
            },
            error: function (data) {
                $registerLoader.hide();
                console.log(data);
            }
        });
    });


    /*
     *Play LovDrop
     */
    var lovdropPlayImage = "/images/play_block.png";
    var lovdropPauseImage = "/images/pause_viewprof.png";

    var playLovDropButton = $("#play_lovdrop");
    var playLovDropImage = playLovDropButton.find("img");
    var sound = new Howl({
        src: [recordFile],
        html5: true, // Force to HTML5 so that the audio can stream in (best for large files).
        onplay: function () {
            playLovDropImage.attr("src", baseUrl + lovdropPauseImage);
        },
        onload: function () {
            playLovDropImage.attr("src",  baseUrl + lovdropPlayImage);
        },
        onend: function () {
            playLovDropImage.attr("src",  baseUrl + lovdropPlayImage);
        },
        onpause: function () {
            playLovDropImage.attr("src",  baseUrl + lovdropPlayImage);
        },
        onstop: function () {
            playLovDropImage.attr("src",  baseUrl + lovdropPlayImage);
        }
    });

    playLovDropButton.click(function() {
        if(sound.playing()){
            sound.pause();
        } else {
            sound.play();
        }
    });

    //make images square if they not
    $(".profile-images div img.member-pic").each(function () {
        if($(this).height() > $(this).width())
            $(this).css('height','auto');
        if($(this).width() > $(this).height())
            $(this).css('width','155');

    });
});

function containsValidationErrors(response) {
    $('.error-msg').each(function () {
        $(this).text('');
        $(this).hide();
    });

    if (response !== null && response.errorMessageList !== null && typeof response.errorMessageList !== 'undefined') {
        for (var i = 0; i < response.errorMessageList.length; i++) {
            var item = response.errorMessageList[i];
            var $controlGroup = $('#' + item.fieldName + 'FormGroup');
            var $errorMsg = $controlGroup.find('.error-msg');
            $errorMsg.text(item.message);
            $errorMsg.show();
        }
        return true;
    }
    return false;
}
   function callAccessProfilePicAPI(access) {
            $.ajax({
                url: baseUrl + "/api/v1/user/profile/photo/access",
                type: "POST",

                data:  JSON.stringify({"access":access}),
                contentType: "application/json; charset=utf-8",
                success: function () {
               return true;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                               if(jqXHR.status === 400)
                                   showValidationMessages(jqXHR.responseJSON);
                               else if(jqXHR.status === 401)
                                   window.location.href = baseUrl + "login";
                           }
            });
    }
function uploadFile() {
    var canvas  = $("#canvas");
    var imageType = $("#image-type").val();
    var url = baseUrl + "api/v1/user/profile/" + imageType +"/upload";
    var fileInput =  $("#fileInput").val();
    if( fileInput===''){
            location.reload();
    return;
    }
    canvas.cropper('getCroppedCanvas',{ fillColor: '#FFFFFF' }).toBlob(function (blob) {
                                                    var formData = new FormData();

                                                    formData.append('file', blob);

                                                    $.ajax(url, {
                                                      method: "POST",
                                                      data: formData,
                                                       enctype: 'multipart/form-data',
                                                      processData: false,
                                                      contentType: false,
                                                      success: function () {
                                                       location.reload();
                                                        console.log('Upload success');
                                                      },
                                                      error: function () {
                                                        console.log('Upload error', e);
                                                      }
                                                    });
                                                  });
//    $.ajax({
//        url: url,
//        type: "POST",
//        data: new FormData($("#image-upload-form")[0]),
//        enctype: 'multipart/form-data',
//        processData: false,
//        contentType: false,
//        cache: false,
//        success: function () {
//            // Handle upload success
//            location.reload();
//        },
//        error: function (e) {
//            // Handle upload error
//            console.log(e);
//        }
//    });
} // function uploadFile