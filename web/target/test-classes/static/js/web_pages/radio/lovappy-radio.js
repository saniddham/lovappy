var radio;
var stations;
var timer;
var userInfo;
var genderImg;
var playBtn;
var currentIndex = 0;
var lastIndex = 0;
//var onAirIcon;
var progress;
var duration;
var blockBtn;
var likeBtn;
var currentPlayUserID;
var loadMoreBtn;
var pageNumber = 1;
var pageSize = 18;
var useFilter = false;
var excludedAdsIds = [];
var index = 0;
var forcePlayAd = false;
var genderFilter = 'BOTH';
var autoPlay = false;
var userPaused = false;
//Ads audio sounds
var audioSoundId;
var audioSound;
var audioPrevUrl;
$(document).ready(function() {

    $('#clear-form').on('click', function() {
        $('#filter-form').find('input:text, input:password, select, textarea, hidden').val('');
        $('#filter-form').find('input:radio, input:checkbox').prop('checked', false);
         $(".option-slice").remove();
    });

    var player = videojs('video');
    var ModalDialog = videojs.getComponent('ModalDialog');

    var modal = new ModalDialog(player, {

        // We don't want this modal to go away when it closes.
        temporary: true
    });

    player.addChild(modal);
    var videoAdClosed = false;
    player.on('pause', function() {

        if (videoAdClosed) {
            $('#privateVideoModel').modal('hide');
            if (autoPlay) {
                radio.play();
            }
        }

    });

    player.on('play', function() {
        forcePlayAd = true;
        if (autoPlay) {
            radio.pause();
        }
    });
    player.on('ended', function() {
        forcePlayAd = false;
        $('#privateVideoModel').modal('hide');
        if (autoPlay) {
            radio.play();
        }
    });

    $("#videoClose").on("click", function() {
        forcePlayAd = false;
        videoAdClosed = true;
        video.pause();
    });
    index = 0;
    currentIndex = 0; // current sound index
    timer = $("#timer");
    userInfo = document.getElementById("playing-user-info");
    genderImg = $("#playing-user-gender");
    playBtn = $("#playBtn");
    //onAirIcon = $(".on-air");
    progress = $("#progress");
    duration = $("#duration");
    blockBtn = $("#block-btn");
    likeBtn = $("#like-btn");
    loadMoreBtn = $("#load-more-btn");

    var videoPlayer;

    var elementA = document.createElement('script');
    elementA.type = 'text/javascript';
    elementA.async = true;
    elementA.defer = true;
    elementA.src = google_url;
    var elementScript = document.getElementsByTagName('script')[0];
    elementScript.parentNode.insertBefore(elementA, elementScript);

    $('#languages-select').dropdown();

    var surveyAnswerStatus = $('#surveyAnswerStatus');

    if (surveyAnswerStatus.length && !$('#hiddenSurveyPage').length && surveyAnswerStatus.val() !== "1" && loggedIn)
        $('#userSurveyModal').modal('show');

    //Gender Filter (Logout filter)
    $('.mini-con.clearfix .filter-by ul li').on("click", function() {
        $('.mini-con.clearfix .filter-by ul li').removeClass('active');
        $(this).addClass('active');

    });
    //Gender Filter (Logout filter)
    $('.filterByGender').on("click", function() {
        genderFilter = $(this).attr("data-gender");
        pageNumber = 1;

        radio.pause();
        radio.select(0);
        stopAll();
        index = 0;
        lastIndex = 0;
        currentIndex = 0;
        searchLovstamps(true);
    });
    //region Filter options selectors
    $('.filter-checkbox input[type=checkbox]').on("change", function() {
        if ($(this).is(':checked')) {
            $('.filter-options').append(
                '<span class="option-slice" data-text="' + $(this).data('text') + '">' + $(this).data('text') + ' <span class="filter-close">x</span></span>'
            );
        } else {
            $(".option-slice[data-text='" + $(this).data('text') + "']").remove();
        }
    });

    $('.filter-area select').on("change", function() {
        $(".option-slice[data-text='" + $(this).data('text') + "']").remove();
        if ($(this).val() != "") {
            var value = $(this).data('text') + ': ' + $(this).children('option:selected').text();
            $('.filter-options').append(
                '<span class="option-slice" data-text="' + $(this).data('text') + '">' + value + '</span>'
            );
        }
    });

    $('.filter-options').on("click", ".option-slice", function() {
        $(".filter-checkbox input[data-text='" + $(this).data('text') + "']").click();
        $(".filter-area select[data-text='" + $(this).data('text') + "']").val("");
        $(this).remove();
    });
    //endregion

    var loadPrePlaySound = true;

    radio = new jPlayerPlaylist({
        jPlayer: "#radioPlayer",
        cssSelectorAncestor: "#jp_container_N"
    }, [

    ], {
        playlistOptions: {
            enableRemoveControls: true
        },
        supplied: "webmv, ogv, m4v, oga, mp3",
        useStateClassSkin: true,
        autoBlur: false,
        smoothPlayBar: true,
        keyEnabled: true,
        audioFullScreen: true
    });

    // Setup radio and pass it the playlist.

    $(document).on($.jPlayer.event.pause, radio.cssSelector.jPlayer, function() {
        if ($("#radioPlayer").data("jPlayer").status.media.type.startsWith("FEATURED")) {
            $("#FEATUREDAD" + currentIndex).addClass("fa-play-circle-o").removeClass("fa-pause-circle-o");
            if (!userPaused && !forcePlayAd)
                currentIndex++;

            $("#radioPlayer").css("z-index", "20");
            $("#radioPlayer").css("display", "none");
        } else if ($("#radioPlayer").data("jPlayer").status.media.type.startsWith("STAMP")) {
            $("#LOVSTAMP" + currentIndex).addClass("fa-play").removeClass("fa-pause");
//            $(".radio-view-profile").hide();
            if (!userPaused &&!forcePlayAd)
                currentIndex++;
        }

    });
//    $(document).on($.jPlayer.event.stop, radio.cssSelector.jPlayer, function() {
//        if ($("#radioPlayer").data("jPlayer").status.media.type.startsWith("FEATURED")) {
//            $("#FEATUREDAD" + currentIndex).addClass("fa-play-circle-o").removeClass("fa-pause-circle-o");
//            if (!forcePlayAd)
//                currentIndex++;
//            $("#radioPlayer").css("z-index", "20");
//            $("#radioPlayer").css("display", "none");
//        } else if ($("#radioPlayer").data("jPlayer").status.media.type.startsWith("STAMP")) {
//            $("#LOVSTAMP" + currentIndex).addClass("fa-play").removeClass("fa-pause");
//            $(".radio-view-profile").hide();
//            if (!forcePlayAd)
//                currentIndex++;
//        }
//
//    });

    $(document).on($.jPlayer.event.play, radio.cssSelector.jPlayer, function(event) {
        if ($("#radioPlayer").data("jPlayer").status.media.type.startsWith("FEATURED")) {
            $("#FEATUREDAD" + currentIndex).addClass("fa-pause-circle-o").removeClass("fa-play-circle-o");
            $("#playBtn").addClass("fa-pause").removeClass("fa-play");
            $(".radio-view-profile").hide();
            if ($("#radioPlayer").data("jPlayer").status.media.isVideo) {
                $("#radioPlayer").css("z-index", "30");
                $("#radioPlayer").css("display", "block");
            }
        } else if ($("#radioPlayer").data("jPlayer").status.media.type.startsWith("STAMP")) {
            $("#LOVSTAMP" + currentIndex).addClass("fa-pause").removeClass("fa-play");
            $("#playBtn").addClass("fa-pause").removeClass("fa-play");
            showUserDataOnAir($("#radioPlayer").data("jPlayer").status.media);

        }
userPaused = false;

        if (currentIndex === (lastIndex - 1)) {
            searchLovstamps(false);

        }
    });



    $(document).on($.jPlayer.event.timeupdate, radio.cssSelector.jPlayer, function(event) {
       if ($("#radioPlayer").data("jPlayer").status.media.type && ($("#radioPlayer").data("jPlayer").status.media.type.startsWith("FEATURED") || $("#radioPlayer").data("jPlayer").status.media.type.startsWith("STAMP")) ) {
        timer.html(formatTime($("#radioPlayer").data("jPlayer").status.currentTime));
        progress.width(event.jPlayer.status.currentPercentAbsolute + "%");
         }
    });

    $(document).on($.jPlayer.event.loadedmetadata, radio.cssSelector.jPlayer, function(event) {
       if ($("#radioPlayer").data("jPlayer").status.media.type && ($("#radioPlayer").data("jPlayer").status.media.type.startsWith("FEATURED") || $("#radioPlayer").data("jPlayer").status.media.type.startsWith("STAMP")) ) {
        duration.html(formatTime($("#radioPlayer").data("jPlayer").status.duration));
        }
    });
    /* Modern Seeking */

    var timeDrag = false; /* Drag status */
    progress.mousedown(function(e) {
        timeDrag = true;
        updatebar(e.pageX);
    });
    $(document).mouseup(function(e) {
        if (timeDrag) {
            timeDrag = false;
            updatebar(e.pageX);
        }
    });
    $(document).mousemove(function(e) {
        if (timeDrag) {
            updatebar(e.pageX);
        }
    });

    //update Progress Bar control
    var updatebar = function(x) {

        var progress = $('.progress-bg');
        var maxduration = $("#radioPlayer").jPlayer.duration; //audio duration
        console.log(maxduration);
        var position = x - progress.offset().left; //Click pos
        var percentage = 100 * position / progress.width();

        //Check within range
        if (percentage > 100) {
            percentage = 100;
        }
        if (percentage < 0) {
            percentage = 0;
        }

        $("#radioPlayer").jPlayer("playHead", percentage);

        //Update progress bar and video currenttime
        $('.play-progress').css('width', percentage + '%');
        $("#radioPlayer").jPlayer.currentTime = maxduration * percentage / 100;
    };


    //  var radio = new Radio(
    searchLovstamps(true);



    $("#backBtn").click(function() {
        radio.previous();
        radio.previous();
        stopAll();
        currentIndex--;
    });

    playBtn.click(function() {
        if (audioSound) {
            if (audioSound.playing()) {

                audioSoundId = audioSound.stop();
            }
        }
        if ($('#radioPlayer').data().jPlayer.status.paused) {
            $(this).addClass("fa-pause").removeClass("fa-play");
            autoPlay = true;
//                userPaused=false;
            radio.play();

        } else {
            autoPlay = false;
            userPaused=true;
            radio.pause();
            stopAll();
        }
    });

    $("#nextBtn").click(function() {
        radio.next();
        stopAll();
        currentIndex++;
    });

    $('.lovdrop-actions').find('.block-lovdrop').on('click', function() {
        if (!loggedIn) {
            window.location = baseUrl + "login";
            return;
        }
        var action = $(this).data('action');
        if (action === 'block') {
            $(".modal[data-edit='block-modal']").attr("userId", $(this).data('userid'));
            $(".modal[data-edit='block-modal']").modal("show");
        } else {
            userBlockUnblock($(this).data('userid'));
        }
    });

    blockBtn.click(function() {
        if (!loggedIn) {
            window.location = baseUrl + "login";
            return;
        }
        var action = $(this).data('action');
        if (action === 'block') {
            $(".modal[data-edit='block-modal']").attr("userId", $(this).data('userid'));
            $(".modal[data-edit='block-modal']").modal("show");
        } else {
            userBlockUnblock($(this).data('userid'));
        }
    });

    $('#modalBlockBtn').click(function() {
        if (!loggedIn) {
            window.location = baseUrl + "login";
            return;
        }
        userBlockUnblock($(".modal[data-edit='block-modal']").attr("userId"));

    });

    $("#flagUserBtn").click(function() {
        $(".modal[data-edit='flag-modal']").attr("userId", $(".modal[data-edit='block-modal']").attr("userId"));
        $(".modal[data-edit='flag-modal']").modal("show");

    });

    $('#submit-flag').click(function(e) {
        e.preventDefault();
        var userId = $(".modal[data-edit='flag-modal']").attr("userId");

        var blockImg = baseUrl + 'images/icons/lovdrop-block.png';
        var unblockImg = baseUrl + 'images/icons/lovdrop-unblock.png';
        var actionBtnInBox = $("img[data-userid='" + userId + "'].block-lovdrop");
        var action = actionBtnInBox.data('action');
        $('#flagForm').ajaxSubmit({
            type: 'POST',
            crossDomain: true,
            headers: {
                Accept: "application/json; charset=utf-8"
            },

            url: baseUrl + 'member/flag/' + userId,

            success: function() {

                if (action === 'block') {

                    //If track is playing, skip to the next one
                    //                    if (player.isPlaying()) {
                    //                        player.stop();
                    //                        player.skip("next");
                    //                    }

                    actionBtnInBox.attr('src', unblockImg);
                    actionBtnInBox.data('action', 'unblock');
                    actionBtnInBox.attr('title', unblockText);
                    actionBtnInBox.closest('.lovdrop-container').addClass('disabled-lovdrop');

                    //Update bar only if playing for that user
                    if (currentPlayUserID === userId) {
                        blockBtn.attr('src', unblockImg);
                        blockBtn.attr('title', unblockText);
                        blockBtn.data('action', 'unblock');
                    }

                    changeIsBlocked(userId, true);
                    $(".modal[data-edit='flag-modal']").modal("hide");
                }
            },
            error: function(e) {
                console.log(e);
            }




        });

    });

    likeBtn.click(function() {
        userLikeUnlike($(this));
    });

    $('#share-btn-email').click(function() {
        $(".modal[data-edit='share-email-radio']").attr("shareUrl", $(this).attr("shareUrl"));
        $(".modal[data-edit='share-email-radio']").modal("show");
    });

    $('#emailShareBtn').click(function() {
        var shareUrl = $(".modal[data-edit='share-email-radio']").attr("shareUrl");
        shareViaEmail(shareUrl);
    });

    $("input.numeric").numeric();

    $('.filter-btn').click(function() {
        $('.filter-area').slideToggle("50");
    });



    $("#filter-submit").click(function(e) {
        e.preventDefault();
        pageNumber = 1;
        useFilter = true;
        excludedAdsIds = [];
        searchLovstamps(true);
    });

    $("#show-map-btn").click(function() {
        $('#location-modal').modal('setting', 'closable', false).modal('show');
        google.maps.event.trigger(map, "resize");
        map.setCenter(new google.maps.LatLng($('#latitude').val(), $('#longitude').val()));
    });

    $("#set-location-btn").on('click', function() {
        var locationStr =
            $("#country").val() + ', ' + $("#state").val() + ', ' + $("#city").val();
        $("#filter-location-input").val(locationStr);

        $("#latitude-hidden").val($('#latitude').val());
        $("#longitude-hidden").val($('#longitude').val());
        $("#radius-hidden").val($('#last-mile-radius-search').val());
    });

    loadMoreBtn.click(function() {
        pageSize = 18;
        searchLovstamps(false);
    });

    $(".radio-skip").on("click", function() {
        radio.next();
    });
    $.LoadingOverlay("hide", true);
});



function shareViaEmail(shareUrl) {

    $("#shareUrl").attr("value", shareUrl);
    $("#shareEmailForm").ajaxSubmit({
        type: 'GET',
        async: false,
        crossDomain: true,
        url: baseUrl + "radio/share/email",
        success: function() {
            $(".modal[data-edit='share-email-radio']").modal("hide");
        },
        error: function(e) {
            console.log(e);
        }


    });

}

function searchLovstamps(clearResults) {
    if (clearResults){
        $("#lovdrop-list").html("");

        radio.setPlaylist([
             ]);
        }

    var formData = null;
    if (loggedIn) {
        $('.filter-area').slideUp("50");

        formData = $("#filter-form").serializeArray();
        formData.push({
            name: "use_filter",
            value: useFilter
        });

    } else {
        //    formData = new FormData();

        $('#filter-form').each(function() {
            $(this).remove();
        })
        formData = $("#filter-form").serializeArray();
        formData.push({
            name: "gender",
            value: genderFilter
        });


        //    formData.append('page_number', pageNumber);
        //    formData.append('page_size', pageSize);
    }

    formData.push({
        name: "page",
        value: pageNumber
    });
    formData.push({
        name: "limit",
        value: pageSize
    });

    getRadioAPI(formData);
}

function getRadioAPI(formData) {
    $.ajax({
        url: baseUrl + "api/v1/radio",
        type: "GET",
        data: formData,
        success: function(response) {
            var data = response.radioPage;
            if (response.showMessage  ) {
                $("#errorResultMessage").show();
            } else {
                $("#errorResultMessage").hide();
            }
            appendLovstamps(data.content);
            loadLovstamps(data.content);
            pageNumber++;

            if (data.last) {
                lastIndex = -1;
                playBtn.addClass("fa-play").removeClass("fa-pause");
                loadMoreBtn.hide();
            } else {
                loadMoreBtn.show();
            }
        },
        error: function(e) {
            console.log(e);
        }
    });
}

function appendLovstamps(data) {
    if (data.length > 0) {
        lastIndex += data.length;
        for (var i = 0; i < data.length; i++) {

            var html = "";

            var userID = data[i].userId;
            var isBlocked = blockedUsersIDs.indexOf(userID) > -1;
            var isLiked = likedUserIds.indexOf(userID) > -1;
            var blockAction = isBlocked ? "unblock" : "block";
            var blockTitle = isBlocked ? unblockText : blockText;
            var likeAction = isLiked ? "unlike" : "like";
            var likeTitle = isLiked ? unLikeText : likeText;
            var blockIcon = baseUrl + (isBlocked ? "/images/icons/lovdrop-unblock.png" : "/images/icons/lovdrop-block.png");
            var likeClass = (isLiked ? "fa-heart" : "fa-heart-o");
            var grayFilterClass = isBlocked ? "disabled-lovdrop" : "";
            var genderImage = baseUrl + (data[i].gender === "FEMALE" ? "images/new-female-icon.png" : "images/new-male-icon.png");
            var viewProfileClass = data[i].gender === "FEMALE" ? "view-female-profile" : "view-male-profile";
            var state = data[i].address;
            var stateSub = (state !== null && state.length > 4) ? state.substring(0, 4) : state;
            var adTypeHtml = '';
            var adType = '';
            var adBackgroundStyle = '';
            var adUrl = '';
            var adTextHtml = '';
            var adTitleHtml = '';
            var adTextStyle = '';
            var playAdButton = '';
            var featuredAd = false;

            if ((data[i].type.startsWith("PRIVATE") || data[i].type.startsWith("FEATURED")) && data[i].extraInfo !== null && data[i].extraInfo !== undefined) {
                featuredAd = data[i].type.startsWith("FEATURED");
                adUrl = data[i].extraInfo.url;
                var textAlignStyle = "top: 50%;transform: translateX(-50%) translateY(-50%);"
                if( data[i].extraInfo.textAlign === "BUTTOM") {
                    textAlignStyle = "top: 95%;transform: translateX(-50%) translateY(-95%);"
                } else  if( data[i].extraInfo.textAlign === "TOP") {
                     textAlignStyle = "top: 5%;transform: translateX(-50%) translateY(-5%);"
                }
              adTitleHtml = data[i].extraInfo.title === null ? '' : '<p  style="' + 'color:#ffffff; top: 10%;transform: translateX(-50%) translateY(-10%);"><b>' + data[i].extraInfo.title + '</b></p>';
               adTextStyle =  'color:' + data[i].extraInfo.textColor + '; text-decoration: none;';

                adTextStyle +=  textAlignStyle;
              adTextHtml = data[i].extraInfo.text === null ? '' : '<p style="' + adTextStyle + '">' + data[i].extraInfo.text + '</p>';

              adBackgroundStyle = jQuery.isEmptyObject(data[i].coverImageUrl) ?
                    'background:' + data[i].backgroundColor + ';' :
                    'background:url(' + data[i].coverImageUrl + ');'


                if (data[i].extraInfo.mediaType === "AUDIO") {
                    data[i].index = index;
                    playAdButton = '<i id="PRIVATE_' + index + '"  data-url=' + data[i].extraInfo.mediaFileUrl +
                        ' class="audioPlay fa fa-play-circle-o" aria-hidden="true" onClick="playAudioAd(' + index + ')" ></i>';

                } else if (data[i].extraInfo.mediaType === "VIDEO") {
                    playAdButton = '<i id="VIDEO_PRI_' + index + '"  data-url=' + data[i].extraInfo.mediaFileUrl +
                        ' class="fa fa-play-circle-o" aria-hidden="true" onClick="playPrivateVideoAd(' + index + ')"  " ></i>';
                }
            }

            if (featuredAd) {
                playFeaturedAdButton = '<i id="FEATUREDAD' + index + '" ' +
                    'class="fa fa-play-circle-o" aria-hidden="true" onClick="playFeaturedAd(' + index + ')"></i>';
                html +=
                    '<div class="ads box-c">' +

                    '       <div class="box-img" style="' + adBackgroundStyle + '">' +
                    '   <a href="' + adUrl + '" target="_blank">' +
                    '           ' + adTextHtml +
                    '           ' + adTypeHtml +
                    '   </a>' +
                    '           ' + playFeaturedAdButton +
                    '       </div>' +

                    '<div class="radio-skip" onClick="skipTo(' + index + ')">'+skipText+'</div></div>';
            } else if (data[i].type.startsWith("PRIVATE")) {
                html +=
                    '<div class="box box-c">' +

                    '       <div class="box-img" style="' + adBackgroundStyle + '">' ;
                    if(adUrl === null)
                      html += '   <a>' ;
                    else
                      html += '   <a href="' + adUrl + '" target="_blank">' ;
                     html += '           ' + adTitleHtml +
                    '           ' + adTextHtml +
                    '           ' + adTypeHtml +
                    '   </a>' +
                    '           ' + playAdButton +
                    '       </div>' +

                    getUserInfoHtml(data[i], userID, genderImage, likeClass, blockIcon, likeTitle, blockTitle,
                        likeAction, blockAction, stateSub, index) +
                    '</div>';
            } else if (data[i].type.startsWith("PUBLIC")) {
                html +=
                    '<div class="box box-c">' +
                    '   <a href="javascript:void(0)" >' +
                    '       <div class="box-img" style="background:url(' + data[i].coverImageUrl + ');">' +

                    '       </div>' +
                    '   </a>' +
                    getUserInfoHtml(data[i], userID, genderImage, likeClass, blockIcon, likeTitle, blockTitle,
                        likeAction, blockAction, stateSub, index) +
                    '</div>';
            }


            $("#lovdrop-list").append(html);
            index++;
        }
    }
}

function getUserInfoHtml(data, userID, genderImage, likeClass, blockIcon, likeTitle, blockTitle, likeAction, blockAction,
    stateSub, index) {
    return '   <div class="main-content ' + isAtrractiveWhiteBorder(data) + '">' +
        getNewVoiceIcon(data.updated) +
        '       <div class="user-status">' +
        '           <img src="' + genderImage + '"/>' +
        '           <a href="#"><span>'+ ageText +'</span>' + (data.age>0?data.age:"") + '</a>' +
        '           <a href="#"><span>'+ locationText +'</span>' + stateSub + '</a>' +
        '           <a href="#"><span>'+ languageText +'</span>' + data.language + '</a>' +
        '       </div>' +
        '       <div class="view-profile">' +
        '           <a href="' + baseUrl + 'member/' + userID + '">'+ viewProfileText +'</a>' +
        '       </div>' +
        '       <div class="lovdrop-actions">' +
        '           <i class="fa '+ likeClass +' like-lovdrop" title="' + likeTitle + '" ' + 'data-userid="' + userID + '" data-action="' + likeAction + '" ' + ' onclick="userLikeUnlike(' + userID + ')"></i>' +
        '           <i class="fa fa-ban block-lovdrop" title="' + blockTitle + '" ' + ' data-userid="' + userID + '" data-action="' + blockAction + '"' + '                onclick="userBlockUnblock(' + userID + ')"></i>' +
        '           <i class="fa fa-share-alt share-lovdrop-list" ' + ' data-userid="' + userID + '" onclick="showShareModal(' + userID + ')"></i>' +
        '       </div>' +
        '       <div class="radio-play-skip"><div class="radio-play">' +
        '           <i id="LOVSTAMP' + index + '" onClick="playLovstamp(' + index + ')" data-index="' + index + '" class="lovstampPlay fa fa-play" aria-hidden="true"></i>' +
        '           <span>' + data.formatedTime + '</span>' +
        '       </div><div class="radio-skip" onClick="skipTo(' + index + ')">'+ skipText +'</div></div>' +
        '       <div class="user-id"><span>User ID</span>' +
        '           ' + userID +
        '           <img src="' + baseUrl + getVerfiedImage(data.verified) + '" class="block-lovdrop"/>' +
        '       </div>' +
        getAtrractiveStarHtml(data);
}
var video = document.getElementById('video');
var source = document.getElementById('videoMP4');
var source2 = document.getElementById('videowebm');

function playPrivateVideoAd(selectorId) {

    if ($('#radioPlayer').data().jPlayer.status.play) {
        radio.pause();
    }
    playVideoButton = $("#VIDEO_PRI_" + selectorId);
    var url = playVideoButton.attr("data-url");

    //video.pause()
    source.setAttribute('src', url);

    $('#privateVideoModel').modal({
        beforeclose: function() {
            return false
        },
        closable: false
    }).modal('show');
    video.load();
    if (fetchVideoAndPlay(url))
        video.play();


}

function fetchVideoAndPlay(url) {
    fetch(url)
        .then(response => response.blob())
        .then(blob => {
            video.srcObject = blob;
            return true;
        })
        .then(_ => {
            video.play();
        })
        .catch(e => {
            return false;
        })
}

function playAudioAd(selectorId) {

    playAudioButton = $("#PRIVATE_" + selectorId);

    var url = playAudioButton.attr("data-url");

    if (audioSound) {
        if (audioSound.playing()) {


            audioSound.stop(audioSoundId);

            if (audioPrevUrl === url) {
                return;
            }
        } else {

        }

    }
    audioPrevUrl = url;
    audioSound = new Howl({
        src: [url],
        html5: true, // Force to HTML5 so that the audio can stream in (best for large files).
        onplay: function() {
            forcePlayAd = true;
            playAudioButton.addClass('fa-pause-circle-o').removeClass('fa-play-circle-o');
            radio.pause();
        },
        onload: function() {
            $(".audioPlay").addClass('fa-play-circle-o').removeClass('fa-pause-circle-o');


        },
        onend: function() {
            forcePlayAd = false;
            $(".audioPlay").addClass('fa-play-circle-o').removeClass('fa-pause-circle-o');
            if (autoPlay)
                radio.play();

        },
        onpause: function() {
            forcePlayAd = false;
            $(".audioPlay").addClass('fa-play-circle-o').removeClass('fa-pause-circle-o');

        },
        onstop: function() {
            forcePlayAd = false;
            $(".audioPlay").addClass('fa-play-circle-o').removeClass('fa-pause-circle-o');
            if (autoPlay)
                radio.play();
        }
    });

    if (audioSound.playing()) {

        audioSoundId = audioSound.stop();
    } else {

        audioSoundId = audioSound.play();

    }

    //End Block Section

}

function skipTo(index) {
    if (index === currentIndex) {
     $("#radioPlayer").css("z-index", "20");
        radio.next();
        stopAll();
        currentIndex++;
    }
}

function playLovstamp(index) {

    if (audioSound) {
        if (audioSound.playing()) {

            audioSoundId = audioSound.stop();
        }
    }

    if ($('#radioPlayer').data().jPlayer.status.paused) {
        $("#LOVSTAMP" + index).addClass("fa-pause").removeClass("fa-play");
        autoPlay = true;
        if(currentIndex === index){
        radio.play();
        }else{
        radio.play(index * 2);
        currentIndex = index;
        }
        userPaused = false;
    } else {
        autoPlay = false;
        userPaused = true;
        radio.pause();
        stopAll();
    }
}

function playFeaturedAd(index) {
    if (audioSound) {
        if (audioSound.playing()) {

            audioSoundId = audioSound.stop();
        }
    }

    if ($('#radioPlayer').data().jPlayer.status.paused) {
        $("#FEATUREDAD" + index).addClass("fa-pause-circle-o").removeClass("fa-play-circle-o");
        autoPlay = true;
         userPaused = false;
         if(currentIndex === index){
                radio.play();
                }else{
                radio.play(index * 2);
                currentIndex = index ;
                }

    } else {
     userPaused = true;
        autoPlay = false;
        radio.pause();
        stopAll();
    }
}

function getVerfiedImage(isVerified) {
    if (isVerified)
        return "images/user-verified.png";

    return "images/alert-icon-image-gallery-27.png"
}

function getNewVoiceIcon(newOrUpdated) {
    if (newOrUpdated)
        return '<div class="radio-span"><span>New Recording</span><img src="' + baseUrl + 'images//updated-icon.png" class="radio-icon"/></div>';

    return '';
}

function isAtrractiveWhiteBorder(data) {
    if (data.attractive)
        return "active";

    return ""
}

function getAtrractiveStarHtml(data) {
    if (data.attractive)
        return '<div class="radio-star"><i class="fa fa-star"></i></div></div>';

    return ""
}

function loadLovstamps(radioJson) {
    for (var i = 0; i < radioJson.length; i++) {
        var file = radioJson[i].fileUrl;
        var isBlocked = blockedUsersIDs.indexOf(radioJson[i].userId) !== -1;

        if (radioJson[i].type !== null && (radioJson[i].type === 'FEATURED_AUDIO' ||
                radioJson[i].type === 'FEATURED_VIDEO')) {
            radio.add({

                type: "START",
                title: "",
                artist: "",
                mp3: radioJson[i].startSound.audioFileCloud.url,
                webmv: radioJson[i].startSound.audioFileCloud.url,
                poster: ""
            });
            radio.add({

                type: "FEATURED",
                isAd: true,
                isVideo: radioJson[i].type === "FEATURED_VIDEO",
                id: radioJson[i].audioMediaType + radioJson[i].id,
                file: radioJson[i].fileUrl,
                user_age: "",
                user_state: "",
                user_city: "",
                user_language: "",
                lovdrop_language: "",
                user_gender: "",
                user_id: "",
                is_blocked: false,
                title: "",
                artist: "",
                m4v: radioJson[i].fileUrl,
                ogv: radioJson[i].fileUrl,
                mp3: radioJson[i].fileUrl,
                webmv: radioJson[i].fileUrl,
                poster: ""
            });

            excludedAdsIds.push(radioJson[i].id);
        } else if (file !== undefined) {
            radio.add({

                type: "START",
                title: "",
                artist: "",
                mp3: radioJson[i].startSound.audioFileCloud.url,
                webmv: radioJson[i].startSound.audioFileCloud.url,
                poster: ""
            });

            radio.add({

                isAd: false,
                type: "STAMP",
                isVideo: false,
                id: "LOVStAMP" + radioJson[i].id,
                file: file,
                howl: null,
                user_age: radioJson[i].age,
                user_state: radioJson[i].address,
                user_city: radioJson[i].address,
                user_language: radioJson[i].language,
                lovdrop_language: radioJson[i].language,
                user_gender: radioJson[i].gender,
                user_id: radioJson[i].userId,
                is_blocked: isBlocked,
                title: "",
                artist: "",
                m4v: radioJson[i].fileUrl,
                ogv: radioJson[i].fileUrl,
                mp3: radioJson[i].fileUrl,
                webmv: radioJson[i].fileUrl,
                poster: ""
            });
        }

    }
}

function openUrl(url) {
    if (url !== 'null') {
        var win = window.open(url, '_blank');
        win.focus();
    }
}

function gotolike() {
    window.location.href = '/like/mylikes';
}

function stopAll() {
    $(".fa-pause").each(function() {
        $(this).addClass("fa-play").removeClass("fa-pause");
        //onAirIcon.addClass("grayscale-img");
    });

    $(".fa-pause-circle-o").each(function() {
        $(this).addClass("fa-play-circle-o").removeClass("fa-pause-circle-o");
    });
}

function userBlockUnblock(userID) {
    if (!loggedIn) {
        window.location = baseUrl + "login";
        return;
    }

    var blockImg = baseUrl + 'images/icons/lovdrop-block.png';
    var unblockImg = baseUrl + 'images/icons/lovdrop-unblock.png';
    var actionBtnInBox = $("img[data-userid='" + userID + "'].block-lovdrop");
    var action = actionBtnInBox.data('action');
    //var action = $('.block-lovdrop[data-userid="+userID+"]').data('action');
    $.ajax({
        url: baseUrl + "member/" + action,
        type: "POST",
        data: {
            userId: userID
        },
        success: function() {

            if (action === 'block') {

                //If track is playing, skip to the next one
                //                if (player.isPlaying()) {
                //                    player.stop();
                //                    player.skip("next");
                //                }

                actionBtnInBox.attr('src', unblockImg);
                actionBtnInBox.data('action', 'unblock');
                actionBtnInBox.attr('title', unblockText);
                actionBtnInBox.closest('.lovdrop-container').addClass('disabled-lovdrop');

                //Update bar only if playing for that user
                if (currentPlayUserID === userID) {
                    blockBtn.attr('src', unblockImg);
                    blockBtn.attr('title', unblockText);
                    blockBtn.data('action', 'unblock');
                }

                changeIsBlocked(userID, true);
                $(".modal[data-edit='block-modal']").modal("hide");
            } else {
                actionBtnInBox.attr('src', blockImg);
                actionBtnInBox.data('action', 'block');
                actionBtnInBox.attr('title', blockText);
                actionBtnInBox.closest('.lovdrop-container').removeClass('disabled-lovdrop');

                //Update bar only if playing for that user
                if (currentPlayUserID === userID) {
                    blockBtn.attr('src', blockImg);
                    blockBtn.attr('title', blockText);
                    blockBtn.data('action', 'block');
                }

                changeIsBlocked(userID, false);
            }
        },
        error: function(e) {
            console.log(e);
        }
    });
}

function userLikeUnlike(userID) {
    if (!loggedIn) {
        window.location = baseUrl + "login";
        return;
    }

    var actionBtnInBox = $("i[data-userid='" + userID + "'].like-lovdrop");
    var action = actionBtnInBox.data('action');
    $.ajax({
        url: baseUrl + "/like/" + userID,
        type: action === 'like' ? "POST" : "DELETE",
        data: {
            userId: userID
        },
        success: function() {

            if (action === 'like') {
                actionBtnInBox.removeClass("fa-heart-o").addClass("fa-heart");
                actionBtnInBox.data('action', 'unlike');
                actionBtnInBox.attr('title', unLikeText);

                //Update bar only if playing for that user
                if (currentPlayUserID === userID) {
                    likeBtn.removeClass("fa-heart-o").addClass("fa-heart");
                    likeBtn.attr('title', unLikeText);
                    likeBtn.data('action', 'unlike');
                }
                $(".modal[data-edit='like-modal']").modal("show");

            } else {
                actionBtnInBox.removeClass("fa-heart").addClass("fa-heart-o");
                actionBtnInBox.data('action', 'like');
                actionBtnInBox.attr('title', likeText);

                //Update bar only if playing for that user
                if (currentPlayUserID === userID) {
                    likeBtn.removeClass("fa-heart").addClass("fa-heart-o");
                    likeBtn.attr('title', likeText);
                    likeBtn.data('action', 'like');
                }
            }
        },
        error: function(e) {
            console.log(e);
        }
    });
}

function changeIsBlocked(userID, isBlocked) {
    for (var i in stations) {
        if (stations[i].user_id === userID) {
            stations[i].is_blocked = isBlocked;
            break;
        }
    }
}

function dontShowSurvey() {
    $.ajax({
        url: baseUrl + "registration/survey/dontShow",
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        data: null,
        success: function(data) {
            $('#userSurveyModal .survey-content').html("<h2>This message won't appear again</h2>")
            $('#userSurveyModal').modal('show');
        },
        error: function(data) {
            console.log(data);
        }
    });
}

function showShareModal(userID) {
    $(".modal[data-edit='share-drop']").modal("show");
    $("#share-btn-facebook").attr("href", "http://www.facebook.com/sharer.php?u=" + shareBaseUrl + "/member/" + userID);
    $("#share-btn-twitter").attr("href", "https://twitter.com/share?url=" + shareBaseUrl + "/member/" + userID);
    $("#share-btn-google").attr("href", "https://plus.google.com/share?url=" + shareBaseUrl + "/member/" + userID);
    $("#share-btn-tumblr").attr("href", "https://www.tumblr.com/widgets/share/tool?canonicalUrl=" + shareBaseUrl + "/member/" + userID);
    $("#share-btn-pinterest").attr("href", "https://pinterest.com/pin/create/bookmarklet/?url=" + shareBaseUrl + "/member/" + userID);
    $("#share-btn-reddit").attr("href", "https://reddit.com/submit?url=" + shareBaseUrl + "/member/" + userID);
    $("#share-btn-email").attr("shareUrl", shareBaseUrl + "/member/" + userID);
}

function showUserDataOnAir(data) {
    if (data.is_blocked) {
        radio.next();
    }

    // Update the track display.
    if (!data.isAd) {
        $("#user-info-img").attr("src", data.user_gender === 'FEMALE' ?
            baseUrl + "images/new-female-icon.png" : baseUrl + "images/new-male-icon.png");
        $("#user-info-age").html(data.user_age);
        $("#user-info-state").html(data.user_state);
        $("#user-info-language").html(data.lovdrop_language);
        $("#view-profile-link").attr("href", baseUrl + "member/" + data.user_id);

        $(".radio-view-profile").show();

        genderImg.attr("src", data.user_gender === 'FEMALE' ?
            baseUrl + "images/new-female-icon.png" : baseUrl + "images/new-male-icon.png");

        var actionBtnInBox = $("img[data-userid='" + data.user_id + "'].block-lovdrop");
        actionBtnInBox.data('userid', data.user_id);
        actionBtnInBox.data('action', data.is_blocked ? 'unblock' : 'block');

        blockBtn.data('userid', data.user_id);
        blockBtn.data('action', data.is_blocked ? 'unblock' : 'block');
        blockBtn.attr('src', data.is_blocked ? baseUrl + 'images/icons/lovdrop-unblock.png' : baseUrl + 'images/icons/lovdrop-block.png');
    } else {
        $(".radio-view-profile").hide();
    }
}

function formatTime(secs) {
    var minutes = Math.floor(secs / 60) || 0;
    var seconds = Math.floor((secs - minutes * 60)) || 0;

    return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
}