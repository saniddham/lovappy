var notifyOptions = {
    globalPosition: 'bottom right',
    className: "error",
    autoHide: true
};

var blogImageUpload = true;
var files;

var options = {
    viewMode:1,
    minCropBoxWidth: 482,
    minCropBoxHeight: 237,
    imageSmoothingEnabled: false,
    imageSmoothingQuality: 'high',
    aspectRatio: 482 / 237,
    preview: $('#img2')
};

$(document).ready(function() {

    $('.image-content').on('click', function(e) {
        e.preventDefault();
        options = {
            viewMode:1,
            minCropBoxWidth: 482,
            minCropBoxHeight: 237,
            imageSmoothingEnabled: false,
            imageSmoothingQuality: 'high',
            aspectRatio: 482 / 237,
            preview: $('#img2')
        };
        $('#image').cropper('destroy').cropper(options);
        $('#upload-modal').modal('show');
        blogImageUpload = true;
    });

    $('#author-image').on('click', function(e) {
        e.preventDefault();
        $('#image').cropper('destroy');
        options = {
            minCropBoxWidth: 198,
            minCropBoxHeight: 200,
            imageSmoothingEnabled: false,
            imageSmoothingQuality: 'high',
            aspectRatio: 198 / 200,
            preview: $('#img2')
        };
        $('#image').cropper('destroy').cropper(options);
        $('#upload-modal').modal('show');
        blogImageUpload = false;
    });


    $('#upload-button').click(function() {
        $('#inputImage').click();
    });

    $('#inputImage').change(function() {
        files = $(this)[0].files[0];
    });

    $('.select1').dropdown({
        maxSelections: 3
    });
    $('.circular').popup({
        position: 'right center',
    });
    $('.ppt')
        .dropdown({
            allowAdditions: true
        });

    $("input[type='radio']").change(function() {
        if ($('#anonymous').is(':checked')) {
            $('.unstackable .gray').addClass('disabled');
        } else {
            $('.unstackable .gray').removeClass('disabled');
        }
    });
    var saveDraft = true;
    $("#upload-author-image").on('change', function() {
        readURL(this, $('#authorImage'));
    });

    $("#upload-blog-img").on('change', function() {
        readURL(this, $('#blog-image'));
    });


    $("#txtCustomHtmlArea").htmlarea({
        css: baseUrl + "css/jHtmlArea/jHtmlArea.Editor.css",
        toolbar: ["bold", "italic", "underline", "|", "orderedlist", "unorderedlist", "|", "indent", "outdent", "|",
            "justifyleft", "justifycenter", "justifyright",
            "p", "h1", "h2", "h3", "h4", "h5", "h6"
        ]
    });


    $('#submit-form').on('keyup keypress keydown', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
    $('#submit-form').click(function(e) {
        saveDraft = false;
        e.preventDefault();
        var html = $("#txtCustomHtmlArea").htmlarea("toHtmlString");
        // $('#submitForm').valid();
        if (validateInputs())
            $('#submitForm').submit();

    });

    $('#draftSaveBtn').click(function(e) {
        e.preventDefault();
        saveDraftArticle();
    });

    window.onbeforeunload = function(e) {

        if (saveDraft && !isEdit) {
            saveDraft = false;




            saveDraftArticle();
            //            return 'You have unsaved changes!';
        }

    }

    function saveDraftArticle() {
        $('#draftSaveBtn').text("Saving");
        var html = $("#txtCustomHtmlArea").htmlarea("toHtmlString");
        var title = $("#blog-title").val();
        var category = $("#categorySelector").val();

        if (html == '' && title == '' && category == '')
            return;
        $("#submitForm").ajaxSubmit({
            type: 'POST',
            async: false,
            crossDomain: true,
            headers: {
                Accept: "application/json; charset=utf-8"
            },

            url: baseUrl + "blog/post/draft/save",
            success: function() {

            },
            error: function() {

            }



        });
        $(".modal[data-edit='save-draft-article']").modal("hide");
        $('#draftSaveBtn').text("Save");
    }

});

function validateInputs() {

    var valid = true;
    if ($("#title").val() == '') {
        $.notify(notifTitle, notifyOptions);
        valid = false;
    }

    if ($("#txtCustomHtmlArea").htmlarea("toHtmlString") == '') {
        $.notify(notifPostBody, notifyOptions);
        valid = false;
    }

    if ($("#categorySelector").val() == '') {
        $.notify(notifCategory, notifyOptions);
        valid = false;
    }

    if ($("#seoTitle").val() == '') {
        $.notify(notifSeoTitle, notifyOptions);
        valid = false;
    }

    if ($("#description").val() == '') {
        $.notify(notifMetaDescription, notifyOptions);
        valid = false;
    }
    var keys = $("#keywordsPhrases").val();
    if ($("#keywordsPhrases").val() == '') {
        $.notify(notifKeywordsPhrases, notifyOptions);
        valid = false;
    } else {
    $("#keywordsPhrasesHidden").val(keys)
    }


    return valid;
}

function checkPhrasesLen(obj, wordLen) {
    var parts = obj.value.split(',');
    if (parts.length >= wordLen) {
        $("#keywordsPhrases").attr('disabled', 'disabled');
    }
    if (parts.length > wordLen) {
       // $("#keywordsPhrases").last().remove();
          var keys = $("#keywordsPhrases").val();
          $("#keywordsPhrases").val(parts[0]+","+parts[1]);
           $("#keywordsPhrases").text(parts[0]+","+parts[1]);
           $("#keywordsPhrasesHidden").val(parts[0]+","+parts[1])
        $.notify("You cannot put more than " + wordLen + " phrases in this text area.", notifyOptions);
        return false;
    }
    return true;
}

function checkWordLen(obj, wordLen) {
    var len = obj.value.split(/[\s]+/);
    if (len.length > wordLen) {
        obj.oldValue = obj.value != obj.oldValue ? obj.value : obj.oldValue;
        obj.value = obj.oldValue ? obj.oldValue : "";
        $.notify("You cannot put more than " + wordLen + " words in this text area.", notifyOptions);
        return false;
    }
    return true;
}

//function checkPhrasesLen(obj, wordLen) {
//    var parts = obj.value.split(',');
//    if (parts.length >= wordLen) {
//        $("#keywordsPhrases").attr('disabled', 'disabled');
//    }
//    if (parts.length > wordLen) {
//        $("#keywordsPhrases").last().remove();
//        $.notify("You cannot put more than " + wordLen + " phrases in this text area.", notifyOptions);
//        return false;
//    }
//    return true;
//}