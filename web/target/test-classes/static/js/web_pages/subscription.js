/**
 * Created by Eias Altawil on 9/9/2017
 */

$(document).ready(function () {

    var submitBtn = $("#lite-button,#premium-button");

    submitBtn.on('click', function(e) {
        e.preventDefault();

        var type = $(this).data("type");
        
         $.ajax({
            url: baseUrl + "api/v1/registration/subscription",
            type: "POST",
            data: {subscriptionType: type},
            success: function (data) {

                window.location.href = baseUrl+'radio'


            },
            error: function (jqXHR, textStatus, errorThrown) {
                if(jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if(jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });
    });
});