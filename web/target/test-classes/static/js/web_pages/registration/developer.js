/**
 *
 */

$(document).ready(function () {
$('#terms-link').on("click", function() {
                $('#dev-terms').modal('show');
});

var submitBtn = $("#submit");

    $("input:checkbox").change(function () {
        var ischecked = $(this).is(':checked');
        if (!ischecked) {
            submitBtn.prop('disabled', true);
            submitBtn.css('cursor', 'not-allowed');
        }
        else {
            submitBtn.prop('disabled', false);
            submitBtn.css('cursor', 'pointer');
        }

    });

});



