/**
 * Created by Tejaswi Venupalli on 8/30/2017
 */

$(document).ready(function () {

    $('#event-others-div').hide();
    $('#event-date-div').hide();

    var d = new Date();
    var userTimeZone = d.toLocaleString('en', {timeZoneName: 'short'}).split(' ').pop();
    $('#datetimepicker-timezone').val(userTimeZone);

    var eventLocationSubmit = $('#event-location-submit');
    eventLocationSubmit.css('cursor', 'pointer');

    eventLocationSubmit.on('click', function (e) {
        e.preventDefault();

        eventLocationSubmit.addClass("loading");
        eventLocationSubmit.prop('disabled', true);

        $.ajax({
            url: baseUrl + 'api/v1/events/create/location',
            type: "POST",
            data: $('#event-location-form').serialize(),

            success: function (data) {
                console.log("success");
                eventLocationSubmit.removeClass("loading");
                eventLocationSubmit.prop('disabled', false);
                $('#event-location-div').hide();
                $('#event-others-div').show();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                eventLocationSubmit.removeClass("loading");
                eventLocationSubmit.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });
    });


    var uploadImageContainer = $("#image-upload-container").find("span");
    var loadingHtml = "<span class=\"ui button loading\"></span>";


    $("#cover-image-file").on('change', function () {
        uploadImageContainer.html(loadingHtml);

        var formData = new FormData();
        formData.append('event_location_picture', this.files[0]);

        $.ajax({
            url: baseUrl + "/api/v1/events/create/upload_location-pictures",
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (data) {
                uploadImageContainer.html("<img src=\"" + data.url + "\"/>");
                $("#event-location-picture1").val(data.id);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 400)
                    showNotifyErrorMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";

            }
        });
    });


    var submitBtn = $("#event-others-submit");
    submitBtn.css('cursor', 'pointer');

    submitBtn.on('click', function (e) {
        e.preventDefault();
        var createForm = $("#event-create")[0];
        var preferenceForm = $("#event-audience-preferences-form");
        submitBtn.addClass("loading");
        submitBtn.prop('disabled', true);

        var formData = new FormData(createForm);
        //console.log($("#eventDescription").htmlarea("toHtmlString"));

        var poData2 = preferenceForm.serializeArray();
        for (var i = 0; i < poData2.length; i++)
            formData.append(poData2[i].name, poData2[i].value);

        $.ajax({
            url: baseUrl + 'api/v1/events/create/others',
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                submitBtn.removeClass("loading");
                submitBtn.prop('disabled', false);
                $('#event-others-div').hide();
                $('#event-date-div').show();
                loadDatepicker1();
                loadDatepicker2();
                loadDatepicker3();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.errorThrown);
                submitBtn.removeClass("loading");
                submitBtn.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });
    });


    var eventSubmit = $("#event-submit");
    eventSubmit.css('cursor', 'pointer');

    eventSubmit.on('click', function (e) {
        e.preventDefault();
        eventSubmit.prop("disabled", true);
        eventSubmit.addClass("loading");
        // eventSubmit.addClass('fa fa-circle-o-notch');

        var eventLcoationForm = $('#event-location-form')[0];
        var eventOthersForm = $("#event-create");
        var preferenceForm = $("#event-audience-preferences-form");
        var eventDateForm = $('#event-create-form');

        var formData = new FormData(eventLcoationForm);

        var f2 = eventOthersForm.serializeArray();
        for (var i = 0; i < f2.length; i++)
            formData.append(f2[i].name, f2[i].value);

        var f3 = preferenceForm.serializeArray();
        for (var i = 0; i < f3.length; i++)
            formData.append(f3[i].name, f3[i].value);

        var f4 = eventDateForm.serializeArray();
        for (var i = 0; i < f4.length; i++)
            formData.append(f4[i].name, f4[i].value);

        $.ajax({
            url: baseUrl + 'api/v1/events/create/date',
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                eventSubmit.removeClass("loading");
                eventSubmit.prop('disabled', false);
                $("#popup-message-title").text("Done");
                $("#popup-message-text").text("Thank you for submitting an Event. You will be notified by email when it is approved.");
                $('#message-popup').modal({
                    closable: false,
                    onApprove: function () {
                        window.location.href = baseUrl + 'events/browse';
                    }
                }).modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                eventSubmit.removeClass("loading");
                eventSubmit.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });
    });

    var eventSubmitAdmin = $("#event-submit-admin");
    eventSubmitAdmin.css('cursor', 'pointer');

    eventSubmitAdmin.on('click', function (e) {
        e.preventDefault();
        eventSubmitAdmin.prop("disabled", true);
        eventSubmitAdmin.addClass("loading");
        // eventSubmit.addClass('fa fa-circle-o-notch');

        var eventLcoationForm = $('#event-location-form')[0];
        var eventOthersForm = $("#event-create");
        var preferenceForm = $("#event-audience-preferences-form");
        var eventDateForm = $('#event-create-form');

        var formData = new FormData(eventLcoationForm);

        var f2 = eventOthersForm.serializeArray();
        for (var i = 0; i < f2.length; i++)
            formData.append(f2[i].name, f2[i].value);

        var f3 = preferenceForm.serializeArray();
        for (var i = 0; i < f3.length; i++)
            formData.append(f3[i].name, f3[i].value);

        var f4 = eventDateForm.serializeArray();
        for (var i = 0; i < f4.length; i++)
            formData.append(f4[i].name, f4[i].value);


        $.ajax({
            url: baseUrl + 'api/v1/events/create/date',
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                eventSubmitAdmin.removeClass("loading");
                eventSubmitAdmin.prop('disabled', false);
                window.location.href = baseUrl + '/lox/events';
            },
            error: function (jqXHR, textStatus, errorThrown) {
                eventSubmitAdmin.removeClass("loading");
                eventSubmitAdmin.prop('disabled', false);
                hideErrorMessages();

                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });
    });


    $('#event-others-previous').css('cursor', 'pointer');
    $('#event-others-previous').on('click', function (e) {
        e.preventDefault();
        $('#event-location-div').show();
        $('#event-others-div').hide();
        hideErrorMessages();
    });


    $('#event-date-previous').css('cursor', 'pointer');
    $('#event-date-previous').on('click', function (e) {
        e.preventDefault();
        $('#event-others-div').show();
        $('#event-date-div').hide();
        hideErrorMessages();
    });

    $('.audience-modal').click(function () {
        $('#audience-preference').modal('show');
    });

    $("#datetimepicker").change(function () {
        var bla = $('#datetimepicker').val();
        $('.date-value').html(bla);
    });
    $("#datetimepicker-two").change(function () {
        var bla2 = $('#datetimepicker-two').val();
        $('.date-value-two').html(bla2);
    });
    $("#datetimepicker-event").change(function () {
        var bla3 = $('#datetimepicker-event').val();
        $('.date-value-event').html(bla3);
    });

    // This button will increment the value
    $('.qtyplus').click(function (e) {
        // Stop acting like a button
        e.preventDefault();

        var currentValue = parseInt($('#attendees-limit').val());

        if (isNaN(currentValue)) {
            currentValue = 0;
        }

        if (!isNaN(currentValue)) {

            $('#attendees-limit').val(currentValue + 1);

        }

    });
    // This button will decrement the value till 0
    $(".qtyminus").click(function (e) {

        // Stop acting like a button
        e.preventDefault();

        var currentValue = parseInt($('#attendees-limit').val());
        if (isNaN(currentValue)) {
            currentValue = 0;
        }
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentValue) && currentValue > 0) {
            // Decrement one
            $('#attendees-limit').val(currentValue - 1);
        } else {
            // Otherwise put a 0 there
            $('#attendees-limit').val(0);
        }
    });

    /*$( "#attendees-limit" ).change(function() {
     var bla4 = $('#attendees-limit').val();
     $('.attendees-limit').val(bla4);
     });*/

    $("#eventDescription").htmlarea({
        toolbar: ["bold", "italic", "underline", "|", "orderedlist", "unorderedlist", "|", "indent", "outdent", "|",
            "justifyleft", "justifycenter", "justifyright"
        ]
    });


});


function loadDatepicker1() {
    var options = {
        upArrow: 'wickedpicker__controls__control-up',
        downArrow: 'wickedpicker__controls__control-down',
        timeSeparator: ':'
    };
    var time = $('.timepicker').wickedpicker(options);
}
function loadDatepicker2() {
    var options = {
        upArrow: 'wickedpicker__controls__control-up',
        downArrow: 'wickedpicker__controls__control-down',
        timeSeparator: ':'
    };
    var time = $('.timepicker1').wickedpicker(options);
}

function loadDatepicker3() {
    jQuery('#datetimepicker-event').datetimepicker({

        format: 'm/d/Y',
        timepicker: false
    })
}

function hideErrorMessages() {
    $('.error-msg').each(function () {
        $(this).text('');
        $(this).hide();
    });
}


