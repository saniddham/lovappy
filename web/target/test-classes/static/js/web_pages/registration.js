$(document).ready(function() {

        $(".content .header ul li").click(function() {
        $('.content .header ul li').removeClass('active');
        $(this).addClass('active');
    });
    $( ".reg-log-modal" ).click(function() {
     $('#login-register').modal('show');
     });
     $( ".log-btn" ).click(function() {
        $(".login-modal").addClass('active');
         $(".register-modal").removeClass('active');
     });
     $( ".reg-btn" ).click(function() {
        $(".register-modal").addClass('active');
         $(".login-modal").removeClass('active');
     });


    var $registerForm = $('#registration-form');
    var $registerLoader = $('#registration-form input[type=submit]').siblings('i');

    $registerForm.on('submit', function(e) {
        e.preventDefault();
        $registerLoader.show();
        $.ajax({
            url: baseUrl + "register",
            type: "POST",
            data: $registerForm.serialize(),
            success: function(data) {
                if (containsValidationErrors(data)) {
                    console.log(data);
                    $registerLoader.hide();
                } else {
                    if (data.result === "no_love") {
                        window.location.href = baseUrl + 'registration/gender';
                    } else {
                        window.location.href = baseUrl + 'registration/couples';
                    }
                    /*$("#verification-email-popup").modal('show');
                     $registerForm.find("input[type=text], input[type=password]").val("");
                     $registerLoader.hide();*/
                }
            },
            error: function(data) {
                $registerLoader.hide();
                console.log(data);
            }
        });
    });




    $('.slider').on('click', function() {
        $(this).siblings('input[type="radio"]:checked').prop("checked", true);
    });

    function containsValidationErrors(response) {
        $('.error-msg').each(function() {
            $(this).text('');
        });

        if (response !== null && response.errorMessageList !== null && typeof response.errorMessageList !== 'undefined') {
            for (var i = 0; i < response.errorMessageList.length; i++) {
                var item = response.errorMessageList[i];
                //var $controlGroup = $('#' + item.fieldName + 'FormGroup');
                //form.find($controlGroup).addClass('has-error');
                //form.find($controlGroup).find('.error-msg').text(item.message);
                var $control = document.getElementsByName(item.fieldName);
                $('.error-container').append("<span class=\"error-msg\">" + item.message + "</span>");
            }
            return true;
        }
        return false;
    }

    $(".socialBtn").on('click', function(e) {
        e.preventDefault();

        var inLove = $('#coupleRadio').is(':checked');
        var provider = $(this).attr("provider");
        if (inLove) {
            socialLoginHack();
        }

        if(provider == 'twitter'){
         //window.location.href = baseUrl + '/signin/social/' + provider;
    $("#twitterFrom").submit();
        } else {
    window.location.href = baseUrl + 'login/' + provider;
   }
    });

    function socialLoginHack() {
        $.ajax({
            url: baseUrl + "register/social",
            type: "POST",
            data: {},
            async: false,
            success: function() {

                return true;
            },
            error: function(e) {
                return false;
            }
        });
    }

});