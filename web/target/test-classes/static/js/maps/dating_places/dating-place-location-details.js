var map;
var googleMapLoaded = false;
// Called in event-create.html
function setGoogleMapLoaded() {
    googleMapLoaded = true;
}

$(document).ready(function () {
    // Prevent submit on press of Enter key
    $(window).keydown(function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            return false;
        }
    });

    var placeID = $('#place-id').val();
    var placeLatitude = parseFloat($('#place-latitude').val());
    var placeLongitude = parseFloat($('#place-longitude').val());

    function drawMap() {
        // Fixes Cannot read property 'firstChild' of null because google map might have been loaded but the DOMs are not yet drawn
        if (!googleMapLoaded) {
            setTimeout(drawMap, 500);
        } else {
            var california = {lat: 37.4419, lng: -122.1419};
            var image = {
                url: baseUrl + '/images/dating/map-marker.png',
                scaledSize: new google.maps.Size(30, 30),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 0)
            };

            var featureImage = {
                url: baseUrl + '/images/dating/feature-icon.png',
                scaledSize: new google.maps.Size(30, 30),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 0)
            };
            map = new google.maps.Map(document.getElementById('google-maps-container'), {
                center: california,
                zoom: 14
            });
            var geocoder = new google.maps.Geocoder(); // Geocoding
            var marker = null;
            var searchMarker = null;
            var infoWindow = new google.maps.InfoWindow;
            var handleLocationError = function (browserHasGeolocation, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                    'Error: The Geolocation service failed.' :
                    'Error: Your browser doesn\'t support geolocation.');
                infoWindow.open(map);
            };

            var datingPlaceMarkers = null;

            var showDatingPlaces = function () {
                var radius = parseInt(500); // show places within 50 mile radius
                if (radius) {
                    if (datingPlaceMarkers !== null) {
                        for (var i = 0; i < datingPlaceMarkers.length; i++) {
                            datingPlaceMarkers[i].setMap(null);
                        }
                    }
                    datingPlaceMarkers = [];

                    $.ajax({
                        type: "POST",
                        url: baseUrl + '/dating/places/show/markers',
                        success: function (results) {
                            if (results.length > 0) {

                                for (var i = 0; i < results.length; i++) {

                                    if (!results[i].featured && results[i].placeId == placeID) {
                                        var markerOptions = createMarkerOptions({
                                            lat: results[i].latitude,
                                            lng: results[i].longitude
                                        }, google.maps.Animation.BOUNCE, image, results[i].placeName);

                                        map.setZoom(17);
                                        var look = new google.maps.Marker(markerOptions);
                                        map.panTo(look.position);

                                    } else if (results[i].featured && results[i].placeId == placeID) {

                                        var featuredMarkerOptions = createMarkerOptions({
                                            lat: results[i].latitude,
                                            lng: results[i].longitude
                                        }, google.maps.Animation.BOUNCE, featureImage, results[i].placeName);

                                        map.setZoom(17);
                                        var look1 = new google.maps.Marker(featuredMarkerOptions);
                                        map.panTo(look1.position);
                                    }
                                    datingPlaceMarkers.push(new google.maps.Marker(featuredMarkerOptions));
                                    datingPlaceMarkers.push(new google.maps.Marker(markerOptions));
                                }
                            } else {
                                console.log("No Dating Places Found");
                            }
                        }
                    });
                }
            };
            //showDatingPlaces();

            function isFunction(functionToCheck) {
                var getType = {};
                return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
            }

            var createMarkerOptions = function (latlng, markerAnimation, image, title) {
                var markerOptions = {
                    position: latlng,
                    draggable: false,
                    map: map,
                    icon: image,
                    title: title
                };
                if (markerAnimation) {
                    markerOptions.animation = markerAnimation;
                }

                return markerOptions;
            };

            var showMarkerGooglePlace = function () {
                /*                var imageUrl = $('#icon-url').val();

                 if (!imageUrl) {
                 imageUrl = baseUrl + '/images/dating/map-marker.png';
                 }
                 var image = {
                 url: imageUrl,
                 scaledSize: new google.maps.Size(30, 30),
                 origin: new google.maps.Point(0, 0),
                 anchor: new google.maps.Point(0, 0)
                 };*/

                var googleMarker = createMarkerOptions({
                    lat: placeLatitude,
                    lng: placeLongitude
                }, google.maps.Animation.BOUNCE, image);

                map.setZoom(17);
                var look2 = new google.maps.Marker(googleMarker);
                map.panTo(look2.position);
            }
            showMarkerGooglePlace();
            /**
             * Converts latlng object which lat/lng property is function to plain latlng object with lat/lng property as float
             * @param latlng
             * @returns {{}}
             */
            var toLatLng = function (latlng) {
                var result = {};
                result.lat = isFunction(latlng.lat) ? latlng.lat() : latlng.lat;
                result.lng = isFunction(latlng.lng) ? latlng.lng() : latlng.lng;
                return result;
            };

            var $zipCode = $('#zipCode');
            var $country = $('#country');
            var $state = $('#state');
            var $province = $('#province');
            var $city = $('#city');
            var $streetNumber = $('#streetNumber');
            var $fullAddress = $('#addressLine');
            var $latitude = $('#latitude');
            var $longitude = $('#longitude');
            var $premise = $('#premise');
            var $route = $('#route');
            var $stateShort = $('#state-short');

            var updateLocation = function (event) {
                if (marker !== null) {
                    marker.setMap(null);
                }
                if (searchMarker !== null) {
                    searchMarker.setMap(null);
                }
                var point = toLatLng(event.latLng);

                geocoder.geocode({'location': point}, function (results, status) {
                    if (status === 'OK') {
                        if (results.length > 0) {
                            var result = results[0];
                            $latitude.val(point.lat);
                            $longitude.val(point.lng);
                            $fullAddress.val(result.formatted_address);
                            $('#addressLine').val(result.formatted_address);
                            var addressComponents = result.address_components;
                            for (var i = 0; i < addressComponents.length; i++) {

                                var addressComponent = addressComponents[i];
                                var addressComponentTypes = addressComponent.types;

                                for (var j = 0; j < addressComponentTypes.length; j++) {
                                    if (addressComponentTypes[j] === 'postal_code') {
                                        $zipCode.val(addressComponent.long_name);
                                        $("#review_zipcode").val(addressComponent.long_name);
                                        break;
                                    } else if (addressComponentTypes[j] === 'country') {
                                        $country.val(addressComponent.long_name);
                                        break;
                                    } else if (addressComponentTypes[j] === 'administrative_area_level_1') {
                                        $state.val(addressComponent.long_name);
                                        $stateShort.val(addressComponent.short_name);
                                        break;
                                    } else if (addressComponentTypes[j] === 'administrative_area_level_2') {
                                        $province.val(addressComponent.long_name);
                                        break;
                                    } else if (addressComponentTypes[j] === 'locality') {
                                        $city.val(addressComponent.long_name);
                                        break;
                                    } else if (addressComponentTypes[j] === 'street_number') {
                                        $streetNumber.val(addressComponent.long_name);
                                        break;
                                    } else if (addressComponentTypes[j] === 'premise') {
                                        $premise.val(addressComponent.long_name);
                                        break;
                                    } else if (addressComponentTypes[j] === 'route') {
                                        $route.val(addressComponent.long_name);
                                        break;
                                    }
                                }
                            }
                            marker = new google.maps.Marker(createMarkerOptions(point, google.maps.Animation.BOUNCE));
                            marker.setIcon('//maps.google.com/mapfiles/ms/icons/green-dot.png');
                            google.maps.event.addListener(marker, "dragend", updateLocation);
                        } else {
                            console.log('No results found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });

            };

            var updateLocationByPlace = function (event) {
                if (marker !== null) {
                    marker.setMap(null);
                }
                if (searchMarker !== null) {
                    searchMarker.setMap(null);
                }
                var placeID = event.placeID;

                geocoder.geocode({'placeId': placeID}, function (results, status) {
                    if (status === 'OK') {
                        if (results.length > 0) {
                            var result = results[0];
                            console.log(results[0].geometry.location.lat());
                            console.log(results[0].geometry.location.lng());

                            $latitude.val(results[0].geometry.location.lat());
                            $longitude.val(results[0].geometry.location.lng());
                            $fullAddress.val(result.formatted_address);
                            $('#addressLine').val(result.formatted_address);
                            var addressComponents = result.address_components;
                            for (var i = 0; i < addressComponents.length; i++) {

                                var addressComponent = addressComponents[i];
                                var addressComponentTypes = addressComponent.types;

                                for (var j = 0; j < addressComponentTypes.length; j++) {
                                    if (addressComponentTypes[j] === 'postal_code') {
                                        $zipCode.val(addressComponent.long_name);
                                        break;
                                    } else if (addressComponentTypes[j] === 'country') {
                                        $country.val(addressComponent.long_name);
                                        break;
                                    } else if (addressComponentTypes[j] === 'administrative_area_level_1') {
                                        $state.val(addressComponent.long_name);
                                        $stateShort.val(addressComponent.short_name);
                                        break;
                                    } else if (addressComponentTypes[j] === 'administrative_area_level_2') {
                                        $province.val(addressComponent.long_name);
                                        break;
                                    } else if (addressComponentTypes[j] === 'locality') {
                                        $city.val(addressComponent.long_name);
                                        break;
                                    } else if (addressComponentTypes[j] === 'street_number') {
                                        $streetNumber.val(addressComponent.long_name);
                                        break;
                                    } else if (addressComponentTypes[j] === 'premise') {
                                        $premise.val(addressComponent.long_name);
                                        break;
                                    } else if (addressComponentTypes[j] === 'route') {
                                        $route.val(addressComponent.long_name);
                                        break;
                                    }
                                }
                            }
                            marker = new google.maps.Marker(createMarkerOptions(results[0].geometry.location, google.maps.Animation.BOUNCE));
                            marker.setIcon('//maps.google.com/mapfiles/ms/icons/green-dot.png');
                            google.maps.event.addListener(marker, "dragend", updateLocation);
                        } else {
                            console.log('No results found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });

            };

            if ($latitude.val() && $longitude.val()) {
                console.log('Initial map center ' + $latitude.val() + ':' + $longitude.val());
                var pos = {
                    lat: parseFloat($latitude.val()),
                    lng: parseFloat($longitude.val())
                };
                map.setCenter(new google.maps.LatLng($latitude.val(), $longitude.val()));
                updateLocation({latLng: pos});
            } else {
                console.log('Fething map center through geolocation');
                // Try HTML5 geolocation.
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        //map.setCenter(pos);
                        updateLocation({latLng: pos});
                    }, function () {
                        handleLocationError(true, map.getCenter());
                    });
                } else {
                    // Browser doesn't support Geolocation
                    handleLocationError(false, map.getCenter());
                }
            }
            google.maps.event.addListener(map, "click", updateLocation);

            var searchMap = function () {
                var address = $lastAddressSearch.val();
                console.log(address);
                if (address) {
                    geocoder.geocode({'address': address}, function (results, status) {
                        if (status === 'OK') {
                            var location = results[0].geometry.location;
                            map.setCenter(location);
                            var newplace = {
                                placeID: results[0].place_id
                            };
                            updateLocationByPlace(newplace);
                        } else {
                            console.log('Geocode was not successful for the following reason: ' + status);
                        }
                    });
                } else {
                    // Set center to last selected marker location
                    map.setCenter(marker.position);
                    showDatingPlaces();
                }
            };

            var $lastAddressSearch = $('#addressLine').keyup(function (e) {
                if (e.which === 13) {
                    console.log("calling search map");
                    searchMap();
                }
            });

            $('#btn-search-map').on('click', function (e) {
                var e = $.Event('keyup');
                e.which = 13;
                $('#addressLine').trigger(e);
            });

        }
    }

    drawMap();

    function degreesToRadians(degrees) {
        return degrees * Math.PI / 180;
    }

    function distanceInKmBetweenEarthCoordinates(lat1, lon1, lat2, lon2) {
        var earthRadiusKm = 6371;

        var dLat = degreesToRadians(lat2 - lat1);
        var dLon = degreesToRadians(lon2 - lon1);

        lat1 = degreesToRadians(lat1);
        lat2 = degreesToRadians(lat2);

        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return earthRadiusKm * c;
    }
});