 $(document).ready(function() {


     $("#newBlogBtn").click(function() {
         setTimeout(function() {
             $("#addCustomHtmlArea").htmlarea({
                 toolbar: ["bold", "italic", "underline", "|", "orderedlist", "unorderedlist", "|", "indent", "outdent", "|",
                     "justifyleft", "justifycenter", "justifyright"
                 ]
             });

         }, 300);
     });



     $("#updateTextArea").htmlarea({
         toolbar: ["bold", "italic", "underline", "|", "orderedlist", "unorderedlist", "|", "indent", "outdent", "|",
             "justifyleft", "justifycenter", "justifyright"
         ]
     });


     $('#submit-new-blog').click(function(e) {
         e.preventDefault();
         var html = $("#addCustomHtmlArea").htmlarea("toHtmlString");
         console.log("HTML  " + html);

         $('#new-blog-form').submit();

     });




     $("#update-blog-image").on('change', function() {
         readURL(this, $('#imageBlog'));
     });
     $("#create-blog-image").on('change', function() {
         readURL(this, $('#imagePost'));
     });

     function readURL(input, selector) {
         if (input.files && input.files[0]) {
             var reader = new FileReader();

             reader.onload = function(e) {

                 selector.attr('src', e.target.result);
             };

             reader.readAsDataURL(input.files[0]);
         }
     }


        $('.upload-photo-id').on('click', function (e) {
            e.preventDefault();
            $('#upload-modal').modal('show');
        });

 });
    var options = {
        viewMode:1,
        minCropBoxWidth: 482,
        minCropBoxHeight: 237,
        imageSmoothingEnabled: false,
        imageSmoothingQuality: 'high',
        aspectRatio: 482 / 237,
        preview: $('#img2')
    };


    $('#upload-button').click(function () {
        $('#inputImage').click();
    });

    function uploadFile() {
        var uploadImageBtn = $("#upload-image");
        var uploadImageContainer = $("#image-upload-container").find("span");
        var url = baseUrl + "/lox/blog/picture/upload";

        $('#image').cropper('getCroppedCanvas',{ fillColor: '#FFFFFF' }).toBlob(function (blob) {

            $(".btn").prop("disabled", true);
            uploadImageBtn.find("i").show();

            var formData = new FormData();

            formData.append('image_file', blob);

            $.ajax(url, {
                method: "POST",
                data: formData,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                success: function (data) {
                    uploadImageContainer.html("<img src=\"" + data.url + "\" style='height: 100%;'/>");

                    $("#image_id").val(data.id);
                    $('#upload-modal').modal('hide');
                    $(".btn").prop("disabled", false);
                    uploadImageBtn.find("i").hide();

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".btn").prop("disabled", false);
                    uploadImageBtn.find("i").hide();
                    if (jqXHR.status === 400)
                        showNotifyErrorMessages(jqXHR.responseJSON);
                    else if (jqXHR.status === 401)
                        window.location.href = baseUrl + "login";

                }
            });
        });
    }
