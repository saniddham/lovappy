 $(document).ready(function() {


       $('#toggle-paused').change(function() {
        if(this.checked) {

            $(this).prop("checked", banOrPauseUser('pause', $('#userId').val(), "DELETE"));
            $('#pauseText').text("Paused")
        }else{
         banOrPauseUser('pause', $('#userId').val(), "POST")
        $(this).prop("checked", false);
         $('#pauseText').text("Pause")
         }

    });

    $('#toggle-banned').change(function() {
        if(this.checked) {

            $(this).prop("checked", banOrPauseUser('ban', $('#userId').val(), "DELETE"));
             $('#banText').text("Banned")
        } else{
         banOrPauseUser('ban', $('#userId').val(), "POST")
         $(this).prop("checked", false);
         $('#banText').text("Ban")
         }
    });

 function banOrPauseUser(status, userId, method) {
        $.ajax({
            url: baseUrl + "lox/users/" + userId + "/" + status,
            type: method,
            data: {},
            success: function () {
           return true;
            },
            error: function () {
          return false;
            }
        });
}

        var saveBtn = $("#btn-save");

        saveBtn.click(function () {
            saveBtn.prop("disabled", true);
            $(this).find("i").show();

            $.ajax({
                url: baseUrl + "lox/users/save_roles",
                type: "POST",
                data: {
                    user_id: $("#userId").val(),
                    author: $("#authorRole").is(':checked'),
                    event: $("#eventRole").is(':checked')
                },
                success: function (data) {
                    location.reload();
                },
                error: function (data) {
                    saveBtn.prop("disabled", false);
                    saveBtn.find("i").hide();
                    console.log(data);
                }
            });
        });
    });