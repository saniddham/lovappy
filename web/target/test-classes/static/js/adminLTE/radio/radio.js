     // wait for the DOM to be loaded
     $(document).ready(function() {
         // bind 'myForm' and provide a simple callback function
         $('.btn-home').click(function() {
             var saveBtn = $(this);
             saveBtn.prop("disabled", true);
             saveBtn.find("i").show();
             var status = $(this).attr("data-status");
             var radioId = $(this).attr("data-id");
             var action;
            console.log(status);
             if (status === 'HIDE') {
                 action = 'DELETE';
             } else {
                 action = 'POST'
             }

             $.ajax({
                 url: baseUrl + 'lox/lovstamp/' + radioId + '/home',
                 type: action,
                 contentType: 'application/json; charset=UTF-8',
                 success: function() {

                     saveBtn.prop("disabled", false);
                     saveBtn.find("i").hide();
                     if (status === 'HIDE') {
                         saveBtn.html('Show <i class="fa fa-spinner fa-spin" style="display: none;"></i>');

               saveBtn.removeClass("btn-success");
                         saveBtn.addClass("btn-danger")
            saveBtn.attr("data-status", 'SHOW');

                     } else {
                         saveBtn.html('Hide <i class="fa fa-spinner fa-spin" style="display: none;"></i>');
                        saveBtn.attr("data-status", 'HIDE');


                                saveBtn.removeClass("btn-danger");
                                                  saveBtn.addClass("btn-success");
                     }

                     // return true;
                 },
                 error: function(e) {

                     saveBtn.prop("disabled", false);
                     saveBtn.find("i").hide();

                 }
             });

         });
     });