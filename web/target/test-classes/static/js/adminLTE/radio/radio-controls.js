/**
 * Created by Manoj on 18/12/2017.
 */

var timeList = [];
var locationList = [];
$(document).ready(function () {


    $(function () {
        $('#datetimepicker1').datetimepicker({format: 'DD-MM-YYYY hh:mm a'});
        $('#datetimepicker2').datetimepicker({format: 'DD-MM-YYYY hh:mm a'});
    });

    $('#languages-select').dropdown();

    // $('.clockpicker').clockpicker({
    //     align: 'left',
    //     donetext: 'Done'
    // });

    $("#addTime").click(function () {
        var startTime = $("#timeStart").val();
        var endTime = $("#timeEnd").val();

        if (!startTime) {
            $("#timeStart").css("border-color", "red");
        } else if (!endTime) {
            $("#timeEnd").css("border-color", "red");
        } else {
            timeList.push({startTime: startTime, endTime: endTime});
            generateList();
            $("#timeStart").css("border-color", "");
            $("#timeEnd").css("border-color", "");
            $("#timeStart").val("");
            $("#timeEnd").val("");
        }

    });

    $('#action').on('change', function() {
        if ( this.value == 'INSERT')
        {
            $("#insertWhereDiv").show();
            $("#insertWhere").prop('required',true);
        }
        else
        {
            $("#insertWhereDiv").hide();
            $("#insertWhere").val("");
            $("#insertWhere").prop('required',false);
        }
    });

    $("#addLocation").click(function () {
        var location = $("#searchLocation").val();

        if (!location) {
            $("#searchLocation").css("border-color", "red");
        } else {
            locationList.push(location);
            generateLocationList();
            $("#searchLocation").css("border-color", "");
            $("#searchLocation").val("");
        }

    });

    var input = document.getElementById('searchLocation');
    var options = {};

    autocomplete = new google.maps.places.Autocomplete(input, options);
});

function removeTime(index) {
    if (index > -1) {
        timeList.splice(index, 1);
        generateList();
    }
}

function generateList() {
    $('#timeList').html('');
    $.each(timeList, function (id, value) {
        $("#timeList").append("<a class='ui label transition visible' data-value='4' style='display: inline-block !important;font-size: 1em !important;'>" + value.startTime + " - " + value.endTime + "<i OnClick='removeTime(" + id + ");' class='delete icon'></i></a>");

        addInputs("timeSlots[" + id + "].startTime", value.startTime);
        addInputs("timeSlots[" + id + "].endTime", value.endTime);
    });
}

function addInputs(name, value) {
    $('<input/>').attr({
        type: 'hidden',
        name: name,
        value: value
    }).appendTo('#timeList');
}

function generateLocationList() {
    $('#locationList').html('');
    $.each(locationList, function (id, value) {
        $("#locationList").append("<a class='ui label transition visible' data-value='4' style='display: inline-block !important;font-size: 1em !important;'>" + value + "<i OnClick='removeLocation(" + id + ");' class='delete icon'></i></a>");
        var name = "locations[" + id + "].location";
        $('<input/>').attr({
            type: 'hidden',
            name: name,
            value: value
        }).appendTo('#locationList');
    });
}

function removeLocation(index) {
    if (index > -1) {
        locationList.splice(index, 1);
        generateLocationList();
    }
}

function validateForm() {
    if (timeList.length <= 0) {
        toastr.warning('Please add start and end time');
        // alert("Please add start and end time");
        return false;
    }

    if (locationList.length <= 0) {
        toastr.warning('Please add locations');
        // alert("Please add locations");
        return false;
    }

    return true;
}