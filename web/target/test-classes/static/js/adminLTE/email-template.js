  $(document).ready(function() {


      setTimeout(function() {
          $("#body").htmlarea({
              toolbar: ["bold", "italic", "underline", "|", "orderedlist", "unorderedlist", "|", "indent", "outdent", "|",
                  "justifyleft", "justifycenter", "justifyright"
              ]
          });

      }, 300);

      $("#email-image").on('change', function() {
          readURL(this, $('#emailImage'));
      });


 $("#sendTestCreateBtn").click(function() {

         sendTestEmail($(this), "lox/emails/templates/test");
      });


      $("#sendTestBtn").click(function() {
          var templateId = $(this).attr("data-id");

           sendTestEmail($(this), "lox/emails/templates/" + templateId + "/test");
      });

      function sendTestEmail(selector, url){
             var html = $("#body").htmlarea("toHtmlString");

                var payload = {
                    email: $("#testEmail").val(),
                    subject: $("#subject").val(),
                    body: html,
                    imageData: $('#emailImage').attr("src"),
                      fromEmail:$("#fromEmail").val()
                }
                $(selector).html('Sending..');
                $.ajax({
                    url: baseUrl + url,
                    type: "POST",
                    data: JSON.stringify(payload),
                    contentType: 'application/json; charset=UTF-8',
                    success: function() {
                        console.log("open success popup");
                        $("#modalSuccessTest").modal('show');
                        $("#sendTestBtn").html('Send Email');
                        // return true;
                    },
                    error: function(e) {
                        console.log("open failure popup" + e);
                        $("#sendTestBtn").html('Send Email');
                        $("#modalFailureTest").modal('show');
                    }
                });
      }
  });