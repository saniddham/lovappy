     // wait for the DOM to be loaded
     $(document).ready(function() {
         // bind 'myForm' and provide a simple callback function
         $('.btn-tips').click(function() {
             var saveBtn = $(this);
             saveBtn.prop("disabled", true);
             saveBtn.find("i").show();
             var status = $(this).attr("data-status");
             var id = $(this).attr("data-id");
             var action;
             if (status === 'Disable') {
                 action = 'DELETE';
             } else {
                 action = 'POST'
             }

             $.ajax({
                 url: baseUrl + 'lox/happy-couples/' + id + '/active',
                 type: action,
                 contentType: 'application/json; charset=UTF-8',
                 success: function() {

                     saveBtn.prop("disabled", false);
                     saveBtn.find("i").hide();
                     if (status === 'Disable') {
                         saveBtn.html('Enable <i class="fa fa-spinner fa-spin" style="display: none;"></i>');

               saveBtn.removeClass("btn-success");
                         saveBtn.addClass("btn-danger")
            saveBtn.attr("data-status", 'Enable');

                     } else {
                         saveBtn.html('Disable <i class="fa fa-spinner fa-spin" style="display: none;"></i>');
                        saveBtn.attr("data-status", 'Disable');


                                saveBtn.removeClass("btn-danger");
                                                  saveBtn.addClass("btn-success");
                     }

                     // return true;
                 },
                 error: function(e) {

                     saveBtn.prop("disabled", false);
                     saveBtn.find("i").hide();

                 }
             });

         });
     });