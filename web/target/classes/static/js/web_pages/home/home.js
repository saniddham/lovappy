$(document).ready(function() {


    $("#btn-ok").click(function() {
        $("#verification-email-popup").modal('hide');
    });

    $('.mobile-menu-btn').click(function() {
        $('.nav-mobile').toggleClass('active');
    });



    $('.login-btn').click(function(event) {
        event.preventDefault();
        $('.login-modal').toggleClass('active');
        $('.hero-video .buttons').toggleClass('active');
        $('.how-this-works').toggleClass('hide');
        $('.register-btn').toggleClass('hide');
    });

    $('.register-btn').click(function(event) {
        event.preventDefault();
        $('.register-modal').toggleClass('active');
        $('.hero-video .buttons').toggleClass('active');
        $('.how-this-works').toggleClass('hide');
        $('.login-btn').toggleClass('hide');
    });

//    var video = $('#video_video');
//    video.on("click",function() {
//        video.play();
//    }, false);
//
//    video.play();
//videojs("#video_video").ready(function(){
//  var myPlayer = this;
//videojs("#video_video").removeClass("video_video-dimensions");
//  // EXAMPLE: Start playing the video.
//  myPlayer.play();
//
//});
    var $document = $(document);
    $document.scroll(function() {
        if ($document.scrollTop() >= 450) {
            $('.arrow-down').hide();
        } else {
            $('.arrow-down').show();
        }
    });


});