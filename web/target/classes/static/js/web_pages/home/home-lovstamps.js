var soundId;
var sound;

var prevUrl;
$(document).ready(function () {

 var playBtn = $(".play");
var playLovDropButton;

    playBtn.click(function() {
        var url = $(this).attr("data-url");
        var userId = $(this).attr("data-id");


        playLovDropButton = $("#" + userId);

        play(url, userId);
    });

    function play(url, userId) {


        if (sound) {
            if (sound.playing()) {


                sound.stop(soundId);

                if (prevUrl === url) {
                    return;
                }
            } else {

                if (prevUrl === url) {

                    sound.play(soundId);
                    return;
                }
            }

        }
        prevUrl = url;
        sound = new Howl({
            src: [url],
            html5: true, // Force to HTML5 so that the audio can stream in (best for large files).
            onplay: function() {

                 $("#playBtn"+userId).addClass('fa-pause').removeClass('fa-play');

                //                $("#listenerAddress").atrr("text", user.)
            },
            onload: function() {
                 $("#playBtn"+userId).addClass('fa-play').removeClass('fa-pause');


            },
            onend: function() {
           $("#playBtn"+userId).addClass('fa-play').removeClass('fa-pause');

            },
            onpause: function() {
               $("#playBtn"+userId).addClass('fa-play').removeClass('fa-pause');

            },
            onstop: function() {
                 $("#playBtn"+userId).addClass('fa-play').removeClass('fa-pause');

            }
        });

        if (sound.playing()) {

            soundId = sound.stop();
        } else {

            soundId = sound.play();

        }

    //End Block Section

    }

    function timeRecords(recordsSeconds, selector){

    var downloadTimer = setInterval(function(){
      selector.value = 10 - --recordsSeconds;
      if(recordsSeconds <= 0)
        clearInterval(downloadTimer);
    },1000);
    }
});
