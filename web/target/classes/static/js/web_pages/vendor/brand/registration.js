$(document).ready(function () {

    $('#country').dropdown();
    $('#product-type-select').dropdown();
    $('#brand-select').dropdown();
});

var submitBtn = $("#register-vendor");

submitBtn.on('click', function (e) {
    e.preventDefault();
    submitBtn.addClass("disabled");
    submitBtn.addClass("loading");

    var x = $('#addressLine1').validate()
        & $('#countryInput').validateCountry()
        & $('#productTypeInput').validateProductType()
        & $('#brandInput').validateBrand()
        & $('#city').validate()
        & $('#state').validate()
        & $('#zipCode').validate();

    if (x) {
        var url = "/brand/register";
        $.ajax({
            url: baseUrl + url,
            type: "POST",
            data: $('#reg-form').serialize(),
            success: function (data) {
                if (containsValidationErrors(data)) {
                    submitBtn.removeClass("disabled");
                    submitBtn.removeClass("loading");
                } else {
                    submitBtn.removeClass("disabled");
                    submitBtn.removeClass("loading");
                    location.reload();
                }
            },
            error: function (data) {
                submitBtn.removeClass("disabled");
                submitBtn.removeClass("loading");
                toastr.error('Failed to save !!');
            }
        });
    } else {
        toastr.error($('#fill-all-fields').val());
        submitBtn.removeClass("disabled");
        submitBtn.removeClass("loading");
    }

});

$('#add-brand').on('click', function (e) {
    e.preventDefault();
    $('#add-brand-modal').modal('show');
});

$('#add-product-type').on('click', function (e) {
    e.preventDefault();
    $('#add-product-type-modal').modal('show');
});

$('#add-brand-button').on('click', function (e) {
    e.preventDefault();
    if ($('#brandName').validate()) {
        var url = "/api/v1/brand/add";
        $.ajax({
            url: baseUrl + url,
            type: "POST",
            data: $('#brand-form').serialize(),
            success: function (data) {
                $('#add-brand-modal').modal('hide');
                var obj = {};
                obj[data.brandName] = data.id;
                setDinamicOptions('#brand-select', obj);
            },
            error: function (response) {
                if (response.status == '302') {
                    toastr.error("Duplicate entry !");
                } else {
                    toastr.error("Failed to save !");
                }
            }
        });
    } else {
        toastr.error("Brand name required !");
    }
});

$('#add-product-type-button').on('click', function (e) {
    e.preventDefault();

    if ($('#typeName').validate()) {
        var url = "/api/v1/product-type/add";
        $.ajax({
            url: baseUrl + url,
            type: "POST",
            data: $('#product-type-form').serialize(),
            success: function (data) {
                $('#add-product-type-modal').modal('hide');
                var obj = {};
                obj[data.typeName] = data.id;
                setDinamicOptions('#product-type-select', obj);
            },
            error: function (response) {
                if (response.status == '302') {
                    toastr.error("Duplicate entry !");
                } else {
                    toastr.error("Failed to save !");
                }
            }
        });
    } else {
        toastr.error("Name required !");
    }
});

function setDinamicOptions(selector, options) {
    var att = "data-dinamic-opt";
    $(selector).find('[' + att + ']').remove();
    var html = $(selector + ' .menu').html();
    for (key in options) {
        html += '<div class="item" data-value="' + options[key] + '" ' + att + '>' + key + '</div>';
    }
    $(selector + ' .menu').html(html);
    $(selector).dropdown();
}


(function ($) {
    $.fn.validate = function () {
        var success = true;
        if ($(this).val() == '') {
            $(this).css('border', '1px solid red');
            success = false;
        } else
            $(this).css('border', '1px solid #898989');

        return success;
    }

}(jQuery));

(function ($) {
    $.fn.validateCountry = function () {
        var success = true;
        if ($(this).val() == '') {
            $('#country').css('border', '1px solid red');
            success = false;
        } else
            $('#country').css('border', '1px solid #898989');

        return success;
    }

}(jQuery));

(function ($) {
    $.fn.validateProductType = function () {
        var success = true;
        if ($(this).val() == '') {
            $('#product-type-select').css('border', '1px solid red');
            success = false;
        } else
            $('#product-type-select').css('border', '1px solid #898989');

        return success;
    }

}(jQuery));

(function ($) {
    $.fn.validateBrand = function () {
        var success = true;
        if ($(this).val() == '') {
            $('#brand-select').css('border', '1px solid red');
            success = false;
        } else
            $('#brand-select').css('border', '1px solid #898989');

        return success;
    }

}(jQuery));

function containsValidationErrors(response) {
    $('.error-msg').each(function () {
        $(this).text('');
        $(this).hide();
    });

    if (response !== null && response.errorMessageList !== null && typeof response.errorMessageList !== 'undefined') {
        var errorMessages;
        for (var i = 0; i < response.errorMessageList.length; i++) {

            if (!!errorMessages) {
                errorMessages += response.errorMessageList[i].message + "</br>";
            } else {
                errorMessages = response.errorMessageList[i].message + "</br>";
            }
            toastr.error(response.errorMessageList[i].message);

        }
        if (!!errorMessages) {
            $('#confirmTerms').html(errorMessages);
            $('#confirmTerms').show();
        }
        return true;
    }
    return false;
}