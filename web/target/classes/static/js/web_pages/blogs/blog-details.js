
$(document).ready(function() {
   $('.search-slider').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            centerMode: false,
            prevArrow: $('.blog-slider-left'),
            nextArrow: $('.blog-slider-right'),
            responsive: [{
                breakpoint: 767,
                settings: {
                    slidesToShow: 1
                }
            }]
        });

        $('#share-blog').click(function () {
            $(".modal[data-edit='share-article']").modal("show");
        });

 $(".user-hover").hover(
        function () {
            $(this).parent().find('.on-hover-username').addClass('active');
            $(this).parent().find('.on-hover-username.active').addClass('active');
        }, function () {
            $(this).parent().find('.on-hover-username').removeClass('active');
        }
    );

});