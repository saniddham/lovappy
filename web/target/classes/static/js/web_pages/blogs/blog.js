$(document).ready(function () {

    $("#secondPost.span.user-hover").hover(
        function () {
            $('.on-hover-username').css('display', 'block');
        },
        function () {
            $('.on-hover-username').css('display', 'none');
        }
    );
    $('#popularTab').on("click", function () {

        $('#popularTab').css("background", "#4791d2");
        $('#recentTab').css("background", "#222222");
        $('#popularList').show();
        $('#recentList').hide();

    });

$(".registrationLink").click(function() {
    $('html, body').animate({
        scrollTop: $("#registration").offset().top
    }, 500, 'linear');
});
    $('#recentTab').on("click", function () {

        $('#recentTab').css("background", "#4791d2");
        $('#popularTab').css("background", "#222222");
        $('#recentList').show();
        $('#popularList').hide();

    });


    $('.slider-right-arrow').click(function () {
        var slideWidth = $('.slider-articles-list li').width();
        $('.slider-articles-list').animate({
            left: +slideWidth
        }, 200, function () {
            $('.slider-articles-list li:last-child').prependTo('.slider-articles-list');
            $('.slider-articles-list').css('left', '');
        });
    });

    $('.slider-left-arrow').click(function () {
        var slideWidth = $('.slider-articles-list li').width();
        $('.slider-articles-list').animate({
            right: +slideWidth
        }, 200, function () {
            $('.slider-articles-list li:last-child').prependTo('.slider-articles-list');
            $('.slider-articles-list').css('right', '');
        });
    });

    $('.delete-article-img').click(function () {
        var postId = $(this).attr("data-id");
        $(".modal[data-edit='delete-article']").attr("postId", postId);
        $(".modal[data-edit='delete-article']").modal("show");
    });


    $('#submit-article-btn').click(function () {
        $(".modal[data-edit='submit-article-modal']").modal("show");
    });

    //share Section
    $('#share-email-article').click(function () {
        $(".modal[data-edit='share-email-article']").modal("show");
    });

    $('#share-btn-facebook').click(function () {
        share('facebook');
    });
    $('#share-btn-twitter').click(function () {
        share('twitter');
    });
    $('#share-btn-google').click(function () {
        console.log("shhareeee")
        share('google');
    });
    $('#share-btn-linkedin').click(function () {
        share('linkedin');
    });
    $('#share-btn-pinterest').click(function () {
        share('pinterest');
    });

    $('#registrationBtn').click(function () {
        $('html, body').animate({
            scrollTop: $("#registration").offset().top
        }, 500);
    });

    $('.registrationLink').click(function () {
        $('html, body').animate({
            scrollTop: $("#registration").offset().top
        }, 500);
    });


    $('#deletePost').click(function () {
        var postId = $(".modal[data-edit='delete-article']").attr("postId");

        $(this).attr("href", baseUrl + "/blog/my-blogs/" + postId + "/delete");
    });

    // changing nav elements to show pop up for EVENT_AMBASSADOR only role
    var isAmbassadorOnly = $('#isAmbassadorOnly').val();

    if (isAmbassadorOnly === '1') {
        $('.main-nav ul li').each(function () {
            var field = $(this).find('a').text();
            if (field.toLowerCase() === 'likes' || field.toLowerCase() === 'my map') {
                $(this).find('a').attr('class', 'ambassadorOnlyPopup');
            }
        });
        $('.responsive-nav ul li').each(function () {
            var field = $(this).find('a').text();
            if (field.toLowerCase() === 'likes' || field.toLowerCase() === 'my map') {
                $(this).find('a').attr('class', 'ambassadorOnlyPopup');
            }
        });
    }

    $('.ambassadorOnlyPopup').on('click', function (e) {
        e.preventDefault();
        $('#ambassador-modal').modal('show');
    });

    $(".user-hover").hover(
        function () {
                var postId = $(this).attr("data-postId");
                $(this).parent().find('span[data-postId=' + postId + '].on-hover-username').addClass('active');
                $(this).parent().find('span[data-postId=' + postId + '].on-hover-username.active').addClass('active');

        }, function () {
         var postId = $(this).attr("data-postId");
         $(this).parent().find('span[data-postId=' + postId + '].on-hover-username').removeClass('active');
        }
    );
});