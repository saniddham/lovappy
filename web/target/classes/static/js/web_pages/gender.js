/**
 * Created by Eias Altawil on 8/16/2017
 */
$(document).ready(function () {

    var submitBtn = $("#submit-gender");
    $('.error-msg').each(function () {
        $(this).text('');
    });
    $('#editSearch').on('click', function (e) {
        e.preventDefault();
        var action = $(this).data('action');

        if (action === 'save') {

            saveLocation();

        } else {

            $(this).text("SAVE");
            $('#locationLable').hide();
            $('#last-address-search').show();
            $('#btn-search-map').show();
            $(this).data('action', 'save');
        }
    });

    submitBtn.on('click', function (e) {
        e.preventDefault();
        genderRegistration(submitBtn);

    });
});


var saveLocation = function () {
    var $lastMileRadiusSearch = $('#last-mile-radius-search');
    var latitude = parseFloat($('#latitude').val());
    var longitude = parseFloat($('#longitude').val());
    var radius = parseInt($lastMileRadiusSearch.val());
    if (latitude && longitude && radius) {
        $.ajax({
            type: "POST",
            url: baseUrl + 'api/v1/registration/location/save',
            data: JSON.stringify({
                latitude: latitude,
                longitude: longitude,
                radius: radius,
            }),
            contentType: "application/json",
            dataType: 'json',
            success: function (data) {
                $('#editSearch').text("EDIT");
                $('#locationLable').show();
                $('#locationLable').text(data.shortAddress);
                $('#last-address-search').hide();
                $('#btn-search-map').hide();
                $('#editSearch').data('action', 'edit');
            },
            error: function (e) {
                console.log(e);
            }
        });
    }
};

function genderRegistration(selector) {

    $('.error-msg').each(function () {
        $(this).text('');
    });

    if ($("input[name='birthDate']").val() == "") {
        $('.error-container').append("<span class=\"error-msg\"> Date of Birth required !</span>");
        toastr.error('Date of Birth required !');
        return;
    }else if (!isDate($("input[name='birthDate']").val())){
        $('.error-container').append("<span class=\"error-msg\"> Invalid Date of Birth !</span>");
        toastr.error('Invalid Date of Birth !');
        return;
    }
     if (!$("#genderMale").prop("checked") && !$("#genderFemale").prop("checked")) {
        toastr.error('Gender required !');
        return;
    }
    if (!$("#prefGenderMale").prop("checked")&&!$("#prefGenderFemale").prop("checked")&&!$("#prefGenderBoth").prop("checked")) {
        toastr.error('Pref Gender required !');
        return;
    }

    var loca = $('#locationLable').text();
    if ($("#last-address-search").val() == "" && loca == "") {
        $('.error-container').append("<span class=\"error-msg\"> Location required !</span>");
        toastr.error('Location required !');
        return;
    }
    selector.addClass("disabled");
    selector.addClass("loading");
    $.ajax({
        url: baseUrl + "api/v1/registration/step1",
        type: "POST",
        data: $('#main-form').serialize(),
        success: function (data) {
            if (containsValidationErrors(data)) {
                selector.removeClass("disabled");
                selector.removeClass("loading");
            } else {
                if ($('#user-type').val() === 'AUTHOR') {
                    authorRegistration();
                } else if ($('#user-type').val() === 'EVENT_AMBASSADOR') {
                    window.location.href = baseUrl + 'ambassador/signup';
                } else if ($('#user-type').val() === 'MUSICIAN') {
                    musicianRegistration();
                } else {
                    window.location.href = baseUrl + 'registration/ageheightsizelang';
                }
                selector.removeClass("disabled");
                selector.removeClass("loading");
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            selector.removeClass("disabled");
            selector.removeClass("loading");

            if (jqXHR.status === 400) {
                    toastr.error("Please fill all required fields !!");
                showValidationMessages(jqXHR.responseJSON);
                }
            else if (jqXHR.status === 401)
                window.location.href = baseUrl + "login";
        }
    });

}

function authorRegistration() {
    $.ajax({
        url: baseUrl + "api/v1/registration/author",
        type: "POST",
        data: {},
        async: false,
        success: function (data) {
            if (data === 'not-social')
                window.location.href = baseUrl + 'account_not_verified';
            else {
                window.location.href = baseUrl + 'blog/submit'
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 400)
                showValidationMessages(jqXHR.responseJSON);
            else if (jqXHR.status === 401)
                window.location.href = baseUrl + "login";
        }
    });
}

function musicianRegistration() {
    $.ajax({
        url: baseUrl + "api/v1/registration/musician",
        type: "POST",
        data: {},
        async: false,
        success: function (data) {
            if (data === 'not-social')
                window.location.href = baseUrl + 'account_not_verified';
            else {
                window.location.href = baseUrl + 'music/upload'
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 400)
                showValidationMessages(jqXHR.responseJSON);
            else if (jqXHR.status === 401)
                window.location.href = baseUrl + "login";
        }
    });
}

function changeToAmbassador() {
    $('#user-type').val('EVENT_AMBASSADOR');
    genderRegistration($("#ambassador"));
}

function changeToAuthor() {
    $('#user-type').val('AUTHOR');
    genderRegistration($("#author"));
}

function changeToMusician() {
    $('#user-type').val('MUSICIAN');
    genderRegistration($("#musician"));
}

function containsValidationErrors(response) {
    $('.error-msg').each(function () {
        $(this).text('');
        $(this).hide();
    });

    if (response !== null && response.errorMessageList !== null && typeof response.errorMessageList !== 'undefined') {
        for (var i = 0; i < response.errorMessageList.length; i++) {
            var item = response.errorMessageList[i];
            var $controlGroup = $('#' + item.fieldName + 'FormGroup');
            var $errorMsg = $controlGroup.find('.error-msg');
            $errorMsg.text(item.message);
            $errorMsg.show();
        }
        return true;
    }
    return false;
}

