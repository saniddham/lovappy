/**
 * Created by Eias Altawil on 5/21/17
 */

$(document).ready(function () {
    $("#sendGiftBtn").click(function () {
        calculatePrice(getCalcPriceData());
        $('#square-payment-form').modal('show');
    });
});

function getCalcPriceData() {
    return {
        gift_id: $('#giftID').val(),
        coupon: $("#coupon-code").val()
    };
}

function processPaymentForm(nonce) {
    $.ajax({
        url: baseUrl + "gifts/send",
        type: "POST",
        data: {
            nonce: nonce,
            to_user_id: $("#userID").val(),
            gift_id: $("#giftID").val(),
            quantity: 1,
            coupon: $("#coupon-code").val()
        },
        success: function () {
            alert("Gift Sent");
            window.location = baseUrl + "member/" + $("#userID").val()
        },
        error: function () {
            toastr.error("Failed to send Gift!");
        },
        complete: function (jqXHR, textStatus) {
            $requestNonceBtn.removeClass("loading disabled");
        }
    });
}
