'use strict';

/* globals MediaRecorder */

// Spec is at http://dvcs.w3.org/hg/dap/raw-file/tip/media-stream-capture/RecordingProposal.html

navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
if(getBrowser() == "Chrome"){
	var constraints = {"audio": true, "video": {  "mandatory": {  "minWidth": 640,  "maxWidth": 640, "minHeight": 480,"maxHeight": 480 }, "optional": [] } };//Chrome did not support the new constraints spec until 59 for video and 60 for audio
}else if(getBrowser() == "Firefox"){
	var constraints = {audio: true,video: {  width: { min: 640, ideal: 640, max: 640 },  height: { min: 480, ideal: 480, max: 480 }}}; //Firefox
}


var recBtn;
var pauseResBtn;
var stopBtn;
var videofile;
var videoElement;
var uploadVideoRecordingBtn;
var videoDuration = 0;
 var vidPreview;

$( document ).ready(function() {
  $.LoadingOverlay("show", {
      image       : baseUrl + "/images/icons/loading-couple.png",
       fade  : [2000, 1000],
      fontawesome : "fa fa-spinner fa-spin"
  });

recBtn = $("button#rec");
pauseResBtn = $("button#pauseRes");
stopBtn = $("button#stop");
uploadVideoRecordingBtn = $("#upload-video-recording-btn");
videoElement = document.querySelector("video#video-record");
videoElement.controls = false;
uploadVideoRecordingBtn.attr("recording-uploaded", false);
vidPreview = document.getElementById("video-preview");
recBtn.on("click", function(){
onBtnRecordClicked();
});

pauseResBtn.on("click", function(){
onPauseResumeClicked();
});

stopBtn.on("click", function(){
onBtnStopClicked();
});


//UPLOAD RECORDING
    uploadVideoRecordingBtn.on('click', function() {
        uploadVideoRecordingBtn.attr("disabled", true);
        uploadVideoRecordingBtn.addClass("loading");

        var formData = new FormData();
        formData.append('video', videofile);

        $.ajax({
            url: baseUrl + "api/v1/ads/video",
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function(data) {
                if (containsValidationErrors(data)) {} else {
                    $("#audio-input").val(data.id);
                    //$("#length-input").val(recordedSeconds);
                   vidPreview.src=data.url;
                    $.notify(
                        "Recording uploaded.", {
                            globalPosition: 'bottom right',
                            className: "success",
                            autoHide: true
                        }
                    );
                }
                //uploadRecordingBtn.removeClass("disabled");
                uploadVideoRecordingBtn.removeClass("loading");
                uploadVideoRecordingBtn.attr("recording-uploaded", true);
                $("#video-play-icon").show();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                 uploadVideoRecordingBtn.attr("disabled", false);
                uploadVideoRecordingBtn.removeClass("loading");

                if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });
    });

});



function errorCallback(error){
	console.log('navigator.getUserMedia error: ', error);
}

/*
var mediaSource = new MediaSource();
mediaSource.addEventListener('sourceopen', handleSourceOpen, false);
var sourceBuffer;
*/

var mediaRecorder;
var chunks = [];
var count = 0;

function startVideoRecording(stream) {
	log('Start recording...');
	if (typeof MediaRecorder.isTypeSupported == 'function'){
		/*
			MediaRecorder.isTypeSupported is a function announced in https://developers.google.com/web/updates/2016/01/mediarecorder and later introduced in the MediaRecorder API spec http://www.w3.org/TR/mediastream-recording/
		*/
		if (MediaRecorder.isTypeSupported('video/webm;codecs=vp9')) {
		  var options = {mimeType: 'video/webm;codecs=vp9'};
		} else if (MediaRecorder.isTypeSupported('video/webm;codecs=h264')) {
		  var options = {mimeType: 'video/webm;codecs=h264'};
		} else  if (MediaRecorder.isTypeSupported('video/webm;codecs=vp8')) {
		  var options = {mimeType: 'video/webm;codecs=vp8'};
		}
		log('Using '+options.mimeType);
		mediaRecorder = new MediaRecorder(stream, options);
	}else{
		log('isTypeSupported is not supported, using default codecs for browser');
		mediaRecorder = new MediaRecorder(stream);
	}

	pauseResBtn.textContent = "Pause";

	mediaRecorder.start(10);

	var url = window.URL || window.webkitURL;
	videoElement.src = url ? url.createObjectURL(stream) : stream;
	videoElement.play();

	mediaRecorder.ondataavailable = function(e) {
		//log('Data available...');
		//console.log(e.data);
		//console.log(e.data.type);
		//console.log(e);
		chunks.push(e.data);
	};

	mediaRecorder.onerror = function(e){
		log('Error: ' + e);
		console.log('Error: ', e);
	};


	mediaRecorder.onstart = function(){
		log('Started & state = ' + mediaRecorder.state);
	};

	mediaRecorder.onstop = function(){
		log('Stopped  & state = ' + mediaRecorder.state);

		var blob = new Blob(chunks, {type: "video/webm"});
		chunks = [];

		var videoURL = window.URL.createObjectURL(blob);

		videoElement.src = videoURL;

		var rand =  Math.floor((Math.random() * 10000000));
		var name  = "video_"+rand+".webm" ;

         videofile = new File([blob], name);
	};

	mediaRecorder.onpause = function(){
		log('Paused & state = ' + mediaRecorder.state);
	}

	mediaRecorder.onresume = function(){
		log('Resumed  & state = ' + mediaRecorder.state);
	}

	mediaRecorder.onwarning = function(e){
		log('Warning: ' + e);
	};
}

//function handleSourceOpen(event) {
//  console.log('MediaSource opened');
//  sourceBuffer = mediaSource.addSourceBuffer('video/webm; codecs="vp9"');
//  console.log('Source buffer: ', sourceBuffer);
//}

function onBtnRecordClicked (){
	 if (typeof MediaRecorder === 'undefined' || !navigator.getUserMedia) {
		alert('MediaRecorder not supported on your browser, use Firefox 30 or Chrome 49 instead.');
	}else {
		navigator.getUserMedia(constraints, startVideoRecording, errorCallback);
		recBtn.attr("disabled", true);
		pauseResBtn.attr("disabled", false);
		stopBtn.attr("disabled", false);
		uploadVideoRecordingBtn.attr("disabled", true);
	}
}

function onBtnStopClicked(){
	mediaRecorder.stop();
	videoElement.controls = true;

    recBtn.attr("disabled", false);
	pauseResBtn.attr("disabled", true);
	stopBtn.attr("disabled", true);
	uploadVideoRecordingBtn.attr("disabled", false);
    videoDuration = videoElement.duration;

}

function onPauseResumeClicked(){
	if(pauseResBtn=== "Pause"){
		console.log("pause");
		pauseResBtn.text("Resume");
		mediaRecorder.pause();
		stopBtn.attr("disabled", true);
	}else{
		console.log("resume");
		pauseResBtn.text("Pause");
		mediaRecorder.resume();
		stopBtn.attr("disabled", false);
	}
	recBtn.attr("disabled", true);
	pauseResBtn.attr("disabled", false);
}


function log(message){
       console.log();
	//dataElement.innerHTML = dataElement.innerHTML+'<br>'+message ;
}



//browser ID
function getBrowser(){
	var nVer = navigator.appVersion;
	var nAgt = navigator.userAgent;
	var browserName  = navigator.appName;
	var fullVersion  = ''+parseFloat(navigator.appVersion);
	var majorVersion = parseInt(navigator.appVersion,10);
	var nameOffset,verOffset,ix;

	// In Opera, the true version is after "Opera" or after "Version"
	if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
	 browserName = "Opera";
	 fullVersion = nAgt.substring(verOffset+6);
	 if ((verOffset=nAgt.indexOf("Version"))!=-1)
	   fullVersion = nAgt.substring(verOffset+8);
	}
	// In MSIE, the true version is after "MSIE" in userAgent
	else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
	 browserName = "Microsoft Internet Explorer";
	 fullVersion = nAgt.substring(verOffset+5);
	}
	// In Chrome, the true version is after "Chrome"
	else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
	 browserName = "Chrome";
	 fullVersion = nAgt.substring(verOffset+7);
	}
	// In Safari, the true version is after "Safari" or after "Version"
	else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
	 browserName = "Safari";
	 fullVersion = nAgt.substring(verOffset+7);
	 if ((verOffset=nAgt.indexOf("Version"))!=-1)
	   fullVersion = nAgt.substring(verOffset+8);
	}
	// In Firefox, the true version is after "Firefox"
	else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
	 browserName = "Firefox";
	 fullVersion = nAgt.substring(verOffset+8);
	}
	// In most other browsers, "name/version" is at the end of userAgent
	else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) <
		   (verOffset=nAgt.lastIndexOf('/')) )
	{
	 browserName = nAgt.substring(nameOffset,verOffset);
	 fullVersion = nAgt.substring(verOffset+1);
	 if (browserName.toLowerCase()==browserName.toUpperCase()) {
	  browserName = navigator.appName;
	 }
	}
	// trim the fullVersion string at semicolon/space if present
	if ((ix=fullVersion.indexOf(";"))!=-1)
	   fullVersion=fullVersion.substring(0,ix);
	if ((ix=fullVersion.indexOf(" "))!=-1)
	   fullVersion=fullVersion.substring(0,ix);

	majorVersion = parseInt(''+fullVersion,10);
	if (isNaN(majorVersion)) {
	 fullVersion  = ''+parseFloat(navigator.appVersion);
	 majorVersion = parseInt(navigator.appVersion,10);
	}


	return browserName;
}
