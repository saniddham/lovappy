$(document).ready(function() {



    // Initialize form validation on the  form.
    // It has the name attribute
    $("#music-edit-form").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side

            text: {
                required: true,
                // Specify that email should be validated
                // by the built-in "email" rule
                text: true
            },
            radio: {
                required: true
            }
        },
        // Specify validation error messages
        messages: {
            text: {
                required: "field is required"
            },
            radio: "field is required"
        }
//        // Make sure the form is submitted to the destination defined
//        // in the "action" attribute of the form when valid
//        submitHandler: function(form) {
//            form.submit();
//        }
    });
    var loadingHtml = "Submit <span class=\"ui button loading\"></span>";
    var saveBtn = $("#musicSubmitBtn");
    saveBtn.on('click', function(e) {
        // e.preventDefault();


        saveBtn.prop("disabled", true);
        saveBtn.find("i").show();
        $("#music-edit-form").submit();
    });

    $("#music-edit-form").submit(function() {

        $(this).ajaxSubmit({
            type: 'POST',
            crossDomain: true,
            headers: {
                Accept: "application/json; charset=utf-8"
            },

            url: baseUrl + "api/v1/music/" + musicId,
            success: function(data) {

                saveBtn.prop("disabled", false);
                saveBtn.find("i").hide();
                $("#popup-message-title").text("Successfully Submitted");
                $("#popup-message-text").text("Your Song is pending approval. You will receive a notification by email.");
                $('#message-popup').modal({
                    closable: false,
                    onApprove: function() {
                        window.location.href = baseUrl + "music/my-music";
                    }
                }).modal('show');

            },
            error: function(jqXHR, textStatus, errorThrown) {
                saveBtn.prop("disabled", false);
                saveBtn.find("i").hide();
                console.log(jqXHR);
                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }



        });

    });

    $("#upload-music-cover").on('change', function() {
        readURL(this, $('#coverPhoto'));
    });



});