/**
 * Created by Eias Altawil on 9/15/2017
 */
var playBtn;
var pauseBtn;
var progress;
var duration;
var timer;
var buyAndSend = false;
var lyricsSel = '';
$(document).ready(function() {


    var songDuration = 30;
//    $(window).on("load", function() {
//        $("#lyrics-content").mCustomScrollbar({
//            scrollButtons: {
//                enable: true,
//                scrollType: "stepped"
//            },
//            keyboard: {
//                scrollType: "stepped"
//            },
//            mouseWheel: {
//                scrollAmount: 188
//            },
//            theme: "rounded-dark",
//            autoExpandScrollbar: true,
//            snapAmount: 188,
//            snapOffset: 65
//        });
//
//    });
  $("#downloadMusic").click(function() {
  downloadMusic(musicID);
});

    //send music to the wanted user
    $("#sendMusic").click(function() {
        sendSongToUser(musicID, sendToUserId);
    });


    $("#deleteSong").click(function() {
        $("#delete-music").modal("show");
    });

    //send music to the wanted user
    $("#deleteMusic").click(function() {

        $.ajax({
            url: baseUrl + "api/v1/music/" + musicID,
            type: "DELETE",
            data: {},
            success: function(data) {
                $("#popup-message-title").text("Done");
                $("#popup-message-text").text("Song has been deleted successfully");
                $('#message-popup').modal({
                    closable: false,
                    onApprove: function() {
                        window.location.href = baseUrl + "music/my-music";
                    }
                }).modal('show');
            },

            error: function(jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });


    });

    $("#lyrics-content").click(function() {
        var isSend = $(this).attr("highlighted");
        console.log(isSend);
        if (isSend === 'true') {
 var range = window.getSelection().getRangeAt(0),
        span = document.createElement('span');

    span.className = 'highlight';
    span.appendChild(range.extractContents());
    range.insertNode(span);
        }
    });

    $("#buy-btn").click(function() {
        if (!isLoggedIn) {
            window.location.href = baseUrl + "login";
            return;
        }
        calculatePrice(getCalcPriceData());
        $('#square-payment-form').modal('show');
    });


    $("#showSendBuyLink").click(function() {
        if (!isLoggedIn) {
            window.location.href = baseUrl + "login";
            return;
        }
        calculatePrice(getCalcPriceData());
        buyAndSend = true;
        $('#square-payment-form').modal('show');
    });


    playBtn = $("#btn-play");
    // pauseBtn = $("#btn-pause");

    progress = document.getElementById("progress");
    duration = document.getElementById("duration");
    timer = document.getElementById("timer");

    /*
     *Play Music
     */
    duration.innerHTML = '0:30';
    Player = function() {
        var self = this;
        this.sound = new Howl({
            src: [musicUrl],
            sprite: {
                preview: [0, 30000]
            },
            html5: true, // Force to HTML5 so that the audio can stream in (best for large files).
            onplay: function() {
                showPlayControls(true);



                // Start updating the progress of the track.
                requestAnimationFrame(self.step.bind(self));
            },
            onload: function() {
                // Display the duration.

                if (self.sound.duration() < 30) {
                    songDuration = self.sound.duration();
                }
                duration.innerHTML = self.formatTime(Math.round(songDuration));

                showPlayControls(false);
            },
            onend: function() {
                showPlayControls(false);
            },
            onpause: function() {
                showPlayControls(false);
            },
            onstop: function() {
                showPlayControls(false);
            }
        });
    };

    Player.prototype = {
        play: function() {
            var self = this;

            // Begin playing the sound.
//              self.sound.sprite = {
//                            preview: [28000, 30000]
//                        };
//                        console.log("daoud")
            self.sound.play('preview');

        },

        pause: function() {
            var self = this;

            // Get the Howl we want to manipulate.
            var sound = self.sound;
            sound.pause();
        },

        seek: function(per) {
            var self = this;

            // Get the Howl we want to manipulate.
            var sound = self.sound;

            // Convert the percent into a seek position.
            if (sound.playing()) {
                sound.seek(songDuration * per);
            }
        },

        step: function() {
            var self = this;

            // Get the Howl we want to manipulate.
            var sound = self.sound;

            // Determine our current seek position.
            var seek = sound.seek() || 0;

            timer.innerHTML = self.formatTime(Math.round(seek));
            progress.style.width = (((seek / songDuration) * 100) || 0) + '%';

            // If the sound is still playing, continue stepping.
            if (sound.playing()) {
                requestAnimationFrame(self.step.bind(self));
            }
        },

        formatTime: function(secs) {
            var minutes = Math.floor(secs / 60) || 0;
            var seconds = (secs - minutes * 60) || 0;

            return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
        },

        isPlaying: function() {
            var self = this;

            // Get the Howl we want to manipulate.
            var sound = self.sound;

            return sound && sound.playing();
        }
    };

    progress.addEventListener('click', function(event) {
        player.seek(event.clientX / window.innerWidth);
    });

    var player = new Player();

    $("#btn-play").click(function() {
        if (player.isPlaying()) {
            player.pause();
        } else {
            player.play();
             //player.step();
        }
    });

    //Rating
    $("#rateYo").rateYo({
        starWidth: "20px",
        ratedFill: "rgba(236, 116, 59, 1)",
        fullStar: true,
        rating: rating,
        readOnly: alreadyRated
    });

    $("#rateYo").rateYo().on("rateyo.set", function(e, data) {
        $.ajax({
            url: baseUrl + "api/v1/music/rate",
            type: "POST",
            data: {
                music_id: musicID,
                rating: data.rating
            },
            success: function(data) {toastr.success("Rating updated successfully !");},
            error: function(jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });
    });

});

function showPlayControls(play) {
    if (play) {
        playBtn.removeClass("fa-play");
        playBtn.addClass("fa-pause");
        //pauseBtn.hide();
    } else {
        playBtn.removeClass("fa-pause");
        playBtn.addClass("fa-play");
    }
}

function getCalcPriceData() {
    return {
        music_id: musicID,
        coupon: $("#coupon-code").val()
    };
}

function sendSongToUser(musicId, userId) {
console.log($("#lyrics-content").html());
    if (userId != null) {
        $.ajax({
            url: baseUrl + "api/v1/music/" + musicId + "/send",
            type: "POST",
            data: {
                userId: userId,
                lyrics:  $("#lyrics-content").html()
            },
            success: function(data) {
                $("#popup-message-title").text("Done");
                $("#popup-message-text").text("Song has been sent successfully");
                $('#message-popup').modal({
                    closable: false,
                    onApprove: function() {
                        window.location.href = baseUrl + "member/" + userId;
                    }
                }).modal('show');
            },

            error: function(jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 400)
                    showValidationMessages(jqXHR.responseJSON);
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });
    }

}

function processPaymentForm(nonce) {
    $.ajax({
        url: baseUrl + "api/v1/music/" + musicID + "/order",
        type: "POST",
        data: {
            nonce: nonce,
            coupon: $("#coupon-code").val()
        },
        success: function(data) {

            if (buyAndSend) {
                sendSongToUser(musicID, sendToUserId);
                buyAndSend = false;
            } else {
                $("#popup-message-title").text("Done");
                $("#popup-message-text").text("Payment Made Successfully");
                $('#message-popup').modal({
                    closable: false,
                    onApprove: function() {
                        location.reload();
                    }
                }).modal('show');
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 400)
                showValidationMessages(jqXHR.responseJSON);
            else if (jqXHR.status === 401)
                window.location.href = baseUrl + "login";
            else if (jqXHR.status === 500)
                $.notify("Order not executed", {
                    globalPosition: 'bottom right',
                    className: "error",
                    autoHide: true
                });
        },
        complete: function(jqXHR, textStatus) {
            $requestNonceBtn.removeClass("loading disabled");
        }
    });
}