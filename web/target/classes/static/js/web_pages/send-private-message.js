/**
 * Created by Eias Altawil on 5/21/17
 */

window.AudioContext = window.AudioContext || window.webkitAudioContext;

var audioInput;
var audioContext = new AudioContext();
var realAudioInput = null,
    inputPoint = null;
var recorder;
var file;

var recordedAudio;
var recordButtonText;
var buttonCircle;

var recordedSeconds = 0;
var recordedMinutes = 0;

var userID;


//Timer Vars
var count = 0;
var clearTime;
var seconds = recordedSeconds,
    minutes = 0,
    hours = 0;
var clearState;
var secs = formatTime(seconds, true);
var mins = formatTime(minutes, false);
var gethours;

$( document ).ready(function() {
    userID = document.getElementById('userID').value;

    initAudio();

    recordedAudio = document.querySelector('audio#recorded');

    recordButtonText = document.querySelector('#record-btn-text');
    buttonCircle = document.querySelector('#round-button-circle');

    // window.isSecureContext could be used for Chrome
    var isSecureOrigin = location.protocol === 'https:' ||
        location.hostname === 'localhost';
    if (!isSecureOrigin) {
        //alert('getUserMedia() must be run from a secure origin: HTTPS or localhost.' +
          //  '\n\nChanging protocol to HTTPS');
        location.protocol = 'HTTPS';
    }

    recordedAudio.addEventListener('ended',function() {
        applyPlayStyle();
    });


    document.getElementById("timer").innerHTML = mins + secs;
});

function toggleRecording() {
    if (recordButtonText.dataset.action === 'Record') {
        startRecording();
        startTime(false);
    } else if (recordButtonText.dataset.action === 'StopRecording') {
        finishRecording();
    } else if (recordButtonText.dataset.action === 'Play') {
        play();
        startTime(true);
        recordButtonText.textContent = 'Stop';
        recordButtonText.dataset.action = 'StopPlaying';
        buttonCircle.classList.add('round-button-circle-red');
        buttonCircle.classList.remove('round-button-circle-green');
    } else if (recordButtonText.dataset.action === 'StopPlaying') {
        recordedAudio.pause();
        applyPlayStyle();
    }
}

function applyPlayStyle() {
    stopTime();
    recordButtonText.textContent = 'Play';
    recordButtonText.dataset.action = 'Play';
    buttonCircle.classList.add('round-button-circle-green');
    buttonCircle.classList.remove('round-button-circle-red');
}

function finishRecording() {
    recordedSeconds = seconds;
    recordedMinutes = minutes;
    stopRecording();
    stopTime();
    recordButtonText.textContent = 'Play';
    recordButtonText.dataset.action = 'Play';
    buttonCircle.classList.add('round-button-circle-green');
    buttonCircle.classList.remove('round-button-circle-red');
    //document.getElementById("timer").style.display = "none";
    $('#send_pm').prop('disabled', false);
}

function startRecording() {
    recorder.startRecording();
    console.log('Recording...');

    recordButtonText.textContent = 'Stop';
    recordButtonText.dataset.action = 'StopRecording';
}

function stopRecording() {
    recorder.finishRecording();
    console.log('Stopped recording.');
}

function resetRecording() {
    stopRecording();
    recordedSeconds = 0;
    recordedMinutes = 0;
    stopTime();
    recordButtonText.textContent = 'Record';
    recordButtonText.dataset.action = 'Record';
    buttonCircle.classList.add('round-button-circle-red');
    buttonCircle.classList.remove('round-button-circle-green');
}

function play() {
    if(recordFile){
        recordedAudio.src = recordFile;
    }else{
        recordedAudio.play();
    }
}

function send() {
    if (recordedSeconds > 3) {
        calculatePrice(getCalcPriceData());
        $('#square-payment-form').modal('show');
    } else {
        toastr.error("You should record at least three seconds to continue.");
        resetRecording();
    }
}

function upload(nonce) {
    if($("#language").val() === "") {
        alert(selectLangMsg);
        return;
    }
    var saveBtn = $('#send_pm');
    saveBtn.prop('disabled', true);
    saveBtn.find('i').show();

    var fd = new FormData();
    fd.append('file', file);
    fd.append('userID', userID);
    fd.append('nonce', nonce);
    fd.append('languageID', $("#language").val());
    fd.append("coupon", $("#coupon-code").val());

    $.ajax({
        url: baseUrl + "private_message/send",
        type: "POST",
        data: fd,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        success: function () {
            alert("Message Sent");
            redirectToMember();
        },
        error: function () {
            saveBtn.find('i').hide();
            saveBtn.prop('disabled', false);
             toastr.error("Your payment card is not valid !!");
             toastr.error("You should record at least three seconds to continue.");

        },
        complete: function (jqXHR, textStatus) {
            $requestNonceBtn.removeClass("loading disabled");
        }
    });

}

function processPaymentForm(nonce){
    upload(nonce);
}

/*
*Recorder Timer Script
*/

function startWatch(reverse) {
    /* check if seconds is equal to 60 and add a +1 to minutes, and set seconds to 0 */
    if (seconds === 60 && !reverse) {
        seconds = 0;
        minutes = minutes + 1;
    }

    if (seconds < 0 && reverse) {
        seconds = 59;
        minutes = minutes - 1;
    }

    /* you use the javascript tenary operator to format how the minutes should look and add 0 to minutes if less than 10 */
    mins = formatTime(minutes, false);
    //mins = (minutes < 10) ? ('0' + minutes + ': ') : (minutes + ': ');

    /* check if minutes is equal to 60 and add a +1 to hours set minutes to 0 */
    if (minutes === 60 && !reverse) {
        minutes = 0;
        hours = hours + 1;
    }

    if (minutes < 0 && reverse) {
        minutes = 59;
        hours = hours - 1;
    }

    /* you use the javascript tenary operator to format how the hours should look and add 0 to hours if less than 10 */
    gethours = formatTime(hours, false);
    //gethours = (hours < 10) ? ('0' + hours + ': ') : (hours + ': ');
    secs = formatTime(seconds, true);
    //secs = (seconds < 10) ? ('0' + seconds) : (seconds);

    // display the stopwatch
    var x = document.getElementById("timer");
    x.innerHTML = /*gethours +*/ mins + secs;

    /* call the seconds counter after displaying the stop watch and increment seconds by +1 to keep it counting */
    if(reverse){
        seconds--;
    }else {
        seconds++;
    }

    if(reverse && seconds < 0){
        seconds = 0;
        stopTime();
        return;
    }

    if(!reverse && seconds > 30){
        finishRecording();
        return;
    }

    /* call the setTimeout( ) to keep the stop watch alive ! */
    clearTime = setTimeout("startWatch(" + reverse + ")", 1000);
}

//create a function to start the stop watch
function startTime(reverse) {

    /* check if seconds, minutes, and hours are equal to zero and start the stop watch */
    //if (seconds === 0 && minutes === 0 && hours === 0) {

    /* hide the start button if the stop watch is running */
    //this.style.display = "none";

    /* call the startWatch( ) function to execute the stop watch whenever the startTime( ) is triggered */
    startWatch(reverse);
    //}
} // startwatch.js end

//create a function to stop the time
function stopTime() {

    /* check if seconds, minutes and hours are not equal to 0 */
    //if (seconds !== 0 || minutes !== 0 || hours !== 0) {

    // reset the stop watch
    seconds = recordedSeconds;
    minutes = recordedMinutes;
    //hours = 0;
    secs = (seconds < 10) ? ('0' + seconds) : (seconds);
    mins = (minutes < 10) ? ('0' + minutes + ': ') : (minutes + ': ');
    //gethours = '0' + hours + ': ';

    /* display the stopwatch after it's been stopped */
    var x = document.getElementById("timer");
    var stopTime = /*gethours +*/ mins + secs;
    x.innerHTML = stopTime;

    /* clear the stop watch using the setTimeout( )
     return value 'clearTime' as ID */
    clearTimeout(clearTime);
    //}
} // stopwatch.js end

function formatTime(time, isSeconds) {
    if(isSeconds)
        return (time < 10) ? ('0' + time) : (time);
    else
        return (time < 10) ? ('0' + time + ': ') : (time + ': ');
}


function redirectToMember() {
    window.location = baseUrl + "/member/" + userID
}

function gotStream(stream) {
    inputPoint = audioContext.createGain();

    // Create an AudioNode from the stream.
    realAudioInput = audioContext.createMediaStreamSource(stream);
    audioInput = realAudioInput;
    audioInput.connect(inputPoint);

    recorder = new WebAudioRecorder(inputPoint, {
        workerDir: baseUrl + "js/WebAudioRecorder/",     // must end with slash
        encoding: "mp3",
        options: {
            timeLimit: 30,
            mp3: {bitRate: 64}
        }
    });

    recorder.onComplete = function(rec, blob) {
        applyPlayStyle();

        recordedAudio.src = window.URL.createObjectURL(blob);

        var filename = (new Date).toISOString().replace(/:|\./g, '-') + '.mp3';
        file = new File([blob], filename);

        console.log('Recording Completed.');
    };

    recorder.onError = function(recorder, message) {
        console.log(message);
    };
}

function initAudio() {
    if (!navigator.getUserMedia)
        navigator.getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
    if (!navigator.cancelAnimationFrame)
        navigator.cancelAnimationFrame = navigator.webkitCancelAnimationFrame || navigator.mozCancelAnimationFrame;
    if (!navigator.requestAnimationFrame)
        navigator.requestAnimationFrame = navigator.webkitRequestAnimationFrame || navigator.mozRequestAnimationFrame;

    navigator.getUserMedia(
        {
            "audio": {
                "mandatory": {
                    "googEchoCancellation": "false",
                    "googAutoGainControl": "false",
                    "googNoiseSuppression": "false",
                    "googHighpassFilter": "false"
                },
                "optional": []
            },
        }, gotStream, function(e) {
            //alert('Error getting audio');
            console.log(e);
        });
}

function getCalcPriceData() {
    return {coupon: $("#coupon-code").val()};
}