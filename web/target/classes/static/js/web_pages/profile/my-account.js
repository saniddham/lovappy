/**
 * Created by Eias Altawil on 7/7/2017
 */

$(document).ready(function () {


    //initialization
    if ($("#emailNotification").is(":checked")) {
        $("#new-likes1").attr('disabled', false);
        $("#new-matches1").attr('disabled', false);
        $("#new-messages1").attr('disabled', false);
        $("#new-songs1").attr('disabled', false);
        $("#new-gifts1").attr('disabled', false);
        $("#unlock-profile").attr('disabled', false);
        $("#song-approval").attr('disabled', false);
        $("#song-denials").attr('disabled', false);
        $("#new-song-purchase").attr('disabled', false);
        $('.label-border label').addClass('active');
    }

    $("#activate").on('click', function () {
        var $this = $(this);
        $this.prop("disabled", true);
        var activate = $(this).attr("activate");
        if (activate == 'true') {
            $(".modal[data-edit='deactivate-account']").modal("show");
        }
        else {
            activateOrDeactivate("POST");
        }
    });
    $("#deactivate-btn").on('click', function () {
        var $this = $(this);
        $this.prop("disabled", true);
        activateOrDeactivate("DELETE");
    });


    var resetBtn = $("#reset-password-btn");
    resetBtn.click(function (e) {

        e.preventDefault();
        resetBtn.addClass("loading");
        resetBtn.prop('disabled', true);

        $.ajax({
            url: baseUrl + "my_account/send_reset_email",
            type: "POST",
            success: function (data) {
                $(".modal[data-edit='reset-password']").modal("show");
                resetBtn.removeClass("loading");
                resetBtn.prop('disabled', false);
            },
            error: function (e) {
                $(".modal[data-edit='reset-password']").modal("hide");
                resetBtn.removeClass("loading");
                resetBtn.prop('disabled', false);
                console.log(e);
            }
        });
    });

    $("#emailNotification").change(function () {
        if ($(this).is(":checked")) {
            $('#settingsFrom').find('input[name="emailNotification"]').val(false)
            $("#new-likes1").attr('disabled', false);
            $("#new-matches1").attr('disabled', false);
            $("#new-messages1").attr('disabled', false);
            $("#new-songs1").attr('disabled', false);
            $("#new-gifts1").attr('disabled', false);
            $("#unlock-profile").attr('disabled', false);
            $("#song-approval").attr('disabled', false);
            $("#song-denials").attr('disabled', false);
            $("#new-song-purchase").attr('disabled', false);


            //$('.label-border label').addClass('active');
            return;
        } else {
            $('#settingsFrom').find('input[name="emailNotification"]').val(true)
            $("#new-likes1").attr('disabled', true);
            $("#new-matches1").attr('disabled', true);
            $("#new-messages1").attr('disabled', true);
            $("#new-songs1").attr('disabled', true);
            $("#new-gifts1").attr('disabled', true);
            $("#unlock-profile").attr('disabled', true);
            $("#song-approval").attr('disabled', true);
            $("#song-denials").attr('disabled', true);
            $("#new-song-purchase").attr('disabled', true);
            //$('.label-border label').removeClass('active');
            return;
        }

    });


    var loadingHtml = "Save <span class=\"ui button loading\"></span>";
    var saveBtn = $("#save-btn");
    saveBtn.on('click', function (e) {
        // e.preventDefault();


        saveBtn.prop("disabled", true);
        saveBtn.find("i").show();
        $("#settingsFrom").submit();
    });

    $("#settingsFrom").submit(function (e) {

        e.preventDefault();
        $(this).ajaxSubmit({
            type: 'POST',
            crossDomain: true,
            async: false,
            headers: {
                Accept: "application/json; charset=utf-8"
            },

            url: baseUrl + "api/v1/user/settings/save",
            success: function (data) {

                saveBtn.prop("disabled", false);
                saveBtn.find("i").hide();
                location.reload();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                saveBtn.prop("disabled", false);
                saveBtn.find("i").hide();
                console.log(jqXHR);
                if (jqXHR.status === 400) {
                    $("#popup-message-title").text("Failed!");
                    $("#popup-message-text").text("Failed to update settings!");
                    $('#message-popup').modal({
                        closable: false,
                        onApprove: function () {
                            location.reload();
                        }
                    }).modal('show');
                }
                else if (jqXHR.status === 401)
                    window.location.href = baseUrl + "login";
            }
        });

    });

    $("#buy-recording-btn").click(function () {
        if (!isLoggedIn) {
            window.location.href = baseUrl + "login";
            return;
        }
        if ($('#response-count').val() > 3) {
            toastr.error("Only upto 3 recordings can be selected once");
        } else {
            calculatePrice(getCalcPriceData($('#response-count').val()));
            $('#square-payment-form').modal('show');
        }
    });
});

function getCalcPriceData(quantity) {
    return {
        coupon: $("#coupon-code").val(),
        //responses: attributeIds.filter(onlyUnique).toString(),
        quantity: quantity
    };
}

function processPaymentForm(nonce) {
    $.ajax({
        url: baseUrl + "api/v1/questions/order",
        type: "POST",
        data: {
            nonce: nonce,
            responses: attributeIds.filter(onlyUnique).toString(),
            coupon: $("#coupon-code").val()
        },
        success: function (data) {

            $("#popup-message-title").text("Done");
            $("#popup-message-text").text("Payment Made Successfully");
            $('#message-popup').modal({
                closable: false,
                onApprove: function () {
                    location.reload();
                }
            }).modal('show');

        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 400)
                showValidationMessages(jqXHR.responseJSON);
            else if (jqXHR.status === 401)
                window.location.href = baseUrl + "login";
            else if (jqXHR.status === 500)
                $.notify("Order not executed", {
                    globalPosition: 'bottom right',
                    className: "error",
                    autoHide: true
                });
        },
        complete: function (jqXHR, textStatus) {
            $requestNonceBtn.removeClass("loading disabled");
        }
    });
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function activateOrDeactivate(method) {
    $.ajax({
        url: baseUrl + "/api/v1/user/activate",
        type: method,
        data: {},
        success: function () {
            window.location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 400)
                showValidationMessages(jqXHR.responseJSON);
            else if (jqXHR.status === 401)
                window.location.href = baseUrl + "login";
        }
    });
}
