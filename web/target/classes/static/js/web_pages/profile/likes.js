/**
 * Created by Daoud on 26/7/2017
 */
var soundId;
var sound;
var prevUrl;
$(document).ready(function() {
    var lovdropPlayImage = "/images/icons/lovdrops_play_icons/btn_play.png";
    var lovdropPauseImage = "/images/icons/lovdrops_play_icons/btn_pause.png";
    var lovdropUrl = "";

    /*
     *Play LovDrop
     */
    var lovdropProfilePauseImage = "/images/icons/lovdrops_play_icons/btn_pause.png";
    var lovdropProfilePlayImage = "/images/icons/lovdrops_play_icons/btn_play.png";

    var playBtn = $(".playBtn");
    var playLovDropButton;

    playBtn.click(function() {
        var url = $(this).attr("data-url");
        var userId = $(this).attr("data-id");
        var userAddress = $(this).attr("data-userAddress");
        var userGender = $(this).attr("data-userGender");
        var isBlocked = $(this).attr("data-blocked");
        var redirectUrl = $(this).attr("data-redirectUrl");
        console.log("blocked play ? " + isBlocked);
        //set all with play icon
        playLovDropButton = playBtn.find("img");
        playLovDropButton.attr("src", baseUrl + lovdropProfilePlayImage);

        playLovDropButton = $("#" + userId);

        play(url, userId, userAddress, userGender, isBlocked, redirectUrl);
    });

    function play(url, userId, userAddress, gender, isBlocked, redirectUrl) {

        var maleIcon = "/images/new-male-icon.png";
        var femaleIcon = "/images/new-female-icon.png";
        if (sound) {
            if (sound.playing()) {


                sound.stop(soundId);

                if (prevUrl === url) {
                    return;
                }
            } else {

                if (prevUrl === url) {

                    sound.play(soundId);
                    return;
                }
            }

        }
        prevUrl = url;
        sound = new Howl({
            src: [url],
            html5: true, // Force to HTML5 so that the audio can stream in (best for large files).
            onplay: function() {
                playLovDropButton.attr("src", baseUrl + lovdropProfilePauseImage);
                $("#listeningTo").css("display", "inline-block");
                $("#genderListenerImg").css("display", "inline-block");
                $("#genderListenerImg").attr("src", baseUrl + ((gender === 'MALE') ? maleIcon : femaleIcon));
                $("#viewProfile").css("display", "block");
                $('#viewProfile').attr("href", baseUrl + "/member/" + userId);

                $('#listenerAddress').text(userAddress);
                $('#listnerLike').css("display", "block");
                $('#listnerBlock').css("display", "block");
                $('#listnerBlock').data('action', isBlocked === 'true' ? 'unblock' : 'block');
                $('#listnerBlock').data('userId', userId);

                $("#imgBlockBtn").attr('src', baseUrl + (isBlocked === 'true' ? '/images/icons/unblock.png' : '/images/icons/block.png'));

                //                $("#listenerAddress").atrr("text", user.)
            },
            onload: function() {
                playLovDropButton.attr("src", baseUrl + lovdropProfilePlayImage);
                $("#listeningTo").css("display", "none");
                $("#genderListenerImg").css("display", "none");
                $("#viewProfile").css("display", "none");
                $("#genderListenerImg").css("display", "none");
                $('#listenerAddress').text('');
                $('#listnerLike').css("display", "none");
                $('#listnerBlock').css("display", "none");



            },
            onend: function() {
                playLovDropButton.attr("src", baseUrl + lovdropProfilePlayImage);
                $("#listeningTo").css("display", "none");
                $("#genderListenerImg").css("display", "none");
                $("#viewProfile").css("display", "none");
                $("#genderListenerImg").css("display", "none");
                $('#listenerAddress').text('');
                $('#listnerLike').css("display", "none");
                $('#listnerBlock').css("display", "none");

            },
            onpause: function() {
                playLovDropButton.attr("src", baseUrl + lovdropProfilePlayImage);
                $("#listeningTo").css("display", "none");
                $("#genderListenerImg").css("display", "none");
                $("#viewProfile").css("display", "none");
                $("#genderListenerImg").css("display", "none");
                $('#listenerAddress').text('');
                $('#listnerLike').css("display", "none");
                $('#listnerBlock').css("display", "none");

            },
            onstop: function() {
                playLovDropButton.attr("src", baseUrl + lovdropProfilePlayImage);
                $("#listeningTo").css("display", "none");
                $("#genderListenerImg").css("display", "none");
                $("#viewProfile").css("display", "none");
                $("#genderListenerImg").css("display", "none");
                $('#listenerAddress').text('');
                $('#listnerLike').css("display", "none");
                $('#listnerBlock').css("display", "none");
                listenToLovdrop(userId);
            }
        });

        if (sound.playing()) {

            soundId = sound.stop();
        } else {

            soundId = sound.play();

        }

    //End Block Section

    }
        //Block Section

        blockBtn = $("#listnerBlock");

        blockBtn.click(function() {

            var action = $(this).data('action');
            var userId = $(this).data('userId');
            if (action === 'block') {
                $(".modal[data-edit='block-modal']").modal("show");
            } else {
                userBlockUnblock(userId);
            }

        });

        $('#modalBlockBtn').click(function() {
            var userId = blockBtn.data('userId');
            blockUser($("#imgBlockBtn"), userId);
            isBlocked = true;
            console.log("userId " + userId);
            $(".modal[data-edit='block-modal']").modal("hide");

            window.location.href = redirectUrl;
        });

        function userBlockUnblock(userId) {
            var action = blockBtn.data('action');
            if (action === 'block') {
                $(".modal[data-edit='block-modal']").modal("show");
                return;
            }
            console.log("userId un block  " + userId);
            unBlockUser($("#imgBlockBtn"), userId);
            isBlocked = false;
            window.location.href = redirectUrl;
        }



        $("#flagUserBtn").click(function() {

            $(".modal[data-edit='flag-modal']").modal("show");

        });

        $('#submit-flag').click(function(e) {
            e.preventDefault();

            $('input[name=redirectUrl]').val(redirectUrl);
            $('#flagForm').attr('action', baseUrl + '/member/' + userId + '/report');
            $('#flagForm').submit();


        });




});