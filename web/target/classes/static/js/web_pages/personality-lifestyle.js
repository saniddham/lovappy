/**
 * Created by Eias Altawil on 6/18/2017
 */

$(document).ready(function () {

    $("#back-button").on('click', function(e) {
        e.preventDefault();
          window.location.href = baseUrl + "/registration/ageheightsizelang";
      });
    var submitBtn = $("#submit-btn");

    submitBtn.on('click', function(e) {
        e.preventDefault();
        submitBtn.addClass("disabled");
        submitBtn.addClass("loading");

        $.ajax({
            url: baseUrl + "api/v1/registration/step3",
            type: "POST",
            data: $('#main-form').serialize(),
            success: function (data) {
                window.location.href = baseUrl + 'registration/personalitystatus';
                submitBtn.removeClass("disabled");
                submitBtn.removeClass("loading");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                submitBtn.removeClass("disabled");
                submitBtn.removeClass("loading");

                if(jqXHR.status === 400) {
                    showValidationMessages(jqXHR.responseJSON);
                    toastr.error("Please fill all required fields !!");
                }else if(jqXHR.status === 401) {
                    window.location.href = baseUrl + "login";
                }
            }
        });
    });
});
