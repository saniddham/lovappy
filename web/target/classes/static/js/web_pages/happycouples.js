var googleMapLoaded = false;

function setGoogleMapLoaded() {
    googleMapLoaded = true;
}
$(document).ready(function() {
    $('.active-modal').click(function() {
        $('#events-loggedout-modal').modal('show');

    });

    $('.active-modal').on('click', function(e) {
        e.preventDefault();
        $('#approve-message-article').modal('show');
    });

    function getLocation() {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            $("#couple-location").text("Not Available");
        }
    }

    function showPosition(position) {
        geocodeLatLng(position);
    }

    function geocodeLatLng(position) {
        var geocoder = new google.maps.Geocoder();
        var couplesLocation = "";
        var city = "";
        var country = "";
        var state = "";
        var latlng = {
            lat: parseFloat(position.coords.latitude),
            lng: parseFloat(position.coords.longitude)
        };
        geocoder.geocode({
            'location': latlng
        }, function(results, status) {
            if (status === 'OK') {
                if (results.length > 0) {
                    var result = results[0];
                    console.log('Setting user location lat:lng to ' + latlng.lat + ':' + latlng.lng);

                    var addressComponents = result.address_components;
                    for (var i = 0; i < addressComponents.length; i++) {
                        var addressComponent = addressComponents[i];
                        var addressComponentTypes = addressComponent.types;
                        for (var j = 0; j < addressComponentTypes.length; j++) {
                            if (addressComponentTypes[j] === 'country') {
                                country = addressComponent.long_name;
                                break;
                            } else if (addressComponentTypes[j] === 'administrative_area_level_1') {
                                state = addressComponent.long_name;
                                break;
                            } else if (addressComponentTypes[j] === 'locality') {
                                city = addressComponent.long_name;
                                break;
                            }
                        }
                    }

                    $("#couple-location").text(city + ", " + state + ", " + country);

                } else {
                    $("#couple-location").text("Not Available");
                }
            } else {
                $("#couple-location").text("Not Available");
            }
        });
    }
    getLocation();

});