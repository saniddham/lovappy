/**
 * Created by Manoj Senevirathne on 07/01/2018
 */

$(document).ready(function () {

$('.ambassador-calendar .col-6:last-child label').on('click', function (e) {
            e.preventDefault();
            $('.ambassador-calendar .col-6:last-child label').removeClass('active');
            $(this).addClass('active');
        });


        $(function () {
                $("#birthdate").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-100:+0",
                });

            });

//    $('#birthdate').calendar({type: 'date'});
    var submitBtn = $("#register-author");


    submitBtn.on('click', function (e) {
        e.preventDefault();
        submitBtn.addClass("disabled");
        submitBtn.addClass("loading");

        var x = $('#author-email').validate()
            & $('#birthdate').validate()
            & validatePassword()
            & $('#author-confirm-password').validate()
            & $('#author-password').validate()
            & $('#author-zipcode').validate();

        if (x) {
            var url = (!!$('#user-id').val()) ? "/user/registration/signup" : "/user/registration/new/signup";
            $.ajax({
                url: baseUrl + url,
                type: "POST",
                data: $('#reg-form').serialize(),
                success: function (data) {
                    if (containsValidationErrors(data)) {
                        submitBtn.removeClass("disabled");
                        submitBtn.removeClass("loading");
                    } else {
                        $('#author-modal').show();
                        submitBtn.removeClass("disabled");
                        submitBtn.removeClass("loading");
                         window.location.href = baseUrl + "blog/submit"
                    }
                },
                error: function (data) {
                    submitBtn.removeClass("disabled");
                    submitBtn.removeClass("loading");
                    console.log(data);
                    toastr.error('Error');
                }
            });
        } else {
            toastr.error($('#fill-all-fields').val());
              submitBtn.addClass("disabled");
                    submitBtn.addClass("loading");
        }

    });

    $('#events-checkuser').click(function () {
        var userId = $('#loggedInuserId').val();
        console.log(userId);
        if (userId === '' || jQuery.type(userId) === null) {
            $('#events-loggedout-modal').modal('show');
        } else {
            window.location.href = baseUrl + '/user/registration/signup';
        }
    });

    (function ($) {
        $.fn.validate = function () {
            var success = true;
            if ($(this).val() == '') {
                $(this).css('border', '1px solid red');
                success = false;
            } else
                $(this).css('border', '1px solid #898989');

            return success;
        }

    }(jQuery));

    (function ($) {
        $.fn.validateConfirmForm = function () {
            var success = true;
            if (!$(this).is(':checked')) {
                $('#confirmTerms').css({
                    'display': 'inline-block',
                    'background-color': '#fff'
                }).html('Please agree to proceed');
                success = false;
            } else
                $('#confirmTerms').html('');

            return success;
        }

    }(jQuery));

    function validatePassword() {
        var password = $("#ambassador-password").val();
        var confirmPassword = $("#ambassador-confirm-password").val();
        if (password != confirmPassword) {
            $("#ambassador-password").css('border', '1px solid red');
            $("#ambassador-confirm-password").css('border', '1px solid red');
            toastr.error($('#password-not-equal').val());
            toster
            return false;
        }
        $("#ambassador-password").css('border', '1px solid #898989');
        $("#ambassador-confirm-password").css('border', '1px solid #898989');
        return true;
    }

});


function containsValidationErrors(response) {
    $('.error-msg').each(function () {
        $(this).text('');
        $(this).hide();
    });

    if (response !== null && response.errorMessageList !== null && typeof response.errorMessageList !== 'undefined') {
        var errorMessages;
        for (var i = 0; i < response.errorMessageList.length; i++) {

            if (!!errorMessages) {
                errorMessages += response.errorMessageList[i].message + "</br>";
            } else {
                errorMessages = response.errorMessageList[i].message + "</br>";
            }
            toastr.error(response.errorMessageList[i].message);

        }
        if (!!errorMessages) {
            $('#confirmTerms').html(errorMessages);
            $('#confirmTerms').show();
        }
        return true;
    }
    return false;
}



