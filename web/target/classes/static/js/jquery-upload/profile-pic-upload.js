$(function() {
    var $recordLovDropDiv = $('#record-lovdrop');
    var $uploadHandsDiv = $('#upload-hands');
    var $uploadFeetDiv = $('#upload-feet');
    var $uploadLegsDiv = $('#upload-legs');
    var $uploadProfileDiv = $('#upload-profile-pic');

    var showIfHidden = function($div) {
        var show = $div.css('display') === 'none';
        $('.upload-container').hide();
        if(show) {
            $div.css('display', 'block');
        }
    };
    $('#record-container').click(function() {
        showIfHidden($recordLovDropDiv);
    });
    var $handsContainer = $('#hands-container').click(function() {
        showIfHidden($uploadHandsDiv);
    });
    var $feetContainer = $('#feet-container').click(function() {
        showIfHidden($uploadFeetDiv);
    });
    var $legsContainer = $('#legs-container').click(function() {
        showIfHidden($uploadLegsDiv);
    });

    var $profileContainer = $('#profile-pic-container').click(function() {
        showIfHidden($uploadProfileDiv);
    });
    var createOptions = function(url, $uploadContainer, $uploadedContainer) {
        return {
            url: url,
            dataType: 'json',
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            previewCrop: true,
            add: function(e, data) {
                // Replace image in preview container
                var $previewFileDiv = $('.preview-file', $uploadContainer);
                if(data.files && data.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('img', $previewFileDiv).attr('src', e.target.result);
                    };
                    reader.readAsDataURL(data.files[0]);
                }
                // Hook to on click of of do-upload-button so that it'll submit the data on click.
                var $doUploadButton = $('.do-upload-button', $uploadContainer);
                $doUploadButton.off('click'); // Remove previous click event
                $doUploadButton.click(function() { // Register new click event
                    var $progressDiv = $('.progress', $previewFileDiv);
                    //$progressDiv.css('width', 0);
                    $progressDiv.css('visibility', 'visible');
                    // data.context = $('<p/>').text('Uploading...').replaceAll($(this)); // No loader yet
                    data.submit();
                });
            },
            progress: function(e, data) {
                /*var progress = parseInt(data.loaded / data.total * 100, 10);
                var $progressDiv = $('.progress', $uploadContainer);
                $progressDiv.css('width', progress + '%');*/
            },
            done: function(e, data) {
                /*var $progressDiv = $('.progress', $uploadContainer);
                console.log("Generated file id: " + data.result.id);
                $progressDiv.css('visibility', 'hidden');
                if(data.files && data.files[0]) {
                    $uploadedContainer.find('.upload_hidden_message').hide();
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('img', $uploadedContainer).attr('src', e.target.result);
                        $uploadedContainer.find("p").hide();
                    };
                    reader.readAsDataURL(data.files[0]);
                }*/
            },
            stop: function (e) {
                location.reload();
            }
        }
    };
    $('input[type="file"]', $uploadHandsDiv).fileupload(createOptions(baseUrl + 'api/v1/user/profile/hands/upload', $uploadHandsDiv, $handsContainer));
    $('input[type="file"]', $uploadFeetDiv).fileupload(createOptions(baseUrl + 'api/v1/user/profile/feet/upload', $uploadFeetDiv, $feetContainer));
    $('input[type="file"]', $uploadLegsDiv).fileupload(createOptions(baseUrl + 'api/v1/user/profile/legs/upload', $uploadLegsDiv, $legsContainer));
    //$('input[type="file"]', $uploadProfileDiv).fileupload(createOptions(baseUrl + 'api/v1/user/profile/photo', $uploadProfileDiv, $profileContainer));

    var defineSkipButtonAction = function(skipPhotoUrl, skipPhotoImgSrc, $uploadContainer, $uploadedContainer) {
          var $previewFileDiv = $('.preview-file', $uploadContainer);
          var $progressDiv = $('.progress', $previewFileDiv);

        $('.skip-upload-button', $uploadContainer).click(function() {
           $progressDiv.css('visibility', 'visible');
            $.ajax({
                url: skipPhotoUrl,
                type: 'PUT',
                crossDomain: true,
                headers: {
                     Accept: "application/json; charset=utf-8"
                  },
                success: function() {
                    $('.do-upload-button', $uploadContainer).off('click');
                    $('.skip-upload', $uploadContainer).show();
                     $('img', $uploadContainer).hide();

                    $('.skip-upload', $uploadedContainer).show();
                    $('img', $uploadedContainer).hide();
                     $progressDiv.css('visibility', 'hidden');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log('Failed to mark photo as skipped. TextStatus: ' + textStatus + ' Error: ' + errorThrown);
                    alert('Failed to mark photo as skipped.');
                    $progressDiv.css('visibility', 'hidden');
                }
            });
        });
    };
    defineSkipButtonAction(baseUrl + 'api/v1/user/profile/hands/skip', baseUrl + 'images/skipped_hand_photo.png', $uploadHandsDiv, $handsContainer);
    defineSkipButtonAction(baseUrl + 'api/v1/user/profile/feet/skip', baseUrl + 'images/skipped_foot_photo.png', $uploadFeetDiv, $feetContainer);
    defineSkipButtonAction(baseUrl + 'api/v1/user/profile/legs/skip', baseUrl + 'images/skipped_legs_photo.png', $uploadLegsDiv, $legsContainer);

     $('input[name=choosePicAccess]').on("change", function() {
       var value = $('input[name=choosePicAccess]:checked').val();

       if(value ==='PUBLIC'){
         $(".managedCounter").hide();
           $("#managedText").hide();
           $("#customText").hide();
          $("#publicText").show();
       }else if(value === 'MANAGED') {
       $(".managedCounter").show();
       $("#managedText").show();
          $("#customText").hide();
            $("#publicText").hide();
       } else {
         $(".managedCounter").hide();
         $("#managedText").hide();
          $("#customText").show();
            $("#publicText").hide();
       }
     });

    $('#letsGo').click(function() {
        var $errorContainer = $('#error_container');
        if(recordFile === null&& recordedSeconds >= 30) {
            //$errorContainer.text('LovDrop recording is required.\nYou should record at least 10 seconds to continue.');
            //$errorContainer.show();
            $("#popup-message-title").text('Voice Recording is required');
            $("#popup-message-text").html('Please save your recording voice first!!.');
            $("#message-popup").modal('show');

           } else  if(recordFile === null || recordedSeconds < 30) {
                              //$errorContainer.text('LovDrop recording is required.\nYou should record at least 10 seconds to continue.');
                              //$errorContainer.show();
                              $("#popup-message-title").text('Voice Recording is required');
                              $("#popup-message-text").html('Voice Recording is required. You should record at least 30 seconds to continue.');
                              $("#message-popup").modal('show');

             }else if(  $('img', $handsContainer).attr('src').indexOf('upload_hand.png') >= 0 && $('img', $handsContainer).is(":visible")
                            || $('img', $feetContainer).attr('src').indexOf('upload_foot.png') >= 0 && $('img', $feetContainer).is(":visible")
                            || $('img', $legsContainer).attr('src').indexOf('upload_legs.png') >= 0&& $('img', $legsContainer).is(":visible")) {
            $("#popup-message-title").text('Upload Photos');
            $("#popup-message-text").html('Skip each photos if you don\'t like to upload one for now.');
            $("#message-popup").modal('show');
            //$errorContainer.text('Skip each photos if you don\'t like to upload one for now.');
            //$errorContainer.show();
        }else if($('img', $profileContainer).attr('src').indexOf('upload_profile_sample.png') >= 0){
            $("#popup-message-title").text('Upload Profile Photo');
            $("#popup-message-text").html('Profile photo is required.');
            $("#message-popup").modal('show');
        } else {
            callAccessProfilePicAPI($('input[name=choosePicAccess]:checked').val(), $("#managedCounter").val());
            //Post subscription and return to radio
            var type = 'LITE';
            $.ajax({
               url: baseUrl + "api/v1/registration/subscription",
               type: "POST",
               data: {subscriptionType: type},
               success: function (data) {
               if(data==='not-social'){
               	      window.location.href = baseUrl + 'registration/subscription';
               	   }
                   else{
                      window.location.href = baseUrl+'radio'
                   }

               },
               error: function (jqXHR, textStatus, errorThrown) {
                   if(jqXHR.status === 400)
                       showValidationMessages(jqXHR.responseJSON);
                   else if(jqXHR.status === 401)
                       window.location.href = baseUrl + "login";
               }
           });

        }
    });

    var windowHref = window.location.href;
    if(windowHref.indexOf('#record-lovdrop') !== -1) {
        showIfHidden($recordLovDropDiv);
    } else if(windowHref.indexOf('#upload-hands') !== -1) {
        showIfHidden($uploadHandsDiv);
    } else if(windowHref.indexOf('#upload-feet') !== -1) {
        showIfHidden($uploadFeetDiv);
    } else if(windowHref.indexOf('#upload-legs') !== -1) {
        showIfHidden($uploadLegsDiv);
    } else if(windowHref.indexOf('#upload-profile-pic') !== -1) {
        showIfHidden($uploadProfileDiv);
    }
});


   function callAccessProfilePicAPI(access, counter) {
            $.ajax({
                url: baseUrl + "/api/v1/user/profile/photo/access",
                type: "POST",

                data:  JSON.stringify({"access":access, "maxTransactions":counter}),
                contentType: "application/json; charset=utf-8",
                success: function () {
               return true;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                               if(jqXHR.status === 400)
                                   showValidationMessages(jqXHR.responseJSON);
                               else if(jqXHR.status === 401)
                                   window.location.href = baseUrl + "login";
                           }
            });
    }