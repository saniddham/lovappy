/**
 * Created by hasan on 7/6/2017.
 */
// JQuery required

$(document).ready(function () {

$(document).ajaxStart(function(){
  $.LoadingOverlay("show", {
      image       : baseUrl + "/images/icons/loading-couple.png",
       fade  : [2000, 1000],
      fontawesome : "fa fa-spinner fa-spin"
  });
});
$(document).ajaxStop(function(){
    $.LoadingOverlay("hide", true);
});
    $('.btn-close-modal').click(function () {
        $('.modal').modal('hide');
    });

    $('.mobile-menu-btn').click(function () {
            $( this ).toggleClass('active');
            $( this ).parent().find('.main-nav .clearfix').toggleClass('active');
        });

    /* scroll to top */

    $(document).on('scroll resize', function(event) {
        if ($(window).scrollTop() > 150) {

            $('.sticky-nav').addClass('active');
        }
        else if ($(window).scrollTop() < 150) {

            $('.sticky-nav').removeClass('active');
        }
    });

    /* end of scrool to top */

    if(typeof popupMessage !== 'undefined' && popupMessage !== null){
        $("#popup-message-title").text(popupMessageTitle);
        $("#popup-message-text").text(popupMessageText);
        $("#message-popup").modal('show');

    }

    $(' body').on("click", "#resendBtn",function () {
      resendVerficationEmail();
    });

    <!--region Remove and Set selected classes-->
    $(' body').on("click", ".uni-select span a",function () {
        console.log($(this).text());
        $(this).siblings('input').prop("checked", true);
        $(this).parent().parent().children().children('a').removeClass("selected");
        $(this).addClass("selected");
    });


    $(".multi-select span a").on("click",function () {
        console.log($(this).hasClass('selected'));
        if ($(this).hasClass('selected')) {
            $(this).siblings('input').prop("checked", false);
            $(this).removeClass("selected");
        } else {
            if ($(this).hasClass('none')) {
                $(this).parent().parent().children().children('a').removeClass("selected");
                $(this).parent().parent().children().children('input').prop("checked", false);
                $(this).siblings('input').prop("checked", true);
                $(this).addClass("selected");
            } else {
                $(this).parent().parent().children().children('a.none').removeClass("selected");
                $(this).parent().parent().children().children('input.none').prop("checked", false);
                $(this).siblings('input').prop("checked", true);
                $(this).siblings(".none").removeClass("selected");
                $(this).addClass("selected");
            }
        }
    });
    $("input[type='radio']:checked, input[type='checkbox']:checked").each(function () {
        $(this).siblings("a").addClass("selected");
    });
    <!--endregion-->
});

function trimText(charNumber, string){
    var trimmedString = string.substring(0, charNumber);
    return trimmedString + '..';
}

    function readURL(input, selector) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                selector
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function showValidationMessages(messages){
        if (messages !== null && typeof messages !== 'undefined') {
            for (var i = 0; i < messages.length; i++) {
                var item = messages[i];
                var $controlGroup = $('#' + item.fieldName + 'FormGroup');
                var $errorMsg = $controlGroup.find('.error-msg');
                $errorMsg.text(item.message);
                $errorMsg.show();
            }
        }
    }

function showNotifyErrorMessages(messages) {
    var options = {
        globalPosition: 'bottom right',
        className: "error",
        autoHide: true
    };

    if (messages !== null && typeof messages !== 'undefined') {
        for (var i = 0; i < messages.length; i++) {
            var item = messages[i];
            $.notify(item.fieldName + ": " + item.message, options);
        }
    }
}

function showPopupMessage(title, body) {
    $("#popup-message-title").text(title);
    $("#popup-message-text").text(body);
    $("#message-popup").modal('show');
}




       String.prototype.trim = function() {
           return this.replace(/^\s+|\s+$/g,"");
         }


         function validURL(url) {
           var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
          return pattern.test(url);
         }

         function validateEmail(email) {
             var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
             return re.test(email.toLowerCase());
         }


         /**
          * Fetch dictionary data and return array of translation strings
          **/
         function getDictionary(languageCode) {
         	if (!languageCode) {
                 languageCode = "en";
         	}
             $.i18n.properties({
                 name: 'messages',
                 language: languageCode,
                 path: baseUrl + 'languages/',
                 mode: 'map',
                 cache: true
             });
             return $.i18n.map;
         }


         //listen to Lovdrop
         function resendVerficationEmail() {
               $.ajax({
                   url: baseUrl + "api/v1/user/verification/resend",
                   type: "GET",
                   data: {},
                   success: function () {

                   },
                   error: function (e) {
                       console.log(e);
                   }
               });
         }

function isDate(txtDate) {
    var currVal = txtDate;
    if (currVal == '')
        return false;

    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    console.log(dtArray);

    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[1];
    dtDay = dtArray[3];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }
    return true;
}

function isValidBirthDate(txtDate) {
    var currVal = txtDate;
    if (currVal == '')
        return false;

    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    console.log(dtArray);

    //Checks for mm/dd/yyyy format.

    var dtYear = parseInt(dtArray[5]);
    var dt = new Date();
    var currentYear = dt.getFullYear();
    if ((currentYear -  dtYear )< 18)
        return false;

    return true;
}