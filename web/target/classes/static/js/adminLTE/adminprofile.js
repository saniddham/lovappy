  $(document).ready(function() {

      $("#inpImage").on('change', function() {
          readURL(this, $('#profilePic'));
      });

      $("#changePwdBtn").click(function() {
          console.log("btn clicked" + $("#currentPassword").val())

          $("#changePasswordForm").valid();
          var payload = {
              currentPassword: $("#currentPassword").val(),
              newPassword: $("#newPassword").val()

          };

        $("#changePasswordForm").ajaxSubmit({type: 'POST',
                                async: false,
                                crossDomain: true,
                                 datatype : "application/json",
                                 headers: {
                                        Accept : "application/json; charset=utf-8"

                                    },

                                url: baseUrl + "/lox/profile/password/save"

                                ,

                                              success: function() {
                                                  console.log("sucess");
                                                  $("#modalChangePassword").modal('hide');
                                              },
                                              error: function(e) {
                                                 // $("#modalChangePassword").modal('hide');
                                                  console.log(e);
                                              }


                                });

  });
   });