     // wait for the DOM to be loaded
     $(document).ready(function() {
         // bind 'myForm' and provide a simple callback function
         $('#btn-approve').click(function() {
             var saveBtn = $(this);
             saveBtn.prop("disabled", true);
             saveBtn.find("i").show();
             var status = $("#musicStatus").val();
             var action;

             if (status === 'APPROVED') {
                 action = 'reject';
             } else {
                 action = 'approve'
             }

             $.ajax({
                 url: baseUrl + 'lox/music/' + musicId + '/' + action,
                 type: "POST",
                 contentType: 'application/json; charset=UTF-8',
                 success: function() {

                     saveBtn.prop("disabled", false);
                     saveBtn.find("i").hide();
                     if (status === 'APPROVED') {
                         saveBtn.html('Approve <i class="fa fa-spinner fa-spin" style="display: none;"></i>');

                         $("#musicStatus").val("REJECTED ");
                         saveBtn.removeClass("btn-danger");
                         saveBtn.addClass("btn-success");

                     } else {
                         saveBtn.html('Reject <i class="fa fa-spinner fa-spin" style="display: none;"></i>');
                         $("#musicStatus").val("APPROVED")
                         saveBtn.removeClass("btn-success");
                         saveBtn.addClass("btn-danger")
                     }

                     // return true;
                 },
                 error: function(e) {

                     saveBtn.prop("disabled", false);
                     saveBtn.find("i").hide();

                 }
             });

         });
     });