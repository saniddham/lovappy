var googleMapLoaded = false;
// Called in gender.html and my-profile.html
function setGoogleMapLoaded() {
    googleMapLoaded = true;
}
var map;

$(document).ready(function() {



    $(".showMap").click(function() {
        var lat = $(this).attr("data-lat");
        var lng = $(this).attr("data-lng");
        var userId = $(this).attr("data-id");
        setTimeout(function() {

            InitializeMap(userId, lat, lng);
        }, 500);
    });

    function InitializeMap(userId, lat, lng) {
        if (!googleMapLoaded) {
            setTimeout(InitializeMap, 500);
        } else {
            var latlng = new google.maps.LatLng(lat, lng);
            var myOptions = {
                zoom: 13,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var marker = new google.maps.Marker({
                position: latlng
            });
            map = new google.maps.Map(document.getElementById("google-maps-container" + userId), myOptions);
            marker.setMap(map);
        }
    }
    
    
    $(".photosupdatebutton").click(function(e) {
    	e.preventDefault();
    	var id = $(this).closest("tr").attr("id");
    	var rowNum = id.slice(3);
    	var userprofileaudioId = "approveprofileaudio"+rowNum;
    	var searchArea = "#"+id+' '+"input[type='radio']:checked";
    	var formData = new FormData();
    	 var inputs = $(searchArea);
    	 formData["userId"] = rowNum;
    	    $.each(inputs, function (obj, v) {
    	    	var userprofileaudioId = $(v).attr("name");
    	    		formData[v.name] =  v.value;
    	    });
    	    
                $.ajax({
                    url: baseUrl + "lox/users/updateapproval",
                    type: "POST",
                     data:  JSON.stringify(formData),
                    cache : false,
                    contentType: "application/json; charset=utf-8",
                    processData: false,
                    success: function (data) {
                    	$("#success-alert").show();
                    	setTimeout(function() { $("#success-alert").hide(); }, 5000);
                       return true;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    	$("#fail-alert").show();
                    	setTimeout(function() { $("#fail-alert").hide(); }, 5000);
                    	return false;
                    }
                });
    });
    
    //Add onclick event to radio buttons
    $("input[type='radio']").change(function() {
		var id = $(this).closest("tr").attr("id");
    	var rowNum = id.slice(3);
    	var formData = new FormData();
    	formData["userId"] = rowNum;
    	formData[this.name] =  $(this).val();
        $.ajax({
            url: baseUrl + "lox/users/updateapproval",
            type: "POST",
             data:  JSON.stringify(formData),
            cache : false,
            contentType: "application/json; charset=utf-8",
            processData: false,
            success: function (data) {
            	$("#success-alert").show();
            	setTimeout(function() { $("#success-alert").hide(); }, 1000);
               return true;
            },
            error: function (jqXHR, textStatus, errorThrown) {
            	$("#fail-alert").show();
            	setTimeout(function() { $("#fail-alert").hide(); }, 1000);
            	return false;
            }
        });
        
    });
    
    var sound = null;
    var playClass =  $(".fa.fa-play");
    var pauseClass =  $(".fa.fa-pause");
    
    
    playClass.click(function(){
    	var id = $(this).closest("tr").attr("id");
    	var rowNum = id.slice(3);
    	var spanId = 'span'+rowNum;
    	var recordfile = $('#'+spanId).text();
    	
    	var audioId = 'audio'+rowNum;
    	var audioPauseId = 'audiopause'+rowNum;
    	
    	if (sound != null && sound.playing()){
    		sound.stop();
    		$('#'+audioPauseId).attr("style", "display:none;");
        	$('#'+audioId).attr("style", "display:block;");
    	} else{
    		sound = new Howl({
                src: [recordfile],
                html5: true, // Force to HTML5 so that the audio can stream in (best for large files).
                onplay: function () {
                	$('#'+audioId).attr("style", "display:none;");
                	$('#'+audioPauseId).attr("style", "display:block;");
                  },
                onload: function () {
                },
                onend: function () {
                	
                },
                onpause: function () {
                	$('#'+audioPauseId).attr("style", "display:none;");
                	$('#'+audioId).attr("style", "display:block;");
                },
                onstop: function () {
                	$('#'+audioPauseId).attr("style", "display:none;");
                	$('#'+audioId).attr("style", "display:block;");
                }
            });
            
            if (sound.playing()) {
                sound.stop();
            } else {
                sound.play();
            }
            
           
    	}
    });
    
    //for pause
    pauseClass.click(function(){
    	var id = $(this).closest("tr").attr("id");
    	var rowNum = id.slice(3);
    	var spanId = 'span'+rowNum;
    	var recordfile = $('#'+spanId).text();
    	
    	var audioId = 'audio'+rowNum;
    	var audioPauseId = 'audiopause'+rowNum;
    	
    	if (sound != null && sound.playing()){
    		sound.stop();
    		$('#'+audioPauseId).attr("style", "display:none;");
        	$('#'+audioId).attr("style", "display:block;");
    	} else{
    		sound = new Howl({
                src: [recordfile],
                html5: true, // Force to HTML5 so that the audio can stream in (best for large files).
                onplay: function () {
                	$('#'+audioId).attr("style", "display:none;");
                	$('#'+audioPauseId).attr("style", "display:block;");
                  },
                onload: function () {
                },
                onend: function () {
                	
                },
                onpause: function () {
                	$('#'+audioPauseId).attr("style", "display:none;");
                	$('#'+audioId).attr("style", "display:block;");
                },
                onstop: function () {
                	$('#'+audioPauseId).attr("style", "display:none;");
                	$('#'+audioId).attr("style", "display:block;");
                }
            });
            
            if (sound.playing()) {
                sound.stop();
            } else {
                sound.play();
            }
            
           
    	}
    });
});