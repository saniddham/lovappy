$(document).ready(function () {
var canvas  = $("#canvas");
var  context = canvas.get(0).getContext("2d");
var $result = $('#result');

$('#fileInput').on( 'change', function(){
    $("#modalPhoto").attr("hidden", true);
    if (this.files && this.files[0]) {
     if ( this.files[0].type.match(/^image\//) ) {

        var reader = new FileReader();
        reader.onload = function(evt) {
           var img = new Image();
           img.onload = function() {
                console.log("file clear" + canvas.width);


             context.canvas.height = img.height;
             context.canvas.width  = img.width;
             context.drawImage(img, 0, 0);
             var cropper = canvas.cropper({
               aspectRatio: 1 /1
             });
             $('#btnCrop').click(function() {
                // Get a string base 64 data url
                var croppedImageDataURL = canvas.cropper('getCroppedCanvas').toDataURL("image/png");

                $result.append( $('<img>').attr('src', croppedImageDataURL) );
             });
             $('#btnRestore').click(function() {

               canvas.cropper('reset');
               $result.empty();
             });
           };
           img.src = evt.target.result;
				};
        reader.readAsDataURL(this.files[0]);
      }
      else {
        alert("Invalid file type! Please select an image file.");
      }
    }
    else {
      alert('No file(s) selected.');
    }
});

});