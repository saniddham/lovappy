/**
 * Created by Eias Altawil on 6/7/2017.
 */

var errorMessages;
var $requestNonceBtn;

// Initializes the payment form. See the documentation for descriptions of
    // each of these parameters.
var paymentForm = new SqPaymentForm({
        applicationId: squareAppID,
        inputClass: 'sq-input',
        inputStyles: [
            {
                fontSize: '15px',
                padding: '5px'
            }
        ],
        cardNumber: {
            elementId: 'sq-card-number',
            placeholder: '•••• •••• •••• ••••'
        },
        cvv: {
            elementId: 'sq-cvv',
            placeholder: 'CVV'
        },
        expirationDate: {
            elementId: 'sq-expiration-date',
            placeholder: 'MM/YY'
        },
        postalCode: {
            elementId: 'sq-postal-code'
        },
        callbacks: {

            // Called when the SqPaymentForm completes a request to generate a card
            // nonce, even if the request failed because of an error.
            cardNonceResponseReceived: function(errors, nonce, cardData) {
                errorMessages.html('');
                if (errors) {
                    console.log("Encountered errors:");

                    // This logs all errors encountered during nonce generation to the
                    // Javascript console.
                    errors.forEach(function(error) {
                        errorMessages.append('• ' + error.message + '<br/>');
                        console.log('  ' + error.message);
                    });
                      $requestNonceBtn.removeClass("loading disabled");
                    // No errors occurred. Extract the card nonce.
                } else {
                    processPaymentForm(nonce);

                    // Delete this line and uncomment the lines below when you're ready
                    // to start submitting nonces to your server.
                    //alert('Nonce received: ' + nonce);


                    /*
                     These lines assign the generated card nonce to a hidden input
                     field, then submit that field to your server.
                     Uncomment them when you're ready to test out submitting nonces.

                     You'll also need to set the action attribute of the form element
                     at the bottom of this sample, to correspond to the URL you want to
                     submit the nonce to.
                     */
                    // document.getElementById('card-nonce').value = nonce;
                    // document.getElementById('nonce-form').submit();

                }
            },

            unsupportedBrowserDetected: function() {
                // Fill in this callback to alert buyers when their browser is not supported.
            },

            // Fill in these cases to respond to various events that can occur while a
            // buyer is using the payment form.
            inputEventReceived: function(inputEvent) {
                switch (inputEvent.eventType) {
                    case 'focusClassAdded':
                        // Handle as desired
                        break;
                    case 'focusClassRemoved':
                        // Handle as desired
                        break;
                    case 'errorClassAdded':
                        // Handle as desired
                        break;
                    case 'errorClassRemoved':
                        // Handle as desired
                        break;
                    case 'cardBrandChanged':
                        // Handle as desired
                        break;
                    case 'postalCodeChanged':
                        // Handle as desired
                        break;
                }
            },

            paymentFormLoaded: function() {
                // Fill in this callback to perform actions after the payment form is
                // done loading (such as setting the postal code field programmatically).
                // paymentForm.setPostalCode('94103');
            }
        }
    });

// This function is called when a buyer clicks the Submit button on the webpage
// to charge their card.
function requestCardNonce(event) {

    // This prevents the Submit button from submitting its associated form.
    // Instead, clicking the Submit button should tell the SqPaymentForm to generate
    // a card nonce, which the next line does.
    event.preventDefault();

    paymentForm.requestCardNonce();
}

$(document).ready(function () {

    var isSecureOrigin = location.protocol === 'https:' || location.hostname === 'localhost';
    if (!isSecureOrigin)
        location.protocol = 'HTTPS';

    $('#square-payment-form').modal();
    $requestNonceBtn = $("#btn-request-nonce");

    errorMessages = $('#error-messages');

    $requestNonceBtn.click(function () {
        $(this).addClass("loading disabled");
        paymentForm.requestCardNonce();
    });

    $("#btn-close-modal").click(function () {
        $("#square-payment-form").modal('hide');
    });

    var wto;
    $("#coupon-code").bind('input propertychange', function() {
        clearTimeout(wto);
        wto = setTimeout(function() {
            calculatePrice(getCalcPriceData());
        }, 1500);
    });
});


function calculatePrice(calcPriceData) {
    var $couponDiv = $("#coupon-code").closest("div");
    $couponDiv.addClass("loading");

    $.ajax({
        url: calcPriceUrl,
        type: "GET",
        data: calcPriceData,
        success: function (data) {
            $couponDiv.removeClass("loading");
            $("#payment-amount").text(data.price);
            $("#ad-total").text(data.totalPrice);

            if(data === "" || data.coupon === null){
                $("#coupon-discount").text("0%");

                if($("#coupon-code").val() !== "") {
                    $.notify("Invalid coupon code", {
                        globalPosition: 'bottom right',
                        className: "error",
                        autoHide: true
                    });
                }
            }else{
                $("#coupon-discount").text(data.coupon.discountPercent + "%");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $couponDiv.removeClass("loading");
            if(jqXHR.status === 400)
                showNotifyErrorMessages(jqXHR.responseJSON);
        }
    });
}