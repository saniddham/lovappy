package com.realroofers.lovappy.common.jpa;


import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

/**
 * Hibernate naming strategy that would make corresponding table name of an entity to plural.
 * Eg. Given entity class
 * <pre>
 * {@code
 * @Entity
 * public class User {}
 * </pre>
 * Generated table name would be <em>users</em>
 *
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public class ApplicationPhysicalNamingStrategy implements PhysicalNamingStrategy {
    private static final Map<String, String> ABBREVIATIONS = buildAbbreviationMap();

    public ApplicationPhysicalNamingStrategy() throws Exception {
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties");
        if (is != null) {
            try {
                Properties properties = new Properties();
                properties.load(is);
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    // DO nothing
                }
            }
        }
    }

    private static Map<String, String> buildAbbreviationMap() {
        TreeMap<String, String> abbreviationMap = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
//        abbreviationMap.put("account", "acct");
//        abbreviationMap.put("number", "num");
        return abbreviationMap;
    }

    @Override
    public Identifier toPhysicalCatalogName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        // Acme naming standards do not apply to catalog names
        return name;
    }

    @Override
    public Identifier toPhysicalSchemaName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        // Acme naming standards do not apply to schema names
        return null;
    }

    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        String tableName = name.getText();
        String upperCasedTableName = tableName.toUpperCase();
        
        if (!upperCasedTableName.endsWith("S")&& !upperCasedTableName.endsWith("Y")) {
            tableName += "s";
        }
        final List<String> parts = splitAndReplace(tableName);
        return jdbcEnvironment.getIdentifierHelper().toIdentifier(
            join(parts),
            name.isQuoted()
        );
    }

    @Override
    public Identifier toPhysicalSequenceName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        final LinkedList<String> parts = splitAndReplace(name.getText());
        // Acme Corp says all sequences should end with _seq
        if (!"seq".equalsIgnoreCase(parts.getLast())) {
            parts.add("seq");
        }
        return jdbcEnvironment.getIdentifierHelper().toIdentifier(
            join(parts),
            name.isQuoted()
        );
    }

    @Override
    public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        final List<String> parts = splitAndReplace(name.getText());
        return jdbcEnvironment.getIdentifierHelper().toIdentifier(
            join(parts),
            name.isQuoted()
        );
    }

    private LinkedList<String> splitAndReplace(String name) {
        LinkedList<String> result = new LinkedList<>();
        for (String part : StringUtils.splitByCharacterTypeCamelCase(name)) {
            if (part == null || part.trim().isEmpty() || "_".equals(part)) {
                // skip null and space
                continue;
            }
            part = applyAbbreviationReplacement(part);
            result.add(part.toLowerCase(Locale.ROOT));
        }
        return result;
    }

    private String applyAbbreviationReplacement(String word) {
        if (ABBREVIATIONS.containsKey(word)) {
            return ABBREVIATIONS.get(word);
        }

        return word;
    }

    private String join(List<String> parts) {
        boolean firstPass = true;
        String separator = "";
        StringBuilder joined = new StringBuilder();
        for (String part : parts) {
            joined.append(separator).append(part);
            if (firstPass) {
                firstPass = false;
                separator = "_";
            }
        }
        return joined.toString();
    }
}