package com.realroofers.lovappy.service;

import java.util.Properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@SpringBootApplication // same as @Configuration @EnableAutoConfiguration @ComponentScan
@EntityScan("com.realroofers.lovappy.service.**.model")
public class ApplicationTest {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(ApplicationTest.class, args);
    }

    @Bean
    public JavaMailSender mailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        Properties properties = new Properties();
        properties.put("mail.debug", false);
        properties.put("mail.transport", false);
        properties.put("mail.smtp.port", 3025); // important: Port 3025 is the default port used by GreenMail
        properties.put("mail.smtp.auth", true);
        properties.put("mail.smtp.user", "user@test.com");
        properties.put("mail.smtp.host", "localhost");
        properties.put("mail.smtp.from", "from@test.com");
        mailSender.setJavaMailProperties(properties);
        mailSender.setUsername("test");
        mailSender.setPassword("test");
        mailSender.setDefaultEncoding("UTF-8");
        return mailSender;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }
}
