package com.realroofers.lovappy.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationTest.class)
@Transactional
@Ignore
public abstract class AbstractTest {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    // Add methods here that you think can be reused by all tests

    protected <T> void assertMapEquals(Map<String, T> map1, Map<String, T> map2) {
        assertEquals(map1.size(), map2.size());
        for (Map.Entry<String, T> entry : map1.entrySet()) {
            String key = entry.getKey();
            T value = entry.getValue();
            assertTrue("Map does not contain key " + key, map2.containsKey(key));
            assertEquals(value, map2.get(key));
        }
    }
}
