package com.realroofers.lovappy.service;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetupTest;
import com.realroofers.lovappy.service.mail.MailService;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Eias on 28-Apr-17
 */


public class MailServiceTest extends AbstractTest {
    @Autowired
    private MailService mailService;

    private GreenMail smtpServer;

    @Before
    public void setUp() throws Exception {
        // smtpServer = new GreenMail(new ServerSetup(25, null, "smtp"));
        /*
        The above line will cause
        Exception in thread "smtp:25" java.lang.IllegalStateException: Can not open server socket for smtp:25
        at com.icegreen.greenmail.server.AbstractServer.initServerSocket(AbstractServer.java:118)
        at com.icegreen.greenmail.server.AbstractServer.run(AbstractServer.java:84)
        Caused by: java.net.BindException: Permission denied
         in linux
         */
        smtpServer = new GreenMail(ServerSetupTest.SMTP);
        smtpServer.start();
    }

    @Ignore
    @Test
    public void shouldSendMail() throws Exception {
        //given
        String recipient = "eiasxv@gmail.com";
        String content = "Eias Altawil requesting password reset.";
        //when
        mailService.sendMail(recipient, content, "test");
        //then
        assertReceivedMessageContains(content);
    }

    private void assertReceivedMessageContains(String expected) throws IOException, MessagingException {
        assertTrue(smtpServer.waitForIncomingEmail(5000, 1));
        MimeMessage[] receivedMessages = smtpServer.getReceivedMessages();
        assertEquals(1, receivedMessages.length);
        String content = (String) receivedMessages[0].getContent();
        assertTrue(content.contains(expected));
    }

    @After
    public void tearDown() throws Exception {
        smtpServer.stop();
    }
}
