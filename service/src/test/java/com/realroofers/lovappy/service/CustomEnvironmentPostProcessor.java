package com.realroofers.lovappy.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;

import com.google.common.collect.Maps;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public class CustomEnvironmentPostProcessor implements EnvironmentPostProcessor {
    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        Path tmp = Paths.get(System.getProperty("java.io.tmpdir"));
        Path testDir = tmp.resolve("lovappy-test");
        if (Files.notExists(testDir)) {
            try {
                Files.createDirectories(testDir);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        Map<String, Object> source = Maps.newHashMap();
        source.put("lovappy.uploadLocation", testDir.toAbsolutePath().toString());
        environment.getPropertySources().addLast(new MapPropertySource("test", source));
    }
}
