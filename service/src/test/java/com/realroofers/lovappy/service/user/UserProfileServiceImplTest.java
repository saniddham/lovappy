package com.realroofers.lovappy.service.user;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

import com.realroofers.lovappy.service.AbstractTest;
import com.realroofers.lovappy.service.user.dto.MemberProfileDto;
import com.realroofers.lovappy.service.user.dto.UserAuthDto;
import com.realroofers.lovappy.service.user.dto.UserProfileDto;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.repo.UserProfileRepo;
import com.realroofers.lovappy.service.user.support.Gender;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public class UserProfileServiceImplTest extends AbstractTest {
    private static final Integer USER_ID = 1;
    @Mock
    private UserProfileService service;
    @Mock
    private UserProfileRepo repo;

    @Before
    public void setup() {
        SecurityContext securityContext = new SecurityContextImpl();
        UserAuthDto principal = new UserAuthDto(USER_ID);
        TestingAuthenticationToken authentication = new TestingAuthenticationToken(principal, null);
        securityContext.setAuthentication(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Ignore
    @Test
    public void saveOrUpdate() {
        UserProfileDto dto = new UserProfileDto();
        dto.setUserId(USER_ID);
        dto.setGender(Gender.MALE);
        Calendar bdateCal = Calendar.getInstance();
        bdateCal.setTimeInMillis(0);
        bdateCal.set(1984, 4, 8);
        dto.setBirthDate(bdateCal.getTime());
        dto.setAllowedProfilePic(true);
        service.saveOrUpdate(dto);

        UserProfile actual = repo.findByUserId(USER_ID);
        assertEquals(Gender.MALE, actual.getGender());
        assertEquals(bdateCal.getTime(), actual.getBirthDate());
        //
        MemberProfileDto  actual2 = service.findFullOne(USER_ID);
        assertEquals(bdateCal.getTime(), actual2.getBirthDate());
    }

    private void testUpload(BiConsumer<String, InputStream> saveHander, Supplier<InputStream> streamSupplier) throws Exception {
        // Prepare Data
        UserProfile userProfile = new UserProfile(USER_ID);
        repo.save(userProfile);
        String fileName = "helloworld.txt";
        String contents = "Hello World!";
        InputStream stream = new ByteArrayInputStream(contents.getBytes(StandardCharsets.UTF_8));
        // Test
        saveHander.accept(fileName, stream);
        try (InputStream actualStream = streamSupplier.get()) {
            assertEquals(contents, IOUtils.toString(actualStream, StandardCharsets.UTF_8));
        }
    }
    /*
    @Test
    public void saveFeetPhoto() throws Exception {
        // Prepare Data
        UserProfile userProfile = new UserProfile(USER_ID);
        repo.saveOrUpdate(userProfile);
        String fileName = "helloworld.txt";
        String contents = "Hello World!";
        InputStream stream = new ByteArrayInputStream(contents.getBytes(StandardCharsets.UTF_8));
        String generatedFileId = UUID.randomUUID().toString();
        when(storageRepo.write(USER_ID.toString(), fileName, stream)).thenReturn(generatedFileId);
        // Test
        service.saveFeetPhoto(fileName, stream);
        UserProfile actual = repo.findOne(USER_ID);
        assertEquals(generatedFileId, actual.getFeetFileId());
    }

    @Test
    public void saveLegsPhoto() throws Exception {
        // Prepare Data
        UserProfile userProfile = new UserProfile(USER_ID);
        repo.saveOrUpdate(userProfile);
        String fileName = "helloworld.txt";
        String contents = "Hello World!";
        InputStream stream = new ByteArrayInputStream(contents.getBytes(StandardCharsets.UTF_8));
        String generatedFileId = UUID.randomUUID().toString();
        when(storageRepo.write(USER_ID.toString(), fileName, stream)).thenReturn(generatedFileId);
        // Test
        service.saveLegsPhoto(fileName, stream);
        UserProfile actual = repo.findOne(USER_ID);
        assertEquals(generatedFileId, actual.getLegsFileId());
    }
    */
}
