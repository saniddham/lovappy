package com.realroofers.lovappy.service.user;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.realroofers.lovappy.service.AbstractTest;
import com.realroofers.lovappy.service.user.dto.GenderAndPrefGenderDto;
import com.realroofers.lovappy.service.user.model.UserPreference;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.repo.UserPreferenceRepo;
import com.realroofers.lovappy.service.user.repo.UserProfileRepo;
import com.realroofers.lovappy.service.user.support.Gender;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public class UserProfileAndPreferenceServiceImplTest extends AbstractTest {
    private static final int USER_ID = 1;
    @Autowired
    private UserProfileAndPreferenceService service;
    @Autowired
    private UserProfileRepo userProfileRepo;
    @Autowired
    private UserPreferenceRepo userPrefRepo;

    @Test
    public void getGenderAndPrefGender() {
        // Prepare data
        Gender userGender = Gender.MALE;
        Gender userPrefGender = Gender.FEMALE;
        UserProfile profile = new UserProfile(USER_ID);
        profile.setGender(userGender);
        userProfileRepo.saveAndFlush(profile);
        UserPreference pref = new UserPreference(USER_ID);
        pref.setGender(userPrefGender);
        userPrefRepo.saveAndFlush(pref);
        // Test
        GenderAndPrefGenderDto expected = new GenderAndPrefGenderDto(userGender, userPrefGender);
        GenderAndPrefGenderDto actual = service.getGenderAndPrefGender(USER_ID);
        assertEquals(expected, actual);
    }

    @Test
    @Ignore
    public void saveOrUpdateGenderAndPrefGenderDto() {
        Gender userGender = Gender.MALE;
        Gender userPrefGender = Gender.FEMALE;
        GenderAndPrefGenderDto dto = new GenderAndPrefGenderDto(userGender, userPrefGender);
        service.saveOrUpdate(USER_ID, dto);
        // Verify
        UserProfile profile = userProfileRepo.findOne(USER_ID);
        UserPreference pref = userPrefRepo.findOne(USER_ID);
        assertEquals(userGender, profile.getGender());
        assertEquals(userPrefGender, pref.getGender());
    }
}
