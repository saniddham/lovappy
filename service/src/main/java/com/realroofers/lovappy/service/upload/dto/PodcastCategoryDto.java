package com.realroofers.lovappy.service.upload.dto;

import com.realroofers.lovappy.service.upload.model.PodcastCategory;
import com.realroofers.lovappy.service.user.dto.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by Manoj
 */


@Data
@ToString
@EqualsAndHashCode
public class PodcastCategoryDto implements Serializable {

    private Integer id;
    private String catName;
    private String description;
    private UserDto createdBy;

    public PodcastCategoryDto() {
    }

    public PodcastCategoryDto(PodcastCategory uploadCategory) {
        if (uploadCategory.getId() != null)
            this.id = uploadCategory.getId();
        this.catName = uploadCategory.getCatName();
        this.description = uploadCategory.getDescription();
        this.createdBy = uploadCategory.getCreatedBy() != null ? new UserDto(uploadCategory.getCreatedBy()) : null;
    }
}
