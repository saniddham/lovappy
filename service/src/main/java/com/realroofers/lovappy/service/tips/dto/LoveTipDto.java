package com.realroofers.lovappy.service.tips.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.realroofers.lovappy.service.tips.model.LoveTip;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 1/1/2018.
 */
@Data
@ToString
@EqualsAndHashCode
public class LoveTipDto implements Serializable {
    private Integer id;

    private Date created;

    private Date updated;

    private String content;
    private String backgroundImage;
    private String textColor;
    private String backgroundColor;
    private Boolean active;

    public LoveTipDto() {
    }
    public LoveTipDto(LoveTip loveTip) {
       this.id = loveTip.getId();
        this.created = loveTip.getCreated();
        this.updated = loveTip.getUpdated();
        this.content = loveTip.getContent();
        this.active = loveTip.getActive();
        this.backgroundImage= loveTip.getBackgroundImage();
        this.textColor = loveTip.getTextColor();
        this.backgroundColor = loveTip.getBackgroundColor();
    }
}
