package com.realroofers.lovappy.service.lovstamps.model;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.lovstamps.support.SampleTypes;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * Created by Eias Altawil on 5/18/17
 */

@Entity
@Table(name="lovdrop_sample")
@Data
@EqualsAndHashCode
public class LovstampSample extends BaseEntity<Integer>{

    @Column(name = "record_file")
    private String recordFile;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile audioFileCloud;

    @ManyToOne
    @JoinColumn(name = "language")
    private Language language;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private SampleTypes type;

    private Integer duration;
}
