package com.realroofers.lovappy.service.user.dto;

import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 8/20/2017.
 */
@EqualsAndHashCode
public class ChangePasswordRequest implements Serializable{
    @NotNull
    @Size(min = 6, message = "Min password length is 6")
    @Pattern(regexp = "^([0-9]+[a-zA-Z]+|[a-zA-Z]+[0-9]+)[0-9a-zA-Z]*$", message = "Password should be mixed of letters and digits")
    private String newPassword;

    private String currentPassword;

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }
}
