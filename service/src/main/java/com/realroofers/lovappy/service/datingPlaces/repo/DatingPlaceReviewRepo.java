package com.realroofers.lovappy.service.datingPlaces.repo;

import com.realroofers.lovappy.service.datingPlaces.model.DatingPlace;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceReview;
import com.realroofers.lovappy.service.datingPlaces.support.PriceRange;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Darrel Rayen on 10/19/17.
 */
@Repository
public interface DatingPlaceReviewRepo extends JpaRepository<DatingPlaceReview, Integer> {

    Page<DatingPlaceReview> findAllByOrderByPlacesReviewIdDesc(Pageable pageable);

    Page<DatingPlaceReview> findAllByPlaceIdAndReviewApprovalStatusAndReviewerTypeOrderByReviewDateDesc(Pageable pageable, DatingPlace datingPlace, ApprovalStatus approvalStatus, String reviewerType);

    @Query("select dr from DatingPlaceReview dr where dr.placeId.datingPlacesId=:placeId and dr.reviewApprovalStatus=:approvalStatus and (dr.reviewerType='user' or dr.reviewerType='google') order by dr.reviewDate DESC")
    Page<DatingPlaceReview> findAllByPlaceIdAndReviewApprovalStatusAndReviewerType(Pageable pageable, @Param("placeId") Integer datingPlace, @Param("approvalStatus") ApprovalStatus approvalStatus);

    @Query("select dr from DatingPlaceReview dr where dr.placeId.datingPlacesId=:placeId and dr.reviewApprovalStatus=:approvalStatus and (dr.reviewerType='user' or dr.reviewerType='google') order by dr.reviewDate DESC")
    List<DatingPlaceReview> findAllByPlaceIdAndReviewApprovalStatusAndReviewerTypeList(@Param("placeId") Integer placeId, @Param("approvalStatus") ApprovalStatus approvalStatus);

    Page<DatingPlaceReview> findAllByReviewApprovalStatusOrderByPlacesReviewIdDesc(Pageable pageable, ApprovalStatus approvalStatus);

    @Query("select dr.securityRating from DatingPlaceReview dr WHERE dr.placeId= :placeId and dr.reviewApprovalStatus='Approved' and dr.reviewerType='user'")
    List<Double> findAllSecurityRatingByPlaceId(@Param("placeId") DatingPlace datingPlace);

    @Query("select dr.parkingRating from DatingPlaceReview dr WHERE dr.placeId= :placeId and dr.reviewApprovalStatus='Approved' and dr.reviewerType='user'")
    List<Double> findAllParkingRatingByPlaceId(@Param("placeId") DatingPlace datingPlace);

    @Query("select dr.parkingAccessRating from DatingPlaceReview dr WHERE dr.placeId= :placeId and dr.reviewApprovalStatus='Approved' and dr.reviewerType='user'")
    List<Double> findAllParkingAccessRatingByPlaceId(@Param("placeId") DatingPlace datingPlace);

    @Query("select dr.averageRating from DatingPlaceReview dr WHERE dr.placeId= :placeId and dr.reviewApprovalStatus='Approved' and dr.reviewerType='user'")
    List<Double> findAllAverageRatingByPlaceId(@Param("placeId") DatingPlace datingPlace);

    @Query("select dr.placeId from DatingPlaceReview dr where dr.placesReviewId=:reviewId")
    DatingPlace findDatingPlaceByDatingPlacesReviewId(@Param("reviewId") Integer reviewId);

    @Query("select COUNT(dr) from DatingPlaceReview dr where dr.placeId=:place and dr.reviewApprovalStatus='Approved'")
    Integer getNumberofReviewsByPlaceId(@Param("place") DatingPlace datingPlace);

    @Query("select COUNT(dr) from DatingPlaceReview dr where dr.placeId=:place and  dr.suitableGender=:gender")
    Integer getGenderCountByPlaceId(@Param("place") DatingPlace datingPlace, @Param("gender") Gender gender);

    @Query("select avg(dr.securityRating) from DatingPlaceReview dr WHERE dr.placeId= :placeId ")
    Double findAllAverageSecurityRatingByPlaceId(@Param("placeId") DatingPlace datingPlace);

    @Query("select avg(dr.parkingRating) from DatingPlaceReview dr WHERE dr.placeId= :placeId ")
    Double findAllAverageParkingRatingByPlaceId(@Param("placeId") DatingPlace datingPlace);

    @Query("select avg(dr.parkingAccessRating) from DatingPlaceReview dr WHERE dr.placeId= :placeId ")
    Double findAllAverageTransportRatingByPlaceId(@Param("placeId") DatingPlace datingPlace);

    @Query("select avg(dr.overallRating) from DatingPlaceReview dr WHERE dr.placeId= :placeId ")
    Double findAverageOverAllRatingByPlaceId(@Param("placeId") DatingPlace datingPlace);

    @Query("select avg(dr.averageRating) from DatingPlaceReview dr WHERE dr.placeId= :placeId ")
    Double findAverageRatingByPlaceId(@Param("placeId") DatingPlace datingPlace);

    DatingPlaceReview findFirst1ByReviewerEmail(String email);

    List<DatingPlaceReview> findAllByGooglePlaceIdAndReviewApprovalStatusOrderByReviewDateDesc(String googlePlaceId, ApprovalStatus approvalStatus);

    @Query("select avg(dr.overallRating) from DatingPlaceReview dr WHERE dr.placeId= :placeId and dr.reviewApprovalStatus='Approved'")
    Double findOverAllRatingByPlaceId(@Param("placeId") DatingPlace datingPlace);

    @Query("select avg(dr.overallRating) from DatingPlaceReview dr WHERE dr.googlePlaceId= :placeId and dr.reviewApprovalStatus='Approved' and dr.reviewerType='user'")
    Double findAverageOverAllRatingByGooglePlaceId(@Param("placeId") String googlePlaceId);

    List<DatingPlaceReview> findAllByGooglePlaceIdAndReviewApprovalStatusAndReviewerType(String googlePlaceId, ApprovalStatus approvalStatus, String reviewerType);

    @Query("select dr.suitableAgeRange,COUNT(dr) AS magnitude  from DatingPlaceReview dr WHERE dr.googlePlaceId= :placeId and dr.reviewApprovalStatus='Approved' and dr.reviewerType='user' group by dr.suitableAgeRange order by magnitude DESC")
    Page<Object[]> findMaxAgeRange(@Param("placeId") String googlePlaceId, Pageable pageable);

    @Query("select dr.suitablePriceRange,COUNT(dr) AS magnitude from DatingPlaceReview dr WHERE dr.googlePlaceId= :placeId and dr.reviewApprovalStatus='Approved' and dr.reviewerType='user' group by dr.suitablePriceRange order by magnitude DESC")
    Page<Object[]> findMaxPriceRange(@Param("placeId") String googlePlaceId, Pageable pageable);

    List<DatingPlaceReview> findAllByIsFeatured(Boolean featured);

    @Query("select dr from DatingPlaceReview dr where dr.overallRating is null and dr.reviewerType = 'user'")
    List<DatingPlaceReview> findAllByOverallRatingIsNull();

    List<DatingPlaceReview> findAllByPlaceNameIsNull();

    List<DatingPlaceReview> findAllByReviewApprovalStatusIsNullAndReviewerTypeEquals(String reviewerType);

}
