package com.realroofers.lovappy.service.user;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public interface PasswordResetRequestService {
    int generateAndSaveRequest(Integer userId);
}
