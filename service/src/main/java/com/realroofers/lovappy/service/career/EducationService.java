package com.realroofers.lovappy.service.career;

import com.realroofers.lovappy.service.career.dto.CareerUserDto;
import com.realroofers.lovappy.service.career.dto.VacancyDto;
import com.realroofers.lovappy.service.career.model.Education;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

public interface EducationService {
    String addEducations(Set<Education> educations);

}
