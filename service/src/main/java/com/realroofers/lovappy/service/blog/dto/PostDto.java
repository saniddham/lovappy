package com.realroofers.lovappy.service.blog.dto;

import com.realroofers.lovappy.service.blog.model.BlogTypes;
import com.realroofers.lovappy.service.blog.model.Post;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.user.dto.AuthorDto;
import lombok.EqualsAndHashCode;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.hibernate.validator.constraints.NotEmpty;
import org.jsoup.Jsoup;
import org.springframework.util.StringUtils;
import org.xml.sax.ContentHandler;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author mwiyono
 */
@EqualsAndHashCode
public class PostDto implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -6944883065009428558L;

    private AuthorDto author;

    private Integer ID;

    private Integer credit;

    private Date createdOn;

    private Boolean approved;

    @NotNull
    @NotEmpty(message = "Please filled up the title")
    private String title;

    //    @NotNull
//    @NotEmpty(message = "Please filled up the content")


    private String videoLink;

    private String keywords;

    private String description;

    private String seoTitle;

    @NotNull
    private Integer categoryId;

    private String imageAlt;

    private String state;

    private Integer viewCount;

    private CloudStorageFileDto image;
    private CloudStorageFileDto originalImage;
    private String shortTitle;
    private String mediumTitle;
    private CloudStorageFileDto authorProfilePhoto;
    private String authorName;
    private String authorAddress;
    private String zipCode;
    private Boolean authorAnonymous;
    private transient boolean allowEdit;

    private String categoryName;

    private boolean writtenByAdmin;

    @Transient
    private boolean hideAuthor;

    private Boolean featured;

    private String htmlContent;

    private Boolean isGallery;

    private Boolean draft;

    private String slugUrl;

    private String keywordsPhrases;

    private String content;
    public PostDto(Post post) {
        super();
        if (post.getPostAuthor() != null) {
            this.author = new AuthorDto(post.getPostAuthor());
            this.authorAddress = author.getAddress();
            if (!StringUtils.isEmpty(author.getFirstName())) {
                this.authorName = author.getFirstName();
                if (!StringUtils.isEmpty(author.getLastName())) {
                    this.authorName += " " + author.getLastName();
                }
            } else if (!StringUtils.isEmpty(author.getLastName())) {
                this.authorName = author.getLastName();
            }
            this.zipCode = author.getZipCode();
            this.authorProfilePhoto = author.getImageProfile();

        }
        ID = post.getId();
        this.credit = post.getCredit();
        this.createdOn = post.getPostDate();
        this.title = post.getPostTitle();
        this.content = post.getPostContent();
        this.videoLink = post.getVideoLink();
        this.keywords = post.getKeywords();
        this.description = post.getDescription();
        this.seoTitle = post.getSeoTitle();
        if (post.getCategory() != null) {
            this.categoryId = post.getCategory().getId();
            this.categoryName = post.getCategory().getName();
        }
        this.imageAlt = post.getImageAlt();
        if (post.getState() != null) {
            this.state = post.getState().name();
        }
        this.viewCount = post.getViewCount();
        this.image = new CloudStorageFileDto(post.getImage());

        this.originalImage = post.getOriginalImage() != null ? new CloudStorageFileDto(post.getOriginalImage()) : new CloudStorageFileDto(post.getImage());

        this.allowEdit = post.isAllowEdit();
        this.hideAuthor = post.isHideAuthor();

        if (title.length() > 11) {
            this.shortTitle = title.substring(0, 11) + "...";
        } else {
            this.shortTitle = title;
        }

        if (title.length() > 35) {
            this.mediumTitle = title.substring(0, 35) + "...";
        } else {
            this.mediumTitle = title;
        }

        this.authorAnonymous = post.getAuthorAnonymous() == null ? false : post.getAuthorAnonymous();
        this.writtenByAdmin = post.getWrittenByAdmin() == null ? false : post.getWrittenByAdmin();
        this.featured = post.getFeatured();

        this.isGallery = BlogTypes.GALLERY.equals(post.getType());
        this.htmlContent = StringUtils.isEmpty(post.getHtmlContent()) ? post.getPostContent() : post.getHtmlContent();

        this.draft = post.getDraft() == null ? false : post.getDraft();
        this.approved = post.getApproved() == null ? false : post.getApproved();
        this.slugUrl = post.getSlugUrl();
        this.keywordsPhrases= post.getKeywordsPhrases();

    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public Boolean getDraft() {
        return draft;
    }

    public void setDraft(Boolean draft) {
        this.draft = draft;
    }

    public Boolean getGallery() {
        return isGallery;
    }

    public void setGallery(Boolean gallery) {
        isGallery = gallery;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    public PostDto() {
        super();
        this.authorAnonymous = false;
        this.image = new CloudStorageFileDto();
    }

    public CloudStorageFileDto getImage() {
        return image;
    }

    public void setImage(CloudStorageFileDto image) {
        this.image = image;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public boolean isHideAuthor() {
        return hideAuthor;
    }

    public void setHideAuthor(boolean hideAuthor) {
        this.hideAuthor = hideAuthor;
    }

    public void setImageAlt(String imageAlt) {
        this.imageAlt = imageAlt;
    }


    public Integer getID() {
        return ID;
    }

    public void setID(Integer iD) {
        ID = iD;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date date) {
        this.createdOn = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPostContent() {
        return content;
    }

    public void setPostContent(String posContent) {
        this.content = posContent;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSeoTitle() {
        return seoTitle;
    }

    public void setSeoTitle(String seo_title) {
        this.seoTitle = seo_title;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public AuthorDto getAuthor() {
        return author;
    }


    @Transient
    public String getPostAuthor() {
        if (authorAnonymous != null && authorAnonymous)
            return "Anonymous";
        if (author != null && author.getEmail() != null) {
            return author.getEmail();
        } else if (author == null) {
            return "Anonymous";
        }
        return "";
    }

    public String getAuthorFullname() {
        if (authorAnonymous != null && authorAnonymous)
            return "Anonymous";
        if (author != null && !StringUtils.isEmpty(author.getFirstName())) {
            return author.getFirstName() + " " + (!StringUtils.isEmpty(author.getLastName()) ? author.getLastName() : "");
        } else {
            return getPostAuthor();
        }
    }

    public void setAuthor(AuthorDto post_author) {
        this.author = post_author;
        this.authorAddress = author.getAddress();
        this.zipCode = author.getZipCode();
        if (!StringUtils.isEmpty(author.getFirstName()) && !StringUtils.isEmpty(author.getLastName()))
            this.setAuthorName(author.getFirstName() + " " + author.getLastName());
        this.authorProfilePhoto = author.getImageProfile();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isAllowEdit() {
        return allowEdit;
    }

    public void setAllowEdit(boolean allowEdit) {
        this.allowEdit = allowEdit;
    }


    public Integer getCredit() {
        return credit;
    }

    public String getMeduimContent() {
        if (content != null && content.length() > 450) {
            String shortDecs = content.substring(0, 450);
            return shortDecs + " ...";
        }
        return content;
    }

    public String getShortContent() {
        if (content != null) {

            try {


                String plainText = Jsoup.parse(content).text();
                if (plainText.length() > 130) {
                        String shortDecs = plainText.substring(0, 130);
                        return shortDecs + " ...";
                    }

            }
            catch (Exception e) {
            }

        }
        return Jsoup.parse(content).text();
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public String getImageAlt() {
        return imageAlt;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public CloudStorageFileDto getAuthorProfilePhoto() {
        return authorProfilePhoto;
    }

    public void setAuthorProfilePhoto(CloudStorageFileDto authorProfilePhoto) {
        this.authorProfilePhoto = authorProfilePhoto;
    }

    public String getAuthorProfileImage() {
        if (getAuthorProfilePhoto() != null && !StringUtils.isEmpty(getAuthorProfilePhoto().getUrl())) {
            return getAuthorProfilePhoto().getUrl();
        } else {
            return "/images/blog/default_author.PNG";

        }

    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorAddress() {
        return authorAddress;
    }

    public void setAuthorAddress(String authorAddress) {
        this.authorAddress = authorAddress;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getPostOnDate() {
        SimpleDateFormat formatPostOnDate = new SimpleDateFormat("MM/dd/yyyy");
        if (createdOn == null)
            return "";
        String formatted = formatPostOnDate.format(getCreatedOn());
        return formatted;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Boolean getAuthorAnonymous() {
        return authorAnonymous;
    }

    public void setAuthorAnonymous(Boolean authorAnonymous) {
        this.authorAnonymous = authorAnonymous;
    }

    public String getMediumTitle() {
        return mediumTitle;
    }

    public void setMediumTitle(String mediumTitle) {
        this.mediumTitle = mediumTitle;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public boolean isWrittenByAdmin() {
        return writtenByAdmin;
    }

    public void setWrittenByAdmin(boolean writtenByAdmin) {
        this.writtenByAdmin = writtenByAdmin;
    }


    public String getImageUrl() {
        return StringUtils.isEmpty(this.image.getUrl()) ? "/images/blog/blogimg.jpg" : this.image.getUrl();
    }

    public String getOriginalImageUrl() {
        CloudStorageFileDto originalImage = this.getOriginalImage();
        if (originalImage == null || originalImage.getId() == null) {
            originalImage = this.image;
        }
        return StringUtils.isEmpty(originalImage.getUrl()) ? "/images/blog/blogimg.jpg" : originalImage.getUrl();
    }

    public Boolean getFeatured() {
        return featured;
    }

    public void setFeatured(Boolean featured) {
        this.featured = featured;
    }

    public String getSlugUrl() {
        return slugUrl;
    }

    public void setSlugUrl(String slugUrl) {
        this.slugUrl = slugUrl;
    }

    public String getKeywordsPhrases() {
        return keywordsPhrases;
    }

    public void setKeywordsPhrases(String keywordsPhrases) {
        this.keywordsPhrases = keywordsPhrases;
    }

    public CloudStorageFileDto getOriginalImage() {
        return originalImage;
    }

    public void setOriginalImage(CloudStorageFileDto originalImage) {
        this.originalImage = originalImage;
    }


    public String getContentUrl() {
        return "/blog/posts/"+slugUrl+"/content";
    }

}
