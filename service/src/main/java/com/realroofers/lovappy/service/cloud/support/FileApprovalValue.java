package com.realroofers.lovappy.service.cloud.support;
/***
 * @author Japheth Odonya
 * */
public enum FileApprovalValue {
    APPROVE("1"), REJECT("0");
    String code;

    FileApprovalValue(String code) {
        this.code = code;
    }

}
