package com.realroofers.lovappy.service.blog.model;

import lombok.EqualsAndHashCode;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity(name = "relaycomment")
@Table(name = "relaycomment")
@EqualsAndHashCode
public class RelayComment {
	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Integer ID;
	
	@ManyToOne
	@JoinColumn(name = "comment_id")
	private Comment comment;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date date;
	
	@Column(name = "content")
	private String content;

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
