package com.realroofers.lovappy.service.questions.repo;

import com.realroofers.lovappy.service.questions.model.BuyResponse;
import com.realroofers.lovappy.service.questions.model.BuyerId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Darrel Rayen on 11/4/18.
 */
public interface BuyerResponseRepo extends JpaRepository<BuyResponse, BuyerId> {

    @Query("select br from BuyResponse br where br.user.userId = :userId")
    List<BuyResponse> findAllByUserId(@Param("userId") Integer userId);
}
