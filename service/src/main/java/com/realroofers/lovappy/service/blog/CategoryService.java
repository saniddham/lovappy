package com.realroofers.lovappy.service.blog;

import java.util.List;

import com.realroofers.lovappy.service.blog.model.CategoryPostCount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.realroofers.lovappy.service.blog.dto.CategoryDto;
import com.realroofers.lovappy.service.blog.model.Category;

public interface CategoryService {
	
	void save(Category category);

	List<Category> getAllCategories();

	Page<CategoryDto> getAllCategories(int page, int limit);

	void update(Category category);

	void delete(Category category);

	Category get(Integer id);
	
	CategoryDto getOne(Integer id);

	int deactivate(Integer categoryId);

	int activate(Integer categoryId);

	List<CategoryDto> findActive();

	Page<CategoryDto> findAll(Pageable pageable);

	Page<CategoryDto> find(String name, Pageable pageable);

	List<CategoryDto> findCategory(String name);

	List<CategoryDto> findAll();

	void delete(Integer id);

	void addImage(Integer id, Long fileId);
	void deleteImage(Integer id, Long fileId);
	Page<CategoryPostCount> getCategoryPostsCount(int page, int limit);
}