package com.realroofers.lovappy.service.music;

import com.realroofers.lovappy.service.music.dto.MusicResponseDto;
import com.realroofers.lovappy.service.music.model.MusicExchange;
import com.realroofers.lovappy.service.music.model.MusicResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MusicExchangeResponseService {

    MusicResponse sendResponse(MusicResponse musicResponse);

    MusicResponse update(MusicResponse musicResponse);

    Page<MusicResponseDto> getResponseForLoginUser(Integer currentUser, Pageable pageable);

    MusicResponse getResponseForExchange(MusicExchange musicExchange);

    MusicResponse findById(Integer musicExchange);

    List<MusicResponseDto> getInboxResponsesForLoginUser(Integer userId);

    MusicResponseDto getOneInboxResponsesForLoginUser(Integer userId);
}
