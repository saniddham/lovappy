package com.realroofers.lovappy.service.system.repo;

import com.realroofers.lovappy.service.system.model.Language;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

/**
 * Created by Eias Altawil on 6/3/2017
 */
public interface LanguageRepo extends CrudRepository<Language, Integer>, QueryDslPredicateExecutor<Language> {
    List<Language> findAllByOrderByNameAsc();
    List<Language> findByEnabledOrderByNameAsc(Boolean enabled);
    List<Language> findByIdIn(List<Integer> ids);
    List<Language> findByAbbreviationIn(Set<String> codes);
    Language findByNameAndAbbreviation(String name, String abbreviation);
    Language findByAbbreviation(String abbreviation);
}
