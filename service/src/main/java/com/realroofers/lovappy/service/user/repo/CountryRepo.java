package com.realroofers.lovappy.service.user.repo;

import com.realroofers.lovappy.service.user.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Manoj on 11/02/2018.
 */
public interface CountryRepo extends JpaRepository<Country, Integer> {

    Country findByCode(String code);

    List<Country> findAllByActiveIsTrueOrderByName();
}
