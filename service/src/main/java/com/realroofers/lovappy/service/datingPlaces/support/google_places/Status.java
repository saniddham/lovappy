package com.realroofers.lovappy.service.datingPlaces.support.google_places;

/**
 * Represents the current status of the place in time.
 */
public enum Status {
    OPENED("Open Now"),
    CLOSED("Close Now"),
    NONE("");

    String text;

    Status(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
