package com.realroofers.lovappy.service.datingPlaces.dto;

import com.realroofers.lovappy.service.datingPlaces.model.DatingPlace;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceDeals;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Darrel Rayen on 1/22/18.
 */
@EqualsAndHashCode
public class DatingPlaceDealsDto implements Serializable {

    private Integer placeDealId;
    private Integer placeId;
    private Integer dealId;
    private Double fullAmount;
    private Double discountValue;
    private String dealDescription;
    private Date startDate;
    private Date expiryDate;
    private ApprovalStatus approvalStatus;
    private Double dealPrice;
    private Boolean isActive;
    private DatingPlace datingPlace;
    private Boolean approved;

    public DatingPlaceDealsDto() {
    }

    public DatingPlaceDealsDto(DatingPlaceDeals placeDeals) {
        this.placeDealId = placeDeals.getPlaceDealId();
        this.placeId = placeDeals.getDatingPlace().getDatingPlacesId();
        this.dealId = placeDeals.getDeals().getDealId();
        this.fullAmount = placeDeals.getFullAmount();
        this.discountValue = placeDeals.getDiscountValue();
        this.dealDescription = placeDeals.getDealDescription();
        this.startDate = placeDeals.getStartDate();
        this.expiryDate = placeDeals.getExpiryDate();
        this.approvalStatus = placeDeals.getApprovalStatus();
        this.dealPrice = placeDeals.getDealPrice();
        this.isActive = placeDeals.getActive() == null ? false : placeDeals.getActive();
        this.datingPlace = placeDeals.getDatingPlace();
        this.approved = placeDeals.getApproved() == null ? false : placeDeals.getApproved();
    }

    public Integer getPlaceDealId() {
        return placeDealId;
    }

    public void setPlaceDealId(Integer placeDealId) {
        this.placeDealId = placeDealId;
    }

    public Integer getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Integer placeId) {
        this.placeId = placeId;
    }

    public Integer getDealId() {
        return dealId;
    }

    public void setDealId(Integer dealId) {
        this.dealId = dealId;
    }

    public Double getFullAmount() {
        return fullAmount;
    }

    public void setFullAmount(Double fullAmount) {
        this.fullAmount = fullAmount;
    }

    public Double getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(Double discountValue) {
        this.discountValue = discountValue;
    }

    public String getDealDescription() {
        return dealDescription;
    }

    public void setDealDescription(String dealDescription) {
        this.dealDescription = dealDescription;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public ApprovalStatus getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(ApprovalStatus approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Double getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(Double dealPrice) {
        this.dealPrice = dealPrice;
    }

    public DatingPlace getDatingPlace() {
        return datingPlace;
    }

    public void setDatingPlace(DatingPlace datingPlace) {
        this.datingPlace = datingPlace;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }
}
