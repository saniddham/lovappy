package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.user.model.UserFlagPK;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.util.StringUtils;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 8/2/2017.
 */
@EqualsAndHashCode
public class UserFlagDto implements Serializable {
    private Integer flagTo;

    private Integer flagBy;

    private String flagByEmail;

    private String flagToEmail;

    private boolean offensive;

    private boolean discriminatory;

    private boolean uncivil;

    private boolean harrassment;

    private boolean notRelevant;

    private String others;

    private String formInput;

    private String redirectUrl;

    private Date submitedOn;

    public UserFlagDto() {
    }

    public UserFlagDto(Integer flagTo, Integer flagBy, Boolean offensive, Boolean discriminatory, Boolean uncivil, Boolean harrassment, Boolean notRelevant, String others, Date submitedOn) {
        this.flagTo = flagTo;
        this.flagBy = flagBy;
        this.offensive = offensive;
        this.discriminatory = discriminatory;
        this.uncivil = uncivil;
        this.harrassment = harrassment;
        this.notRelevant = notRelevant;
        this.others = others;
        this.submitedOn = submitedOn;
    }
    public UserFlagDto(Integer flagTo, String flagToEmail, String flagByEmail, Boolean offensive, Boolean discriminatory, Boolean uncivil, Boolean harrassment, Boolean notRelevant, String others, Date submitedOn) {
        this.flagTo = flagTo;
        this.flagByEmail = flagByEmail;
        this.offensive = offensive;
        this.discriminatory = discriminatory;
        this.uncivil = uncivil;
        this.harrassment = harrassment;
        this.notRelevant = notRelevant;
        this.others = others;
        this.submitedOn = submitedOn;
        this.flagToEmail = flagToEmail;
    }
    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public Integer getFlagTo() {
        return flagTo;
    }

    public void setFlagTo(Integer flagTo) {
        this.flagTo = flagTo;
    }

    public Integer getFlagBy() {
        return flagBy;
    }

    public void setFlagBy(Integer flagBy) {
        this.flagBy = flagBy;
    }

    public Boolean getOffensive() {
        return offensive;
    }

    public void setOffensive(Boolean offensive) {
        this.offensive = offensive;
    }

    public Boolean getDiscriminatory() {
        return discriminatory;
    }

    public void setDiscriminatory(Boolean discriminatory) {
        this.discriminatory = discriminatory;
    }

    public Boolean getUncivil() {
        return uncivil;
    }

    public void setUncivil(Boolean uncivil) {
        this.uncivil = uncivil;
    }

    public Boolean getHarrassment() {
        return harrassment;
    }

    public void setHarrassment(Boolean harrassment) {
        this.harrassment = harrassment;
    }

    public Boolean getNotRelevant() {
        return notRelevant;
    }

    public void setNotRelevant(Boolean notRelevant) {
        this.notRelevant = notRelevant;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public String getFormInput() {
        return formInput;
    }

    public void setFormInput(String formInput) {
        this.formInput = formInput;
    }

    public Date getSubmitedOn() {
        return submitedOn;
    }

    public void setSubmitedOn(Date submitedOn) {
        this.submitedOn = submitedOn;
    }

    public String getFlagToEmail() {
        return flagToEmail;
    }

    public void setFlagToEmail(String flagToEmail) {
        this.flagToEmail = flagToEmail;
    }

    public String getDetails(){
        String complaints = "";
        if(harrassment) {
            complaints = "harrassment";
        }else if(offensive) {
            complaints = "offensive";
        }else if(discriminatory) {
            complaints = "discriminatory";
        }else if(uncivil) {
            complaints = "uncivil";
        }else if(notRelevant) {
            complaints = "notRelevant";
        }
        if(!StringUtils.isEmpty(others)) {
            complaints = others;
        }

        return "Complaints beacause of : " + complaints;
    }
    public String getFlagByEmail() {
        return flagByEmail;
    }

    public void setFlagByEmail(String flagByEmail) {
        this.flagByEmail = flagByEmail;
    }


}
