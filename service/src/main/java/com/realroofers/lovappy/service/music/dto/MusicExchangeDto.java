package com.realroofers.lovappy.service.music.dto;

import com.realroofers.lovappy.service.music.model.MusicExchange;
import com.realroofers.lovappy.service.user.dto.UserDto;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;


@EqualsAndHashCode
public class MusicExchangeDto implements Serializable {

    private Integer id;
    private MusicDto music;
    private UserDto fromUser;
    private UserDto toUser;
    private Date exchangeDate;
    private String revceivedLyrics;
    private Boolean userResponded;
    private int senderMobileDownloads;
    private int senderWebDownloads;
    private int receiverMobileDownloads;
    private int receiverWebDownloads;
    private Boolean seen;

    public MusicExchangeDto() {
    }

    public MusicExchangeDto(MusicExchange musicExchange) {
        this.id = musicExchange.getId();
        this.music = new MusicDto(musicExchange.getMusic());
        this.fromUser = new UserDto(musicExchange.getFromUser());
        this.toUser = new UserDto(musicExchange.getToUser());
        this.exchangeDate = musicExchange.getExchangeDate();
        this.revceivedLyrics = musicExchange.getLyrics();
        this.seen = musicExchange.getSeen();
        this.userResponded = musicExchange.getUserResponded();
        this.senderMobileDownloads = musicExchange.getSenderMobileDownloads();
        this.senderWebDownloads = musicExchange.getSenderWebDownloads();
        this.receiverMobileDownloads = musicExchange.getReceiverMobileDownloads();
        this.receiverWebDownloads = musicExchange.getReceiverWebDownloads();
    }

    public String getRevceivedLyrics() {
        return revceivedLyrics;
    }

    public void setRevceivedLyrics(String revceivedLyrics) {
        this.revceivedLyrics = revceivedLyrics;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MusicDto getMusic() {
        return music;
    }

    public void setMusic(MusicDto music) {
        this.music = music;
    }

    public UserDto getFromUser() {
        return fromUser;
    }

    public void setFromUser(UserDto fromUser) {
        this.fromUser = fromUser;
    }

    public UserDto getToUser() {
        return toUser;
    }

    public void setToUser(UserDto toUser) {
        this.toUser = toUser;
    }

    public Date getExchangeDate() {
        return exchangeDate;
    }

    public void setExchangeDate(Date exchangeDate) {
        this.exchangeDate = exchangeDate;
    }

    public String getUserToDetails() {
        return getToUser().getUserProfile().getAge() + " / " + getToUser().getUserProfile().getAddress().getCity() + " / " + getToUser().getUserProfile().getLanguage();
    }

    public String getUserFromDetails() {
        return getFromUser().getUserProfile().getAge() + " / " + getFromUser().getUserProfile().getAddress().getCity() + " / " + getFromUser().getUserProfile().getLanguage();
    }

    public Boolean getUserResponded() {
        return userResponded;
    }

    public void setUserResponded(Boolean userResponded) {
        this.userResponded = userResponded;
    }

    public Boolean getSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }

    public int getSenderMobileDownloads() {
        return senderMobileDownloads;
    }

    public void setSenderMobileDownloads(int senderMobileDownloads) {
        this.senderMobileDownloads = senderMobileDownloads;
    }

    public int getSenderWebDownloads() {
        return senderWebDownloads;
    }

    public void setSenderWebDownloads(int senderWebDownloads) {
        this.senderWebDownloads = senderWebDownloads;
    }

    public int getReceiverMobileDownloads() {
        return receiverMobileDownloads;
    }

    public void setReceiverMobileDownloads(int receiverMobileDownloads) {
        this.receiverMobileDownloads = receiverMobileDownloads;
    }

    public int getReceiverWebDownloads() {
        return receiverWebDownloads;
    }

    public void setReceiverWebDownloads(int receiverWebDownloads) {
        this.receiverWebDownloads = receiverWebDownloads;
    }
}
