package com.realroofers.lovappy.service.news;

import com.realroofers.lovappy.service.news.dto.NewsDto;
import com.realroofers.lovappy.service.news.dto.NewsStoryAdminDto;
import com.realroofers.lovappy.service.news.dto.NewsStoryDto;
import com.realroofers.lovappy.service.news.model.NewsStory;

import java.io.IOException;
import java.util.List;


public interface NewsStoryService {
    NewsStory saveNewsStory(NewsStoryDto newsStoryDto) throws IOException;

    NewsStory getNewsStoryById(Long Id);

    NewsStory getNewsStoryByTitle(String title);

    NewsStory saveNewsStory(NewsStory newsStory);

    List<NewsDto> getAllNewsStories();

    Long getLastId();

    List<NewsStory> getNewsStoriesByTitleAndContain(String search);

    List<NewsStoryAdminDto> getAllNewsStoryAdminDto();

    List<NewsStoryAdminDto> getAllNewsStoryAdminDtoByTitleAndContain(String search);

    NewsStoryAdminDto getNewsStoryAdminDtoById(Long id);

    void newsApproval(Long id, boolean status);

    List<NewsDto> getApprovedNewsStories();

    List<NewsDto> getLatesNewsStories();

    List<NewsDto> getNewsByCategoryId(Long catId);
}
