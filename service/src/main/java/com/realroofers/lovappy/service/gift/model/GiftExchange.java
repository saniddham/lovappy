package com.realroofers.lovappy.service.gift.model;

import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.vendor.model.VendorLocation;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Eias Altawil on 5/13/17
 */

@Entity
@Data
@Table(name = "gift_exchange")
@EqualsAndHashCode
public class GiftExchange {

    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "gift")
    private Product gift;

    @ManyToOne
    @JoinColumn(name = "from_user")
    private User fromUser;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "to_user")
    private User toUser;

    @Column(name = "sold_price")
    private Double soldPrice;

    @Column(name = "commission_percentage")
    private Double commissionPercentage;

    private Double commission;

    // This is the actual amount received to seller after removing commission
    private Double earned;

    private Date sentAt;

    @Column(name = "payment_method")
    private String paymentMethod;

    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "redeemed", columnDefinition = "boolean default false", nullable = false)
    private Boolean redeemed = false;

    @Column(name = "redeem_on")
    private Date redeemOn;

    @Column(name = "redeem_code")
    private String redeemCode;

    @ManyToOne
    @JoinColumn(name = "redeem_at")
    private VendorLocation redeemAt;

    private String transactionID;

    @Transient
    private String refNumber;

    public String getRefNumber() {
        String format = "%1$08d";
        String result = String.format(format, this.getId());
        refNumber = "LOV" + result;
        return refNumber;
    }
}
