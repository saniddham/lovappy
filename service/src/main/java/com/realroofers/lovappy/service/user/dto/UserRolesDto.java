package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.model.UserRoles;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 5/12/2018.
 */
@Data
@EqualsAndHashCode
public class UserRolesDto implements Serializable {
    private Integer userId;
    private String email;
    private RoleDto role;
    private Date requestDate;
    private ApprovalStatus approvalStatus;
    private Long numOfLogins;
    private Date lastLogin;
    private Double latitude;
    private Double longitude;
    private Boolean allowAccess;
    public UserRolesDto(UserRoles userRoles) {
        User user = userRoles.getId().getUser();
        this.userId = user.getUserId();
        this.email = user.getEmail();
        this.role = new RoleDto(userRoles.getId().getRole());
        this.requestDate = userRoles.getCreated();
        this.approvalStatus = userRoles.getApprovalStatus();
        this.numOfLogins = user.getLoginsCount();
        this.lastLogin = user.getLastLogin();
        if(user.getUserProfile() != null && user.getUserProfile().getAddress() != null) {
            this.latitude = user.getUserProfile().getAddress().getLatitude();
            this.longitude = user.getUserProfile().getAddress().getLongitude();
        }
        this.allowAccess = ApprovalStatus.APPROVED == approvalStatus;
    }
}
