package com.realroofers.lovappy.service.ads.dto;

import com.realroofers.lovappy.service.ads.model.AdBackgroundColor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
public class AdBackgroundColorDto {

    private Integer id;
    private String color;

    public AdBackgroundColorDto(AdBackgroundColor backgroundColor) {
        this.id = backgroundColor.getId();
        this.color = backgroundColor.getColor();
    }
}
