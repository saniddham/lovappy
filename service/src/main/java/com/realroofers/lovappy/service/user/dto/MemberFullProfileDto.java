package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.couples.dto.CouplesProfileDto;
import com.realroofers.lovappy.service.lovstamps.dto.LovastampDetailsDto;
import com.realroofers.lovappy.service.lovstamps.model.Lovstamp;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.model.UserPreference;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.ProfilePictureAccess;
import com.realroofers.lovappy.service.user.support.SubscriptionType;
import com.realroofers.lovappy.service.user.support.ZodiacSigns;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;


/**
 * @author mwiyono
 */
@EqualsAndHashCode
@Data
@ToString
public class MemberFullProfileDto {

	private Integer userId;

	private String email;

	private boolean verifiedEmail;

	private Gender gender;

	private Date birthDate;

	private Integer heightFt;

	private Integer heightIn;
	private Boolean paused;
	private Boolean banned;
	private Boolean activated;
	private List<RoleDto> roles;
	private CloudStorageFileDto profilePhotoCloudFile;
	private CloudStorageFileDto handsCloudFile;
	private CloudStorageFileDto feetCloudFile;
	private CloudStorageFileDto legsCloudFile;

	private Boolean handsFileSkipped;

	private Boolean feetFileSkipped;

	private Boolean legsFileSkipped;

	private UserPersonalityDto personalities;

	private UserLifeStylesDto lifestyles;

	private UserStatusesDto statuses;

	private SubscriptionType subscriptionType;

	private Boolean iliked=false;

	private Boolean matches=false;
	private Boolean blocked=false;

    private ZodiacSigns zodiac;

    private AddressDto address;

	private CouplesProfileDto coupleProfile;
    private UserPreferenceDto preferences;
    private Set<LanguageDto> speakingLanguages;
	private LovastampDetailsDto voice;
    private ProfilePictureAccess profilePictureAccess;
    private Integer transactions;

    private Map<String, Object> additionalInfo = new HashMap<>();
	public MemberFullProfileDto() {

	}
public MemberFullProfileDto(User user, Lovstamp voice) {
	this(user);
	this.voice = new LovastampDetailsDto(voice);
}

	public MemberFullProfileDto(User user) {
		this.userId = user.getUserId();
		this.email = user.getEmail();
		this.verifiedEmail = user.getEmailVerified() == null ? false : user.getEmailVerified();
		UserProfile userProfile = user.getUserProfile();
		this.gender = userProfile.getGender();
		this.birthDate = userProfile.getBirthDate();
		this.heightFt = userProfile.getHeightFt();
		this.heightIn = userProfile.getHeightIn();
		if(userProfile.getAllowedProfilePic() != null) {
			profilePictureAccess = userProfile.getAllowedProfilePic()? ProfilePictureAccess.PUBLIC : ProfilePictureAccess.PRIVATE;
			this.transactions = userProfile.getMaxTransactions();
			if(this.transactions != null && transactions >0) {
				profilePictureAccess = ProfilePictureAccess.MANAGED;
			}

		}else
		{
			profilePictureAccess = ProfilePictureAccess.MANAGED;
		}
		this.activated = user.getActivated();
		this.banned = user.getBanned() == null ? false : user.getBanned();
		this.paused = user.getPaused() == null ? false : user.getPaused();
		this.handsCloudFile = new CloudStorageFileDto(userProfile.getHandsCloudFile());
		this.feetCloudFile = new CloudStorageFileDto(userProfile.getFeetCloudFile());
		this.legsCloudFile = new CloudStorageFileDto(userProfile.getLegsCloudFile());
        this.profilePhotoCloudFile = new CloudStorageFileDto(userProfile.getProfilePhotoCloudFile());
		this.handsFileSkipped = userProfile.getHandsFileSkipped();
		this.feetFileSkipped = userProfile.getFeetFileSkipped();
		this.legsFileSkipped = userProfile.getLegsFileSkipped();
		this.zodiac = userProfile.getZodiacSigns() == null ? ZodiacSigns.getZodiac(userProfile.getBirthDate()):  userProfile.getZodiacSigns();
		if (userProfile.getPersonalities() != null) {
			this.personalities = new UserPersonalityDto(userProfile.getPersonalities());
		}
		if (userProfile.getLifestyles() != null) {
			this.lifestyles = new UserLifeStylesDto(userProfile.getLifestyles());
		}
		if (userProfile.getStatuses() != null) {
			this.statuses = new UserStatusesDto(userProfile.getStatuses());
		}

		this.subscriptionType = userProfile.getSubscriptionType();

		if(userProfile.getAddress() != null)
			this.address = AddressDto.toAddressDto(userProfile.getAddress());

		UserPreference preference = user.getUserPreference();
		this.preferences = new UserPreferenceDto(user.getUserPreference());
		if (userProfile.getSpeakingLanguage() != null) {
			this.speakingLanguages = new HashSet<>();
			for (Language language : userProfile.getSpeakingLanguage()) {
				this.speakingLanguages.add(new LanguageDto(language));
			}
		}
		this.roles = user.getRoles().stream().map(userRole -> new RoleDto(userRole.getId().getRole())).collect(Collectors.toList());
	}


	public String getProfilePhotoCloudFileUrl() {
		return this.profilePhotoCloudFile != null && !StringUtils.isEmpty(this.profilePhotoCloudFile.getUrl())
				? this.profilePhotoCloudFile.getUrl() : "/images/icons/upload_icons/upload_profile_sample.png"
				;
	}

	public void setProfilePhotoCloudFileUrl(Boolean access, String baseUrl) {
		if(this.profilePhotoCloudFile == null || StringUtils.isEmpty(this.profilePhotoCloudFile.getUrl())) {
			this.profilePhotoCloudFile.setUrl(baseUrl +  "/images/icons/upload_icons/upload_profile_sample.png");
		} else {
			if(!access) {
				this.profilePhotoCloudFile.setUrl(baseUrl + "/images/private-photo-preview.png");
			} else if(!this.profilePhotoCloudFile.getApproved()) {
				this.profilePhotoCloudFile.setUrl(baseUrl + "/images/PhotoPending.png");
			}
		}
	}
	public void setHandsCloudFileUrl( String baseUrl) {
		if(getHandsFileSkipped() != null && getHandsFileSkipped()) {
			this.handsCloudFile.setUrl(baseUrl + "/images/skipped_hand_photo.png");
		}else if(this.handsCloudFile == null || StringUtils.isEmpty(this.handsCloudFile.getUrl())) {
			this.handsCloudFile.setUrl( baseUrl + "/images/photo-error.png");
		}else if(!this.handsCloudFile.getApproved()) {
				this.profilePhotoCloudFile.setUrl(baseUrl + "/images/PhotoPending.png");

		}
	}


	public void setFeetCloudFileUrl(String baseUrl) {
		if(getFeetFileSkipped() != null && getFeetFileSkipped()) {
			this.feetCloudFile.setUrl(baseUrl + "/images/skipped_foot_photo.png");
		}else
		if(this.feetCloudFile == null || StringUtils.isEmpty(this.feetCloudFile.getUrl())) {
			this.feetCloudFile.setUrl((baseUrl +"/images/photo-error.png"));
		}else if(!this.feetCloudFile.getApproved()) {
				this.feetCloudFile.setUrl(baseUrl + "/images/PhotoPending.png");

		}
	}


	public void setLegsCloudFileUrl( String baseUrl) {
		if(getLegsFileSkipped() != null && getLegsFileSkipped()) {
			this.legsCloudFile.setUrl(baseUrl + "/images/skipped_legs_photo.png");
		}else if(this.legsCloudFile == null || StringUtils.isEmpty(this.legsCloudFile.getUrl())) {
			this.legsCloudFile.setUrl((baseUrl +"/images/photo-error.png"));
		}else if(!this.legsCloudFile.getApproved()) {
				this.legsCloudFile.setUrl(baseUrl + "/images/PhotoPending.png");

		}

	}


	public Integer getAge() {
		if (this.birthDate == null) {
			return 0;
		}
		Calendar today = Calendar.getInstance();
		Calendar birthDate = Calendar.getInstance();
		birthDate.setTime(this.birthDate);
		if (birthDate.after(today)) {
			throw new IllegalArgumentException("You don't exist yet");
		}
		int todayYear = today.get(Calendar.YEAR);
		int birthDateYear = birthDate.get(Calendar.YEAR);
		int todayDayOfYear = today.get(Calendar.DAY_OF_YEAR);
		int birthDateDayOfYear = birthDate.get(Calendar.DAY_OF_YEAR);
		int todayMonth = today.get(Calendar.MONTH);
		int birthDateMonth = birthDate.get(Calendar.MONTH);
		int todayDayOfMonth = today.get(Calendar.DAY_OF_MONTH);
		int birthDateDayOfMonth = birthDate.get(Calendar.DAY_OF_MONTH);
		int age = todayYear - birthDateYear;

		// If birth date is greater than todays date (after 2 days adjustment of
		// leap year) then decrement age one year
		if ((birthDateDayOfYear - todayDayOfYear > 3) || (birthDateMonth > todayMonth)) {
			age--;

			// If birth date and todays date are of same month and birth day of
			// month is greater than todays day of month then decrement age
		} else if ((birthDateMonth == todayMonth) && (birthDateDayOfMonth > todayDayOfMonth)) {
			age--;
		}

		return age;
	}


}
