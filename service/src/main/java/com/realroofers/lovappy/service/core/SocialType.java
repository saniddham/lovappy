package com.realroofers.lovappy.service.core;

import com.fasterxml.jackson.annotation.JsonCreator;


/**
 * @author mwiyono
 */
public enum SocialType {
    FACEBOOK("facebook"), GOOGLE("google"), LINKEDIN("linkedin"),TWITTER("twitter") ,PINTEREST("pinterest"),INSTAGRAM("instagram"),EMAIl("email") ;
	String text;

	SocialType(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}
	@JsonCreator
	public static SocialType fromString(String string) {
		if ("facebook".equalsIgnoreCase(string)) {
			return FACEBOOK;
		} else if ("google".equalsIgnoreCase(string)) {
			return GOOGLE;
		} else if ("linkedin".equalsIgnoreCase(string)) {
			return LINKEDIN;
		} else if ("twitter".equalsIgnoreCase(string)) {
			return TWITTER;
		} else if ("instagram".equalsIgnoreCase(string)) {
			return INSTAGRAM;
		} else if ("pinterest".equalsIgnoreCase(string)) {
			return PINTEREST;

		}  else if ("email".equalsIgnoreCase(string)) {
			return EMAIl;

		}else {
			throw new IllegalArgumentException(string + " has no corresponding value");
		}
	}
}