package com.realroofers.lovappy.service.order.support;

/**
 * Created by hasan on 16/11/2017.
 */
public enum CouponRulesEntities {
    GIFT("GIFT"), MUSIC("MUSIC"), MESSAGE("MESSAGE"), AD("AD"), EVENT("EVENT");

    String text;
    CouponRulesEntities(String text){
        this.text = text;
    }
}
