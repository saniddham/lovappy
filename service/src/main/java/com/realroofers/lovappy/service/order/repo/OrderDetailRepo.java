package com.realroofers.lovappy.service.order.repo;

import com.realroofers.lovappy.service.order.model.OrderDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Eias Altawil on 5/20/17
 */
public interface OrderDetailRepo extends JpaRepository<OrderDetail, Integer>, QueryDslPredicateExecutor<OrderDetail> {
}
