package com.realroofers.lovappy.service.gift.dto;

import com.realroofers.lovappy.service.gift.model.GiftExchange;
import com.realroofers.lovappy.service.product.dto.ProductDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.vendor.model.VendorLocation;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * Created by Eias Altawil on 5/13/17
 */
@Data
@EqualsAndHashCode
public class GiftExchangeDto {

    private Integer id;
    private GiftDto gift;
    private UserDto fromUser;
    private UserDto toUser;
    private Date sentAt;
    private Double soldPrice;
    private Double commission;
    private String paymentMethod;
    private String cardNumber;
    private String refNumber;
    private Boolean redeemed;
    private Date redeemOn;
    private String redeemCode;
    private VendorLocation redeemAt;
    private String transactionID;

    public GiftExchangeDto() {
    }

    public GiftExchangeDto(GiftExchange giftExchange) {
        this.id = giftExchange.getId();
        this.gift = new GiftDto(new ProductDto(giftExchange.getGift()));
        this.fromUser = new UserDto(giftExchange.getFromUser());
        this.toUser = new UserDto(giftExchange.getToUser());
        this.sentAt = giftExchange.getSentAt();
        this.soldPrice = giftExchange.getSoldPrice() != null ? giftExchange.getSoldPrice() : 0d;
        this.commission = giftExchange.getCommission() != null ? giftExchange.getCommission() : 0d;
        this.paymentMethod = giftExchange.getPaymentMethod();
        this.cardNumber = giftExchange.getCardNumber();
        this.refNumber = giftExchange.getRefNumber();
        this.redeemed = giftExchange.getRedeemed();
        this.redeemOn = giftExchange.getRedeemOn();
        this.redeemAt = giftExchange.getRedeemAt();
        this.redeemCode =giftExchange.getRedeemCode();
        this.transactionID=giftExchange.getTransactionID();
    }

}
