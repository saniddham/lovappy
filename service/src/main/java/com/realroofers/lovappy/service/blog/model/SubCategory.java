package com.realroofers.lovappy.service.blog.model;

import lombok.EqualsAndHashCode;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/*
 * SubCategory Class 
 * 
 */

@SuppressWarnings("serial")
@Entity(name="subcategory")
@Table(name="subcategory")
@EqualsAndHashCode
public class SubCategory implements Serializable {
	
	@Id
	@Column(name="ID")
	@GeneratedValue
	private Integer subcatId;
	
	@Column(name="name")
	private String catName;
	
	@Column(name="state")
	private String catState;
	
	@ManyToOne(targetEntity=SubCategory.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "CATID", nullable = false)
	private Category category;	

	public Integer getSubcatId() {
		return subcatId;
	}

	public void setSubcatId(Integer subcatId) {
		this.subcatId = subcatId;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public String getCatState() {
		return catState;
	}

	public void setCatState(String catState) {
		this.catState = catState;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "SubCategory [subcatId=" + subcatId + ", catName=" + catName
				+ ", catState=" + catState + ", category=" + category + "]";
	}

}
