package com.realroofers.lovappy.service.order.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.user.model.User;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 * Created by Eias Altawil on 5/11/17
 */

@Entity
@Table(name="product_order")
@Data
@DynamicUpdate
public class ProductOrder extends BaseEntity<Long> {

    @ManyToOne
    @JoinColumn(name = "user")
    private User user;

    private Date date;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "orders_details",
            joinColumns = @JoinColumn(
                    name = "order_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "detail_id", referencedColumnName = "id"))
    private Collection<OrderDetail> ordersDetails;

    @Column(name = "transaction_id")
    private String transactionID;

    @ManyToOne
    private Coupon coupon;

    private Double totalPrice;

    @Enumerated(EnumType.STRING)
    private PaymentMethodType paymentMethod;

    private String cardType;
    private String cardNo;
}
