package com.realroofers.lovappy.service.likes;

import com.realroofers.lovappy.service.likes.dto.LikeDto;
import com.realroofers.lovappy.service.likes.model.Like;
import com.realroofers.lovappy.service.user.dto.UserLikesDto;
import com.realroofers.lovappy.service.user.support.Gender;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Set;

/**
 * Created by Daoud Shaheen on 7/22/2017.
 */
public interface LikeService {

    LikeDto getLikeById(Integer userLikeBy, Integer userLikeTo);

    Page<UserLikesDto> getLikesByUserLikeBy(int page, int limit, Integer userLikeBy);

    Page<UserLikesDto> getLikesByUserLikeTo(int page, int limit, Integer userLikeTo);

    Page<UserLikesDto> getMatchesForUser(int page, int limit, Integer userLikeBy);

    Long countAllLikeFromByGender(Integer likeFrom, Gender gender);

	Long countAllLikeToByGender(Integer likeTo, Gender gender);

    boolean likeme(Integer currentUserId, Integer userId);

    boolean liketo(Integer currentUserId, Integer userId);

    boolean match(Integer currentUserId, Integer userId);

    LikeDto addLike(Integer likeBy, Integer liketo);

    void unLike(Integer likeBy, Integer liketo);
    Integer countMatchesByUserId(Integer userId);

    Integer countMyLikesByUserId(Integer userId);

    Integer countLikeMeByUserId(Integer userId);

    List<Integer> filterUserLikesByUserIds(Integer currentUserId, List<Integer> userIds);

}
