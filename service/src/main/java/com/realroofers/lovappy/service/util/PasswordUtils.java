package com.realroofers.lovappy.service.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Daoud Shaheen on 6/25/2017.
 */
public class PasswordUtils {
    private static final char[] NUMS = {'0','1','2','3','4','5','6','7','8','9'};
    private static final char[] LOWER_CASE_LETTERS = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
    private static final char[] UPPER_CASE_LETTERS = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
    private static final char[] SPECIAL_CHARS = {'^','!','%','{','~','#','*'};

    /**
     * Generates secured password that contains numbers,
     * upper case and lower case letters and special characters
     *
     * @return String
     */
    public static String generatePassword() {
        Random random = new Random();
        String password = "";
        for (int i=0; i<4; i++) {
            password += NUMS[random.nextInt(NUMS.length)];
            password += LOWER_CASE_LETTERS[random.nextInt(LOWER_CASE_LETTERS.length)];
            password += UPPER_CASE_LETTERS[random.nextInt(UPPER_CASE_LETTERS.length)];
            password += SPECIAL_CHARS[random.nextInt(SPECIAL_CHARS.length)];
        }
        return shuffle(password);
    }

    /**
     * Shuffle string's characters
     *
     * @return String output
     */
    private static String shuffle(String input) {
        List<Character> characters = new ArrayList<>();
        for(char c:input.toCharArray()) {
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(input.length());
        while(characters.size() != 0) {
            int randPicker = (int)(Math.random() * characters.size());
            output.append(characters.remove(randPicker));
        }

        return output.toString();
    }
}
