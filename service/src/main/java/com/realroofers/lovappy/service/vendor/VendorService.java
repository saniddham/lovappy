package com.realroofers.lovappy.service.vendor;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.vendor.dto.BrandRegistrationDto;
import com.realroofers.lovappy.service.vendor.dto.VendorDto;

import java.util.List;

/**
 * Created by Manoj on 18/02/2018.
 */
public interface VendorService extends AbstractService<VendorDto, Integer> {

    VendorDto findByUser(Integer user);

    List<VendorDto> findByGiftId(Integer giftId);

    VendorDto registerBrand(Integer user,BrandRegistrationDto brandRegistrationDto) throws Exception;
}
