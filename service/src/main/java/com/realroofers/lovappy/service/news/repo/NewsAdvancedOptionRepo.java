package com.realroofers.lovappy.service.news.repo;

import com.realroofers.lovappy.service.news.model.NewsAdvancedOption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsAdvancedOptionRepo extends JpaRepository<NewsAdvancedOption, Long> {
public NewsAdvancedOption findNewsAdvancedOptionByNewsStory_Id(Long id);
}
