package com.realroofers.lovappy.service.likes.model;

import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 7/22/2017.
 */
@Embeddable
@EqualsAndHashCode
public class LikePK implements Serializable{


    private User likeBy;

    private User likeTo;

    public LikePK() {
    }

    public LikePK(User likeBy, User likeTo) {
        this.likeBy = likeBy;
        this.likeTo = likeTo;
    }

    @ManyToOne
    @JoinColumn(name = "like_by")
    public User getLikeBy() {
        return likeBy;
    }

    public void setLikeBy(User likeBy) {
        this.likeBy = likeBy;
    }

    @ManyToOne
    @JoinColumn(name = "like_to")
    public User getLikeTo() {
        return likeTo;
    }

    public void setLikeTo(User likeTo) {
        this.likeTo = likeTo;
    }
}
