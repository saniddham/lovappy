package com.realroofers.lovappy.service.likes.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 7/22/2017.
 */
@Entity
@DynamicUpdate
@Table(name = "users_likes")
@AssociationOverrides({
        @AssociationOverride(name = "id.likeBy",
                joinColumns = @JoinColumn(name = "like_by")),
        @AssociationOverride(name = "id.likeTo",
                joinColumns = @JoinColumn(name = "like_to")) })
@EqualsAndHashCode
public class Like implements Serializable{

    private LikePK id;

    @CreationTimestamp
    @Column(name = "created", insertable = false, updatable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date created;

    private Boolean seen;

    public Like() {
        created = new Date();
        seen = false;
    }

    @EmbeddedId
    public LikePK getId() {
        return id;
    }

    public void setId(LikePK id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Boolean getSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }
}
