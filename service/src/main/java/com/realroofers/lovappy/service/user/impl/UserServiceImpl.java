package com.realroofers.lovappy.service.user.impl;

import com.google.maps.model.LatLng;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.cloud.repo.CloudStorageRepo;
import com.realroofers.lovappy.service.core.SocialType;
import com.realroofers.lovappy.service.exception.GenericServiceException;
import com.realroofers.lovappy.service.exception.ResourceNotFoundException;
import com.realroofers.lovappy.service.exception.UnauthorizedUserException;
import com.realroofers.lovappy.service.likes.model.Like;
import com.realroofers.lovappy.service.likes.model.LikePK;
import com.realroofers.lovappy.service.likes.repo.LikeRepository;
import com.realroofers.lovappy.service.lovstamps.repo.LovstampRepo;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.dto.*;
import com.realroofers.lovappy.service.user.model.*;
import com.realroofers.lovappy.service.user.model.embeddable.Address;
import com.realroofers.lovappy.service.user.repo.*;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.ZodiacSigns;
import com.realroofers.lovappy.service.util.AddressUtil;
import com.realroofers.lovappy.service.vendor.dto.VendorDto;
import com.realroofers.lovappy.service.vendor.repo.VendorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;

import static com.realroofers.lovappy.service.user.model.QUser.user;

@Service("userService")
public class UserServiceImpl implements UserService {

    private static Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    private UserRepo userRepo;
    private UserProfileRepo userProfileRepo;
    private PasswordResetRequestRepo passwordResetRequestRepo;
    private PasswordEncoder passwordEncoder;
    private final RoleRepo roleRepo;
    private AddressUtil addressUtil;
    private UserFlagRepository userFlagRepository;
    private LikeRepository likeRepository;
    private final CloudStorageRepo cloudStorageRepo;
    private NotificationService notificationService;
    private final SocialProfileRepo socialProfileRepo;
    private final VendorRepository vendorRepository;
    private final LovstampRepo lovstampRepo;
    private final UserRolesRepo userRolesRepo;

    @Autowired
    public UserServiceImpl(UserRepo userRepo, UserProfileRepo userProfileRepo,
                           PasswordResetRequestRepo passwordResetRequestRepo, PasswordEncoder passwordEncoder,
                           AddressUtil addressUtil, UserFlagRepository userFlagRepository,
                           LikeRepository likeRepository, CloudStorageRepo cloudStorageRepo, RoleRepo roleRepo, NotificationService notificationService, SocialProfileRepo socialProfileRepo, VendorRepository vendorRepository, LovstampRepo lovstampRepo, UserRolesRepo userRolesRepo) {
        this.userRepo = userRepo;
        this.userProfileRepo = userProfileRepo;
        this.passwordResetRequestRepo = passwordResetRequestRepo;
        this.passwordEncoder = passwordEncoder;
        this.addressUtil = addressUtil;
        this.userFlagRepository = userFlagRepository;
        this.likeRepository = likeRepository;
        this.cloudStorageRepo = cloudStorageRepo;
        this.roleRepo = roleRepo;
        this.notificationService = notificationService;
        this.socialProfileRepo = socialProfileRepo;
        this.vendorRepository = vendorRepository;
        this.lovstampRepo = lovstampRepo;
        this.userRolesRepo = userRolesRepo;
    }

    // TODO Add the PasswordEncoder at the module dependency level
    //@Autowired
    //private PasswordEncoder passwordEncoder;
    @Transactional(readOnly = true)
    public User getUserById(Integer userId) {
        return userRepo.findOne(userId);
    }


    @Transactional(readOnly = true)
    public boolean isMusicianUser(Integer userId) {
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepo.findByName(Roles.MUSICIAN.getValue()));
        User user = userRepo.findByUserIdAndRolesIn(userId, roles);

        return user != null;
    }

    @Transactional(readOnly = true)
    public boolean isAuthor(Integer userId) {
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepo.findByName(Roles.AUTHOR.getValue()));
        User user = userRepo.findByUserIdAndRolesIn(userId, roles);

        return user != null;
    }

    @Transactional(readOnly = true)
    public boolean isVendor(Integer userId) {
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepo.findByName(Roles.VENDOR.getValue()));
        User user = userRepo.findByUserIdAndRolesIn(userId, roles);

        return user != null;
    }

    @Transactional(readOnly = true)
    public User getUserByEmail(String email) {
        return userRepo.findOneByEmail(email);
    }


    private boolean setUserRole(User user, Roles role, ApprovalStatus status, String password) {
        UserRolePK pk = new UserRolePK(user, roleRepo.findByName(role.getValue()));
        UserRoles userRoles = userRolesRepo.findOne(pk);
        if(userRoles != null)
            return false;
            userRoles = new UserRoles();
            userRoles.setId(pk);
        if(role.equals(Roles.USER.getValue()) || role.equals(Roles.COUPLE.getValue())) {
            userRoles.setApprovalStatus(ApprovalStatus.APPROVED);
        } else {
            userRoles.setApprovalStatus(status);
            userRoles.setPassphrase(password);
        }
        userRoles.setCreated(new Date());
        userRolesRepo.save(userRoles);
//        user.getRoles().add(userRoles);

        return true;
    }
    @Deprecated
    @Transactional
    public User registerNewUser(UserDto userDto) {
        User user = new User();
        user.setEmail(userDto.getEmail());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setEnabled(true);

        //user.setEmailVerificationKey(userDto.getEmailVerificationKey());
        user.setEmailVerified(false);

        /*UserProfile userProfile = new UserProfile();
        user.setUserProfile(userProfile);

        userProfile.setUser(user);*/


        User savedUser = userRepo.save(user);

        setUserRole(user, (userDto.getIsInLove()!=null && userDto.getIsInLove()) ?  Roles.COUPLE:Roles.USER, ApprovalStatus.APPROVED, null);
        UserProfile userProfile = new UserProfile(savedUser.getUserId());
        userProfile.setUser(savedUser);
        userProfile.setAvgDistanceOfViewers(0.0);
        userProfile.setAddress(addressUtil.extractAddress(userDto.getZipcode()));
        userProfile.setMaxTransactions(0);
        userProfileRepo.save(userProfile);

        return savedUser;
    }

    @Override
    @Transactional
    public UserDto registerNewUser(UserDto userDto,  WithinRadiusDto location) {
        User user = new User();
        user.setEmail(userDto.getEmail());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setEnabled(true);

        //user.setEmailVerificationKey(userDto.getEmailVerificationKey());
        user.setEmailVerified(false);

        /*UserProfile userProfile = new UserProfile();
        user.setUserProfile(userProfile);

        userProfile.setUser(user);*/


        User savedUser = userRepo.save(user);

        setUserRole(user, Roles.USER, ApprovalStatus.APPROVED, null);
////
        UserProfile userProfile = new UserProfile(savedUser.getUserId());
        userProfile.setUser(savedUser);
        userProfile.setAvgDistanceOfViewers(0.0);
        if(location != null) { //THis code should be moved out
            userProfile.setAddress(addressUtil.extractAddress(new LatLng(location.getLatitude(), location.getLongitude()), location.getRadius()));
        }
        userProfile.setMaxTransactions(0);
        userProfileRepo.save(userProfile);
////
        return new UserDto(savedUser);
    }

    @Transactional
    @Override
    public User createNewAdmin(UserDto userDto) {
        User user = new User();
        user.setEmail(userDto.getEmail());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setEnabled(true);


        user.setEmailVerified(true);

        User savedUser = userRepo.save(user);
        setUserRole(user, Roles.ADMIN, ApprovalStatus.APPROVED, null);
        UserProfile userProfile = new UserProfile(savedUser.getUserId());
        userProfile.setUser(savedUser);
        userProfile.setAvgDistanceOfViewers(0.0);
        userProfileRepo.save(userProfile);
        return savedUser;
    }

    @Transactional
    @Override
    public String registerDeveloper(DeveloperRegistration registration) {
        User user = userRepo.findOneByEmail(registration.getEmail());

        if(user == null) {
            user = new User();
            user.setEmail(registration.getEmail());
            user.setEnabled(true);

            user.setEmailVerified(true);

        }
            user.setPhoneNumber(registration.getPhoneNumber());
            user.setFirstName(registration.getName());
            User savedUser = userRepo.save(user);
            boolean exists = setUserRole(user, Roles.DEVELOPER, ApprovalStatus.PENDING, "");
            if (exists) {
                return "Already Applied";
            }
            UserProfile userProfile = userProfileRepo.findByUserId(savedUser.getUserId());
            if(userProfile == null) {
                userProfile = new UserProfile(savedUser.getUserId());
                userProfile.setUser(savedUser);
                userProfile.setSkills(registration.getSkills());
                userProfile.setAvgDistanceOfViewers(0.0);
            }
            userProfile.setSkills(registration.getSkills());
            try {
                userProfile.setAddress(addressUtil.extractAddress(registration.getZipCode()));
            } catch (Exception ex) {
                LOG.error("Can't extract address from zipcode");
                Address address = new Address();
                address.setZipCode(registration.getZipCode());
                address.setCity(registration.getCity());
                address.setState(registration.getState());
                userProfile.setAddress(address);
            }

            userProfileRepo.save(userProfile);
        return "success";
    }

    @Transactional(readOnly = true)
    @Override
    public Set<Integer> filterUserBlockedByUserIds(Integer currentUserId, List<Integer> userIds) {
        return userProfileRepo.findBlockedUserIdsByUserId(currentUserId, userIds);
    }
    @Transactional
    @Override
    public void activateUser(Integer userId, Boolean activate) {
       User user = userRepo.getOne(userId);
       user.setActivated(activate);
       userRepo.save(user);
    }
    @Transactional
    @Override
    public void updateProfile(Integer userId, String email, String firstName, String lastName) {
         User user = userRepo.getOne(userId);
         if(user == null){
             throw new ResourceNotFoundException("User with id " + userId);
         }

         User userE = userRepo.findOneByEmail(email);
         if(userE!=null&&userE.getUserId() != user.getUserId()){
          //nothing to do
         } else {
             if (!StringUtils.isEmpty(email)) {
                 user.setEmail(email);
             }
         }
         if(!StringUtils.isEmpty(firstName)) {
             user.setFirstName(firstName);
         }
        if(!StringUtils.isEmpty(lastName)) {
            user.setLastName(lastName);
        }

        userRepo.save(user);
    }

    @Transactional
    @Override
    public User createNewAuthor(UserDto userDto) {
        User user = new User();
        user.setEmail(userDto.getEmail());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setEnabled(true);

        user.setEmailVerified(true);

        User savedUser = userRepo.save(user);
        setUserRole(user, Roles.AUTHOR, ApprovalStatus.APPROVED, null);
        UserProfile userProfile = new UserProfile(savedUser.getUserId());
        userProfile.setUser(savedUser);
        userProfile.setMaxTransactions(0);
        userProfile.setAvgDistanceOfViewers(0.0);
        userProfileRepo.save(userProfile);
        return savedUser;
    }

    @Transactional
    @Override
    public boolean assignRole(String roleName, Integer userId) {
        User user = userRepo.findOne(userId);
        Role role = roleRepo.findByName(roleName);

        if (user != null && role != null) {
            UserRoles userRoles = userRolesRepo.findOne(new UserRolePK(user, role));
            if (userRoles == null) {

                userRepo.save(user);
                setUserRole(user, Roles.valueOf(roleName), ApprovalStatus.APPROVED, null);
                return true;
            }
        }
        return false;
    }

    @Transactional
    @Override
    public boolean approveAmbassador(Integer userId) {
        User user = userRepo.findOne(userId);
        if (user != null) {
            boolean status = assignRole(Roles.EVENT_AMBASSADOR.getValue(), userId);
            //is first time
            return status;
        }
        return false;
    }

    @Transactional
    @Override
    public boolean unassignRole(String roleName, Integer userId) {
        User user = userRepo.findOne(userId);
        Role role = roleRepo.findByName(roleName);

        if (user != null && role != null) {
            UserRoles userRoles = userRolesRepo.findOne(new UserRolePK(user, role));
            boolean roleExists = userRoles != null;

            if (roleExists) {
                user.getRoles().remove(userRoles);
                userRepo.save(user);
                return true;
            }

        }
        return false;
    }

    @Override
    @Transactional(readOnly = true)
    public User getUserMostExchangeMessagesWithByUserId(Integer userId) {
        return userRepo.findMostUserExchangeMessageWithByUserId(userId);
    }

    @Transactional
    @Override
    public void updateLastLogin(Integer userId) {
        User user = userRepo.findOne(userId);
        user.setLastLogin(new Date());
        user.setLoginsCount(user.getLoginsCount() == null ? 1 : user.getLoginsCount() + 1);
        userRepo.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AuthorDto> getAuthorsByActivated(Boolean activated, int page, int limit, Sort.Direction sorting) {
        Pageable pageRequest = null;
        if (sorting != null) {
            pageRequest = new PageRequest(page - 1, limit, sorting, "createdOn");
        } else {
            pageRequest = new PageRequest(page - 1, limit);
        }
        List<ApprovalStatus> statuses = activated? Collections.singletonList(ApprovalStatus.APPROVED) : Arrays.asList(ApprovalStatus.PENDING, ApprovalStatus.REJECTED);
        Page<User> users = userRepo.findDistinctByRolesInAndActivated(Collections.singleton(roleRepo.findByName(Roles.AUTHOR.getValue())), statuses, pageRequest);
        return users.map(AuthorDto::new);
    }

    @Override
    @Transactional
    public void activateAuthor(Integer userId, Boolean activate) {
        UserRolePK pk = new UserRolePK(userRepo.findOne(userId), roleRepo.findByName(Roles.AUTHOR.getValue()));
        UserRoles role = userRolesRepo.findOne(pk);

        role.setApprovalStatus(activate? ApprovalStatus.APPROVED : ApprovalStatus.REJECTED);
        userRolesRepo.save(role);
    }

    @Override
    @Transactional
    public boolean inviteAuthorByEmail(String email, String invitedByEmail) {
        User user = userRepo.findOneByEmail(email);
        if (user != null) {


            Role author = roleRepo.findByName(Roles.AUTHOR.getValue());
            UserRolePK pk = new UserRolePK(user, author);
            UserRoles userRoles = userRolesRepo.findOne(pk );
            if (userRoles == null) {
                user.getRoles().add(userRoles);

            }
            user.setInvitedBy(invitedByEmail);
            userRepo.save(user);
            return true;
        }
        return false;
    }

    @Override
    @Transactional(readOnly = true)
    public AuthorDto getAuthor(Integer userId) {
        Role author = roleRepo.findByName(Roles.AUTHOR.getValue());
        User user = userRepo.findByUserIdAndRolesIn(userId, Collections.singleton(author));

        if (user != null)
            return new AuthorDto(user);

        return null;
    }

    @Transactional
    @Override
    public User updateAuthorDetails(Integer userId, CloudStorageFileDto cloudStorageFileDto, String zipcode, String authorName) {
        CloudStorageFile cloudStorageFile = new CloudStorageFile();
        cloudStorageFile.setName(cloudStorageFileDto.getName());
        cloudStorageFile.setBucket(cloudStorageFileDto.getBucket());
        cloudStorageFile.setUrl(cloudStorageFileDto.getUrl());

        User user = userRepo.findOne(userId);
        user.getUserProfile().setProfilePhotoCloudFile(cloudStorageFile);

        try {
            user.getUserProfile().setAddress(addressUtil.extractAddress(zipcode));
        } catch (Exception ex) {
            LOG.error("Can't extract address from zipcode");
        }
        user.getUserProfile().getAddress().setZipCode(zipcode);
        if (authorName.contains(" ")) {
            String names[] = authorName.split(" ");

            user.setFirstName(names[0]);
            if (names.length > 1) {
                user.setLastName(names[1]);
            } else {
                user.setLastName("");
            }
        } else {
            user.setFirstName(authorName);
            user.setLastName("");
        }

        Role author = roleRepo.findByName(Roles.AUTHOR.getValue());
        UserRoles userRoles = userRolesRepo.findOne(new UserRolePK(user, author));
        if (userRoles == null) {
            setUserRole(user, Roles.AUTHOR, ApprovalStatus.APPROVED, null);
        }

        userRepo.saveAndFlush(user);
        return user;
    }



    @Override
    @Transactional
    public User createUserWithRole(UserAuthDto dto, Roles role) {
        User user = new User();
        user.setEnabled(dto.isEnabled());
        if(dto.getPassword() !=null)
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setEmail(dto.getEmail());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());


        user.setEmailVerified(true);

        User savedUser = userRepo.save(user);
        setUserRole(user, role, ApprovalStatus.PENDING, null);
        Integer savedUserID = savedUser.getUserId();

        UserProfile userProfile = new UserProfile(savedUserID);
        userProfile.setUser(savedUser);
        userProfileRepo.save(userProfile);

        NotificationDto notificationDto = new NotificationDto();
        if (role.equals(Roles.EVENT_AMBASSADOR)) {
            notificationDto.setContent(dto.getFirstName() + " is registered as an Ambassador in lovappy");
            notificationDto.setType(NotificationTypes.AMBASSADOR_REGISTRATION);
        } else if (role.equals(Roles.MUSICIAN)) {
            notificationDto.setContent(dto.getFirstName() + " is registered as an Musician in lovappy");
            notificationDto.setType(NotificationTypes.MUSICIAN_REGISTRATION);
        } else if (role.equals(Roles.AUTHOR)) {
            notificationDto.setContent(dto.getFirstName() + " is registered as an Author in lovappy");
            notificationDto.setType(NotificationTypes.AUTHOR_REGISTRATION);
        } else {
            notificationDto.setContent(dto.getFirstName() + " is registered in lovappy");
            notificationDto.setType(NotificationTypes.USER_REGISTRATION);
        }
        notificationDto.setUserId(savedUserID);
        notificationService.sendNotificationToAdmins(notificationDto);

        return user;
    }

    @Transactional
    @Override
    public UserAuthDto updateSocialMissingEmail(UserAuthDto userAuthDto) {
        User user = userRepo.findOneByEmail(userAuthDto.getEmail());
        if(user == null) {
            user = createUserWithRole(userAuthDto, Roles.USER);
        }
        SocialProfile socialProfile = socialProfileRepo.findBySocialIdAndSocialPlatform(userAuthDto.getSocialId(), SocialType.fromString(userAuthDto.getSocialPlatform()));
       if(socialProfile!=null) {
           socialProfile.setUser(user);
           socialProfileRepo.save(socialProfile);
       }
       userAuthDto.setUserId(user.getUserId());
       return userAuthDto;
    }

    @Override
    @Transactional
    public void update(UserAuthDto dto) {
        User user = userRepo.findOne(dto.getUserId());
        if (user == null) {
            throw new GenericServiceException("User with id " + dto.getUserId() + " does not exist.");
        }
        user.setEnabled(dto.isEnabled());
        user.setPassword(dto.getPassword());
        user.setEmail(dto.getEmail());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());

        userRepo.save(user);
    }

    @Override
    @Transactional
    public void update(UserAuthDto dto, Roles role) {
        User user = userRepo.findOne(dto.getUserId());
        if (user == null) {
            throw new GenericServiceException("User with id " + dto.getUserId() + " does not exist.");
        }
        user.setEnabled(dto.isEnabled());
        if(dto.getPassword() != null)
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setEmail(dto.getEmail());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());

        List<String> userRoles = getRolesForUser(dto.getUserId());

        boolean roleAdded = false;
        User savedUser = userRepo.save(user);
        if (!userRoles.contains(role.getValue())) {
            setUserRole(user, role, ApprovalStatus.PENDING, null);
            roleAdded = true;
        }



        Integer savedUserID = savedUser.getUserId();

        if (roleAdded) {
            NotificationDto notificationDto = new NotificationDto();
            if (role.equals(Roles.EVENT_AMBASSADOR)) {
                notificationDto.setContent(dto.getFirstName() + " is registered as an Ambassador in lovappy");
                notificationDto.setType(NotificationTypes.AMBASSADOR_REGISTRATION);
            } else if (role.equals(Roles.MUSICIAN)) {
                notificationDto.setContent(dto.getFirstName() + " is registered as an Musician in lovappy");
                notificationDto.setType(NotificationTypes.MUSICIAN_REGISTRATION);
            } else if (role.equals(Roles.AUTHOR)) {
                notificationDto.setContent(dto.getFirstName() + " is registered as an Author in lovappy");
                notificationDto.setType(NotificationTypes.AUTHOR_REGISTRATION);
            } else {
                notificationDto.setContent(dto.getFirstName() + " is registered in lovappy");
                notificationDto.setType(NotificationTypes.USER_REGISTRATION);
            }
            notificationDto.setUserId(savedUserID);
            notificationService.sendNotificationToAdmins(notificationDto);
        }
    }

    private boolean doesEmailExist(String email) {
        return userRepo.findOneByEmail(email) != null;
    }

//    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepo.findOneByEmail(email);

//        if (user == null) {
//            return new UserAuthDto(null,
//                    " ", " ", true, true, true, true,
//                    getAuthorities(Collections.singletonList(
//                            roleRepo.findByName(Roles.USER.getValue()))));
//        }

        UserAuthDto userAuthDto = new UserAuthDto(user.getUserId(),
                user.getEmail(), user.getPassword(), user.isEnabled(), true, true,
                true, getAuthorities(user.getRoles()));
        LOG.info(userAuthDto.toString());
        return userAuthDto;
    }

    public Collection<GrantedAuthority> getAuthorities(
            Collection<UserRoles> roles) {

        return getGrantedAuthorities(roles);
    }

    public Collection<GrantedAuthority> getAuthorities(
            Integer userId) {

        return getGrantedAuthorities(userRepo.findOne(userId).getRoles());
    }

    @Override
    @Transactional(readOnly = true)
    public UserAuthDto findOne(String socialId, String socialPlatform) {
        SocialProfile socialProfile = socialProfileRepo.findBySocialIdAndSocialPlatform(socialId, SocialType.fromString(socialPlatform));
        if(socialProfile == null)
            return null;
        User user = socialProfile.getUser();
        return user != null ? new UserAuthDto(user.getUserId(),
                user.getEmail(), user.getPassword(), user.isEnabled(), true, true,
                true, getAuthorities(user.getRoles())) : null;
    }

    @Override
    @Transactional(readOnly = true)
    public boolean emailExists(String email) {
        return emailExists(email, null);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean emailExists(String email, Integer excludeId) {
        QUser qUser = user;
        BooleanExpression expression = qUser.email.eq(email);
        if (excludeId != null) {
            expression = expression.and(qUser.userId.ne(excludeId));
        }
        return userRepo.count(expression) > 0;
    }

    @Override
    @Transactional
    public UserDto setNewPassword(String token, String password) {
        PasswordResetRequest request = passwordResetRequestRepo.findByToken(token);
        if (request != null) {
            User user = userRepo.findOne(request.getUserID());
            if (user != null) {
                user.setPassword(passwordEncoder.encode(password));
                return new UserDto(userRepo.save(user));
            }
        }
        return null;
    }
    @Override
    @Transactional
    public UserDto restNewPassword(String email, String password) {
            User user = userRepo.findOneByEmail(email);
            if (user != null) {
                user.setPassword(passwordEncoder.encode(password));
                return new UserDto(userRepo.save(user));
            }
        return null;
    }
    @Override
    @Transactional(readOnly = true)
    public UserDto getUser(Integer userId) {
        User user = userRepo.findOne(userId);
        return (user != null ? new UserDto(user) : null);
    }
    @Override
    @Transactional(readOnly = true)
    public ToUser getToUser(Integer userId) {
        User user = userRepo.findOne(userId);
        return (user != null ? new ToUser(user) : null);
    }
    @Override
    @Transactional(readOnly = true)
    public List<Integer> getUserLikeIDs(Integer userId) {
        User user = userRepo.findOne(userId);

        if (user != null) {
            return userRepo.findLikeIdsByIdLikeBy(user);

        }
        return new ArrayList<>();
    }


    @Override
    @Transactional(readOnly = true)
    public UserDto getUser(String email) {
        User user = userRepo.findOneByEmail(email);
        return (user != null ? new UserDto(user) : null);
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<UserDto> getAllUsers() {
        Collection<UserDto> users = new ArrayList<>();
        Iterable<User> all = userRepo.findAll();
        for (User user : all) {
            users.add(new UserDto(user));
        }

        return users;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserDto> findAllPageable(Pageable pageable) {
        Page<User> all = userRepo.findAll(pageable);
        return all.map(UserDto::new);
    }


    @Override
    @Transactional(readOnly = true)
    public Collection<UserDto> findWithUserRole() {
        Collection<UserDto> users = new ArrayList<>();
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepo.findByName(Roles.USER.getValue()));
        Iterable<User> nonAdminList = userRepo.findByRolesIn(roles, new PageRequest(0, Integer.MAX_VALUE));
        for (User user : nonAdminList) {
            users.add(new UserDto(user));
        }

        return users;
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<UserDto> getUserByRole(String role, int page, int limit) {
        Collection<UserDto> users = new ArrayList<>();
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepo.findByName(role));
        Pageable pageable = new PageRequest(page - 1, limit);
        Iterable<User> adminList = userRepo.findByRolesIn(roles, pageable).getContent();
        for (User user : adminList) {
            users.add(new UserDto(user));
        }

        return users;
    }

    @Override
    @Transactional(readOnly = true)
    public Integer countArtistByDateBetween(Date fromDate, Date toDate) {
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepo.findByName(Roles.MUSICIAN.getValue()));
        return userRepo.countByRolesIdRoleInAndCreatedOnBetween(roles, fromDate, toDate);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserDto> getUsersByRole(String role, Pageable pageable) {
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepo.findByName(role));

        return userRepo.findByRolesIn(roles, pageable).map(UserDto::new);
    }

    @Transactional
    @Override
    public boolean updatePassword(Integer userId, String oldPassword, String newPassword) {

        User user = userRepo.findOne(userId);

//        if(user.getPassword().equals(passwordEncoder.encode(oldPassword)))
//        return false;

        user.setPassword(passwordEncoder.encode(newPassword));
        userRepo.save(user);
        return true;
    }

    @Transactional
    @Override
    public boolean updateVendorPassword(Integer userId, String oldPassword, String newPassword) {

        User user = userRepo.findOne(userId);

        if(!passwordEncoder.matches(oldPassword,user.getPassword()))
        return false;

        user.setPassword(passwordEncoder.encode(newPassword));
        userRepo.save(user);
        return true;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserFlagDto> getUserComplaints(String searchEmail, Pageable pageable) {
        if (StringUtils.isEmpty(searchEmail))
            return userFlagRepository.findAllComplients(pageable);

        return userFlagRepository.findAllComplientsByEmail(searchEmail, pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserDto> getLastLoginUsers(String keyword, Pageable pageable, Date from, Date to) {
        if (StringUtils.isEmpty(keyword))
            return userRepo.findByLastLoginBetween(from, to, pageable).map(UserDto::new);
        return userRepo.findByLastLoginBetweenAndEmailLike(from, to, keyword, pageable).map(UserDto::new);
    }

    @Transactional
    @Override
    public boolean setBanUserStatus(Integer userId, boolean status) {
        User user = userRepo.findOne(userId);
        user.setBanned(status);
        userRepo.save(user);
        return true;
    }

    @Transactional
    @Override
    public boolean setPauseUserStatus(Integer userId, boolean status) {
        User user = userRepo.findOne(userId);
        user.setPaused(status);
        userRepo.save(user);
        return true;
    }

    @Override
    @Transactional
    public void revokeAdminRole(Integer adminId) {
        Role role = roleRepo.findByName(Roles.ADMIN.getValue());
        User admin = userRepo.findOne(adminId);
        admin.getRoles().remove(role);
        userRepo.save(admin);
    }

    @Override
    @Transactional(readOnly = true)
    public MyAccountDto getMyAccount(Integer userID) {
        User user = userRepo.findOne(userID);

        if (user == null)
            return null;

        return new MyAccountDto(user);
    }

    @Override
    @Transactional
    public MyAccountDto updateMyAccount(Integer userID, MyAccountDto myAccountDto) {
        User user = userRepo.findOne(userID);

        if (user == null) {
            return null;
        }

        user.setEmail(myAccountDto.getEmail());
        //save notifications preferences
        UserPreference userPreference = user.getUserPreference();
        if(userPreference == null) {
            userPreference = new UserPreference();
            userPreference.setUserId(userID);
            user.setUserPreference(userPreference);
        }
        user.setEmailNotifications(myAccountDto.getEmailNotifications());
        userPreference.setNewGiftsNotifications(myAccountDto.isNewGiftsNotifications());
        userPreference.setNewSongsNotifications(myAccountDto.isNewSongsNotifications());
        userPreference.setNewLikesNotifications(myAccountDto.isNewLikesNotifications());
        userPreference.setNewMatchesNotifications(myAccountDto.isNewMatchesNotifications());
        userPreference.setNewMessagesNotifications(myAccountDto.isNewMessagesNotifications());
        userPreference.setNewUnlockRequestNotifications(myAccountDto.isNewUnlockRequestNotifications());
        userPreference.setSongApproval(myAccountDto.getSongApproval());
        userPreference.setSongDenials(myAccountDto.getSongDenials());
        userPreference.setNewSongPurchase(myAccountDto.getNewSongPurchase());
        userRepo.save(user);

        return new MyAccountDto(user);
    }

    @Override
    @Transactional
    public AdminProfileDto updateAdminProfile(Integer userID, AdminProfileDto adminProfileDto) {
        User user = userRepo.findOne(userID);

        if (user == null) {
            return null;
        }

        user.setEmail(adminProfileDto.getEmail());
        if (!StringUtils.isEmpty(adminProfileDto.getPassword()))
            user.setPassword(passwordEncoder.encode(adminProfileDto.getPassword()));
        user.setFirstName(adminProfileDto.getFirstName());
        user.setLastName(adminProfileDto.getLastName());

        if (adminProfileDto.getProfileUrl() != null && adminProfileDto.getProfileUrl().getUrl() != null) {
            CloudStorageFile cloudStorageFile = new CloudStorageFile();
            cloudStorageFile.setName(adminProfileDto.getProfileUrl().getName());
            cloudStorageFile.setBucket(adminProfileDto.getProfileUrl().getBucket());
            cloudStorageFile.setUrl(adminProfileDto.getProfileUrl().getUrl());

            user.getUserProfile().setProfilePhotoCloudFile(cloudStorageFile);
        }

        userRepo.save(user);

        //update Authentication with new details
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof UsernamePasswordAuthenticationToken) {
            UsernamePasswordAuthenticationToken authenticationToken = (UsernamePasswordAuthenticationToken) authentication;
            authenticationToken.setDetails(user);
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }
        return new AdminProfileDto(user);
    }

    @Override
    @Transactional
    public UserDto verifyEmail(String key) {
        User user = userRepo.findByEmailVerificationKey(key);
        if (user != null) {
            //user.setEnabled(true);
            user.setEmailVerified(true);
            user.setEmailVerificationKey(null);
            userRepo.save(user);
            return new UserDto(user);
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public void authenticate(String username, String password) {
        User user = userRepo.findOneByEmail(username);

        if (user == null)
            throw new UnauthorizedUserException("Invalid Email");

        if (!passwordEncoder.matches(password, user.getPassword()))
            throw new UnauthorizedUserException("Invalid Password");

        //create user authentication object used later for authentication
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(username,
                user.getPassword(), getAuthorities(user.getRoles()));
        authentication.setDetails(user);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    public UserDetails loadUserSecurityByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findOneByEmail(username);

        if (user == null) {
            throw new UsernameNotFoundException("User '" + username + "' is not found");
        }

        return new UserAuthDto(user.getUserId(), user.getEmail(), user.getPassword(), true, true, true, true, getGrantedAuthorities(user.getRoles()));
    }

    /**
     * Returns granted authorities from a given role list
     *
     * @param roles user roles
     * @return List<GrantedAuthority>
     */
    private List<GrantedAuthority> getGrantedAuthorities(Collection<UserRoles> roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();

        for (UserRoles role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.getId().getRole().getName()));
        }
        return authorities;
    }

    /**
     * Get Username from user authenticated object
     *
     * @return username
     */
    @Transactional(readOnly = true)
    public User getAuthenticatedUsername() throws AuthorizationServiceException {
        org.springframework.security.core.Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new AuthorizationServiceException("The user is not authorized");
        }
        return userRepo.findOneByEmail(authentication.getName());
    }

    /**
     * Login service for admin
     *
     * @param username
     * @param password
     * @return
     */
    @Transactional(readOnly = true)
    public String adminLogin(String username, String password) {
        try {
            UserDetails user = loadUserSecurityByUsername(username);

            if (user != null) {
                if (!passwordEncoder.matches(password, user.getPassword())) {

                    LOG.error("Invalid Password " + password);
                    return "Invalid Password";
                }
                if (!(user.getAuthorities().contains(new SimpleGrantedAuthority(Roles.SUPER_ADMIN.getValue())) || user.getAuthorities().contains(new SimpleGrantedAuthority(Roles.ADMIN.getValue()))))
                    return "Access Denied";
            }

            //create user authentication object used later for authentication
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(username,
                    user.getPassword(), user.getAuthorities());
            authentication.setDetails(userRepo.findOneByEmail(username));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return "success";
        } catch (UsernameNotFoundException ex) {
            return "Invalid Email";
        }
    }

    @Override
    @Transactional
    public UserDto updateVerifyEmailKey(Integer userID, String key) {
        User user = userRepo.findOne(userID);
        if (user != null) {
            user.setEmailVerificationKey(key);
            userRepo.save(user);

            return new UserDto(user);
        }

        return null;
    }


    @Transactional
    @Override
    public String reportUser(UserFlagDto userFlagDto) {
        User flagTo = userRepo.findOne(userFlagDto.getFlagTo());
        User flagBy = userRepo.findOne(userFlagDto.getFlagBy());
        UserFlagPK pk = new UserFlagPK(flagBy, flagTo);
        UserFlag userFlag = userFlagRepository.findById(pk);
        if (userFlag == null) {
            userFlag = new UserFlag(pk);
        }

        userFlag.setDiscriminatory(userFlagDto.getDiscriminatory());
        userFlag.setHarrassment(userFlagDto.getHarrassment());
        userFlag.setNotRelevant(userFlagDto.getNotRelevant());
        userFlag.setOffensive(userFlagDto.getOffensive());
        userFlag.setUncivil(userFlagDto.getUncivil());
        userFlag.setOthers(userFlagDto.getOthers());

        userFlagRepository.save(userFlag);

        //block the user
        UserProfile model = flagBy.getUserProfile();
        List<User> blockedUsers = model.getBlockedUsers();

        blockedUsers.add(flagTo);
        userProfileRepo.save(model);

        //unlike the user

        LikePK id = new LikePK(flagBy, flagTo);
        Like likeYou = likeRepository.findById(id);
        if (likeYou != null) {

            //matched
            if (likeRepository.findById(new LikePK(flagTo, flagBy)) != null) {
                flagBy.setMatchesCount(flagBy.getMatchesCount() != null && flagBy.getMatchesCount() > 0 ? flagBy.getMatchesCount() - 1 : 0);
            }


            flagBy.setMyLikesCount(flagBy.getMyLikesCount() == null ? 0 : flagBy.getMyLikesCount() - 1);

            //save to save user counters
            likeRepository.save(likeYou);
            likeRepository.delete(likeYou);
        }

        return flagTo.getEmail();
    }


    @Override
    @Transactional(readOnly = true)
    public RegisterUserDto getAmbassador(Integer userId) {
        if (userId == null) {
            return null;
        }

        Role ambassador = roleRepo.findByName(Roles.EVENT_AMBASSADOR.getValue());
        User user = userRepo.findByUserIdAndRolesIn(userId, Collections.singleton(ambassador));

        if (user != null) {
            UserProfile userProfile = userProfileRepo.findOne(userId);
            return new RegisterUserDto(user, userProfile);
        }

        return null;
    }

    @Override
    @Transactional
    public User createNewRegistration(Integer userId, RegisterUserDto registerUserDto) {
        User user = getUserByEmail(registerUserDto.getEmail());
        if (user == null) {
            user = new User();
            user.setEmail(registerUserDto.getEmail());
            user.setPassword(passwordEncoder.encode(registerUserDto.getPassword()));
            user.setEnabled(true);

            user.setRoles(new HashSet<>());
            user.setEmailVerified(false);

            User savedUser = userRepo.save(user);

            UserProfile userProfile = new UserProfile(savedUser.getUserId());
            userProfile.setUser(savedUser);
            userProfile.setAvgDistanceOfViewers(0.0);
            userProfile.setAddress(addressUtil.extractAddress(registerUserDto.getZipcode()));

            userProfileRepo.save(userProfile);
            user = savedUser;
        }
        String name = registerUserDto.getName();
        if (name != null) {
            extractName(user, name);
        }
        userId = user.getUserId();
        user.setPhoneNumber(registerUserDto.getPhoneNumber());
        if (registerUserDto.getRole().equals(Roles.EVENT_AMBASSADOR)) {
            setUserRole(user, registerUserDto.getRole(), ApprovalStatus.PENDING, null);
        }
        List<String> userRoles = getRolesForUser(userId);
        Set<UserRoles> roles = new HashSet<>();


        if (!userRoles.contains(registerUserDto.getRole().getValue())) {
            setUserRole(user,registerUserDto.getRole(), ApprovalStatus.PENDING, null );
        }
        user.setRoles(roles);
        User savedUser = userRepo.save(user);

        UserProfile userProfile = userProfileRepo.findOne(userId);
        if (registerUserDto.getBirthDate() != null) {
            userProfile.setBirthDate(registerUserDto.getBirthDate());
            userProfile.setZodiacSigns(ZodiacSigns.getZodiac(registerUserDto.getBirthDate()));
        }
        if (registerUserDto.getGender() != null) {
            userProfile.setGender(registerUserDto.getGender());
        }
        if(registerUserDto.getPhotoIdentificationCloudFile() !=null && registerUserDto.getPhotoIdentificationCloudFile().getId() !=null) {
            userProfile.setPhotoIdentificationCloudFile(cloudStorageRepo.findOne(registerUserDto.getPhotoIdentificationCloudFile().getId()));
        }

        UserProfile saved = userProfileRepo.save(userProfile);
        if (savedUser != null && saved != null) {
            return user;
        }
        return user;
    }


    @Override
    @Transactional
    public User createNewVendorRegistration(RegisterUserDto registerUserDto) {
        User user = getUserByEmail(registerUserDto.getEmail());
        if (user == null) {
            user = new User();
            user.setEmail(registerUserDto.getEmail());
            user.setPassword(passwordEncoder.encode(registerUserDto.getPassword()));
            user.setEnabled(true);



            user.setEmailVerified(true);
            user.setEmailVerificationKey(null);
            user.setMobileVerified(false);

            user.setRoles(new HashSet<>());
            User savedUser = userRepo.save(user);


            UserProfile userProfile = new UserProfile(savedUser.getUserId());
            userProfile.setUser(savedUser);
            userProfile.setAvgDistanceOfViewers(0.0);
            userProfile.setAddress(addressUtil.extractAddress(registerUserDto.getZipcode()));

            userProfileRepo.save(userProfile);
            user = savedUser;
        }
        String name = registerUserDto.getName();
        if (name != null) {
            extractName(user, name);
        }
        Integer userId = user.getUserId();
        user.setPhoneNumber(registerUserDto.getPhoneNumber());

        List<String> userRoles = getRolesForUser(userId);

        if (!userRoles.contains(registerUserDto.getRole().getValue())) {
            setUserRole(user, registerUserDto.getRole(), ApprovalStatus.PENDING, null);
        }

        User savedUser = userRepo.save(user);

        UserProfile userProfile = userProfileRepo.findOne(userId);
        if (registerUserDto.getBirthDate() != null) {
            userProfile.setBirthDate(registerUserDto.getBirthDate());
            userProfile.setZodiacSigns(ZodiacSigns.getZodiac(registerUserDto.getBirthDate()));
        }
        if (registerUserDto.getGender() != null) {
            userProfile.setGender(registerUserDto.getGender());
        }
        if(registerUserDto.getPhotoIdentificationCloudFile() !=null && registerUserDto.getPhotoIdentificationCloudFile().getId() !=null) {
            userProfile.setPhotoIdentificationCloudFile(cloudStorageRepo.findOne(registerUserDto.getPhotoIdentificationCloudFile().getId()));
        }

        UserProfile saved = userProfileRepo.save(userProfile);
        if (savedUser != null && saved != null) {
            return user;
        }
        return user;
    }

    @Override
    @Transactional
    public User createRegistration(Integer userId, RegisterExistingUserDto registerExistingUserDto) {
        User user = userRepo.findOne(userId);
        String name = registerExistingUserDto.getName();
        if (name != null) {
            extractName(user, name);
        }
        userId = user.getUserId();
        user.setPhoneNumber(registerExistingUserDto.getPhoneNumber());

        List<String> userRoles = getRolesForUser(userId);

        if (!userRoles.contains(registerExistingUserDto.getRole().getValue())) {
            setUserRole(user, registerExistingUserDto.getRole(), ApprovalStatus.PENDING, null);
        }

        User savedUser = userRepo.save(user);

        UserProfile userProfile = userProfileRepo.findOne(userId);
        if (registerExistingUserDto.getBirthDate() != null) {
            userProfile.setBirthDate(registerExistingUserDto.getBirthDate());
            userProfile.setZodiacSigns(ZodiacSigns.getZodiac(registerExistingUserDto.getBirthDate()));
        }
        if (registerExistingUserDto.getGender() != null) {
            userProfile.setGender(registerExistingUserDto.getGender());
        }
        if(registerExistingUserDto.getPhotoIdentificationCloudFile() !=null && registerExistingUserDto.getPhotoIdentificationCloudFile().getId() !=null) {
            userProfile.setPhotoIdentificationCloudFile(cloudStorageRepo.findOne(registerExistingUserDto.getPhotoIdentificationCloudFile().getId()));
        }
        UserProfile saved = userProfileRepo.save(userProfile);
        if (savedUser != null && saved != null) {
            return user;
        }
        return user;
    }

    private void extractName(User user, String name) {
        if (name.contains(" ")) {
            user.setFirstName(name.substring(0, name.lastIndexOf(" ")).trim());
            user.setLastName(name.substring(name.lastIndexOf(" ")).trim());
        } else {
            user.setFirstName(name);
            user.setLastName("");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserDto> getAllByKeyword(String role, String keyword, Pageable pageable) {
       if("ALL".equals(role)) {
           QUser qUser = user;
           return userRepo.findAll(qUser.email.containsIgnoreCase(keyword), pageable).map(UserDto::new);
       }
       Set<Role> roles = new HashSet<>();
       if("AMBASSADOR".equals(role)) {
           roles.add(roleRepo.findByName(Roles.EVENT_AMBASSADOR.getValue()));
           roles.add(roleRepo.findByName(Roles.VENDOR.getValue()));
           roles.add(roleRepo.findByName(Roles.PLACE_OWNER.getValue()));
       } else if ("ADMIN".equals(role)){
           roles.add(roleRepo.findByName(Roles.ADMIN.getValue()));
           roles.add(roleRepo.findByName(Roles.SUPER_ADMIN.getValue()));
       } else {
           roles.add(roleRepo.findByName(role));
       }
       if(StringUtils.isEmpty(keyword)){
           return  userRepo.findByRoleIn(roles, pageable).map(UserDto::new);
       }

        return  userRepo.findByRoleInAndLikeEmail(roles, keyword, pageable).map(UserDto::new);
    }


    @Override
    @Transactional(readOnly = true)
    public Page<UserReviewDto> findAllUsersWithLovdrop(String keyword, Pageable pageable) {
        //map(UserDto::new)
        //return userRepo.findAllUsersWithLovstamp(keyword, pageable);
        return lovstampRepo.findAll(pageable).map(UserReviewDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserDto> getMusicianByKeyword(String keyword, Pageable pageable) {
        if(StringUtils.isEmpty(keyword))
            return userRolesRepo.findByRole(roleRepo.findByName(Roles.MUSICIAN.getValue()), pageable).map(UserDto::new);

        return userRolesRepo.findByRoleAndLikeEmail(roleRepo.findByName(Roles.MUSICIAN.getValue()), keyword.toLowerCase(), pageable).map(UserDto::new);
    }


    @Override
    @Transactional(readOnly = true)
    public Page<UserDto> getVendorByKeyword(String keyword, Pageable pageable) {
        if(StringUtils.isEmpty(keyword))
            return userRolesRepo.findByRole(roleRepo.findByName(Roles.VENDOR.getValue()), pageable).map(UserDto::new);
        return userRolesRepo.findByRoleAndLikeEmail(roleRepo.findByName(Roles.VENDOR.getValue()), keyword.toLowerCase(), pageable).map(UserDto::new); }

    @Transactional(readOnly = true)
    @Override
    public Integer countUsersByGender(Gender gender) {
        return userProfileRepo.countByGender(gender);
    }

    @Transactional
    @Override
    public void setTweeted(Boolean isTweeted, Integer userId) {
        User user = userRepo.findOne(userId);
        user.setTweeted(isTweeted);
        userRepo.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public List<String> getRolesForUser(Integer userId) {
        return userRepo.findUserRolesforUserId(userId);
    }

    @Transactional(readOnly = true)
    @Override
    public VendorDto findVendorByUserId(Integer user) {
        return new VendorDto(vendorRepository.findByUser_UserId(user));
    }

    @Override
    @Transactional
    public UserDto verifyMobile(Integer userId) {
        User user = userRepo.findOne(userId);
        if (user != null) {
            user.setEmailVerified(true);
            user.setMobileVerified(true);
            user.setEmailVerificationKey(null);
            userRepo.save(user);
            return new UserDto(user);
        }
        return null;
    }
}
