package com.realroofers.lovappy.service.user.dto;

import java.util.*;

import com.realroofers.lovappy.service.user.model.Role;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.google.common.collect.Sets;
import com.realroofers.lovappy.service.user.model.User;

// Previously called LovappyUser
@EqualsAndHashCode
public class UserAuthDto implements UserDetails {

    private static final long serialVersionUID = 7302501518540710819L;
    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean enabled = true;
    private boolean verified = true;
    private Integer userId;
    private String password;
    private String email;
    private String socialId;
    private String socialPlatform;
    private String firstName;
    private String lastName;
    private String imageUrl;
    private Double latitude;
    private Double longitude;
    private Collection<GrantedAuthority> authorities;
    //private Collection<RoleDto> roles;

    public UserAuthDto() {
    }

    public UserAuthDto(Integer userId) {
        this.userId = userId;
    }

    public UserAuthDto(User user) {
        authorities = new ArrayList<>();


        this.enabled = user.isEnabled();
        this.verified = user.getEmailVerified();
        this.userId = user.getUserId();
        this.password = user.getPassword();
        this.email = user.getEmail();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        if(user.getUserProfile() != null && user.getUserProfile().getAddress()!=null) {
            this.latitude = user.getUserProfile().getAddress().getLatitude();
            this.longitude = user.getUserProfile().getAddress().getLongitude();
        }

    }

    public UserAuthDto(String email, String socialId, String socialPlatform, String firstName, String lastName) {
        this.email = email;
        this.socialId = socialId;
        this.socialPlatform = socialPlatform;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public UserAuthDto(Integer userID, String email, String password, boolean enabled, boolean accountNonExpired,
                        boolean credentialsNonExpired,boolean accountNonLocked,
                        Collection<GrantedAuthority> authorities) {
        this.userId =  userID;
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.credentialsNonExpired = credentialsNonExpired;
        this.verified = enabled;
        this.enabled = true;
        this.password = password;
        this.email = email;
        this.authorities = authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    public Integer getUserId() {
        return userId;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getSocialPlatform() {
        return socialPlatform;
    }

    public void setSocialPlatform(String socialPlatform) {
        this.socialPlatform = socialPlatform;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAuthorities(Collection<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object rhs) {
        if (rhs instanceof UserAuthDto) {
            return email.equals(((UserAuthDto) rhs).email);
        }
        return false;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString()).append(": ");
        sb.append("Email: ").append(this.email).append("; ");
        sb.append("Password: [PROTECTED]; ");
        sb.append("Enabled: ").append(this.enabled).append("; ");
        sb.append("AccountNonExpired: ").append(this.accountNonExpired).append("; ");
        sb.append("credentialsNonExpired: ").append(this.credentialsNonExpired)
                .append("; ");
        sb.append("AccountNonLocked: ").append(this.accountNonLocked).append("; ");

        if (authorities != null && !authorities.isEmpty()) {
            sb.append("Granted Authorities: ");

            boolean first = true;
            for (GrantedAuthority auth : authorities) {
                if (!first) {
                    sb.append(",");
                }
                first = false;

                sb.append(auth);
            }
        }
        else {
            sb.append("Not granted any authorities");
        }

        return sb.toString();
    }

}
