package com.realroofers.lovappy.service.radio.impl;

import com.realroofers.lovappy.service.radio.AudioTypeService;
import com.realroofers.lovappy.service.radio.model.AudioType;
import com.realroofers.lovappy.service.radio.repo.AudioTypeRepo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Manoj on 17/12/2017.
 */
@Service
public class AudioTypeServiceImpl implements AudioTypeService {

    private final AudioTypeRepo audioTypeRepo;

    public AudioTypeServiceImpl(AudioTypeRepo audioTypeRepo) {
        this.audioTypeRepo = audioTypeRepo;
    }

    @Override
    public List<AudioType> findAll() {
        return audioTypeRepo.findAll();
    }

    @Override
    public List<AudioType> findAllActive() {
        return audioTypeRepo.findAllByActiveIsTrueOrderByName();
    }

    @Override
    public AudioType create(AudioType audioType) {
        return audioTypeRepo.saveAndFlush(audioType);
    }

    @Override
    public AudioType update(AudioType audioType) {
        return audioTypeRepo.save(audioType);
    }

    @Override
    public void delete(Integer id) {
        audioTypeRepo.delete(id);
    }

    @Override
    public AudioType findById(Integer id) {
        return audioTypeRepo.findOne(id);
    }

    @Override
    public AudioType findByName(String name) {
        return audioTypeRepo.findByName(name);
    }
}
