package com.realroofers.lovappy.service.career.model;

import com.realroofers.lovappy.service.career.dto.EducationDto;

import javax.persistence.*;
import java.util.Date;
import java.util.StringJoiner;

@Entity
@Table(name = "education")
public class Education {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    @Column(name = "education_id")
    private long id;
    private int endMonth;
    private int endYear;
    private int startMonth;
    private int startYear;
    private String schoolType;
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private CareerUser user;

    public Education(EducationDto education, CareerUser user) {
        this.endMonth = education.getEndMonth();
        this.endYear = education.getEndYear();
        this.schoolType = education.getSchoolType();
        this.startMonth = education.getStartMonth();
        this.startYear = education.getStartYear();
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public String getSchoolType() {
        return schoolType;
    }

    public void setSchoolType(String schoolType) {
        this.schoolType = schoolType;
    }

    public CareerUser getUser() {
        return user;
    }

    public void setUser(CareerUser user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Education.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("endMonth=" + endMonth)
                .add("endYear=" + endYear)
                .add("startMonth=" + startMonth)
                .add("startYear=" + startYear)
                .add("schoolType='" + schoolType + "'")
                .add("user=" + user)
                .toString();
    }
}
