package com.realroofers.lovappy.service.ads.model;


import com.realroofers.lovappy.service.ads.support.AdMediaType;
import com.realroofers.lovappy.service.ads.support.AdType;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * @author Eias Altawil
 */

@Entity
@Table(name="ad_pricing")
@EqualsAndHashCode
public class AdPricing {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private Integer duration;

    @Enumerated(EnumType.STRING)
    private AdType adType;

    @Enumerated(EnumType.STRING)
    private AdMediaType mediaType;

    private Double price;

    public AdPricing() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public AdType getAdType() {
        return adType;
    }

    public void setAdType(AdType adType) {
        this.adType = adType;
    }

    public AdMediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(AdMediaType mediaType) {
        this.mediaType = mediaType;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
