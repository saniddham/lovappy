package com.realroofers.lovappy.service.product.dto.mobile;

import com.realroofers.lovappy.service.product.dto.ProductDto;
import com.realroofers.lovappy.service.product.model.Brand;
import com.realroofers.lovappy.service.product.model.Product;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode
public class ItemDto {

    private Integer id;
    private String productName;
    private String productCode;
    private String productDescription;
    private BigDecimal price;
    private String image;
    private String image1;
    private String image2;
    private BrandItemDto brand;

    public ItemDto() {
    }

    public ItemDto(Product product) {
        if (product.getId() != null) {
            this.id = product.getId();
        }
        this.productName = product.getProductName();
        this.productCode = product.getProductCode();
        this.productDescription = product.getProductDescription();
        this.price = product.getPrice();
        if(product.getImage1()!=null) {
            this.image = product.getImage1().getUrl();
        }
        if(product.getImage2()!=null) {
            this.image1 = product.getImage2().getUrl();
        }
        if(product.getImage3()!=null) {
            this.image2 = product.getImage3().getUrl();
        }
        if(product.getBrand()!=null){
            this.brand= new BrandItemDto(product.getBrand());
        }
    }

    public ItemDto(ProductDto product) {
        if (product.getId() != null) {
            this.id = product.getId();
        }
        this.productName = product.getProductName();
        this.productCode = product.getProductCode();
        this.productDescription = product.getProductDescription();
        this.price = product.getPrice();
        if(product.getImage1()!=null) {
            this.image = product.getImage1().getUrl();
        }
        if(product.getImage2()!=null) {
            this.image1 = product.getImage2().getUrl();
        }
        if(product.getImage3()!=null) {
            this.image2 = product.getImage3().getUrl();
        }
        if(product.getBrand()!=null){
            this.brand= new BrandItemDto(new Brand(product.getBrand()));
        }
    }
}
