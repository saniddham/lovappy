package com.realroofers.lovappy.service.user;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.user.model.Country;

/**
 * Created by Manoj on 11/02/2018.
 */
public interface CountryService extends AbstractService<Country, Integer> {

    Country findByCode(String code);

    String[] getCountryNameList();
}
