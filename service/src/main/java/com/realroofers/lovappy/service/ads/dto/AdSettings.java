package com.realroofers.lovappy.service.ads.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Daoud Shaheen on 1/26/2018.
 */
@Data
@EqualsAndHashCode
public class AdSettings {
    private Integer textLimit;
    private Integer audioLimit;
    private Integer videoLimit;

    public AdSettings(Map<String, String> settings) {
        this.textLimit = Integer.parseInt(settings.get("text_limit"));
        this.audioLimit = Integer.parseInt(settings.get("audio_limit"));
        this.videoLimit = Integer.parseInt(settings.get("video_limit"));
    }

    public AdSettings() {
    }

    public Map<String, String> toMap(){
        Map<String, String> settings = new HashMap<>();
        settings.put("text_limit", String.valueOf(this.textLimit));
        settings.put("audio_limit", String.valueOf(this.audioLimit));
        settings.put("video_limit", String.valueOf(this.videoLimit));
        return settings;
    }
}
