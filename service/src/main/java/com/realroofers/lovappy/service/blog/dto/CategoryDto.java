package com.realroofers.lovappy.service.blog.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.realroofers.lovappy.service.blog.model.Category;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.util.ListMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Data
@EqualsAndHashCode
public class CategoryDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1229929263075919334L;

	private Integer id;

	private List<CloudStorageFileDto> images;


	@NotNull
	@NotEmpty(message = "Category name cannot be empty.")
	private String name;

	private String catState;

	private Date createdAt;
	public CategoryDto(Category category) {
		super();
		id = category.getId();
		this.name = category.getName();
		this.catState = category.getCatState();
		this.createdAt = category.getCreatedAt();
		images = new ListMapper<CloudStorageFile>(category.getImages()).map(CloudStorageFileDto::new);
	}

	public CategoryDto(Integer id) {
		super();
		id = id;
	}

	public CategoryDto() {
	}



	@Override
	public String toString() {
		return "Category [ID=" + id + ", name=" + name + ", catState=" + catState + "]";
	}

}
