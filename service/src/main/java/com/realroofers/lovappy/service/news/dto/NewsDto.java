package com.realroofers.lovappy.service.news.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.news.model.NewsAdvancedOption;
import com.realroofers.lovappy.service.news.model.NewsStory;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class NewsDto implements Serializable{
    private static final long serialVersionUID = 1229929263075919334L;


    private Long id;
    private String title;
    private String audioVideoUrl;
    private String contain;
    private String source;
    private Date submitDate;
    private Date postedDate;
    private Boolean posted;
    private String imagekeyword;
    private String imageDescription;
    private CloudStorageFileDto newsImage;
    private NewsAdvancedOption newsAdvancedOption;
    private CloudStorageFileDto newsUserIdentification;
    private CloudStorageFileDto newsVideo;
    private NewsCategoryDto newsCategory;

    public NewsDto() {
    }

    public NewsDto(NewsStory newsStory) {
        if(newsStory.getId()!=null)
        this.id = newsStory.getId();
        this.title = newsStory.getTitle();
        this.audioVideoUrl = newsStory.getAudioVideoUrl();
        this.contain = newsStory.getContain();
        this.source = newsStory.getSource();
        this.submitDate = newsStory.getSubmitDate();
        this.postedDate = newsStory.getPostedDate();
        this.posted = newsStory.getPosted();
        this.newsImage = new CloudStorageFileDto(newsStory.getNewsImage());
        this.newsAdvancedOption = newsStory.getNewsAdvancedOption();
        this.newsUserIdentification = new CloudStorageFileDto(newsStory.getNewsImage());
        this.newsVideo = new CloudStorageFileDto(newsStory.getNewsImage());
        this.newsCategory = new NewsCategoryDto(newsStory.getNewsCategory());
        this.imageDescription=newsStory.getImageDescription();
        this.imagekeyword=newsStory.getImageKeywords();
    }
}
