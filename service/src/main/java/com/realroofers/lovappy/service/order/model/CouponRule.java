package com.realroofers.lovappy.service.order.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import com.realroofers.lovappy.service.order.support.RuleActionType;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by hasan on 15/11/2017.
 */
@Entity
@Data
@DynamicUpdate
public class CouponRule extends BaseEntity<Integer> {


    private String ruleDescription;

    @Enumerated(EnumType.STRING)
    private CouponCategory affectedCategory;

    @Enumerated(EnumType.STRING)
    private RuleActionType actionType;

    private Double discountValue;
    private Date creationDate;
    private Date expiryDate;

}
