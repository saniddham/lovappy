package com.realroofers.lovappy.service.cms.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * Created by Daoud Shaheen on 6/6/2017.
 */
@MappedSuperclass
@EqualsAndHashCode
public abstract class Widget extends BaseEntity<Integer> {
    @Column(name = "name",unique=true)
    private String name;

    public Widget(String name) {
        this.name = name;
    }

    public Widget() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Widget that = (Widget) o;

        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        return name != null ? !name.equals(that.name) : that.name != null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Widget{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                '}';
    }
}
