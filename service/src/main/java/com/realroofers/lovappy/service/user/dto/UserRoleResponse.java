package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.user.model.Roles;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 5/13/2018.
 */
@Data
@EqualsAndHashCode
public class UserRoleResponse implements Serializable {
    private String passpharse;
    private Roles role;
    private String email;
    private Boolean approved;

    public UserRoleResponse(String passpharse, Roles role, String email, Boolean approved) {
        this.passpharse = passpharse;
        this.role = role;
        this.email = email;
        this.approved = approved;
    }
}
