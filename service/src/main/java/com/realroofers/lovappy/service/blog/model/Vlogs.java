package com.realroofers.lovappy.service.blog.model;

import com.realroofers.lovappy.service.blog.support.PostStatus;
import com.realroofers.lovappy.service.core.VideoProvider;
import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 8/8/2017.
 */
@Entity(name = "vlogs")
@Table(name="vlogs")
@DynamicUpdate
@EqualsAndHashCode
public class Vlogs extends BaseEntity<Integer> {


    private String title;

    private String keywords;

    private String captions;

    private String videoLink;

    @Column(name="state")
    @Enumerated(EnumType.STRING)
    private PostStatus state;

    @Column(name="video_provider")
    @Enumerated(EnumType.STRING)
    private VideoProvider videoProvider;

    @Column(name = "view_count")
    private Integer viewCount;

    private String videoId;

    public Vlogs() {
        setCreated(new Date());
        viewCount = 0;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getCaptions() {
        return captions;
    }

    public void setCaptions(String captions) {
        this.captions = captions;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public PostStatus getState() {
        return state;
    }

    public void setState(PostStatus state) {
        this.state = state;
    }

    public VideoProvider getVideoProvider() {
        return videoProvider;
    }

    public void setVideoProvider(VideoProvider videoProvider) {
        this.videoProvider = videoProvider;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }
}
