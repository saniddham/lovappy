package com.realroofers.lovappy.service.cms.impl;

import com.realroofers.lovappy.service.cms.CMSService;
import com.realroofers.lovappy.service.cms.dto.*;
import com.realroofers.lovappy.service.cms.model.*;
import com.realroofers.lovappy.service.cms.repo.PageMetaDataRepository;
import com.realroofers.lovappy.service.cms.repo.PageRepository;
import com.realroofers.lovappy.service.cms.repo.TextTranslationRepository;
import com.realroofers.lovappy.service.cms.repo.WidgetRepository;
import com.realroofers.lovappy.service.exception.ResourceNotFoundException;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.system.repo.LanguageRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by Daoud Shaheen on 6/6/2017.
 */
@Service("cmsService")
public class CMSServiceImpl implements CMSService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CMSServiceImpl.class);
    private PageRepository pageRepository;
    private WidgetRepository widgetRepository;
    private TextTranslationRepository textTranslationRepository;
    private PageMetaDataRepository pageMetaDataRepository;
    private final  LanguageRepo languageRepo;

    public CMSServiceImpl(PageRepository pageRepository,
                          WidgetRepository widgetRepository,
                          TextTranslationRepository textTranslationRepository, PageMetaDataRepository pageMetaDataRepository, LanguageRepo languageRepo) {
        this.pageRepository = pageRepository;
        this.widgetRepository = widgetRepository;
        this.textTranslationRepository = textTranslationRepository;
        this.pageMetaDataRepository = pageMetaDataRepository;
        this.languageRepo = languageRepo;
    }


    @Override
    @Transactional(readOnly = true)
    public List<Page> getAllPages() {
        return pageRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public org.springframework.data.domain.Page<Page> getAllPagesByName(String name, Pageable pageable) {
        return pageRepository.findAllByName(name, pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public List<Page> getAllActivePages() {
        return pageRepository.findAllByActiveIsTrue();
    }

    @Transactional(readOnly = true)
    @Override
    public Map<String, String> getPageSettings(String pageName) {
        return pageRepository.findByTag(pageName).getSettings();
    }

    @Override
    @Transactional
    public void updateSettings(String pageName, Map<String, String> updatedSettings) {
        Page page = pageRepository.findByTag(pageName);
        page.getSettings().putAll(updatedSettings);
//        for (Map.Entry<String, String> entry: updatedSettings.entrySet()) {
//            settings.put(entry.getKey(), entry.getValue());
//        }
//        page.setSettings(settings);
        pageRepository.save(page);
    }


    @Override
    @Transactional(readOnly = true)
    public List<Page> findAllOrderByActive() {
        return pageRepository.findAllByOrderByActiveDescNameAsc();
    }

    @Override
    @Transactional
    public Page updatePageStatus(Integer pageId) {
        Page page = pageRepository.findOne(pageId);
        if (page != null) {
            page.setActive(!page.getActive());
            pageRepository.save(page);
        }
        return page;
    }

    @Override
    @Transactional(readOnly = true)
    public List<String> getImagesByPageTagAndImageType(String pageTag, String imageType) {
        return widgetRepository.findByPageTagAndImageType(pageTag, imageType);
    }

    @Override
    @Transactional(readOnly = true)
    public Set<ImageWidget> getImagesByPageTag(String pageTag) {
        return pageRepository.findByTag(pageTag).getImages();
    }

    @Override
    @Transactional(readOnly = true)
    public PageDTO findPageByTagWithImagesAndTexts(String pageTag, String language) {
        Page page = pageRepository.findByTag(pageTag);
        if (page == null) {
            throw new ResourceNotFoundException("Page {" + pageTag + "} is not found");
        }


        Language lang = languageRepo.findByAbbreviation(language);
       Set<PageMetaData> pageMetaDataList = new HashSet<>();
        PageMetaData pageMetaData = pageMetaDataRepository.findByPageAndLanguage(page, lang);
        pageMetaDataList.add(pageMetaData);
        Set<ImageWidgetDTO> bgImages = widgetRepository.findImagesByPageIdAndType(page.getId(), ImageType.BACKGROUND.name().toLowerCase());
        Set<ImageWidgetDTO> bannersImages = widgetRepository.findImagesByPageIdAndType(page.getId(), ImageType.BANNER.name().toLowerCase());

        Set<TextWidgetDTO> texts = widgetRepository.findTextsByPageIdAndLanguage(page.getId(), language);

        Set<VideoWidgetDTO> videoWidgetDTOS = new HashSet<>();
        page.getVideos().forEach(videoWidget -> videoWidgetDTOS.add(new VideoWidgetDTO(videoWidget)));
        //Load images and texts because they are lazy loading
        PageDTO pageDTO = new PageDTO(page.getId(),page.getTag(), page.getName(), page.getActive(), pageMetaDataList, bgImages, bannersImages, texts, videoWidgetDTOS);

        return pageDTO;
    }

    @Override
    @Transactional
    public void updatePageImage(String pageTag, Integer imageId, ImageWidgetDTO imageWidgetDTO) {
        Page page = pageRepository.findByTag(pageTag);
        ImageWidget imageWidget = widgetRepository.findByImageId(imageId);
        imageWidget.setUrl(imageWidgetDTO.getUrl());
        page.getImages().add(imageWidget);
        widgetRepository.save(imageWidget);
        pageRepository.save(page);
    }

    @Override
    @Transactional
    public void savePageImage(String pageTag, ImageWidget imageWidget) {
        Page page = pageRepository.findByTag(pageTag);
        page.getImages().add(imageWidget);
        widgetRepository.save(imageWidget);
        pageRepository.save(page);
    }

    @Override
    @Transactional
    public void savePageText(String pageTag, TextWidgetDTO textWidgetDTO) {
        Page page = pageRepository.findByTag(pageTag);

        TextWidget term = new TextWidget();
        term.setName(pageTag + "-" + System.currentTimeMillis());
        TextTranslation translation = new TextTranslation();
        translation.setContent(textWidgetDTO.getContent());
        translation.setLanguage(textWidgetDTO.getLanguage());
        translation.setTextWidget(term);
        textTranslationRepository.save(translation);

        term.getTranslations().add(translation);

        page.getTextContents().add(widgetRepository.save(term));
        pageRepository.save(page);
    }


    @Override
    @Transactional
    public String updateTextByTagAndLanguage(String tag, String language, String content) {
        TextWidget textWidget = widgetRepository.findByTextName(tag);
        if (textWidget != null) {
            TextTranslation textTranslation = textTranslationRepository.findByTextWidgetAndLanguage(textWidget, language);
            textTranslation.setContent(content);
            textTranslationRepository.save(textTranslation);
            return textTranslation != null ? textTranslation.getContent() : "";
        }
        return null;
    }

    @Override
    public Set<VideoWidgetDTO> getVideosByPageTag(String pageTag) {
        Set<VideoWidgetDTO> videoWidgetDTOS = new HashSet<>();
        Page page = pageRepository.findByTag(pageTag);
        if (page != null) {
            page.getVideos().forEach(videoWidget -> videoWidgetDTOS.add(new VideoWidgetDTO(videoWidget)));
        }
        return videoWidgetDTOS;
    }

    @Transactional(readOnly = true)
    @Override
    public VideoWidgetDTO getVideosByKey(String key) {
        return new VideoWidgetDTO(widgetRepository.findByVideoName(key));
    }

    @Override
    @Transactional
    public void updatePageVideo(String pageTag, Integer videoId, VideoWidgetDTO videoWidgetDTO) {
        Page page = pageRepository.findByTag(pageTag);
        VideoWidget videoWidget = widgetRepository.findByVideoId(videoId);
        videoWidget.setUrl(videoWidgetDTO.getUrl());
        videoWidget.setProviderId(videoWidgetDTO.getProviderId());
        videoWidget.setProvider(videoWidgetDTO.getProvider());
        widgetRepository.save(videoWidget);
    }

    @Override
    public void updateMetaTags(PageMetaDTO PageMetaDTO, Integer metaId) {
        PageMetaData pageMetaData = pageMetaDataRepository.findOne(metaId);
        pageMetaData.setTitle(PageMetaDTO.getTitle());
        pageMetaData.setKeywords(PageMetaDTO.getKeywords());
        pageMetaData.setAbstractDescription(PageMetaDTO.getAbstractDescription());
        pageMetaData.setDescription(PageMetaDTO.getDescription());
        pageMetaDataRepository.save(pageMetaData);
    }

    @Override
    @Transactional(readOnly = true)
    public TextWidget getTextById(Integer textId) {
        TextWidget text = widgetRepository.findByTextId(textId);
        if (text == null) {
            throw new ResourceNotFoundException("Text {" + textId + "} is not found");
        }
        return text;
    }

    @Override
    @Transactional(readOnly = true)
    public String getTextByTagAndLanguage(String tag, String language) {
        TextWidget textWidget = widgetRepository.findByTextName(tag);
        if (textWidget != null) {
            TextTranslation textTranslation = textTranslationRepository.findByTextWidgetAndLanguage(textWidget, language);
            return textTranslation != null ? textTranslation.getContent() : "";
        }
        return "";
    }

    @Override
    @Transactional(readOnly = true)
    public TextTranslation getTextTranslationByTagAndLanguage(String tag, String language) {
        TextWidget textWidget = widgetRepository.findByTextName(tag);
        if (textWidget != null) {
            TextTranslation textTranslation = textTranslationRepository.findByTextWidgetAndLanguage(textWidget, language);
            return textTranslation;
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public String getImageUrlByTag(String tag) {
        ImageWidget imageWidget = widgetRepository.findByImageName(tag);
        return imageWidget == null ? "" : imageWidget.getUrl();
    }

    @Override
    @Transactional
    public TextTranslation updateTextByIdAndLanguage(Integer textId, String language, String updatedContent) {
        TextWidget textWidget = getTextById(textId);

        TextTranslation textTranslation = textTranslationRepository.findByTextWidgetAndLanguage(textWidget, language);
        if (textTranslation == null) {
            textTranslation = new TextTranslation();
            textTranslation.setLanguage(language);
            textTranslation.setTextWidget(textWidget);
        }
        textTranslation.setContent(updatedContent);
        return textTranslationRepository.save(textTranslation);
    }
}
