package com.realroofers.lovappy.service.datingPlaces.model;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.datingPlaces.support.DatingAgeRange;
import com.realroofers.lovappy.service.datingPlaces.support.DatingPlaceProvider;
import com.realroofers.lovappy.service.datingPlaces.support.NeighborHood;
import com.realroofers.lovappy.service.datingPlaces.support.PriceRange;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Darrel Rayen on 10/17/17.
 */
@Entity
@Table(name = "dating_place" , uniqueConstraints = {@UniqueConstraint(columnNames = {"externalPlaceId", "provider"})})

@EqualsAndHashCode
public class DatingPlace implements Serializable {

    @Id
    @GeneratedValue
    private Integer datingPlacesId;
    @Column( unique = true)
    private String externalPlaceId;

    @Enumerated(EnumType.STRING)
    private DatingPlaceProvider provider = DatingPlaceProvider.LOVAPPY;

    private String placeName;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile coverPicture;

    @Column(name = "country", nullable = false)
    private String country;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "address_line", nullable = false)
    private String addressLine;

    private String addressLineTwo;

    @Column(name = "zip_code", nullable = false)
    private String zipCode;

    private String state;

    @Column(name = "create_email", nullable = false)
    private String createEmail;

    @Column(name = "is_featured", columnDefinition = "BIT(1) default 0")
    private Boolean isFeatured;

    @Column(name = "lovestamp_featured", columnDefinition = "BIT(1) default 0")
    private Boolean loveStampFeatured;

    @Column(name = "map_featured", columnDefinition = "BIT(1) default 0")
    private Boolean mapFeatured;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Enumerated(EnumType.STRING)
    private DatingAgeRange ageRange;

    @Enumerated(EnumType.STRING)
    private PriceRange priceRange;

    @Enumerated(EnumType.STRING)
    private ApprovalStatus placeApprovalStatus;

    @Column(name = "approved", columnDefinition = "BIT(1) default 0")
    private Boolean approved;

    @Enumerated(EnumType.STRING)
    private NeighborHood neighborHood;

    private String placeDescription;

    @Column(name = "is_pets_allowed", columnDefinition = "BIT(1) default 0")
    private Boolean isPetsAllowed;

    @Column(name = "is_good_for_married_couple", columnDefinition = "BIT(1) default 0")
    private Boolean isGoodForMarriedCouple;

    @CreationTimestamp
    @Column(name = "created_date", nullable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date createdDate;

    private Date deletedDate;

    private Double latitude;

    private Double longitude;

    private Double averageRating;

    private Double parkingAverage;

    private Double securityAverage;

    private Double transportAverage;

    private Double googleAverage;

    private Integer suitableForMaleCount;

    private Integer suitableForFemale;

    private Integer suitableForBoth;

    private String ownerEmail;

    private String ownerContactNumber;

    private Double averageOverAllRatingByUser;

    @Column(name = "is_claimed", columnDefinition = "BIT(1) default 0")
    private Boolean isClaimed;

    @OneToMany(mappedBy = "placeId", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DatingPlaceReview> datingPlaceReviews = new ArrayList<>();

    @OneToMany(mappedBy = "placeId", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PlacesAttachment> placesAttachments = new ArrayList<>();

    @OneToMany(mappedBy = "placeId", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DatingPlaceClaim> placeClaimList = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "datingPlace", cascade = CascadeType.ALL)
    private List<DatingPlaceDeals> placeDeals = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "placeId", cascade = CascadeType.ALL)
    private List<DatingPlaceAtmospheres> placeAtmospheres = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "placeId", cascade = CascadeType.ALL)
    private List<DatingPlaceFoodTypes> foodTypes = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "placeId")
    private List<DatingPlacePersonTypes> personTypes = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner;

    public DatingPlace() {
        this.createdDate = new Date();
        this.isFeatured = false;
    }

    public Integer getDatingPlacesId() {
        return datingPlacesId;
    }

    public void setDatingPlacesId(Integer datingPlacesId) {
        this.datingPlacesId = datingPlacesId;
    }

    public String getExternalPlaceId() {
        return externalPlaceId;
    }

    public void setExternalPlaceId(String externalPlaceId) {
        this.externalPlaceId = externalPlaceId;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public CloudStorageFile getCoverPicture() {
        return coverPicture;
    }

    public void setCoverPicture(CloudStorageFile coverPicture) {
        this.coverPicture = coverPicture;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCreateEmail() {
        return createEmail;
    }

    public void setCreateEmail(String createEmail) {
        this.createEmail = createEmail;
    }

    public Boolean getFeatured() {
        return isFeatured;
    }

    public void setFeatured(Boolean featured) {
        isFeatured = featured;
    }

    public Boolean getLoveStampFeatured() {
        return loveStampFeatured;
    }

    public void setLoveStampFeatured(Boolean loveStampFeatured) {
        this.loveStampFeatured = loveStampFeatured;
    }

    public Boolean getMapFeatured() {
        return mapFeatured;
    }

    public void setMapFeatured(Boolean mapFeatured) {
        this.mapFeatured = mapFeatured;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public DatingAgeRange getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(DatingAgeRange ageRange) {
        this.ageRange = ageRange;
    }

    public PriceRange getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(PriceRange priceRange) {
        this.priceRange = priceRange;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public List<DatingPlaceReview> getDatingPlaceReviews() {
        return datingPlaceReviews;
    }

    public void setDatingPlaceReviews(List<DatingPlaceReview> datingPlaceReviews) {
        this.datingPlaceReviews = datingPlaceReviews;
    }

    public List<PlacesAttachment> getPlacesAttachments() {
        return placesAttachments;
    }

    public void setPlacesAttachments(List<PlacesAttachment> placesAttachments) {
        this.placesAttachments = placesAttachments;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddressLineTwo() {
        return addressLineTwo;
    }

    public void setAddressLineTwo(String addressLineTwo) {
        this.addressLineTwo = addressLineTwo;
    }

    public ApprovalStatus getPlaceApprovalStatus() {
        return placeApprovalStatus;
    }

    public void setPlaceApprovalStatus(ApprovalStatus placeApprovalStatus) {
        this.placeApprovalStatus = placeApprovalStatus;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }

    public String getPlaceDescription() {
        return placeDescription;
    }

    public void setPlaceDescription(String placeDescription) {
        this.placeDescription = placeDescription;
    }

    public Boolean getPetsAllowed() {
        return isPetsAllowed;
    }

    public void setPetsAllowed(Boolean petsAllowed) {
        isPetsAllowed = petsAllowed;
    }

    public Boolean getGoodForMarriedCouple() {
        return isGoodForMarriedCouple;
    }

    public void setGoodForMarriedCouple(Boolean goodForMarriedCouple) {
        isGoodForMarriedCouple = goodForMarriedCouple;
    }

    public Integer getSuitableForMaleCount() {
        return suitableForMaleCount;
    }

    public void setSuitableForMaleCount(Integer suitableForMaleCount) {
        this.suitableForMaleCount = suitableForMaleCount;
    }

    public Integer getSuitableForFemale() {
        return suitableForFemale;
    }

    public void setSuitableForFemale(Integer suitableForFemale) {
        this.suitableForFemale = suitableForFemale;
    }

    public Integer getSuitableForBoth() {
        return suitableForBoth;
    }

    public void setSuitableForBoth(Integer suitableForBoth) {
        this.suitableForBoth = suitableForBoth;
    }

    public Double getParkingAverage() {
        return parkingAverage;
    }

    public void setParkingAverage(Double parkingAverage) {
        this.parkingAverage = parkingAverage;
    }

    public Double getSecurityAverage() {
        return securityAverage;
    }

    public void setSecurityAverage(Double securityAverage) {
        this.securityAverage = securityAverage;
    }

    public Double getTransportAverage() {
        return transportAverage;
    }

    public void setTransportAverage(Double transportAverage) {
        this.transportAverage = transportAverage;
    }

    public Double getGoogleAverage() {
        return googleAverage;
    }

    public void setGoogleAverage(Double googleAverage) {
        this.googleAverage = googleAverage;
    }

    public NeighborHood getNeighborHood() {
        return neighborHood;
    }

    public void setNeighborHood(NeighborHood neighborHood) {
        this.neighborHood = neighborHood;
    }

    public Boolean getClaimed() {
        return isClaimed;
    }

    public void setClaimed(Boolean claimed) {
        isClaimed = claimed;
    }

    public List<DatingPlaceClaim> getPlaceClaimList() {
        return placeClaimList;
    }

    public void setPlaceClaimList(List<DatingPlaceClaim> placeClaimList) {
        this.placeClaimList = placeClaimList;
    }

    public List<DatingPlaceDeals> getPlaceDeals() {
        return placeDeals;
    }

    public void setPlaceDeals(List<DatingPlaceDeals> placeDeals) {
        this.placeDeals = placeDeals;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public String getOwnerContactNumber() {
        return ownerContactNumber;
    }

    public void setOwnerContactNumber(String ownerContactNumber) {
        this.ownerContactNumber = ownerContactNumber;
    }

    public List<DatingPlaceAtmospheres> getPlaceAtmospheres() {
        return placeAtmospheres;
    }

    public void setPlaceAtmospheres(List<DatingPlaceAtmospheres> placeAtmospheres) {
        this.placeAtmospheres = placeAtmospheres;
    }

    public List<DatingPlaceFoodTypes> getFoodTypes() {
        return foodTypes;
    }

    public void setFoodTypes(List<DatingPlaceFoodTypes> foodTypes) {
        this.foodTypes = foodTypes;
    }

    public List<DatingPlacePersonTypes> getPersonTypes() {
        return personTypes;
    }

    public void setPersonTypes(List<DatingPlacePersonTypes> personTypes) {
        this.personTypes = personTypes;
    }

    public Double getAverageOverAllRatingByUser() {
        return averageOverAllRatingByUser;
    }

    public void setAverageOverAllRatingByUser(Double averageOverAllRatingByUser) {
        this.averageOverAllRatingByUser = averageOverAllRatingByUser;
    }

    public DatingPlaceProvider getProvider() {
        return provider;
    }

    public void setProvider(DatingPlaceProvider provider) {
        this.provider = provider;
    }
}
