package com.realroofers.lovappy.service.product.repo;

import com.realroofers.lovappy.service.product.model.Brand;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BrandRepo extends JpaRepository<Brand, Integer> {

    Brand findByBrandName(String brandName);

    List<Brand> findAllByActiveIsTrue();

    @Query("select b from Brand b where b.createdBy=:user and (:name is null or b.brandName like :name)")
    Page<Brand> findAllByCreatedBy(@Param(value = "user") User createdBy,@Param("name") String name, Pageable pageable);

    long countAllByCreatedBy(User createdBy);
}
