package com.realroofers.lovappy.service.user;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.user.dto.*;
import com.realroofers.lovappy.service.user.support.SubscriptionType;
import com.realroofers.lovappy.service.user.support.UnlockRequestStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public interface UserProfileService {

    UserProfileDto findOne(Integer usedId);

    UserProfileDto findByUserId(Integer id);

    void saveOrUpdate(UserProfileDto userProfileDto);
    CloudStorageFileDto getAllowedProfilePhoto(Integer memberId, Integer currentId);
    PersonalityLifestyleDto getPersonalityLifestyle(Integer userId);

    void saveOrUpdate(Integer userId, PersonalityLifestyleDto dto);

    UserProfileDto update(Integer userID, Collection<LanguageDto> speakingLanguages, AddressDto address);

    PersonalityLifestyleStatusLocationDto getPersonalityLifestyleStatusLocation(Integer userId);

    void saveOrUpdate(Integer userId, PersonalityLifestyleStatusLocationDto dto);
    
    ProfilePicsDto getProfilePicsDto(Integer userId);

    void saveSubscriptionType(Integer userId, SubscriptionType subscriptionType);

    SubscriptionType getSubscriptionType(Integer userId);

    void saveHandsPhoto(CloudStorageFileDto cloudStorageFile);

    void saveFeetPhoto(CloudStorageFileDto cloudStorageFile);

    void saveLegsPhoto(CloudStorageFileDto cloudStorageFile);

    void saveProfilePhoto(Integer userId, CloudStorageFileDto cloudStorageFile);

    void saveIdentificationProfilePhoto(CloudStorageFileDto cloudStorageFile);

    void skipHandsPhoto();

    void skipAllPhotos();

    void skipVoice();

    void skipFeetPhoto();

    void skipLegsPhoto();

    CloudStorageFileDto showProfilePhotoForUser(Integer userId, Integer currentUserId);
	/**
	 * To get Full Profile in one call DB
	 * @param usedId
	 * @return
	 */
	MemberProfileDto findFullOne(Integer usedId);

    /**
     * To get Full Profile in one call DB
     * @return
     */
    MemberProfileDto findFullOneWihtAllowedProfilePic(Integer memberId, Integer currentId);
    MemberFullProfileDto getFullProfile(Integer usedId);
	/**
	 * 
	 * @param searchDto
	 * @return
	 */
    List<UserLocationDto> getNearbyUsers(WithinRadiusDto searchDto);

    Long countNearbyUsers(Integer userId, WithinRadiusDto searchDto);
    /**
     * 
     * @param loginUserId
     * @param blockedUserId
     * @return
     */
	int blockUser(Integer loginUserId, Integer blockedUserId);

	
	List<Integer> getBlockUserIdList(Integer loginUserId);

	Boolean getBlockStatus(Integer loginUserId, Integer checkUserId);

	int unblockUser(Integer loginUserId, Integer blockedUserId);


    void updateUserProfileViewsAndAvgDistance(Integer userId, Integer viewerId);

    Integer getTotalCommunicationsCount(Integer userID1, Integer userID2);

    void enableProfileAccessView(Integer userId, Integer viewerId);

    void disableProfileAccessView(Integer userId, Integer viewerId);

    boolean isProfilePicAccessed(Integer userId, Integer viewerId);

    void setProfileImageAccess(boolean access, Integer maxTransactions, Integer userId);

    Long countUserProfileImageAccess(Integer userId);

    Page<UserProfilePictureAccessDto> getUserProfileImageAccess(Integer userId, int page, int limit);
    boolean lockedByTransactionAlgorithem(Integer userFromId, Integer userToId);
    boolean saveTransaction(Integer userFromId, Integer userToId);
    Long countPendingProfileAccess(Integer userId, Integer maxTransactions);

    Page<UserProfilePictureAccessDto> getPendingUserProfileImageAccess(Integer userId, Integer maxTransactions, Pageable pageable);


    Long countUnseenProfileImageAccess(Integer userId);

    void setTransactionAsSeen(Integer userId);
    Set<Integer> filterUserBlocksByUserIds(Integer currentUserId, List<Integer> userIds);

    String getStripeIdByUserId(Integer userId);
    void saveStripeCustomerId(Integer userId, String customerId);
}
