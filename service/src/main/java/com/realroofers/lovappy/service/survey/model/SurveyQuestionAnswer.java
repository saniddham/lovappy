package com.realroofers.lovappy.service.survey.model;


import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@EqualsAndHashCode
public class SurveyQuestionAnswer {

    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    private UserSurvey userSurvey;


    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private SurveyQuestion question;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private SurveyAnswer answer;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserSurvey getUserSurvey() {
        return userSurvey;
    }

    public void setUserSurvey(UserSurvey userSurvey) {
        this.userSurvey = userSurvey;
    }

    public SurveyQuestion getQuestion() {
        return question;
    }

    public void setQuestion(SurveyQuestion question) {
        this.question = question;
    }

    public SurveyAnswer getAnswer() {
        return answer;
    }

    public void setAnswer(SurveyAnswer answer) {
        this.answer = answer;
    }
}
