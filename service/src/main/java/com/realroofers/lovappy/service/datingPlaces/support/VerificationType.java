package com.realroofers.lovappy.service.datingPlaces.support;

/**
 * Created by Darrel Rayen on 1/4/18.
 */
public enum VerificationType {
    SMS("sms"),
    CALL("Call");

    String text;

    VerificationType(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
