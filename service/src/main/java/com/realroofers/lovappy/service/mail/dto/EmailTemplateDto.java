package com.realroofers.lovappy.service.mail.dto;

import com.realroofers.lovappy.service.mail.model.EmailTemplate;
import com.realroofers.lovappy.service.mail.model.EmailTemplateContent;
import com.realroofers.lovappy.service.mail.support.EmailTypes;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Daoud Shaheen on 9/6/2017.
 */
@EqualsAndHashCode
public class EmailTemplateDto{

    private Integer id;

    private String name;

    private String subject;

    private String body;

    private String htmlBody;

    private String language;

    private String fromEmail;

    private String fromName;

    private String templateUrl;

    private Boolean enabled;

    private EmailTypes type;

    private String imageUrl;

    private Map<String, String> additionalInformation = new HashMap<>();
    private Map<String, String> variables = new HashMap<>();

    public EmailTemplateDto() {
    }
    public EmailTemplateDto(EmailTemplateContent emailTemplateContent) {
        if(emailTemplateContent != null) {
            EmailTemplate emailTemplate = emailTemplateContent.getTemplate();
            this.id = emailTemplate.getId();
            this.body = emailTemplateContent.getBody();
            this.subject = emailTemplateContent.getSubject();
            this.htmlBody = emailTemplateContent.getHtmlBody();
            this.language = emailTemplateContent.getLanguage();
            this.name = emailTemplate.getName();
            this.fromEmail = emailTemplate.getFromEmail();
            this.fromName = emailTemplate.getFromName();
            this.templateUrl = emailTemplate.getTemplateUrl();
            this.enabled = !emailTemplate.getEnabled(); //this is only for UI
            this.type = emailTemplate.getType();
            this.imageUrl = emailTemplate.getImageUrl();
        }
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getHtmlBody() {
        return htmlBody;
    }

    public void setHtmlBody(String htmlBody) {
        this.htmlBody = htmlBody;
    }

    public String getLanguage() {
        if(StringUtils.isEmpty(language))
            return "EN";
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getTemplateUrl() {
        return templateUrl;
    }

    public void setTemplateUrl(String templateUrl) {
        this.templateUrl = templateUrl;
    }

    public Boolean getEnabled() {
        return enabled;
    }
    public Boolean isEnabled() {
        return !enabled;
    }
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Map<String, String> getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(Map<String, String> additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public Map<String, String> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, String> variables) {
        this.variables = variables;
    }

    public EmailTypes getType() {
        return type;
    }

    public void setType(EmailTypes type) {
        this.type = type;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
         return ToStringBuilder.reflectionToString(this);
    }
}
