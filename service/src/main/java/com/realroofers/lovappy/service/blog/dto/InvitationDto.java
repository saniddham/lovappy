package com.realroofers.lovappy.service.blog.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.realroofers.lovappy.service.validator.UserRegister;

/**
 * @author mwiyono
 */
@EqualsAndHashCode
public class InvitationDto  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -580291464581250654L;

    @NotEmpty(message = "You should provide your email address.", groups = {UserRegister.class})
    @NotNull(message = "You should provide your email address.", groups = {UserRegister.class})
    @Email(message = "This is not a valid email address.", groups = {UserRegister.class})
	private String email;

	private String content;
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	
	

}
