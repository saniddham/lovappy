package com.realroofers.lovappy.service.survey.dto;

import com.realroofers.lovappy.service.survey.model.SurveyQuestionAnswer;
import com.realroofers.lovappy.service.survey.model.UserSurvey;
import com.realroofers.lovappy.service.user.dto.UserDto;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by hasan on 9/8/2017.
 */
@EqualsAndHashCode
public class UserSurveyDto {
    private Map<Integer, Integer> answers;
    private Integer id;
    private UserDto userDto;
    private List<SurveyQuestionAnswerDto> surveyQuestionAnswer;

    public UserSurveyDto() {
    }

    public UserSurveyDto(UserSurvey userSurvey) {
        this.id = userSurvey.getId();
        this.userDto = new UserDto(userSurvey.getUser());

        if (userSurvey.getQuestionAnswers() != null){
            surveyQuestionAnswer = new ArrayList<>();
            for (SurveyQuestionAnswer questionAnswer : userSurvey.getQuestionAnswers()){
                surveyQuestionAnswer.add(new SurveyQuestionAnswerDto(questionAnswer));
            }
        }
    }

    public UserSurveyDto(Map<Integer, Integer> answers, Integer id) {
        this.answers = answers;
        this.id = id;
    }

    public Map<Integer, Integer> getAnswers() {
        return answers;
    }

    public void setAnswers(Map<Integer, Integer> answers) {
        this.answers = answers;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }

    public List<SurveyQuestionAnswerDto> getSurveyQuestionAnswer() {
        return surveyQuestionAnswer;
    }

    public void setSurveyQuestionAnswer(List<SurveyQuestionAnswerDto> surveyQuestionAnswer) {
        this.surveyQuestionAnswer = surveyQuestionAnswer;
    }
}
