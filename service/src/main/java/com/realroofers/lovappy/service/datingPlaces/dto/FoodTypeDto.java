package com.realroofers.lovappy.service.datingPlaces.dto;

import com.realroofers.lovappy.service.datingPlaces.model.FoodType;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * Created by Darrel Rayen on 12/9/17.
 */
@EqualsAndHashCode
public class FoodTypeDto implements Serializable{

    private Integer id;
    private String foodTypeName;
    private Boolean enabled;

    public FoodTypeDto() {
    }

    public FoodTypeDto(FoodType foodType) {
        this.id = foodType.getId();
        this.foodTypeName = foodType.getFoodTypeName();
        this.enabled = foodType.getEnabled();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFoodTypeName() {
        return foodTypeName;
    }

    public void setFoodTypeName(String foodTypeName) {
        this.foodTypeName = foodTypeName;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
