package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.lovstamps.model.Lovstamp;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.EqualsAndHashCode;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 7/25/2017.
 */
@EqualsAndHashCode
public class UserLikesDto {

    private Integer id;
    private String city;
    private Integer age;
    private Gender gender;
    private Boolean listened;
    private String language;
    private CloudStorageFileDto audioFileCloud;
    private Boolean blocked;
    private Boolean matched;
    private Boolean likedBy;
    public UserLikesDto() {
    }

    public UserLikesDto(User user, Lovstamp lovstamp, Boolean blocked) {
        this.id = user.getUserId();
        if(user.getUserProfile()!=null) {
            if (user.getUserProfile().getAddress() != null && user.getUserProfile().getAddress().getStateShort() != null) {

                this.city = user.getUserProfile().getAddress().getStateShort().length() > 2 ? user.getUserProfile().getAddress().getStateShort().substring(0, 2) : user.getUserProfile().getAddress().getStateShort();
            } else
                this.city = "";

            setAge(user.getUserProfile().getBirthDate());
            this.gender = user.getUserProfile().getGender();
            this.listened = false;
            setLanguage(user.getUserProfile().getSpeakingLanguage());
        }
        if(lovstamp != null)
        this.audioFileCloud = new CloudStorageFileDto(lovstamp.getAudioFileCloud());
        this.blocked = blocked ;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setAge(Date birthDate) {
        if (birthDate == null) {
            this.age = 0;
            return;
        }
        Calendar today = Calendar.getInstance();
        Calendar birthDate1 = Calendar.getInstance();
        birthDate1.setTime(birthDate);
        if (birthDate1.after(today)) {
            this.age = 0;
            //throw new IllegalArgumentException("You don't exist yet");
        }
        int todayYear = today.get(Calendar.YEAR);
        int birthDateYear = birthDate1.get(Calendar.YEAR);
        int todayDayOfYear = today.get(Calendar.DAY_OF_YEAR);
        int birthDateDayOfYear = birthDate1.get(Calendar.DAY_OF_YEAR);
        int todayMonth = today.get(Calendar.MONTH);
        int birthDateMonth = birthDate1.get(Calendar.MONTH);
        int todayDayOfMonth = today.get(Calendar.DAY_OF_MONTH);
        int birthDateDayOfMonth = birthDate1.get(Calendar.DAY_OF_MONTH);
        this.age = todayYear - birthDateYear;

        // If birth date is greater than todays date (after 2 days adjustment of leap year) then decrement age one year
        if ((birthDateDayOfYear - todayDayOfYear > 3) || (birthDateMonth > todayMonth)) {
            this.age--;

            // If birth date and todays date are of same month and birth day of month is greater than todays day of month then decrement age
        } else if ((birthDateMonth == todayMonth) && (birthDateDayOfMonth > todayDayOfMonth)) {
            this.age--;
        }
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }



    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setLanguage(Collection<Language> languages) {
        String sl = "";
        if(languages != null) {
            for (Language languageDto : languages) {
                sl = sl.concat(languageDto.getAbbreviation());
                break;
            }
        }

        this.language =  sl;
    }

    public String getUserDetails() {
        return getAge()+" / "+((getCity() == null)?"":getCity()) +" / "+ getLanguage();
    }

    public Boolean getListened() {
        return listened;
    }

    public void setListened(Boolean listened) {
        this.listened = listened;
    }

    public CloudStorageFileDto getAudioFileCloud() {
        return audioFileCloud;
    }

    public String getAudioFileCloudUrl() {
        return audioFileCloud == null? "" : audioFileCloud.getUrl();
    }

    public void setAudioFileCloud(CloudStorageFileDto audioFileCloud) {
        this.audioFileCloud = audioFileCloud;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public Boolean getMatched() {
        return matched;
    }

    public void setMatched(Boolean matched) {
        this.matched = matched;
    }

    public Boolean getLikedBy() {
        return likedBy;
    }

    public void setLikedBy(Boolean likedBy) {
        this.likedBy = likedBy;
    }
}
