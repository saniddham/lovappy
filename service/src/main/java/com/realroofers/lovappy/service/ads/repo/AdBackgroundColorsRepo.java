package com.realroofers.lovappy.service.ads.repo;

import com.realroofers.lovappy.service.ads.model.AdBackgroundColor;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Eias Altawil
 */
public interface AdBackgroundColorsRepo extends JpaRepository<AdBackgroundColor, Integer> {
}
