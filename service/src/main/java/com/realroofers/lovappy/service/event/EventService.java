package com.realroofers.lovappy.service.event;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.core.SocialType;
import com.realroofers.lovappy.service.event.dto.EventDto;
import com.realroofers.lovappy.service.event.dto.EventLocationPictureDto;
import com.realroofers.lovappy.service.event.dto.EventSearchDto;
import com.realroofers.lovappy.service.event.dto.EventUserPictureDto;
import com.realroofers.lovappy.service.event.dto.mobile.AddEventMobileDto;
import com.realroofers.lovappy.service.event.dto.mobile.EventMobileDto;
import com.realroofers.lovappy.service.event.model.Event;
import com.realroofers.lovappy.service.event.support.EventStatus;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Tejaswi Venupalli on 8/29/17
 */

public interface EventService {

    EventDto findEventById(Integer id);

    Page<EventDto> findAllFutureActiveEvents(Pageable pageable);

    Page<EventDto> findAllByEventLocationAndEventStatus(Pageable pageable, String location);

    Page<EventMobileDto> findAllMobileEventByQuery(Pageable pageable, String location, Integer userId);

    Page<EventDto> findEventsByEventDateGreaterThanEqual(Pageable pageable, String eventDate);

    List<EventDto> findAllFutureActiveEvents();

    List<EventDto> getEventsInNextWeek();

    void registerUserForEvent(Integer eventId, Integer userId);

    List<Event> getEventTitlesByCategory(String categoryName);

    EventLocationPictureDto updateEventLocation(Integer eventId, CloudStorageFileDto cloudStorageFileDto);

    EventDto addEvent(EventDto eventDto);

    EventDto updateEvent(EventDto eventDto);

    Integer getNoOfUserAttendees(Integer eventId);

    Integer getNoOfPaidAttendees(Integer eventId);

    boolean deleteLocationImage(Integer eventId);

    //EventLocationPictureDto getEventLocation(Integer id);

    List<Integer> cancelEvent(Integer eventId);

    Page<EventDto> getAll(int evalPage, Integer pageSize);

    Page<EventDto> getAllEvents(Pageable pageable);

    List<EventDto> getNearByEventsByDate(EventSearchDto eventSearchDto);

    List<EventMobileDto> getNearByEvents(EventSearchDto eventSearchDto);

    Page<EventMobileDto> getNearByEventsMobile(EventSearchDto eventSearchDto, Integer userId, Pageable pageable);

    Page<EventDto> getNearByEventsPage(EventSearchDto eventSearchDto, Pageable pageable);

    Integer getNearByEventsCount(EventSearchDto eventSearchDto);

    CalculatePriceDto calculatePrice(Integer eventID, String coupon);

    OrderDto registerUserForEvent(Integer eventId, Integer userId, String couponCode, PaymentMethodType paymentMethodType);

    Page<EventMobileDto> findFutureEventsByUserId(String date, Integer userId, Pageable pageable);

    Page<EventMobileDto> findPastEventsByUserId(String date, Integer userId, Pageable pageable);

    Boolean isRegisteredForEvent(Integer eventId, Integer userId);

    EventDto updateStatus(Integer eventId, EventStatus status);

    List<EventMobileDto> findFutureEventsListByUserId(String date, Integer userId, Pageable pageable);

    List<EventMobileDto> findPastEventsListByUserId(String date, Integer userId);

    Boolean addEventLocationPictures(Integer userId, Integer eventId, List<CloudStorageFileDto> list);

    Page<EventUserPictureDto> getEventPhotos(Integer eventId, Pageable pageable);

    boolean deleteEvent(Integer eventID);

    boolean changeEventApprovalStatus(Integer eventId, ApprovalStatus status);

    Map<Date, List<EventDto>> getPreviousEvents();

    Map<Date, List<EventDto>> getPreviousEvents(Date eventDate);

    void shareBySocialProvider(Integer eventId, SocialType socialType);

    EventMobileDto findMobileEventById(Integer eventId, Integer userId) throws Exception;

    List<EventMobileDto> findMobileAllActiveEvents();

    Page<EventMobileDto> findMobileAllActiveEventsPaginated(Pageable pageable, Integer userId);

    Page<EventMobileDto> findMobileAllActiveEventsByDate(Pageable pageable, Integer userId);

    List<EventMobileDto> findMobileEventsByDate(Date date);

    Page<EventMobileDto> findMobilePreviousEvents(Pageable pageable);

    EventMobileDto addMobileEvent(AddEventMobileDto eventDto) throws Exception;

    Boolean isAmbassador(Integer userId);

}
