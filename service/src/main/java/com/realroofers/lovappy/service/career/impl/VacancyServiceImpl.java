package com.realroofers.lovappy.service.career.impl;

import com.realroofers.lovappy.service.career.VacancyService;
import com.realroofers.lovappy.service.career.dto.VacancyDto;
import com.realroofers.lovappy.service.career.model.Vacancy;
import com.realroofers.lovappy.service.career.repo.VacanyRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VacancyServiceImpl implements VacancyService {


    private VacanyRepo vacanyRepo;

    @Autowired
    public VacancyServiceImpl(VacanyRepo vacanyRepo) {
        this.vacanyRepo = vacanyRepo;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<VacancyDto> findAll(Pageable page) {
        Page<Vacancy> vacancies =vacanyRepo.findAll(page);
        Page<VacancyDto> dtos = vacancies.map(VacancyDto::new);
        return dtos;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<VacancyDto> findByStatus(String status, Pageable page) {

        Page<Vacancy> vacancies = vacanyRepo.findByStatus(status, page);
        Page<VacancyDto> dtos = vacancies.map(VacancyDto::new);
        return dtos;
    }

    @Override
    public Page<VacancyDto> search(String keyWord, Pageable page) {
        Page<Vacancy> vacancies = vacanyRepo.search(keyWord, page);
        Page<VacancyDto> dtos = vacancies.map(VacancyDto::new);
        return dtos;
    }

    @Override
    @Transactional(readOnly = true)
    public VacancyDto findById(long id) {

        Vacancy vacancy = vacanyRepo.findOne(id);
        VacancyDto dto = new VacancyDto(vacancy);
        return dto;
    }

    @Override
    public String insertVacancy(VacancyDto dto) {
        String status;
        try {
            Vacancy vacancy = new Vacancy(dto);
            vacancy.setOpenDate(new Date(new java.util.Date().getTime()));
            vacanyRepo.save(vacancy);
            status = "SuccessFully Saved";

        } catch (Exception e){
            System.out.println("error in inserting");
            System.out.println(e);;
            status = "Error Occured While Saving";
        }
        System.out.println(status);
        return status;
    }

    @Override
    public String deleteVacancy(long id) {
        String status;
        try {
            vacanyRepo.delete(id);
            status = "SuccessFully Deleted";

        } catch (Exception e){
            status = "Error Occured While Deleting";
        }
        return status;
    }

    @Override
    public String saveVacancy(VacancyDto dto) {

        String status;
        try {
            Vacancy vacancy = new Vacancy(dto);
            vacancy.setId(dto.getId());
            vacanyRepo.save(vacancy);
            status = "SuccessFully Saved";

        } catch (Exception e){
            status = "Error Occured While Saving";
        }
        return status;
    }

    @Override
    public String closeVacancy(long vacancyId, String status) {
        String statusCode;
        try {
            Vacancy dto = vacanyRepo.findOne(vacancyId);
            dto.setStatus(status);
            vacanyRepo.save(dto);
            statusCode = "SuccessFully Closed";
        } catch (Exception e){
            statusCode = "Error Occured While Closing the vacancy";
        }
        return statusCode;
    }

    private List<VacancyDto> getDtoFromMOdel(List<Vacancy> vacancies){
        List<VacancyDto> vacanyList = vacancies.stream().map(entry-> new VacancyDto(entry))
                .collect(Collectors.toList());
        return vacanyList;
    }
}
