package com.realroofers.lovappy.service.music.support;

/**
 * Created by Daoud Shaheen on 11/16/2017.
 */
public enum SendStatus {
    SENDING, SEEN
}
