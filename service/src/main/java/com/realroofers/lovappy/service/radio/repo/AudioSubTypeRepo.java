package com.realroofers.lovappy.service.radio.repo;

import com.realroofers.lovappy.service.radio.model.AudioSubType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Manoj on 17/12/2017.
 */
public interface AudioSubTypeRepo extends JpaRepository<AudioSubType,Integer>{

    List<AudioSubType> findAllByActiveIsTrueOrderByName();

    AudioSubType findByName(String name);

}
