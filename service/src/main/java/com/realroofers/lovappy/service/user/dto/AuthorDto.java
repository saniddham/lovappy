package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.util.CommonUtils;
import lombok.EqualsAndHashCode;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 7/7/2017.
 */
@EqualsAndHashCode
public class AuthorDto implements Serializable {
    private Integer id;
    private String email;
    private String firstName;
    private String lastName;
    private String invitedBy;
    private CloudStorageFileDto imageProfile;
    private Date createdOn;
    private Boolean activated;
    private Gender gender;
    private String address;
    private String city;
    private String state;
    private String country;
    private String zipCode;
    private Boolean isTweeted;

    public AuthorDto() {
    }
    public AuthorDto(User user) {
        this.id = user.getUserId();
        this.email = user.getEmail();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.invitedBy = user.getInvitedBy();
        this.createdOn = user.getCreatedOn();
        this.imageProfile = new CloudStorageFileDto(user.getUserProfile().getProfilePhotoCloudFile());
        this.gender = user.getUserProfile().getGender() == null ? Gender.MALE : user.getUserProfile().getGender();
        if(user.getUserProfile().getAddress()!=null) {
            this.city = (user.getUserProfile().getAddress().getCity() == null?"": user.getUserProfile().getAddress().getCity());
            this.country = ( user.getUserProfile().getAddress().getCountry()== null?"": user.getUserProfile().getAddress().getCountry());
            this.state = (user.getUserProfile().getAddress().getState()== null?"": user.getUserProfile().getAddress().getState());

            this.zipCode = user.getUserProfile().getAddress().getZipCode();
        }

        this.isTweeted = user.getTweeted() == null ? false : user.getTweeted();
    }
    public AuthorDto(Integer id, String email, String firstName, String lastName, String invitedBy, Date createdOn, Boolean activated, CloudStorageFileDto imageProfile) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.invitedBy = invitedBy;
        this.createdOn = createdOn;
        this.activated = activated;
        this.imageProfile = imageProfile;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getInvitedBy() {
        return invitedBy;
    }

    public void setInvitedBy(String invitedBy) {
        this.invitedBy = invitedBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public CloudStorageFileDto getImageProfile() {
        return imageProfile;
    }

    public void setImageProfile(CloudStorageFileDto imageProfile) {
        this.imageProfile = imageProfile;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return (!StringUtils.isEmpty(city) ? city + ", ": "" )+
                (!StringUtils.isEmpty(state) ? state +  ", " : "" )+
                (!StringUtils.isEmpty(country) ? CommonUtils.getShortCountryName(country) : "" );

    }

    public Boolean getTweeted() {
        return isTweeted;
    }

    public void setTweeted(Boolean tweeted) {
        isTweeted = tweeted;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
