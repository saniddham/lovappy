package com.realroofers.lovappy.service.ads.model;

import com.realroofers.lovappy.service.user.model.embeddable.Address;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * Created by Eias Altawil on 7/9/2017
 */

@Entity
@Table(name = "ad_target_area")
@EqualsAndHashCode
public class AdTargetArea {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ad")
    private Ad ad;

    @Embedded
    private Address address;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Ad getAd() {
        return ad;
    }

    public void setAd(Ad ad) {
        this.ad = ad;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
