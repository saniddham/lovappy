package com.realroofers.lovappy.service.datingPlaces.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlace;
import com.realroofers.lovappy.service.datingPlaces.model.PlacesAttachment;
import lombok.EqualsAndHashCode;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Darrel Rayen on 10/19/17.
 */
@EqualsAndHashCode
public class PlacesAttachmentDto implements Serializable{

    private Integer attachmentId;
    private DatingPlace placeId;
    private String url;
    private CloudStorageFileDto picture;
    private Date createdDate;
    private String imageDescription;

    public PlacesAttachmentDto() {
    }

    public PlacesAttachmentDto(PlacesAttachment placesAttachment) {
        this.attachmentId = placesAttachment.getAttachmentId();
        this.placeId = placesAttachment.getPlaceId();
        this.url = placesAttachment.getUrl();
        this.picture = new CloudStorageFileDto(placesAttachment.getPicture());
        this.createdDate = placesAttachment.getCreatedDate();
    }

    public PlacesAttachmentDto(Integer attachmentId, String url, CloudStorageFileDto picture, Date createdDate, String imageDescription) {
        this.attachmentId = attachmentId;
        this.url = url;
        this.picture = picture;
        this.createdDate = createdDate;
        this.imageDescription = imageDescription;
    }

    public PlacesAttachment getPlacesAttachment(){
        PlacesAttachment picture = new PlacesAttachment();
        BeanUtils.copyProperties(this,picture);
        return picture;
    }

    public String getPlacesAttachmentFileUrl() {
        return this.picture == null || StringUtils.isEmpty(this.picture.getUrl())
                ? "/images/dating/post.jpg"
                : this.picture.getUrl();
    }

    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }

    public DatingPlace getPlaceId() {
        return placeId;
    }

    public void setPlaceId(DatingPlace placeId) {
        this.placeId = placeId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public CloudStorageFileDto getPicture() {
        return picture;
    }

    public void setPicture(CloudStorageFileDto picture) {
        this.picture = picture;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getImageDescription() {
        return imageDescription;
    }

    public void setImageDescription(String imageDescription) {
        this.imageDescription = imageDescription;
    }
}
