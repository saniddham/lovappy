package com.realroofers.lovappy.service.blog.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.realroofers.lovappy.service.blog.RelayCommentService;
import com.realroofers.lovappy.service.blog.model.RelayComment;
import com.realroofers.lovappy.service.blog.repo.RelayCommentRepo;

@Service
public class RelayCommentServiceImpl implements RelayCommentService {
    @Autowired
    private RelayCommentRepo relayCommentRepo;

    @Transactional
    public void save(RelayComment relaycomment) {
        relayCommentRepo.save(relaycomment);
    }

    @Transactional(readOnly = true)
    public List<RelayComment> getAllRelayComments() {
        return relayCommentRepo.findAll();
    }
}
