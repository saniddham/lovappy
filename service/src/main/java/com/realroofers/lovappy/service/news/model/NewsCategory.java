package com.realroofers.lovappy.service.news.model;

import com.realroofers.lovappy.service.news.dto.NewsCategoryDto;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Shehan Wijesinghe on 28/08/2018.
 */
@Entity
@EqualsAndHashCode
@Table(name = "news_category")
public class NewsCategory implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "cat_id")
    private Long categoryId;

    @Column(name = "cat_name", nullable = false, unique = true)
    private String categoryName;


    @Column(name = "cat_description", nullable = false)
    private String categoryDescription;

    @Column(name = "status")
    private int status=1;

    public NewsCategory() {
    }

    public NewsCategory(NewsCategoryDto categoryDto) {
        this.categoryName = categoryDto.getCategoryName();
        this.categoryDescription = categoryDto.getCategoryDescription();
        this.status = categoryDto.getStatus();
    }

    public NewsCategory(String categoryName, String categoryDescription) {
        this.categoryName = categoryName;
        this.categoryDescription = categoryDescription;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("NewsCategory{");
        sb.append("categoryId=").append(categoryId);
        sb.append(", categoryName='").append(categoryName).append('\'');
        sb.append(", categoryDescription='").append(categoryDescription).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
