package com.realroofers.lovappy.service.mail.support;

/**
 * Created by Daoud Shaheen on 9/9/2017.
 */
public enum EmailTypes {
    VERFICATION,
    RESET_PASSWORD,
    CONFIRMATION_PASSWORD,
    BLOG_SHARE, //TODO DELETE
    EMAIL_SHARE,
    EMAIL_APPROVAL,
    EMAIL_DENIAL,
    EMAIL_INVITATION,
    EMAIL_INTERACTION, // this used for likes/matches emails
    EMAIL_ACCEPT_DENY,
    EMAIL_SUBMITION, // this used for approve/denial for blogs and ads emails
    EMAIL_CANCELLATION,
    DEFAULT_EMAIL,
    GIFT_SENT,
    GIFT_RECEIVED,
    GIFT_SOLD,
    GIFT_REDEEM,
    DEACTIVATION,
}
