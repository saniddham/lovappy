package com.realroofers.lovappy.service.radio.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.radio.model.StartSound;
import com.realroofers.lovappy.service.radio.support.StartSoundType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
public class StartSoundDto {

    private Integer id;
    private CloudStorageFileDto audioFileCloud;
    private StartSoundType soundType;

    public StartSoundDto() {
    }

    public StartSoundDto(StartSound sound) {
        if(sound != null) {
            this.id = sound.getId();
            this.audioFileCloud = new CloudStorageFileDto(sound.getAudioFileCloud());
            this.soundType = sound.getSoundType();
        }
    }

    public StartSoundDto(CloudStorageFileDto audioFileCloud, StartSoundType type) {
        this.audioFileCloud = audioFileCloud;
        this.soundType = type;
    }
}
