package com.realroofers.lovappy.service.cms.dto;

import com.realroofers.lovappy.service.cms.model.PageMetaData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * Created by Daoud Shaheen on 6/10/2017.
 */
@Data
@EqualsAndHashCode
public class PageDTO implements Serializable{
    private Integer id;
    private String tag;
    private String name;
    private Boolean active;

    private Set<PageMetaData> pageMetaList;

    private Set<ImageWidgetDTO> backgroundImages;
    private Set<ImageWidgetDTO> bannersImages;
    private Set<TextWidgetDTO> texts;
    private Set<VideoWidgetDTO> videos;
    private Map<String, String> settings;

    public PageDTO(String tag, String name, Set<PageMetaData> pageMetaList, Set<ImageWidgetDTO> backgroundImages, Set<ImageWidgetDTO> bannersImages, Set<TextWidgetDTO> texts, Set<VideoWidgetDTO> videos) {
        this.tag = tag;
        this.name = name;
        this.pageMetaList = pageMetaList;
        this.backgroundImages = backgroundImages;
        this.bannersImages = bannersImages;
        this.texts = texts;
        this.videos = videos;
    }

    public PageDTO(Integer id, String tag, String name, Boolean active, Set<PageMetaData> pageMetaList, Set<ImageWidgetDTO> backgroundImages, Set<ImageWidgetDTO> bannersImages, Set<TextWidgetDTO> texts, Set<VideoWidgetDTO> videos) {
        this.id = id;
        this.tag = tag;
        this.name = name;
        this.active = active;
        this.pageMetaList = pageMetaList;
        this.backgroundImages = backgroundImages;
        this.bannersImages = bannersImages;
        this.texts = texts;
        this.videos = videos;
    }
}
