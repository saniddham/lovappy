package com.realroofers.lovappy.service.career.model;

import com.realroofers.lovappy.service.career.dto.CareerUserDto;
import com.realroofers.lovappy.service.career.dto.VacancyDto;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@Entity
@Table(name = "career_user")
public class CareerUser {

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
                name = "vacancy_user_map",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = { @JoinColumn(name = "vacancy_id") }
    )
    private Set<Vacancy> vacancies = new HashSet<Vacancy>(0);

    @OneToMany(mappedBy="user")
    private Set<Education> educations = new HashSet<Education>(0);

    @OneToMany(mappedBy="user")
    private Set<Employment> employments = new HashSet<Employment>(0);
    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    @Column(name = "user_id")
    private long id;
    @NotNull(message = "You should provide your valid email address")
    @NotEmpty(message = "You should provide your valid email address")
    @Email(message = "This is Not a Valid email address")
    private String email;
    @Column(name="first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "phone_number")
    private String phoneNumber;
    @Column(name = "us_worker")
    private boolean authorizedInUs;
    @Column(name = "zip_code")
    private int zipCode;
    private int gender;
    private String address1;
    private String address2;
    private int ethnicity;
    @Column(name = "birth_year")
    private int birthYear;
    @Column(name = "birth_month")
    private int birthMonth;
    @Column(name = "birth_date")
    private int birthDate;
    private String description;
    private String country;
    private String city;
    private String state;
    private boolean agree;
    private String hobbies;

    public CareerUser(CareerUserDto user) {
        this.address1 = user.getAddress1();
        this.address2 = user.getAddress2();
        this.agree = user.isAgree();
        this.birthDate = user.getBirthDate();
        this.birthMonth = user.getBirthMonth();
        this.birthYear = user.getBirthYear();
        this.authorizedInUs = user.isAuthorizedInUs();
        this.city = user.getCity();
        this.state = user.getState();
//        this.country = "testing2";
        this.id = user.getId();
        this.zipCode = user.getZipCode();
        this.gender = user.getGender();
        this.ethnicity = user.getEthnicity();
        this.phoneNumber = user.getPhoneNumber();
//        this.description = "testing1";
        this.lastName = user.getLastName();
        this.firstName = user.getFirstName();
        this.email = user.getEmail();
        this.hobbies = user.getHobbies();
        if (user.getEducation().size() > 0) {
            Set<Education> educations =  user.getEducation().stream().map(entry-> new Education(entry, this))
            .collect(Collectors.toSet());
            this.educations = educations;
        }

        if (user.getEmployment().size() > 0) {
            Set<Employment> employments =  user.getEmployment().stream().map(entry-> new Employment(entry, this))
                    .collect(Collectors.toSet());
            this.employments = employments;
        }

    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    public CareerUser() {
        this.agree = false;
        this.authorizedInUs = false;
    }

    public boolean isAgree() {
        return agree;
    }

    public Set<Education> getEducations() {
        return educations;
    }

    public void setEducations(Set<Education> educations) {
        this.educations = educations;
    }

    public Set<Employment> getEmployments() {
        return employments;
    }

    public void setEmployments(Set<Employment> employments) {
        this.employments = employments;
    }

    public void setAgree(boolean agree) {
        this.agree = agree;
    }

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinTable(
            name = "vacancy_user_map",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = { @JoinColumn(name = "vacancy_id") }
    )
    public Set<Vacancy> getVacancies() {
        return vacancies;
    }

    public void setVacancies(Set<Vacancy> vacancies) {
        this.vacancies = vacancies;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isAuthorizedInUs() {
        return authorizedInUs;
    }

    public void setAuthorizedInUs(boolean authorizedInUs) {
        this.authorizedInUs = authorizedInUs;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public int getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(int ethnicity) {
        this.ethnicity = ethnicity;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public int getBirthMonth() {
        return birthMonth;
    }

    public void setBirthMonth(int birthMonth) {
        this.birthMonth = birthMonth;
    }

    public int getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(int birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CareerUser.class.getSimpleName() + "[", "]")
                .add("vacancies=" + vacancies)
                .add("educations=" + educations)
                .add("employments=" + employments)
                .add("id=" + id)
                .add("email='" + email + "'")
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("phoneNumber='" + phoneNumber + "'")
                .add("authorizedInUs=" + authorizedInUs)
                .add("zipCode=" + zipCode)
                .add("gender=" + gender)
                .add("address1='" + address1 + "'")
                .add("address2='" + address2 + "'")
                .add("ethnicity=" + ethnicity)
                .add("birthYear=" + birthYear)
                .add("birthMonth=" + birthMonth)
                .add("birthDate=" + birthDate)
                .add("description='" + description + "'")
                .add("country='" + country + "'")
                .add("city='" + city + "'")
                .add("state='" + state + "'")
                .add("agree=" + agree)
                .add("hobbies='" + hobbies + "'")
                .toString();
    }
}
