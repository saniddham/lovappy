package com.realroofers.lovappy.service.radio.impl;

import com.realroofers.lovappy.service.radio.RadioProgramPlayTimeService;
import com.realroofers.lovappy.service.radio.model.RadioProgramPlayTime;
import com.realroofers.lovappy.service.radio.repo.RadioProgramPlayTimeRepo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Manoj on 17/12/2017.
 */
@Service
public class RadioProgramPlayTimeServiceImpl implements RadioProgramPlayTimeService {

    private final RadioProgramPlayTimeRepo radioProgramPlayTimeRepo;

    public RadioProgramPlayTimeServiceImpl(RadioProgramPlayTimeRepo radioProgramPlayTimeRepo) {
        this.radioProgramPlayTimeRepo = radioProgramPlayTimeRepo;
    }

    @Override
    public List<RadioProgramPlayTime> findAll() {
        return radioProgramPlayTimeRepo.findAll();
    }

    @Override
    public List<RadioProgramPlayTime> findAllActive() {
        return radioProgramPlayTimeRepo.findAllByActiveIsTrue();
    }

    @Override
    public RadioProgramPlayTime create(RadioProgramPlayTime radioProgramPlayTime) {
        return radioProgramPlayTimeRepo.saveAndFlush(radioProgramPlayTime);
    }

    @Override
    public RadioProgramPlayTime update(RadioProgramPlayTime radioProgramPlayTime) {
        return radioProgramPlayTimeRepo.save(radioProgramPlayTime);
    }

    @Override
    public void delete(Integer id) {
        radioProgramPlayTimeRepo.delete(id);
    }

    @Override
    public RadioProgramPlayTime findById(Integer id) {
        return radioProgramPlayTimeRepo.findOne(id);
    }
}
