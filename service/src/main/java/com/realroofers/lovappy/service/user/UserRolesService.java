package com.realroofers.lovappy.service.user;

import com.realroofers.lovappy.service.user.dto.UserRoleResponse;
import com.realroofers.lovappy.service.user.dto.UserRolesDto;
import com.realroofers.lovappy.service.user.model.Role;
import com.realroofers.lovappy.service.user.model.Roles;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Daoud Shaheen on 5/12/2018.
 */
public interface UserRolesService {


    Page<UserRolesDto> getUserRequestsByEmail(String email, Pageable pageable);

    boolean assignRole(Integer userId, Roles roleName) ;
    boolean unAssignRole(Integer userId, Roles roleName) ;
    UserRoleResponse approveRole(Integer userId, Integer roleId);
    UserRoleResponse rejectRole(Integer userId, Integer roleId);
    UserRoleResponse approveRoleAndGeneratePassword(Integer userId, Integer roleId);
}
