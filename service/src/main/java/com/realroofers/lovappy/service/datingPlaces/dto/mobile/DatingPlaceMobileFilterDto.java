package com.realroofers.lovappy.service.datingPlaces.dto.mobile;

import com.realroofers.lovappy.service.datingPlaces.support.Importance;
import com.realroofers.lovappy.service.datingPlaces.support.NeighborHood;
import com.realroofers.lovappy.service.datingPlaces.support.PriceRange;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Darrel Rayen on 11/9/17.
 */
@EqualsAndHashCode
public class DatingPlaceMobileFilterDto implements Serializable {

    private String placeName;
    private Gender gender;
    private String ageRange;
    private PriceRange priceRange;
    private Double averageRating;
    private Boolean isPetsAllowed;
    private Boolean isGoodForCouple;
    private NeighborHood neighborhood;
    private Double latitude;
    private Double longitude;
    private Importance security;
    private Importance parking;
    private Importance transport;
    private Boolean isFiltered;
    private Boolean isDealsActive;
    private List<Integer> personTypes;
    private List<Integer> placeAtmosphere;
    private List<Integer> foodType;
    private Long currentDate;

    public DatingPlaceMobileFilterDto() {
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public Gender getGender() {
        return gender == null ? Gender.NA : gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getAgeRange() {
        return ageRange == null ? "" : ageRange;
    }

    public void setAgeRange(String ageRange) {
        this.ageRange = ageRange;
    }

    public PriceRange getPriceRange() {
        return priceRange == null ? PriceRange.NA : priceRange;
    }

    public void setPriceRange(PriceRange priceRange) {
        this.priceRange = priceRange;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }

    public Boolean getPetsAllowed() {
        return isPetsAllowed;
    }

    public void setPetsAllowed(Boolean petsAllowed) {
        isPetsAllowed = petsAllowed;
    }

    public Boolean getGoodForCouple() {
        return isGoodForCouple;
    }

    public void setGoodForCouple(Boolean goodForCouple) {
        isGoodForCouple = goodForCouple;
    }

    public NeighborHood getNeighborhood() {
        return neighborhood == null ? NeighborHood.NA : neighborhood;
    }

    public void setNeighborhood(NeighborHood neighborhood) {
        this.neighborhood = neighborhood;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Importance getSecurity() {
        return security == null ? Importance.NA : security;
    }

    public void setSecurity(Importance security) {
        this.security = security;
    }

    public Importance getParking() {
        return parking == null ? Importance.NA : parking;
    }

    public void setParking(Importance parking) {
        this.parking = parking;
    }

    public Importance getTransport() {
        return transport == null ? Importance.NA : transport;
    }

    public void setTransport(Importance transport) {
        this.transport = transport;
    }

    public Boolean getFiltered() {
        return isFiltered;
    }

    public void setFiltered(Boolean filtered) {
        isFiltered = filtered;
    }

    public Boolean getDealsActive() {
        return isDealsActive;
    }

    public void setDealsActive(Boolean dealsActive) {
        isDealsActive = dealsActive;
    }

    public List<Integer> getPersonTypes() {
        return personTypes;
    }

    public void setPersonTypes(List<Integer> personTypes) {
        this.personTypes = personTypes;
    }

    public List<Integer> getPlaceAtmosphere() {
        return placeAtmosphere;
    }

    public void setPlaceAtmosphere(List<Integer> placeAtmosphere) {
        this.placeAtmosphere = placeAtmosphere;
    }

    public List<Integer> getFoodType() {
        return foodType;
    }

    public void setFoodType(List<Integer> foodType) {
        this.foodType = foodType;
    }

    public Long getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Long currentDate) {
        this.currentDate = currentDate;
    }

    @Override
    public String toString() {
        return "DatingPlaceMobileFilterDto{" +
                "placeName='" + placeName + '\'' +
                ", gender=" + gender +
                ", ageRange='" + ageRange + '\'' +
                ", priceRange=" + priceRange +
                ", averageRating=" + averageRating +
                ", isPetsAllowed=" + isPetsAllowed +
                ", isGoodForCouple=" + isGoodForCouple +
                ", neighborhood=" + neighborhood +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", security=" + security +
                ", parking=" + parking +
                ", transport=" + transport +
                ", isFiltered=" + isFiltered +
                ", isDealsActive=" + isDealsActive +
                ", personTypes=" + personTypes +
                ", placeAtmosphere=" + placeAtmosphere +
                ", foodType=" + foodType +
                ", currentDate=" + currentDate +
                '}';
    }
}
