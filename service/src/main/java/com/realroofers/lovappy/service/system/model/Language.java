package com.realroofers.lovappy.service.system.model;

import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * Created by Eias Altawil on 6/3/2017
 */
@Entity
@Table(name="language",
        uniqueConstraints=
        @UniqueConstraint(columnNames={"full_name", "abbreviation"}))
@EqualsAndHashCode
public class Language {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "full_name")
    private String name;

    private String abbreviation;

    @Column(name = "enabled", nullable = false, columnDefinition = "bit default 1")
    private Boolean enabled = true;

    public Language() {
    }

    public Language(Integer id) {
        this.id = id;
    }

    public Language(String name, String abbreviation) {
        this.name = name;
        this.abbreviation = abbreviation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
