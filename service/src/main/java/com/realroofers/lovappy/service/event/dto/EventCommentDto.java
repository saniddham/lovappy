package com.realroofers.lovappy.service.event.dto;


import com.realroofers.lovappy.service.event.model.Event;
import com.realroofers.lovappy.service.event.model.EventComment;
import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * Created by Tejaswi Venupalli on 8/29/17
 */
@EqualsAndHashCode
public class EventCommentDto {

    private Integer id;
    private Event eventId;
    private User userId;
    private String commentDescription;
    private Date commentPostedDate;

    public EventCommentDto(){

    }
    public EventCommentDto(EventComment comment){
        this.eventId = comment.getEventId();
        this.userId = comment.getUserId();
        this.commentDescription = comment.getCommentDescription();
        this.commentPostedDate = comment.getCommentPostedDate();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Event getEventId() {
        return eventId;
    }

    public void setEventId(Event eventId) {
        this.eventId = eventId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public String getCommentDescription() {
        return commentDescription;
    }

    public void setCommentDescription(String commentDescription) {
        this.commentDescription = commentDescription;
    }

    public Date getCommentPostedDate() {
        return commentPostedDate;
    }

    public void setCommentPostedDate(Date commentPostedDate) {
        this.commentPostedDate = commentPostedDate;
    }
}
