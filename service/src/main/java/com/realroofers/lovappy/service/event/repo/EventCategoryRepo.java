package com.realroofers.lovappy.service.event.repo;

import com.realroofers.lovappy.service.event.model.EventCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by darrel on 8/31/17.
 */
@Repository
public interface EventCategoryRepo extends JpaRepository<EventCategory,Integer>,QueryDslPredicateExecutor<EventCategory> {

    EventCategory findByCategoryName(String categoryName);

}
