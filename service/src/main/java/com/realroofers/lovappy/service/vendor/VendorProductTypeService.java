package com.realroofers.lovappy.service.vendor;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.vendor.dto.VendorProductTypeDto;

public interface VendorProductTypeService extends AbstractService<VendorProductTypeDto, Integer> {
}
