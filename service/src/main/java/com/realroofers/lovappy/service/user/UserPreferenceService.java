package com.realroofers.lovappy.service.user;

import com.realroofers.lovappy.service.user.dto.UserPreferenceDto;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.PrefHeight;
import com.realroofers.lovappy.service.user.support.Size;

public interface UserPreferenceService {

	UserPreferenceDto findOne(Integer usedId);

	UserPreferenceDto update(Integer userID, Boolean penisSizeMatter, Size prefBustSize, Size prefButtSize);

	UserPreferenceDto update(Integer userID, Integer minAge, Integer maxAge, PrefHeight height, Gender gender);

}
