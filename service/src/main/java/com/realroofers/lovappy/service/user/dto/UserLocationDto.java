package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.user.support.Gender;
import lombok.EqualsAndHashCode;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
@EqualsAndHashCode
public class UserLocationDto {
    private Gender gender;
    private String fullAddress;
    private Double latitude;
    private Double longitude;

    public UserLocationDto(Gender gender, String fullAddress, Double latitude, Double longitude) {
        this.gender = gender;
        this.fullAddress = fullAddress;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Gender getGender() {
        return gender;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserLocationDto that = (UserLocationDto) o;

        if (gender != that.gender) return false;
        if (fullAddress != null ? !fullAddress.equals(that.fullAddress) : that.fullAddress != null) return false;
        if (latitude != null ? !latitude.equals(that.latitude) : that.latitude != null) return false;
        return longitude != null ? longitude.equals(that.longitude) : that.longitude == null;
    }

    @Override
    public int hashCode() {
        int result = gender != null ? gender.hashCode() : 0;
        result = 31 * result + (fullAddress != null ? fullAddress.hashCode() : 0);
        result = 31 * result + (latitude != null ? latitude.hashCode() : 0);
        result = 31 * result + (longitude != null ? longitude.hashCode() : 0);
        return result;
    }
}
