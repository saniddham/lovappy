package com.realroofers.lovappy.service.datingPlaces.dto.mobile;

import com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceDeals;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Darrel Rayen on 1/22/18.
 */
@Data
@EqualsAndHashCode
public class DatingPlaceDealsMobileDto implements Serializable {

    private Integer placeDealId;
    private Integer placeId;
    private Integer dealId;
    private Double fullAmount;
    private Double discountValue;
    private String dealDescription;
    private Date startDate;
    private Date expiryDate;
    private ApprovalStatus approvalStatus;
    private Double dealPrice;
    private Boolean isActive;
    private Boolean approved;

    public DatingPlaceDealsMobileDto() {
    }

    public DatingPlaceDealsMobileDto(DatingPlaceDeals placeDeals) {
        this.placeDealId = placeDeals.getPlaceDealId();
        this.placeId = placeDeals.getDatingPlace().getDatingPlacesId();
        this.dealId = placeDeals.getDeals().getDealId();
        this.fullAmount = placeDeals.getFullAmount();
        this.discountValue = placeDeals.getDiscountValue();
        this.dealDescription = placeDeals.getDealDescription();
        this.startDate = placeDeals.getStartDate();
        this.expiryDate = placeDeals.getExpiryDate();
        this.approvalStatus = placeDeals.getApprovalStatus();
        this.dealPrice = placeDeals.getDealPrice();
        this.isActive = placeDeals.getActive() == null ? false : placeDeals.getActive();
        this.approved = placeDeals.getApproved();
    }
}
