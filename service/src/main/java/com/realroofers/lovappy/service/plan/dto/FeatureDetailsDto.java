package com.realroofers.lovappy.service.plan.dto;

import com.realroofers.lovappy.service.plan.model.Feature;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * Created by hasan on 9/11/2017.
 */
@EqualsAndHashCode
public class FeatureDetailsDto {
    private Integer featureID;
    @NotNull
    private Integer planId;
    @NotNull
    private String featureDetails;
    private String featureNote;
    private String noteColor;

    private Boolean isValid;

    public FeatureDetailsDto() {
    }

    public FeatureDetailsDto(Integer featureId, Integer planId, String featureDetails, String featureNote, String noteColor, Boolean isValid) {
        this.featureID = featureId;
        this.planId = planId;
        this.featureDetails = featureDetails;
        this.featureNote = featureNote;
        this.noteColor = noteColor;
        this.isValid = isValid;
    }

    public FeatureDetailsDto(Feature feature) {
        this.featureID = feature.getFeatureID();
        this.planId = feature.getPlan().getPlanID();
        this.featureDetails = feature.getFeatureDetails();
        this.featureNote = feature.getFeatureNote();
        this.noteColor = feature.getNoteColor();
        this.isValid = feature.getIsValid();
    }


    public Integer getFeatureID() {
        return featureID;
    }

    public void setFeatureID(Integer featureId) {
        this.featureID = featureId;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public String getFeatureDetails() {
        return featureDetails;
    }

    public void setFeatureDetails(String featureDetails) {
        this.featureDetails = featureDetails;
    }

    public String getFeatureNote() {
        return featureNote;
    }

    public void setFeatureNote(String featureNote) {
        this.featureNote = featureNote;
    }

    public String getNoteColor() {
        return noteColor;
    }

    public void setNoteColor(String noteColor) {
        this.noteColor = noteColor;
    }

    public Boolean getIsValid() {
        return isValid;
    }

    public void setIsValid(Boolean valid) {
        isValid = valid;
    }

}
