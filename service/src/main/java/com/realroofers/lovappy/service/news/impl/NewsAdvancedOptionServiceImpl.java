package com.realroofers.lovappy.service.news.impl;

import com.realroofers.lovappy.service.news.NewsAdvancedOptionService;
import com.realroofers.lovappy.service.news.model.NewsAdvancedOption;
import com.realroofers.lovappy.service.news.repo.NewsAdvancedOptionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NewsAdvancedOptionServiceImpl implements NewsAdvancedOptionService {

    @Autowired
    private NewsAdvancedOptionRepo newsAdvancedOptionRepo;

    @Transactional
    @Override
    public NewsAdvancedOption save(NewsAdvancedOption newsAdvancedOption) {
        return newsAdvancedOptionRepo.save(newsAdvancedOption);
    }

    @Transactional
    @Override
    public NewsAdvancedOption getOneBystoryId(Long id) {
        return newsAdvancedOptionRepo.findNewsAdvancedOptionByNewsStory_Id(id);
    }
}
