package com.realroofers.lovappy.service.user.repo;

import java.util.List;
import java.util.Set;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.Gender;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.realroofers.lovappy.service.user.model.UserProfile;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@Repository
public interface UserProfileRepo extends JpaRepository<UserProfile, Integer>, QueryDslPredicateExecutor<UserProfile> {
    /**
     * Get all user profiles within the radius given the approximate linear computation. <br />
     * SQRT( SQR(ABS(lat1 - lat2) + SQR(ABS(lng1 - lng2) ) * 70
     * <br/> Where 70 is the approximate earth radius in miles that would give result close to the result of
     * <a href="http://www.movable-type.co.uk/scripts/latlong.html">haversine</a> formula.
     *
     * @param centerLatitude  center latitude
     * @param centerLongitude center longitude
     * @param radiusInMiles   radius in miles
     * @return list of user profiles within the given radius
     */
    @Query(nativeQuery = true, value = "SELECT\n" +
            "  u.*\n" +
            "FROM user_profile u\n" +
            "WHERE  u.gender=:gender AND (\n" +
            "    3959 * acos (\n" +
            "      cos ( radians(:lat) )\n" +
            "      * cos( radians( u.latitude ) )\n" +
            "      * cos( radians( u.longitude ) - radians(:lng) )\n" +
            "      + sin ( radians(:lat) )\n" +
            "      * sin( radians( u.latitude ) )\n" +
            "    )\n" +
            "  ) < :radius\n" +
            "LIMIT 0 , 20;")
    List<UserProfile> getNearbyUsers(@Param("lat") double centerLatitude, @Param("lng") double centerLongitude,@Param("radius") double radiusInMiles,@Param("gender") String gender);

    @Query(nativeQuery = true, value = "SELECT\n" +
            "  count(u.user_id)\n" +
            "FROM user_profile u\n" +
            "WHERE  u.user_id=:userId AND u.gender=:gender And (\n" +
            "    3959 * acos (\n" +
            "      cos ( radians(:lat) )\n" +
            "      * cos( radians( u.latitude ) )\n" +
            "      * cos( radians( u.longitude ) - radians(:lng) )\n" +
            "      + sin ( radians(:lat) )\n" +
            "      * sin( radians( u.latitude ) )\n" +
            "    )\n" +
            "  ) < :radius")
    Long countNearbyUsers(@Param("lat") double centerLatitude, @Param("lng") double centerLongitude,@Param("radius") double radiusInMiles, @Param("userId") int excludeUserId, @Param("gender") String gender);

    @Query(nativeQuery = true, value = "SELECT\n" +
            "  u.*\n" +
            "FROM user_profile u\n" +
            "WHERE   (\n" +
            "    3959 * acos (\n" +
            "      cos ( radians(:lat) )\n" +
            "      * cos( radians( u.latitude ) )\n" +
            "      * cos( radians( u.longitude ) - radians(:lng) )\n" +
            "      + sin ( radians(:lat) )\n" +
            "      * sin( radians( u.latitude ) )\n" +
            "    )\n" +
            "  ) < :radius\n" +
            " LIMIT 0 , 20;")
    List<UserProfile> getNearbyUsers(@Param("lat") double centerLatitude, @Param("lng") double centerLongitude,@Param("radius") double radiusInMiles);

    @Query(nativeQuery = true, value = "SELECT\n" +
            "  count(u.user_id)\n" +
            "FROM user_profile u\n" +
            "WHERE  u.user_id=:userId And (\n" +
            "    3959 * acos (\n" +
            "      cos ( radians(:lat) )\n" +
            "      * cos( radians( u.latitude ) )\n" +
            "      * cos( radians( u.longitude ) - radians(:lng) )\n" +
            "      + sin ( radians(:lat) )\n" +
            "      * sin( radians( u.latitude ) )\n" +
            "    )\n" +
            "  ) < :radius;")
    Long countNearbyUsers(@Param("lat") double centerLatitude, @Param("lng") double centerLongitude,@Param("radius") double radiusInMiles, @Param("userId") int excludeUserId);

    @Query("select up from UserProfile up Join up.blockedUsers u where up.userId=:currentUser AND u.userId=:checkedUser")
    UserProfile checkUserBlockStatus(@Param("currentUser") Integer userId, @Param("checkedUser") Integer checkedUserId);

    Integer countByGender(Gender gender);

    UserProfile findByUserId(Integer userId);

    @Query("SELECT up.profilePhotoCloudFile from UserProfile up JOIN up.allowedProfileUsers u where up.userId=:userId AND u.userId=:checkedUser")
    CloudStorageFile findUserPicture(@Param("userId") Integer userId, @Param("checkedUser") Integer checkedUserId);

    @Query("Select u.userProfile FROM UserProfile up JOIN up.allowedProfileUsers u Where up.userId=:userId AND up.isAllowedProfilePic=true")
    Page<UserProfile> findAllRevokedUsersProfileAccess(@Param("userId") Integer userId, Pageable pageable);

    @Query("Select u.userProfile FROM UserProfile up JOIN up.allowedProfileUsers u Where up.userId=:userId AND up.isAllowedProfilePic=false")
    Page<UserProfile> findCustomUsersProfileAccess(@Param("userId") Integer userId, Pageable pageable);

//    @Query("Select Count(u.userProfile) " +
//            "FROM UserProfile up JOIN up.allowedProfileUsers u " +
//            " LEFT JOIN ProfileTransaction AS pt ON u.userId = me.id.userTo.userId " +
//            "Where (up.userId=:userId AND up.isAllowedProfilePic=false) or pt.transactionCount<")
//
//    @Query(value = "SELECT m" +
//            " FROM Music AS m" +
//            " LEFT JOIN MusicExchange AS me ON m.id = me.music.id" +
//            " WHERE m.status='APPROVED'"+
//            " GROUP BY m.id" +
//            " ORDER BY COUNT(me.id) DESC")
    @Query("Select Count(u.userProfile) FROM UserProfile up JOIN up.allowedProfileUsers u Where up.userId=:userId AND up.isAllowedProfilePic=false")
    Long countCustomUsersProfileAccess(@Param("userId") Integer userId);

    @Query(nativeQuery = true, value = "SELECT\n" +
            "  user_id\n" +
            "FROM user_profile u\n" +
            "WHERE  (\n" +
            "    3959 * acos (\n" +
            "      cos ( radians(:lat) )\n" +
            "      * cos( radians( u.latitude ) )\n" +
            "      * cos( radians( u.longitude ) - radians(:lng) )\n" +
            "      + sin ( radians(:lat) )\n" +
            "      * sin( radians( u.latitude ) )\n" +
            "    )\n" +
            "  ) < :radius\n" +
            "LIMIT 0 , 100;")
    List<Integer> getUserIdsNearBy(@Param("lat") double lat, @Param("lng") double lng, @Param("radius") Integer radiusInMile);

    @Query("SELECT u.userId From User u JOIN u.userProfile.blockedUsers ub Where u.userId=:userId AND ub.userId IN :userIds")
    Set<Integer> findBlockedUserIdsByUserId(@Param("userId") int userId, @Param("userIds") List<Integer> users);
}
