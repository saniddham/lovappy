package com.realroofers.lovappy.service.vendor.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.product.support.Upload;
import com.realroofers.lovappy.service.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Manoj on 04/02/2018.
 */
@Entity
@Data
@EqualsAndHashCode
@Table(name = "vendor_location")
public class VendorLocation extends BaseEntity<Integer> implements Serializable {
    
    @NotBlank(message = "Company name cannot be empty.", groups = {Upload.class})
    @Column(name = "company_name")
    private String companyName;

    @NotBlank(message = "Store id cannot be empty.", groups = {Upload.class})
    @Column(name = "store_id")
    private String storeId;

    @NotBlank(message = "Manager name cannot be empty.", groups = {Upload.class})
    @Column(name = "manager")
    private String storeManager;

    @NotBlank(message = "Manager contact details cannot be empty.", groups = {Upload.class})
    @Column(name = "manager_contact")
    private String storeManagerContact;

    @Column(name = "assistant_manager")
    private String assistantManager;

    @Column(name = "assistant_manager_contact")
    private String assistantManagerContact;

    @NotBlank(message = "Address line one cannot be empty.", groups = {Upload.class})
    @Column(name = "address_line_1")
    private String addressLine1;

    @Column(name = "address_line_2")
    private String addressLine2;

    @NotBlank(message = "City cannot be empty.", groups = {Upload.class})
    @Column(name = "city")
    private String city;

    @NotBlank(message = "State cannot be empty.", groups = {Upload.class})
    @Column(name = "state")
    private String state;

    @NotBlank(message = "Zip code cannot be empty.", groups = {Upload.class})
    @Column(name = "zip_code")
    private String zipCode;

    @NotBlank(message = "Country cannot be empty.", groups = {Upload.class})
    @Column(name = "country")
    private String country;

    @NotBlank(message = "Location contact cannot be empty.", groups = {Upload.class})
    @Column(name = "location_contact")
    private String locationContact;

    @NotBlank(message = "Open time cannot be empty.", groups = {Upload.class})
    @Column(name = "open_from")
    private String openFrom;

    @NotBlank(message = "Open time cannot be empty.", groups = {Upload.class})
    @Column(name = "open_to")
    private String openTo;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;

    private Double latitude;
    private Double longitude;

    @JsonIgnore
    @JoinColumn(name = "created_by")
    @ManyToOne
    private User createdBy;
}
