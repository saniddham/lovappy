package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.SubscriptionType;
import lombok.EqualsAndHashCode;
import org.springframework.util.StringUtils;

import java.util.*;


/**
 * @author mwiyono
 */
@EqualsAndHashCode
public class UserProfileDto {
    private Integer userId;

    private Gender gender;

    private Date birthDate;

    private Integer age;

    private Integer heightFt;

    private Integer heightIn;

    private AddressDto address;

    private Collection<LanguageDto> speakingLanguages;

    private String speakingLanguagesAbb;

    private String language;

    private String selectedLanguages;

    private SubscriptionType subscriptionType;

    private CloudStorageFileDto profilePhotoCloudFile;
    private CloudStorageFileDto photoIdentificationCloudFile;

    private CloudStorageFileDto handsCloudFile;
    private CloudStorageFileDto feetCloudFile;
    private CloudStorageFileDto legsCloudFile;
    private Boolean handsFileSkipped;
    private Boolean feetFileSkipped;
    private Boolean legsFileSkipped;

    private Integer listenedToCount;
    private Integer listenedToFemaleCount;
    private Integer listenedToMaleCount;
    private Double avgDistanceOfViewers;

    private Map<String, String> personalities;
    private Map<String, String> lifestyles;
    private Map<String, String> statuses;
    private Boolean isAllowedProfilePic;
    private Integer maxTransactions;

    private String skills;
    public UserProfileDto() {

    }

    public UserProfileDto(Integer userId) {
        this.userId = userId;
    }

    public UserProfileDto(UserProfile model) {
        if (model != null) {
            this.userId = model.getUserId();
            this.gender = model.getGender();
            this.birthDate = model.getBirthDate();
            this.age = getAge();
            this.heightFt = model.getHeightFt();
            this.heightIn = model.getHeightIn();
            this.address = AddressDto.toAddressDto(model.getAddress());
            if(this.address == null) {
                this.address = new AddressDto();
            }
            this.listenedToMaleCount = model.getListenedToMaleCount() == null ? 0 : model.getListenedToMaleCount();
            this.listenedToFemaleCount = model.getListenedToFemaleCount() == null ? 0 : model.getListenedToFemaleCount();
            this.avgDistanceOfViewers = model.getAvgDistanceOfViewers() == null ? 0.0 : model.getAvgDistanceOfViewers();
            this.listenedToCount = model.getListenedToCount() == null ? 0 : model.getListenedToCount();
            this.maxTransactions = model.getMaxTransactions();
            this.language = "";
            this.speakingLanguages = new ArrayList<>();
            if(model.getSpeakingLanguage() != null) {
                for (Language language : model.getSpeakingLanguage()) {
                    this.speakingLanguages.add(new LanguageDto(language));
                    this.language = this.language.concat(language.getAbbreviation() + ",");
                }
            }
            if (this.language.length() > 1)
                this.language = this.language.substring(0, this.language.length() - 1);

            this.subscriptionType = model.getSubscriptionType();

            this.profilePhotoCloudFile = new CloudStorageFileDto(model.getProfilePhotoCloudFile());
            this.photoIdentificationCloudFile = new CloudStorageFileDto(model.getPhotoIdentificationCloudFile());

            this.handsCloudFile = new CloudStorageFileDto(model.getHandsCloudFile());
            this.feetCloudFile = new CloudStorageFileDto(model.getFeetCloudFile());
            this.legsCloudFile = new CloudStorageFileDto(model.getLegsCloudFile());
            this.handsFileSkipped = model.getHandsFileSkipped();
            this.feetFileSkipped = model.getFeetFileSkipped();
            this.legsFileSkipped = model.getLegsFileSkipped();

            this.personalities = model.getPersonalities();
            this.lifestyles = model.getLifestyles();
            this.statuses = model.getStatuses();
            this.isAllowedProfilePic = model.getAllowedProfilePic();
            this.skills = model.getSkills();
        }
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public Integer getMaxTransactions() {
        return maxTransactions;
    }

    public void setMaxTransactions(Integer maxTransactions) {
        this.maxTransactions = maxTransactions;
    }

    public Boolean getAllowedProfilePic() {
        return isAllowedProfilePic;
    }

    public void setAllowedProfilePic(Boolean allowedProfilePic) {
        isAllowedProfilePic = allowedProfilePic;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setSpeakingLanguagesAbb(String speakingLanguagesAbb) {
        this.speakingLanguagesAbb = speakingLanguagesAbb;
    }

    public Integer getListenedToCount() {
        return listenedToCount;
    }

    public void setListenedToCount(Integer listenedToCount) {
        this.listenedToCount = listenedToCount;
    }

    public Integer getListenedToFemaleCount() {
        return listenedToFemaleCount;
    }

    public void setListenedToFemaleCount(Integer listenedToFemaleCount) {
        this.listenedToFemaleCount = listenedToFemaleCount;
    }

    public Integer getListenedToMaleCount() {
        return listenedToMaleCount;
    }

    public Double getAvgDistanceOfViewers() {
        return avgDistanceOfViewers;
    }

    public void setAvgDistanceOfViewers(Double avgDistanceOfViewers) {
        this.avgDistanceOfViewers = avgDistanceOfViewers;
    }

    public void setListenedToMaleCount(Integer listenedToMaleCount) {
        this.listenedToMaleCount = listenedToMaleCount;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }


    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getHeightFt() {
        return heightFt;
    }

    public void setHeightFt(Integer heightFt) {
        this.heightFt = heightFt;
    }

    public Integer getHeightIn() {
        return heightIn;
    }

    public void setHeightIn(Integer heightIn) {
        this.heightIn = heightIn;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public Collection<LanguageDto> getSpeakingLanguages() {
        return speakingLanguages;
    }

    public void setSpeakingLanguages(Collection<LanguageDto> speakingLanguages) {
        this.speakingLanguages = speakingLanguages;
    }

    public String getSpeakingLanguagesAbb() {
        String sl = "";
        if (this.speakingLanguages != null) {
            for (LanguageDto languageDto : this.speakingLanguages) {
                sl = sl.concat(languageDto.getAbbreviation());
                break;
            }
        }

        return sl.toString();
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public SubscriptionType getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(SubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public String getProfilePhotoCloudFileUrl() {
        return this.profilePhotoCloudFile == null || StringUtils.isEmpty(this.profilePhotoCloudFile.getUrl())
                ? "/images/photo-error.png"
                : this.profilePhotoCloudFile.getUrl();
    }

    public CloudStorageFileDto getProfilePhotoCloudFile() {
        return profilePhotoCloudFile;
    }

    public void setProfilePhotoCloudFile(CloudStorageFileDto profilePhotoCloudFile) {
        this.profilePhotoCloudFile = profilePhotoCloudFile;
    }

    public String getPhotoIdentificationCloudFileUrl() {
        return this.photoIdentificationCloudFile == null || StringUtils.isEmpty(this.photoIdentificationCloudFile.getUrl())
                ? "/images/photo-error.png"
                : this.photoIdentificationCloudFile.getUrl();
    }

    public CloudStorageFileDto getPhotoIdentificationCloudFile() {
        return photoIdentificationCloudFile;
    }

    public void setPhotoIdentificationCloudFile(CloudStorageFileDto photoIdentificationCloudFile) {
        this.photoIdentificationCloudFile = photoIdentificationCloudFile;
    }

    public String getHandsCloudFileUrl() {
        if (handsFileSkipped != null && handsFileSkipped)
            return "/images/skipped_hand_photo.png";

        return this.handsCloudFile == null || StringUtils.isEmpty(this.handsCloudFile.getUrl())
                ? "/images/photo-error.png"
                : this.handsCloudFile.getUrl();
    }

    public CloudStorageFileDto getHandsCloudFile() {
        return handsCloudFile;
    }

    public void setHandsCloudFile(CloudStorageFileDto handsCloudFile) {
        this.handsCloudFile = handsCloudFile;
    }

    public String getFeetCloudFileUrl() {
        if (feetFileSkipped != null && feetFileSkipped)
            return "/images/skipped_legs_photo.png";

        return this.feetCloudFile == null || StringUtils.isEmpty(this.feetCloudFile.getUrl())
                ? "/images/photo-error.png"
                : this.feetCloudFile.getUrl();
    }

    public CloudStorageFileDto getFeetCloudFile() {
        return feetCloudFile;
    }

    public void setFeetCloudFile(CloudStorageFileDto feetCloudFile) {
        this.feetCloudFile = feetCloudFile;
    }

    public String getLegsCloudFileUrl() {
        if (legsFileSkipped != null && legsFileSkipped)
            return "/images/skipped_legs_photo.png";

        return this.legsCloudFile == null || StringUtils.isEmpty(this.legsCloudFile.getUrl())
                ? "/images/photo-error.png"
                : this.legsCloudFile.getUrl();
    }

    public CloudStorageFileDto getLegsCloudFile() {
        return legsCloudFile;
    }

    public void setLegsCloudFile(CloudStorageFileDto legsCloudFile) {
        this.legsCloudFile = legsCloudFile;
    }

    public Boolean getHandsFileSkipped() {
        return handsFileSkipped;
    }

    public void setHandsFileSkipped(Boolean handsFileSkipped) {
        this.handsFileSkipped = handsFileSkipped;
    }

    public Boolean getFeetFileSkipped() {
        return feetFileSkipped;
    }

    public void setFeetFileSkipped(Boolean feetFileSkipped) {
        this.feetFileSkipped = feetFileSkipped;
    }

    public Boolean getLegsFileSkipped() {
        return legsFileSkipped;
    }

    public void setLegsFileSkipped(Boolean legsFileSkipped) {
        this.legsFileSkipped = legsFileSkipped;
    }

    public Map<String, String> getPersonalities() {
        return personalities;
    }

    public void setPersonalities(Map<String, String> personalities) {
        this.personalities = personalities;
    }

    public Map<String, String> getLifestyles() {
        return lifestyles;
    }

    public void setLifestyles(Map<String, String> lifestyles) {
        this.lifestyles = lifestyles;
    }

    public Map<String, String> getStatuses() {
        return statuses;
    }

    public void setStatuses(Map<String, String> statuses) {
        this.statuses = statuses;
    }

    public Date getLastReviewed(CloudStorageFile audioFile) {
        if (audioFile == null) return null;
        if ((getProfilePhotoCloudFile() == null) && getHandsCloudFile() == null && getFeetCloudFile() == null && getLegsCloudFile() == null)
            return null;
        SortedSet<Date> dates = new TreeSet<Date>();
        if (getProfilePhotoCloudFile() != null && getProfilePhotoCloudFile().getApprovedDate() != null)
            dates.add(getProfilePhotoCloudFile().getApprovedDate());
        if (getProfilePhotoCloudFile() != null && getProfilePhotoCloudFile().getRejectedDate() != null)
            dates.add(getProfilePhotoCloudFile().getRejectedDate());
        if (getHandsCloudFile() != null && getHandsCloudFile().getApprovedDate() != null)
            dates.add(getHandsCloudFile().getApprovedDate());
        if (getHandsCloudFile() != null && getHandsCloudFile().getRejectedDate() != null)
            dates.add(getHandsCloudFile().getRejectedDate());
        if (getLegsCloudFile() != null && getLegsCloudFile().getApprovedDate() != null)
            dates.add(getLegsCloudFile().getApprovedDate());
        if (getLegsCloudFile() != null && getLegsCloudFile().getRejectedDate() != null)
            dates.add(getLegsCloudFile().getRejectedDate());
        if (getFeetCloudFile() != null && getFeetCloudFile().getApprovedDate() != null)
            dates.add(getFeetCloudFile().getApprovedDate());
        if (getFeetCloudFile() != null && getFeetCloudFile().getRejectedDate() != null)
            dates.add(getFeetCloudFile().getRejectedDate());
        if (audioFile != null && audioFile.getApprovedDate() != null) dates.add(audioFile.getApprovedDate());
        if (audioFile != null && audioFile.getRejected() != null) dates.add(audioFile.getRejectedDate());
        return dates.size() > 0 ? dates.last() : null;
    }

    public Boolean profilePhotoCloudFileExists() {
        return !(this.profilePhotoCloudFile == null || StringUtils.isEmpty(this.profilePhotoCloudFile.getUrl()));
    }

    public Boolean feetCloudFileExists() {
        if (feetFileSkipped != null && feetFileSkipped)
            return false;
        return !(this.feetCloudFile == null || StringUtils.isEmpty(this.feetCloudFile.getUrl()));
    }

    public Boolean handsCloudFileExists() {
        if (handsFileSkipped != null && handsFileSkipped)
            return false;

        return !(this.handsCloudFile == null || StringUtils.isEmpty(this.handsCloudFile.getUrl()));
    }

    public Boolean legsCloudFileExists() {
        if (legsFileSkipped != null && legsFileSkipped)
            return false;

        return !(this.legsCloudFile == null || StringUtils.isEmpty(this.legsCloudFile.getUrl()));
    }

    public Integer getAge() {
        if (this.birthDate == null) {
            return null;
        }
        Calendar today = Calendar.getInstance();
        Calendar birthDate = Calendar.getInstance();
        birthDate.setTime(this.birthDate);
        if (birthDate.after(today)) {
            return 0;
            //throw new IllegalArgumentException("You don't exist yet");
        }
        int todayYear = today.get(Calendar.YEAR);
        int birthDateYear = birthDate.get(Calendar.YEAR);
        int todayDayOfYear = today.get(Calendar.DAY_OF_YEAR);
        int birthDateDayOfYear = birthDate.get(Calendar.DAY_OF_YEAR);
        int todayMonth = today.get(Calendar.MONTH);
        int birthDateMonth = birthDate.get(Calendar.MONTH);
        int todayDayOfMonth = today.get(Calendar.DAY_OF_MONTH);
        int birthDateDayOfMonth = birthDate.get(Calendar.DAY_OF_MONTH);
        int age = todayYear - birthDateYear;

        // If birth date is greater than todays date (after 2 days adjustment of leap year) then decrement age one year
        if ((birthDateDayOfYear - todayDayOfYear > 3) || (birthDateMonth > todayMonth)) {
            age--;

            // If birth date and todays date are of same month and birth day of month is greater than todays day of month then decrement age
        } else if ((birthDateMonth == todayMonth) && (birthDateDayOfMonth > todayDayOfMonth)) {
            age--;
        }
        return age;
    }

    public String getSelectedLanguages() {
        return selectedLanguages;
    }

    public void setSelectedLanguages(String selectedLanguages) {
        this.selectedLanguages = selectedLanguages;
    }
    //    public int getAge() {
//        long ageInMillis = new Date().getTime() - getBirthDate().getTime();
//
//        Date age = new Date(ageInMillis);
//
//        return age.getYear();
//    }
}
