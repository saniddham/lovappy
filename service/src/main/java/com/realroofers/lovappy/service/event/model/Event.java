package com.realroofers.lovappy.service.event.model;


import com.realroofers.lovappy.service.event.model.embeddable.EventLocation;
import com.realroofers.lovappy.service.event.support.EventStatus;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.*;


/**
 * Created by Tejaswi Venupalli on 8/28/17
 */

@Entity
@EqualsAndHashCode
public class Event {

    @Id
    @GeneratedValue
    private Integer eventId;

    @Column(name = "event_title", nullable = false)
    private String eventTitle;

    @Column(name = "event_description", nullable = false)
    @Type(type = "text")
    private String eventDescription;

    @Column(name = "event_date", nullable = false)
    private Date eventDate;

    private Date startTime;
    private Date finishTime;

    @Enumerated(EnumType.STRING)
    private ApprovalStatus eventApprovalStatus;

    @Embedded
    private EventLocation eventLocation;

    private String eventLocationDescription;

    private Integer attendeesLimit;

    private Integer outsiderCount;

    @Column(name = "admission_cost", precision = 10, scale = 2)
    private Double admissionCost;

    @Enumerated(EnumType.STRING)
    private EventStatus eventStatus;

    @Column(name = "ambassador_user_id")
    private Integer ambassadorUserId;

    @Column(name = "approved", columnDefinition = "BIT(1) default 0")
    private Boolean approved;

    private String timeZone;

//    @ManyToMany
//    @JoinTable(name ="event_user",
//            joinColumns = @JoinColumn(
//                name = "event_id",referencedColumnName = "eventId"
//            ),
//            inverseJoinColumns = @JoinColumn(
//                        name = "user_id",referencedColumnName = "userId"
//            )
//    )
//    private Collection<User> userList = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "id.event", cascade = CascadeType.ALL)
    private Set<EventUser> eventUsers;

    @ManyToMany
    @JoinTable(name = "event_eventcategory",
            joinColumns = @JoinColumn(
                        name = "event_id", referencedColumnName = "eventId"
            ),
            inverseJoinColumns = @JoinColumn(
                        name = "category_id"
            )
    )
    private Collection<EventCategory> eventCategories = new ArrayList<>();

    @ElementCollection(targetClass = String.class)
    private Map<String,String> targetedAudience = new HashMap<>();

    @OneToMany(mappedBy = "eventId", fetch = FetchType.EAGER, cascade = CascadeType.ALL,orphanRemoval = true)
    private List<EventLocationPicture> locationPictures =  new ArrayList<>();

    @OneToMany(mappedBy = "eventId", orphanRemoval = true)
    private Collection<EventComment> comments = new ArrayList<>();

    @OneToMany(mappedBy = "eventId", cascade = CascadeType.ALL)
    private List<EventUserPicture> eventPictures =  new ArrayList<>();

    @Column(name = "facebook_shares", columnDefinition = "INT(11) default 0")
    private Integer facebookShares;

    @Column(name = "linkedin_shares", columnDefinition = "INT(11) default 0")
    private Integer linkedinShares;

    @Column(name = "google_shares", columnDefinition = "INT(11) default 0")
    private Integer googleShares;

    @Column(name = "twitter_shares", columnDefinition = "INT(11) default 0")
    private Integer twitterShares;

    @Column(name = "email_shares", columnDefinition = "INT(11) default 0")
    private Integer emailShares;

    @Column(name = "pinterest_shares", columnDefinition = "INT(11) default 0")
    private Integer pinterestShares;

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public EventLocation getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(EventLocation eventLocation) {
        this.eventLocation = eventLocation;
    }

    public String getEventLocationDescription() {
        return eventLocationDescription;
    }

    public void setEventLocationDescription(String eventLocationDescription) {
        this.eventLocationDescription = eventLocationDescription;
    }

    public Integer getAttendeesLimit() {
        return attendeesLimit;
    }

    public void setAttendeesLimit(Integer attendeesLimit) {
        this.attendeesLimit = attendeesLimit;
    }

    public Integer getOutsiderCount() {
        return outsiderCount;
    }

    public void setOutsiderCount(Integer outsiderCount) {
        this.outsiderCount = outsiderCount;
    }

    public Double getAdmissionCost() {
        return admissionCost;
    }

    public void setAdmissionCost(Double admissionCost) {
        this.admissionCost = admissionCost;
    }

    public EventStatus getEventStatus() {
        return eventStatus;
    }

    public void setEventStatus(EventStatus eventStatus) {
        this.eventStatus = eventStatus;
    }

    public Integer getAmbassadorUserId() {
        return ambassadorUserId;
    }

    public void setAmbassadorUserId(Integer ambassadorUserId) {
        this.ambassadorUserId = ambassadorUserId;
    }

//    public Collection<User> getUserList() {
//        return userList;
//    }
//
//    public void setUserList(Collection<User> userList) {
//        this.userList = userList;
//    }

    public Set<EventUser> getEventUsers() {
        return eventUsers;
    }

    public void setEventUsers(Set<EventUser> eventUsers) {
        this.eventUsers = eventUsers;
    }

    public Collection<EventCategory> getEventCategories() {
        return eventCategories;
    }

    public void setEventCategories(Collection<EventCategory> eventCategories) {
        this.eventCategories = eventCategories;
    }

    public Map<String, String> getTargetedAudience() {
        return targetedAudience;
    }

    public void setTargetedAudience(Map<String, String> targetedAudience) {
        this.targetedAudience = targetedAudience;
    }

    public List<EventLocationPicture> getLocationPictures() {
        return locationPictures;
    }

    public void setLocationPictures(List<EventLocationPicture> locationPictures) {
        this.locationPictures = locationPictures;
    }

    public Collection<EventComment> getComments() {
        return comments;
    }

    public void setComments(Collection<EventComment> comments) {
        this.comments = comments;
    }

    public List<EventUserPicture> getEventPictures() {
        return eventPictures;
    }

    public void setEventPictures(List<EventUserPicture> eventPictures) {
        this.eventPictures = eventPictures;
    }

    public Integer getFacebookShares() {
        return facebookShares;
    }

    public void setFacebookShares(Integer facebookShares) {
        this.facebookShares = facebookShares;
    }

    public Integer getLinkedinShares() {
        return linkedinShares;
    }

    public void setLinkedinShares(Integer linkedinShares) {
        this.linkedinShares = linkedinShares;
    }

    public Integer getGoogleShares() {
        return googleShares;
    }

    public void setGoogleShares(Integer googleShares) {
        this.googleShares = googleShares;
    }

    public Integer getTwitterShares() {
        return twitterShares;
    }

    public void setTwitterShares(Integer twitterShares) {
        this.twitterShares = twitterShares;
    }

    public Integer getEmailShares() {
        return emailShares;
    }

    public void setEmailShares(Integer emailShares) {
        this.emailShares = emailShares;
    }

    public Integer getPinterestShares() {
        return pinterestShares;
    }

    public void setPinterestShares(Integer pinterestShares) {
        this.pinterestShares = pinterestShares;
    }

    public ApprovalStatus getEventApprovalStatus() {
        return eventApprovalStatus;
    }

    public void setEventApprovalStatus(ApprovalStatus eventApprovalStatus) {
        this.eventApprovalStatus = eventApprovalStatus;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
}
