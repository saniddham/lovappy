package com.realroofers.lovappy.service.blog.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.realroofers.lovappy.service.blog.dto.CategoryDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;


@Entity(name = "Category")
@Table(name = "category")
@Data
@EqualsAndHashCode
public class Category implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3447409517106084077L;

	@Id
	@GeneratedValue()
	private Integer id;


	@Column(name="name")
	private String name;
	
	@Column(name="state")
	private String catState;
	

    @ManyToOne
    @JoinColumn(name = "created_by")
    private User createdBy;

    private Date createdAt;

    @ManyToOne
    @JoinColumn(name = "updated_by")
    private User updatedBy;

    private Date updatedAt;

    @ManyToOne
    @JoinColumn(name = "deleted_by")
    private User deletedBy;


	@ManyToMany
	@JoinTable(
			name = "category_images",
			joinColumns = @JoinColumn(
					name = "cat_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(
					name = "img_id", referencedColumnName = "id"))
	@JsonManagedReference
    private List<CloudStorageFile> images = new ArrayList<>();

	public Category(Integer id) {
		super();
		id = id;
	}


	public Category(CategoryDto categoryDto) {
		super();
		id = categoryDto.getId();
		name = categoryDto.getName();
	}

	public Category() {
	}

}
