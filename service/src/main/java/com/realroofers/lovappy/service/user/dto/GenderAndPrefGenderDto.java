package com.realroofers.lovappy.service.user.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.realroofers.lovappy.service.user.model.UserPreference;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.validator.UserRegister;
import lombok.EqualsAndHashCode;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@EqualsAndHashCode
public class GenderAndPrefGenderDto implements Serializable {
    private Gender gender;
    private Gender prefGender;
	private Date birthDate;
	private AddressDto location;
	private String userType;


	public GenderAndPrefGenderDto(UserProfile profile, UserPreference pref) {
		gender = profile != null ? profile.getGender() : null;
		birthDate = profile != null ? profile.getBirthDate() : null;
		prefGender = pref != null ? pref.getGender() : null;

		if (profile!=null&& profile.getAddress() != null) {
			this.location = AddressDto.toAddressDto(profile.getAddress());
		} else {
			this.location = new AddressDto();
			this.location.setLastMileRadiusSearch(300);
		}
	}

    public GenderAndPrefGenderDto() {
		this.location = new AddressDto();
		this.location.setLastMileRadiusSearch(300);
    }

    public GenderAndPrefGenderDto(Gender gender, Gender prefGender) {
        this.gender = gender;
        this.prefGender = prefGender;
		this.location = new AddressDto();
		this.location.setLastMileRadiusSearch(300);
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Gender getPrefGender() {
        return prefGender;
    }

    public void setPrefGender(Gender prefGender) {
        this.prefGender = prefGender;
    }

    public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public AddressDto getLocation() {
		return location;
	}

	public void setLocation(AddressDto location) {
		this.location = location;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	@Override
	public String toString() {
		return "GenderAndPrefGenderDto{" +
				"gender=" + gender +
				", prefGender=" + prefGender +
				", birthDate=" + birthDate +
				", location=" + location +
				", userType='" + userType + '\'' +
				'}';
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GenderAndPrefGenderDto that = (GenderAndPrefGenderDto) o;

    	if (gender != that.gender) return false;
		if (birthDate != that.birthDate) return false;
//		if (location != that.location) return false;
		return prefGender == that.prefGender;
	}

	@Override
	public int hashCode() {
		int result = gender != null ? gender.hashCode() : 0;
		result = 31 * result + (prefGender != null ? prefGender.hashCode() : 0);
		result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
		return result;
	}
}