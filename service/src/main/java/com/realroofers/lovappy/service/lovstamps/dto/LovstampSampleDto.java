package com.realroofers.lovappy.service.lovstamps.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.lovstamps.model.LovstampSample;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by Eias Altawil on 5/18/17
 */
@Data
@ToString
@EqualsAndHashCode
public class LovstampSampleDto {

    private Integer id;
    private CloudStorageFileDto audioFileCloud;
    private LanguageDto language;
    private Gender gender;
    private Integer duration;
    public LovstampSampleDto() {
    }

    public LovstampSampleDto(LovstampSample lovdropSample) {
        if (lovdropSample != null) {
            this.id = lovdropSample.getId();
            this.audioFileCloud = new CloudStorageFileDto(lovdropSample.getAudioFileCloud());
            this.language = new LanguageDto(lovdropSample.getLanguage());
            this.gender = lovdropSample.getGender();
            this.duration =  lovdropSample.getDuration();
        }
    }
}
