package com.realroofers.lovappy.service.product.model;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.product.dto.ProductDto;
import com.realroofers.lovappy.service.product.support.ProductAvailability;
import com.realroofers.lovappy.service.product.support.Upload;
import com.realroofers.lovappy.service.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Manoj on 31/01/2018.
 */
@Entity
@Data
@EqualsAndHashCode
@Table(name = "product")
public class Product extends BaseEntity<Integer> implements Serializable {

    @NotNull(message = "Product type cannot be empty.", groups = {Upload.class})
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_type")
    private ProductType productType;


    @NotNull(message = "Product name cannot be empty.", groups = {Upload.class})
    @Column(name = "product_name")
    private String productName;


    @NotNull(message = "Product code cannot be empty.", groups = {Upload.class})
    @Column(name = "product_code")
    private String productCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_brand")
    private Brand brand;

    @Column(name = "description")
    @Type(type = "text")
    private String productDescription;

    @NotNull(message = "Price cannot be empty.", groups = {Upload.class})
    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "product_availability")
    private ProductAvailability productAvailability;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "image1")
    private CloudStorageFile image1;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "image2")
    private CloudStorageFile image2;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "image3")
    private CloudStorageFile image3;

    @Column(name = "ean")
    private String productEan;

    @Column(name = "commission")
    private BigDecimal commissionEarned;

    // normal enable flag for the seller to activate and deactivate product
    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;

    // This is true when the admin approves the item
    @Column(name = "is_approved", columnDefinition = "boolean default false", nullable = false)
    private Boolean approved = false;

    // This is true when the admin block an item
    @Column(name = "is_blocked", columnDefinition = "boolean default false", nullable = false)
    private Boolean blocked = false;

    @JoinColumn(name = "approved_by")
    @ManyToOne
    private User approvedBy;

    @JoinColumn(name = "blocked_by")
    @ManyToOne
    private User blockedBy;

    @JoinColumn(name = "created_by")
    @ManyToOne
    private User createdBy;

    @JoinColumn(name = "updated_by")
    @ManyToOne
    private User updatedBy;

    public Product() {
    }

    public Product(ProductDto product) {
        if (product.getId() != null) {
            setId(product.getId());
        }
        this.productName = product.getProductName();
        this.productCode = product.getProductCode();
        this.productDescription = product.getProductDescription();
        this.price = product.getPrice();
        this.productAvailability = product.getProductAvailability();

        this.productType = product.getProductType() != null ? new ProductType(product.getProductType()) : null;
        this.brand = product.getBrand() != null ? new Brand(product.getBrand()) : null;
        this.image1 = (product.getImage1() != null && product.getImage1().getId() != null) ? new CloudStorageFile(product.getImage1().getId()) : null;
        this.image2 = (product.getImage2() != null && product.getImage2().getId() != null) ? new CloudStorageFile(product.getImage2().getId()) : null;
        this.image3 = (product.getImage3() != null && product.getImage3().getId() != null) ? new CloudStorageFile(product.getImage3().getId()) : null;
        this.productEan = product.getProductEan();
        this.commissionEarned = product.getCommissionEarned();
        this.active = product.getActive() != null ? product.getActive() : true;
        this.approved = product.getApproved() != null ? product.getApproved() : false;
        this.blocked = product.getBlocked() != null ? product.getBlocked() : false;

        this.approvedBy = product.getApprovedBy() != null ? new User(product.getApprovedBy().getID()) : null;
        this.blockedBy = product.getBlockedBy() != null ? new User(product.getBlockedBy().getID()) : null;
        this.createdBy = product.getCreatedBy() != null ? new User(product.getCreatedBy().getID()) : null;
        this.updatedBy = product.getUpdatedBy() != null ? new User(product.getUpdatedBy().getID()) : null;
    }
}
