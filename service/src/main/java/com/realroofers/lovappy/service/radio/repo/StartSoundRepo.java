package com.realroofers.lovappy.service.radio.repo;

import com.realroofers.lovappy.service.radio.model.StartSound;
import com.realroofers.lovappy.service.radio.support.StartSoundType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * @author Eias Altawil
 */
public interface StartSoundRepo extends JpaRepository<StartSound, Integer>, QueryDslPredicateExecutor<StartSound> {
    StartSound findBySoundType(StartSoundType startSoundType);
}
