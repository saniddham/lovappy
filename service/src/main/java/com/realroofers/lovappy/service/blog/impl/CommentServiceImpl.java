package com.realroofers.lovappy.service.blog.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.realroofers.lovappy.service.blog.CommentService;
import com.realroofers.lovappy.service.blog.model.Comment;
import com.realroofers.lovappy.service.blog.model.QComment;
import com.realroofers.lovappy.service.blog.repo.CommentRepo;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentRepo commentRepo;

    @Transactional
    public void save(Comment comment) {
        commentRepo.save(comment);
    }

    @Transactional(readOnly = true)
    public List<Comment> getAllComments() {
        return commentRepo.findAll();
    }

    @Transactional
    public void update(Comment comment) {
        commentRepo.save(comment);
    }
    
    @Transactional(readOnly = true)
    public List<Comment> findByPostId(Integer postId) {
    	QComment qComment = QComment.comment;
		return Lists.newArrayList(commentRepo.findAll(qComment.post.id
				.eq(postId)));
    }

    

}
