package com.realroofers.lovappy.service.datingPlaces.dto;

import com.realroofers.lovappy.service.datingPlaces.model.DatingPlace;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceClaim;
import com.realroofers.lovappy.service.datingPlaces.support.VerificationType;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import lombok.EqualsAndHashCode;

/**
 * Created by Darrel Rayen on 1/15/18.
 */
@EqualsAndHashCode
public class DatingPlaceClaimDto {

    private Integer claimId;
    private DatingPlace place;
    private String content;
    private String sendTo;
    private String sendFrom;
    private String verificationCode;
    private ApprovalStatus placeApprovalStatus;
    private User claimer;
    private Integer claimerId;
    private String placeId;
    private VerificationType verificationType;

    public DatingPlaceClaimDto() {
    }

    public DatingPlaceClaimDto(DatingPlaceClaim placeClaim) {
        this.claimId = placeClaim.getClaimId();
        this.place = placeClaim.getPlaceId();
        this.content = placeClaim.getContent();
        this.sendTo = placeClaim.getSendTo();
        this.sendFrom = placeClaim.getSendFrom();
        this.verificationCode = placeClaim.getVerificationCode();
        this.placeApprovalStatus = placeClaim.getPlaceApprovalStatus();
        this.claimer = placeClaim.getClaimer();
        this.verificationType = placeClaim.getVerificationType();
    }

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }

    public DatingPlace getPlace() {
        return place;
    }

    public void setPlace(DatingPlace place) {
        this.place = place;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSendTo() {
        return sendTo;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    public String getSendFrom() {
        return sendFrom;
    }

    public void setSendFrom(String sendFrom) {
        this.sendFrom = sendFrom;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public ApprovalStatus getPlaceApprovalStatus() {
        return placeApprovalStatus;
    }

    public void setPlaceApprovalStatus(ApprovalStatus placeApprovalStatus) {
        this.placeApprovalStatus = placeApprovalStatus;
    }

    public User getClaimer() {
        return claimer;
    }

    public void setClaimer(User claimer) {
        this.claimer = claimer;
    }

    public Integer getClaimerId() {
        return claimerId;
    }

    public void setClaimerId(Integer claimerId) {
        this.claimerId = claimerId;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public VerificationType getVerificationType() {
        return verificationType;
    }

    public void setVerificationType(VerificationType verificationType) {
        this.verificationType = verificationType;
    }
}
