package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@EqualsAndHashCode
public class UserMinDto implements Serializable {
    Integer ID;
    private String email;
    private String title;
    private String firstName;
    private String lastName;
    private String zipcode;


    public UserMinDto(User user) {
        if (user != null) {
            this.ID = user.getUserId();
            this.email = user.getEmail();
            this.title = user.getTitle();
            this.firstName = user.getFirstName();
            this.lastName = user.getLastName();
        }
    }

}
