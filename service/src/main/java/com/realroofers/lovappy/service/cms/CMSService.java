package com.realroofers.lovappy.service.cms;

import com.realroofers.lovappy.service.cms.dto.*;
import com.realroofers.lovappy.service.cms.model.ImageWidget;
import com.realroofers.lovappy.service.cms.model.Page;
import com.realroofers.lovappy.service.cms.model.TextTranslation;
import com.realroofers.lovappy.service.cms.model.TextWidget;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Daoud Shaheen on 6/6/2017.
 */
public interface CMSService {
    List<String> getImagesByPageTagAndImageType(String pageTag, String imageType);
    Set<ImageWidget> getImagesByPageTag(String pageTag);
    PageDTO findPageByTagWithImagesAndTexts(String pageTag, String language);
    void savePageImage(String pageTag, ImageWidget imagePageContent);
    void updatePageImage(String pageTag, Integer imageId, ImageWidgetDTO imageWidgetDTO);
    TextWidget getTextById(Integer textId);
    String getTextByTagAndLanguage(String tag, String language);
    String getImageUrlByTag(String tag);
    TextTranslation updateTextByIdAndLanguage(Integer textId, String language, String updatedContent);

    TextTranslation getTextTranslationByTagAndLanguage(String tag, String language);

    void savePageText(String pageTag, TextWidgetDTO textWidget);

    String updateTextByTagAndLanguage(String tag, String language, String content);

    Set<VideoWidgetDTO> getVideosByPageTag(String pageTag);
    VideoWidgetDTO getVideosByKey(String key);
    void updatePageVideo(String pageTag, Integer videoId, VideoWidgetDTO videoWidgetDTO);

    void updateMetaTags(PageMetaDTO PageMetaDTO, Integer metaId);

    List<Page> getAllPages();

    org.springframework.data.domain.Page<Page> getAllPagesByName(String name, Pageable pageable);

    List<Page> getAllActivePages();

    Map<String, String> getPageSettings(String pageName);
    void updateSettings(String pageName, Map<String, String> settings);

    List<Page> findAllOrderByActive();

    Page updatePageStatus(Integer pageId);
}
