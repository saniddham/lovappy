package com.realroofers.lovappy.service.datingPlaces.repo;

import com.realroofers.lovappy.service.datingPlaces.model.DatingPlace;
import com.realroofers.lovappy.service.datingPlaces.support.DatingAgeRange;
import com.realroofers.lovappy.service.datingPlaces.support.DatingPlaceProvider;
import com.realroofers.lovappy.service.datingPlaces.support.PriceRange;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Darrel Rayen on 10/19/17.
 */
@Repository
public interface DatingPlaceRepo extends JpaRepository<DatingPlace, Integer>, DatingPlaceRepoCustom {

    Page<DatingPlace> findAllByOrderByDatingPlacesIdDesc(Pageable pageable);

    List<DatingPlace> findAllByPlaceApprovalStatusOrderByDatingPlacesIdDesc(ApprovalStatus approvalStatus);

    Page<DatingPlace> findAllByPlaceApprovalStatusAndIsFeaturedFalseOrderByAverageRatingDesc(Pageable pageable, ApprovalStatus approvalStatus);

    @Query("SELECT e FROM DatingPlace e WHERE SQRT((ABS(e.latitude - ?1) * ABS(e.latitude - ?1)) + (ABS(e.longitude - ?2) * ABS(e.longitude - ?2))) * 70 <= 30" +
            " and e.placeApprovalStatus='Approved' order by e.averageRating DESC ")
    Page<DatingPlace> findNearByDatingPlacesByLatitudeAndLongitude(Double latitude, Double longitude, Pageable pageable);

    @Query("SELECT e FROM DatingPlace e WHERE SQRT((ABS(e.latitude - ?1) * ABS(e.latitude - ?1)) + (ABS(e.longitude - ?2) * ABS(e.longitude - ?2))) * 70 <= 30" +
            " and e.placeApprovalStatus='Approved' order by e.averageRating ASC ")
    Page<DatingPlace> findNearByDatingPlacesByLatitudeAndLongitudeAsc(Double latitude, Double longitude, Pageable pageable);

    @Query("SELECT e FROM DatingPlace e WHERE SQRT((ABS(e.latitude - ?1) * ABS(e.latitude - ?1)) + (ABS(e.longitude - ?2) * ABS(e.longitude - ?2))) * 70 <= 30" +
            " and e.placeApprovalStatus='Approved' order by e.averageRating DESC ")
    List<DatingPlace> findNearByDatingPlacesByLatitudeAndLongitudeList(Double latitude, Double longitude);

    @Query("SELECT e FROM DatingPlace e WHERE SQRT((ABS(e.latitude - ?1) * ABS(e.latitude - ?1)) + (ABS(e.longitude - ?2) * ABS(e.longitude - ?2))) * 70 <= 30" +
            " and e.placeApprovalStatus='Approved' order by e.averageRating ASC ")
    List<DatingPlace> findNearByDatingPlacesByLatitudeAndLongitudeListAsc(Double latitude, Double longitude);

    Page<DatingPlace> findAllByGenderAndPlaceApprovalStatus(Pageable pageable, Gender gender, ApprovalStatus approvalStatus);

    Page<DatingPlace> findAllByPriceRangeAndPlaceApprovalStatus(Pageable pageable, PriceRange priceRange, ApprovalStatus approvalStatus);

    Page<DatingPlace> findAllByAgeRangeAndPlaceApprovalStatus(Pageable pageable, DatingAgeRange ageRange, ApprovalStatus approvalStatus);

    Page<DatingPlace> findAllByAgeRangeAndPriceRangeAndGenderAndPlaceApprovalStatus(Pageable pageable, DatingAgeRange ageRange, PriceRange priceRange, Gender gender, ApprovalStatus approvalStatus);

    Page<DatingPlace> findByLoveStampFeatured(Boolean featuredStamp, Pageable pageable);

    List<DatingPlace> findAllByIsFeaturedTrueAndPlaceApprovalStatusOrderByDatingPlacesIdDesc(ApprovalStatus approvalStatus);

    @Query("SELECT e FROM DatingPlace e WHERE SQRT((ABS(e.latitude - ?1) * ABS(e.latitude - ?1)) + (ABS(e.longitude - ?2) * ABS(e.longitude - ?2))) * 70 <= ?3" +
            " and e.placeApprovalStatus='Approved' and e.city like CONCAT(?4,'%') ")
    List<DatingPlace> findNearByDatingPlacesByRadiusList(Double latitude, Double longitude, Double radius, String city);

    List<DatingPlace> findAllByExternalPlaceIdIsNotNull();

    DatingPlace findByExternalPlaceIdAndProvider(String externalId, DatingPlaceProvider provider);

    Page<DatingPlace> findAllByPlaceApprovalStatus(ApprovalStatus approvalStatus, Pageable pageable);

    @Query("SELECT dp FROM DatingPlace dp , DatingPlaceReview dpv WHERE dp = dpv.placeId" +
            " AND dp.placeName LIKE CONCAT('%',:placeName,'%')" +
            " AND dp.placeApprovalStatus= 'APPROVED'" +
            " AND dpv.reviewApprovalStatus= 'APPROVED'" +
            " AND (:ageRange is null or dpv.suitableAgeRange LIKE CONCAT(:ageRange,'%'))" +
            " AND (:gender is null or dpv.suitableGender LIKE CONCAT(:gender,'%'))" +
            " AND (:priceRange is null or dpv.suitablePriceRange LIKE CONCAT(:priceRange,'%'))" +
            " AND dpv.securityRating >= :security" +
            " AND dpv.parkingRating >= :parking" +
            " AND dpv.parkingAccessRating >= :transport" +
            " AND dp.neighborHood LIKE CONCAT(:neighbor,'%')" +
            " AND (:pets is null or dp.isPetsAllowed = :pets)" +
            " AND (:couple is null or dpv.isGoodForCouple = :couple)" +
            " GROUP BY dpv.placeId" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> getSearchedPlaces(Pageable pageable, @Param("placeName") String placeName,
                                        @Param("ageRange") String ageRange,
                                        @Param("gender") String gender, @Param("priceRange") String priceRange,
                                        @Param("security") Double security, @Param("parking") Double parking,
                                        @Param("transport") Double transport, @Param("pets") Boolean petsAllowed,
                                        @Param("neighbor") String neighbor, @Param("couple") Boolean coupleAllowed);

    @Query("SELECT dp FROM DatingPlace dp , DatingPlaceReview dpv WHERE dp = dpv.placeId" +
            " AND dp.placeName LIKE CONCAT('%',:placeName,'%')" +
            " AND dp.placeApprovalStatus= 'APPROVED'" +
            " AND dpv.reviewApprovalStatus= 'APPROVED'" +
            " AND (:ageRange is null or dpv.suitableAgeRange LIKE CONCAT(:ageRange,'%'))" +
            " AND (:gender is null or dpv.suitableGender LIKE CONCAT(:gender,'%'))" +
            " AND (:priceRange is null or dpv.suitablePriceRange LIKE CONCAT(:priceRange,'%'))" +
            " AND dpv.securityRating >= :security" +
            " AND dpv.parkingRating >= :parking" +
            " AND dpv.parkingAccessRating >= :transport" +
            " AND dp.neighborHood LIKE CONCAT(:neighbor,'%')" +
            " AND (:pets is null or dp.isPetsAllowed = :pets)" +
            " AND (:couple is null or dpv.isGoodForCouple = :couple)" +
            " GROUP BY dpv.placeId" +
            " ORDER BY dp.averageRating DESC")
    List<DatingPlace> getSearchedPlaces(@Param("placeName") String placeName,
                                        @Param("ageRange") String ageRange,
                                        @Param("gender") String gender, @Param("priceRange") String priceRange,
                                        @Param("security") Double security, @Param("parking") Double parking,
                                        @Param("transport") Double transport, @Param("pets") Boolean petsAllowed,
                                        @Param("neighbor") String neighbor, @Param("couple") Boolean coupleAllowed);


    @Query("SELECT dp FROM DatingPlace dp , DatingPlaceReview dpv WHERE dp = dpv.placeId" +
            " AND SQRT((ABS(dp.latitude - :latitude) * ABS(dp.latitude - :latitude)) + (ABS(dp.longitude - :longitude) * ABS(dp.longitude - :longitude))) * 70 <= :radius" +
            " AND dp.placeName LIKE CONCAT('%',:placeName,'%')" +
            " AND dp.placeApprovalStatus= 'APPROVED'" +
            " AND dpv.reviewApprovalStatus= 'APPROVED'" +
            " AND (:ageRange is null or dpv.suitableAgeRange LIKE CONCAT(:ageRange,'%'))" +
            " AND (:gender is null or dpv.suitableGender LIKE CONCAT(:gender,'%'))" +
            " AND (:priceRange is null or dpv.suitablePriceRange LIKE CONCAT(:priceRange,'%'))" +
            " AND dpv.securityRating >= :security" +
            " AND dpv.parkingRating >= :parking" +
            " AND dpv.parkingAccessRating >= :transport" +
            " AND dp.neighborHood LIKE CONCAT(:neighbor,'%')" +
            " AND (:pets is null or dp.isPetsAllowed = :pets)" +
            " AND (:couple is null or dpv.isGoodForCouple = :couple)" +
            " GROUP BY dpv.placeId" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> getSearchedPlacesByLocation(Pageable pageable, @Param("placeName") String placeName,
                                                  @Param("ageRange") String ageRange,
                                                  @Param("gender") String gender, @Param("priceRange") String priceRange,
                                                  @Param("security") Double security, @Param("parking") Double parking,
                                                  @Param("transport") Double transport, @Param("pets") Boolean petsAllowed,
                                                  @Param("neighbor") String neighbor, @Param("couple") Boolean coupleAllowed,
                                                  @Param("latitude") Double latitude, @Param("longitude") Double longitude,
                                                  @Param("radius") Double radius);



    @Query("SELECT dp FROM DatingPlace dp , DatingPlaceReview dpv WHERE dp = dpv.placeId" +
            " AND SQRT((ABS(dp.latitude - :latitude) * ABS(dp.latitude - :latitude)) + (ABS(dp.longitude - :longitude) * ABS(dp.longitude - :longitude))) * 70 <= :radius" +
            " AND dp.placeName LIKE CONCAT('%',:placeName,'%')" +
            " AND dp.placeApprovalStatus= 'APPROVED'" +
            " AND dpv.reviewApprovalStatus= 'APPROVED'" +
            " AND (:ageRange is null or dpv.suitableAgeRange LIKE CONCAT(:ageRange,'%'))" +
            " AND (:gender is null or dpv.suitableGender LIKE CONCAT(:gender,'%'))" +
            " AND (:priceRange is null or dpv.suitablePriceRange LIKE CONCAT(:priceRange,'%'))" +
            " AND dpv.securityRating >= :security" +
            " AND dpv.parkingRating >= :parking" +
            " AND dpv.parkingAccessRating >= :transport" +
            " AND dp.neighborHood LIKE CONCAT(:neighbor,'%')" +
            " AND (:pets is null or dp.isPetsAllowed = :pets)" +
            " AND (:couple is null or dpv.isGoodForCouple = :couple)" +
            " GROUP BY dpv.placeId" +
            " ORDER BY dp.averageRating DESC")
    List<DatingPlace> getSearchedPlacesByLocation(@Param("placeName") String placeName,
                                                  @Param("ageRange") String ageRange,
                                                  @Param("gender") String gender, @Param("priceRange") String priceRange,
                                                  @Param("security") Double security, @Param("parking") Double parking,
                                                  @Param("transport") Double transport, @Param("pets") Boolean petsAllowed,
                                                  @Param("neighbor") String neighbor, @Param("couple") Boolean coupleAllowed,
                                                  @Param("latitude") Double latitude, @Param("longitude") Double longitude,
                                                  @Param("radius") Double radius);

    @Query("SELECT dp FROM DatingPlace dp WHERE dp.averageRating=(SELECT MAX(d.averageRating) " +
            "FROM DatingPlace d where d.placeApprovalStatus = 'APPROVED')")
    List<DatingPlace> findAllByMaximumAverageRating();

    @Query("SELECT dp FROM DatingPlace dp,DatingPlaceReview dpv WHERE dp = dpv.placeId " +
            "AND dp.placeApprovalStatus = 'APPROVED' " +
            "AND dpv.reviewApprovalStatus = 'APPROVED' " +
            "AND dpv.suitableGender LIKE CONCAT(:gender,'%') " +
            "GROUP BY dpv.placeId " +
            "ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findAllByGenderTest(@Param("gender") String gender, Pageable pageable);

    @Query("SELECT dp FROM DatingPlace dp,DatingPlaceReview dpv WHERE dp = dpv.placeId " +
            "AND dp.placeApprovalStatus = 'APPROVED' " +
            "AND dpv.reviewApprovalStatus = 'APPROVED' " +
            "AND dpv.suitableGender LIKE CONCAT(:gender,'%') " +
            "GROUP BY dpv.placeId " +
            "ORDER BY dp.averageRating DESC")
    List<DatingPlace> findAllByGenderTest(@Param("gender") String gender);

    @Query("select dp from DatingPlace dp where dp.averageOverAllRatingByUser is null and (dp.googleAverage is null or dp.averageRating <> dp.googleAverage)")
    List<DatingPlace> findAllByAverageOverAllRatingByUserIsNull();



//    @Query("SELECT dp FROM DatingPlace dp WHERE  dp.placeApprovalStatus = 'APPROVED' " +
//            "AND dp.suitableGender LIKE CONCAT(:gender,'%') " +
//            "GROUP BY dpv.placeId " +
//            "ORDER BY dp.averageRating DESC")
//    Page<DatingPlace> findPlacesByGenderAndRatingDesc(@Param("gender") String gender, Pageable pageable);

    @Query("SELECT dp ,SQRT((ABS(dp.latitude - :latitude) * ABS(dp.latitude - :latitude)) + (ABS(dp.longitude - :longitude) * ABS(dp.longitude - :longitude))) * 70 AS distance FROM DatingPlace dp WHERE SQRT((ABS(dp.latitude - :latitude) * ABS(dp.latitude - :latitude)) + (ABS(dp.longitude - :longitude) * ABS(dp.longitude - :longitude))) * 70 <= :radius AND dp.placeApprovalStatus= 'APPROVED' order by dp.isFeatured DESC,  distance ASC")
    Page<Object[]> getDefaultPlaces(Pageable pageable, @Param("latitude") Double latitude,
                                    @Param("longitude") Double longitude,
                                    @Param("radius") Double radius);

    //find featured places
    @Query("SELECT dp FROM DatingPlace dp  WHERE dp.isFeatured=true AND dp.placeApprovalStatus= 'APPROVED'" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findFeaturedPlacesByRatingDesc(Pageable pageable);

    @Query("SELECT dp FROM DatingPlace dp WHERE dp.placeApprovalStatus= 'APPROVED' AND dp.isFeatured=true AND SQRT((ABS(dp.latitude - :latitude) * ABS(dp.latitude - :latitude)) + (ABS(dp.longitude - :longitude) * ABS(dp.longitude - :longitude))) * 70 <= :radius  order by dp.averageRating DESC")
    Page<DatingPlace> findNearByFeaturedPlacesByRatingDesc(Pageable pageable, @Param("latitude") Double latitude,
                                    @Param("longitude") Double longitude,
                                    @Param("radius") Double radius);

    //Quick filter quires
    //worldwide query
    @Query("SELECT dp FROM DatingPlace dp  WHERE  dp.placeApprovalStatus= 'APPROVED'" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findPlacesByRatingDesc(Pageable pageable);

    //worldwide query all gender query
    @Query("SELECT dp FROM DatingPlace dp  WHERE  dp.placeApprovalStatus= 'APPROVED'" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findPlacesByAllGendersDesc(Pageable pageable);

    @Query("SELECT dp FROM DatingPlace dp  WHERE dp.suitableForMaleCount > 0 AND dp.placeApprovalStatus= 'APPROVED'" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findPlacesByMaleGendersDesc(Pageable pageable);
    @Query("SELECT dp FROM DatingPlace dp  WHERE dp.suitableForFemale > 0 AND dp.placeApprovalStatus= 'APPROVED'" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findPlacesByFemaleGendersDesc(Pageable pageable);

    @Query("SELECT dp FROM DatingPlace dp  WHERE dp.suitableForBoth > 0 AND dp.placeApprovalStatus= 'APPROVED'" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findPlacesByBothGendersDesc(Pageable pageable);

    // local queriesp
     final static String DISTANCE_FORMULA = "SQRT((ABS(dp.latitude - :latitude) * ABS(dp.latitude - :latitude)) + (ABS(dp.longitude - :longitude) * ABS(dp.longitude - :longitude))) * 70";

    @Query("SELECT dp FROM DatingPlace dp  WHERE " + DISTANCE_FORMULA + " < :radius AND dp.placeApprovalStatus= 'APPROVED'" +
            " ORDER BY dp.averageRating DESC")
     Page<DatingPlace> findByAllGenderAndLocation(Pageable pageable, @Param("latitude") Double latitude,
                                    @Param("longitude") Double longitude,
                                    @Param("radius") Double radius);

    @Query("SELECT dp FROM DatingPlace dp  WHERE dp.suitableForBoth > 0 AND " + DISTANCE_FORMULA + " < :radius AND dp.placeApprovalStatus= 'APPROVED'" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findByBothGenderAndLocation(Pageable pageable, @Param("latitude") Double latitude,
                                                 @Param("longitude") Double longitude,
                                                 @Param("radius") Double radius);
    @Query("SELECT dp FROM DatingPlace dp  WHERE dp.suitableForFemale > 0 AND " + DISTANCE_FORMULA + " < :radius AND dp.placeApprovalStatus= 'APPROVED'" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findByFemaleGenderAndLocation(Pageable pageable, @Param("latitude") Double latitude,
                                                  @Param("longitude") Double longitude,
                                                  @Param("radius") Double radius);
    @Query("SELECT dp FROM DatingPlace dp  WHERE dp.suitableForMaleCount > 0 AND " + DISTANCE_FORMULA + " < :radius AND dp.placeApprovalStatus= 'APPROVED'" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findByMaleGenderAndLocation(Pageable pageable, @Param("latitude") Double latitude,
                                                    @Param("longitude") Double longitude,
                                                    @Param("radius") Double radius);

    @Query("SELECT dp FROM DatingPlace dp  WHERE " + DISTANCE_FORMULA + " < :radius AND dp.placeApprovalStatus= 'APPROVED' And dp.isFeatured=true" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findFeaturedByAllGenderAndLocation(Pageable pageable, @Param("latitude") Double latitude,
                                                 @Param("longitude") Double longitude,
                                                 @Param("radius") Double radius);

    @Query("SELECT dp FROM DatingPlace dp  WHERE dp.suitableForBoth > 0 AND " + DISTANCE_FORMULA + " < :radius  And dp.isFeatured=true AND dp.placeApprovalStatus= 'APPROVED'" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findFeaturedByBothGenderAndLocation(Pageable pageable, @Param("latitude") Double latitude,
                                                  @Param("longitude") Double longitude,
                                                  @Param("radius") Double radius);
    @Query("SELECT dp FROM DatingPlace dp  WHERE dp.suitableForFemale > 0 AND " + DISTANCE_FORMULA + " < :radius  And dp.isFeatured=true AND dp.placeApprovalStatus= 'APPROVED'" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findFeaturedByFemaleGenderAndLocation(Pageable pageable, @Param("latitude") Double latitude,
                                                    @Param("longitude") Double longitude,
                                                    @Param("radius") Double radius);
    @Query("SELECT dp FROM DatingPlace dp  WHERE dp.suitableForMaleCount > 0 AND " + DISTANCE_FORMULA + " < :radius  And dp.isFeatured=true AND dp.placeApprovalStatus= 'APPROVED'" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findFeaturedByMaleGenderAndLocation(Pageable pageable, @Param("latitude") Double latitude,
                                                  @Param("longitude") Double longitude,
                                                  @Param("radius") Double radius);


    @Query("SELECT dp FROM DatingPlace dp  WHERE  dp.placeApprovalStatus= 'APPROVED' And dp.isFeatured=true "  +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findFeaturedPlacesByAllGendersDesc(Pageable pageable);

    @Query("SELECT dp FROM DatingPlace dp  WHERE dp.suitableForMaleCount > 0 And dp.isFeatured=true  AND dp.placeApprovalStatus= 'APPROVED'" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findFeaturedPlacesByMaleGendersDesc(Pageable pageable);
    @Query("SELECT dp FROM DatingPlace dp  WHERE dp.suitableForFemale > 0 And dp.isFeatured=true  AND dp.placeApprovalStatus= 'APPROVED'" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findFeaturedPlacesByFemaleGendersDesc(Pageable pageable);

    @Query("SELECT dp FROM DatingPlace dp  WHERE dp.suitableForBoth > 0 And dp.isFeatured=true  AND dp.placeApprovalStatus= 'APPROVED'" +
            " ORDER BY dp.averageRating DESC")
    Page<DatingPlace> findFeaturedPlacesByBothGendersDesc(Pageable pageable);
}
