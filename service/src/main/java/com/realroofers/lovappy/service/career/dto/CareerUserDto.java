package com.realroofers.lovappy.service.career.dto;

import com.realroofers.lovappy.service.career.model.CareerUser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;
import java.util.StringJoiner;

public class CareerUserDto implements Serializable {
    private long id;

    private String email;
    private String firstName;
    private String lastName;
    private String description;
    private String country;
    private String state;
    private String city;
    private String phoneNumber;
    private boolean authorizedInUs;
    private int zipCode;
    private int gender;
    private String address1;
    private String address2;
    private int ethnicity;
    private int birthYear;
    private int birthMonth;
    private int birthDate;
    private boolean agree;
    private String hobbies;
    private ArrayList<EducationDto> education;
    private ArrayList<EmploymentDto> employment;

    public ArrayList<EducationDto> getEducation() {
        return education;
    }

    public void setEducation(ArrayList<EducationDto> education) {
        this.education = education;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    public CareerUserDto(CareerUser user) {
        this.address1 = user.getAddress1();
        this.address2 = user.getAddress2();
        this.agree = user.isAgree();
        this.birthDate = user.getBirthDate();
        this.birthMonth = user.getBirthMonth();
        this.birthYear = user.getBirthYear();
        this.authorizedInUs = user.isAuthorizedInUs();
        this.city = user.getCity();
        this.state = user.getState();
        this.country = user.getCountry();
        this.id = user.getId();
        this.zipCode = user.getZipCode();
        this.gender = user.getGender();
        this.ethnicity = user.getEthnicity();
        this.phoneNumber = user.getPhoneNumber();
        this.description = user.getDescription();
        this.lastName = user.getLastName();
        this.firstName = user.getFirstName();
        this.email = user.getEmail();
        this.hobbies = user.getHobbies();
    }

    public ArrayList<EmploymentDto> getEmployment() {
        return employment;
    }

    public void setEmployment(ArrayList<EmploymentDto> employment) {
        this.employment = employment;
    }

    public CareerUserDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isAuthorizedInUs() {
        return authorizedInUs;
    }

    public void setAuthorizedInUs(boolean authorizedInUs) {
        this.authorizedInUs = authorizedInUs;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public int getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(int ethnicity) {
        this.ethnicity = ethnicity;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public int getBirthMonth() {
        return birthMonth;
    }

    public void setBirthMonth(int birthMonth) {
        this.birthMonth = birthMonth;
    }

    public int getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(int birthDate) {
        this.birthDate = birthDate;
    }

    public boolean isAgree() {
        return agree;
    }

    public void setAgree(boolean agree) {
        this.agree = agree;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CareerUserDto.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("email='" + email + "'")
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("description='" + description + "'")
                .add("country='" + country + "'")
                .add("state='" + state + "'")
                .add("city='" + city + "'")
                .add("phoneNumber='" + phoneNumber + "'")
                .add("authorizedInUs=" + authorizedInUs)
                .add("zipCode=" + zipCode)
                .add("gender=" + gender)
                .add("address1='" + address1 + "'")
                .add("address2='" + address2 + "'")
                .add("ethnicity=" + ethnicity)
                .add("birthYear=" + birthYear)
                .add("birthMonth=" + birthMonth)
                .add("birthDate=" + birthDate)
                .add("agree=" + agree)
                .add("hobbies='" + hobbies + "'")
                .add("education=" + education)
                .toString();
    }
}
