package com.realroofers.lovappy.service.career.impl;

import com.realroofers.lovappy.service.career.CareerUserService;
import com.realroofers.lovappy.service.career.EducationService;
import com.realroofers.lovappy.service.career.EmploymentService;
import com.realroofers.lovappy.service.career.dto.CareerUserDto;
import com.realroofers.lovappy.service.career.model.CareerUser;
import com.realroofers.lovappy.service.career.model.Vacancy;
import com.realroofers.lovappy.service.career.repo.CareerUserRepo;
import com.realroofers.lovappy.service.career.repo.VacanyRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class CareerUserImpl implements CareerUserService {

    @Autowired
    private CareerUserRepo careerUserRepo;
    @Autowired
    private VacanyRepo vacanyRepo;

    @Autowired
    private EducationService educationService;

    @Autowired
    private EmploymentService employmentService;

    public CareerUserRepo getCareerUserRepo() {
        return careerUserRepo;
    }

    public void setCareerUserRepo(CareerUserRepo careerUserRepo) {
        this.careerUserRepo = careerUserRepo;
    }

    public VacanyRepo getVacanyRepo() {
        return vacanyRepo;
    }

    public void setVacanyRepo(VacanyRepo vacanyRepo) {
        this.vacanyRepo = vacanyRepo;
    }

    @Override
    public String addUser(CareerUserDto user, long vacancyId) {
        String status;
        CareerUser careerUser = new CareerUser(user);
        Vacancy vacancy = vacanyRepo.findOne(vacancyId);
        Set<Vacancy> vacancies = careerUser.getVacancies();
        vacancies.add(vacancy);
        careerUser.setVacancies(vacancies);
;
        try {
            CareerUser savedCareerUser = careerUserRepo.save(careerUser);
            try{
                educationService.addEducations(savedCareerUser.getEducations());

                try{
                    employmentService.addEmployments(savedCareerUser.getEmployments());

                } catch (Exception e){
                    System.out.println(e.getStackTrace());
                    status = "Error Occured While Saving employments";
                    return status;
                }

            } catch (Exception e){
                System.out.println(e.getStackTrace());
                status = "Error Occured While Saving education";
                return status;
            }

            status = "SuccessFully Saved";

        } catch (Exception e){
            System.out.println(e.getStackTrace());
            status = "Error Occured While Saving";
        }
        System.out.println(status);
        return status;
    }

    @Override
    public CareerUserDto getUserById(long id) {
        return null;
    }

    @Override
    public CareerUserDto searchUserByEmail(String email) {
        return null;
    }

    @Override
    public List<CareerUserDto> getAllUsers() {
        return null;
    }

    @Override
    public String editUser(CareerUserDto user) {
        return null;
    }
}
