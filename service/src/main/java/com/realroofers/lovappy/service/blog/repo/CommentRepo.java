package com.realroofers.lovappy.service.blog.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.realroofers.lovappy.service.blog.model.Comment;

public interface CommentRepo extends JpaRepository<Comment, Integer>, QueryDslPredicateExecutor<Comment> {
}
