package com.realroofers.lovappy.service.ads.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
public class AudioFileDto {

    private String name;
    private String url;

    public AudioFileDto(String name, String url) {
        this.name = name;
        this.url = url;
    }
}
