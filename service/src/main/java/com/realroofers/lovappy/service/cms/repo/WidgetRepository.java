package com.realroofers.lovappy.service.cms.repo;

import com.realroofers.lovappy.service.cms.dto.ImageWidgetDTO;
import com.realroofers.lovappy.service.cms.dto.TextWidgetDTO;
import com.realroofers.lovappy.service.cms.model.ImageWidget;
import com.realroofers.lovappy.service.cms.model.VideoWidget;
import com.realroofers.lovappy.service.cms.model.Widget;
import com.realroofers.lovappy.service.cms.model.TextWidget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

/**
 * Created by Daoud Shaheen on 6/6/2017.
 */
public interface WidgetRepository extends JpaRepository<Widget, Integer> {

    @Query("SELECT img.url FROM Page p JOIN p.images img WHERE p.tag=:pageTag AND img.type=:imgType")
    List<String> findByPageTagAndImageType(@Param("pageTag") String pageTag, @Param("imgType") String imageType);

    @Query("SELECT img FROM ImageWidget img WHERE img.id=:imgId")
    ImageWidget findByImageId(@Param("imgId") Integer id);

    @Query("SELECT video FROM VideoWidget video WHERE video.id=:videoId")
    VideoWidget findByVideoId(@Param("videoId") Integer id);

    @Query("SELECT video FROM VideoWidget video WHERE video.name=:videoTag")
    VideoWidget findByVideoName(@Param("videoTag") String tag);

    @Query("SELECT txt FROM TextWidget txt WHERE txt.id=:txtId")
    TextWidget findByTextId(@Param("txtId") Integer id);

    @Query("SELECT txt FROM TextWidget txt WHERE txt.name=:txtTag")
    TextWidget findByTextName(@Param("txtTag") String tag);

    @Query("SELECT img FROM ImageWidget img WHERE img.name=:imageTag")
    ImageWidget findByImageName(@Param("imageTag") String tag);

    @Query("SELECT NEW com.realroofers.lovappy.service.cms.dto.ImageWidgetDTO(img.id, img.name, img.url, img.type) FROM Page p JOIN p.images img WHERE p.id=:pageId ")
    Set<ImageWidgetDTO> findImagesByPageId(@Param("pageId") Integer pageId);

    @Query("SELECT NEW com.realroofers.lovappy.service.cms.dto.ImageWidgetDTO(img.id, img.name, img.url, img.type) FROM Page p JOIN p.images img WHERE p.id=:pageId AND img.type=:imgType ")
    Set<ImageWidgetDTO> findImagesByPageIdAndType(@Param("pageId") Integer pageId, @Param("imgType") String imageType);

    @Query("SELECT NEW com.realroofers.lovappy.service.cms.dto.TextWidgetDTO(txt.id, tr.content, tr.language, txt.name) FROM Page p JOIN p.textContents txt JOIN txt.translations tr  WHERE p.id=:pageId AND tr.language=:language ")
    Set<TextWidgetDTO> findTextsByPageIdAndLanguage(@Param("pageId") Integer pageId, @Param("language")String language);


}
