package com.realroofers.lovappy.service.radio.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by Manoj on 21/12/2017.
 */
@Data
@ToString
@EqualsAndHashCode
public class TimeSlotDto {

    private String startTime;
    private String endTime;

}
