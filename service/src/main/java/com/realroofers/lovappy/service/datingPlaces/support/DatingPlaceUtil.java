package com.realroofers.lovappy.service.datingPlaces.support;

import com.realroofers.lovappy.service.datingPlaces.dto.DatingPlaceDto;
import com.realroofers.lovappy.service.datingPlaces.dto.DatingPlaceReviewDto;
import com.realroofers.lovappy.service.datingPlaces.dto.FoodTypeDto;
import com.realroofers.lovappy.service.datingPlaces.dto.PlaceAtmosphereDto;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceAtmospheres;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceFoodTypes;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.validation.ErrorMessage;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Darrel Rayen on 2/7/18.
 */
public class DatingPlaceUtil {

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean isValidateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public static boolean isInteger(String str) {
        int size = str.length();

        for (int i = 0; i < size; i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return size > 0;
    }

    public static String generateRandomId(int count) {
        String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    public static Double calculateDealFee(Double discount, Double feePercentage) {
        Double fee = discount * feePercentage;
        return fee < 1.99 ? 1.99 : fee;
    }

    public static Comparator<DatingPlaceReviewDto> sortByDateDescending = new Comparator<DatingPlaceReviewDto>() {
        @Override
        public int compare(DatingPlaceReviewDto o1, DatingPlaceReviewDto o2) {
            if (o1.getReviewDate() == o2.getReviewDate())
                return 0;
            else if (o1.getReviewDate().before(o2.getReviewDate()))
                return 1;
            else
                return -1;
        }
    };

    public static String returnEmptyIfNull(String string) {
        return string == null ? "" : string;
    }

    public static HashMap<String, List<ErrorMessage>> isValidDatingPlace(
            DatingPlaceDto datingPlace, Integer locationPictureId, UserService userService) {
        HashMap<String, List<ErrorMessage>> map = new HashMap<>();
        List<ErrorMessage> errorMessages = new ArrayList<>();
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        if (datingPlace.getUserType() == null) {
            errorMessages.add(new ErrorMessage("userType", "Select whether you want to Review Place/Own the Place"));
        }
        if (datingPlace.getPlaceName().isEmpty()) {
            errorMessages.add(new ErrorMessage("Place name", "Provide a Name for the Dating Place"));
        }
        if (userId == null) {
            if (!isValidateEmail(datingPlace.getCreateEmail()) || datingPlace.getCreateEmail().isEmpty()) {
                errorMessages.add(new ErrorMessage("createEmail", "Provide a valid Email Address"));
            } else if (userService.emailExists(datingPlace.getCreateEmail())) {
                errorMessages.add(new ErrorMessage("createEmail", "You are already registered."));
            } else {
                if (datingPlace.getPassword().isEmpty()) {
                    errorMessages.add(new ErrorMessage("password", "Provide a password"));
                }
            }
        }

        if (locationPictureId == null) {
            errorMessages.add(new ErrorMessage("Image", "Upload an image"));
        }
        if (datingPlace.getCity().isEmpty()) {
            errorMessages.add(new ErrorMessage("City", "Provide a city"));
        }
        if (datingPlace.getAddressLine().isEmpty()) {
            errorMessages.add(new ErrorMessage("AddressLine", "Provide an address"));
        }
        if (datingPlace.getState().isEmpty()) {
            errorMessages.add(new ErrorMessage("State", "Provide a state"));
        }
        if (datingPlace.getCountry().isEmpty()) {
            errorMessages.add(new ErrorMessage("Country", "Select a Country"));
        }
        if (datingPlace.getZipCode().isEmpty()) {
            errorMessages.add(new ErrorMessage("Zipcode", "Provide a Zipcode"));
        }
        if (datingPlace.getAgeRange() == null) {
            errorMessages.add(new ErrorMessage("Age Range", "Select an Age Range"));
        }
        if (datingPlace.getGender() == null) {
            errorMessages.add(new ErrorMessage("Gender", "Select Gender"));
        }
        if (datingPlace.getPriceRange() == null) {
            errorMessages.add(new ErrorMessage("Price Range", "Select suitable Price Range"));
        }
        if (datingPlace.getLatitude() == null || datingPlace.getLongitude() == null) {
            errorMessages.add(new ErrorMessage("verify", "Please Verify the location you've entered"));
        }
        map.put("errorMessages", errorMessages);
        return map;
    }

    public static List<PlaceAtmosphereDto> mapToPlaceAtmosphereDto(List<DatingPlaceAtmospheres> datingPlaceAtmospheres) {
        List<PlaceAtmosphereDto> atmospheres = new ArrayList<>();
        datingPlaceAtmospheres.forEach(datingPlaceAtmosphere -> {
            atmospheres.add(new PlaceAtmosphereDto(datingPlaceAtmosphere.getAtmosphere()));
        });
        return atmospheres;
    }

    public static List<FoodTypeDto> mapToFoodTypeDto(List<DatingPlaceFoodTypes> foodTypes) {
        List<FoodTypeDto> foodTypesList= new ArrayList<>();
        foodTypes.forEach(foodType -> {
            foodTypesList.add(new FoodTypeDto(foodType.getFoodType()));
        });
        return foodTypesList;
    }
}
