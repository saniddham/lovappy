/**
 * Package com.realroofers.lovappy.service.user.dto is a package for data transfer objects.
 * DTO is a design pattern. DTO's serves as a medium between front layer (controllers) and business layer (service)
 */
package com.realroofers.lovappy.service.user.dto;