package com.realroofers.lovappy.service.order.support;

/**
 * Created by Eias Altawil on 5/11/17
 */
public enum OrderDetailType {
    GIFT("gift", 1.0), MUSIC("music", 1.99), PRIVATE_MESSAGE("private_message", 1.5), AD("ad", 0.0), EVENT_ATTEND("event_attend", 0.0),
    DATING_PLACE("dating_place", 0.0), CREDITS("credit", 1.5), DAILY_QUESTION("daily_question",  2.5 );

    String text;
    double defaultPrice;

    OrderDetailType(String text, double defaultPrice) {
        this.text = text;
        this.defaultPrice = defaultPrice;

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getDefaultPrice() {
        return defaultPrice;
    }

    public void setDefaultPrice(double defaultPrice) {
        this.defaultPrice = defaultPrice;
    }
}
