package com.realroofers.lovappy.service.datingPlaces.support;

import com.realroofers.lovappy.service.datingPlaces.dto.FoodTypeDto;
import com.realroofers.lovappy.service.datingPlaces.dto.PersonTypeDto;
import com.realroofers.lovappy.service.datingPlaces.dto.PlaceAtmosphereDto;
import com.realroofers.lovappy.service.datingPlaces.model.FoodType;
import com.realroofers.lovappy.service.datingPlaces.model.PersonType;
import com.realroofers.lovappy.service.datingPlaces.model.PlaceAtmosphere;
import com.realroofers.lovappy.service.datingPlaces.repo.FoodTypeRepo;
import com.realroofers.lovappy.service.datingPlaces.repo.PersonTypeRepo;
import com.realroofers.lovappy.service.datingPlaces.repo.PlaceAtmosphereRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darrel Rayen on 12/9/17.
 */
@Service
public class DatingPlaceSupportServiceImpl implements DatingPlaceSupportService {

    private final FoodTypeRepo foodTypeRepo;
    private final PlaceAtmosphereRepo placeAtmosphereRepo;
    private final PersonTypeRepo personTypeRepo;

    @Autowired
    public DatingPlaceSupportServiceImpl(FoodTypeRepo foodTypeRepo, PlaceAtmosphereRepo placeAtmosphereRepo, PersonTypeRepo personTypeRepo) {
        this.foodTypeRepo = foodTypeRepo;
        this.placeAtmosphereRepo = placeAtmosphereRepo;
        this.personTypeRepo = personTypeRepo;
    }

    @Override
    @Transactional(readOnly = true)
    public List<FoodTypeDto> getAllFoodTypesEnabled() {
        List<FoodType> foodTypeList = new ArrayList<>();
        List<FoodTypeDto> foodTypeDtos = new ArrayList<>();
        foodTypeList = foodTypeRepo.findAllByEnabled(true);
        foodTypeList.forEach(foodType -> {
            foodTypeDtos.add(new FoodTypeDto(foodType));
        });
        return foodTypeDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public List<FoodTypeDto> getAllFoodTypes() {
        List<FoodType> foodTypeList = new ArrayList<>();
        List<FoodTypeDto> foodTypeDtos = new ArrayList<>();
        foodTypeList = foodTypeRepo.findAll();
        foodTypeList.forEach(foodType -> {
            foodTypeDtos.add(new FoodTypeDto(foodType));
        });
        return foodTypeDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public FoodTypeDto getFoodTypeById(Integer id) {
        FoodType foodType = foodTypeRepo.findOne(id);
        return new FoodTypeDto(foodType);
    }

    @Override
    @Transactional
    public FoodTypeDto addFoodType(FoodTypeDto foodTypeDto) {
        FoodType foodType = new FoodType();
        foodType.setFoodTypeName(foodTypeDto.getFoodTypeName());
        foodType.setEnabled(false);
        return new FoodTypeDto(foodTypeRepo.save(foodType));
    }

    @Override
    @Transactional
    public FoodTypeDto updateFoodType(FoodTypeDto foodTypeDto) {
        FoodType foodType = foodTypeRepo.getOne(foodTypeDto.getId());
        foodType.setFoodTypeName(foodTypeDto.getFoodTypeName());
        foodType.setEnabled(foodTypeDto.getEnabled());
        return new FoodTypeDto(foodTypeRepo.saveAndFlush(foodType));
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlaceAtmosphereDto> getAllAtmospheresEnabled() {
        List<PlaceAtmosphereDto> placeAtmosphereDtos = new ArrayList<>();
        List<PlaceAtmosphere> placeAtmospheres = placeAtmosphereRepo.findAllByEnabled(true);
        placeAtmospheres.forEach(placeAtmosphere -> {
            placeAtmosphereDtos.add(new PlaceAtmosphereDto(placeAtmosphere));
        });
        return placeAtmosphereDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlaceAtmosphereDto> findAllAtmospheresByPlaceID(Integer id) {
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlaceAtmosphereDto> getAllAtmospheres() {
        List<PlaceAtmosphereDto> placeAtmosphereDtos = new ArrayList<>();
        List<PlaceAtmosphere> placeAtmospheres = placeAtmosphereRepo.findAll();
        placeAtmospheres.forEach(placeAtmosphere -> {
            placeAtmosphereDtos.add(new PlaceAtmosphereDto(placeAtmosphere));
        });
        return placeAtmosphereDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public PlaceAtmosphereDto getAtmosphereById(Integer id) {
        PlaceAtmosphere placeAtmosphere = placeAtmosphereRepo.findOne(id);
        return new PlaceAtmosphereDto(placeAtmosphere);
    }

    @Override
    @Transactional
    public PlaceAtmosphereDto addPlaceAtmosphere(PlaceAtmosphereDto placeAtmosphereDto) {
        PlaceAtmosphere placeAtmosphere = new PlaceAtmosphere();
        placeAtmosphere.setAtmosphereName(placeAtmosphereDto.getAtmosphereName());
        placeAtmosphere.setEnabled(placeAtmosphere.getEnabled());
        return new PlaceAtmosphereDto(placeAtmosphereRepo.save(placeAtmosphere));
    }

    @Override
    @Transactional
    public PlaceAtmosphereDto updatePlaceAtmosphere(PlaceAtmosphereDto placeAtmosphereDto) {
        PlaceAtmosphere placeAtmosphere = placeAtmosphereRepo.findOne(placeAtmosphereDto.getId());
        placeAtmosphere.setAtmosphereName(placeAtmosphereDto.getAtmosphereName());
        placeAtmosphere.setEnabled(placeAtmosphereDto.getEnabled());
        return new PlaceAtmosphereDto(placeAtmosphereRepo.saveAndFlush(placeAtmosphere));
    }

    @Override
    @Transactional
    public PersonTypeDto addPersonType(PersonTypeDto personTypeDto) {
        PersonType personType = new PersonType();
        personType.setPersonTypeName(personTypeDto.getPersonTypeName());
        personType.setEnabled(true);
        return new PersonTypeDto(personTypeRepo.saveAndFlush(personType));
    }

    @Override
    @Transactional
    public PersonTypeDto updatePersonType(PersonTypeDto personTypeDto) {
        PersonType personType = personTypeRepo.findOne(personTypeDto.getId());
        personType.setPersonTypeName(personTypeDto.getPersonTypeName());
        personType.setEnabled(personTypeDto.getEnabled());
        return new PersonTypeDto(personTypeRepo.saveAndFlush(personType));
    }

    @Override
    @Transactional(readOnly = true)
    public List<PersonTypeDto> getAllPersonTypes() {
        List<PersonType> personTypeList = personTypeRepo.findAll();
        List<PersonTypeDto> personTypeDtos = new ArrayList<>();
        personTypeList.forEach(personType -> {
            personTypeDtos.add(new PersonTypeDto(personType));
        });
        return personTypeDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PersonTypeDto> getAllPersonTypesEnabled() {
        List<PersonType> personTypeList = personTypeRepo.findAllByEnabledTrue();
        List<PersonTypeDto> personTypeDtos = new ArrayList<>();
        personTypeList.forEach(personType -> {
            personTypeDtos.add(new PersonTypeDto(personType));
        });
        return personTypeDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public PersonTypeDto getPersonTypeById(Integer id) {
        PersonType personType = personTypeRepo.findOne(id);
        return new PersonTypeDto(personType);
    }
}
