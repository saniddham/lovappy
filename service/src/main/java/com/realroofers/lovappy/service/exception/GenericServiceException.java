package com.realroofers.lovappy.service.exception;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public class GenericServiceException extends RuntimeException {
    public GenericServiceException(Throwable cause) {
        super(cause);
    }

    public GenericServiceException(String message) {
        super(message);
    }
}
