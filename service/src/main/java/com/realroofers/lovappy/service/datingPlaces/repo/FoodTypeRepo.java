package com.realroofers.lovappy.service.datingPlaces.repo;

import com.realroofers.lovappy.service.datingPlaces.model.DatingPlacePayment;
import com.realroofers.lovappy.service.datingPlaces.model.FoodType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Darrel Rayen on 10/19/17.
 */
@Repository
public interface FoodTypeRepo extends JpaRepository<FoodType, Integer> {

    FoodType findByFoodTypeName(String foodName);
    FoodType findByFoodTypeNameAndEnabledTrue(String foodName);
    List<FoodType> findAllByEnabled(Boolean enabled);
}
