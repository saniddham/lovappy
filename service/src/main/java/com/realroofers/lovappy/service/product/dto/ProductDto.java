package com.realroofers.lovappy.service.product.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.product.support.ProductAvailability;
import com.realroofers.lovappy.service.product.support.Upload;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.vendor.model.VendorLocation;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode
public class ProductDto {

    private Integer id;
    @NotNull(message = "Product name cannot be empty.", groups = {Upload.class})
    private String productName;
    @NotNull(message = "Product code cannot be empty.", groups = {Upload.class})
    private String productCode;
    private String productDescription;
    @NotNull(message = "Product price cannot be empty.", groups = {Upload.class})
    private BigDecimal price;
    private ProductAvailability productAvailability;
    @NotNull(message = "Product category cannot be empty.", groups = {Upload.class})
    private ProductTypeDto productType;
    private BrandDto brand;

    @NotNull(message = "Main image cannot be empty.", groups = {Upload.class})
    private CloudStorageFileDto image1 = new CloudStorageFileDto();
    @NotNull(message = "Second image cannot be empty.", groups = {Upload.class})
    private CloudStorageFileDto image2 = new CloudStorageFileDto();
    @NotNull(message = "Third image cannot be empty.", groups = {Upload.class})
    private CloudStorageFileDto image3 = new CloudStorageFileDto();
    private String productEan;
    private BigDecimal commissionEarned;

    private Boolean active = true;
    private Boolean approved = false;
    private Boolean blocked = false;

    private UserDto approvedBy;
    private UserDto blockedBy;
    private UserDto createdBy;
    private UserDto updatedBy;
    private Date updated;
    private List<VendorLocation> vendorLocations = new ArrayList<>();

    public ProductDto() {
    }

    public ProductDto(Product product) {
        if (product.getId() != null) {
            this.id = product.getId();
        }
        this.productName = product.getProductName();
        this.productCode = product.getProductCode();
        this.productDescription = product.getProductDescription();
        this.price = product.getPrice();
        this.productAvailability = product.getProductAvailability();

        this.productType = product.getProductType() != null ? new ProductTypeDto(product.getProductType()) : new ProductTypeDto();
        this.brand = product.getBrand() != null ? new BrandDto(product.getBrand()) : new BrandDto();
        this.image1 = new CloudStorageFileDto(product.getImage1());
        this.image2 = new CloudStorageFileDto(product.getImage2());
        this.image3 = new CloudStorageFileDto(product.getImage3());
        this.productEan = product.getProductEan();
        this.commissionEarned = product.getCommissionEarned();
        this.active = product.getActive() != null ? product.getActive() : true;
        this.approved = product.getApproved() != null ? product.getApproved() : false;
        this.blocked = product.getBlocked() != null ? product.getBlocked() : false;

        this.approvedBy = product.getApprovedBy() != null ? new UserDto(product.getApprovedBy()) : null;
        this.blockedBy = product.getBlockedBy() != null ? new UserDto(product.getBlockedBy()) : null;
        this.createdBy = product.getCreatedBy() != null ? new UserDto(product.getCreatedBy()) : null;
        this.updatedBy = product.getUpdatedBy() != null ? new UserDto(product.getUpdatedBy()) : null;
        this.updated = product.getUpdated();
    }

    public ProductDto(ProductRegDto product) {
        if (product.getId() != null) {
            this.id = product.getId();
        }
        this.productName = product.getProductName();
        this.productCode = product.getProductCode();
        this.productDescription = product.getProductDescription();
        this.price = product.getPrice();
        this.productAvailability = ProductAvailability.IN_STOCK;

        if(product.getProductType()!=null) {
            ProductTypeDto productTypeDto = new ProductTypeDto();
            productTypeDto.setId(product.getProductType());
            this.productType = productTypeDto;
        }

        if (product.getBrand() != null) {
            BrandDto brandDto = new BrandDto();
            brandDto.setId(product.getBrand());
            this.brand = brandDto;
        }
        if (product.getImage1()!=null) {
            CloudStorageFile cloudStorageFile=new CloudStorageFile();
            cloudStorageFile.setId(product.getImage1());
            this.image1 = new CloudStorageFileDto(cloudStorageFile);
        }
        if (product.getImage2()!=null) {
            CloudStorageFile cloudStorageFile=new CloudStorageFile();
            cloudStorageFile.setId(product.getImage2());
            this.image1 = new CloudStorageFileDto(cloudStorageFile);
        }
        if (product.getImage3()!=null) {
            CloudStorageFile cloudStorageFile=new CloudStorageFile();
            cloudStorageFile.setId(product.getImage3());
            this.image1 = new CloudStorageFileDto(cloudStorageFile);
        }
        this.productEan = product.getProductEan();
        this.commissionEarned = new BigDecimal(0);
        this.active = true;
        this.approved = false;
        this.blocked = false;
    }
}
