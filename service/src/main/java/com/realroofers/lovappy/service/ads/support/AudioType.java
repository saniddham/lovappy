package com.realroofers.lovappy.service.ads.support;

public enum AudioType {
    UPLOAD, RECORD
}
