package com.realroofers.lovappy.service.credits;

import com.realroofers.lovappy.service.credits.dto.CreditDto;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.CouponDto;

/**
 * Created by Daoud Shaheen on 8/10/2018.
 */
public interface CreditService {
    CreditDto addCredit(Integer userId, Integer balance);
    CreditDto sendUser(Integer fromUserId, Integer toUser, Integer balance);
    CreditDto chargeByCredit(Integer fromUserId, Integer toUser, Integer balance);

    CreditDto getCredit(Integer userId);
    CreditDto getCreditByToUser(Integer fromUserId, Integer toUser);
    CalculatePriceDto calculatePrice(CouponDto coupon, Integer quantity);

}
