package com.realroofers.lovappy.service.user.repo;

import com.realroofers.lovappy.service.core.SocialType;
import com.realroofers.lovappy.service.user.model.SocialProfile;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Daoud Shaheen on 3/3/2018.
 */
public interface SocialProfileRepo extends JpaRepository<SocialProfile, Integer>{

    SocialProfile findBySocialIdAndSocialPlatform(String socialId, SocialType socialPlatform);
}
