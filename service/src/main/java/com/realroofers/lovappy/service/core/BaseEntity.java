package com.realroofers.lovappy.service.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import javax.persistence.*;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 6/7/2017.
 */
@MappedSuperclass
public class BaseEntity <ID> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private ID id;

    @JsonIgnore
    @CreationTimestamp
    @Column(name = "created", insertable = false, updatable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date created;

    @JsonIgnore
    //@UpdateTimestamp
    private Date updated;

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @PrePersist
    public void onPrePersist() {
     setCreated(new Date());
    }

    @PreUpdate
    public void onPreUpdate() {
        setUpdated(new Date());
    }
}
