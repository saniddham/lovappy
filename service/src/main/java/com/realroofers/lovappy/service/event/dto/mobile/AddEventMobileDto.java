package com.realroofers.lovappy.service.event.dto.mobile;

import com.realroofers.lovappy.service.event.dto.EventCategoryDto;
import com.realroofers.lovappy.service.event.dto.EventLocationDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Darrel Rayen on 4/5/18.
 */
@Data
@EqualsAndHashCode
public class AddEventMobileDto implements Serializable {
    private Integer eventId;
    private String eventTitle;
    private String eventDescription;
    private Date eventDate;
    private String startTime;
    private String finishTime;
    private EventLocationDto eventLocation;
    private Integer outsiderCount;
    private Double admissionCost;
    private String timeZone;
    private Integer ambassadorUserId;
    private Map<String, String> targetedAudience;
    private List<EventCategoryDto> eventCategoryDtos;
}
