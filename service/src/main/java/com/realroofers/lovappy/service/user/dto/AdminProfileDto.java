package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Daoud Shaheen on 6/25/2017.
 */
@EqualsAndHashCode
public class AdminProfileDto {

    private Integer userId;
    @NotEmpty(message = "You should provide your email address.")
    @NotNull
    @Email(message = "This is not a valid email address.")
    private String email;

    private String password;

    @NotNull
    @NotEmpty(message = "firstName cannot be empty.")
    private String firstName;

    @NotNull
    @NotEmpty(message = "lastName cannot be empty.")
    private String lastName;

    private CloudStorageFileDto profileUrl;

    public AdminProfileDto() {
        profileUrl = new CloudStorageFileDto();
    }
    public AdminProfileDto(User user) {
        this.userId = user.getUserId();
        this.email = user.getEmail();
        this.password = user.getPassword();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.profileUrl = new CloudStorageFileDto(user.getUserProfile().getProfilePhotoCloudFile());
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public CloudStorageFileDto getProfileUrl() {
        return  profileUrl;
    }

    public void setProfileUrl(CloudStorageFileDto profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
