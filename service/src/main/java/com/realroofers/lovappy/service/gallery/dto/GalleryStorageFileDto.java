package com.realroofers.lovappy.service.gallery.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.realroofers.lovappy.service.gallery.model.GalleryStorageFile;
import com.realroofers.lovappy.service.user.model.embeddable.Address;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import lombok.EqualsAndHashCode;

@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode
public class GalleryStorageFileDto {
	  private Integer id;
	    private String name;
	    private String url;
	    private String bucket;
		private String photoCaption;
		private String zipCode;
		private Address address;
	     private ApprovalStatus status;
	    public GalleryStorageFileDto(GalleryStorageFile galleryStorageFile, Address address){
	        if(galleryStorageFile != null) {
	            this.id = galleryStorageFile.getId();
	            this.name = galleryStorageFile.getName();
	            this.url = galleryStorageFile.getUrl();
	            this.bucket = galleryStorageFile.getBucket();
	            this.photoCaption = galleryStorageFile.getPhotoCaption();
	            this.address = address;
	            this.status = galleryStorageFile.getStatus();
	        }
	    }

	public GalleryStorageFileDto(GalleryStorageFile galleryStorageFile){
		if(galleryStorageFile != null) {
			this.id = galleryStorageFile.getId();
			this.name = galleryStorageFile.getName();
			this.url = galleryStorageFile.getUrl();
			this.bucket = galleryStorageFile.getBucket();
			this.photoCaption = galleryStorageFile.getPhotoCaption();
			this.address = galleryStorageFile.getAddress();
			this.status = galleryStorageFile.getStatus();
		}
	}

	    public GalleryStorageFileDto(String name, String url, String bucket, String photoCaption, String zipCode, Address address) {
	        this.name = name;
	        this.url = url;
	        this.bucket = bucket;
	        this.photoCaption = photoCaption;
	        this.zipCode =  zipCode;
	        this.address = address;
	    }

	    public GalleryStorageFileDto() {
		}

	    public Integer getId() {
	        return id;
	    }

	    public void setId(Integer id) {
	        this.id = id;
	    }

	    public String getName() {
	        return name;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }

	    public String getUrl() {
	        return url;
	    }

	    public void setUrl(String url) {
	        this.url = url;
	    }

	    public String getBucket() {
	        return bucket;
	    }

	    public void setBucket(String bucket) {
	        this.bucket = bucket;
	    }

		public String getPhotoCaption() {
			return photoCaption;
		}

		public void setPhotoCaption(String photoCaption) {
			this.photoCaption = photoCaption;
		}

		public String getZipCode() {
			return zipCode;
		}

		public void setZipCode(String zipCode) {
			this.zipCode = zipCode;
		}

		public Address getAddress() {
			return address;
		}

		public void setAddress(Address address) {
			this.address = address;
		}

	public ApprovalStatus getStatus() {
		return status;
	}

	public void setStatus(ApprovalStatus status) {
		this.status = status;
	}
}
