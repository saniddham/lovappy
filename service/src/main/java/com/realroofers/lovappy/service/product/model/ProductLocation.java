package com.realroofers.lovappy.service.product.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.product.dto.ProductLocationDto;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.vendor.model.VendorLocation;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@EqualsAndHashCode
@Table(name = "product_location")
public class ProductLocation extends BaseEntity<Integer> implements Serializable {


    @ManyToOne
    @JoinColumn(name = "fk_location")
    private VendorLocation location;

    @ManyToOne
    @JoinColumn(name = "fk_product")
    private Product product;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active;

    @JsonIgnore
    @JoinColumn(name = "created_by")
    @ManyToOne
    private User createdBy;

    public ProductLocation() {
    }

    public ProductLocation(ProductLocationDto productLocationDto) {
        super();
        if (productLocationDto.getId() != null) {
            this.setId(productLocationDto.getId());
        }
        if(productLocationDto.getLocation()!=null) {
            this.location = productLocationDto.getLocation();
        }
        if(productLocationDto.getProduct()!=null) {
            this.product = new Product(productLocationDto.getProduct());
        }
        this.active = productLocationDto.getActive();
        if(productLocationDto.getCreatedBy()!=null) {
            this.createdBy = new User(productLocationDto.getCreatedBy().getID());
        }
    }
}
