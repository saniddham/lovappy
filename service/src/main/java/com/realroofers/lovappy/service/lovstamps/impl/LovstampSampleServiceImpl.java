package com.realroofers.lovappy.service.lovstamps.impl;

import com.realroofers.lovappy.service.cloud.repo.CloudStorageRepo;
import com.realroofers.lovappy.service.lovstamps.LovstampSampleService;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampSampleDto;
import com.realroofers.lovappy.service.lovstamps.model.LovstampSample;
import com.realroofers.lovappy.service.lovstamps.repo.LovstampSampleRepo;
import com.realroofers.lovappy.service.lovstamps.support.SampleTypes;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.system.repo.LanguageRepo;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.util.ListMapper;
import org.apache.commons.codec.language.bm.Lang;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Daoud Shaheen on 12/31/2017.
 */
@Service
public class LovstampSampleServiceImpl implements LovstampSampleService {

    private final LovstampSampleRepo lovstampSampleRepo;
    private final LanguageRepo languageRepo;
    private final CloudStorageRepo cloudStorageRepo;

    public LovstampSampleServiceImpl(LovstampSampleRepo lovstampSampleRepo, LanguageRepo languageRepo, CloudStorageRepo cloudStorageRepo) {
        this.lovstampSampleRepo = lovstampSampleRepo;
        this.languageRepo = languageRepo;
        this.cloudStorageRepo = cloudStorageRepo;
    }

    @Transactional
    @Override
    public LovstampSampleDto saveRecordingSample(Integer id, Long audioFileCloudID, Integer languageID, Gender gender, SampleTypes type) {
        LovstampSample lovstampSample = new LovstampSample();

        if(id != null)
            lovstampSample = lovstampSampleRepo.findOne(id);

        if(lovstampSample != null){

            if(audioFileCloudID != null)
                lovstampSample.setAudioFileCloud(cloudStorageRepo.findOne(audioFileCloudID));

            lovstampSample.setLanguage(languageRepo.findOne(languageID));
            lovstampSample.setGender(gender);
            lovstampSample.setType(type);
            LovstampSample save = lovstampSampleRepo.save(lovstampSample);
            return save == null ? null : new LovstampSampleDto(save);
        }

        return null;
    }

    @Transactional
    @Override
    public LovstampSampleDto saveDefaultSample(Integer duration, Long audioFileCloudID, Gender gender, String langAbbrev) {
        Language language = languageRepo.findByAbbreviation(langAbbrev);
        Page<LovstampSample> lovstampSamples = lovstampSampleRepo.findByLanguageAndGenderAndType(language, gender, SampleTypes.DEFAULT, new PageRequest(0, 1));
        if(lovstampSamples.getContent().size() >  0){

            LovstampSample lovstampSample = lovstampSamples.getContent().get(0);
            if(audioFileCloudID != null) {
                lovstampSample.setAudioFileCloud(cloudStorageRepo.findOne(audioFileCloudID));
                lovstampSample.setDuration(duration);
            }

            lovstampSample.setLanguage(language);
            lovstampSample.setGender(gender);
            lovstampSample.setType(SampleTypes.DEFAULT);
            LovstampSample save = lovstampSampleRepo.save(lovstampSample);
            return save == null ? null : new LovstampSampleDto(save);
        }

        return null;
    }

    @Transactional(readOnly = true)
    @Override
    public Page<LovstampSampleDto> getAllRecordingSamplesAndType(SampleTypes type, Pageable pageable) {
        return lovstampSampleRepo.findByType(type, pageable).map(LovstampSampleDto::new);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<LovstampSampleDto> getLovstampSamplesByLanguageAndGenderAndType(Integer languageID, Gender gender, SampleTypes type, Pageable pageable) {
           return lovstampSampleRepo.findByLanguageAndGenderAndType(languageRepo.findOne(languageID), gender, type, pageable).map(LovstampSampleDto::new);
    }
    @Transactional(readOnly = true)
    @Override
    public Page<LovstampSampleDto> getLovstampSamplesByLanguageAndGenderAndType(String languageCode, Gender gender, SampleTypes type, Pageable pageable) {
        return lovstampSampleRepo.findByLanguageAndGenderAndType(languageRepo.findByAbbreviation(languageCode), gender, type, pageable).map(LovstampSampleDto::new);
    }

    @Transactional(readOnly = true)
    @Override
    public LovstampSampleDto getRecordingSample(Integer id) {
        LovstampSample lovstampSample = lovstampSampleRepo.findOne(id);
        return lovstampSample == null ? null : new LovstampSampleDto(lovstampSample);
    }

}
