package com.realroofers.lovappy.service.datingPlaces.repo;

import com.realroofers.lovappy.service.datingPlaces.model.PlacesAttachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Darrel Rayen on 10/19/17.
 */
@Repository
public interface PlacesAttachmentRepo extends JpaRepository<PlacesAttachment,Integer> {
}
