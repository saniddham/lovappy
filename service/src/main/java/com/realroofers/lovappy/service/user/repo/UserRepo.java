package com.realroofers.lovappy.service.user.repo;

import com.realroofers.lovappy.service.lovstamps.model.Lovstamp;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.dto.UserReviewDto;
import com.realroofers.lovappy.service.user.model.Role;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Repository
public interface UserRepo extends JpaRepository<User, Integer>, QueryDslPredicateExecutor<User>{

    User findOneByEmail(String email);
    User findOneByUserId(Integer  userId);
    User findByEmailVerificationKey(String emailVerificationKey);

    @Query( "select o from User o JOIN o.roles r where r.id.role in :roles" )
    Page<User> findByRolesIn(@Param("roles") Set<Role> roles, Pageable pageRequest);

    User findByListenLovstampSetIn(Lovstamp lovstamp);

    @Query(nativeQuery = true, value =
            "Select u.*  From user u Join " +
                    "(SELECT count(*) as counter, pm.from_user as from_user,pm.to_user as to_user , max(pm.id) as lastId FROM private_message pm " +
                    "Where pm.from_user=:userId Or pm.to_user = :userId group by pm.from_user,pm.to_user\n" +
                    " order by counter desc) T On u.user_id= T.to_user or u.user_id= T.from_user Where u.user_id!=:userId  group by u.user_id having max(T.lastId) order by  sum(T.counter)desc ,max(T.lastId) desc limit 1; ")
    User findMostUserExchangeMessageWithByUserId(@Param("userId") Integer userId);

    @Query( "select distinct o from User o JOIN o.roles r where r.id.role in :roles AND r.approvalStatus in :approvalStatus" )
    Page<User> findDistinctByRolesInAndActivated(@Param("roles") Set<Role> role, @Param("approvalStatus") List<ApprovalStatus> approvalStatus, Pageable pageRequest);


    @Query( "select o from User o JOIN o.roles r where o.userId =:userId AND r.id.role in :roles" )
    User findByUserIdAndRolesIn(@Param("userId") Integer id, @Param("roles") Set<Role> roles);

    @Query(nativeQuery = true, value = "Select us.* From  users_likes u2 JOIN (SELECT u.like_to FROM users_likes u Where u.like_by=:userId) T on T.like_to = u2.like_by Join user us on us.user_id = u2.like_by Where u2.like_to=:userId  Order by created desc limit :page, :limit ;")
    List<User> findMatches(@Param("userId") int userId, @Param("page") int page, @Param("limit") int limit);

    @Query(nativeQuery = true, value = "Select Count(*) From  users_likes u2 JOIN (SELECT u.like_to FROM users_likes u Where u.like_by=:userId) T on T.like_to = u2.like_by Join user us on us.user_id = u2.like_by Where u2.like_to=:userId ;")
    Integer countNumberOFmatches(@Param("userId") int userId);

    @Query("SELECT u.id.likeTo FROM com.realroofers.lovappy.service.likes.model.Like u  WHERE u.id.likeBy=:likeBy")
    Page<User> findByIdLikeBy(@Param("likeBy") User likeBy, Pageable pageable);

    @Query("SELECT u.id.likeBy FROM com.realroofers.lovappy.service.likes.model.Like u  WHERE u.id.likeTo=:likeTo")
    Page<User> findByIdLikeTo(@Param("likeTo") User likeTo, Pageable pageable);

    User findByUserIdAndListenLovstampSetIn(Integer userId, Set<Lovstamp> lovstamps);

    @Query("SELECT u.id.likeTo.userId FROM com.realroofers.lovappy.service.likes.model.Like u  WHERE u.id.likeBy=:likeBy")
    List<Integer> findLikeIdsByIdLikeBy(@Param("likeBy") User likeBy);

    @Query("SELECT u From User u Where u.lastLogin > :from AND u.lastLogin < :to AND u.email like %:email%")
    Page<User> findByLastLoginBetweenAndEmailLike(@Param("from") Date from, @Param("to") Date to, @Param("email") String email,  Pageable pageable);

    Page<User> findByLastLoginBetween(Date from, Date to, Pageable pageable);

    @Query(nativeQuery = true, value = "SELECT r.name FROM role r INNER JOIN users_roles ur ON r.id = ur.role_id INNER JOIN user u ON u.user_id = ur.user_id AND u.user_id =:userId ;")
    List<String> findUserRolesforUserId(@Param("userId") Integer userId);

    Integer countByRolesIdRoleInAndCreatedOnBetween(Set<Role> role, Date start, Date end);

    @Query("SELECT u From User u JOIN u.roles r  Where r.id.role in :roles AND u.email like %:email%")
    Page<User> findByRoleInAndLikeEmail(@Param("roles") Set<Role> roles, @Param("email") String email, Pageable pageable);
    @Query("SELECT  u From User u JOIN u.roles r  Where r.id.role in :roles ")
    Page<User> findByRoleIn(@Param("roles")  Set<Role> roles, Pageable pageable);
//    @Query("Select new com.realroofers.lovappy.service.user.dto.UserReviewDto(u, l) from User u  left join Lovstamp l on u.userId=l.user.userId WHERE  u.email like %:email% ")
//     Page<UserReviewDto> findAllUsersWithLovstamp(@Param("email") String email, Pageable pageable);
}
