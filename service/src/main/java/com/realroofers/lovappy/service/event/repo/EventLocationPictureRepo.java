package com.realroofers.lovappy.service.event.repo;

import com.realroofers.lovappy.service.event.model.Event;
import com.realroofers.lovappy.service.event.model.EventComment;
import com.realroofers.lovappy.service.event.model.EventLocationPicture;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by darrel on 8/31/17.
 */

public interface EventLocationPictureRepo extends JpaRepository<EventLocationPicture,Integer> {

    List<EventLocationPicture> findByEventId(Event event);
}
