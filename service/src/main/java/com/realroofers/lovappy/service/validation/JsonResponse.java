package com.realroofers.lovappy.service.validation;

import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Created by Eias Altawil on 5/26/2017
 */
@EqualsAndHashCode
public class JsonResponse {

    private ResponseStatus status;
    private Object result;
    private List<ErrorMessage> errorMessageList;
    private String cause;

    public JsonResponse(){
    }

    public JsonResponse(ResponseStatus status, Object result, List<ErrorMessage> errorMessageList, String cause){
        this.status = status;
        this.result = result;
        this.errorMessageList = errorMessageList;
        this.cause = cause;
    }

    public ResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public List<ErrorMessage> getErrorMessageList() {
        return this.errorMessageList;
    }

    public void setErrorMessageList(List<ErrorMessage> errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String toString(){
        return "{\"status\":\"" + status + "\",\"cause\":\"" + cause + "\"}";
    }
}
