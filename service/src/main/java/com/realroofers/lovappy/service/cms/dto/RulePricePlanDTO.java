package com.realroofers.lovappy.service.cms.dto;

import lombok.EqualsAndHashCode;

/**
 * Created by Daoud Shaheen on 7/31/2017.
 */
@EqualsAndHashCode
public class RulePricePlanDTO implements Comparable<RulePricePlanDTO> {

    private String name;
    private String lite;
    private String prime;
    private Integer index;

    public RulePricePlanDTO() {
    }

    public RulePricePlanDTO(String name, String lite, String prime, Integer index) {
        this.lite = lite;
        this.prime = prime;
        this.index = index;
        this.name = name;
    }

    public String getLite() {
        return lite;
    }

    public void setLite(String lite) {
        this.lite = lite;
    }

    public String getPrime() {
        return prime;
    }

    public void setPrime(String prime) {
        this.prime = prime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "RulePricePlanDTO{" +
                "name='" + name + '\'' +
                ", lite='" + lite + '\'' +
                ", prime='" + prime + '\'' +
                ", index=" + index +
                '}';
    }

    @Override
    public int compareTo(RulePricePlanDTO o) {
        return this.index - o.index;
    }
}
