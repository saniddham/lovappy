/**
 * Package com.realroofers.lovappy.service.user.repo is a package for user DAO classes.
 */
package com.realroofers.lovappy.service.user.repo;