package com.realroofers.lovappy.service.music.repo;

import com.realroofers.lovappy.service.music.model.Genre;
import com.realroofers.lovappy.service.music.model.Music;
import com.realroofers.lovappy.service.music.support.MusicStatus;
import com.realroofers.lovappy.service.user.model.Role;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.Set;

/**
 * Created by Eias Altawil on 8/25/17
 */
public interface MusicRepo extends JpaRepository<Music, Integer>, QueryDslPredicateExecutor<Music> {

    @Query(value = "SELECT m" +
            " FROM Music AS m" +
            " LEFT JOIN MusicExchange AS me ON m.id = me.music.id" +
            " WHERE m.status='APPROVED'"+
            " GROUP BY m.id" +
            " ORDER BY COUNT(me.id) DESC")
    Page<Music> getMostPopularPurchased(Pageable pageable);

    @Query(value = "SELECT m" +
            " FROM Music AS m" +
            " LEFT JOIN MusicExchange AS me ON m.id = me.music.id" +
            " WHERE m.status='APPROVED'"+
            " GROUP BY m.id" +
            " ORDER BY MAX(me.exchangeDate) DESC")
    Page<Music> getRecentlyPurchased(Pageable pageable);

    @Query(value = "SELECT m" +
            " FROM Music AS m" +
            " LEFT JOIN MusicExchange AS me ON m.id = me.music.id" +
             " JOIN m.author.roles r WHERE r.id.role IN :roles AND m.status='APPROVED'" +
            " GROUP BY m.id" +
            " ORDER BY COUNT(me.id) DESC")
    Page<Music> getMostPopularPurchasedByRole(@Param("roles") Set<Role> roles, Pageable pageable);

    Page<Music> findAllByStatus(MusicStatus status, Pageable pageable);

    @Query("SELECT m FROM Music m WHERE m.status=:status and (:title is null or m.title like :title) and (:genre is null or m.genre = :genre) and (:decade is null or m.decade = :decade)")
    Page<Music> search(@Param("status") MusicStatus status, @Param("title") String title, @Param("genre") Genre genre,@Param("decade") String decade, Pageable pageable);

    Integer countByCreatedBetween(Date start, Date end);

    Page<Music> findByAuthor(User author, Pageable pageable);

    Music findByProviderResourceId(Long providerResourceId);
}
