package com.realroofers.lovappy.service.user.model;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.couples.model.CouplesProfile;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.user.model.embeddable.Address;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.SubscriptionType;
import com.realroofers.lovappy.service.user.support.ZodiacSigns;
import com.realroofers.lovappy.service.util.UnitsConvert;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@Entity
@Table(name = "user_profile")
@DynamicUpdate
public class UserProfile implements Serializable{

    /*@GenericGenerator(name = "generator", strategy = "foreign",
            parameters = {@Parameter(name = "property", value = "user")})
    @GeneratedValue(generator = "generator")*/
    @Id
    @Column(name = "user_id", unique = true, nullable = false)
    private Integer userId;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private Date birthDate;


    @Enumerated(EnumType.STRING)
    private ZodiacSigns zodiacSigns;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "couple_id")
    private CouplesProfile couplesProfile;

    /*@Column(name = "height_feet")
    private Integer heightFt;
    @Column(name = "height_inches")
    private Integer heightIn;*/

    @Column(name = "height_centimeters")
    private Double heightCm;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn
    private CloudStorageFile profilePhotoCloudFile;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn
    private CloudStorageFile handsCloudFile;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn
    private CloudStorageFile feetCloudFile;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn
    private CloudStorageFile legsCloudFile;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn
    private CloudStorageFile photoIdentificationCloudFile;

    @Type(type = "yes_no")
    private Boolean handsFileSkipped;
    @Type(type = "yes_no")
    private Boolean feetFileSkipped;
    @Type(type = "yes_no")
    private Boolean legsFileSkipped;

    @Type(type = "yes_no")
    private Boolean lovstampSkipped;

    @ColumnDefault("false")
    private Boolean isAllowedProfilePic;

    @ElementCollection(targetClass = String.class)
    private Map<String, String> personalities;
    @ElementCollection(targetClass = String.class)
    private Map<String, String> lifestyles;
    @ElementCollection(targetClass = String.class)
    private Map<String, String> statuses;
    @Enumerated(EnumType.STRING)
    private SubscriptionType subscriptionType;

    @ManyToOne
    @JoinColumn(name = "language")
    private Language language;

    @Embedded
    private Address address;

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private User user;

    @ElementCollection(targetClass = User.class)
    private List<User> blockedUsers;

    @ManyToMany
    @JoinTable(
            name = "user_profile_speaking_language",
            joinColumns = @JoinColumn(
                    name = "user_profile_user_id", referencedColumnName = "user_id"),
            inverseJoinColumns = @JoinColumn(
                    name = "speaking_language_id", referencedColumnName = "id"))
    private Collection<Language> speakingLanguage;


    @ColumnDefault("0.0")
    @Column(name = "avg_distance_viewers")
    private Double avgDistanceOfViewers;

    @ColumnDefault("0")
    private Integer views;

    @ColumnDefault("0")
    private Integer creditBalance;

    @Column(name = "listened_to_count", columnDefinition="INT(11) default '0'" )
    private Integer listenedToCount;
    @Column(name = "listened_to_female_count", columnDefinition="INT(11) default '0'" )
    private Integer listenedToFemaleCount;
    @Column(name = "listened_to_male_count", columnDefinition="INT(11) default '0'" )
    private Integer listenedToMaleCount;

    @ManyToMany
    @JoinTable(
            name = "user_profile_pic_allowance",
            joinColumns = @JoinColumn(
                    name = "user_profile_user_id", referencedColumnName = "user_id"),
            inverseJoinColumns = @JoinColumn(
                    name = "allowed_user_id", referencedColumnName = "userId"))
    private Set<User> allowedProfileUsers = new HashSet<>(0);

    @Column(name = "max_transtacionts")
    private Integer maxTransactions;

    private Boolean completed;

    private String skills;

    private String stripeCustomerId;

    public UserProfile() {
        this.isAllowedProfilePic=false;
        this.setListenedToCount(0);
        this.setListenedToFemaleCount(0);
        this.setListenedToMaleCount(0);
    }

    public UserProfile(Integer userId) {
        this.userId = userId;
        this.isAllowedProfilePic=false;
        this.setListenedToCount(0);
        this.setListenedToFemaleCount(0);
        this.setListenedToMaleCount(0);
    }

    public String getStripeCustomerId() {
        return stripeCustomerId;
    }

    public void setStripeCustomerId(String stripeCustomerId) {
        this.stripeCustomerId = stripeCustomerId;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public ZodiacSigns getZodiacSigns() {
        return zodiacSigns;
    }

    public void setZodiacSigns(ZodiacSigns zodiacSigns) {
        this.zodiacSigns = zodiacSigns;
    }

    public Integer getMaxTransactions() {
        return maxTransactions;
    }

    public void setMaxTransactions(Integer maxTransactions) {
        this.maxTransactions = maxTransactions;
    }

    public Set<User> getAllowedProfileUsers() {
        return allowedProfileUsers;
    }

    public void setAllowedProfileUsers(Set<User> allowedProfileUsers) {
        this.allowedProfileUsers = allowedProfileUsers;
    }

    public CouplesProfile getCouplesProfile() {
        return couplesProfile;
    }

    public void setCouplesProfile(CouplesProfile couplesProfile) {
        this.couplesProfile = couplesProfile;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }


    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getHeightFt() {
        return UnitsConvert.centimeterToFeet(this.heightCm)[0];
    }

    public Integer getHeightIn() {
        return UnitsConvert.centimeterToFeet(this.heightCm)[1];
    }

    public Double getHeightCm() {
        return heightCm;
    }

    public void setHeightCm(Double heightCm) {
        this.heightCm = heightCm;
    }

    public CloudStorageFile getProfilePhotoCloudFile() {
        return profilePhotoCloudFile;
    }

    public void setProfilePhotoCloudFile(CloudStorageFile profilePhotoCloudFile) {
        this.profilePhotoCloudFile = profilePhotoCloudFile;
    }

    public CloudStorageFile getHandsCloudFile() {
        return handsCloudFile;
    }

    public void setHandsCloudFile(CloudStorageFile handsCloudFile) {
        this.handsCloudFile = handsCloudFile;
    }

    public CloudStorageFile getFeetCloudFile() {
        return feetCloudFile;
    }

    public void setFeetCloudFile(CloudStorageFile feetCloudFile) {
        this.feetCloudFile = feetCloudFile;
    }

    public CloudStorageFile getLegsCloudFile() {
        return legsCloudFile;
    }

    public void setLegsCloudFile(CloudStorageFile legsCloudFile) {
        this.legsCloudFile = legsCloudFile;
    }

    public CloudStorageFile getPhotoIdentificationCloudFile() {
        return photoIdentificationCloudFile;
    }

    public void setPhotoIdentificationCloudFile(CloudStorageFile photoIdentificationCloudFile) {
        this.photoIdentificationCloudFile = photoIdentificationCloudFile;
    }

    public Boolean getHandsFileSkipped() {
        return handsFileSkipped;
    }

    public void setHandsFileSkipped(Boolean handsFileSkipped) {
        this.handsFileSkipped = handsFileSkipped;
    }

    public Boolean getFeetFileSkipped() {
        return feetFileSkipped;
    }

    public void setFeetFileSkipped(Boolean feetFileSkipped) {
        this.feetFileSkipped = feetFileSkipped;
    }

    public Boolean getLegsFileSkipped() {
        return legsFileSkipped;
    }

    public void setLegsFileSkipped(Boolean legsFileSkipped) {
        this.legsFileSkipped = legsFileSkipped;
    }

    public Map<String, String> getPersonalities() {
        return personalities;
    }

    public void setPersonalities(Map<String, String> personalities) {
        this.personalities = personalities;
    }

    public Map<String, String> getLifestyles() {
        return lifestyles;
    }

    public void setLifestyles(Map<String, String> lifestyles) {
        this.lifestyles = lifestyles;
    }

    public Map<String, String> getStatuses() {
        return statuses;
    }

    public void setStatuses(Map<String, String> statuses) {
        this.statuses = statuses;
    }

    public SubscriptionType getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(SubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

	public List<User> getBlockedUsers() {
		return blockedUsers;
	}

	public void setBlockedUsers(List<User> blockedUsers) {
		this.blockedUsers = blockedUsers;
	}

    public Collection<Language> getSpeakingLanguage() {
        return speakingLanguage;
    }

    public Double getAvgDistanceOfViewers() {
        return avgDistanceOfViewers;
    }

    public void setAvgDistanceOfViewers(Double avgDistanceOfViewers) {
        this.avgDistanceOfViewers = avgDistanceOfViewers;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }
    public Integer getListenedToCount() {
        return listenedToCount;
    }

    public void setListenedToCount(Integer listenedToCount) {
        this.listenedToCount = listenedToCount;
    }

    public Integer getListenedToFemaleCount() {
        return listenedToFemaleCount;
    }

    public void setListenedToFemaleCount(Integer listenedToFemaleCount) {
        this.listenedToFemaleCount = listenedToFemaleCount;
    }

    public Integer getListenedToMaleCount() {
        return listenedToMaleCount;
    }

    public void setListenedToMaleCount(Integer listenedToMaleCount) {
        this.listenedToMaleCount = listenedToMaleCount;
    }

    public void setSpeakingLanguage(Collection<Language> speakingLanguage) {
        this.speakingLanguage.clear();
        this.speakingLanguage.addAll(speakingLanguage);
    }

    public Boolean getAllowedProfilePic() {
        return isAllowedProfilePic;
    }

    public void setAllowedProfilePic(Boolean allowedProfilePic) {
        isAllowedProfilePic = allowedProfilePic;
    }

    public Boolean getLovstampSkipped() {
        return lovstampSkipped;
    }

    public void setLovstampSkipped(Boolean lovstampSkipped) {
        this.lovstampSkipped = lovstampSkipped;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Integer getCreditBalance() {
        return creditBalance;
    }

    public void setCreditBalance(Integer creditBalance) {
        this.creditBalance = creditBalance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserProfile that = (UserProfile) o;

        if (!userId.equals(that.userId)) return false;
        if (gender != that.gender) return false;
        if (birthDate != null ? !birthDate.equals(that.birthDate) : that.birthDate != null) return false;
        if (heightCm != null ? !heightCm.equals(that.heightCm) : that.heightCm != null) return false;
        if (handsFileSkipped != null ? !handsFileSkipped.equals(that.handsFileSkipped) : that.handsFileSkipped != null)
            return false;
        if (feetFileSkipped != null ? !feetFileSkipped.equals(that.feetFileSkipped) : that.feetFileSkipped != null)
            return false;
        if (legsFileSkipped != null ? !legsFileSkipped.equals(that.legsFileSkipped) : that.legsFileSkipped != null)
            return false;
        if (lovstampSkipped != null ? !lovstampSkipped.equals(that.lovstampSkipped) : that.lovstampSkipped != null)
            return false;
        if (isAllowedProfilePic != null ? !isAllowedProfilePic.equals(that.isAllowedProfilePic) : that.isAllowedProfilePic != null)
            return false;
        if (subscriptionType != that.subscriptionType) return false;
        if (language != null ? !language.equals(that.language) : that.language != null) return false;
        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (avgDistanceOfViewers != null ? !avgDistanceOfViewers.equals(that.avgDistanceOfViewers) : that.avgDistanceOfViewers != null)
            return false;
        if (views != null ? !views.equals(that.views) : that.views != null) return false;
        if (listenedToCount != null ? !listenedToCount.equals(that.listenedToCount) : that.listenedToCount != null)
            return false;
        if (listenedToFemaleCount != null ? !listenedToFemaleCount.equals(that.listenedToFemaleCount) : that.listenedToFemaleCount != null)
            return false;
        if (listenedToMaleCount != null ? !listenedToMaleCount.equals(that.listenedToMaleCount) : that.listenedToMaleCount != null)
            return false;
        if (maxTransactions != null ? !maxTransactions.equals(that.maxTransactions) : that.maxTransactions != null)
            return false;
        return completed != null ? completed.equals(that.completed) : that.completed == null;
    }

    @Override
    public int hashCode() {
        int result = userId.hashCode();
        result = 31 * result + gender.hashCode();
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (heightCm != null ? heightCm.hashCode() : 0);
        result = 31 * result + (handsFileSkipped != null ? handsFileSkipped.hashCode() : 0);
        result = 31 * result + (feetFileSkipped != null ? feetFileSkipped.hashCode() : 0);
        result = 31 * result + (legsFileSkipped != null ? legsFileSkipped.hashCode() : 0);
        result = 31 * result + (lovstampSkipped != null ? lovstampSkipped.hashCode() : 0);
        result = 31 * result + (isAllowedProfilePic != null ? isAllowedProfilePic.hashCode() : 0);
        result = 31 * result + (subscriptionType != null ? subscriptionType.hashCode() : 0);
        result = 31 * result + (language != null ? language.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (avgDistanceOfViewers != null ? avgDistanceOfViewers.hashCode() : 0);
        result = 31 * result + (views != null ? views.hashCode() : 0);
        result = 31 * result + (listenedToCount != null ? listenedToCount.hashCode() : 0);
        result = 31 * result + (listenedToFemaleCount != null ? listenedToFemaleCount.hashCode() : 0);
        result = 31 * result + (listenedToMaleCount != null ? listenedToMaleCount.hashCode() : 0);
        result = 31 * result + (maxTransactions != null ? maxTransactions.hashCode() : 0);
        result = 31 * result + (completed != null ? completed.hashCode() : 0);
        return result;
    }
}

