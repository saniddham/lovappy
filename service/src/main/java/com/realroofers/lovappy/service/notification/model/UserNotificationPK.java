package com.realroofers.lovappy.service.notification.model;

import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 6/24/2017.
 * UserNotifaiton Primary key
 */
@Embeddable
@EqualsAndHashCode
public class UserNotificationPK implements Serializable{

    private User user;

    private Notification notification;

    public UserNotificationPK() {
    }

    public UserNotificationPK(User user, Notification notification) {
        this.user = user;
        this.notification = notification;
    }

    @ManyToOne
    @JoinColumn(name = "user_id")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne
    @JoinColumn(name = "notification_id")
    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }
}
