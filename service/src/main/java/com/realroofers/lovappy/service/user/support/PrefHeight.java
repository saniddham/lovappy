package com.realroofers.lovappy.service.user.support;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public enum PrefHeight {
    SHORTER("Shorter height"), TALLER("Taller height"), SAME("About the same height"), DOESNTMATTER("Height doesn't matter");
    String text;

    PrefHeight(String text) {
        this.text = text;
    }

    public String getText(){
        return text;
    }
}
