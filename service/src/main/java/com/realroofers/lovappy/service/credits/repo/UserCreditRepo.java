package com.realroofers.lovappy.service.credits.repo;

import com.realroofers.lovappy.service.credits.model.UserCreditPK;
import com.realroofers.lovappy.service.credits.model.UserCredits;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Daoud Shaheen on 8/10/2018.
 */
public interface UserCreditRepo extends JpaRepository<UserCredits, UserCreditPK> {
}
