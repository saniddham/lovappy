package com.realroofers.lovappy.service.product.dto;

import com.realroofers.lovappy.service.product.model.RetailerCommission;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.Country;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

@Data
@EqualsAndHashCode
public class RetailerCommissionDto implements Serializable {

    private Integer id;
    private Double commissionPercentage;
    private Date affectiveDate;
    private String effectiveDate;
    private Country country;
    private Integer countryId;
    private UserDto createdBy;
    private UserDto modifiedBy;
    private Boolean active;
    private Date created;
    private Date updated;

    public RetailerCommissionDto() {
    }

    public RetailerCommissionDto(RetailerCommission retailerCommission) {
        this.id = retailerCommission.getId();
        this.commissionPercentage = retailerCommission.getCommissionPercentage();
        this.affectiveDate = retailerCommission.getAffectiveDate();
        this.country = retailerCommission.getCountry();
        this.createdBy = new UserDto(retailerCommission.getCreatedBy());
        this.modifiedBy = new UserDto(retailerCommission.getModifiedBy());
        this.active = retailerCommission.getActive();
        this.created = retailerCommission.getCreated();
        this.updated = retailerCommission.getUpdated();
    }
}
