package com.realroofers.lovappy.service.music.model;

import com.realroofers.lovappy.service.ads.support.AgeRange;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.music.support.MusicProvider;
import com.realroofers.lovappy.service.music.support.MusicStatus;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Created by Eias Altawil on 8/25/17
 */

@Entity
@Table(name = "music")
@DynamicUpdate
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE music SET deleted = b'1' WHERE id = ?", check = ResultCheckStyle.COUNT)
@EqualsAndHashCode
public class Music extends BaseEntity<Integer>  implements Serializable {

    private String title;

    private Long providerResourceId;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile previewFile;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile file;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile coverImage;

    private String fileUrl;

    private String previewFileUrl;

    private String coverImageUrl;

    private String downloadUrl;

    @Type(type="text")
    private String lyrics;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn
    private User author;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn
    private Genre genre;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Enumerated(EnumType.STRING)
    private AgeRange ageRange;

    @Enumerated(EnumType.STRING)
    private MusicProvider provider;

    @Enumerated(EnumType.STRING)
    private MusicStatus status;

    private Double avgRating;

    @Transient
    private Boolean alreadyRate;

    private Boolean advertised;

    private String artistName;

    private String decade;

    @Column(name = "deleted", columnDefinition = "BIT(1) default 0")
    private Boolean deleted;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "id.music", cascade = CascadeType.ALL)
    private Set<MusicUser> musicUsers;

    public Music() {
        this.advertised = false;
    }

    public Music(Integer id, String title, CloudStorageFile file, CloudStorageFile coverImage, String lyrics, User author, Genre genre, Gender gender, AgeRange ageRange, MusicStatus status, Date createdOn, Double avgRating, Boolean alreadyRate) {
        setId(id);
        this.title = title;
        this.file = file;
        this.coverImage = coverImage;
        this.lyrics = lyrics;
        this.author = author;
        this.genre = genre;
        this.gender = gender;
        this.ageRange = ageRange;
        this.status = status;
        setCreated(createdOn);
        this.avgRating = avgRating;
        this.alreadyRate = alreadyRate;
    }

    public String getDecade() {
        return decade;
    }

    public void setDecade(String decade) {
        this.decade = decade;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public CloudStorageFile getPreviewFile() {
        return previewFile;
    }

    public void setPreviewFile(CloudStorageFile previewFile) {
        this.previewFile = previewFile;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CloudStorageFile getFile() {
        return file;
    }

    public CloudStorageFile getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(CloudStorageFile coverImage) {
        this.coverImage = coverImage;
    }

    public void setFile(CloudStorageFile file) {
        this.file = file;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public AgeRange getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(AgeRange ageRange) {
        this.ageRange = ageRange;
    }

    public MusicStatus getStatus() {
        return status;
    }

    public void setStatus(MusicStatus status) {
        this.status = status;
    }

    public Double getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(Double avgRating) {
        this.avgRating = avgRating;
    }


    public Set<MusicUser> getMusicUsers() {
        return musicUsers;
    }

    public void setMusicUsers(Set<MusicUser> musicUsers) {
        this.musicUsers = musicUsers;
    }

    public Boolean getAlreadyRate() {
        return alreadyRate;
    }

    public void setAlreadyRate(Boolean alreadyRate) {
        this.alreadyRate = alreadyRate;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public MusicProvider getProvider() {
        return provider;
    }

    public Boolean getAdvertised() {
        return advertised;
    }

    public void setAdvertised(Boolean advertised) {
        this.advertised = advertised;
    }

    public void setProvider(MusicProvider provider) {
        this.provider = provider;
    }

    public Long getProviderResourceId() {
        return providerResourceId;
    }

    public void setProviderResourceId(Long providerResourceId) {
        this.providerResourceId = providerResourceId;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getPreviewFileUrl() {
        return previewFileUrl;
    }

    public void setPreviewFileUrl(String previewFileUrl) {
        this.previewFileUrl = previewFileUrl;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }
}
