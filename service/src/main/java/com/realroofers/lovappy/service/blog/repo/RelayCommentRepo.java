package com.realroofers.lovappy.service.blog.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.realroofers.lovappy.service.blog.model.RelayComment;


public interface RelayCommentRepo extends JpaRepository<RelayComment, Integer>, QueryDslPredicateExecutor<RelayComment> {


//	public List<RelayComment> getAllRelayComments() {
//		return sessionFactory.getCurrentSession().createQuery("from relaycomment").list();
//	}
//	public void saveOrUpdate(RelayComment relaycomment) {
//		sessionFactory.getCurrentSession().saveOrUpdate(relaycomment);
//	}

}
