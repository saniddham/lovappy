package com.realroofers.lovappy.service.gift.repo;

import com.realroofers.lovappy.service.gift.dto.SaleByType;
import com.realroofers.lovappy.service.gift.dto.TopSellingItem;
import com.realroofers.lovappy.service.gift.model.GiftExchange;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Eias Altawil on 5/13/17
 */
public interface GiftExchangeRepo extends CrudRepository<GiftExchange, Integer>, QueryDslPredicateExecutor<GiftExchange> {
    List<GiftExchange> findAllByToUserOrderBySentAtDesc(User toUser);

    Page<GiftExchange> findAllByToUser(User toUser, Pageable pageable);

    List<GiftExchange> findAllByFromUserOrderBySentAtDesc(User fromUser);

    Page<GiftExchange> findAllByFromUser(User fromUser, Pageable pageable);

    long countAllByFromUser(User fromUser);

    long countAllByToUser(User ToUser);

    @Query("SELECT g FROM GiftExchange g WHERE g.gift.createdBy=:user order by g.sentAt desc ")
    List<GiftExchange> findAllByProductUser(@Param("user") User user);

    @Query("SELECT g FROM GiftExchange g WHERE g.gift.createdBy=:user and (:itemName is null or g.gift.productName like :itemName) and (:redeemCode is null or g.redeemCode like :redeemCode)")
    Page<GiftExchange> findAllPagesByProductUser(@Param("user") User user, @Param("itemName") String itemName,@Param("redeemCode") String redeemCode, Pageable pageable);

    @Query("SELECT g FROM GiftExchange g WHERE (:itemName is null or g.gift.productName like :itemName) and (:redeemCode is null or g.redeemCode like :redeemCode)")
    Page<GiftExchange> findAllSales(@Param("itemName") String itemName,@Param("redeemCode") String redeemCode, Pageable pageable);

    @Query(value = "SELECT sum(g.sold_price) FROM gift_exchange g left join product p on p.id = g.gift where p.created_by=:userId", nativeQuery = true)
    Double getTotalSalesByVendor(@Param("userId") Integer userId);

    @Query(value = "SELECT sum(g.sold_price) FROM gift_exchange g where g.from_user=:userId", nativeQuery = true)
    Double getTotalCostForUser(@Param("userId") Integer userId);

    @Query(value = "SELECT sum(g.commission) FROM gift_exchange g left join product p on p.id = g.gift where p.created_by=:userId", nativeQuery = true)
    Double getTotalCommissionByVendor(@Param("userId") Integer userId);


    @Query(value = "SELECT p.id,sum(g.sold_price) as price, count(g.id) as sale FROM gift_exchange g left join product p on p.id = g.gift where p.created_by=:userId group by g.gift order by sale DESC limit 1", nativeQuery = true)
    List<TopSellingItem> getTopSellingItemByVendor(@Param("userId") Integer userId);


    @Query(value = "SELECT up.gender as chartType, count(g.id) as sale FROM gift_exchange g left join product p on p.id = g.gift left join user_profile up on up.user_id = g.to_user where p.created_by=:userId group by up.gender", nativeQuery = true)
    List<SaleByType> getSaleByGenderForVendor(@Param("userId") Integer userId);

    @Query(value = "SELECT pc.type_name as chartType ,count(g.id) as sale FROM gift_exchange g left join product p on p.id = g.gift left join product_type pc on pc.id = p.fk_type where p.created_by=:userId group by p.fk_type", nativeQuery = true)
    List<SaleByType> getSaleByCategoryForVendor(@Param("userId") Integer userId);

    @Query(value = "SELECT pc.brand_name as chartType ,count(g.id) as sale FROM gift_exchange g left join product p on p.id = g.gift left join product_brand pc on pc.id = p.fk_brand where p.created_by=:userId group by p.fk_brand", nativeQuery = true)
    List<SaleByType> getSaleByBrandForVendor(@Param("userId") Integer userId);

}



