package com.realroofers.lovappy.service.cms.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.realroofers.lovappy.service.core.VideoProvider;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Daoud Shaheen on 6/6/2017.
 */
@Entity
@Table(name="video_widget")
@EqualsAndHashCode
public class VideoWidget extends Widget {
    private String url;

    @Enumerated(EnumType.STRING)
    private VideoProvider provider;

    private String providerId;

    @ManyToMany(mappedBy = "videos")
    @JsonBackReference
    private Set<Page> pages = new HashSet<>(0);

    public VideoWidget() {
    }

    public VideoWidget(String name, String url, VideoProvider provider) {
        super(name);
        this.provider = provider;
        this.url = url;
    }

    public Set<Page> getPages() {
        return pages;
    }

    public void setPages(Set<Page> pages) {
        this.pages = pages;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public VideoProvider getProvider() {
        return provider;
    }

    public void setProvider(VideoProvider provider) {
        this.provider = provider;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        VideoWidget that = (VideoWidget) o;

        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        if (provider != that.provider) return false;
        return pages != null ? pages.equals(that.pages) : that.pages == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (provider != null ? provider.hashCode() : 0);
        result = 31 * result + (pages != null ? pages.hashCode() : 0);
        return result;
    }
}
