package com.realroofers.lovappy.service.music.repo;

import com.realroofers.lovappy.service.music.model.MusicUser;
import com.realroofers.lovappy.service.music.model.MusicUserPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Daoud Shaheen on 9/23/2017.
 */
public interface MusicUserRepo extends JpaRepository<MusicUser, MusicUserPK> {

    @Query(nativeQuery = true, value = "SELECT AVG(rating) FROM musics_users WHERE music_id=:musicId AND rating>=0")
    Double findAverageRatingByMusicId(@Param("musicId") Integer musicId);
}
