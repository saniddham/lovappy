package com.realroofers.lovappy.service.credits.model;

import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 8/10/2018.
 */
@Embeddable
@EqualsAndHashCode
public class UserCreditPK implements Serializable{

    private User fromUser;

    private User toUser;

    public UserCreditPK() {
    }

    public UserCreditPK(User fromUser, User toUser) {
        this.fromUser = fromUser;
        this.toUser = toUser;
    }

    @ManyToOne
    @JoinColumn(name = "from_user")
    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }


    @ManyToOne
    @JoinColumn(name = "to_user")
    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }
}

