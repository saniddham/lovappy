package com.realroofers.lovappy.service.product.dto;

import com.realroofers.lovappy.service.product.model.Brand;
import com.realroofers.lovappy.service.user.dto.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class BrandDto {

    private Integer id;
    private String brandName;
    private String brandDescription;
    private Boolean active = true;
    private UserDto createdBy;

    public BrandDto() {
    }

    public BrandDto(Integer id) {
        this.id = id;
    }

    public BrandDto(Brand brand) {
        if(brand.getId()!=null) {
            this.id = brand.getId();
        }
        this.brandName = brand.getBrandName();
        this.brandDescription = brand.getBrandDescription();
        this.active = brand.getActive();
        this.createdBy = new UserDto(brand.getCreatedBy());
    }
}
