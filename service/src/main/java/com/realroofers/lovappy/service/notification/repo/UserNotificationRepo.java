package com.realroofers.lovappy.service.notification.repo;

import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.notification.model.UserNotification;
import com.realroofers.lovappy.service.notification.model.UserNotificationPK;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Daoud Shaheen on 6/24/2017.
 */
public interface UserNotificationRepo extends JpaRepository<UserNotification, UserNotificationPK> {

    UserNotification findById(UserNotificationPK pk);

    @Query(nativeQuery = true, value="UPDATE users_notification un JOIN notification nt ON nt.id=un.notification_id  set un.seen=1 WHERE un.user_id=:user")
    @Modifying
    @Transactional
    void updateSeenByUser(@Param("user") User user);

    @Query("UPDATE UserNotification un set un.seen=true WHERE un.id.user=:user AND un.id.notification.type=:type")
    @Modifying
    @Transactional
    void updateSeenByUserAndType(@Param("user") User user, @Param("type")NotificationTypes type);


    @Query(nativeQuery = true, value="UPDATE users_notification un JOIN notification nt ON nt.id=un.notification_id  set un.seen=1 WHERE un.user_id=:user AND nt.type=:notifType")
    @Modifying
    @Transactional
    void updateSeenByUserIdAndType(@Param("user") Integer user, @Param("notifType")String type);

    @Query(nativeQuery = true, value="UPDATE users_notification un JOIN notification nt ON nt.id=un.notification_id  set un.seen=1 WHERE un.user_id=:user AND nt.type in :notifTypes")
    @Modifying
    @Transactional
    void updateSeenByUserIdAndTypes(@Param("user") Integer user, @Param("notifTypes")List<String> types);
}
