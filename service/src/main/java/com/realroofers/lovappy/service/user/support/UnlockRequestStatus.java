package com.realroofers.lovappy.service.user.support;

/**
 * @author Eias Altawil
 */

public enum UnlockRequestStatus {
    PENDING("PENDING"), APPROVED("APPROVED"), REJECTED("REJECTED");
    String text;

    UnlockRequestStatus(String text) {
        this.text = text;
    }
}
