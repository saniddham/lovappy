package com.realroofers.lovappy.service.cloud.support;

/**
 * @author Japheth Odonya
 * 
 * */
public enum PhotoNames {
   PROFILEPHOTO("Profile Photo"),
   PROFILEAUDIO("Profile Audio"),
   HANDSPHOTO("Hands Photo"),
   FEETPHOTO("Feet Photo"),
   LEGSPHOTO("Legs Photo");

    private String status;

    PhotoNames(String status){
        this.status = status;
    }

    public String getValue(){
        return status;
    }


}
