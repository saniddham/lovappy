package com.realroofers.lovappy.service.product.dto;

import com.realroofers.lovappy.service.product.support.Upload;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode
public class ProductUploadDto {

    private Integer id;
    @NotNull(message = "Product name cannot be empty.", groups = {Upload.class})
    private String productName;
    @NotNull(message = "Product code cannot be empty.", groups = {Upload.class})
    private String productCode;
    @NotNull(message = "Product description cannot be empty.", groups = {Upload.class})
    private String productDescription;
    @NotNull(message = "Product price cannot be empty.", groups = {Upload.class})
    private Double price;
    @NotNull(message = "Product availability cannot be empty.", groups = {Upload.class})
    private String productAvailability;
    @NotNull(message = "Product category cannot be empty.", groups = {Upload.class})
    private String productType;
    private String brand;

    @NotNull(message = "Main image cannot be empty.", groups = {Upload.class})
    private String image1;
    @NotNull(message = "Second image cannot be empty.", groups = {Upload.class})
    private String image2;
    @NotNull(message = "Third image cannot be empty.", groups = {Upload.class})
    private String image3;
    private String productEan;
    private BigDecimal commissionEarned;

}
