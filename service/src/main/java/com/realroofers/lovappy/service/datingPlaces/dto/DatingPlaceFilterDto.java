package com.realroofers.lovappy.service.datingPlaces.dto;

import com.realroofers.lovappy.service.datingPlaces.support.Importance;
import com.realroofers.lovappy.service.datingPlaces.support.NeighborHood;
import com.realroofers.lovappy.service.datingPlaces.support.PriceRange;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Darrel Rayen on 11/9/17.
 */
@EqualsAndHashCode
public class DatingPlaceFilterDto implements Serializable {

    private Gender gender;
    private String ageRange;
    private PriceRange priceRange;
    private Boolean isPetsAllowed;
    private Boolean isGoodForCouple;
    private List<Integer> placeAtmosphere;
    private List<Integer> foodTypes;
    private List<Integer> personTypes;
    private NeighborHood neighborhood;
    private Double latitude;
    private Double longitude;
    private Importance security;
    private Importance parking;
    private Importance transport;
    private Boolean isFiltered;
    private Boolean isQuickFiltered;
    private Boolean isWorldWide;
    private Boolean isLocal;
    private Boolean isDealsActive;
    private Boolean featured;
    private Long currentDate;

    public DatingPlaceFilterDto() {
    }

    public Boolean getFeatured() {
        return featured==null?false:featured;
    }

    public void setFeatured(Boolean featured) {
        this.featured = featured;
    }

    public Gender getGender() {
        return gender == null ? Gender.NA : gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getAgeRange() {
        return ageRange == null || ageRange.equalsIgnoreCase("NA") ? "" : ageRange;
    }

    public void setAgeRange(String ageRange) {
        this.ageRange = ageRange;
    }

    public PriceRange getPriceRange() {
        return priceRange == null ? PriceRange.NA : priceRange;
    }

    public void setPriceRange(PriceRange priceRange) {
        this.priceRange = priceRange;
    }

    public Boolean getPetsAllowed() {
        return isPetsAllowed;
    }

    public void setPetsAllowed(Boolean petsAllowed) {
        isPetsAllowed = petsAllowed;
    }

    public Boolean getGoodForCouple() {
        return isGoodForCouple;
    }

    public void setGoodForCouple(Boolean goodForCouple) {
        isGoodForCouple = goodForCouple;
    }

    public List<Integer> getPlaceAtmosphere() {
        return placeAtmosphere;
    }

    public void setPlaceAtmosphere(List<Integer> placeAtmosphere) {
        this.placeAtmosphere = placeAtmosphere;
    }

    public List<Integer> getFoodTypes() {
        return foodTypes;
    }

    public void setFoodTypes(List<Integer> foodTypes) {
        this.foodTypes = foodTypes;
    }

    public NeighborHood getNeighborhood() {
        return neighborhood == null ? NeighborHood.NA : neighborhood;
    }

    public void setNeighborhood(NeighborHood neighborhood) {
        this.neighborhood = neighborhood;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Importance getSecurity() {
        return security == null ? Importance.NA : security;
    }

    public void setSecurity(Importance security) {
        this.security = security;
    }

    public Importance getParking() {
        return parking == null ? Importance.NA : parking;
    }

    public void setParking(Importance parking) {
        this.parking = parking;
    }

    public Importance getTransport() {
        return transport == null ? Importance.NA : transport;
    }

    public void setTransport(Importance transport) {
        this.transport = transport;
    }

    public Boolean getFiltered() {
        return isFiltered;
    }

    public void setFiltered(Boolean filtered) {
        isFiltered = filtered;
    }

    public Boolean getQuickFiltered() {
        return isQuickFiltered;
    }

    public void setQuickFiltered(Boolean quickFiltered) {
        isQuickFiltered = quickFiltered;
    }

    public Boolean getWorldWide() {
        return isWorldWide == null ? false : isWorldWide;
    }

    public void setWorldWide(Boolean worldWide) {
        isWorldWide = worldWide;
    }

    public Boolean getLocal() {
        return isLocal == null ? false : isLocal;
    }

    public void setLocal(Boolean local) {
        isLocal = local;
    }

    public Boolean getDealsActive() {
        return isDealsActive == null ? false : isDealsActive;
    }

    public void setDealsActive(Boolean dealsActive) {
        isDealsActive = dealsActive;
    }

    public Long getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Long currentDate) {
        this.currentDate = currentDate;
    }

    public List<Integer> getPersonTypes() {
        return personTypes;
    }

    public void setPersonTypes(List<Integer> personTypes) {
        this.personTypes = personTypes;
    }

    @Override
    public String toString() {
        return "DatingPlaceFilterDto{" +
                "gender=" + gender +
                ", ageRange='" + ageRange + '\'' +
                ", priceRange=" + priceRange +
                ", isPetsAllowed=" + isPetsAllowed +
                ", isGoodForCouple=" + isGoodForCouple +
                ", placeAtmosphere=" + placeAtmosphere +
                ", foodTypes=" + foodTypes +
                ", personTypes=" + personTypes +
                ", neighborhood=" + neighborhood +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", security=" + security +
                ", parking=" + parking +
                ", transport=" + transport +
                ", isFiltered=" + isFiltered +
                ", isQuickFiltered=" + isQuickFiltered +
                ", isWorldWide=" + isWorldWide +
                ", isLocal=" + isLocal +
                ", isDealsActive=" + isDealsActive +
                ", currentDate=" + currentDate +
                '}';
    }
}
