package com.realroofers.lovappy.service.notification.model;

/**
 * Created by Daoud Shaheen on 6/24/2017.
 */
public enum NotificationTypes {
    USER_REGISTRATION("New User Registration"),
    USER_FLAGS("New User Complaints"),
    USER_BLOGS("New User Blog"),
    USER_ADS("New User Ads"),
    AMBASSADOR_REGISTRATION("New Ambassador Registration"),
    MUSICIAN_REGISTRATION("New Musician Registration"),
    AUTHOR_REGISTRATION("New Author Registration"),
    VENDOR_REGISTRATION("New Vendor Registration"),
    EVENT_APPROVAL("New Event Approval"),
    PLACE_APPROVAL("New Dating Place Approval"),
    REVIEW_APPROVAL("New Review Approval"),
    MUSIC_ORDER("Music New Order");

    String value;

    NotificationTypes(String value){
        this.value = value;
    }

    public String getValue(){
        return this.value;
    }
}
