package com.realroofers.lovappy.service.radio;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.radio.dto.RadioProgramControlDto;
import com.realroofers.lovappy.service.radio.model.RadioProgramControl;

/**
 * Created by Manoj on 17/12/2017.
 */
public interface RadioProgramControlService extends AbstractService<RadioProgramControl, Integer> {

    void saveRadioControl(RadioProgramControlDto controlDto) throws Exception;
}
