package com.realroofers.lovappy.service.upload.repo;

import com.realroofers.lovappy.service.upload.model.FileUpload;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Manoj
 */
public interface FileUploadRepo extends JpaRepository<FileUpload, Integer> {
}
