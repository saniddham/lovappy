package com.realroofers.lovappy.service.gift.dto;

import com.realroofers.lovappy.service.gift.model.PurchasedGift;
import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.user.dto.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Manoj on 08/02/2018.
 */
@Data
@ToString
@EqualsAndHashCode
public class PurchasedGiftDto implements Serializable {

    Integer id;
    Product product;
    private UserDto user;
    private Date purchasedOn;

    public PurchasedGiftDto() {
    }

    public PurchasedGiftDto(PurchasedGift purchasedGift) {
        id = purchasedGift.getId();
        product = purchasedGift.getProduct();
        user = new UserDto(purchasedGift.getUser());
        purchasedOn = purchasedGift.getPurchasedOn();
    }

}
