package com.realroofers.lovappy.service.plan.impl;

import com.realroofers.lovappy.service.plan.PlansService;
import com.realroofers.lovappy.service.plan.dto.FeatureDetailsDto;
import com.realroofers.lovappy.service.plan.dto.PlanDetailsDto;
import com.realroofers.lovappy.service.plan.dto.PlansDto;
import com.realroofers.lovappy.service.plan.model.Feature;
import com.realroofers.lovappy.service.plan.model.Plan;
import com.realroofers.lovappy.service.plan.repo.FeatureRepo;
import com.realroofers.lovappy.service.plan.repo.PlanRepo;
import com.realroofers.lovappy.service.util.ListMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by hasan on 7/11/2017.
 */
@Service
public class PlanServiceImpl implements PlansService{
    private PlanRepo planRepo;
    private FeatureRepo featureRepo;
    @Autowired
    public PlanServiceImpl(PlanRepo planRepo, FeatureRepo featureRepo){
        this.planRepo = planRepo;
        this.featureRepo = featureRepo;
    }
    @Override
    public List<PlansDto> allPlans() {
        List<Plan> all = planRepo.findAll();
        return new ListMapper<>(all).map(PlansDto::new);
    }

    @Override
    public List<PlanDetailsDto> allDetailedPlans(boolean onlyValid) {
        List<Plan> all = planRepo.findAll();
        List<Plan> result = new ArrayList<>();
        try {

            all.stream().forEach(x -> x.setPlanFeatures(x.getPlanFeatures().stream().filter(i -> i.getIsValid()).collect(Collectors.toList())));

            if (onlyValid)
                result = all.stream().filter(x -> x.getIsValid()).collect(Collectors.toList());
        }
        catch (Exception e){
            return new ListMapper<>(all).map(PlanDetailsDto::new);
        }
        return onlyValid ? new ListMapper<>(result).map(PlanDetailsDto::new) : new ListMapper<>(all).map(PlanDetailsDto::new);
    }

    @Override
    public PlanDetailsDto savePlan(PlanDetailsDto plansDto) {
        Plan plan = null;

        if(plansDto.getPlanID() != null)
            plan = planRepo.findOne(plansDto.getPlanID());

        if(plan == null)
            plan = new Plan();

        plan.setColorScheme(plansDto.getColorScheme());
        plan.setPlanName(plansDto.getPlanName());
        plan.setPlanSubtitle(plansDto.getSubTitle());
        plan.setIsValid(plansDto.getIsValid());
        plan.setCost(plansDto.getCost());
        plan.setCostDescription(plansDto.getCostDescription());

        Plan save = planRepo.save(plan);

        return save == null ? null : new PlanDetailsDto(save);
    }

    @Override
    @Transactional(readOnly = true)
    public PlanDetailsDto getPlanDetails(Integer planId, boolean hasValidFeatures) {
        PlanDetailsDto plansDto = new PlanDetailsDto(planRepo.findOne(planId));
        if(!hasValidFeatures)
            plansDto.setPlanFeatures(plansDto.getPlanFeatures().stream().filter(x-> !x.getIsValid()).collect(Collectors.toList()));

        return plansDto.getPlanID() == null ? new PlanDetailsDto() : plansDto;
    }

    @Override
    @Transactional(readOnly = true)
    public FeatureDetailsDto getFeatureByID(Integer id) {
        FeatureDetailsDto featureDetailsDto = new FeatureDetailsDto(featureRepo.findOne(id));

        return featureDetailsDto.getFeatureID() == null ? new FeatureDetailsDto() : featureDetailsDto;
    }

    @Override
    public FeatureDetailsDto saveFeature(FeatureDetailsDto featureDetailsDto) {
        Feature feature = null;
        if(featureDetailsDto.getFeatureID() != null)
            feature = featureRepo.findOne(featureDetailsDto.getFeatureID());
        if(featureDetailsDto.getFeatureID() == null)
            feature = new Feature();

        feature.setFeatureDetails(featureDetailsDto.getFeatureDetails());
        feature.setFeatureNote(featureDetailsDto.getFeatureNote());
        feature.setNoteColor(featureDetailsDto.getNoteColor());
        feature.setIsValid(featureDetailsDto.getIsValid());
        feature.setPlan(planRepo.findOne(featureDetailsDto.getPlanId()));

        Feature savedFeature = featureRepo.save(feature);

        return new FeatureDetailsDto(savedFeature);

    }

    @Override
    public List<PlansDto> getPlansByName(){
        List<Plan> all = planRepo.findAll();
        return new ListMapper<>(all).map(PlansDto::new);
    }

    @Override
    public List<FeatureDetailsDto> saveFeatures(List<FeatureDetailsDto> features){
        featureRepo.save(features.stream().map(x->
                new Feature(planRepo.findOne(x.getPlanId()), x.getFeatureDetails(), x.getFeatureNote(), x.getNoteColor(), x.getIsValid())).collect(Collectors.toList()));
        return features;
    }
}
