package com.realroofers.lovappy.service.event.repo;

import com.realroofers.lovappy.service.event.model.Event;
import com.realroofers.lovappy.service.event.support.EventStatus;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by Tejaswi Venupalli on 8/29/17
 */
@Repository
public interface EventRepo extends JpaRepository<Event, Integer>, QueryDslPredicateExecutor<Event> {


    Page<Event> findEventsByEventDateGreaterThanEqualAndEventStatusAndEventApprovalStatusOrderByEventDate(Pageable pageable, Date eventDate, EventStatus eventStatus, ApprovalStatus approvalStatus);

    Page<Event> findAllByOrderByEventIdDesc(Pageable pageable);

    @Query("select e from Event e where (e.eventLocation.city LIKE CONCAT('%',:filter,'%') or e.eventDescription LIKE CONCAT('%',:filter,'%') or e.eventTitle LIKE CONCAT('%',:filter,'%') or e.eventLocation.state LIKE CONCAT('%',:filter,'%') or e.eventLocation.zipCode LIKE CONCAT('%',:filter,'%')) and e.eventApprovalStatus='APPROVED' and e.eventStatus='ACTIVE' and (e.eventDate >= CURRENT_DATE )")
    Page<Event> findAllByFilters(Pageable pageable, @Param("filter") String queryString);

    @Query("select e, (case when eu.id.user.userId = :userId then true else false end) as paid from Event e LEFT OUTER JOIN EventUser eu ON eu.id.event = e and eu.id.user.userId = :userId where (e.eventLocation.city LIKE CONCAT('%',:filter,'%') or e.eventDescription LIKE CONCAT('%',:filter,'%') or e.eventTitle LIKE CONCAT('%',:filter,'%') or e.eventLocation.state LIKE CONCAT('%',:filter,'%') or e.eventLocation.zipCode LIKE CONCAT('%',:filter,'%')) and e.eventApprovalStatus='APPROVED' and e.eventStatus='ACTIVE' and (e.eventDate >= CURRENT_DATE )")
    Page<Object[]> findAllByMobileFilters(Pageable pageable, @Param("filter") String queryString, @Param("userId") Integer userId);

    @Query("select e, (case when eu.id.user.userId = :userId then true else false end) as paid from Event e LEFT OUTER JOIN EventUser eu ON eu.id.event = e and eu.id.user.userId = :userId WHERE (e.eventDate >= CURRENT_DATE ) and e.eventStatus = :eventStatus and e.eventApprovalStatus = :approvalStatus order by e.eventDate DESC ")
    Page<Object[]> findAllByEventApprovalStatusOrderByEventDateDesc(@Param("eventStatus") EventStatus eventStatus, @Param("approvalStatus") ApprovalStatus approvalStatus, @Param("userId") Integer userId, Pageable pageable);

    @Query("select e from Event e WHERE (e.eventDate >= CURRENT_DATE ) and e.eventStatus = :eventStatus and e.eventApprovalStatus = :eventApprovalStatus order by e.eventDate")
    List<Event> findByEventDateAndEventStatus(@Param("eventStatus") EventStatus eventStatus, @Param("eventApprovalStatus") ApprovalStatus approvalStatus);

    @Query("select e from Event e WHERE (e.eventDate >= CURRENT_DATE ) and e.eventStatus = :eventStatus and e.eventApprovalStatus = :approvalStatus order by e.eventDate")
    Page<Event> findByDate(@Param("eventStatus") EventStatus eventStatus, @Param("approvalStatus") ApprovalStatus approvalStatus, Pageable pageable);

    @Query("select e, (case when eu.id.user.userId = :userId then true else false end) as paid from Event e LEFT OUTER JOIN EventUser eu ON eu.id.event = e and eu.id.user.userId = :userId WHERE (e.eventDate >= CURRENT_DATE ) and e.eventStatus = :eventStatus and e.eventApprovalStatus = :approvalStatus order by e.eventDate")
    Page<Object[]> findByDateMobile(@Param("eventStatus") EventStatus eventStatus, @Param("approvalStatus") ApprovalStatus approvalStatus, @Param("userId") Integer userId, Pageable pageable);

    @Query(nativeQuery = true, value = "SELECT ev.* FROM event ev INNER JOIN event_eventcategory eev ON eev.event_id = ev.event_id " +
            "INNER JOIN event_category ec ON ec.category_id = eev.category_id AND ec. category_name =:categoryName ;")
    List<Event> findEventTitlesByCategory(@Param("categoryName") String categoryName);

    @Query("SELECT e FROM Event e WHERE "
            + "SQRT((ABS(e.eventLocation.latitude - ?1) * ABS(e.eventLocation.latitude - ?1)) + "
            + "(ABS(e.eventLocation.longitude - ?2) * ABS(e.eventLocation.longitude - ?2))) * 70 <= ?3 " +
            " AND e.eventStatus ='Active' AND e.eventDate BETWEEN ?4 AND ?5 AND e.eventApprovalStatus = 'Approved' ORDER BY e.eventDate ")
    List<Event> findNearByEvents(double centerLatitude, double centerLongitude, double radiusInMiles, Date fromDate, Date toDate);

    @Query("SELECT e FROM Event e WHERE "
            + "SQRT((ABS(e.eventLocation.latitude - ?1) * ABS(e.eventLocation.latitude - ?1)) + "
            + "(ABS(e.eventLocation.longitude - ?2) * ABS(e.eventLocation.longitude - ?2))) * 70 <= ?3 " +
            " AND e.eventStatus ='Active' AND e.eventDate BETWEEN ?4 AND ?5  AND e.eventApprovalStatus = 'Approved' ORDER BY e.eventDate ")
    Page<Event> findNearByEventsPage(double centerLatitude, double centerLongitude, double radiusInMiles, Date fromDate, Date toDate, Pageable pageable);

    @Query("SELECT e ,(case when eu.id.user.userId = ?1 then true else false end) as paid ,SQRT((ABS(e.eventLocation.latitude - ?2) * ABS(e.eventLocation.latitude - ?2)) + (ABS(e.eventLocation.longitude - ?3) * ABS(e.eventLocation.longitude - ?3))) * 70 AS distance FROM Event e LEFT OUTER JOIN EventUser eu ON eu.id.event = e and eu.id.user.userId = ?1 WHERE "
            + "SQRT((ABS(e.eventLocation.latitude - ?2) * ABS(e.eventLocation.latitude - ?2)) + "
            + "(ABS(e.eventLocation.longitude - ?3) * ABS(e.eventLocation.longitude - ?3))) * 70 <= ?4 "
            + " AND e.eventStatus ='Active' AND (e.eventDate >= current_date)  AND e.eventApprovalStatus = 'Approved'"
            + "group by e.eventId order by distance")
    Page<Object[]> findNearByEvents(Integer userId, double centerLatitude, double centerLongitude, double radiusInMiles, Pageable pageable);

    @Query("SELECT e FROM Event e WHERE "
            + "SQRT((ABS(e.eventLocation.latitude - ?1) * ABS(e.eventLocation.latitude - ?1)) + "
            + "(ABS(e.eventLocation.longitude - ?2) * ABS(e.eventLocation.longitude - ?2))) * 70 <= ?3 " +
            " AND e.eventStatus ='Active' AND (e.eventDate >= current_date)  AND e.eventApprovalStatus = 'Approved'")
    List<Event> findNearByEventsList(double centerLatitude, double centerLongitude, double radiusInMiles);

    @Query("select e from Event e where e.eventDate < current_date")
    List<Event> findPreviousEvents();

    @Query("select e from Event e where e.eventDate < current_date")
    Page<Event> findPreviousEvents(Pageable pageable);

    @Query("SELECT e FROM Event e WHERE e.eventDate =:eventDate")
    List<Event> findPreviousEventsByDate(@Param("eventDate") Date eventDate);

    @Query("SELECT COUNT (e) FROM Event e WHERE "
            + "SQRT((ABS(e.eventLocation.latitude - ?1) * ABS(e.eventLocation.latitude - ?1)) + "
            + "(ABS(e.eventLocation.longitude - ?2) * ABS(e.eventLocation.longitude - ?2))) * 70 <= ?3 " +
            " AND e.eventStatus ='Active' AND e.eventDate BETWEEN ?4 AND ?5 AND e.eventApprovalStatus = 'Approved' ORDER BY e.eventDate ")
    Integer findNearByEventsCount(double centerLatitude, double centerLongitude, double radiusInMiles, Date fromDate, Date toDate);

    @Query("select e, (case when u.id.user.userId = :userId then true else false end) from EventUser u, Event e where e.eventId = u.id.event.eventId and u.id.user.userId =:userId and e.eventDate >= :eventDate")
    Page<Object[]> findFutureEventsByUserId(@Param("userId") Integer userId, @Param("eventDate") Date date, Pageable pageable);

    @Query("select e, (case when u.id.user.userId = :userId then true else false end) as paid from EventUser u, Event e where e.eventId = u.id.event.eventId and u.id.user.userId =:userId and e.eventDate < :eventDate")
    Page<Object[]> findPastEventsByUserId(@Param("userId") Integer userId, @Param("eventDate") Date date, Pageable pageable);

    @Query("select e, (case when u.id.user.userId = :userId then true else false end) as paid from EventUser u, Event e where e.eventId = u.id.event.eventId and u.id.user.userId =:userId and e.eventDate < :eventDate")
    List<Object[]> findPastEventsByUserId(@Param("userId") Integer userId, @Param("eventDate") Date date);

    @Query("select e, (case when u.id.user.userId = :userId then true else false end) from EventUser u, Event e where e.eventId = u.id.event.eventId and u.id.user.userId =:userId and e.eventDate >= :eventDate")
    List<Object[]> findFutureEventsByUserId(@Param("userId") Integer userId, @Param("eventDate") Date date);

}
