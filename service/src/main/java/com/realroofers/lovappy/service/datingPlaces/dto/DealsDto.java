package com.realroofers.lovappy.service.datingPlaces.dto;

import com.realroofers.lovappy.service.datingPlaces.model.Deals;
import lombok.EqualsAndHashCode;

/**
 * Created by Darrel Rayen on 1/18/18.
 */
@EqualsAndHashCode
public class DealsDto {

    private Integer dealId;
    private Double startPrice;
    private Double endPrice;
    private Double feePercentage;

    public DealsDto() {
    }

    public DealsDto(Deals deals) {
        this.dealId = deals.getDealId();
        this.startPrice = deals.getStartPrice();
        this.endPrice = deals.getEndPrice();
        this.feePercentage = deals.getFeePercentage();
    }


    public Integer getDealId() {
        return dealId;
    }

    public void setDealId(Integer dealId) {
        this.dealId = dealId;
    }

    public Double getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(Double startPrice) {
        this.startPrice = startPrice;
    }

    public Double getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(Double endPrice) {
        this.endPrice = endPrice;
    }

    public Double getFeePercentage() {
        return feePercentage;
    }

    public void setFeePercentage(Double feePercentage) {
        this.feePercentage = feePercentage;
    }
}
