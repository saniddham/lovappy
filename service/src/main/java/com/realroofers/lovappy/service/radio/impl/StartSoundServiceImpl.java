package com.realroofers.lovappy.service.radio.impl;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.radio.StartSoundService;
import com.realroofers.lovappy.service.radio.dto.StartSoundDto;
import com.realroofers.lovappy.service.radio.model.StartSound;
import com.realroofers.lovappy.service.radio.repo.StartSoundRepo;
import com.realroofers.lovappy.service.radio.support.StartSoundType;
import com.realroofers.lovappy.service.util.ListMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Daoud Shaheen on 1/12/2018.
 */
@Service
public class StartSoundServiceImpl implements StartSoundService {

    private final StartSoundRepo startSoundRepo;

    public StartSoundServiceImpl(StartSoundRepo startSoundRepo) {
        this.startSoundRepo = startSoundRepo;
    }

    @Transactional(readOnly = true)
    @Override
    public List<StartSoundDto> getStartSounds() {
        List<StartSound> all = startSoundRepo.findAll();
        return new ListMapper<>(all).map(StartSoundDto::new);
    }

    @Transactional
    @Override
    public StartSoundDto addStartSound(StartSoundDto sound) {
        StartSound startSound = startSoundRepo.findBySoundType(sound.getSoundType());

        if(startSound == null){
            startSound = new StartSound();
        }

        CloudStorageFile cloudStorageFile = new CloudStorageFile();
        cloudStorageFile.setBucket(sound.getAudioFileCloud().getBucket());
        cloudStorageFile.setName(sound.getAudioFileCloud().getName());
        cloudStorageFile.setUrl(sound.getAudioFileCloud().getUrl());

        startSound.setAudioFileCloud(cloudStorageFile);
        startSound.setSoundType(sound.getSoundType());

        StartSound save = startSoundRepo.save(startSound);
        return save == null ? null : new StartSoundDto(save);
    }

    @Transactional(readOnly = true)
    @Override
    public StartSoundDto getSoundByType(StartSoundType type) {
        StartSound sound = startSoundRepo.findBySoundType(type);
        return sound == null ? null : new StartSoundDto(sound);
    }
}
