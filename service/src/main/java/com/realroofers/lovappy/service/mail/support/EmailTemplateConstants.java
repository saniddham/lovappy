package com.realroofers.lovappy.service.mail.support;

/**
 * Created by Daoud Shaheen on 9/11/2017.
 */
public interface EmailTemplateConstants {
    String ACCOUNT_VERIFICATION = "Account Verification";
    String ACCOUNT_DEACTIVATION = "Account Deactivation";

    String RESET_PASSWORD = "Reset Password";
    String NEW_PASSWORD_CONFIRMATION = "New Password Confirmation";
    String LOVSTAMP_SHARE = "LovStamp Share";
    String BLOG_SHARE = "Blog Share";
    String EVENT_SHARE = "Event Share";
    String EVENT_CANCELLATION = "Event Cancellation";
    String AMBASSADOR_APPROVAL = "Ambassador Approval";
    String AMBASSADOR_DENIAL = "Ambassador Denial";
    String DEVELOPER_APPROVAL = "Developer Approval";
    String DEVELOPER_DENIAL = "Developer Denial";
    String NEW_LIKES = "New Likes";
    String NEW_MATCHES = "New Matches";
    String NEW_MESSAGES = "New Messages";
    String NEW_SONGS = "New Songs";
    String NEW_GIFTS = "New Gifts";
    String GIFT_SENT = "Gift Sent";
    String GIFT_REDEEM = "Gift Redeem";
    String GIFT_SOLD = "Gift Sold";
    String GIFT_RECEIVED = "Gift Received";
    String PHOTO_UNLOCK_REQUESTS = "Photo Unlock Requests";
    String SUBMITTED_BLOG_APPROVAL = "Submitted Blog Approval";
    String SUBMITTED_BLOG_DENIAL = "Submitted Blog Denial";
    String SUBMITTED_AD_APPROVAL = "Submitted Ad Approval";
    String SUBMITTED_AD_DENIAL = "Submitted Ad Denial";
    String BLOG_INVITATION = "Blog Invitation";
    String SUBMITTED_MUSIC_APPROVAL = "Submitted Music Approval";
    String SUBMITTED_MUSIC_DENIAL = "Submitted Music Denial";
    String SUBMITTED_EVENT_APPROVAL = "Submitted Event Approval";
    String SUBMITTED_EVENT_DENIAL = "Submitted Event Denial";
    String SUBMITTED_REVIEW_APPROVAL = "Submitted Review Approval";
    String SUBMITTED_REVIEW_DENIAL = "Submitted Review Denial";
    String SUBMITTED_PLACE_APPROVAL = "Submitted Dating Place Approval";
    String SUBMITTED_PLACE_DENIAL = "Submitted Dating Place Denial";
    String PAYMENT_CONFIRMATION = "Payment Confirmation";
    String PHOTO_REJECTED = "Photo Rejected";
    String PHOTO_SKIPPED = "Photo Skipped";
    String PHOTO_MISSING = "Photo Missing";
    String PHOTO_UNLOCK = "Photo Unlock";
    String SUBMITTED_DEAL_APPROVAL = "Submitted Deal Approval";
    String SUBMITTED_DEAL_DENIAL = "Submitted Deal Denial";
    String COUPLE_APPROVAL = "Couple Approval";
    String NEW_NOTIFICATION = "new_notification";
    String VOICE_REJECTED = "Voice Rejected";
}
