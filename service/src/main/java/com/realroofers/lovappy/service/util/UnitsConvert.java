package com.realroofers.lovappy.service.util;

/**
 * Created by Eias Altawil on 5/24/2017
 */
public class UnitsConvert {

    public static Double feetToCentimeter(Integer feet, Integer inch) {
        if (feet == null || inch == null)
            return 0d;

        return ((Double.valueOf(feet)) * 30.48) + ((Double.valueOf(inch)) * 2.54);
    }

    public static Integer[] centimeterToFeet(Double dCentimeter) {
        if (dCentimeter == null)
            return new Integer[]{0, 0};

        int feetPart = (int) Math.floor((dCentimeter / 2.54) / 12);
        int inchesPart = (int) Math.ceil((dCentimeter / 2.54) - (feetPart * 12));

        return new Integer[]{feetPart, inchesPart};
    }
}
