package com.realroofers.lovappy.service.vendor.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.product.model.ProductType;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.vendor.dto.VendorProductTypeDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode
@Table(name = "vendor_product_type")
public class VendorProductType extends BaseEntity<Integer> {

    @JoinColumn(name = "product_type")
    @ManyToOne
    private ProductType productType;

    @JoinColumn(name = "vendor")
    @ManyToOne
    private Vendor vendor;

    @Column(name = "is_approved", columnDefinition = "boolean default false", nullable = false)
    private Boolean approved = false;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;

    @JoinColumn(name = "created_by")
    @ManyToOne
    private User createdBy;

    @JsonIgnore
    @JoinColumn(name = "approved_by")
    @ManyToOne
    private User approvedBy;

    public VendorProductType() {
    }


    public VendorProductType(VendorProductTypeDto vendorProductType) {
        if (vendorProductType.getId() != null) {
            this.setId(vendorProductType.getId());
        }
        if (vendorProductType.getProductType() != null) {
            this.productType = new ProductType(vendorProductType.getProductType());
        }
        if (vendorProductType.getVendor() != null) {
            this.vendor = new Vendor(vendorProductType.getVendor());
        }
        this.approved = vendorProductType.getApproved();
        this.active = vendorProductType.getActive();

        if (vendorProductType.getCreatedBy() != null) {
            this.createdBy = new User(vendorProductType.getCreatedBy().getID());
        }
        if (vendorProductType.getApprovedBy() != null) {
            this.approvedBy = new User(vendorProductType.getApprovedBy().getID());
        }
    }
}
