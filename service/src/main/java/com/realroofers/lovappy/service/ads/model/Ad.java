package com.realroofers.lovappy.service.ads.model;

import com.realroofers.lovappy.service.ads.support.*;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.core.TextAlign;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

/**
 * Created by Eias Altawil on 7/7/2017
 */

@Entity
@Table(name="ad")
@Data
@EqualsAndHashCode
@DynamicUpdate
public class Ad extends BaseEntity<Integer> {

    private String email;

    private Integer length;
    private Boolean promote;

    @Column(name = "coupon_number")
    private String coupon;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile mediaFileCloud;

    @Enumerated(EnumType.STRING)
    private AudioType audioType;

    @Enumerated(EnumType.STRING)
    private AdType adType;

    @Enumerated(EnumType.STRING)
    private AdMediaType mediaType;

    @OneToMany(mappedBy = "ad", cascade = { CascadeType.ALL })
    private List<AdTargetArea> targetAreas;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Enumerated(EnumType.STRING)
    private AgeRange ageRange;

    @ManyToOne
    @JoinColumn(name = "language")
    private Language language;

    @ElementCollection(targetClass = String.class)
    private Map<String, String> preferences;

    @Enumerated(EnumType.STRING)
    private AdStatus status;

    private String url;

    private String text;
    private String textColor;
    @Enumerated(EnumType.STRING)
    private TextAlign textAlign;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile backgroundFileCloud;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile featuredAdBackgroundFileCloud;

    private String backgroundColor;

    @Column(nullable = false, columnDefinition = "BIT(1) default 0")
    private Boolean featured;

    private Double price;

    private String mediaFileUrl;
}
