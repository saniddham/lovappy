package com.realroofers.lovappy.service.user.model;

import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 5/11/2018.
 */
@Embeddable
@EqualsAndHashCode
public class UserRolePK implements Serializable {
    private Role role;
    private User user;

    public UserRolePK() {
    }

    public UserRolePK(User user, Role role) {
        this.role = role;
        this.user = user;
    }
    @ManyToOne
    @JoinColumn(name = "role_id")
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @ManyToOne
    @JoinColumn(name = "user_id")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
