package com.realroofers.lovappy.service.user.repo;

import com.realroofers.lovappy.service.user.model.Role;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.model.UserRolePK;
import com.realroofers.lovappy.service.user.model.UserRoles;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.Set;

/**
 * Created by Daoud Shaheen on 5/11/2018.
 */
public interface UserRolesRepo extends JpaRepository<UserRoles, UserRolePK> , QueryDslPredicateExecutor<UserRoles> {
    Set<UserRoles> findByIdRoleName(String name);

    @Query("SELECT u.id.user From UserRoles u Where u.id.role =:role AND u.id.user.email like %:email%")
    Page<User> findByRoleAndLikeEmail(@Param("role") Role role, @Param("email") String email, Pageable pageable);
    @Query("SELECT u.id.user From UserRoles u Where u.id.role =:role ")
    Page<User> findByRole(@Param("role") Role role, Pageable pageable);



    Page<UserRoles> findByIdRoleIn(Set<Role> roles, Pageable pageable);
}
