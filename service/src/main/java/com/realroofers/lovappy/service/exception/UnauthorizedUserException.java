package com.realroofers.lovappy.service.exception;

/**
 * @author Eias Altawil
 */

public class UnauthorizedUserException extends RuntimeException {
    public UnauthorizedUserException(Throwable cause) {
        super(cause);
    }

    public UnauthorizedUserException(String message) {
        super(message);
    }
}
