package com.realroofers.lovappy.service.datingPlaces.dto.mobile;

import com.realroofers.lovappy.service.datingPlaces.dto.PersonTypeDto;
import com.realroofers.lovappy.service.datingPlaces.support.DatingAgeRange;
import com.realroofers.lovappy.service.datingPlaces.support.PriceRange;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by Darrel Rayen on 8/1/18.
 */
@Data
@EqualsAndHashCode
public class AddDatingPlaceReviewMobileDto implements Serializable {
    private String placeId;
    private String reviewerComment;
    private String reviewerEmail;
    private Double parkingRating;
    private Double parkingAccessRating;
    private Double securityRating;
    private Double overAllRating;
    private Gender suitableGender;
    private DatingAgeRange suitableAgeRange;
    private PriceRange suitablePriceRange;
    private Boolean isGoodForCouple;
    private String googlePlaceId;
    private String reviewZipCode;
    private String reviewerType;
    private String placeName;
    private Collection<PersonTypeDto> personTypes;
}
