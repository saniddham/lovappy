package com.realroofers.lovappy.service.gift.dto;

public interface TopSellingItem {

    Integer getId();

    Double getPrice();

    Long getSale();
}
