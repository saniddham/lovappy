package com.realroofers.lovappy.service.music.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.music.dto.GenreDto;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Created by Eias Altawil on 8/25/17
 */

@Entity
@DynamicUpdate
@EqualsAndHashCode
public class Genre extends BaseEntity<Integer> {

    private String title;
    private Boolean enabled;

    @Column(name = "added_to_survey", columnDefinition = "boolean default false", nullable = false)
    private Boolean addedToSurvey = true;

    public Genre() {

    }

    public Genre(GenreDto genre) {
        if (genre.getId() != null)
            this.setId(genre.getId());
        this.title = genre.getTitle();
        this.enabled = genre.getEnabled();
        this.addedToSurvey = genre.getAddedToSurvey();
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getAddedToSurvey() {
        return addedToSurvey;
    }

    public void setAddedToSurvey(Boolean addedToSurvey) {
        this.addedToSurvey = addedToSurvey;
    }
}
