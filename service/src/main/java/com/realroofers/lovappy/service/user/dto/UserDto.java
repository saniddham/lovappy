package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.lovstamps.model.Lovstamp;
import com.realroofers.lovappy.service.user.model.*;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.validator.UserRegister;
import com.realroofers.lovappy.service.validator.constraint.ZipCode;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@ToString
@Qualifier("user")
@EqualsAndHashCode
public class UserDto implements Serializable {
    Integer ID;

    @NotEmpty(message = "You should provide your email address.", groups = {UserRegister.class})
    @NotNull(message = "You should provide your email address.", groups = {UserRegister.class})
    @Email(message = "This is not a valid email address.", groups = {UserRegister.class})
    private String email;

    @NotEmpty(message = "Password cannot be empty.", groups = {UserRegister.class})
    @NotNull(message = "Password cannot be empty.", groups = {UserRegister.class})
    @Size(min = 6, message = "Min password length is 6.", groups = {UserRegister.class})
    private String password;

    private String confirmPassword;

    private String title;
    private String firstName;
    private String lastName;

    @NotEmpty(message = "Zip Code is required.", groups = {UserRegister.class})
    @NotNull(message = "Zip Code is required.", groups = {UserRegister.class})
    @ZipCode(groups = {UserRegister.class})
    private String zipcode;

    //TODO Check it later
    //@NotNull(message = "Love status is required.", groups = {UserRegister.class})
    private Boolean isInLove = false;

    private UserProfileDto userProfile;

    private Set<RoleDto> roles;

    private List<UserDto> blockedUsers;
    private List<Integer> blockedUsersIDs;

    private List<Integer> likedUserIds;

    private Boolean emailVerified;
    private Boolean mobileVerified;
    private String emailVerificationKey;

    private String socialPlatform;

    private String createdOn;

    private Long myLikesCount;

    private Long likeMeCount;

    private Long matchesCount;

    private Date lastLogin;

    private Boolean banned;

    private Boolean paused;

    private Long numberOfLogins;

    private Boolean isTweeted;

    private Boolean ambassadorApproved;

    private CloudStorageFileDto audioFileCloud;
    private Boolean activated;

    public UserDto(Integer userId, String email, UserProfile userProfile, CloudStorageFile audioFileCloud) {
        this.ID = userId;
        this.email = email;
        this.userProfile = new UserProfileDto(userProfile);
        this.audioFileCloud = new CloudStorageFileDto(audioFileCloud);
    }


    public UserDto(User user, Lovstamp lovstamp) {
        if (user != null) {
            roles = new HashSet<>();
            this.ID = user.getUserId();
            this.email = user.getEmail();
            this.title = user.getTitle();
            this.firstName = user.getFirstName();
            this.lastName = user.getLastName();
            this.myLikesCount = user.getMyLikesCount();
            this.likeMeCount = user.getLikeMeCount();
            this.matchesCount = user.getMatchesCount();
            SimpleDateFormat formatCreateOn = new SimpleDateFormat("MM/dd/yyyy");
            this.createdOn = formatCreateOn.format(user.getCreatedOn() == null ? new Date() : user.getCreatedOn());
            this.numberOfLogins = user.getLoginsCount() == null ? 0 : user.getLoginsCount();
            this.banned = user.getBanned() == null ? false : user.getBanned();
            this.paused = user.getPaused() == null ? false : user.getPaused();

            this.audioFileCloud = lovstamp != null ? new CloudStorageFileDto(lovstamp.getAudioFileCloud()) : null;

            for (UserRoles role : user.getRoles()) {
                this.roles.add(new RoleDto(role.getId().getRole()));
            }
            this.userProfile = new UserProfileDto(user.getUserProfile());


            if (user.getUserProfile() != null) {
                List<User> blocked = user.getUserProfile().getBlockedUsers();

                if (blocked != null) {
                    this.blockedUsers = new ArrayList<>();
                    this.blockedUsersIDs = new ArrayList<>();

                    for (User blockedUser : blocked) {
                        this.blockedUsers.add(new UserDto(blockedUser));
                        this.blockedUsersIDs.add(blockedUser.getUserId());
                    }
                }
            }

            this.activated = user.getActivated();
            this.lastLogin = user.getLastLogin();
            this.emailVerified = user.getEmailVerified();
            this.mobileVerified = user.getMobileVerified();
            this.isTweeted = user.getTweeted() == null ? false : user.getTweeted();

            for (UserRoles role : user.getRoles()) {
                if (role.getId().getRole().getName().equals(Roles.EVENT_AMBASSADOR)) {
                    this.ambassadorApproved = ApprovalStatus.APPROVED.getValue().equals(role.getApprovalStatus());
                }
            }

        }
    }

    public UserDto(User user) {
        if (user != null) {
            roles = new HashSet<>();
            this.ID = user.getUserId();
            this.email = user.getEmail();
            this.title = user.getTitle();
            this.firstName = user.getFirstName();
            this.lastName = user.getLastName();
            this.myLikesCount = user.getMyLikesCount();
            this.likeMeCount = user.getLikeMeCount();
            this.matchesCount = user.getMatchesCount();
            SimpleDateFormat formatCreateOn = new SimpleDateFormat("MM/dd/yyyy");
            this.createdOn = formatCreateOn.format(user.getCreatedOn() == null ? new Date() : user.getCreatedOn());
            this.numberOfLogins = user.getLoginsCount() == null ? 0 : user.getLoginsCount();
            this.banned = user.getBanned() == null ? false : user.getBanned();
            this.paused = user.getPaused() == null ? false : user.getPaused();
            this.activated = user.getActivated();

            for (UserRoles role : user.getRoles()) {
                this.roles.add(new RoleDto(role.getId().getRole()));
            }
            this.userProfile = new UserProfileDto(user.getUserProfile());


            if (user.getUserProfile() != null) {
                List<User> blocked = user.getUserProfile().getBlockedUsers();

                if (blocked != null) {
                    this.blockedUsers = new ArrayList<>();
                    this.blockedUsersIDs = new ArrayList<>();

                    for (User blockedUser : blocked) {
                        this.blockedUsers.add(new UserDto(blockedUser));
                        this.blockedUsersIDs.add(blockedUser.getUserId());
                    }
                }
            }


            this.lastLogin = user.getLastLogin();
            this.emailVerified = user.getEmailVerified();
            this.mobileVerified = user.getMobileVerified();
            this.isTweeted = user.getTweeted() == null ? false : user.getTweeted();
            for (UserRoles role : user.getRoles()) {
                if (role.getId().getRole().getName().equals(Roles.EVENT_AMBASSADOR)) {
                    this.ambassadorApproved = ApprovalStatus.APPROVED.getValue().equals(role.getApprovalStatus());
                }
            }
        }
    }

    public Boolean getMobileVerified() {
        return mobileVerified;
    }

    public void setMobileVerified(Boolean mobileVerified) {
        this.mobileVerified = mobileVerified;
    }

    public Boolean getAmbassadorApproved() {
        return ambassadorApproved != null ? ambassadorApproved : false;
    }

    public void setAmbassadorApproved(Boolean ambassadorApproved) {
        this.ambassadorApproved = ambassadorApproved;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public UserDto() {
        super();
    }

    public UserDto(Integer ID) {
        this.ID = ID;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<RoleDto> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleDto> roles) {
        this.roles = roles;
    }

    public UserProfileDto getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfileDto userProfile) {
        this.userProfile = userProfile;
    }

    public List<UserDto> getBlockedUsers() {
        return blockedUsers;
    }

    public void setBlockedUsers(List<UserDto> blockedUsers) {
        this.blockedUsers = blockedUsers;
    }

    public List<Integer> getBlockedUsersIDs() {
        return blockedUsersIDs;
    }

    public void setBlockedUsersIDs(List<Integer> blockedUsersIDs) {
        this.blockedUsersIDs = blockedUsersIDs;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public Boolean getIsInLove() {
        return isInLove;
    }

    public void setIsInLove(Boolean inLove) {
        isInLove = inLove;
    }

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getEmailVerificationKey() {
        return emailVerificationKey;
    }

    public void setEmailVerificationKey(String emailVerificationKey) {
        this.emailVerificationKey = emailVerificationKey;
    }

    public String getSocialPlatform() {
        return socialPlatform;
    }

    public void setSocialPlatform(String socialPlatform) {
        this.socialPlatform = socialPlatform;
    }

    public Long getMyLikesCount() {
        return myLikesCount;
    }

    public void setMyLikesCount(Long myLikesCount) {
        this.myLikesCount = myLikesCount;
    }

    public Long getLikeMeCount() {
        return likeMeCount;
    }

    public void setLikeMeCount(Long likeMeCount) {
        this.likeMeCount = likeMeCount;
    }

    public Long getMatchesCount() {
        return matchesCount;
    }

    public void setMatchesCount(Long matchesCount) {
        this.matchesCount = matchesCount;
    }

    public List<Integer> getLikedUserIds() {
        return likedUserIds;
    }

    public void setLikedUserIds(List<Integer> likedUserIds) {
        this.likedUserIds = likedUserIds;
    }

    public Boolean getBanned() {
        return banned;
    }

    public void setBanned(Boolean banned) {
        this.banned = banned;
    }

    public Boolean getPaused() {
        return paused;
    }

    public void setPaused(Boolean paused) {
        this.paused = paused;
    }

    public Long getNumberOfLogins() {
        return numberOfLogins;
    }

    public void setNumberOfLogins(Long numberOfLogins) {
        this.numberOfLogins = numberOfLogins;
    }

    public Boolean getTweeted() {
        return isTweeted;
    }

    public void setTweeted(Boolean tweeted) {
        isTweeted = tweeted;
    }

    public String getAbbRoles() {
        return StringUtils.collectionToCommaDelimitedString(roles.stream().map(abb -> {
            return abb.getRoleAbb();
        }).collect(Collectors.toSet()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ID == null) ? 0 : ID.hashCode());
        result = prime * result + ((blockedUsers == null) ? 0 : blockedUsers.hashCode());
        result = prime * result + ((blockedUsersIDs == null) ? 0 : blockedUsersIDs.hashCode());
//		result = prime * result + ((confirmPassword == null) ? 0 : confirmPassword.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((roles == null) ? 0 : roles.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        result = prime * result + ((userProfile == null) ? 0 : userProfile.hashCode());
        result = prime * result + ((zipcode == null) ? 0 : zipcode.hashCode());
        //    result = prime * result + (isInLove ? 0 : isInLove.hashCode());
        return result;
    }

//	public Set<Lovdrop> getListenLovdropSet() {
//		return listenLovdropSet;
//	}
//
//	public void setListenLovdropSet(Set<Lovdrop> listenLovdropSet) {
//		this.listenLovdropSet = listenLovdropSet;
//	}


    public CloudStorageFileDto getAudioFileCloud() {
        return audioFileCloud;
    }

    public void setAudioFileCloud(CloudStorageFileDto audioFileCloud) {
        this.audioFileCloud = audioFileCloud;
    }

    public Boolean registrationEqualsLastupdated() {
        Date createdDate = null;
        try {
            createdDate = new SimpleDateFormat("MM/dd/yyyy").parse(getCreatedOn());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat compareDatesFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (createdDate != null)
                createdDate = compareDatesFormat.parse(compareDatesFormat.format(createdDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date lastLoginDate = getLastLogin();
        try {
            if (lastLoginDate != null)
                lastLoginDate = compareDatesFormat.parse(compareDatesFormat.format(lastLoginDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if ((createdDate == null) || (lastLoginDate == null)) return false;
        return createdDate.compareTo(lastLoginDate) == 0;

    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }
}
