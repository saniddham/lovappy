package com.realroofers.lovappy.service.music.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 9/26/2017.
 */
@EqualsAndHashCode
@Data
public class MusicSubmitDto implements Serializable{

    @NotNull
    private String title;

    @NotNull
    private Integer genreId;

    @NotNull
    private String lyrics;

    @NotNull
    private String decade;

    @NotNull
    private String artistName;

    private boolean advertised;

    private boolean confirmed;

    public MusicSubmitDto() {
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
