package com.realroofers.lovappy.service.gift.impl;

import com.realroofers.lovappy.service.gift.GiftService;
import com.realroofers.lovappy.service.gift.dto.GiftDto;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.CouponDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.repo.CouponRepo;
import com.realroofers.lovappy.service.order.repo.OrderRepo;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.product.dto.ProductDto;
import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.product.repo.ProductRepo;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Eias Altawil on 5/7/17
 */

@Service
public class GiftServiceImpl implements GiftService{

    private ProductRepo giftRepo;
    private UserRepo userRepo;
    private OrderRepo orderRepo;
    private final OrderService orderService;
    private final CouponRepo couponRepo;

    @Autowired
    public GiftServiceImpl(ProductRepo giftRepo, UserRepo userRepo, OrderRepo orderRepo, OrderService orderService, CouponRepo couponRepo){
        this.giftRepo = giftRepo;
        this.userRepo = userRepo;
        this.orderRepo = orderRepo;
        this.orderService = orderService;
        this.couponRepo = couponRepo;
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<GiftDto> getAllGifts() {
        Collection<GiftDto> gifts = new ArrayList<>();

        Iterable<ProductDto> all = giftRepo.findAllByActiveIsTrueAndApprovedIsTrueAndBlockedIsFalseOrderByProductName();
        for (ProductDto gift : all)
            gifts.add(new GiftDto(gift));

        return gifts;
    }

    @Override
    @Transactional(readOnly = true)
    public GiftDto getGiftByID(Integer giftID) {
        ProductDto gift = new ProductDto(giftRepo.findOne(giftID));
        return (gift == null ? null : new GiftDto(gift));
    }


    @Override
    @Transactional
    public OrderDto purchaseGift(Integer fromUserID, Integer toUserID, Integer giftID, Integer quantity, String coupon, PaymentMethodType paymentMethodType) {
        Product gift = giftRepo.findOne(giftID);

        return orderService.addOrder(gift, OrderDetailType.GIFT, gift.getPrice().doubleValue(), 1,
                fromUserID,coupon, paymentMethodType);
    }

    @Transactional(readOnly = true)
    @Override
    public CalculatePriceDto calculatePrice(GiftDto gift, String couponCode) {
        Double price = gift.getPrice();
        Double totalPrice = gift.getPrice();

        Double discountValue = 0d;
        CouponDto coupon = orderService.getCoupon(couponCode, CouponCategory.GIFT);
        if(coupon != null)
            discountValue = totalPrice * coupon.getDiscountPercent() * 0.01;

        totalPrice -= discountValue;

        return new CalculatePriceDto(price, coupon, totalPrice);
    }
}
