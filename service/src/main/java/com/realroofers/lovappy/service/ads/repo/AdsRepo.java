package com.realroofers.lovappy.service.ads.repo;

import com.realroofers.lovappy.service.ads.model.Ad;
import com.realroofers.lovappy.service.ads.support.AdStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Eias Altawil on 7/7/2017
 */
public interface AdsRepo extends JpaRepository<Ad, Integer>, QueryDslPredicateExecutor<Ad> {
    List<Ad> findTop2ByOrderByIdDesc();

    Page<Ad> findByFeaturedAndStatus(Boolean featured, AdStatus status,  Pageable pageable);

    @Query(value = "select ads from Ad ads Where ads.status=:status AND ads.featured=:featured order by rand()")
    Page<Ad> findAllRandom(@Param("featured") Boolean featured, @Param("status") AdStatus status, Pageable pageable);
}
