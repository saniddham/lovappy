package com.realroofers.lovappy.service.music.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class MusicResponse implements Serializable{

    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "music_exchange")
    private MusicExchange musicExchange;

    @Column(name = "response_date")
    private Date responseDate;

    @Enumerated(EnumType.STRING)
    private MusicResponseType type;

    private String response;

    private Boolean seen;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MusicExchange getMusicExchange() {
        return musicExchange;
    }

    public void setMusicExchange(MusicExchange musicExchange) {
        this.musicExchange = musicExchange;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    public MusicResponseType getType() {
        return type;
    }

    public void setType(MusicResponseType type) {
        this.type = type;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Boolean getSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }
}
