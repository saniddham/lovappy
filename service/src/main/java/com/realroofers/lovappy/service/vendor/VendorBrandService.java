package com.realroofers.lovappy.service.vendor;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.vendor.dto.VendorBrandDto;

public interface VendorBrandService extends AbstractService<VendorBrandDto, Integer> {
}
