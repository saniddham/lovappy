package com.realroofers.lovappy.service.radio.model;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.radio.support.StartSoundType;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name="start_sound")
@EqualsAndHashCode
public class StartSound extends BaseEntity<Integer>{

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile audioFileCloud;

    @Column(name = "sound_type", unique = true)
    @Enumerated(EnumType.STRING)
    private StartSoundType soundType;

    public CloudStorageFile getAudioFileCloud() {
        return audioFileCloud;
    }

    public void setAudioFileCloud(CloudStorageFile audioFileCloud) {
        this.audioFileCloud = audioFileCloud;
    }

    public StartSoundType getSoundType() {
        return soundType;
    }

    public void setSoundType(StartSoundType soundType) {
        this.soundType = soundType;
    }
}
