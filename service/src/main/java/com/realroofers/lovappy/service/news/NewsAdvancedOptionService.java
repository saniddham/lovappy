package com.realroofers.lovappy.service.news;

import com.realroofers.lovappy.service.news.model.NewsAdvancedOption;

public interface NewsAdvancedOptionService {
    public NewsAdvancedOption save(NewsAdvancedOption newsAdvancedOption);
    public NewsAdvancedOption getOneBystoryId(Long id);
}
