package com.realroofers.lovappy.service.news.repo;

import com.realroofers.lovappy.service.news.model.NewsCategory;
import com.realroofers.lovappy.service.news.model.NewsStory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NewsStoryRepo extends JpaRepository<NewsStory, Long> {

    @Query(value = "SELECT ns FROM NewsStory ns WHERE ns.title =:title")
    public NewsStory getNewsStoryByTitle(@Param("title") String title);


    @Query(value = "SELECT ns FROM NewsStory ns WHERE ns.posted=true AND ns.title like %:searchtext% OR ns.contain like %:searchtext%")
    public List<NewsStory> findNewsStoriesByTitleIsLikeAndContainIsLike(@Param("searchtext") String searchText);

    public NewsStory findTopByOrderByIdDesc();

    @Query(value = "SELECT ns FROM NewsStory ns WHERE ns.posted=true ")
    public List<NewsStory> getApprovedNews();

    @Query(value = "SELECT ns FROM NewsStory ns WHERE ns.posted=true ORDER BY ns.submitDate ")
    public List<NewsStory> getLatestNews();

    public List<NewsStory> getNewsStoryByNewsCategoryEquals(NewsCategory newsCategory);
}
