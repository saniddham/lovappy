package com.realroofers.lovappy.service.radio.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Manoj on 17/12/2017.
 */

@Data
@Entity
@Table(name = "radio_audio_type")
@EqualsAndHashCode
public class AudioType extends BaseEntity<Integer> implements Serializable {

    @Column(name = "type_name")
    private String name;

    @Column(name = "type_description")
    private String description;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;


    public AudioType() {
    }

    public AudioType(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return "AudioType{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", active=" + getActive() +
                '}';
    }
}
