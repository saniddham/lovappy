package com.realroofers.lovappy.service.gift.impl;

import com.realroofers.lovappy.service.gift.PurchasedGiftService;
import com.realroofers.lovappy.service.gift.dto.PurchasedGiftDto;
import com.realroofers.lovappy.service.gift.model.PurchasedGift;
import com.realroofers.lovappy.service.gift.repo.PurchasedGiftRepo;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PurchasedGiftServiceImpl implements PurchasedGiftService {

    private final PurchasedGiftRepo purchasedGiftRepo;

    public PurchasedGiftServiceImpl(PurchasedGiftRepo purchasedGiftRepo) {
        this.purchasedGiftRepo = purchasedGiftRepo;
    }


    @Override
    //fix Pagination
    @Transactional(readOnly = true)
    public List<PurchasedGiftDto> findAll() {
        List<PurchasedGiftDto> purchasedGiftDtoList = new ArrayList<>();

        purchasedGiftRepo.findAll().stream().forEach(purchasedGift -> {
            purchasedGiftDtoList.add(new PurchasedGiftDto(purchasedGift));
        });

        return purchasedGiftDtoList;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PurchasedGiftDto> findAllActive() {
        List<PurchasedGiftDto> purchasedGiftDtoList = new ArrayList<>();

        purchasedGiftRepo.findAll().stream().forEach(purchasedGift -> {
            purchasedGiftDtoList.add(new PurchasedGiftDto(purchasedGift));
        });

        return purchasedGiftDtoList;
    }

    @Override
    @Transactional
    public PurchasedGiftDto create(PurchasedGiftDto purchasedGiftDto) {
        PurchasedGift purchasedGift = purchasedGiftRepo.findTopByUserAndProductOrderByPurchasedOnDesc(new User(purchasedGiftDto.getUser().getID()), purchasedGiftDto.getProduct());
        if (purchasedGift != null) {
            purchasedGift.setPurchasedOn(new Date());
            purchasedGift = purchasedGiftRepo.save(purchasedGift);
        } else {
            purchasedGiftDto.setPurchasedOn(new Date());
            purchasedGift = purchasedGiftRepo.saveAndFlush(new PurchasedGift(purchasedGiftDto));
            if (purchasedGiftRepo.count() > 15) {
                PurchasedGift gift = purchasedGiftRepo.findTopByUserOrderByPurchasedOnDesc(purchasedGift.getUser());
                delete(gift.getId());
            }
        }

        return new PurchasedGiftDto(purchasedGift);
    }

    @Override
    @Transactional
    public PurchasedGiftDto update(PurchasedGiftDto purchasedGiftDto) {
        PurchasedGift purchasedGift = purchasedGiftRepo.save(new PurchasedGift(purchasedGiftDto));
        return new PurchasedGiftDto(purchasedGift);
    }

    @Override
    @Transactional
    @Modifying
    public void delete(Integer id) {
        purchasedGiftRepo.delete(id);
    }

    @Override
    @Transactional(readOnly = true)
    public PurchasedGiftDto findById(Integer id) {

        PurchasedGift gift = purchasedGiftRepo.findOne(id);

        if (gift != null) {
            return new PurchasedGiftDto(gift);
        }

        return null;
    }
}
