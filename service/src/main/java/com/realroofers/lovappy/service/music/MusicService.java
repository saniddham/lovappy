package com.realroofers.lovappy.service.music;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.music.dto.*;
import com.realroofers.lovappy.service.music.model.MusicExchange;
import com.realroofers.lovappy.service.music.support.MusicDownload;
import com.realroofers.lovappy.service.music.support.MusicProvider;
import com.realroofers.lovappy.service.music.support.MusicStatus;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.CouponDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.user.support.Gender;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

/**
 * Created by Eias Altawil on 8/25/17
 */
public interface MusicService {

    void create(MusicDto music, MusicProvider provider, Integer authorId);

    MusicDto create(MusicSubmitDto musicSubmit, Integer authorId);

    MusicDto update(Integer musicId, MusicSubmitDto musicSubmit);

    void delete(Integer musicId);

    MusicDto addFile(Integer musicId, CloudStorageFileDto file, CloudStorageFileDto demoFile);

    MusicDto addCoverPhoto(Integer musicId, CloudStorageFileDto file);

    Page<MusicDto> getAll(Pageable pageable);

    MusicDto getMusic(Integer id);

    MusicDto getMusicWithRating(Integer id,Integer userID);

    CloudStorageFileDto getMusicFile(Integer musicId);

    Page<MusicExchangeDto> getMusicExchange(Pageable pageable);

    MusicExchange getMusicExchange(Integer musicExchangeId);

    List<GenreDto> getAllGenres();
    List<GenreDto> getAllEnabledGenres();
    GenreDto getGenre(Integer id);

    List<GenreDto> findByAddedToSurveyIsFalse();

    GenreDto addGenre(GenreDto genre);
    GenreDto addGenreIfNotExist(GenreDto genreDto);
    GenreDto updateGenre(GenreDto genre);

    MusicDto updateStatus(Integer id, MusicStatus status);

    Page<MusicDto> getMostPopularPurchased(Pageable pageable);

    Page<MusicDto> getRecentlyPurchased(Pageable pageable);

    Page<MusicDto> findByFilterAndKeyword(String filter, String keyword, Pageable pageable);

    void rateMusic(Integer musicID, Integer userID, Integer rating);

    boolean isPurchased(Integer musicID, Integer userID);

    MusicUserDto getMusicUserDetails(Integer musicID, Integer userID);

    Page<MusicSubDetailsDto> getTopByLovappyUploader(Pageable pageable);

    Page<MusicSubDetailsDto> getTopByMusicianUploader(Pageable pageable);

    Page<MusicDto> getAllApproved(Pageable pageable);

    Page<MusicDto> getAllByAuthor(Integer authorId, Pageable pageable);

    Page<MusicDto> getAllByKeywordAndAuthor(String keyword, String email, Pageable pageable);

    MusicDto getApprovedMusic(Integer id);

    MusicExchangeDto send(Integer senderId, Integer toUserId, Integer musicId, String lyrics);

    CalculatePriceDto calculatePrice(Integer musicId, CouponDto coupon);

    OrderDto order(Integer musicId, String couponCode, PaymentMethodType paymentMethodType);

    void setPurchased(Integer musicID, Integer userID);

    void setDownloadStatus(Integer musicID, Integer userID, MusicDownload downloadBy);

    void setReceived(Integer musicID, Integer userID);

    void setUserResponded(Integer musicID);

    Integer countMusicSent(Integer userId);

    Integer countUnSeenMusicReceived(Integer userId);

    Integer countMusicReceived(Integer userId);

    Integer countMusicSentBetweenFromAndToUser(Integer fromUser, Integer toUser);

    Integer countSongsByDateBetween(Date fromDate, Date toDate);

    Integer countSendingMusicByFromUserAndGenderAndSendDateBetween(Integer userId, Gender gender, Date fromDate, Date toDate);

    Page<MusicExchangeDto> getMusicExchangeByFromUser(Integer fromUser, Pageable pageable);

    Page<MusicExchangeDto> getMusicExchangeByToUser(Integer toUser, Pageable pageable);

    Integer countMusicSentBetweenFromAndToUserAndMusic(Integer fromUser, Integer toUser, Integer musicId);

    void deleteAll();

    void setMusicNotificationSeen(Integer userId, Integer sendBy, Integer musicId);

    Page<MusicDto> getAllMusic(PageRequest pageRequest, String searchText, int genre, String decade);

    void updateMusicExchange(MusicExchange musicExchange);
}
