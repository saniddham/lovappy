package com.realroofers.lovappy.service.order.impl;

import com.realroofers.lovappy.service.ads.model.Ad;
import com.realroofers.lovappy.service.ads.repo.AdsRepo;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlace;
import com.realroofers.lovappy.service.datingPlaces.repo.DatingPlaceRepo;
import com.realroofers.lovappy.service.event.model.Event;
import com.realroofers.lovappy.service.event.repo.EventRepo;
import com.realroofers.lovappy.service.music.model.Music;
import com.realroofers.lovappy.service.music.repo.MusicRepo;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CouponDto;
import com.realroofers.lovappy.service.order.dto.CouponRuleDto;
import com.realroofers.lovappy.service.order.dto.OrderDetailDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.model.*;
import com.realroofers.lovappy.service.order.repo.*;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.privatemessage.model.PrivateMessage;
import com.realroofers.lovappy.service.privatemessage.repo.PrivateMessageRepo;
import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.product.repo.ProductRepo;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import com.realroofers.lovappy.service.util.ListMapper;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by Eias Altawil on 5/11/17
 */

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepo orderRepo;
    private final OrderDetailRepo orderDetailRepo;
    private final UserRepo userRepo;
    private final CouponRepo couponRepo;
    private final PriceRepo priceRepo;
    private final MusicRepo musicRepo;
    private final PrivateMessageRepo privateMessageRepo;
    private final ProductRepo productRepo;
    private final AdsRepo adsRepo;
    private final EventRepo eventRepo;
    private final DatingPlaceRepo datingPlaceRepo;
    private final CouponRuleRepo couponRuleRepo;

    @Autowired
    public OrderServiceImpl(OrderRepo orderRepo, OrderDetailRepo orderDetailRepo, UserRepo userRepo, CouponRepo couponRepo, PriceRepo priceRepo, MusicRepo musicRepo, PrivateMessageRepo privateMessageRepo, ProductRepo productRepo, AdsRepo adsRepo, EventRepo eventRepo, DatingPlaceRepo datingPlaceRepo, CouponRuleRepo couponRuleRepo) {
        this.orderRepo = orderRepo;
        this.orderDetailRepo = orderDetailRepo;
        this.userRepo = userRepo;
        this.couponRepo = couponRepo;
        this.priceRepo = priceRepo;
        this.musicRepo = musicRepo;
        this.privateMessageRepo = privateMessageRepo;
        this.productRepo = productRepo;
        this.adsRepo = adsRepo;
        this.eventRepo = eventRepo;
        this.datingPlaceRepo = datingPlaceRepo;
        this.couponRuleRepo = couponRuleRepo;
    }

    @Deprecated
    @Transactional
    @Override
    public OrderDto addOrder(Object object, OrderDetailType orderDetailType, Double price, Integer quantity, Integer userID,
                             String couponCode, PaymentMethodType paymentMethod) {
        OrderDetail orderDetail = new OrderDetail();

        Price priceObj = priceRepo.findByType(orderDetailType);
        if (price == null || price == 0d) {
            price = priceObj.getPrice();
        }
        CouponCategory couponCategory = null;
        switch (orderDetailType) {
            case AD:
                orderDetail.setOriginalId(((Ad) object).getId().longValue());
                couponCategory = CouponCategory.AD;
                break;

            case PRIVATE_MESSAGE:
                orderDetail.setOriginalId(((PrivateMessage) object).getId().longValue());
                couponCategory = CouponCategory.MESSAGE;
                break;

            case MUSIC:
                orderDetail.setOriginalId(((Music) object).getId().longValue());
                couponCategory = CouponCategory.MUSIC;
                break;

            case GIFT:
                orderDetail.setOriginalId(((Product) object).getId().longValue());
                couponCategory = CouponCategory.GIFT;
                break;

            case EVENT_ATTEND:
                orderDetail.setOriginalId(((Event) object).getEventId().longValue());
                couponCategory = CouponCategory.EVENT_ATTEND;
                break;

            case DATING_PLACE:
                orderDetail.setOriginalId(((DatingPlace) object).getDatingPlacesId().longValue());
                couponCategory = CouponCategory.DATING_PLACE;
        }

        orderDetail.setType(orderDetailType);
        orderDetail.setPrice(price);
        orderDetail.setQuantity(quantity);

        Collection<OrderDetail> orderDetails = new ArrayList<>();
        orderDetails.add(orderDetailRepo.save(orderDetail));

        ProductOrder order = new ProductOrder();

        if (userID != null)
            order.setUser(userRepo.findOne(userID));

        order.setDate(new Date());
        order.setOrdersDetails(orderDetails);

        //Set coupon
        CouponDto coupon = getCoupon(couponCode, couponCategory);
        if (coupon != null)
            order.setCoupon(couponRepo.findOne(coupon.getId()));
        order.setPaymentMethod(paymentMethod);
        order.setTotalPrice(price);
        ProductOrder savedOrder = orderRepo.save(order);


        return savedOrder == null ? null : new OrderDto(savedOrder);
    }

    @Transactional
    @Override
    public OrderDto addOrder(Integer productId, OrderDetailType orderDetailType, Double price,
                             Integer quantity, Integer userID, String couponCode, String transactionId, PaymentMethodType paymentMethod,
                             String cardNo,String cardType) {

        Price priceObj = priceRepo.findByType(orderDetailType);
        if (price == null || price == 0d) {
            price = priceObj.getPrice();
        }
        CouponCategory couponCategory = null;
        switch (orderDetailType) {
            case AD:
                couponCategory = CouponCategory.AD;
                break;

            case PRIVATE_MESSAGE:

                couponCategory = CouponCategory.MESSAGE;
                break;

            case MUSIC:

                couponCategory = CouponCategory.MUSIC;
                break;

            case GIFT:
                Product gift = productRepo.getOne(productId);
                price = gift.getPrice().doubleValue();
                couponCategory = CouponCategory.GIFT;
                break;

            case EVENT_ATTEND:
                couponCategory = CouponCategory.EVENT_ATTEND;
                break;

            case DATING_PLACE:
                couponCategory = CouponCategory.DATING_PLACE;
                break;

            case CREDITS:
                couponCategory = CouponCategory.CREDITS;
                break;

            case DAILY_QUESTION:
                couponCategory = CouponCategory.DAILY_QUESTION;
                break;
        }

        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setOriginalId(productId.longValue());
        orderDetail.setType(orderDetailType);
        orderDetail.setPrice(price);
        orderDetail.setQuantity(quantity);

        Collection<OrderDetail> orderDetails = new ArrayList<>();
        orderDetails.add(orderDetailRepo.save(orderDetail));

        ProductOrder order = new ProductOrder();

        if (userID != null)
            order.setUser(userRepo.findOne(userID));

        order.setDate(new Date());
        order.setOrdersDetails(orderDetails);
        order.setCardNo(cardNo);
        order.setCardType(cardType);

        //Set coupon
        CouponDto coupon = getCoupon(couponCode, couponCategory);
        if (coupon != null)
            order.setCoupon(couponRepo.findOne(coupon.getId()));
        order.setPaymentMethod(paymentMethod);
        order.setTotalPrice(price);
        order.setTransactionID(transactionId);
        ProductOrder savedOrder = orderRepo.save(order);

        return savedOrder == null ? null : new OrderDto(savedOrder);
    }
    @Transactional
    @Override
    public OrderDto addOrder(List<OrderDetailDto> orderItems, Double totalPrice,
                              Integer userID, String couponCode, String transactionId, PaymentMethodType paymentMethod) {
        CouponCategory couponCategory = null;
        Collection<OrderDetail> orderDetails = new ArrayList<>();
        CouponDto coupon = null;
        for (OrderDetailDto orderDetailDto : orderItems ) {
            Price priceObj = priceRepo.findByType(orderDetailDto.getType());
            Double price = priceObj==null? orderDetailDto.getType().getDefaultPrice():priceObj.getPrice();

        switch (orderDetailDto.getType()) {
            case AD:
                couponCategory = CouponCategory.AD;
                break;

            case PRIVATE_MESSAGE:

                couponCategory = CouponCategory.MESSAGE;
                break;

            case MUSIC:

                couponCategory = CouponCategory.MUSIC;
                break;

            case GIFT:
                Product gift = productRepo.getOne(orderDetailDto.getOriginalId().intValue());
                price = gift.getPrice().doubleValue();
                couponCategory = CouponCategory.GIFT;
                break;

            case EVENT_ATTEND:
                couponCategory = CouponCategory.EVENT_ATTEND;
                break;

            case DATING_PLACE:
                couponCategory = CouponCategory.DATING_PLACE;
                break;

            case CREDITS:
                couponCategory = CouponCategory.CREDITS;
                break;

            case DAILY_QUESTION:
                couponCategory = CouponCategory.DAILY_QUESTION;
                break;
        }
        if(coupon == null) {
            coupon = getCoupon(couponCode, couponCategory);
        }
        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setOriginalId(orderDetailDto.getOriginalId());
        orderDetail.setType(orderDetailDto.getType());
        orderDetail.setPrice(price);
        orderDetail.setQuantity(orderDetailDto.getQuantity());

            orderDetails.add(orderDetailRepo.save(orderDetail));
        }



        ProductOrder order = new ProductOrder();

        if (userID != null)
            order.setUser(userRepo.findOne(userID));

        order.setDate(new Date());
        order.setOrdersDetails(orderDetails);

        //Set coupon

        if (coupon != null)
            order.setCoupon(couponRepo.findOne(coupon.getId()));
        order.setPaymentMethod(paymentMethod);
        order.setTotalPrice(totalPrice);
        order.setTransactionID(transactionId);
        ProductOrder savedOrder = orderRepo.save(order);

        return savedOrder == null ? null : new OrderDto(savedOrder);
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<OrderDto> getOrdersByUserID(Integer userID) {
        Collection<OrderDto> orders = new ArrayList<>();

        List<ProductOrder> productOrders = orderRepo.findAllByUserOrderByDateDesc(userRepo.findOne(userID));
        for (ProductOrder order : productOrders) {
            orders.add(new OrderDto(order));
        }

        return orders;
    }

    @Override
    @Transactional(readOnly = true)
    public Double getTotalSpentByUserID(Integer userID) {
        /*List list = orderRepo.getTotalSpentByUserID(userID);
        if (list != null && list.size() > 0)
            return (Double) list.get(0);*/

        Double totalSpent = 0d;
        List<ProductOrder> productOrders = orderRepo.findAllByUserOrderByDateDesc(userRepo.findOne(userID));
        for (ProductOrder order : productOrders) {
            if (order.getUser().getUserId().equals(userID)) {
                for (OrderDetail detail : order.getOrdersDetails()) {
                    totalSpent += detail.getPrice();
                }
            }
        }

        return totalSpent;
    }

    @Override
    @Transactional(readOnly = true)
    public OrderDto getOrderByID(Long id) {
        ProductOrder order = orderRepo.findOne(id);
        return order == null ? null : new OrderDto(order);
    }

    @Override
    @Transactional(readOnly = true)
    public OrderDto getOrderByTransactionID(String transactionID) {
        ProductOrder order = orderRepo.findByTransactionID(transactionID);
        return order == null ? null : new OrderDto(order);
    }

    @Override
    @Transactional
    public OrderDto setOrderTransactionID(Long orderID, String transactionID) {
        ProductOrder order = orderRepo.findOne(orderID);
        if (order != null) {
            order.setTransactionID(transactionID);
            ProductOrder save = orderRepo.save(order);
            return save == null ? null : new OrderDto(save);
        }
        return null;
    }

    @Transactional(readOnly = true)
    @Override
    public List<CouponDto> getAllCoupons() {
        List<Coupon> all = couponRepo.findAll();
        return new ListMapper<>(all).map(CouponDto::new);
    }

    @Transactional(readOnly = true)
    @Override
    public CouponDto getCoupon(Integer id) {
        Coupon coupon = couponRepo.findOne(id);
        return coupon == null ? null : new CouponDto(coupon);
    }

    @Transactional(readOnly = true)
    @Override
    public CouponDto getCoupon(String code, CouponCategory category) {
        Coupon coupon = couponRepo.findByCouponCodeAndCategoryAndExpiryDateAfter(
                code, category, DateUtils.addDays(new Date(), -1));
        return coupon == null ? null : new CouponDto(coupon);
    }

    @Transactional
    @Override
    public CouponDto saveCoupon(CouponDto couponDto) {
        Coupon coupon = null;

        if (couponDto.getId() != null)
            coupon = couponRepo.findOne(couponDto.getId());

        if (coupon == null)
            coupon = new Coupon();

        coupon.setCouponCode(couponDto.getCouponCode());
        coupon.setCategory(couponDto.getCategory());
        coupon.setDiscountPercent(couponDto.getDiscountPercent());
        coupon.setExpiryDate(couponDto.getExpiryDate());
        coupon.setDescription(couponDto.getDescription());

        Coupon save = couponRepo.save(coupon);

        return save == null ? null : new CouponDto(save);
    }

    @Transactional
    @Override
    public void revertOrder(Long id) {
        orderRepo.delete(id);
    }

    //region Coupons Rules
    @Transactional
    @Override
    public CouponRuleDto saveRule(CouponRuleDto couponRuleDto) {
        CouponRule couponRule = null;
        if (couponRuleDto.getId() != null)
            couponRule = couponRuleRepo.findOne(couponRuleDto.getId());

        if (couponRule == null)
            couponRule = new CouponRule();

        couponRule.setActionType(couponRuleDto.getActionType());
        couponRule.setAffectedCategory(couponRuleDto.getAffectedCategory());
        couponRule.setCreationDate(couponRuleDto.getCreationDate());
        couponRule.setDiscountValue(couponRuleDto.getDiscountValue());
        couponRule.setExpiryDate(couponRuleDto.getExpiryDate());
        couponRule.setRuleDescription(couponRuleDto.getRuleDescription());
        couponRuleRepo.save(couponRule);
        return new CouponRuleDto(couponRule);
    }

    @Transactional(readOnly = true)
    @Override
    public CouponRuleDto getCouponRule(Integer id) {
        CouponRule couponRule = null;
        couponRule = couponRuleRepo.findOne(id);

        return couponRule == null ? new CouponRuleDto() : new CouponRuleDto(couponRule);
    }

    @Transactional(readOnly = true)
    @Override
    public List<CouponRuleDto> getValidCouponRules() {
        return couponRuleRepo.findAllByExpiryDateAfter(new Date());
    }

    @Transactional(readOnly = true)
    @Override
    public List<CouponRuleDto> getValidCouponRulesByCategory(CouponCategory couponCategory) {
        return couponRuleRepo.findAllByExpiryDateAfterAndAffectedCategory(new Date(), couponCategory);
    }

    @Transactional(readOnly = true)
    @Override
    public List<CouponRuleDto> getAllCouponRules() {
        return new ListMapper<>(couponRuleRepo.findAll()).map(CouponRuleDto::new);
    }
    //endregion
}
