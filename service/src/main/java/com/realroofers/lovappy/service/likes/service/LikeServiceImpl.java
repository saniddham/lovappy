package com.realroofers.lovappy.service.likes.service;

import com.realroofers.lovappy.service.exception.ResourceNotFoundException;
import com.realroofers.lovappy.service.likes.LikeService;
import com.realroofers.lovappy.service.likes.dto.LikeDto;
import com.realroofers.lovappy.service.likes.model.Like;
import com.realroofers.lovappy.service.likes.model.LikePK;
import com.realroofers.lovappy.service.likes.repo.LikeRepository;
import com.realroofers.lovappy.service.lovstamps.model.Lovstamp;
import com.realroofers.lovappy.service.lovstamps.repo.LovstampRepo;
import com.realroofers.lovappy.service.user.dto.UserLikesDto;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.repo.UserProfileRepo;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import com.realroofers.lovappy.service.user.support.Gender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by Daoud Shaheen on 7/22/2017.
 */
@Transactional
@Service
public class LikeServiceImpl implements LikeService {

    @Autowired
    private LikeRepository likeRepository;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private LovstampRepo lovstampRepo;

    @Autowired
    private UserProfileRepo userProfileRepo;

    @Override
    @Transactional(readOnly = true)
    public LikeDto getLikeById(Integer userLikeBy, Integer userLikeTo) {

        User likeBy = userRepo.findOne(userLikeBy);
        User likeTo = userRepo.findOne(userLikeTo);

        LikePK id = new LikePK(likeBy, likeTo);
        Like like = likeRepository.findById(id);
        if (like != null)
            return new LikeDto(like);
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserLikesDto> getLikesByUserLikeBy(int page, int limit, Integer userLikeBy) {
        User likeBy = userRepo.findOne(userLikeBy);
        Pageable pageable = new PageRequest(page - 1, limit);
        Page<User> userLikesDtoPage =  userRepo.findByIdLikeBy(likeBy, pageable);
        List<UserLikesDto> userLikesDtoList = new ArrayList<>();
        for (User user: userLikesDtoPage.getContent()) {
            Lovstamp userLovstamp = lovstampRepo.findLovstampByUser(user);

            UserLikesDto userLikesDto = new UserLikesDto(user, userLovstamp, false);
            userLikesDto.setMatched(match(userLikeBy, user.getUserId()));
            userLikesDto.setLikedBy(true);
            //TODO improve
            if(userLovstamp != null)
            userLikesDto.setListened(userRepo.findByUserIdAndListenLovstampSetIn(userLikeBy, new HashSet<>(Collections.singletonList(userLovstamp)))!=null);
            userLikesDtoList.add(userLikesDto);

        }
        return new PageImpl<>(userLikesDtoList, pageable, userLikesDtoPage.getTotalElements());
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserLikesDto> getLikesByUserLikeTo(int page, int limit, Integer userLikeTo) {
        User likeTo = userRepo.findOne(userLikeTo);
        Lovstamp lovstamp = lovstampRepo.findLovstampByUser(likeTo);
        Pageable pageable = new PageRequest(page - 1, limit);
        Page<User> userLikesDtoPage =  userRepo.findByIdLikeTo(likeTo, pageable);
        List<UserLikesDto> userLikesDtoList = new ArrayList<>();
        for (User user: userLikesDtoPage.getContent()) {
            Lovstamp userLovdrop = lovstampRepo.findLovstampByUser(user);
            UserLikesDto userLikesDto = new UserLikesDto(user, userLovdrop, userProfileRepo.checkUserBlockStatus(userLikeTo, user.getUserId())!=null);
            //improve
            if(lovstamp != null) {
                userLikesDto.setListened(userRepo.findByUserIdAndListenLovstampSetIn(user.getUserId(), new HashSet<>(Collections.singletonList(lovstamp))) != null);
            }
            userLikesDtoList.add(userLikesDto);
            userLikesDto.setMatched(match(userLikeTo, user.getUserId()));
            userLikesDto.setLikedBy(userLikesDto.getMatched());
        }
        return new PageImpl<UserLikesDto>(userLikesDtoList, pageable, userLikesDtoPage.getTotalElements());
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserLikesDto> getMatchesForUser(int page, int limit, Integer userId) {
        User user = userRepo.findOne(userId);
        Pageable pageRequest = new PageRequest(page - 1, limit);
        Lovstamp lovstamp = lovstampRepo.findLovstampByUser(user);
        if(user != null) {
            List<User> usersMatched = userRepo.findMatches(userId, pageRequest.getOffset(), pageRequest.getPageSize());
            Integer total = userRepo.countNumberOFmatches(userId);

            List<UserLikesDto> userLikesDtoList = new ArrayList<>();
            for (User userLike: usersMatched) {
                Lovstamp userLovstamp= lovstampRepo.findLovstampByUser(userLike);
                UserLikesDto userLikesDto = new UserLikesDto(userLike, userLovstamp, userProfileRepo.checkUserBlockStatus(userId, userLike.getUserId())!=null);
                //improve
                if(lovstamp != null)
                userLikesDto.setListened(userRepo.findByUserIdAndListenLovstampSetIn(userLike.getUserId(), new HashSet<>(Collections.singletonList(lovstamp)))!=null);
                userLikesDtoList.add(userLikesDto);

            }
            return new PageImpl<UserLikesDto>(userLikesDtoList, pageRequest, total);
        }
        return  new PageImpl<UserLikesDto>(new ArrayList<>(), pageRequest, 0);
    }

    @Override
    @Transactional(readOnly = true)
    public Long countAllLikeFromByGender(Integer likeFrom, Gender gender) {
        return likeRepository.countByLikeFromByGender(likeFrom, gender);
    }

    @Override
    @Transactional(readOnly = true)
    public Long countAllLikeToByGender(Integer likeTo, Gender gender) {
        return likeRepository.countByLikeToByGender(likeTo, gender);
    }

    @Override
    public boolean liketo(Integer currentUserId, Integer userId) {
        return getLikeById(currentUserId, userId) != null;
    }

    @Override
    public boolean likeme(Integer currentUserId, Integer userId) {
        return getLikeById(userId, currentUserId) != null;
    }

    @Override
    public boolean match(Integer currentUserId, Integer userId) {
        return likeme(currentUserId, userId) && liketo(currentUserId, userId);
    }

    @Override
    @Transactional
    public LikeDto addLike(Integer currentUserId, Integer userLikeTo) {
        User likeBy = userRepo.findOne(currentUserId);
        User likeTo = userRepo.findOne(userLikeTo);
        if(likeTo == null) {
            throw new ResourceNotFoundException("User " + userLikeTo + "not found");
        }
        LikePK id = new LikePK(likeBy, likeTo);
        Like like = likeRepository.findById(id);
        if (like == null) {
            like = new Like();
            likeBy.setMyLikesCount(likeBy.getMyLikesCount() == null? 1:likeBy.getMyLikesCount()+1);
            likeTo.setLikeMeCount(likeTo.getLikeMeCount() == null? 1:likeTo.getLikeMeCount()+1);
            //matched
            if(likeme(currentUserId,userLikeTo)){
                likeBy.setMatchesCount(likeBy.getMatchesCount() == null? 1:likeBy.getMatchesCount()+1);
                likeTo.setMatchesCount(likeTo.getMatchesCount() == null? 1:likeTo.getMatchesCount()+1);
            }
            like.setId(id);
            return new LikeDto(likeRepository.save(like));

        }
        return null;
    }

    @Override
    @Transactional
    public void unLike(Integer likeBy, Integer liketo) {
        User likeByUser = userRepo.findOne(likeBy);
        User likeToUser = userRepo.findOne(liketo);
        if(likeToUser == null) {
            throw new ResourceNotFoundException("User " + likeToUser + "not found");
        }
        LikePK id = new LikePK(likeByUser, likeToUser);
        Like like = likeRepository.findById(id);
        if (like != null) {

            if(match(likeBy, liketo)){
                likeByUser.setMatchesCount(likeByUser.getMatchesCount() != null &&likeByUser.getMatchesCount() >0 ? likeByUser.getMatchesCount()-1:0);
                likeToUser.setMatchesCount(likeToUser.getMatchesCount() != null &&likeToUser.getMatchesCount() >0 ? likeToUser.getMatchesCount()-1:0);
            }


            likeByUser.setMyLikesCount(likeByUser.getMyLikesCount() == null? 0:likeByUser.getMyLikesCount()-1);
            likeToUser.setLikeMeCount(likeToUser.getLikeMeCount() == null? 0:likeToUser.getLikeMeCount()-1);

            //save to save user counters
            likeRepository.save(like);
            likeRepository.delete(like);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Integer countMatchesByUserId(Integer userId) {
        return likeRepository.countMatches(userId);
    }

    @Override
    @Transactional(readOnly = true)
    public Integer countMyLikesByUserId(Integer userId) {
        User user = userRepo.findOne(userId);
        if(user == null)
            return 0;
        return likeRepository.countByIdLikeBy(user);
    }

    @Override
    @Transactional(readOnly = true)
    public Integer countLikeMeByUserId(Integer userId) {
        User user = userRepo.findOne(userId);
        if(user == null)
            return 0;
        return likeRepository.countByIdLikeTo(user);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Integer> filterUserLikesByUserIds(Integer currentUserId, List<Integer> userIds) {
        return likeRepository.findLikedUserIdsByUserId(currentUserId, userIds);
    }
}
