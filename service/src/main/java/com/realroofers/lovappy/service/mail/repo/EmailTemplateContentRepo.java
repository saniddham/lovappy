package com.realroofers.lovappy.service.mail.repo;

import com.realroofers.lovappy.service.mail.model.EmailTemplate;
import com.realroofers.lovappy.service.mail.model.EmailTemplateContent;
import com.realroofers.lovappy.service.mail.support.EmailTypes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Daoud Shaheen on 9/8/2017.
 */
public interface EmailTemplateContentRepo extends JpaRepository<EmailTemplateContent, Integer> {

    EmailTemplateContent findByTemplate(EmailTemplate template);
    EmailTemplateContent findByLanguageAndTemplateId(String language, Integer templateId);
    EmailTemplateContent findByLanguageAndTemplateType(String language, EmailTypes type);
    EmailTemplateContent findByLanguageAndTemplateName(String language, String name);
    Page<EmailTemplateContent> findByLanguage(String language, Pageable pageable);
}
