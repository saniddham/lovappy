package com.realroofers.lovappy.service.upload.impl;

import com.realroofers.lovappy.service.upload.FileUploadService;
import com.realroofers.lovappy.service.upload.dto.FileUploadDto;
import com.realroofers.lovappy.service.upload.model.FileUpload;
import com.realroofers.lovappy.service.upload.repo.FileUploadRepo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Manoj
 */
@Service
public class FileUploadServiceImpl implements FileUploadService {

    private final FileUploadRepo fileUploadRepo;

    public FileUploadServiceImpl(FileUploadRepo fileUploadRepo) {
        this.fileUploadRepo = fileUploadRepo;
    }

    @Override
    public List<FileUploadDto> findAll() {
        List<FileUploadDto> list = new ArrayList<>();
        fileUploadRepo.findAll().stream().forEach(fileUpload -> list.add(new FileUploadDto(fileUpload)));
        return list;
    }

    @Override
    public List<FileUploadDto> findAllActive() {
        List<FileUploadDto> list = new ArrayList<>();
        fileUploadRepo.findAll().stream().forEach(fileUpload -> list.add(new FileUploadDto(fileUpload)));
        return list;
    }

    @Override
    public FileUploadDto create(FileUploadDto fileUploadDto) throws Exception {
        return new FileUploadDto(fileUploadRepo.saveAndFlush(new FileUpload(fileUploadDto)));
    }

    @Override
    public FileUploadDto update(FileUploadDto fileUploadDto) throws Exception {
        return new FileUploadDto(fileUploadRepo.save(new FileUpload(fileUploadDto)));
    }

    @Override
    public void delete(Integer integer) throws Exception {

    }

    @Override
    public FileUploadDto findById(Integer id) {
        Optional<FileUpload> uploadCategory = Optional.ofNullable(fileUploadRepo.findOne(id));
        return uploadCategory.map(FileUploadDto::new).orElse(null);
    }

    @Override
    public Page<FileUploadDto> findAll(Pageable pageable, Integer userId) {
        Page<FileUploadDto> list;

        Page<FileUpload> all = fileUploadRepo.findAll(pageable);
        list = all.map(FileUploadDto::new);
        return list;
    }

    @Override
    public FileUploadDto findById(Integer id, Integer userId) {
        return null;
    }
}
