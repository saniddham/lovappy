package com.realroofers.lovappy.service.radio;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.radio.model.AudioSubType;

/**
 * Created by Manoj on 17/12/2017.
 */
public interface AudioSubTypeService extends AbstractService<AudioSubType, Integer> {
    AudioSubType findByName(String name);
}
