package com.realroofers.lovappy.service.radio.impl;

import com.realroofers.lovappy.service.radio.*;
import com.realroofers.lovappy.service.radio.dto.RadioProgramControlDto;
import com.realroofers.lovappy.service.radio.model.*;
import com.realroofers.lovappy.service.radio.repo.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Manoj on 17/12/2017.
 */
@Service
public class RadioProgramControlServiceImpl implements RadioProgramControlService {

    private final RadioProgramControlRepo radioProgramControlRepo;
    private final MediaTypeRepo mediaTypeRepo;
    private final AudioTypeRepo audioTypeRepo;
    private final AudioSubTypeRepo audioSubTypeRepo;
    private final RadioProgramLanguageRepo radioProgramLanguageRepo;
    private final RadioProgramLocationRepo radioProgramLocationRepo;
    private final RadioProgramPlayTimeRepo radioProgramPlayTimeRepo;

    public RadioProgramControlServiceImpl(RadioProgramControlRepo radioProgramControlRepo,
                                          MediaTypeRepo mediaTypeRepo, AudioTypeRepo audioTypeRepo,
                                          AudioSubTypeRepo audioSubTypeRepo,
                                          RadioProgramLanguageRepo radioProgramLanguageRepo,
                                          RadioProgramLocationRepo radioProgramLocationRepo,
                                          RadioProgramPlayTimeRepo radioProgramPlayTimeRepo) {

        this.radioProgramControlRepo = radioProgramControlRepo;
        this.mediaTypeRepo = mediaTypeRepo;
        this.audioTypeRepo = audioTypeRepo;
        this.audioSubTypeRepo = audioSubTypeRepo;
        this.radioProgramLanguageRepo = radioProgramLanguageRepo;
        this.radioProgramLocationRepo = radioProgramLocationRepo;
        this.radioProgramPlayTimeRepo = radioProgramPlayTimeRepo;
    }

    @Override
    @Transactional
    public void saveRadioControl(RadioProgramControlDto controlDto) throws Exception {
        MediaType mediaType = mediaTypeRepo.findOne(controlDto.getMediaType());
        AudioType audioType = audioTypeRepo.findOne(controlDto.getAudioType());
        AudioSubType audioSubType = audioSubTypeRepo.findOne(controlDto.getAudioSubType());

        RadioProgramControl radioProgramControl = new RadioProgramControl();
        radioProgramControl.setMediaType(mediaType);
        radioProgramControl.setAudioType(audioType);
        radioProgramControl.setAudioSubType(audioSubType);
        radioProgramControl.setAudioAction(controlDto.getAudioAction());
        radioProgramControl.setInsertWhere(controlDto.getInsertWhere());
        radioProgramControl.setGender(controlDto.getGender());
        radioProgramControl.setDailyLimit(controlDto.getDailyLimit());
        radioProgramControl.setLifetimeLimit(controlDto.getLifetimeLimit());
        radioProgramControl.setContentBefore(controlDto.getContentBefore());
        radioProgramControl.setContentAfter(controlDto.getContentAfter());
        radioProgramControl.setAgeBetween18And24(controlDto.getAgeBetween18And24());
        radioProgramControl.setAgeBetween25And34(controlDto.getAgeBetween25And34());
        radioProgramControl.setAgeBetween35And49(controlDto.getAgeBetween35And49());
        radioProgramControl.setAgeAbove50(controlDto.getAgeAbove50());
        radioProgramControl.setPlayToAll(controlDto.getPlayToAll());
        radioProgramControl.setPlayNext(controlDto.getPlayNext());

        final RadioProgramControl control = create(radioProgramControl);

        controlDto.getLanguages().stream().forEach(l -> {
            RadioProgramLanguage radioProgramLanguage = new RadioProgramLanguage();
            radioProgramLanguage.setLanguage(l);
            radioProgramLanguage.setRadioProgramControl(control);
            radioProgramLanguageRepo.saveAndFlush(radioProgramLanguage);
        });

        controlDto.getLocations().stream().forEach(l -> {
            RadioProgramLocation radioProgramLocation = new RadioProgramLocation();
            radioProgramLocation.setLocation(l.getLocation());
            radioProgramLocation.setRadioProgramControl(control);
            radioProgramLocationRepo.saveAndFlush(radioProgramLocation);
        });

        controlDto.getTimeSlots().stream().forEach(t -> {
            try {
                RadioProgramPlayTime radioProgramPlayTime = new RadioProgramPlayTime();
                radioProgramPlayTime.setStartTime(getDate(t.getStartTime()));
                radioProgramPlayTime.setEndTime(getDate(t.getEndTime()));
                radioProgramPlayTime.setRadioProgramControl(control);
                radioProgramPlayTimeRepo.saveAndFlush(radioProgramPlayTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
    }

    private Date getDate(String time) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        return (Date) format.parse(time);
    }

    @Override
    public List<RadioProgramControl> findAll() {
        return radioProgramControlRepo.findAll();
    }

    @Override
    public List<RadioProgramControl> findAllActive() {
        return radioProgramControlRepo.findAllByActiveIsTrue();
    }

    @Override
    public RadioProgramControl create(RadioProgramControl radioProgramControl) {
        return radioProgramControlRepo.saveAndFlush(radioProgramControl);
    }

    @Override
    public RadioProgramControl update(RadioProgramControl radioProgramControl) {
        return radioProgramControlRepo.save(radioProgramControl);
    }

    @Override
    public void delete(Integer id) {
        radioProgramControlRepo.delete(id);
    }

    @Override
    public RadioProgramControl findById(Integer id) {
        return radioProgramControlRepo.findOne(id);
    }
}
