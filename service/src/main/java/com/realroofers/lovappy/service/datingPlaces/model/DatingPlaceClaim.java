package com.realroofers.lovappy.service.datingPlaces.model;

import com.realroofers.lovappy.service.datingPlaces.support.VerificationType;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * Created by Darrel Rayen on 1/12/18.
 */
@Entity
@EqualsAndHashCode
public class DatingPlaceClaim {

    @Id
    @GeneratedValue
    private Integer claimId;

    @ManyToOne
    @JoinColumn(name = "place_id")
    private DatingPlace placeId;

    private String content;

    private String sendTo;

    private String sendFrom;

    private String verificationCode;

    private String googlePlaceId;

    @Enumerated(EnumType.STRING)
    private VerificationType verificationType;

    @Enumerated(EnumType.STRING)
    private ApprovalStatus placeApprovalStatus;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User claimer;

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }

    public DatingPlace getPlaceId() {
        return placeId;
    }

    public void setPlaceId(DatingPlace placeId) {
        this.placeId = placeId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSendTo() {
        return sendTo;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    public String getSendFrom() {
        return sendFrom;
    }

    public void setSendFrom(String sendFrom) {
        this.sendFrom = sendFrom;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public ApprovalStatus getPlaceApprovalStatus() {
        return placeApprovalStatus;
    }

    public void setPlaceApprovalStatus(ApprovalStatus placeApprovalStatus) {
        this.placeApprovalStatus = placeApprovalStatus;
    }

    public User getClaimer() {
        return claimer;
    }

    public void setClaimer(User claimer) {
        this.claimer = claimer;
    }

    public String getGooglePlaceId() {
        return googlePlaceId;
    }

    public void setGooglePlaceId(String googlePlaceId) {
        this.googlePlaceId = googlePlaceId;
    }

    public VerificationType getVerificationType() {
        return verificationType;
    }

    public void setVerificationType(VerificationType verificationType) {
        this.verificationType = verificationType;
    }
}
