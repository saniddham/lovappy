package com.realroofers.lovappy.service.gift;

import com.realroofers.lovappy.service.gift.dto.FilerObject;
import com.realroofers.lovappy.service.gift.dto.GiftExchangeDto;
import com.realroofers.lovappy.service.gift.dto.SaleByType;
import com.realroofers.lovappy.service.gift.dto.TopSellingItem;
import com.realroofers.lovappy.service.gift.dto.mobile.GiftExchangeInboxDto;
import com.realroofers.lovappy.service.gift.model.GiftExchange;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;

/**
 * Created by Eias Altawil on 5/13/17
 */
public interface GiftExchangeService {

    GiftExchangeDto sendGift(Integer giftID, Integer fromUserID, Integer toUserID, OrderDto orderDto);

    GiftExchangeDto findById(Integer giftId);

    GiftExchangeDto redeem(Integer giftId, Integer userId, Integer locationId) throws Exception;

    Collection<GiftExchangeDto> getAllReceivedGifts(Integer userID);

    Collection<GiftExchangeDto> getAllSentGifts(Integer userID);

    Collection<GiftExchangeDto> findAllByProductUser(Integer userID);

    Page<GiftExchange> findAllByProductUser(Integer userID, Pageable pageable,FilerObject filerObject);

    Page<GiftExchange> findAllSales(Pageable pageable,FilerObject filerObject);

    List<GiftExchangeDto> getGiftsByFromUserAndToUser(Integer fromUserID, Integer toUserID);

    Double getTotalSalesByVendor(Integer userId);

    Double getTotalCommissionByVendor(Integer userId);

    Double getTotalCostForUser(Integer userId);

    TopSellingItem getTopSellingItemByVendor(Integer userId);

    List<SaleByType> getSaleByGenderForVendor(Integer userId);

    List<SaleByType> getSaleByCategoryForVendor(Integer userId);

    List<SaleByType> getSaleByBrandForVendor(Integer userId);

    Page<GiftExchangeInboxDto> getGiftExchangeInboxMobile(Integer userId, PageRequest pageable);

    long getGiftExchangeInboxCount(Integer userId);

    long getGiftExchangeOutboxCount(Integer userId);

    GiftExchangeInboxDto getGiftExchangeInboxMobile(Integer id);

    Page<GiftExchangeInboxDto>  getGiftExchangeOutboxMobile(Integer userId, PageRequest pageable);
}
