package com.realroofers.lovappy.service.news.model;

import lombok.EqualsAndHashCode;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "news_advanced_option")
@EqualsAndHashCode
public class NewsAdvancedOption {
    @Id
    @GeneratedValue
    @Column(name = "advanced_option_id")
    private Long id;

    @Column(name = "advanced_option_seo_title", nullable = false)
    private String seoTitle;

    @Column(name = "advanced_option_meta_description", columnDefinition = "VARCHAR(1024)", nullable = false)
    private String metaDescription;


    @Column(name = "advanced_option_keyword_phrases")
    private String keywordPhrases;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "story_id", nullable = false)
    private NewsStory newsStory;

    public NewsAdvancedOption() {
    }

    public NewsAdvancedOption(String seoTitle, String metaDescription, String keywordPhrases, NewsStory newsStory) {

        this.seoTitle = seoTitle;
        this.metaDescription = metaDescription;
        this.keywordPhrases = keywordPhrases;
        this.newsStory = newsStory;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSeoTitle() {
        return seoTitle;
    }

    public void setSeoTitle(String seoTitle) {
        this.seoTitle = seoTitle;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getKeywordPhrases() {
        return keywordPhrases;
    }

    public void setKeywordPhrases(String keywordPhrases) {
        this.keywordPhrases = keywordPhrases;
    }

    public NewsStory getNewsStory() {
        return newsStory;
    }

    public void setNewsStory(NewsStory newsStory) {
        this.newsStory = newsStory;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("NewsAdvancedOption{");
        sb.append("id=").append(id);
        sb.append(", seoTitle='").append(seoTitle).append('\'');
        sb.append(", metaDescription='").append(metaDescription).append('\'');
        sb.append(", keywordPhrases='").append(keywordPhrases).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
