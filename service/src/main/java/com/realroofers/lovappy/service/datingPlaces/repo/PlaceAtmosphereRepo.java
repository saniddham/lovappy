package com.realroofers.lovappy.service.datingPlaces.repo;

import com.realroofers.lovappy.service.datingPlaces.model.PlaceAtmosphere;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Darrel Rayen on 10/19/17.
 */
@Repository
public interface PlaceAtmosphereRepo extends JpaRepository<PlaceAtmosphere, Integer> {

    PlaceAtmosphere findByAtmosphereName(String name);

    PlaceAtmosphere findByAtmosphereNameAndEnabledTrue(String name);

    List<PlaceAtmosphere> findAllByEnabled(Boolean enabled);
}
