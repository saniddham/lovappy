package com.realroofers.lovappy.service.music.dto;

import com.realroofers.lovappy.service.music.model.Genre;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

@EqualsAndHashCode
public class GenreDto implements Serializable {
    private Integer id;
    private String title;
    private Boolean enabled;
    private Boolean addedToSurvey = true;

    public GenreDto() {

    }

    public GenreDto(Genre genre) {
        this.id = genre.getId();
        this.title = genre.getTitle();
        this.enabled = genre.getEnabled();
        this.addedToSurvey=genre.getAddedToSurvey();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getAddedToSurvey() {
        return addedToSurvey;
    }

    public void setAddedToSurvey(Boolean addedToSurvey) {
        this.addedToSurvey = addedToSurvey;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
