package com.realroofers.lovappy.service.tips.impl;

import com.realroofers.lovappy.service.tips.TipsService;
import com.realroofers.lovappy.service.tips.dto.LoveTipDto;
import com.realroofers.lovappy.service.tips.model.LoveTip;
import com.realroofers.lovappy.service.tips.repo.LoveTipRepo;
import com.realroofers.lovappy.service.util.ListMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Daoud Shaheen on 1/1/2018.
 */
@Service
public class TipsServiceImpl implements TipsService {

    private final LoveTipRepo loveTipRepo;

    public TipsServiceImpl(LoveTipRepo loveTipRepo) {
        this.loveTipRepo = loveTipRepo;
    }

    @Transactional(readOnly = true)
    @Override
    public List<LoveTipDto> findAll() {
        return new ListMapper<>(loveTipRepo.findAll()).map(LoveTipDto::new);
    }

    @Transactional(readOnly = true)
    @Override
    public List<LoveTipDto> findAllActive() {
        return new ListMapper<>(loveTipRepo.findByActive(true)).map(LoveTipDto::new);
    }

    @Transactional
    @Override
    public LoveTipDto create(LoveTipDto loveTipDto) {
        LoveTip loveTip = new LoveTip();
        loveTip.setActive(false);
        loveTip.setContent(loveTipDto.getContent());
        loveTip.setBackgroundColor(loveTipDto.getBackgroundColor());
        loveTip.setBackgroundImage(loveTipDto.getBackgroundColor());
        return new LoveTipDto(loveTipRepo.save(loveTip));
    }

    @Transactional
    @Override
    public LoveTipDto update(LoveTipDto loveTipDto) {
        LoveTip loveTip = loveTipRepo.findOne(loveTipDto.getId());
        loveTip.setContent(loveTipDto.getContent());
        loveTip.setUpdated(new Date());
        return new LoveTipDto(loveTipRepo.save(loveTip));
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        loveTipRepo.delete(id);
    }

    @Transactional(readOnly = true)
    @Override
    public LoveTipDto findById(Integer id) {
        LoveTip loveTip = loveTipRepo.findOne(id);
        return loveTip == null ? null : new LoveTipDto(loveTip);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<LoveTipDto> getAll(Pageable pageable) {
        return loveTipRepo.findAll(pageable).map(LoveTipDto::new);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<LoveTipDto> getAllByActive(Pageable pageable, Boolean active) {
        return loveTipRepo.findByActive(active, pageable).map(LoveTipDto::new);
    }

    @Transactional
    @Override
    public void setActive(Integer id, boolean active) {
     LoveTip loveTip = loveTipRepo.findOne(id);
     loveTip.setActive(active);
     loveTipRepo.save(loveTip);
     }
}
