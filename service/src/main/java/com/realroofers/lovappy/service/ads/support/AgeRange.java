package com.realroofers.lovappy.service.ads.support;

/**
 * Created by Eias Altawil on 7/9/2017
 */
public enum AgeRange {
    RANGE1(18, 24), RANGE2(25, 34), RANGE3(35, 49), RANGE4(50, 100);

    private int from;
    private int to;

    AgeRange(int from, int to){
        this.from = from;
        this.to = to;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public static AgeRange getRangeByAge(Integer age){
        if(betweenExclusive(age, RANGE1.from, RANGE1.to)){
            return RANGE1;
        }else if(betweenExclusive(age, RANGE2.from, RANGE2.to)){
            return RANGE2;
        }else if(betweenExclusive(age, RANGE3.from, RANGE3.to)){
            return RANGE3;
        }else if(betweenExclusive(age, RANGE4.from, RANGE4.to)){
            return RANGE4;
        }
        return null;
    }

    private static boolean betweenExclusive(int x, int min, int max) {
        return x > min && x < max;
    }
}
