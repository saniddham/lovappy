package com.realroofers.lovappy.service.tips.repo;

import com.realroofers.lovappy.service.tips.model.LoveTip;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Daoud Shaheen on 1/1/2018.
 */
public interface LoveTipRepo extends JpaRepository<LoveTip, Integer> {

    List<LoveTip> findByActive(Boolean active);
    Page<LoveTip> findByActive(Boolean active, Pageable pageable);
}
