package com.realroofers.lovappy.service.datingPlaces.support.google_places;

/**
 * Represents a price ranking of some place.
 */
public enum Price {
    FREE,
    INEXPENSIVE,
    MODERATE,
    EXPENSIVE,
    VERY_EXPENSIVE,
    NONE
}
