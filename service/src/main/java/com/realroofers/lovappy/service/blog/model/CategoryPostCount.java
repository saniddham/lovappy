package com.realroofers.lovappy.service.blog.model;

import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 8/5/2017.
 */
@EqualsAndHashCode
public class CategoryPostCount implements Serializable{

    private Integer categoryId;

    private String name;

    private Long numOfPosts;

    public CategoryPostCount(Integer categoryId, String name, Long numOfPosts) {
        this.name = name;
        this.categoryId = categoryId;
        this.numOfPosts = numOfPosts;
    }

    public CategoryPostCount() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getNumOfPosts() {
        return numOfPosts;
    }

    public void setNumOfPosts(Long numOfPosts) {
        this.numOfPosts = numOfPosts;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
