package com.realroofers.lovappy.service.vendor.dto;

import com.realroofers.lovappy.service.user.model.Country;
import com.realroofers.lovappy.service.validator.UserRegister;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by Manoj on 16/02/2018.
 */
@Data
@EqualsAndHashCode
public class BrandRegistrationDto {

    @NotNull(message = "Address line 1 is required.", groups = {UserRegister.class})
    @NotEmpty(message = "Address line 1 is required.", groups = {UserRegister.class})
    private String addressLine1;
    private String addressLine2;
    @NotNull(message = "City is required.", groups = {UserRegister.class})
    @NotEmpty(message = "City is required.", groups = {UserRegister.class})
    private String city;
    @NotNull(message = "State is required.", groups = {UserRegister.class})
    @NotEmpty(message = "State is required.", groups = {UserRegister.class})
    private String state;
    @NotNull(message = "Zip Code is required.", groups = {UserRegister.class})
    @NotEmpty(message = "Zip Code is required.", groups = {UserRegister.class})
    private String zipCode;
    @NotNull(message = "Country is required.", groups = {UserRegister.class})
    private Country country;


    @Size(min = 1, message = "Product type is required.", groups = {UserRegister.class})
    private List<Integer> productTypeList;


    @Size(min = 1, message = "Brand is required.", groups = {UserRegister.class})
    private List<Integer> brandList;
}
