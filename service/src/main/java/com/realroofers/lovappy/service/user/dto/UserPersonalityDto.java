package com.realroofers.lovappy.service.user.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Daoud Shaheen on 6/18/2018.
 */
@Data
public class UserPersonalityDto  implements Serializable{
    private String outdoorsyORindoorsy;
    private String frugalORbigspender;
    private String morningpersonORnightowl;
    private String extrovertORintrovert;

    public UserPersonalityDto() {
    }

    public UserPersonalityDto(Map<String, String> personalites) {
        this.outdoorsyORindoorsy=personalites.get("outdoorsyORindoorsy");
        this.frugalORbigspender=personalites.get("frugalORbigspender");
        this.morningpersonORnightowl=personalites.get("morningpersonORnightowl");
        this.extrovertORintrovert=personalites.get("extrovertORintrovert");
    }
}
