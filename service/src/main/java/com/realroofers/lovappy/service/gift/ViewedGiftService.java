package com.realroofers.lovappy.service.gift;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.gift.dto.ViewedGiftDto;

public interface ViewedGiftService extends AbstractService<ViewedGiftDto, Integer> {
}
