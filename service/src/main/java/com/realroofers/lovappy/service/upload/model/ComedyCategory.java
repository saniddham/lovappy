package com.realroofers.lovappy.service.upload.model;

import com.realroofers.lovappy.service.upload.dto.ComedyCategoryDto;
import com.realroofers.lovappy.service.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Manoj
 */

@Entity
@Data
@ToString
@EqualsAndHashCode
public class ComedyCategory implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;

    private String catName;

    private String description;

    @ManyToOne
    private User createdBy;

    @CreationTimestamp
    @Column(name = "created", insertable = false, updatable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date created;

    public ComedyCategory() {
    }

    public ComedyCategory(ComedyCategoryDto uploadCategory) {
        if (uploadCategory.getId() != null)
            this.id = uploadCategory.getId();
        this.catName = uploadCategory.getCatName();
        this.description = uploadCategory.getDescription();
        this.createdBy = uploadCategory.getCreatedBy() != null ? new User(uploadCategory.getCreatedBy().getID()) : null;
    }
}
