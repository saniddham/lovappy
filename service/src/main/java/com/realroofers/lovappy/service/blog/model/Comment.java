package com.realroofers.lovappy.service.blog.model;

import lombok.EqualsAndHashCode;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity(name = "comment")
@Table(name = "comment")
@EqualsAndHashCode
public class Comment {
	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Integer ID;
	
	@ManyToOne
	@JoinColumn(name = "blog_id")
	private Post post;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date date;

	@Column(name = "content")
	private String content;
	

//	@Transient
//	private Integer postId;
//
	
//	@OneToMany(targetEntity=RelayComment.class,mappedBy="comment",
//			cascade=CascadeType.ALL,fetch=FetchType.LAZY)
//	private List<RelayComment> relays=new ArrayList<RelayComment>();
	
//	public List<RelayComment> getRelays() {
//		return relays;
//	}
//
//	public void setRelays(List<RelayComment> relays) {
//		this.relays = relays;
//	}

//	public Integer getPostId() {
//		return postId;
//	}
//
//	public void setPostId(Integer postId) {
//		this.postId = postId;
//	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public Date getDate() {
		return date;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}



	
}