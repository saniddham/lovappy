package com.realroofers.lovappy.service.likes.repo;

import com.realroofers.lovappy.service.likes.model.Like;
import com.realroofers.lovappy.service.likes.model.LikePK;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.Gender;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.method.P;

import java.util.List;
import java.util.Set;

/**
 * Created by Daoud Shaheen on 7/22/2017.
 */
public interface LikeRepository extends JpaRepository<Like, LikePK> {

    Like findById(LikePK id);

    Page<Like> findByIdLikeBy(User likeBy, Pageable pageable);

    Page<Like> findByIdLikeTo(User likeTo, Pageable pageable);

    @Query(nativeQuery = true, value = "Select u2.* From users_likes u2 JOIN (SELECT u.like_to FROM users_likes u Where u.like_by=:userId) T on T.like_to = u2.like_by Where u2.like_to=:userId order by created desc limit :page, :limit")
    List<Like> findMatches(@Param("userId") int userId, @Param("page") int page, @Param("limit") int limit);

    @Query(nativeQuery = true, value = "Select Count(*) From users_likes u2 JOIN (SELECT u.like_to FROM users_likes u Where u.like_by=:userId) T on T.like_to = u2.like_by Where u2.like_to=:userId ;")
    Integer countMatches(@Param("userId") int userId);

    Integer countByIdLikeTo(User likeTo);

    Integer countByIdLikeBy(User likeBy);

    @Query("SELECT COUNT(u.id.likeBy.userId) FROM Like u WHERE u.id.likeTo.userId=?1 AND u.id.likeBy.userProfile.gender=?2")
    Long countByLikeFromByGender(Integer likeFrom, Gender gender);

    @Query("SELECT COUNT(u.id.likeTo.userId) FROM Like u WHERE u.id.likeBy.userId=?1 AND u.id.likeTo.userProfile.gender=?2")
    Long countByLikeToByGender(Integer likeTo, Gender gender);

    @Query(nativeQuery = true, value = "SELECT like_to FROM users_likes where like_by=:userId and like_to in (:userIds);")
    List<Integer> findLikedUserIdsByUserId(@Param("userId") int userId, @Param("userIds") List<Integer> users);
}
