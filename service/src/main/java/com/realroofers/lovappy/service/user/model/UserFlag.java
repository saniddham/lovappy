package com.realroofers.lovappy.service.user.model;

import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 8/2/2017.
 */
@Entity
@DynamicUpdate
@Table(name = "users_flags")
@AssociationOverrides({
        @AssociationOverride(name = "id.flagBy",
                joinColumns = @JoinColumn(name = "flag_by")),
        @AssociationOverride(name = "id.flagTo",
                joinColumns = @JoinColumn(name = "flag_to")) })
@EqualsAndHashCode
public class UserFlag implements Serializable{

    private UserFlagPK id;

    @CreationTimestamp
    @Column(name = "created", insertable = false, updatable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date created;

    @Column(name = "offensive", columnDefinition = "BIT(1) default 0")
    private Boolean offensive;

    @Column(name = "discriminatory", columnDefinition = "BIT(1) default 0")
    private Boolean discriminatory;

    @Column(name = "uncivil", columnDefinition = "BIT(1) default 0")
    private Boolean uncivil;

    @Column(name = "harrassment", columnDefinition = "BIT(1) default 0")
    private Boolean harrassment;

    @Column(name = "notRelevant", columnDefinition = "BIT(1) default 0")
    private Boolean notRelevant;

    private String others;

    public UserFlag(UserFlagPK id) {
        this.id = id;
        this.created = new Date();
    }

    public UserFlag() {
    }
    @EmbeddedId
    public UserFlagPK getId() {
        return id;
    }

    public void setId(UserFlagPK id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Boolean getOffensive() {
        return offensive;
    }

    public void setOffensive(Boolean offensive) {
        this.offensive = offensive;
    }

    public Boolean getDiscriminatory() {
        return discriminatory;
    }

    public void setDiscriminatory(Boolean discriminatory) {
        this.discriminatory = discriminatory;
    }

    public Boolean getUncivil() {
        return uncivil;
    }

    public void setUncivil(Boolean uncivil) {
        this.uncivil = uncivil;
    }

    public Boolean getHarrassment() {
        return harrassment;
    }

    public void setHarrassment(Boolean harrassment) {
        this.harrassment = harrassment;
    }

    public Boolean getNotRelevant() {
        return notRelevant;
    }

    public void setNotRelevant(Boolean notRelevant) {
        this.notRelevant = notRelevant;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }
}
