package com.realroofers.lovappy.service.user.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by Daoud Shaheen on 6/18/2018.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserLifeStylesDto implements Serializable{
    private String catORdog;
    private String drinker;
    private String smoker;
    private String user;
    @JsonProperty("no-habit")
    private String nohabit;
    private String ruralORurbanORsuburban;
    private String politicalopinion;

    public UserLifeStylesDto() {
    }

    public UserLifeStylesDto(Map<String, String> lifestyles) {
     this.catORdog  = lifestyles.get("catORdog");
       this.drinker = lifestyles.get("drinker") ;
       this.smoker = lifestyles.get("smoker") ;
       this.user = lifestyles.get("user");
       this.nohabit = lifestyles.get("no-habit") ;
       this.ruralORurbanORsuburban = lifestyles.get("ruralORurbanORsuburban");
       this.politicalopinion = lifestyles.get("politicalopinion");
   }
}
