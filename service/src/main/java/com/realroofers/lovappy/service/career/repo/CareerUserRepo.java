package com.realroofers.lovappy.service.career.repo;

import com.realroofers.lovappy.service.career.model.CareerUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CareerUserRepo extends JpaRepository<CareerUser, Long> {

}
