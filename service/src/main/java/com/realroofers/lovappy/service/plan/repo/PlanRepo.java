package com.realroofers.lovappy.service.plan.repo;

import com.realroofers.lovappy.service.plan.dto.PlansDto;
import com.realroofers.lovappy.service.plan.model.Plan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by hasan on 7/11/2017.
 */
public interface PlanRepo  extends JpaRepository<Plan, Integer> {
    List<PlansDto> findByIsValid(Boolean isValid);
}
