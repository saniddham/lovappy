package com.realroofers.lovappy.service.ads.dto;

import com.realroofers.lovappy.service.ads.model.AdBackgroundImage;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
public class AdBackgroundImageDto {

    private Integer id;
    private CloudStorageFileDto imageFileCloud;
    private CloudStorageFileDto featuredAdBackgroundFileCloud;

    public AdBackgroundImageDto(AdBackgroundImage backgroundImage) {
        this.id = backgroundImage.getId();
        this.imageFileCloud = new CloudStorageFileDto(backgroundImage.getImageFileCloud());
        this.featuredAdBackgroundFileCloud = new CloudStorageFileDto(backgroundImage.getFeaturedAdBackgroundFileCloud());
    }
}
