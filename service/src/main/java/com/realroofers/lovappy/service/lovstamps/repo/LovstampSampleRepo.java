package com.realroofers.lovappy.service.lovstamps.repo;

import com.realroofers.lovappy.service.lovstamps.model.LovstampSample;
import com.realroofers.lovappy.service.lovstamps.support.SampleTypes;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.user.support.Gender;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import java.util.List;

/**
 * Created by Daoud Shaheen on 12/31/2017.
 */
public interface LovstampSampleRepo extends JpaRepository<LovstampSample, Integer>, QueryDslPredicateExecutor<LovstampSample> {

    Page<LovstampSample> findByLanguageAndGenderAndType(Language language, Gender gender, SampleTypes type, Pageable pageable);

    Page<LovstampSample> findByType( SampleTypes type, Pageable pageable);
}

