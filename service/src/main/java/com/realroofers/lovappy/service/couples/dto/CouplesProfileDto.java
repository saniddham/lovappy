package com.realroofers.lovappy.service.couples.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.couples.model.CouplesProfile;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.model.UserPreference;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by hasan on 6/10/2017.
 */
@EqualsAndHashCode
public class CouplesProfileDto implements Serializable {
    private Integer coupleId;
    private Gender gender;
    private Gender coupleGender;
    private Date birthDate;
    private Date anniversaryDate;
    private Integer yearsTogether;

    private CloudStorageFileDto coupleImage;
    private String partnerEmail;
    private Integer partnerId;
    private ApprovalStatus status;
    private Date lastUpdated;
    public CouplesProfileDto() {
    }

    public CouplesProfileDto(CouplesProfile coupleProfile, User user, UserPreference preference, String partnerEmail, Integer partnerId) {
        UserProfile profile = user.getUserProfile();
        this.coupleId = coupleProfile.getId();
        this. gender = profile.getGender();
        this. birthDate = profile != null ? profile.getBirthDate() : null;
        this. anniversaryDate = coupleProfile.getAnniversaryDate();
        this.coupleGender = preference != null ? preference.getGender() : null;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(coupleProfile.getFirstDate());
        this.yearsTogether = coupleProfile != null ? Calendar.getInstance().get(Calendar.YEAR) - calendar.get(Calendar.YEAR)  : 0;
        this.coupleImage  = new CloudStorageFileDto(coupleProfile.getCouplePhotoImage());
        this.partnerEmail = partnerEmail;
        this.status = coupleProfile.getStatus();
        this.partnerId = partnerId;
        this.lastUpdated = coupleProfile.getUpdated() == null? coupleProfile.getCreated() : coupleProfile.getUpdated();
    }

    public Integer getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }
//region setters and getters

    public Integer getCoupleId() {
        return coupleId;
    }

    public void setCoupleId(Integer coupleId) {
        this.coupleId = coupleId;
    }

    public ApprovalStatus getStatus() {
        return status;
    }

    public void setStatus(ApprovalStatus status) {
        this.status = status;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Gender getCoupleGender() {
        return coupleGender;
    }

    public void setCoupleGender(Gender coupleGender) {
        this.coupleGender = coupleGender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getAnniversaryDate() {
        return anniversaryDate;
    }

    public void setAnniversaryDate(Date anniversaryDate) {
        this.anniversaryDate = anniversaryDate;
    }

    public Integer getYearsTogether() {
        return yearsTogether;
    }

    public void setYearsTogether(Integer yearsTogether) {
        this.yearsTogether = yearsTogether;
    }

    public String getPartnerEmail() {
        return partnerEmail;
    }

    public void setPartnerEmail(String partnerEmail) {
        this.partnerEmail = partnerEmail;
    }

    public CloudStorageFileDto getCoupleImage() {
        return coupleImage;
    }

    public void setCoupleImage(CloudStorageFileDto coupleImage) {
        this.coupleImage = coupleImage;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String toString() {
        return "CouplesProfileDto{" +
                "coupleId=" + coupleId +
                ", gender=" + gender +
                ", coupleGender=" + coupleGender +
                ", birthDate=" + birthDate +
                ", anniversaryDate=" + anniversaryDate +
                ", yearsTogether=" + yearsTogether +
                ", coupleImage=" + coupleImage +
                ", partnerEmail='" + partnerEmail + '\'' +
                ", partnerId=" + partnerId +
                ", status=" + status +
                '}';
    }
//endregion
}
