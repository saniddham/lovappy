package com.realroofers.lovappy.service.product.impl;

import com.realroofers.lovappy.service.product.BrandCommissionService;
import com.realroofers.lovappy.service.product.dto.BrandCommissionDto;
import com.realroofers.lovappy.service.product.model.BrandCommission;
import com.realroofers.lovappy.service.product.repo.BrandCommissionRepo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class BrandCommissionServiceImpl implements BrandCommissionService {

    private final BrandCommissionRepo brandCommissionRepo;

    public BrandCommissionServiceImpl(BrandCommissionRepo brandCommissionRepo) {
        this.brandCommissionRepo = brandCommissionRepo;
    }

    @Transactional(readOnly = true)
    @Override
    public List<BrandCommissionDto> findAll() {
        List<BrandCommissionDto> list = new ArrayList<>();
        brandCommissionRepo.findAll().stream().forEach(commission -> list.add(new BrandCommissionDto(commission)));
        return list;
    }

    @Transactional(readOnly = true)
    @Override
    public Page<BrandCommission> findAll(Pageable pageable) {
        return brandCommissionRepo.findAll(pageable);
    }

    @Override
    public BrandCommissionDto getCurrentCommission() {
        BrandCommission one = brandCommissionRepo.findTopByActiveIsTrueOrderByAffectiveDateAsc();
        return one != null ? new BrandCommissionDto(one) : null;
    }

    @Transactional(readOnly = true)
    @Override
    public List<BrandCommissionDto> findAllActive() {
        List<BrandCommissionDto> list = new ArrayList<>();
        brandCommissionRepo.findAllByActiveIsTrue().stream().forEach(commission -> list.add(new BrandCommissionDto(commission)));
        return list;
    }

    @Transactional
    @Override
    public BrandCommissionDto create(BrandCommissionDto brandCommissionDto) throws Exception {
        return new BrandCommissionDto(brandCommissionRepo.saveAndFlush(new BrandCommission(brandCommissionDto)));
    }

    @Transactional
    @Override
    public BrandCommissionDto update(BrandCommissionDto brandCommissionDto) throws Exception {
        return new BrandCommissionDto(brandCommissionRepo.saveAndFlush(new BrandCommission(brandCommissionDto)));
    }

    @Transactional
    @Override
    public void delete(Integer integer) throws Exception {
        BrandCommissionDto commissionDto = findById(integer);
        if (commissionDto != null) {
            commissionDto.setActive(false);
            update(commissionDto);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public BrandCommissionDto findById(Integer integer) {
        BrandCommission one = brandCommissionRepo.findOne(integer);
        return one != null ? new BrandCommissionDto(one) : null;
    }
}
