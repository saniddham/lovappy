package com.realroofers.lovappy.service.user;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.core.SocialType;
import com.realroofers.lovappy.service.user.dto.SocialProfileDto;
import com.realroofers.lovappy.service.user.dto.UserAuthDto;
import com.realroofers.lovappy.service.user.model.Roles;

/**
 * Created by Daoud Shaheen on 3/3/2018.
 */
public interface SocialProfileService extends AbstractService<SocialProfileDto, Integer> {

    SocialProfileDto getProfileBySocialIdAndProvider(String socialId, SocialType socialProvider);
    int saveSocial(UserAuthDto dto, Roles role);

}
