package com.realroofers.lovappy.service.util;

import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Eias Altawil
 */
public class ListMapper<T> {
    private List<T> content;

    public ListMapper(List<T> content) {
        this.content = content;
    }

    public <S> List<S> map(Converter<? super T, ? extends S> converter) {
        List<S> result = new ArrayList<>(content.size());

        for (T element : content) {
            result.add(converter.convert(element));
        }

        return result;
    }
}
