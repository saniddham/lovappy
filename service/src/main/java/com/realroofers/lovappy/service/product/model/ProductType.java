package com.realroofers.lovappy.service.product.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.product.dto.ProductTypeDto;
import com.realroofers.lovappy.service.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Manoj on 06/02/2018.
 */
@Entity
@Data
@EqualsAndHashCode
@Table(name = "product_type")
public class ProductType extends BaseEntity<Integer> implements Serializable {

    @NotBlank
    @Column(name = "type_name")
    private String typeName;


    @Column(name = "type_description")
    private String typeDescription;


    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;

    @Column(name = "added_to_survey", columnDefinition = "boolean default false", nullable = false)
    private Boolean addedToSurvey = true;

    @JsonIgnore
    @JoinColumn(name = "created_by")
    @ManyToOne
    private User createdBy;

    public ProductType() {
    }

    public ProductType(ProductTypeDto productType) {
        if (productType.getId() != null) {
            this.setId(productType.getId());
        }
        this.typeName = productType.getTypeName();
        this.typeDescription = productType.getTypeDescription();
        this.active = productType.getActive();
        this.addedToSurvey = productType.getAddedToSurvey();
        if(productType.getCreatedBy()!=null) {
            this.createdBy = new User(productType.getCreatedBy().getID());
        }
    }
}
