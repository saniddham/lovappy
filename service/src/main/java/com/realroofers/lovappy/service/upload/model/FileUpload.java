package com.realroofers.lovappy.service.upload.model;


import com.realroofers.lovappy.service.ads.support.AdMediaType;
import com.realroofers.lovappy.service.ads.support.AdType;
import com.realroofers.lovappy.service.ads.support.AudioType;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.music.model.Genre;
import com.realroofers.lovappy.service.upload.dto.FileUploadDto;
import com.realroofers.lovappy.service.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Manoj
 */

@Entity
@Data
@ToString
@EqualsAndHashCode
public class FileUpload implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;

    private String title;

    private String description;

    @Enumerated(EnumType.STRING)
    private AudioType audioType;

    @Enumerated(EnumType.STRING)
    private AdType type;

    @Enumerated(EnumType.STRING)
    private AdMediaType mediaType;

    @ManyToOne
    private Genre genre;

    @Type(type = "text")
    private String lyrics;

    @ManyToOne
    private PodcastCategory podcastCategory;

    @ManyToOne
    private ComedyCategory comedyCategory;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn
    private CloudStorageFile imageFile;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn
    private CloudStorageFile file;

    @Column(name = "is_approved", columnDefinition = "boolean default false", nullable = false)
    private Boolean approved = false;

    @Column(name = "is_rejected", columnDefinition = "boolean default false", nullable = false)
    private Boolean rejected = false;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;

    @Column(name = "user_agreement", columnDefinition = "boolean default false", nullable = false)
    private Boolean userAgreement = false;

    @Column(name = "is_advertisement", columnDefinition = "boolean default false", nullable = false)
    private Boolean isAdvertisement = false;

    @ManyToOne
    private User createdBy;

    @CreationTimestamp
    @Column(name = "created", insertable = false, updatable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date created;

    @ManyToOne
    private User approvedBy;

    @ManyToOne
    private User rejectedBy;

    private Double price;

    public FileUpload() {
    }

    public FileUpload(FileUploadDto fileUpload) {
        if (fileUpload.getId() != null)
            this.id = fileUpload.getId();
        this.title = fileUpload.getTitle();
        this.description = fileUpload.getDescription();
        this.audioType = fileUpload.getAudioType();
        this.type = fileUpload.getType();
        this.mediaType = fileUpload.getMediaType();
        this.genre = fileUpload.getGenre() != null ? new Genre(fileUpload.getGenre()) : null;
        this.lyrics = fileUpload.getLyrics();
        this.podcastCategory = fileUpload.getPodcastCategory() != null ? new PodcastCategory(fileUpload.getPodcastCategory()) : null;
        this.comedyCategory = fileUpload.getComedyCategory() != null ? new ComedyCategory(fileUpload.getComedyCategory()) : null;
        this.imageFile = fileUpload.getImageFile() != null ? new CloudStorageFile(fileUpload.getImageFile().getId()) : null;
        this.file = fileUpload.getFile() != null ? new CloudStorageFile(fileUpload.getFile().getId()) : null;
        this.approved = fileUpload.getApproved();
        this.active = fileUpload.getActive();
        this.rejected = fileUpload.getRejected();
        this.userAgreement = fileUpload.getUserAgreement();
        this.isAdvertisement = fileUpload.getIsAdvertisement();
        this.createdBy = fileUpload.getCreatedBy() != null ? new User(fileUpload.getCreatedBy().getID()) : null;
        this.approvedBy = fileUpload.getApprovedBy() != null ? new User(fileUpload.getApprovedBy().getID()) : null;
        this.rejectedBy = fileUpload.getRejectedBy() != null ? new User(fileUpload.getRejectedBy().getID()) : null;
        this.price = fileUpload.getPrice();
    }
}
