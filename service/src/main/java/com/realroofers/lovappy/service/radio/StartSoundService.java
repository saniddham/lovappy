package com.realroofers.lovappy.service.radio;

import com.realroofers.lovappy.service.radio.dto.StartSoundDto;
import com.realroofers.lovappy.service.radio.support.StartSoundType;

import java.util.List;

/**
 * Created by Daoud Shaheen on 1/12/2018.
 */
public interface StartSoundService {

    List<StartSoundDto> getStartSounds();

    StartSoundDto addStartSound(StartSoundDto sound);

    StartSoundDto getSoundByType(StartSoundType type);
}
