package com.realroofers.lovappy.service.user.repo;

import com.realroofers.lovappy.service.user.model.PasswordResetRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Eias on 27-Apr-17
 */

@Repository
public interface PasswordResetRequestRepo extends CrudRepository<PasswordResetRequest, Integer> {
    PasswordResetRequest findByToken(String token);
}
