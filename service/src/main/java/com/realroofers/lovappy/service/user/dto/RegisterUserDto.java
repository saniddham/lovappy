package com.realroofers.lovappy.service.user.dto;


import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.validator.UserRegister;
import com.realroofers.lovappy.service.validator.constraint.ZipCode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by Tejaswi Venupalli on 8/30/2017.
 */
@Data
@EqualsAndHashCode
public class RegisterUserDto {

    @NotEmpty(message = "You should provide your email address.", groups = {UserRegister.class})
    @NotNull(message = "You should provide your email address.", groups = {UserRegister.class})
    @Email(message = "This is not a valid email address.", groups = {UserRegister.class})
    private String email;

    @NotEmpty(message = "Password cannot be empty.", groups = {UserRegister.class})
    @NotNull(message = "Password cannot be empty.", groups = {UserRegister.class})
    @Size(min = 6, message = "Minimum password length is 6.", groups = {UserRegister.class})
    private String password;

    private String confirmPassword;

    private String name;

    private String phoneNumber;

    @NotEmpty(message = "Zip Code is required.", groups = {UserRegister.class})
    @NotNull(message = "Zip Code is required.", groups = {UserRegister.class})
    @ZipCode(groups = {UserRegister.class})
    private String zipcode;

    private Gender gender;

    private Date birthDate;


    private CloudStorageFileDto photoIdentificationCloudFile;

    @Enumerated(EnumType.STRING)
    private ApprovalStatus approvalStatus;

    NotificationTypes notificationType;

    Roles role;

    public RegisterUserDto(){

    }
    public RegisterUserDto(User user){
        this(user, user.getUserProfile());

    }
    public RegisterUserDto(User user, UserProfile userProfile){
            this.name = user!= null ? user.getFirstName()+' '+user.getLastName() : null;
            this.phoneNumber = user!= null ? user.getPhoneNumber() : null;
            this.photoIdentificationCloudFile = userProfile!= null ? new CloudStorageFileDto(userProfile.getPhotoIdentificationCloudFile()):null;
            this.email = user.getEmail();
    }
}
