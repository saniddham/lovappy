package com.realroofers.lovappy.service.music.model;

import java.io.Serializable;

public enum MusicResponseType implements Serializable{
    THANKS,THANKS_AND_NEW_SONG, NO_RESPONSE
}
