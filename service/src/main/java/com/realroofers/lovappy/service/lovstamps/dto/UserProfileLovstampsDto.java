package com.realroofers.lovappy.service.lovstamps.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
public class UserProfileLovstampsDto {

    private LovstampDto prevLovstamp;
    private LovstampDto nextLovstamp;

    public UserProfileLovstampsDto(LovstampDto prevLovstamp, LovstampDto nextLovstamp) {
        this.prevLovstamp = prevLovstamp;
        this.nextLovstamp = nextLovstamp;
    }
}
