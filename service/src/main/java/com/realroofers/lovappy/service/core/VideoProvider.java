package com.realroofers.lovappy.service.core;

/**
 * Created by Daoud Shaheen on 8/9/2017.
 */
public enum VideoProvider {
    YOUTUBE, OTHER
}
