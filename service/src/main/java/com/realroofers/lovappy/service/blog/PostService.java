package com.realroofers.lovappy.service.blog;

import java.io.InputStream;
import java.util.List;

import com.realroofers.lovappy.service.blog.dto.PostShareStatisticsDto;
import com.realroofers.lovappy.service.blog.model.BlogTypes;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.core.SocialType;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.realroofers.lovappy.service.blog.dto.PostDto;
import com.realroofers.lovappy.service.blog.model.Post;
import com.realroofers.lovappy.service.blog.support.PostStatus;

/**
 * 
 * @author mwiyono
 *
 */
public interface PostService {

	void save(Post post);

	PostDto save(PostDto post, Integer author);

	void update(Post post);

	PostDto update( Integer postId, PostDto post);

	void delete(Post post);

	PostDto findById(Integer ID);
	
	Page<PostDto> findAllByPostDateAsc(int page, int limit);


	PostDto getPost(Integer postId);

	List<Post> findByCategory(Integer categoryId);

	Page<PostDto> getPostsByCategory(Integer categoryId, int page, int limit);

	PostDto getAndUpdateViewCountById(Integer viewCount, Integer ID);

	List<PostDto> getTopRecentBlogs(int numberOfBlogs);

	PostDto findTopOneOrderByViewCountDesc();

	PostDto findTopOneOrderByPostDate();

	Page<PostDto> findMostViewed(int pageNo, int size);

	boolean updateStatus(Integer postId, PostStatus status);

	boolean delete(Integer postId);

	Page<PostDto> findAll(Pageable pageable);
	PostDto updateByAdmin(Integer postId, PostDto postDto);
	Page<PostDto> findPostByKeyword(String keyword, Pageable pageable);

	Page<PostDto> findPostByUserId(Integer userId, Integer page);

	Integer getNumberOfPostByUserId(Integer userId);

	Page<PostDto> getAdminPosts(int page, int limit);

	Page<PostDto> getFeaturedPosts(int page, int limit);

	Page<PostDto> getPostByType(BlogTypes type, int page, int limit);

	void setFeatured(Integer id);

	void updateImage(Integer postId, CloudStorageFileDto cloudStorageFileDto);

	Page<PostShareStatisticsDto> getPostsStatestics(Pageable pageable);

	void shareBySocialProvider(Integer postId, SocialType socialType);

	String createSlugUrl(String title, Integer postId);
	PostDto findBySlugUrl(String slugUrl);
	PostDto getMusicPost();
	PostDto getAndUpdateViewCountBySlugUrl(Integer viewCount, String slugUrl);
}
