package com.realroofers.lovappy.service.user.support;

/**
 * Created by Tejaswi Venupalli on 9/1/17
 */

public enum ApprovalStatus {

    PENDING("Pending"),APPROVED("Approved"), REJECTED("Rejected");

    private String status;

    ApprovalStatus(String status){
        this.status = status;
    }

    public String getValue(){
        return status;
    }


}