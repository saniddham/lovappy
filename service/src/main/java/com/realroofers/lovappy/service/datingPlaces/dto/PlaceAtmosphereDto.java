package com.realroofers.lovappy.service.datingPlaces.dto;

import com.realroofers.lovappy.service.datingPlaces.model.PlaceAtmosphere;
import lombok.EqualsAndHashCode;

/**
 * Created by Darrel Rayen on 12/9/17.
 */
@EqualsAndHashCode
public class PlaceAtmosphereDto {

    private Integer id;
    private String atmosphereName;
    private Boolean enabled;

    public PlaceAtmosphereDto() {
    }

    public PlaceAtmosphereDto(PlaceAtmosphere placeAtmosphere) {
        this.id = placeAtmosphere.getId();
        this.atmosphereName = placeAtmosphere.getAtmosphereName();
        this.enabled = placeAtmosphere.getEnabled();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAtmosphereName() {
        return atmosphereName;
    }

    public void setAtmosphereName(String atmosphereName) {
        this.atmosphereName = atmosphereName;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
