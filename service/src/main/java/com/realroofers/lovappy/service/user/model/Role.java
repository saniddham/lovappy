package com.realroofers.lovappy.service.user.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Eias Altawil on 5/5/17
 */

@Entity
@Table(name = "role")
@DynamicUpdate
@EqualsAndHashCode
public class Role extends BaseEntity<Integer> implements Serializable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
