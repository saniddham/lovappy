package com.realroofers.lovappy.service.product;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.gift.dto.FilerObject;
import com.realroofers.lovappy.service.product.dto.BrandDto;
import com.realroofers.lovappy.service.product.dto.mobile.BrandItemDto;
import com.realroofers.lovappy.service.product.model.Brand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BrandService extends AbstractService<BrandDto, Integer> {

    BrandDto findByName(String name);

    String[] getStringArray();

    long getBrandCountByUser(int userId);

    Page<Brand> findBrandsByLoginUser(int userId, Pageable pageable,FilerObject filerObject);

    List<BrandItemDto> findAllForMobile();
}
