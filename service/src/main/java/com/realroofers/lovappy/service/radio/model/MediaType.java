package com.realroofers.lovappy.service.radio.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Manoj on 17/12/2017.
 */

@Data
@Entity
@Table(name = "radio_media_type")
@EqualsAndHashCode
public class MediaType extends BaseEntity<Integer> implements Serializable {

    @Column(name = "type_name")
    private String name;

    @Column(name = "type_description")
    private String description;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;


    public MediaType() {
    }

    public MediaType(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return "MediaType{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", active=" + getActive() +
                '}';
    }
}
