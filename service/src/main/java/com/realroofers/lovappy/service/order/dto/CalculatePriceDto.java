package com.realroofers.lovappy.service.order.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
public class CalculatePriceDto {
    private Double price;
    private CouponDto coupon;
    private Double totalPrice;

    public CalculatePriceDto(Double price, CouponDto coupon, Double totalPrice) {
        this.price = price;
        this.coupon = coupon;
        this.totalPrice = totalPrice;
    }
}
