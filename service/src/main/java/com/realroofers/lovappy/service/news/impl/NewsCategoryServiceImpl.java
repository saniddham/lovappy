package com.realroofers.lovappy.service.news.impl;

import com.realroofers.lovappy.service.news.NewsCategoryService;
import com.realroofers.lovappy.service.news.dto.NewsCategoryDto;
import com.realroofers.lovappy.service.news.model.NewsCategory;
import com.realroofers.lovappy.service.news.repo.NewsCategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class NewsCategoryServiceImpl implements NewsCategoryService {
    @Autowired
    private NewsCategoryRepo newsCategoryRepo;

    @Transactional
    @Override
    public List<NewsCategory> getAllNewsCategory() {
        return newsCategoryRepo.findAll();
    }

    @Transactional
    @Override
    public NewsCategory getNewsCategoryByName(String categoryName) {
        return newsCategoryRepo.getOneByCategoryName(categoryName);
    }

    @Override
    public NewsCategory getOne(Long id) {
        return newsCategoryRepo.getOne(id);
    }

    @Override
    public NewsCategory getNewsCategoryByCategoryId(Long categoryId) {
        return newsCategoryRepo.getOne(categoryId);
    }

    public NewsCategory save(NewsCategory category){
        return newsCategoryRepo.save(category);
    }

    public List<NewsCategory> getActiveNewsCategory() {
        return newsCategoryRepo.getActiveNewsCatgeories();
    }

    @Override
    public Page<NewsCategoryDto> findAll(PageRequest pageable) {
        Page<NewsCategory> newsCategories = newsCategoryRepo.findAll(pageable);
        return newsCategoryRepo.findAll(pageable).map(NewsCategoryDto::new);
    }

    @Override
    public NewsCategoryDto getCategoryById(Long id) {
        NewsCategoryDto newsCategory = new NewsCategoryDto(newsCategoryRepo.getOne(id));
        return newsCategory;
    }

    @Override
    public NewsCategoryDto getCategoryByName(String categoryName) {

        try {
            NewsCategory newsCategory = newsCategoryRepo.getOneByCategoryName(categoryName);
            if (newsCategory != null){
                return new NewsCategoryDto(newsCategory);
            }

        } catch (Exception e){
            System.out.println(e);
        }
        return null;
    }

    @Override
    public NewsCategoryDto save(NewsCategoryDto category) {
        NewsCategory updatedCategory = new NewsCategory(category);
        updatedCategory.setCategoryId(category.getCategoryId());

        try {
            newsCategoryRepo.save(updatedCategory);

        } catch (Exception e){
            System.out.println(e);
        }

        return category;
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        try {
            newsCategoryRepo.delete(new Long(id));
        } catch (Exception e){
            System.out.println(e);
        }
    }
}
