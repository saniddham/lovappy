package com.realroofers.lovappy.service.lovstamps;

import com.realroofers.lovappy.service.lovstamps.dto.LovastampDetailsDto;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampDto;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampFilterDto;
import com.realroofers.lovappy.service.lovstamps.dto.UserProfileLovstampsDto;
import com.realroofers.lovappy.service.radio.dto.RadioFilterDto;
import com.realroofers.lovappy.service.user.support.Gender;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Daoud Shaheen on 12/31/2017.
 */
public interface LovstampService {

    LovstampDto getLovstampByUserID(Integer userID);
    LovastampDetailsDto getVoiceByUserID(Integer userID);
    LovstampDto getLovstampDetailsByUserID(Integer userID);

    Page<LovstampDto> getLovstamps(Pageable pageable);

    Page<LovstampDto> getLovstampsByGender(Gender gender, Pageable pageable);

    Page<LovstampDto> getAllLovstampsFiltered(Integer userID, Integer profileUserID, Boolean nextProfile,
                                              RadioFilterDto filter, Boolean useFilter, Pageable pageable);

    UserProfileLovstampsDto getUserProfileLovstamps(Integer currentUser, Integer profileUserID);

    LovstampDto create(LovstampDto lovstampDto, boolean skipped);
    LovastampDetailsDto save(LovstampDto lovstampDto, boolean skipped);
    LovstampDto update(LovstampDto lovstampDto);

    void delete(Integer lovstampId);


    void incrementListenByCounter(Integer listionToUserId, Integer listenByUserId);

    void showLovstampInHomePage(Integer lovstampId, Boolean show);

    Page<LovstampDto> findByUserUserProfileIsAllowedProfilePicAndShowHome(boolean allowed, boolean showInHome, Pageable pageable);

}
