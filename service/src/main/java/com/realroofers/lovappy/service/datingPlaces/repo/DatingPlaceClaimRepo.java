package com.realroofers.lovappy.service.datingPlaces.repo;

import com.realroofers.lovappy.service.datingPlaces.model.DatingPlace;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceClaim;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Darrel Rayen on 1/31/18.
 */
@Repository
public interface DatingPlaceClaimRepo extends JpaRepository<DatingPlaceClaim, Integer> {

    DatingPlaceClaim findAllByVerificationCodeAndClaimerAndPlaceId(String verificationCode, User user, DatingPlace datingPlace);

    List<DatingPlaceClaim> findAllByContentAndVerificationCode(String messageId, String verificationCode);

    DatingPlaceClaim findDistinctByContent(String messageId);
}
