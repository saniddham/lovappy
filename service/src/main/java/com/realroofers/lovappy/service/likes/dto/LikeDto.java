package com.realroofers.lovappy.service.likes.dto;

import com.realroofers.lovappy.service.likes.model.Like;
import com.realroofers.lovappy.service.user.dto.UserDto;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 7/22/2017.
 */
@EqualsAndHashCode
public class LikeDto implements Serializable {
    private UserDto likeBy;
    private UserDto likeTo;
    private Date createdOn;
    private String userDetails;


    public LikeDto(UserDto likeBy, UserDto likeTo, Date createdOn) {
        this.likeBy = likeBy;
        this.likeTo = likeTo;
        this.createdOn = createdOn;
    }

    public LikeDto() {
    }
    public LikeDto(Like like) {
        this.likeBy = new UserDto(like.getId().getLikeBy());
        this.likeTo = new UserDto(like.getId().getLikeTo());
        this.createdOn = like.getCreated();
    }
    public UserDto getLikeBy() {
        return likeBy;
    }

    public void setLikeBy(UserDto likeBy) {
        this.likeBy = likeBy;
    }

    public UserDto getLikeTo() {
        return likeTo;
    }

    public void setLikeTo(UserDto likeTo) {
        this.likeTo = likeTo;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserDetails() {
        return getLikeTo().getUserProfile().getAge()+" / "+getLikeTo().getUserProfile().getAddress().getCity()+" / "+ getLikeTo().getUserProfile().getLanguage();
}

    public void setUserDetails(String userDetails) {
        this.userDetails = userDetails;
    }
}
