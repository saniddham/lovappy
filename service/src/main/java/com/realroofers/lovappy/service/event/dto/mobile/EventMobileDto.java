package com.realroofers.lovappy.service.event.dto.mobile;

import com.realroofers.lovappy.service.event.dto.EventCategoryDto;
import com.realroofers.lovappy.service.event.dto.EventLocationDto;
import com.realroofers.lovappy.service.event.model.Event;
import com.realroofers.lovappy.service.event.model.EventCategory;
import com.realroofers.lovappy.service.event.model.EventComment;
import com.realroofers.lovappy.service.event.model.EventLocationPicture;
import com.realroofers.lovappy.service.event.support.EventDateUtil;
import com.realroofers.lovappy.service.event.support.EventStatus;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;

/**
 * Created by Darrel Rayen on 3/31/18.
 */
@Data
@EqualsAndHashCode
public class EventMobileDto implements Serializable {

    private Integer eventId;
    private String eventTitle;
    private String eventDescription;
    private Date eventDate;
    private String startTime;
    private String finishTime;
    private EventLocationDto eventLocation;
    private String eventLocationDescription;
    private Integer attendeesLimit;
    private Integer attendeesCount;
    private Integer outsiderCount;
    private Double admissionCost;
    private EventStatus eventStatus;
    private ApprovalStatus eventApprovalStatus;
    private Boolean approved;
    private String timeZone;
    private Boolean paid;

    @NotNull
    private Integer ambassadorUserId;

    private Collection<User> userList = new ArrayList<>();
    private Map<String, String> targetedAudience;
    private List<String> locationPicturesUrls = new ArrayList<>();
    private Collection<EventComment> comments = new ArrayList<>();
    private Collection<EventCategoryDto> eventCategories;
    private List<String> eventUserPictureUrl = new ArrayList<>();

    public EventMobileDto() {
    }

    public EventMobileDto(Event event) {
        this.eventId = event.getEventId();
        this.eventTitle = event.getEventTitle();
        this.eventDescription = event.getEventDescription();
        this.eventDate = event.getEventDate();
        this.startTime = EventDateUtil.convertToString(event.getStartTime());
        this.finishTime = EventDateUtil.convertToString(event.getFinishTime());
        this.eventLocation = EventLocationDto.toEventLocationDto(event.getEventLocation());
        this.eventLocationDescription = event.getEventLocationDescription();
        this.attendeesLimit = event.getAttendeesLimit();
        this.outsiderCount = event.getOutsiderCount();
        this.admissionCost = event.getAdmissionCost();
        this.eventStatus = event.getEventStatus();
        this.eventApprovalStatus = event.getEventApprovalStatus();
        this.ambassadorUserId = event.getAmbassadorUserId();
        this.targetedAudience = event.getTargetedAudience();
        this.locationPicturesUrls = getImageUrls(event.getLocationPictures());
        this.timeZone = event.getTimeZone();
        this.approved = event.getApproved() == null ? false : event.getApproved();
        this.eventCategories = getEventCategories(event.getEventCategories());
    }

    public EventMobileDto(Object[] objects) {
        Event event = (Event) objects[0];
        this.eventId = event.getEventId();
        this.eventTitle = event.getEventTitle();
        this.eventDescription = event.getEventDescription();
        this.eventDate = event.getEventDate();
        this.startTime = EventDateUtil.convertToString(event.getStartTime());
        this.finishTime = EventDateUtil.convertToString(event.getFinishTime());
        this.eventLocation = EventLocationDto.toEventLocationDto(event.getEventLocation());
        this.eventLocationDescription = event.getEventLocationDescription();
        this.attendeesLimit = event.getAttendeesLimit();
        this.outsiderCount = event.getOutsiderCount();
        this.admissionCost = event.getAdmissionCost();
        this.eventStatus = event.getEventStatus();
        this.eventApprovalStatus = event.getEventApprovalStatus();
        this.ambassadorUserId = event.getAmbassadorUserId();
        this.targetedAudience = event.getTargetedAudience();
        this.locationPicturesUrls = getImageUrls(event.getLocationPictures());
        this.timeZone = event.getTimeZone();
        this.approved = event.getApproved() == null ? false : event.getApproved();
        this.eventCategories = getEventCategories(event.getEventCategories());
        this.paid = (Boolean) objects[1];
    }

    private Collection<EventCategoryDto> getEventCategories(Collection<EventCategory> categories) {
        Collection<EventCategoryDto> eventCategoryDtos = new ArrayList<>();
        categories.forEach(eventCategory -> {
            eventCategoryDtos.add(new EventCategoryDto(eventCategory));
        });
        return eventCategoryDtos;
    }

    private List<String> getImageUrls(List<EventLocationPicture> locationPictures) {
        List<String> imageUrls = new ArrayList<>();
        locationPictures.forEach(eventLocationPicture -> {
            imageUrls.add(eventLocationPicture.getPicture().getUrl());
        });
        return imageUrls;
    }
}
