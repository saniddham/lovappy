package com.realroofers.lovappy.service.news.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@ToString
@Qualifier("newsStoryAdminDto")
@EqualsAndHashCode
public class NewsStoryAdminDto {
    private Long newsStoryId;
    @NotEmpty(message = "You should provide your story title.")
    //@NotNull(message = "You should provide your story title.")
    @Size(max = 30, message = "Maximum title length is 30.")
    private String newsStoryTitle;

    private String newsStoryAudioVideoUrl;

    @NotEmpty(message = "You should provide your post.")
    //@NotNull(message = "You should provide your post.")
    private String newsStoryContain;
    private String source;

    private Date newsStorySubmitDate;
    private Boolean newsStoryPosted;
    @NotEmpty(message = "You should provide SEO title.")
    //@NotNull(message = "You should provide SEO title.")
    @Size(max = 55, message = "Maximum SEO title length is 55.")
    private String advancedOptionSeoTitle;

    @NotEmpty(message = "You should provide meta Description.")
    //@NotNull(message = "You should provide meta Description.")
    @Size(max = 230, message = "Max meta description length is 230.")
    private String advancedOptionMetaDescription;

    @NotEmpty(message = "You should provide keyword phrases.")
    //@NotNull(message = "You should provide keyword phrases.")
    //@Size(max = 2, message = "Maximum Keyword length is 2.")
    private String advancedOptionKeywordPhrases;

    //@NotEmpty(message = "Select your story category.")
    @NotNull(message = "Select your story category.")
    private Long categoryId;
    private String categoryName;
    //@NotEmpty(message = "Please Upload picture")
    private String imageName;
    private String imageDescription;
    private String description;
    @Size(max = 6, message = "Maximum title length is 6.")
    private String imagekeyword;
    private CloudStorageFileDto image;
    private CloudStorageFileDto authorImage;
    private CloudStorageFileDto video;

    public NewsStoryAdminDto() {
    }

    public NewsStoryAdminDto(Long newsStoryId, String newsStoryTitle, String newsStoryAudioVideoUrl,
                             String newsStoryContain, String source, Date newsStorySubmitDate,
                             Boolean newsStoryPosted, String advancedOptionSeoTitle, String advancedOptionMetaDescription,
                             String advancedOptionKeywordPhrases, Long categoryId, String categoryName, MultipartFile newsPicture,
                             String imageName, String imageDescription, String imagekeyword, MultipartFile newsAuthorPicture,
                             String identificationImageName, MultipartFile videoUploadFile, String videoName, String videoUrl,
                             String description) {

        this.newsStoryId = newsStoryId;
        this.newsStoryTitle = newsStoryTitle;
        this.newsStoryAudioVideoUrl = newsStoryAudioVideoUrl;
        this.newsStoryContain = newsStoryContain;
        this.source = source;
        this.newsStorySubmitDate = newsStorySubmitDate;
        this.newsStoryPosted = newsStoryPosted;
        this.advancedOptionSeoTitle = advancedOptionSeoTitle;
        this.advancedOptionMetaDescription = advancedOptionMetaDescription;
        this.advancedOptionKeywordPhrases = advancedOptionKeywordPhrases;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.imageName = imageName;
        this.imageDescription = imageDescription;
        this.imagekeyword = imagekeyword;
        this.description=description;
    }

    public Long getNewsStoryId() {

        return newsStoryId;
    }

    public void setNewsStoryId(Long newsStoryId) {
        this.newsStoryId = newsStoryId;
    }

    public String getNewsStoryTitle() {
        return newsStoryTitle;
    }

    public void setNewsStoryTitle(String newsStoryTitle) {
        this.newsStoryTitle = newsStoryTitle;
    }

    public String getNewsStoryAudioVideoUrl() {
        return newsStoryAudioVideoUrl;
    }

    public void setNewsStoryAudioVideoUrl(String newsStoryAudioVideoUrl) {
        this.newsStoryAudioVideoUrl = newsStoryAudioVideoUrl;
    }

    public String getNewsStoryContain() {
        return newsStoryContain;
    }

    public void setNewsStoryContain(String newsStoryContain) {
        this.newsStoryContain = newsStoryContain;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Date getNewsStorySubmitDate() {
        return newsStorySubmitDate;
    }

    public void setNewsStorySubmitDate(Date newsStorySubmitDate) {
        this.newsStorySubmitDate = newsStorySubmitDate;
    }

    public Boolean getNewsStoryPosted() {
        return newsStoryPosted;
    }

    public void setNewsStoryPosted(Boolean newsStoryPosted) {
        this.newsStoryPosted = newsStoryPosted;
    }

    public String getAdvancedOptionSeoTitle() {
        return advancedOptionSeoTitle;
    }

    public void setAdvancedOptionSeoTitle(String advancedOptionSeoTitle) {
        this.advancedOptionSeoTitle = advancedOptionSeoTitle;
    }

    public String getAdvancedOptionMetaDescription() {
        return advancedOptionMetaDescription;
    }

    public void setAdvancedOptionMetaDescription(String advancedOptionMetaDescription) {
        this.advancedOptionMetaDescription = advancedOptionMetaDescription;
    }

    public String getAdvancedOptionKeywordPhrases() {
        return advancedOptionKeywordPhrases;
    }

    public void setAdvancedOptionKeywordPhrases(String advancedOptionKeywordPhrases) {
        this.advancedOptionKeywordPhrases = advancedOptionKeywordPhrases;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageDescription() {
        return imageDescription;
    }

    public void setImageDescription(String imageDescription) {
        this.imageDescription = imageDescription;
    }

    public String getImagekeyword() {
        return imagekeyword;
    }

    public void setImagekeyword(String imagekeyword) {
        this.imagekeyword = imagekeyword;
    }

    public CloudStorageFileDto getImage() {
        return image;
    }

    public void setImage(CloudStorageFileDto image) {
        this.image = image;
    }

    public CloudStorageFileDto getAuthorImage() {
        return authorImage;
    }

    public void setAuthorImage(CloudStorageFileDto authorImage) {
        this.authorImage = authorImage;
    }

    public CloudStorageFileDto getVideo() {
        return video;
    }

    public void setVideo(CloudStorageFileDto video) {
        this.video = video;
    }
}
