package com.realroofers.lovappy.service.privatemessage.repo;

import com.realroofers.lovappy.service.privatemessage.model.PrivateMessage;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Eias Altawil on 5/7/17
 */
public interface PrivateMessageRepo
        extends CrudRepository<PrivateMessage, Integer>, QueryDslPredicateExecutor<PrivateMessage> {
    List<PrivateMessage> findAllByOrderBySentAtDesc();
    List<PrivateMessage> findAllByFromUser(User fromUser);
    List<PrivateMessage> findAllByToUser(User toUser);
    List<PrivateMessage> findAllByFromUserAndToUser(User fromUser, User toUser);

    @Query("SELECT Count(DISTINCT  msg.fromUser) FROM PrivateMessage msg Where msg.toUser=:toUser")
    Integer countExchangedMessagesByToUser(@Param("toUser") User toUser);
    @Query("SELECT  Count(DISTINCT  msg.toUser) FROM PrivateMessage msg Where msg.fromUser=:fromUser")
    Integer countExchangedMessagesByFromUser(@Param("fromUser") User fromUser);


    @Query("SELECT Count(msg.fromUser) FROM PrivateMessage msg Where msg.toUser=:toUser")
    Integer countMessagesByToUser(@Param("toUser") User toUser);
    @Query("SELECT  Count(msg.toUser) FROM PrivateMessage msg Where msg.fromUser=:fromUser")
    Integer countMessagesByFromUser(@Param("fromUser") User fromUser);
}
