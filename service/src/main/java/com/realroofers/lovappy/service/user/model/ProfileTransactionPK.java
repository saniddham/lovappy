package com.realroofers.lovappy.service.user.model;

import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 1/18/2018.
 */
@Embeddable
@EqualsAndHashCode
public class ProfileTransactionPK implements Serializable {


    private User userFrom;

    private User userTo;

    public ProfileTransactionPK() {
    }

    public ProfileTransactionPK(User userFrom, User userTo) {
        this.userFrom = userFrom;
        this.userTo = userTo;
    }

    @ManyToOne
    @JoinColumn(name = "user_from")
    public User getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(User userFrom) {
        this.userFrom = userFrom;
    }

    @ManyToOne
    @JoinColumn(name = "user_to")
    public User getUserTo() {
        return userTo;
    }

    public void setUserTo(User userTo) {
        this.userTo = userTo;
    }
}
