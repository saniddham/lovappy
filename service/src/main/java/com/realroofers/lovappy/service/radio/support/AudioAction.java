package com.realroofers.lovappy.service.radio.support;


/**
 * Created by Manoj on 19/12/2017.
 */
public enum AudioAction {

    PLAY("Play"), INSERT("Insert");

    private String name;

    AudioAction(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
