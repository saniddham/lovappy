package com.realroofers.lovappy.service.user.dto;

import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * Created by Eias on 27-Apr-17
 */
@EqualsAndHashCode
public class PasswordResetRequestDto {
    private Integer ID;
    private Integer userID;
    private String key;
    private Date createdAt;

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
