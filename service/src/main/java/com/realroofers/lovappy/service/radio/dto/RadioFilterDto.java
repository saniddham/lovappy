package com.realroofers.lovappy.service.radio.dto;

        import com.realroofers.lovappy.service.user.dto.WithinRadiusDto;
        import com.realroofers.lovappy.service.user.support.ZodiacSigns;
        import com.realroofers.lovappy.service.user.support.Gender;
        import com.realroofers.lovappy.service.user.support.PrefHeight;
        import com.realroofers.lovappy.service.user.support.Size;
        import lombok.Data;
        import lombok.EqualsAndHashCode;
        import lombok.ToString;
        import org.jsondoc.core.annotation.ApiObject;
        import org.springframework.util.StringUtils;

        import java.util.Arrays;
        import java.util.HashMap;
        import java.util.HashSet;
        import java.util.Set;

@ApiObject
@Data
@ToString
@EqualsAndHashCode
public class RadioFilterDto {

    private Integer userId;
    private Integer ageFrom;
    private Integer ageTo;
    private Gender gender;
    private PrefHeight height;
    private Set<String> languages;
    private Boolean penisSizeMatter;
    private Size breastsSize;
    private Size buttsSize;
    private HashMap<String, String> personalities;
    private HashMap<String, String> lifestyles;
    private HashMap<String, String> statuses;
    private WithinRadiusDto withinRadius;
    private ZodiacSigns[] zodiacsigns;

    public RadioFilterDto() {
    }

    public RadioFilterDto(Integer userId, Integer ageFrom, Integer ageTo, Gender gender, PrefHeight height,
                          String languages, Boolean penisSizeMatter, Size breastsSize, Size buttsSize,
                          HashMap<String, String> personalities, HashMap<String, String> lifestyles,
                          HashMap<String, String> statuses, WithinRadiusDto withinRadiusDto, ZodiacSigns[] zodiacSigns) {
        this.userId = userId;
        this.ageFrom = ageFrom;
        this.ageTo = ageTo;
        this.gender = gender;
        this.height = height;
        this.languages = StringUtils.isEmpty(languages)? null : new HashSet<>(Arrays.asList(languages.split(",")));
        this.penisSizeMatter = penisSizeMatter;
        this.breastsSize = breastsSize;
        this.buttsSize = buttsSize;
        this.personalities = personalities;
        this.lifestyles = lifestyles;
        this.statuses = statuses;
        this.withinRadius = withinRadiusDto;
        this.zodiacsigns = zodiacSigns;
    }

}