package com.realroofers.lovappy.service.product;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.gift.dto.FilerObject;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.product.dto.ProductDto;
import com.realroofers.lovappy.service.product.dto.ProductRegDto;
import com.realroofers.lovappy.service.product.dto.mobile.ItemDto;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by Manoj on 01/02/2018.
 */
public interface ProductService extends AbstractService<ProductDto, Integer> {


    Page<ProductDto> getAllProductsByName(String name, Pageable pageable);

    List<ProductDto> getAllProductsByCode(String code);

    List<ProductDto> getAllProductsForSales();

    Page<ItemDto> getProductForMobile(PageRequest pageRequest, String searchTerm, int brandId, int categoryId);

    Page<ItemDto> getProductForMobile(PageRequest pageRequest, int categoryId);

    ItemDto getProductForMobileById(Integer id);

    ProductDto findById(Integer id);

    List<ProductDto> getAllProductsByRecentlyViwed(Integer userId);

    List<ProductDto> getAllProductsRecentlyPurchased(Integer userId);

    Page<ProductDto> getAllProductsByNameAndByUser(FilerObject filerObject, Pageable pageable, User user);

    Page<ProductDto> findAllByNameOrderByApproved(String name, Pageable pageable);

    ProductDto updateProductStatus(Integer id, Integer userId);

    ProductDto addProduct(ProductRegDto productRegDto, Integer userId);

    ProductDto updateProduct(ProductRegDto productRegDto, Integer userId);

    ProductDto approveProduct(Integer id, Integer userId);

    ProductDto updateProductStatusAdmin(Integer id, Integer userId);

    CalculatePriceDto calculatePrice(Integer giftId, String coupon);
}
