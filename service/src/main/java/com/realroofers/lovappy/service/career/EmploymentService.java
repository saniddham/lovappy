package com.realroofers.lovappy.service.career;

import com.realroofers.lovappy.service.career.model.Education;
import com.realroofers.lovappy.service.career.model.Employment;

import java.util.Set;

public interface EmploymentService {

    String addEmployments(Set<Employment> employments);
}
