package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.user.model.Role;
import com.realroofers.lovappy.service.user.model.Roles;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Eias Altawil on 5/5/17
 */
@EqualsAndHashCode
public class RoleDto {
    private Integer id;
    private String name;

    public RoleDto(String name) {
        this.name = name;
    }

    public RoleDto(Role role) {

        if (role!=null){
        	this.id = role.getId();
             this.name = role.getName();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoleAbb(){
        return  Roles.valueOf(name).getAbb();
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoleDto roleDto = (RoleDto) o;

        return name.equals(roleDto.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
