package com.realroofers.lovappy.service.datingPlaces.repo;

import com.realroofers.lovappy.service.ads.support.AgeRange;
import com.realroofers.lovappy.service.datingPlaces.dto.DatingPlaceDto;
import com.realroofers.lovappy.service.datingPlaces.dto.DatingPlaceFilterDto;
import com.realroofers.lovappy.service.datingPlaces.dto.LocationDto;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlace;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceAtmospheres;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceFoodTypes;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlacePersonTypes;
import com.realroofers.lovappy.service.datingPlaces.support.DatingAgeRange;
import com.realroofers.lovappy.service.datingPlaces.support.Importance;
import com.realroofers.lovappy.service.datingPlaces.support.NeighborHood;
import com.realroofers.lovappy.service.datingPlaces.support.PriceRange;
import com.realroofers.lovappy.service.util.ListMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by daoud on 1/4/2019.
 */
@Repository
@Transactional(readOnly = true)
@Slf4j
public class DatingPlaceRepoImpl implements DatingPlaceRepoCustom {
    @PersistenceContext
    EntityManager entityManager;

    public static final String OR = " OR ";
    public static final String AND = " AND ";

    @Override
    public Page<DatingPlace> findAllByAdvancedFilters(DatingPlaceFilterDto filterDto, LocationDto locationDto, Pageable pageable) {
        return  findByAdvancedFilters(false, filterDto, locationDto, pageable);
    }

    @Override
    public Page<DatingPlace> findAllByAnyMatchAdvancedFilters(DatingPlaceFilterDto filterDto, LocationDto locationDto, Pageable pageable) {
     return  findByAdvancedFilters(true, filterDto, locationDto, pageable);
    }

    private Page<DatingPlace> findByAdvancedFilters(boolean isOr, DatingPlaceFilterDto filterDto, LocationDto locationDto, Pageable pageable) {

        Query query = generateQuery(false, isOr, filterDto, locationDto);
        int pageSize = pageable.getPageSize();
        query.setFirstResult(pageable.getOffset());
        query.setMaxResults(pageSize);
        List<DatingPlace> list = query.getResultList();
        query = generateQuery(true, isOr, filterDto, locationDto);
        long count = (Long)query.getSingleResult();
        return new PageImpl<>(list, pageable, count);
    }
    @Transactional(readOnly = true)
    public Query generateQuery(boolean isCount, boolean isOr, DatingPlaceFilterDto filterDto, LocationDto locationDto) {
        StringBuilder queryBuilder = new StringBuilder();
        String operation = isOr ? OR : AND; // enhance it later
        if (isCount) {
            queryBuilder.append("SELECT COUNT(dp) FROM DatingPlace dp ");
        } else {
            queryBuilder.append("SELECT dp FROM DatingPlace dp ");
        }
        if (filterDto.getDealsActive()) {
                queryBuilder.append(" JOIN dp.placeDeals dd "); //WHERE dp.placeApprovalStatus= 'APPROVED' AND (:date BETWEEN dd.startDate AND dd.expiryDate)   ");
        }
        if (!CollectionUtils.isEmpty(filterDto.getPlaceAtmosphere())){
            queryBuilder.append(" JOIN dp.placeAtmospheres pa ");
        }
        if (!CollectionUtils.isEmpty(filterDto.getFoodTypes())){
            queryBuilder.append(" JOIN dp.foodTypes pf ");
        }
        if (!CollectionUtils.isEmpty(filterDto.getPersonTypes())){
            queryBuilder.append(" JOIN dp.personTypes pt ");
        }


        queryBuilder.append("WHERE dp.placeApprovalStatus= 'APPROVED' ");
        boolean isFirstOr = true;

        if (filterDto.getDealsActive()) {
            queryBuilder.append(getOeration(isOr, isFirstOr) + " (:date BETWEEN dd.startDate AND dd.expiryDate) ");
            isFirstOr = false;
        }
        if (!CollectionUtils.isEmpty(filterDto.getPlaceAtmosphere())){
            queryBuilder.append(getOeration(isOr, isFirstOr) + " pa.atmosphere.id IN (:placeAtmospheres) ");
            isFirstOr = false;
        }
        if (!CollectionUtils.isEmpty(filterDto.getFoodTypes())){
            queryBuilder.append(getOeration(isOr, isFirstOr) + "pf.foodType.id IN (:foodTypes) ");
            isFirstOr = false;
        }
        if (!CollectionUtils.isEmpty(filterDto.getPersonTypes())){
            queryBuilder.append(getOeration(isOr, isFirstOr) + " pt.id.personTypeId IN (:personTypes) ");
            isFirstOr = false;
        }

        if (locationDto.getLatitude() != null && locationDto.getLongitude() != null) {
            queryBuilder.append(getOeration(isOr, isFirstOr) + "SQRT((ABS(dp.latitude - :latitude) * ABS(dp.latitude - :latitude)) + (ABS(dp.longitude - :longitude) * ABS(dp.longitude - :longitude))) * 70 <= :radius");
            isFirstOr = false;
        }
        if (locationDto.getPlaceName() != null) {
            queryBuilder.append(getOeration(isOr, isFirstOr) + "dp.placeName LIKE CONCAT('%',:placeName,'%')");
            isFirstOr = false;
        }

        if(filterDto.getNeighborhood() != null && filterDto.getNeighborhood() != NeighborHood.NA) {
            queryBuilder.append(getOeration(isOr, isFirstOr) + " dp.neighborHood LIKE CONCAT('%',:neighbor,'%')");
            isFirstOr = false;
        }
        if(filterDto.getPetsAllowed() != null) {
            queryBuilder.append(getOeration(isOr, isFirstOr) + " dp.isPetsAllowed = :pets");
            isFirstOr = false;
        }

        if(filterDto.getParking() != null) {
            queryBuilder.append(getOeration(isOr, isFirstOr) + " dp.parkingAverage >= :parking");
            isFirstOr = false;
        }
        if(filterDto.getSecurity() != null) {
            queryBuilder.append(getOeration(isOr, isFirstOr) + " dp.securityAverage >= :security");
            isFirstOr = false;
        }
        if(filterDto.getTransport() != null) {
            queryBuilder.append(getOeration(isOr, isFirstOr) + " dp.transportAverage >= :transport");
            isFirstOr = false;
        }

        if(!StringUtils.isEmpty(filterDto.getAgeRange()) && !filterDto.getAgeRange().equals(AgeRange.RANGE4.name())) {
            queryBuilder.append(getOeration(isOr, isFirstOr) + " dp.ageRange LIKE CONCAT('%',:ageRange,'%')");
            isFirstOr = false;
        }

        if(filterDto.getGender() != null) {
            switch (filterDto.getGender()) {
                case BOTH:
                    queryBuilder.append(getOeration(isOr, isFirstOr) + " dp.suitableForBoth > 0");
                    isFirstOr = false;
                    break;
                case FEMALE:
                    queryBuilder.append(getOeration(isOr, isFirstOr) + " dp.suitableForFemale > 0");
                    isFirstOr = false;
                    break;
                case MALE:
                    queryBuilder.append(getOeration(isOr, isFirstOr) + " dp.suitableForMaleCount > 0");
                    isFirstOr = false;
                    break;
            }
        }

        if(filterDto.getPriceRange() != null && filterDto.getPriceRange() != PriceRange.NA) {
            queryBuilder.append(getOeration(isOr, isFirstOr) + " dp.priceRange LIKE CONCAT('%',:priceRange,'%')");
            isFirstOr = false;
        }
        if(filterDto.getGoodForCouple() != null) {
            queryBuilder.append(getOeration(isOr, isFirstOr) + " dp.isGoodForMarriedCouple = :couple");
            isFirstOr = false;
        }

        if(isOr && !isFirstOr)
          queryBuilder.append(") ");

        if(!isCount)
            queryBuilder.append(" ORDER BY dp.averageRating DESC");


        Query query = entityManager.createQuery(queryBuilder.toString());

        if (filterDto.getDealsActive()) {
            query.setParameter("date", new Date(filterDto.getCurrentDate()));
        }

        if (locationDto.getLatitude() != null && locationDto.getLongitude() != null) {
            query.setParameter("latitude", locationDto.getLatitude());
            query.setParameter("longitude", locationDto.getLongitude());
            query.setParameter("radius", 50D);
        }
        if (locationDto.getPlaceName() != null) {
            query.setParameter("placeName", locationDto.getPlaceName());
        }

        if(filterDto.getNeighborhood() != null&& filterDto.getNeighborhood() != NeighborHood.NA) {
            query.setParameter("neighbor", filterDto.getNeighborhood().name());
        }
        if(filterDto.getPetsAllowed() != null) {
            query.setParameter("pets", filterDto.getPetsAllowed());
        }

        if(filterDto.getParking() != null) {
            query.setParameter("parking", filterDto.getParking().getValue());
        }
        if(filterDto.getSecurity() != null) {
            query.setParameter("security", filterDto.getSecurity().getValue());
        }
        if(filterDto.getTransport() != null) {
            query.setParameter("transport", filterDto.getTransport().getValue());
        }

        if(!StringUtils.isEmpty(filterDto.getAgeRange()) && !filterDto.getAgeRange().equals(DatingAgeRange.RANGE4.name())) {
            query.setParameter("ageRange", filterDto.getAgeRange());
        }

        if(filterDto.getPriceRange() != null && filterDto.getPriceRange() != PriceRange.NA) {
            query.setParameter("priceRange", filterDto.getPriceRange().name());
        }
        if(filterDto.getGoodForCouple() != null) {
            query.setParameter("couple", filterDto.getGoodForCouple());
        }

        if (!CollectionUtils.isEmpty(filterDto.getPlaceAtmosphere())){
            query.setParameter("placeAtmospheres", filterDto.getPlaceAtmosphere());
        }
        if (!CollectionUtils.isEmpty(filterDto.getFoodTypes())){
            query.setParameter("foodTypes", filterDto.getFoodTypes());
        }
        if (!CollectionUtils.isEmpty(filterDto.getPersonTypes())){
            query.setParameter("personTypes", filterDto.getPersonTypes());
        }
        return query;
    }


    private String getOeration(boolean isOr, boolean isFirst) {
        if (isOr) {
            if (isFirst) {
                return " AND ( ";
            }
            return OR;
        }
        return AND;
    }
}
