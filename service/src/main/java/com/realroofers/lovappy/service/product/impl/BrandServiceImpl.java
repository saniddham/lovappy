package com.realroofers.lovappy.service.product.impl;

import com.realroofers.lovappy.service.gift.dto.FilerObject;
import com.realroofers.lovappy.service.product.BrandService;
import com.realroofers.lovappy.service.product.dto.BrandDto;
import com.realroofers.lovappy.service.product.dto.mobile.BrandItemDto;
import com.realroofers.lovappy.service.product.model.Brand;
import com.realroofers.lovappy.service.product.repo.BrandRepo;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {

    private final BrandRepo brandRepo;
    private final UserService userService;

    public BrandServiceImpl(BrandRepo brandRepo, UserService userService) {
        this.brandRepo = brandRepo;
        this.userService = userService;
    }

    @Transactional(readOnly = true)
    @Override
    public List<BrandDto> findAll() {
        List<BrandDto> list = new ArrayList<>();
        brandRepo.findAll().stream().forEach(brand -> list.add(new BrandDto(brand)));
        return list;
    }

    @Transactional(readOnly = true)
    @Override
    public List<BrandDto> findAllActive() {
        List<BrandDto> list = new ArrayList<>();
        brandRepo.findAllByActiveIsTrue().stream().forEach(brand -> list.add(new BrandDto(brand)));
        return list;
    }

    @Transactional
    @Override
    public BrandDto create(BrandDto brand) throws Exception {
        return new BrandDto(brandRepo.saveAndFlush(new Brand(brand)));
    }

    @Transactional
    @Override
    public BrandDto update(BrandDto brand) throws Exception {
        return new BrandDto(brandRepo.save(new Brand(brand)));
    }

    @Transactional
    @Modifying
    @Override
    public void delete(Integer integer) throws Exception {
        brandRepo.delete(integer);
    }

    @Transactional(readOnly = true)
    @Override
    public BrandDto findById(Integer integer) {
        Brand brand = brandRepo.findOne(integer);
        return brand != null ? new BrandDto(brand) : null;
    }

    @Override
    public BrandDto findByName(String name) {
        Brand brand = brandRepo.findByBrandName(name);
        return brand != null ? new BrandDto(brand) : null;
    }

    @Override
    public String[] getStringArray() {

        List<BrandDto> brandDtos = findAllActive();
        int i = brandDtos.size();
        int n = ++i;
        String[] stringArray = new String[n];
        for (int cnt = 0; cnt < brandDtos.size(); cnt++) {
            stringArray[cnt] = brandDtos.get(cnt).getBrandName();
        }
        return stringArray;
    }

    @Transactional(readOnly = true)
    @Override
    public long getBrandCountByUser(int userId) {
        User user = userService.getUserById(userId);
        return brandRepo.countAllByCreatedBy(user);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Brand> findBrandsByLoginUser(int userId, Pageable pageable, FilerObject filerObject) {

        String name = (filerObject != null && filerObject.getFilterType() != null
                && !filerObject.getSearchTerm().equals("")) ? "%" + filerObject.getSearchTerm() + "%" : null;

        User user = userService.getUserById(userId);
        return brandRepo.findAllByCreatedBy(user, name, pageable);
    }

    @Override
    public List<BrandItemDto> findAllForMobile() {
        List<BrandItemDto> list = new ArrayList<>();
        list.add(new BrandItemDto(0, "All"));
        brandRepo.findAllByActiveIsTrue().stream().forEach(brand -> list.add(new BrandItemDto(brand)));
        return list;
    }
}
