package com.realroofers.lovappy.service.music.model;

import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 9/23/2017.
 */
@Embeddable
@EqualsAndHashCode
public class MusicUserPK implements Serializable {
    private User user;

    private Music music;

    public MusicUserPK(User user, Music music) {
        this.user = user;
        this.music = music;
    }

    public MusicUserPK() {
    }

    @ManyToOne
    @JoinColumn(name = "user_id")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne
    @JoinColumn(name = "music_id")
    public Music getMusic() {
        return music;
    }

    public void setMusic(Music music) {
        this.music = music;
    }
}
