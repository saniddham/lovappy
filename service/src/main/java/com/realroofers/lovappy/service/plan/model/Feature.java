package com.realroofers.lovappy.service.plan.model;

import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * Created by hasan on 6/11/2017.
 */
@Entity(name = "plan_features")
@EqualsAndHashCode
public class Feature {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer featureID;


    @ManyToOne(cascade = CascadeType.ALL)
    private Plan plan;

    private String featureDetails;
    private String featureNote;
    private String noteColor;

    private Boolean isValid;


    public Feature() {
    }

    public Feature(Plan plan, String featureDetails, String featureNote, String noteColor, Boolean isValid) {
        this.plan = plan;
        this.featureDetails = featureDetails;
        this.featureNote = featureNote;
        this.noteColor = noteColor;
        this.isValid = isValid;
    }

    public Boolean getIsValid() {
        return isValid;
    }

    public void setIsValid(Boolean valid) {
        isValid = valid;
    }

    public Integer getFeatureID() {
        return featureID;
    }

    public void setFeatureID(Integer featureID) {
        this.featureID = featureID;
    }

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    public String getFeatureDetails() {
        return featureDetails;
    }

    public void setFeatureDetails(String featureDetails) {
        this.featureDetails = featureDetails;
    }

    public String getFeatureNote() {
        return featureNote;
    }

    public void setFeatureNote(String featureNote) {
        this.featureNote = featureNote;
    }

    public String getNoteColor() {
        return noteColor;
    }

    public void setNoteColor(String noteColor) {
        this.noteColor = noteColor;
    }
}
