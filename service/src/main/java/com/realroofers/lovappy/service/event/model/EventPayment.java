package com.realroofers.lovappy.service.event.model;

import com.realroofers.lovappy.service.event.support.UserEventStatus;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Darrel Rayen on 8/30/17
 */
//@Entity
//@Table(name = "event_payment")
public class EventPayment implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "payment_id", nullable = false, updatable = false)
    private Integer id;

    private String paymentType;

    private Double paymentAmount;

    //Foreign Key
    private Integer eventId;

    //Foreign Key
    private Integer userId;

    @CreatedDate
    private Date paidDate;

    private String paymentReference;

    public EventPayment() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public Date getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(Date paidDate) {
        this.paidDate = paidDate;
    }

    public String getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }
}
