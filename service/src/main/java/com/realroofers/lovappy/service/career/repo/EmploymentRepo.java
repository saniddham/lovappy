package com.realroofers.lovappy.service.career.repo;

import com.realroofers.lovappy.service.career.model.Employment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmploymentRepo extends JpaRepository<Employment, Long> {
}
