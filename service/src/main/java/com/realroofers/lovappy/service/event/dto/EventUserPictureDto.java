package com.realroofers.lovappy.service.event.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.event.model.Event;
import com.realroofers.lovappy.service.event.model.EventUserPicture;
import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;

/**
 * Created by Tejaswi Venupalli on 9/15/17.
 */
@EqualsAndHashCode
public class EventUserPictureDto {

    private Integer id;
    private Event eventId;
    private User userId;
    private CloudStorageFileDto picture;

    public EventUserPictureDto(){

    }

    public EventUserPictureDto(EventUserPicture eventUserPicture){
        this.id = eventUserPicture.getId();
        this.eventId = eventUserPicture.getEventId();
        this.userId = eventUserPicture.getUserId();
        this.picture = new CloudStorageFileDto(eventUserPicture.getPicture());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Event getEventId() {
        return eventId;
    }

    public void setEventId(Event eventId) {
        this.eventId = eventId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public CloudStorageFileDto getPicture() {
        return picture;
    }

    public void setPicture(CloudStorageFileDto picture) {
        this.picture = picture;
    }
}
