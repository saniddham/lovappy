package com.realroofers.lovappy.service.news;

import com.realroofers.lovappy.service.news.dto.NewsCategoryDto;
import com.realroofers.lovappy.service.news.model.NewsCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface NewsCategoryService {
    List<NewsCategory> getAllNewsCategory();

    NewsCategory getNewsCategoryByName(String categoryName);
    NewsCategory getOne(Long id);

    NewsCategory getNewsCategoryByCategoryId(Long categoryId);

    NewsCategory save(NewsCategory category);

    List<NewsCategory> getActiveNewsCategory();

    Page<NewsCategoryDto> findAll(PageRequest pageable);

    NewsCategoryDto getCategoryById(Long id);

    NewsCategoryDto getCategoryByName(String categoryName);

    NewsCategoryDto save(NewsCategoryDto category);

    void delete(Integer id);
}
