package com.realroofers.lovappy.service.event.model;

import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Tejaswi Venupalli on 8/28/17
 */

@Entity
@Table(name = "event_email")
@EqualsAndHashCode
public class EventUserEmail {

    @Id
    @GeneratedValue
    private Integer eventEmailId;

    private String emailId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User userId;

    @Type(type="text")
    private String title;

    @Type(type="text")
    private String emailBody;
    private Date sentDate;

    public Integer getEventEmailId() {
        return eventEmailId;
    }

    public void setEventEmailId(Integer eventEmailId) {
        this.eventEmailId = eventEmailId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }
}
