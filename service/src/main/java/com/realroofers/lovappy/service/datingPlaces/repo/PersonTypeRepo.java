package com.realroofers.lovappy.service.datingPlaces.repo;

import com.realroofers.lovappy.service.datingPlaces.model.FoodType;
import com.realroofers.lovappy.service.datingPlaces.model.PersonType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Darrel Rayen on 10/19/17.
 */
@Repository
public interface PersonTypeRepo extends JpaRepository<PersonType, Integer> {

    PersonType findByPersonTypeName(String foodName);
    PersonType findByPersonTypeNameAndEnabledTrue(String foodName);
    List<PersonType> findAllByEnabledTrue();
}
