package com.realroofers.lovappy.service.radio.support;

public enum StartSoundType {
    MALE, FEMALE, ADVERTISEMENT, NEWS, COMEDY, PODCAST, MUSIC, AUDIO_BOOK
}