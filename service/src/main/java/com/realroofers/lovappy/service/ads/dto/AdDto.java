package com.realroofers.lovappy.service.ads.dto;

import com.realroofers.lovappy.service.ads.model.Ad;
import com.realroofers.lovappy.service.ads.model.AdTargetArea;
import com.realroofers.lovappy.service.ads.support.*;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.core.TextAlign;
import com.realroofers.lovappy.service.core.VideoProvider;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.util.CommonUtils;
import com.realroofers.lovappy.service.validator.SubmitAdvertisement;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Eias Altawil on 7/7/2017
 */
@Data
@ToString
@EqualsAndHashCode
public class AdDto {
    private Integer id;

//    @NotEmpty(message = "Email address is required.", groups = {SubmitAdvertisement.class})
//    @NotNull(message = "Email address is required.", groups = {SubmitAdvertisement.class})
//    @Email(message = "This is not a valid email address.", groups = {SubmitAdvertisement.class})
    private String email;

    private Integer length;
    private Boolean promote;
    private String coupon;
    private CloudStorageFileDto mediaFileCloud;
    private CloudStorageFileDto backgroundFileCloud;
    private CloudStorageFileDto featuredAdBackgroundFileCloud;
    private AudioType audioType;
    private AdType adType;
    private AdMediaType mediaType;

    private List<AdTargetAreaDto> targetAreas;

    @NotNull(message = "Required.", groups = {SubmitAdvertisement.class})
    private Gender gender;

    @NotNull(message = "Required.", groups = {SubmitAdvertisement.class})
    private AgeRange ageRange;

    @NotNull(message = "Required.", groups = {SubmitAdvertisement.class})
    private LanguageDto language;

    private Map<String, String> preferences;
    private AdStatus status;

    @NotEmpty(message = "URL is required.", groups = {SubmitAdvertisement.class})
    @NotNull(message = "URL is required.", groups = {SubmitAdvertisement.class})
    private String url;

    private Boolean featured = false;

    private String text;
    private String textColor;
    private String backgroundColor;
    private Integer backgroundColorID;
    private Long backgroundImageID;
    private Long featuredAdBackgroundID;

    private Double price;
    private String mediaFileUrl;
    private TextAlign textAlign;

    public AdDto() {
    }

    public AdDto(Ad ad) {
        if(ad != null) {
            this.id = ad.getId();
            this.email = ad.getEmail();
            this.length = ad.getLength();
            this.promote = ad.getPromote();
            this.coupon = ad.getCoupon();
            this.mediaFileCloud = new CloudStorageFileDto(ad.getMediaFileCloud());
            this.backgroundFileCloud = new CloudStorageFileDto(ad.getBackgroundFileCloud());
            this.featuredAdBackgroundFileCloud = new CloudStorageFileDto(ad.getFeaturedAdBackgroundFileCloud());
            this.audioType = ad.getAudioType();
            this.adType = ad.getAdType();
            this.mediaType = ad.getMediaType();
            this.gender = ad.getGender();
            this.ageRange = ad.getAgeRange();
            this.language = new LanguageDto(ad.getLanguage());
            this.preferences = ad.getPreferences();
            this.status = ad.getStatus();
            this.url = ad.getUrl();
            this.text = ad.getText();
            this.textColor = ad.getTextColor();
            this.backgroundColor = ad.getBackgroundColor();
            this.featured = ad.getFeatured();
            this.price = ad.getPrice();
            this.mediaFileUrl = ad.getMediaFileUrl();
            this.textAlign = ad.getTextAlign() == null ? TextAlign.CENTER : ad.getTextAlign();
            if (ad.getTargetAreas() != null){
                this.targetAreas = new ArrayList<>();
                for (AdTargetArea adTargetArea : ad.getTargetAreas()) {
                    this.targetAreas.add(new AdTargetAreaDto(adTargetArea));
                }
            }
        }
    }

    public AdDto(String email, Integer length, Boolean promote, String coupon,
                 CloudStorageFileDto audioFile, AudioType audioType, AdType adType,
                 List<AdTargetAreaDto> targetAreas, Gender gender, AgeRange ageRange, LanguageDto language,
                 Map<String, String> preferences, String url, AdMediaType mediaType,
                 String text, String textColor, Integer backgroundColorID, Long backgroundImageID,
                 Boolean featured, Double price, String mediaFileUrl, Long featuredAdBackgroundID, TextAlign textAlign) {
        this.email = email;
        this.length = length;
        this.promote = promote;
        this.coupon = coupon;
        this.mediaFileCloud = audioFile;
        this.audioType = audioType;
        this.adType = adType;
        this.mediaType = mediaType;
        this.targetAreas = targetAreas;
        this.gender = gender;
        this.ageRange = ageRange;
        this.language = language;
        this.preferences = preferences;
        this.url = url;
        this.text = text;
        this.textColor = textColor;
        this.backgroundColorID = backgroundColorID;
        this.backgroundImageID = backgroundImageID;
        this.featured = featured == null ? false : featured;
        this.price = price;
        this.mediaFileUrl=mediaFileUrl;
        this.featuredAdBackgroundID=featuredAdBackgroundID;
        this.textAlign = textAlign == null ? TextAlign.CENTER : textAlign;
    }

    public String getBackgroundFileCloudUrl() {
        if(AdMediaType.VIDEO.equals(getMediaType()) && backgroundFileCloud.getUrl() == null) {
            return "https://www.googleapis.com/download/storage/v1/b/lovappy-images-testing/o/1516386001315.?generation=1516386004450424&alt=media";
        }
        return backgroundFileCloud.getUrl();
    }

    public String getFeaturedAdBackgroundFileCloudUrl() {
        if(AdMediaType.VIDEO.equals(getMediaType()) && backgroundFileCloud.getUrl() == null) {
            return "https://www.googleapis.com/download/storage/v1/b/lovappy-images-testing/o/1517078088329.?generation=1517078091571765&alt=media";
        }
        return featuredAdBackgroundFileCloud.getUrl() !=null ? featuredAdBackgroundFileCloud.getUrl():backgroundFileCloud.getUrl();
    }

    public String getMediaFileUri() {

        if(AdMediaType.VIDEO.equals(getMediaType()) && mediaFileCloud.getUrl() == null) {
            if(!StringUtils.isEmpty(mediaFileUrl) && mediaFileUrl.contains("youtube")) {
                String newLink = "//www.youtube.com/embed/" + CommonUtils.findYoutubeVideoId(getMediaFileUrl());
                setMediaFileUrl(newLink);
            }
            return getMediaFileUrl();
        }
        return mediaFileCloud.getUrl();
    }

    public VideoProvider getVideoProvider() {

        if(AdMediaType.VIDEO.equals(getMediaType()) && mediaFileCloud.getUrl() == null) {
            if(!StringUtils.isEmpty(mediaFileUrl) && mediaFileUrl.contains("youtube"))
            return VideoProvider.YOUTUBE;
        }
        return VideoProvider.OTHER;
    }
}
