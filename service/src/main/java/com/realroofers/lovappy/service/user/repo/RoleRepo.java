package com.realroofers.lovappy.service.user.repo;

import com.realroofers.lovappy.service.user.model.Role;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface RoleRepo extends JpaRepository<Role, Integer> {
    Role findByName(String name);
    List<Role> findByNameIn(List<String> roles);

    @Query(nativeQuery = true, value = "SELECT r.* FROM role r INNER JOIN users_roles ur ON r.id = ur.role_id INNER JOIN user u ON u.user_id = ur.user_id AND u.user_id =:userId ;")
    List<Role> findUserRoles(@Param("userId") Integer userId);

}
