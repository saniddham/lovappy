package com.realroofers.lovappy.service.event.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.event.model.Event;
import com.realroofers.lovappy.service.event.model.EventLocationPicture;
import lombok.EqualsAndHashCode;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * Created by Tejaswi Venupalli on 8/28/2017.
 */
@EqualsAndHashCode
public class EventLocationPictureDto {

    private Integer id;
    private Event eventId;
    private CloudStorageFileDto picture;

    public EventLocationPictureDto() {

    }

    public EventLocationPictureDto(EventLocationPicture picture) {
        this.id = picture.getId();
        this.eventId = picture.getEventId();
        this.picture = new CloudStorageFileDto(picture.getPicture());

    }

    public EventLocationPicture getEventLocationPicture() {
        EventLocationPicture picture = new EventLocationPicture();
        BeanUtils.copyProperties(this, picture);
        return picture;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Event getEventId() {
        return eventId;
    }

    public void setEventId(Event eventId) {
        this.eventId = eventId;
    }

    public CloudStorageFileDto getPicture() {
        return picture;
    }

    public void setPicture(CloudStorageFileDto picture) {
        this.picture = picture;
    }

    public String getEventLocationPictureFileUrl() {
        return this.picture == null || StringUtils.isEmpty(this.picture.getUrl())
                ? "/images/icons/upload_icons/upload_profile_sample.png"
                : this.picture.getUrl();
    }
}
