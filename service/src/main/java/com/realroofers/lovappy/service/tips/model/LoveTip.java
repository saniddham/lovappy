package com.realroofers.lovappy.service.tips.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Daoud Shaheen on 1/1/2018.
 */
@Table
@Entity(name = "love_tips")
@Data
@DynamicUpdate
@EqualsAndHashCode
public class LoveTip extends BaseEntity<Integer> {

    private String content;
    private String backgroundImage;
    private String textColor;
    private String backgroundColor;
    @Column(nullable = false, columnDefinition = "BIT(1) default 0")
    private Boolean active;
}
