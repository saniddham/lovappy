package com.realroofers.lovappy.service.notification.impl;

import com.realroofers.lovappy.service.exception.ResourceNotFoundException;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.notification.NotificationService;
import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.Notification;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.notification.model.UserNotification;
import com.realroofers.lovappy.service.notification.model.UserNotificationPK;
import com.realroofers.lovappy.service.notification.repo.NotificationRepo;
import com.realroofers.lovappy.service.notification.repo.UserNotificationRepo;
import com.realroofers.lovappy.service.user.model.Role;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.repo.RoleRepo;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Daoud Shaheen on 6/24/2017.
 */
@Service
@Slf4j
public class NotificationServiceImpl implements NotificationService {

    private UserRepo userRepo;
    private NotificationRepo notificationRepo;
    private UserNotificationRepo userNotificationRepo;
    private RoleRepo roleRepo;
    private EmailTemplateService emailTemplateService;
    private final MailService mailService;
    private final ITemplateEngine templateEngine;
    @Value("${lovappy.baseurl:https://lovappy.com}")
    private String baseUrl;
    public NotificationServiceImpl(RoleRepo roleRepo, UserRepo userRepo, NotificationRepo notificationRepo, UserNotificationRepo userNotificationRepo, EmailTemplateService emailTemplateService, MailService mailService, ITemplateEngine templateEngine) {
        this.roleRepo = roleRepo;
        this.userRepo = userRepo;
        this.notificationRepo = notificationRepo;
        this.userNotificationRepo = userNotificationRepo;
        this.emailTemplateService = emailTemplateService;
        this.mailService = mailService;
        this.templateEngine = templateEngine;
    }

    @Override
    public void sendNotificationToAdmins(NotificationDto notificationDto) {
        notificationDto = sendNotificationsToAdmins(notificationDto);

        EmailTemplateDto emailTemplate = emailTemplateService.getByNameAndLanguage(EmailTemplateConstants.NEW_NOTIFICATION, "en");

        //send email to admin@lovappy.com
        if(emailTemplate != null) {
            emailTemplate.setBody(notificationDto.getContent());
            emailTemplate.setSubject(notificationDto.getType().getValue());
            emailTemplate.getAdditionalInformation().put("url", baseUrl + "/lox/notifications/" + notificationDto.getId());
             sendEmail(baseUrl, "admin@lovappy.com", emailTemplate);
        }
    }

    private void sendEmail(String baseUrl, String email, EmailTemplateDto emailTemplateDto) {
        try {
            Context context = new Context();

            String body = emailTemplateDto.getBody();


            context.setVariable("body", emailTemplateDto.getBody());
            context.setVariable("link", emailTemplateDto.getAdditionalInformation().get("url"));
            context.setVariable("baseUrl", baseUrl);
            context.setVariable("emailBG", emailTemplateDto.getImageUrl());
            context.setVariable("title", emailTemplateDto.getSubject());

            context.setVariable("buttonText", emailTemplateDto.getAdditionalInformation().get("buttonText") == null ? "Click Here" : emailTemplateDto.getAdditionalInformation().get("buttonText"));
            String content = templateEngine.process(emailTemplateDto.getTemplateUrl(), context);
            mailService.sendMail(email, content, emailTemplateDto.getSubject(), emailTemplateDto.getFromEmail(), emailTemplateDto.getFromName());
        } catch (Exception ex) {
            log.error("Failed to send email: {}", ex);
        }
    }
    @Transactional
    public NotificationDto sendNotificationsToAdmins(NotificationDto notificationDto) {
        Notification notification = new Notification();
        notification.setContent(notificationDto.getContent());
        notification.setType(notificationDto.getType());
        notification.setUserId(notificationDto.getUserId());
        notification.setSourceId(notificationDto.getSourceId());
        notificationRepo.save(notification);
        List<Role> roles = roleRepo.findByNameIn(Arrays.asList(Roles.SUPER_ADMIN.getValue(), Roles.ADMIN.getValue()));

        int page = 0;

        Page<User> adminPage = userRepo.findByRolesIn(new HashSet<>(roles), new PageRequest(page, 10));

        Set<UserNotification> usersNotifications = new HashSet<>(0);

        do {
            page++;
            for (User admin : adminPage.getContent()) {
                UserNotificationPK pk = new UserNotificationPK(admin, notification);
                UserNotification userNotification = new UserNotification();
                userNotification.setId(pk);
                userNotification.setSeen(false);
                usersNotifications.add(userNotification);
            }
            adminPage = userRepo.findByRolesIn(new HashSet<>(roles), new PageRequest(page, 10));
        } while (page < adminPage.getTotalPages());
        userNotificationRepo.save(usersNotifications);

        return new NotificationDto(notification);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<NotificationDto> getPageOfNotificaionByUser(User user, int page, int limit) {
        Pageable pageRequest = new PageRequest(page - 1, limit, new Sort(Sort.Direction.DESC, "created"));
        return notificationRepo.findPageByUser(user, pageRequest).map(NotificationDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<NotificationDto> getPageOfNotificaionByUserAndType(User user, NotificationTypes type, int page, int limit) {
        Pageable pageRequest = new PageRequest(page - 1, limit, new Sort(Sort.Direction.DESC, "created"));
        return notificationRepo.findPageByUserAndType(user, type, pageRequest).map(NotificationDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<NotificationDto> getPageOfNotificaionByUserExceptRegistartionAndBlog(User user, int page, int limit) {
        Pageable pageRequest = new PageRequest(page - 1, limit, new Sort(Sort.Direction.DESC, "created"));
        List<NotificationTypes> types = new ArrayList<>();
        types.add(NotificationTypes.USER_BLOGS);
        types.add(NotificationTypes.USER_REGISTRATION);
        return notificationRepo.findPageByUserAndNotTypeIn(user, types, pageRequest).map(NotificationDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Integer countNotSeenNotificationsByUser(User user) {
        return notificationRepo.countBySeenAndUser(false, user);
    }

    @Override
    @Transactional(readOnly = true)
    public Integer countNotSeenNotificationsByUserAndType(User user, NotificationTypes type) {
        return notificationRepo.countBySeenAndUserAndType(false, user, type);
    }

    @Override
    @Transactional(readOnly = true)
    public Integer countNotSeenNotificationsByUserExceptRegistrationAndBlog(User user) {
        List<NotificationTypes> types = new ArrayList<>();
        types.add(NotificationTypes.USER_BLOGS);
        types.add(NotificationTypes.USER_REGISTRATION);
        return notificationRepo.countBySeenAndUserAndNotInTypes(false, user, types);
    }

    @Override
    @Transactional
    public void setNotificaitonAsSeenByUser(Long notificaitonId, User user) {
        Notification notification = notificationRepo.findOne(notificaitonId);
        if (notification == null) {
            throw new ResourceNotFoundException("Notification with " + notificaitonId + " is not found");
        }

        UserNotificationPK pk = new UserNotificationPK(user, notification);
        UserNotification userNotification = userNotificationRepo.findById(pk);
        userNotification.setSeen(true);
        userNotificationRepo.save(userNotification);

    }

    @Override
    @Transactional
    public void setAllNotificaitonAsSeenByUser(User user) {
        userNotificationRepo.updateSeenByUser(user);
    }

    @Override
    @Transactional
    public void setAllNotificaitonAsSeenByUserAndType(User user, NotificationTypes type) {
        userNotificationRepo.updateSeenByUserIdAndType(user.getUserId(), type.name());
    }

    @Transactional
    @Override
    public void setAllNotificaitonAsSeenByUserAndTypes(User user, List<NotificationTypes> types) {
        userNotificationRepo.updateSeenByUserIdAndTypes(user.getUserId(), types.stream().map(type -> type.name()).collect(Collectors.toList()));
    }

    @Override
    @Transactional(readOnly = true)
    public NotificationDto getNotificationById(Long id) {
        return new NotificationDto(notificationRepo.findOne(id));
    }
}
