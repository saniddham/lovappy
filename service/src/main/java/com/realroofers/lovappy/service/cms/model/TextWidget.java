package com.realroofers.lovappy.service.cms.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Daoud Shaheen on 6/6/2017.
 */
@Entity
@Table(name="text_widget")
public class TextWidget extends Widget {

    @ManyToMany(mappedBy = "textContents")
    @JsonBackReference
    private Set<Page> pages = new HashSet<>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "textWidget", cascade = CascadeType.PERSIST)
    @JsonManagedReference
    private Set<TextTranslation> translations = new HashSet<>(0);

    public Set<Page> getPages() {
        return pages;
    }
    public void setPages(Set<Page> pages) {
        this.pages = pages;
    }

    public Set<TextTranslation> getTranslations() {
        return translations;
    }

    public void setTranslations(Set<TextTranslation> translations) {
        this.translations = translations;
    }


    @Override
    public String toString() {
        return "TextWidget{" +
                '}';
    }
}
