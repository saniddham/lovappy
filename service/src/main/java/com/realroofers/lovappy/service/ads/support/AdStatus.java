package com.realroofers.lovappy.service.ads.support;

/**
 * Created by Eias Altawil on 7/9/2017
 */
public enum AdStatus {
    PENDING, APPROVED, REJECTED, DISABLED
}
