package com.realroofers.lovappy.service.user.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 5/12/2018.
 */
@Data
@EqualsAndHashCode
public class DeveloperRegistration implements Serializable {
  private String name;
  private String zipCode;
  private String email;
  private String phoneNumber;
  private String streetAddress;
  private String state;
  private String city;
  private String skills;

}
