package com.realroofers.lovappy.service.mail.repo;

import com.realroofers.lovappy.service.mail.model.LovappyEmail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Daoud Shaheen on 9/7/2017.
 */
public interface LovappyEmailRepo extends JpaRepository<LovappyEmail, Integer> {
    LovappyEmail findByEmail(String email);
}
