package com.realroofers.lovappy.service.event.dto;

import com.realroofers.lovappy.service.event.model.Event;
import com.realroofers.lovappy.service.event.model.EventComment;
import com.realroofers.lovappy.service.event.model.EventLocationPicture;
import com.realroofers.lovappy.service.event.model.embeddable.EventLocation;
import com.realroofers.lovappy.service.event.support.EventDateUtil;
import com.realroofers.lovappy.service.event.support.EventStatus;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * Created by Tejaswi Venupalli on 8/29/17
 */
@EqualsAndHashCode
public class EventDto {

    private Integer eventId;
    private String eventTitle;
    private String eventDescription;
    private Date eventDate;
    private String startTime;
    private String finishTime;
    private EventLocationDto eventLocation;
    private String eventLocationDescription;
    private Integer attendeesLimit;
    private Integer outsiderCount;
    private Double admissionCost;
    private EventStatus eventStatus;
    private ApprovalStatus eventAprrovalStatus;
    private Boolean approved;
    private String timeZone;

    @NotNull
    private Integer ambassadorUserId;

    private Collection<User> userList = new ArrayList<>();
    private String eventCategories;
    private Map<String, String> targetedAudience;
    private List<EventLocationPictureDto> locationPictures = new ArrayList<>();
    private Collection<EventComment> comments = new ArrayList<>();

    public EventDto() {

    }

    public EventDto(String eventTitle, String eventDescription, Date eventDate, String startTime, String finishTime,
                    Integer outsiderCount, Double admissionCost, String eventCategories) {
        this.eventTitle = eventTitle;
        this.eventDescription = eventDescription;
        this.eventDate = eventDate;
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.outsiderCount = outsiderCount;
        this.admissionCost = admissionCost;
        this.eventCategories = eventCategories;

    }

    public EventDto(Integer eventId, EventLocation eventLocation, String startTime, Date eventDate, String eventTitle) {
        this.eventTitle = eventTitle;
        this.eventId = eventId;
        this.eventLocation = EventLocationDto.toEventLocationDto(eventLocation);
        this.startTime = startTime;
        this.eventDate = eventDate;
    }

    public EventDto(Event event) {
        this.eventId = event.getEventId();
        this.eventTitle = event.getEventTitle();
        this.eventDescription = event.getEventDescription();
        this.eventDate = event.getEventDate();
        this.startTime = EventDateUtil.convertToString(event.getStartTime());
        this.finishTime = EventDateUtil.convertToString(event.getFinishTime());
        this.eventLocation = EventLocationDto.toEventLocationDto(event.getEventLocation());
        this.eventLocationDescription = event.getEventLocationDescription();
        this.attendeesLimit = event.getAttendeesLimit();
        this.outsiderCount = event.getOutsiderCount();
        this.admissionCost = event.getAdmissionCost();
        this.eventStatus = event.getEventStatus();
        this.eventAprrovalStatus = event.getEventApprovalStatus();
        this.ambassadorUserId = event.getAmbassadorUserId();
        this.targetedAudience = event.getTargetedAudience();
        this.locationPictures = getImageList(event.getLocationPictures());
        this.timeZone = event.getTimeZone();
        this.approved = event.getApproved() == null ? false : event.getApproved();

    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public EventLocationDto getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(EventLocationDto eventLocation) {
        this.eventLocation = eventLocation;
    }

    public String getEventLocationDescription() {
        return eventLocationDescription;
    }

    public void setEventLocationDescription(String eventLocationDescription) {
        this.eventLocationDescription = eventLocationDescription;
    }

    public Integer getAttendeesLimit() {
        return attendeesLimit;
    }

    public void setAttendeesLimit(Integer attendeesLimit) {
        this.attendeesLimit = attendeesLimit;
    }

    public Integer getOutsiderCount() {
        return outsiderCount;
    }

    public void setOutsiderCount(Integer outsiderCount) {
        this.outsiderCount = outsiderCount;
    }

    public Double getAdmissionCost() {
        return admissionCost;
    }

    public void setAdmissionCost(Double admissionCost) {
        this.admissionCost = admissionCost;
    }

    public EventStatus getEventStatus() {
        return eventStatus;
    }

    public void setEventStatus(EventStatus eventStatus) {
        this.eventStatus = eventStatus;
    }

    public ApprovalStatus getEventAprrovalStatus() {
        return eventAprrovalStatus;
    }

    public void setEventAprrovalStatus(ApprovalStatus eventAprrovalStatus) {
        this.eventAprrovalStatus = eventAprrovalStatus;
    }

    public Integer getAmbassadorUserId() {
        return ambassadorUserId;
    }

    public void setAmbassadorUserId(Integer ambassadorUserId) {
        this.ambassadorUserId = ambassadorUserId;
    }

    public Collection<User> getUserList() {
        return userList;
    }

    public void setUserList(Collection<User> userList) {
        this.userList = userList;
    }

    public String getEventCategories() {
        return eventCategories;
    }

    public void setEventCategories(String eventCategories) {
        this.eventCategories = eventCategories;
    }

    public Map<String, String> getTargetedAudience() {
        return targetedAudience;
    }

    public void setTargetedAudience(Map<String, String> targetedAudience) {
        this.targetedAudience = targetedAudience;
    }

    public List<EventLocationPictureDto> getLocationPictures() {
        return locationPictures;
    }

    public void setLocationPictures(List<EventLocationPictureDto> locationPictures) {
        this.locationPictures = locationPictures;
    }

    public Collection<EventComment> getComments() {
        return comments;
    }

    public void setComments(Collection<EventComment> comments) {
        this.comments = comments;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public List<EventLocationPictureDto> getImageList(List<EventLocationPicture> eventLocationPictureList) {

        List<EventLocationPictureDto> eventLocationPictureDtoList = new ArrayList<>();

        eventLocationPictureList.forEach(eventLocationPicture -> {

            eventLocationPictureDtoList.add(new EventLocationPictureDto(eventLocationPicture));
        });
        return eventLocationPictureDtoList;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
}
