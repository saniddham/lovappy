package com.realroofers.lovappy.service.ads.dto;

import com.realroofers.lovappy.service.ads.model.AdPricing;
import com.realroofers.lovappy.service.ads.support.AdMediaType;
import com.realroofers.lovappy.service.ads.support.AdType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
public class AdPricingDto {

    private Integer id;
    private Integer duration;
    private String durationUnit;
    private AdType adType;
    private AdMediaType mediaType;
    private Double price;

    public AdPricingDto() {
    }

    public AdPricingDto(AdPricing adPricing) {
        this.id = adPricing.getId();
        this.adType = adPricing.getAdType();
        this.mediaType = adPricing.getMediaType();
        this.price = adPricing.getPrice();

        if(adPricing.getDuration() != null) {
            this.duration = adPricing.getDuration() > 60 ? (adPricing.getDuration() / 60) : adPricing.getDuration();
            this.durationUnit = adPricing.getDuration() > 60 ? "Minutes" : "Seconds";
        }
    }
}
