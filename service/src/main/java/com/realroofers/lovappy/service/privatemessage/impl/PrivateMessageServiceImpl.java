package com.realroofers.lovappy.service.privatemessage.impl;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.credits.model.UserCreditPK;
import com.realroofers.lovappy.service.credits.model.UserCredits;
import com.realroofers.lovappy.service.credits.repo.UserCreditRepo;
import com.realroofers.lovappy.service.exception.ResourceNotFoundException;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.CouponDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.repo.CouponRepo;
import com.realroofers.lovappy.service.order.repo.OrderRepo;
import com.realroofers.lovappy.service.order.repo.PriceRepo;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.privatemessage.PrivateMessageService;
import com.realroofers.lovappy.service.privatemessage.dto.PrivateMessageDto;
import com.realroofers.lovappy.service.privatemessage.model.PrivateMessage;
import com.realroofers.lovappy.service.privatemessage.model.QPrivateMessage;
import com.realroofers.lovappy.service.privatemessage.repo.PrivateMessageRepo;
import com.realroofers.lovappy.service.privatemessage.support.Status;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.system.repo.LanguageRepo;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.repo.UserProfileRepo;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by Eias Altawil on 5/7/17
 */

@Service
public class PrivateMessageServiceImpl implements PrivateMessageService {
    private final UserRepo userRepo;
    private final PrivateMessageRepo privateMessageRepo;
    private final LanguageRepo languageRepo;
    private OrderRepo orderRepo;
    private final CouponRepo couponRepo;
    private final OrderService orderService;
    private final UserProfileRepo userProfileRepo;
    private final PriceRepo priceRepo;
    private final UserCreditRepo userCreditRepo;

    @Autowired
    public PrivateMessageServiceImpl(UserRepo userRepo, PrivateMessageRepo privateMessageRepo, OrderRepo orderRepo,
                                     LanguageRepo languageRepo, CouponRepo couponRepo, OrderService orderService, UserProfileRepo userProfileRepo, PriceRepo priceRepo, UserCreditRepo userCreditRepo) {
        this.userRepo = userRepo;
        this.privateMessageRepo = privateMessageRepo;
        this.orderRepo = orderRepo;
        this.languageRepo = languageRepo;
        this.couponRepo = couponRepo;
        this.orderService = orderService;
        this.userProfileRepo = userProfileRepo;
        this.priceRepo = priceRepo;
        this.userCreditRepo = userCreditRepo;
    }


    @Override
    @Transactional(readOnly = true)
    public Collection<PrivateMessageDto> getAllPrivateMessages() {
        Collection<PrivateMessageDto> privateMessages = new ArrayList<>();

        Iterable<PrivateMessage> message = privateMessageRepo.findAllByOrderBySentAtDesc();
        for (PrivateMessage privateMessage : message)
            privateMessages.add(new PrivateMessageDto(privateMessage));

        return privateMessages;
    }

    @Override
    @Transactional(readOnly = true)
    public PrivateMessageDto getPrivateMessageByID(Integer ID) {
        PrivateMessage privateMessage = privateMessageRepo.findOne(ID);
        return (privateMessage == null ? null : new PrivateMessageDto(privateMessage));
    }

    @Override
    @Transactional
    public OrderDto sendPrivateMessage(PrivateMessageDto privateMessageDto, String coupon, PaymentMethodType paymentMethod) {

        Integer fromUserID = privateMessageDto.getFromUser().getID();
        Integer toUserID = privateMessageDto.getToUser().getID();
        if (fromUserID == null || toUserID == null)
            return null;

        CloudStorageFile file = new CloudStorageFile();
        file.setName(privateMessageDto.getAudioFileCloud().getName());
        file.setUrl(privateMessageDto.getAudioFileCloud().getUrl());
        file.setBucket(privateMessageDto.getAudioFileCloud().getBucket());

        PrivateMessage newPrivateMessage = new PrivateMessage();
        newPrivateMessage.setFromUser(userRepo.findOne(fromUserID));
        newPrivateMessage.setToUser(userRepo.findOne(toUserID));
        newPrivateMessage.setAudioFileCloud(file);
        newPrivateMessage.setLanguage(languageRepo.findOne(privateMessageDto.getLanguage().getId()));
        newPrivateMessage.setSentAt(new Date());
        newPrivateMessage.setStatus(Status.PAID);

        privateMessageRepo.save(newPrivateMessage);
        if (newPrivateMessage.getFromUser() == null || newPrivateMessage.getToUser() == null)
            return null;

        return orderService.addOrder(newPrivateMessage, OrderDetailType.PRIVATE_MESSAGE, priceRepo.findByType(OrderDetailType.PRIVATE_MESSAGE).getPrice(), 1,
                fromUserID, coupon, paymentMethod);
    }

    @Override
    @Transactional
    public PrivateMessageDto sendPrivateMessageByCredit(PrivateMessageDto privateMessageDto) {
        Integer fromUserID = privateMessageDto.getFromUser().getID();
        Integer toUserID = privateMessageDto.getToUser().getID();
        if (fromUserID == null || toUserID == null)
            return null;
        User fromUser = userRepo.findOne(fromUserID);
        if(fromUser == null) {
            throw new ResourceNotFoundException("User (" + fromUserID+ ") is not found");
        }
        User toUser = userRepo.findOne(toUserID);
        if(toUser == null) {
            throw new ResourceNotFoundException("User (" + toUserID+ ") is not found");
        }
        //subtract the blance form real big profile
        UserProfile userProfile = fromUser.getUserProfile();

        UserCreditPK userCreditPK = new UserCreditPK(toUser, fromUser);
        UserCredits userCredits = userCreditRepo.findOne(userCreditPK);

        if(userCredits != null && userCredits.getBalance() >1){
            userCredits.setUpdated(new Date());
            userCredits.setBalance(userCredits.getBalance() - 1);
            userCreditRepo.save(userCredits);
        } else if(userProfile.getCreditBalance() > 1){
            userProfile.setCreditBalance(userProfile.getCreditBalance() - 1);
            userProfileRepo.save(userProfile);
        } else {
            throw new ResourceNotFoundException("You don't have enough credits");
        }

        CloudStorageFile file = new CloudStorageFile();
        file.setName(privateMessageDto.getAudioFileCloud().getName());
        file.setUrl(privateMessageDto.getAudioFileCloud().getUrl());
        file.setBucket(privateMessageDto.getAudioFileCloud().getBucket());

        PrivateMessage newPrivateMessage = new PrivateMessage();
        newPrivateMessage.setFromUser(userRepo.findOne(fromUserID));
        newPrivateMessage.setToUser(userRepo.findOne(toUserID));
        newPrivateMessage.setAudioFileCloud(file);
        newPrivateMessage.setLanguage(languageRepo.findOne(privateMessageDto.getLanguage().getId()));
        newPrivateMessage.setSentAt(new Date());
        newPrivateMessage.setStatus(Status.PAID);

        privateMessageRepo.save(newPrivateMessage);
        if (newPrivateMessage.getFromUser() == null || newPrivateMessage.getToUser() == null)
            return null;

        return new PrivateMessageDto(newPrivateMessage);
    }
    @Override
    @Transactional(readOnly = true)
    public Collection<PrivateMessageDto> getReceivedPrivateMessages(Integer fromUserID) {
        return findByUserID(fromUserID);
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<PrivateMessageDto> getSentPrivateMessages(Integer toUserID) {
        return findByUserID(toUserID);
    }

    private Collection<PrivateMessageDto> findByUserID(Integer userID) {
        Collection<PrivateMessageDto> privateMessages = new ArrayList<>();
        QPrivateMessage qPrivateMessage = QPrivateMessage.privateMessage;

        //Iterable<PrivateMessage> message = privateMessageRepo.findAllByToUser(userRepo.findOne(userID));

        Iterable<PrivateMessage> message = privateMessageRepo.findAll(
                qPrivateMessage.toUser.userId.eq(userID)
                        .and(qPrivateMessage.status.eq(Status.PAID).or(qPrivateMessage.status.eq(Status.FREE))));

        for (PrivateMessage privateMessage : message)
            privateMessages.add(new PrivateMessageDto(privateMessage));

        return privateMessages;
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<PrivateMessageDto> getPrivateMessagesByFromUserAndToUser(Integer fromUserID, Integer toUserID) {
        Collection<PrivateMessageDto> privateMessages = new ArrayList<>();
        QPrivateMessage qPrivateMessage = QPrivateMessage.privateMessage;

        /*Iterable<PrivateMessage> messages =
                privateMessageRepo.findAllByFromUserAndToUser(userRepo.findOne(fromUserID), userRepo.findOne(toUserID));*/

        Iterable<PrivateMessage> messages = privateMessageRepo.findAll(
                qPrivateMessage.fromUser.userId.eq(fromUserID).and(qPrivateMessage.toUser.userId.eq(toUserID))
                        .and(qPrivateMessage.status.eq(Status.PAID).or(qPrivateMessage.status.eq(Status.FREE))));

        for (PrivateMessage privateMessage : messages)
            privateMessages.add(new PrivateMessageDto(privateMessage));

        return privateMessages;
    }

    @Override
    @Transactional(readOnly = true)
    public void updatePrivateMessageStatus(Integer privateMessageID, Status status) {
        PrivateMessage privateMessage = privateMessageRepo.findOne(privateMessageID);
        if (privateMessage != null) {
            privateMessage.setStatus(status);
            privateMessageRepo.save(privateMessage);
        }
    }

    @Override
    public Integer countExchangedMessagesByUser(Integer userId) {
        User user = userRepo.findOne(userId);
        if (user != null) {
            return privateMessageRepo.countExchangedMessagesByFromUser(user) +
                    privateMessageRepo.countExchangedMessagesByToUser(user);
        }
        return 0;
    }

    @Override
    public CalculatePriceDto calculatePrice(String couponCode) {
        Double price = priceRepo.findByType(OrderDetailType.PRIVATE_MESSAGE).getPrice();
        Double totalPrice = price;

        CouponDto coupon = orderService.getCoupon(couponCode, CouponCategory.MESSAGE);
        if (coupon != null) {
            totalPrice = totalPrice * coupon.getDiscountPercent() * 0.01;
        }

        return new CalculatePriceDto(price, coupon, totalPrice);
    }

    @Override
    public Page<PrivateMessageDto> getSentPrivateMessages(Integer userId, PageRequest pageRequest) {

        QPrivateMessage qPrivateMessage = QPrivateMessage.privateMessage;
        Page<PrivateMessage> message = privateMessageRepo.findAll(
                qPrivateMessage.fromUser.userId.eq(userId)
                        .and(qPrivateMessage.status.eq(Status.PAID).or(qPrivateMessage.status.eq(Status.FREE))),
                pageRequest);
        return message.map(PrivateMessageDto::new);
    }

    @Override
    public Page<PrivateMessageDto> getReceivedPrivateMessages(Integer userId, PageRequest pageRequest) {

        QPrivateMessage qPrivateMessage = QPrivateMessage.privateMessage;
        Page<PrivateMessage> message = privateMessageRepo.findAll(
                qPrivateMessage.toUser.userId.eq(userId)
                        .and(qPrivateMessage.status.eq(Status.PAID).or(qPrivateMessage.status.eq(Status.FREE))),
                pageRequest);
        return message.map(PrivateMessageDto::new);
    }

    @Override
    public Integer countSent(Integer userId) {
        User user = userRepo.findOne(userId);
        if (user != null) {
            return privateMessageRepo.countMessagesByFromUser(user);
        }
        return 0;
    }

    @Override
    public Integer countRecived(Integer userId) {
        User user = userRepo.findOne(userId);
        if (user != null) {
            return privateMessageRepo.countMessagesByToUser(user);
        }
        return 0;
    }

    public int size(Iterable<?> it) {
        if (it instanceof Collection)
            return ((Collection<?>) it).size();

        int i = 0;
        for (Object obj : it) i++;
        return i;
    }
}
