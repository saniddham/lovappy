package com.realroofers.lovappy.service.news.impl;

import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.news.NewsAdvancedOptionService;
import com.realroofers.lovappy.service.news.NewsCategoryService;
import com.realroofers.lovappy.service.news.NewsStoryService;
import com.realroofers.lovappy.service.news.dto.NewsDto;
import com.realroofers.lovappy.service.news.dto.NewsStoryAdminDto;
import com.realroofers.lovappy.service.news.dto.NewsStoryDto;
import com.realroofers.lovappy.service.news.model.NewsAdvancedOption;
import com.realroofers.lovappy.service.news.model.NewsCategory;
import com.realroofers.lovappy.service.news.model.NewsStory;
import com.realroofers.lovappy.service.news.repo.NewsCategoryRepo;
import com.realroofers.lovappy.service.news.repo.NewsStoryRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class NewsStoryServiceImpl implements NewsStoryService {
    private static Logger LOG = LoggerFactory.getLogger(NewsStoryServiceImpl.class);

    @Autowired
    private NewsStoryRepo newsStoryRepo;

    @Autowired
    private NewsCategoryRepo newsCategoryRepo;

    @Autowired
    private NewsCategoryService newsCategoryService;

    @Autowired
    private NewsStoryService newsStoryService;

    @Autowired
    private NewsAdvancedOptionService newsAdvancedOptionService;

    @Autowired
    private CloudStorageService cloudStorageService;


    @Override
    public NewsStory saveNewsStory(NewsStoryDto newsStoryDto) throws IOException {
        //story
        NewsStory newsStory = new NewsStory();
        newsStory.setTitle(newsStoryDto.getNewsStoryTitle());
        newsStory.setAudioVideoUrl(newsStoryDto.getNewsStoryAudioVideoUrl());
        newsStory.setContain(newsStoryDto.getNewsStoryContain());
        newsStory.setPosted(null);
        newsStory.setSubmitDate(new Date());
        newsStory.setAudioVideoUrl(newsStoryDto.getNewsStoryAudioVideoUrl());
        newsStory.setNewsCategory(newsCategoryService.getNewsCategoryByCategoryId(newsStoryDto.getCategoryId()));
        newsStory.setSource(newsStoryDto.getSource());
        newsStory.setAudioVideoUrl(newsStoryDto.getVideoUrl());
        newsStory.setImageDescription(newsStoryDto.getImageDescription());
        newsStory.setImageKeywords(newsStoryDto.getImagekeyword());
        if (newsStoryDto.getNewsPicture() != null && newsStoryDto.getNewsPicture().getId() != null) {
            newsStory.setNewsImage(cloudStorageService.findById(newsStoryDto.getNewsPicture().getId()));
        }

        if (newsStoryDto.getNewsAuthorPicture() != null && newsStoryDto.getNewsAuthorPicture().getId() != null) {
            newsStory.setNewsUserIdentification(cloudStorageService.findById(newsStoryDto.getNewsAuthorPicture().getId()));
        }

        newsStory = newsStoryService.saveNewsStory(newsStory);

        try {
            //newsAdvanced Option
            NewsAdvancedOption newsAdvancedOption = new NewsAdvancedOption();
            newsAdvancedOption.setNewsStory(newsStory);
            newsAdvancedOption.setSeoTitle(newsStoryDto.getAdvancedOptionSeoTitle());
            newsAdvancedOption.setMetaDescription(newsStoryDto.getAdvancedOptionMetaDescription());
            newsAdvancedOption.setKeywordPhrases(newsStoryDto.getAdvancedOptionKeywordPhrases());
            newsAdvancedOptionService.save(newsAdvancedOption);

        } catch (ArrayIndexOutOfBoundsException ex) {
            ex.printStackTrace();
        }

        return newsStory;
    }

    @Transactional
    @Override
    public NewsStory getNewsStoryById(Long Id) {
        return newsStoryRepo.findOne(Id);
    }

    @Transactional
    @Override
    public NewsStory getNewsStoryByTitle(String title) {
        return newsStoryRepo.getNewsStoryByTitle(title);
    }

    @Transactional
    @Override
    public NewsStory saveNewsStory(NewsStory newsStory) {
        return newsStoryRepo.save(newsStory);
    }


    @Transactional
    @Override
    public List<NewsDto> getAllNewsStories() {
        List<NewsDto> newsDtoList = new ArrayList<>();
        newsStoryRepo.findAll().stream().forEach(h ->
                newsDtoList.add(new NewsDto(h))
        );

        return newsDtoList;
    }

    @Transactional
    @Override
    public List<NewsStory> getNewsStoriesByTitleAndContain(String search) {
        return newsStoryRepo.findNewsStoriesByTitleIsLikeAndContainIsLike(search);
    }

    @Transactional
    @Override
    public Long getLastId() {
        NewsStory news = newsStoryRepo.findTopByOrderByIdDesc();
        return Long.valueOf(news.getId());
    }

    @Transactional
    @Override
    public List<NewsStoryAdminDto> getAllNewsStoryAdminDto() {
        List<NewsStoryAdminDto> newsStoryAdminDtos = new ArrayList<>();
        List<NewsDto> newsStories = new ArrayList<>();
        newsStories = getAllNewsStories();
        System.out.println("Inside");
//        System.out.println("Story L :"+newsStories.get(1).getTitle());
        for (int i = 0; i < newsStories.size(); i++) {
            System.out.println("DATA LIST : NEWS STORY :" + newsStories.get(i));
            NewsDto newsStory = new NewsDto();
            NewsStoryAdminDto newsStoryAdminDto = new NewsStoryAdminDto();
            newsStory = newsStories.get(i);
            newsStoryAdminDto.setNewsStoryId(newsStory.getId());
            newsStoryAdminDto.setNewsStoryTitle(newsStory.getTitle());
            newsStoryAdminDto.setNewsStoryContain(newsStory.getContain());
            newsStoryAdminDto.setSource(newsStory.getSource());
            newsStoryAdminDto.setNewsStorySubmitDate(newsStory.getSubmitDate());
            newsStoryAdminDto.setNewsStoryPosted(newsStory.getPosted());
            newsStoryAdminDto.setImageDescription(newsStory.getImageDescription());
            newsStoryAdminDto.setImagekeyword(newsStory.getImagekeyword());
            try {
                newsStoryAdminDto.setAdvancedOptionSeoTitle(newsAdvancedOptionService.getOneBystoryId(newsStory.getId()).getSeoTitle());
                newsStoryAdminDto.setAdvancedOptionMetaDescription(newsAdvancedOptionService.getOneBystoryId(newsStory.getId()).getMetaDescription());
                newsStoryAdminDto.setAdvancedOptionKeywordPhrases(newsAdvancedOptionService.getOneBystoryId(newsStory.getId()).getKeywordPhrases());

                newsStoryAdminDto.setCategoryId(newsCategoryService.getOne(newsStory.getNewsCategory().getCategoryId()).getCategoryId());
                newsStoryAdminDto.setCategoryName(newsCategoryService.getOne(newsStory.getNewsCategory().getCategoryId()).getCategoryName());

            } catch (NullPointerException e) {
                // e.printStackTrace();
                continue;
            }

            newsStoryAdminDtos.add(newsStoryAdminDto);

        }

        return newsStoryAdminDtos;
    }

    @Transactional
    public NewsStoryAdminDto getNewsStoryAdminDtoById(Long id) {
        NewsStoryAdminDto newsStoryAdminDto = new NewsStoryAdminDto();
        NewsStory newsStory = getNewsStoryById(id);
        System.out.println("Inside");
//        System.out.println("Story L :"+newsStories.get(1).getTitle());

        //  System.out.println("DATA LIST : NEWS STORY :" + newsStories.get(i));
        //NewsStory newsStory = new NewsStory();
        // NewsStoryAdminDto newsStoryAdminDto = new NewsStoryAdminDto();
        newsStoryAdminDto.setNewsStoryId(newsStory.getId());
        newsStoryAdminDto.setNewsStoryTitle(newsStory.getTitle());
        newsStoryAdminDto.setNewsStoryContain(newsStory.getContain());
        newsStoryAdminDto.setSource(newsStory.getSource());
        newsStoryAdminDto.setNewsStorySubmitDate(newsStory.getSubmitDate());
        newsStoryAdminDto.setNewsStoryPosted(newsStory.getPosted());
        newsStoryAdminDto.setImageDescription(newsStory.getImageDescription());
        newsStoryAdminDto.setImagekeyword(newsStory.getImageKeywords());
        //   System.out.println("SEO Title : " + newsAdvancedOptionService.getOneBystoryId(newsStory.getId()).getSeoTitle());

        newsStoryAdminDto.setAdvancedOptionSeoTitle(newsAdvancedOptionService.getOneBystoryId(newsStory.getId()).getSeoTitle());
        newsStoryAdminDto.setAdvancedOptionMetaDescription(newsAdvancedOptionService.getOneBystoryId(newsStory.getId()).getMetaDescription());
        newsStoryAdminDto.setAdvancedOptionKeywordPhrases(newsAdvancedOptionService.getOneBystoryId(newsStory.getId()).getKeywordPhrases());

        newsStoryAdminDto.setCategoryId(newsCategoryService.getOne(newsStory.getNewsCategory().getCategoryId()).getCategoryId());
        newsStoryAdminDto.setCategoryName(newsCategoryService.getOne(newsStory.getNewsCategory().getCategoryId()).getCategoryName());
        if (newsStory.getNewsImage() != null)
            newsStoryAdminDto.setImage(cloudStorageService.findDtoById(newsStory.getNewsImage().getId()));
        if (newsStory.getNewsUserIdentification() != null)
            newsStoryAdminDto.setAuthorImage(cloudStorageService.findDtoById(newsStory.getNewsUserIdentification().getId()));
        if (newsStory.getNewsVideo() != null)
            newsStoryAdminDto.setVideo(cloudStorageService.findDtoById(newsStory.getNewsVideo().getId()));

        return newsStoryAdminDto;
    }

    @Transactional
    @Override
    public List<NewsStoryAdminDto> getAllNewsStoryAdminDtoByTitleAndContain(String search) {
        List<NewsStoryAdminDto> newsStoryAdminDtos = new ArrayList<>();
        List<NewsStory> newsStories = new ArrayList<>();
        newsStories = getNewsStoriesByTitleAndContain(search);
        System.out.println("Inside");
//        System.out.println("Story L :"+newsStories.get(1).getTitle());
        for (int i = 0; i < newsStories.size(); i++) {
            System.out.println("DATA LIST : NEWS STORY :" + newsStories.get(i));
            NewsStory newsStory = new NewsStory();
            NewsStoryAdminDto newsStoryAdminDto = new NewsStoryAdminDto();
            newsStory = newsStories.get(i);
            newsStoryAdminDto.setNewsStoryId(newsStory.getId());
            newsStoryAdminDto.setNewsStoryTitle(newsStory.getTitle());
            newsStoryAdminDto.setNewsStoryContain(newsStory.getContain());
            newsStoryAdminDto.setSource(newsStory.getSource());
            newsStoryAdminDto.setNewsStorySubmitDate(newsStory.getSubmitDate());
            newsStoryAdminDto.setNewsStoryPosted(newsStory.getPosted());
            newsStoryAdminDto.setImageDescription(newsStory.getImageDescription());
            newsStoryAdminDto.setImagekeyword(newsStory.getImageKeywords());
            try {
                newsStoryAdminDto.setAdvancedOptionSeoTitle(newsAdvancedOptionService.getOneBystoryId(newsStory.getId()).getSeoTitle());
                newsStoryAdminDto.setAdvancedOptionMetaDescription(newsAdvancedOptionService.getOneBystoryId(newsStory.getId()).getMetaDescription());
                newsStoryAdminDto.setAdvancedOptionKeywordPhrases(newsAdvancedOptionService.getOneBystoryId(newsStory.getId()).getKeywordPhrases());

                newsStoryAdminDto.setCategoryId(newsCategoryService.getOne(newsStory.getNewsCategory().getCategoryId()).getCategoryId());
                newsStoryAdminDto.setCategoryName(newsCategoryService.getOne(newsStory.getNewsCategory().getCategoryId()).getCategoryName());

            } catch (NullPointerException e) {
                continue;
            }

            newsStoryAdminDtos.add(newsStoryAdminDto);

        }

        return newsStoryAdminDtos;
    }

    @Transactional
    public void newsApproval(Long id, boolean status) {

        NewsStory news = newsStoryRepo.getOne(id);

        news.setPosted(status);
    }

    @Transactional
    public List<NewsStory> getApprovedStories() {

        return newsStoryRepo.getApprovedNews();
    }


    @Transactional
    public List<NewsDto> getApprovedNewsStories() {
        List<NewsDto> newsStoryAdminDtos = new ArrayList<>();
        getApprovedStories().stream().forEach(h -> newsStoryAdminDtos.add(new NewsDto(h)));

        return newsStoryAdminDtos;
    }

    @Transactional
    public List<NewsStory> getLatestStories() {

        return newsStoryRepo.getLatestNews();
    }


    @Transactional
    public List<NewsDto> getLatesNewsStories() {
        List<NewsDto> newsStoryAdminDtos = new ArrayList<>();
        getLatestStories().stream().forEach(h -> {
            if (h.getNewsVideo() != null && h.getNewsVideo().getUrl() != null && !h.getNewsVideo().getUrl().equals(""))
                newsStoryAdminDtos.add(new NewsDto(h));
        });

        return newsStoryAdminDtos;
    }

    @Override
    public List<NewsDto> getNewsByCategoryId(Long catId) {
        List<NewsDto> newsStoryAdminDtos = new ArrayList<>();

        NewsCategory newsCategory = newsCategoryRepo.getOne(catId);
        try {
            List<NewsStory> newsStoryList = newsStoryRepo.getNewsStoryByNewsCategoryEquals(newsCategory);
            newsStoryList.stream().forEach(h -> {
                if (h.getNewsVideo() != null && h.getNewsVideo().getUrl() != null && !h.getNewsVideo().getUrl().equals(""))
                    newsStoryAdminDtos.add(new NewsDto(h));
            });
        } catch (Exception e){
            System.out.println(e);
            newsStoryAdminDtos.add(new NewsDto());
        }
        return newsStoryAdminDtos;
    }
}