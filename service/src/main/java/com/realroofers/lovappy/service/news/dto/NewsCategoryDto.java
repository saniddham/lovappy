package com.realroofers.lovappy.service.news.dto;

import com.realroofers.lovappy.service.news.model.NewsCategory;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by Shehan Wijesinghe on 28/08/2018.
 */
@Data
public class NewsCategoryDto implements Serializable {

    private Long categoryId;
    private String categoryName;
    private String categoryDescription;
    private int status;

    public NewsCategoryDto() {
    }

    public NewsCategoryDto(NewsCategory newsCategory) {
        if (newsCategory.getCategoryId() != null)
            this.categoryId = newsCategory.getCategoryId();
        this.categoryName = newsCategory.getCategoryName();
        this.categoryDescription = newsCategory.getCategoryDescription();
        this.status = newsCategory.getStatus();
    }
}
