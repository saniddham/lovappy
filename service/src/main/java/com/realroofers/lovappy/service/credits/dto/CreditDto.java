package com.realroofers.lovappy.service.credits.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 8/10/2018.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreditDto implements Serializable{
    private Integer balance;
    private Integer recievedBalance;
    private Double price;

    public CreditDto() {
        this.balance = 0;
        this.recievedBalance = 0;
    }

    public CreditDto(Integer balance) {
        this.balance = balance;
    }

    public CreditDto(Integer balance, Integer recievedBalance, Double price) {
        this.balance = balance ==null? 0 : balance;
        this.recievedBalance = recievedBalance ==null? 0 : recievedBalance;
        this.price = price == null? 1.99 : price;
    }
}
