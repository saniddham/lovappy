package com.realroofers.lovappy.service.mail.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Daoud Shaheen on 9/7/2017.
 */
@Entity
@Table(name = "lovappy_emails")
@DynamicUpdate
@EqualsAndHashCode
public class LovappyEmail extends BaseEntity<Integer> {

    private String email;

    public LovappyEmail(){

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
