package com.realroofers.lovappy.service.privatemessage.model;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.privatemessage.support.Status;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Eias Altawil on 5/7/17
 */

@Entity
@Table(name="private_message")
@EqualsAndHashCode
public class PrivateMessage extends BaseEntity<Integer>{

    @ManyToOne(optional = false)
    @JoinColumn(name = "from_user")
    private User fromUser;

    @ManyToOne(optional = false)
    @JoinColumn(name = "to_user")
    private User toUser;

    @ManyToOne
    @JoinColumn(name = "language")
    private Language language;

    private String fileName;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile audioFileCloud;

    private Date sentAt;

    @Enumerated(EnumType.STRING)
    private Status status;

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setAudioFileCloud(CloudStorageFile audioFileCloud) {
        this.audioFileCloud = audioFileCloud;
    }

    public CloudStorageFile getAudioFileCloud() {
        return audioFileCloud;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Date getSentAt() {
        return sentAt;
    }

    public void setSentAt(Date sentAt) {
        this.sentAt = sentAt;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
