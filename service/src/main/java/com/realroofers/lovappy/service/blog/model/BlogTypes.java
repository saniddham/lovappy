package com.realroofers.lovappy.service.blog.model;

/**
 * Created by Daoud Shaheen on 8/8/2017.
 */
public enum BlogTypes {

    REGULAR, GALLERY, LOVAPPY, FEATURED
}
