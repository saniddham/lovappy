package com.realroofers.lovappy.service.cloud.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.cloud.model.ImageThumbnail;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;

@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode
public class CloudStorageFileDto implements Serializable {

    private Long id;
    private String name;
    private String url;
    private String bucket;
    private Integer duration;
    @JsonIgnore
    private List<ImageThumbnailDto> thumbnails;

    private Boolean approved = false;
    private Boolean rejected = false;
    @JsonIgnore
    private Date approvedDate;
    @JsonIgnore
    private Date rejectedDate;


    public CloudStorageFileDto(CloudStorageFile cloudStorageFile) {
        if (cloudStorageFile != null) {
            this.id = cloudStorageFile.getId();
            this.name = cloudStorageFile.getName();
            this.url = cloudStorageFile.getUrl();
            this.bucket = cloudStorageFile.getBucket();

            this.approved = cloudStorageFile.getApproved() == null ? false : cloudStorageFile.getApproved();
            this.rejected = cloudStorageFile.getRejected() == null ? false : cloudStorageFile.getRejected();
            this.approvedDate = cloudStorageFile.getApprovedDate();
            this.rejectedDate = cloudStorageFile.getRejectedDate();


            if (cloudStorageFile.getThumbnails() != null) {
                this.thumbnails = new ArrayList<>();
                for (ImageThumbnail thumbnail : cloudStorageFile.getThumbnails())
                    this.thumbnails.add(new ImageThumbnailDto(thumbnail));
            }
        }
    }

    public CloudStorageFileDto(CloudStorageFile cloudStorageFile, Boolean withoutFile) {
        if (cloudStorageFile != null) {
            this.id = cloudStorageFile.getId();
            this.name = cloudStorageFile.getName();
            this.url = cloudStorageFile.getUrl();
            this.bucket = cloudStorageFile.getBucket();

            this.approved = cloudStorageFile.getApproved() == null ? false : cloudStorageFile.getApproved();
            this.rejected = cloudStorageFile.getRejected() == null ? false : cloudStorageFile.getRejected();
            this.approvedDate = cloudStorageFile.getApprovedDate();
            this.rejectedDate = cloudStorageFile.getRejectedDate();

        }
    }

    public CloudStorageFileDto(String name, String url, String bucket) {
        this.name = name;
        this.url = url;
        this.bucket = bucket;
    }

    public CloudStorageFileDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public List<ImageThumbnailDto> getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(List<ImageThumbnailDto> thumbnails) {
        this.thumbnails = thumbnails;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public Boolean getRejected() {
        return rejected;
    }

    public void setRejected(Boolean rejected) {
        this.rejected = rejected;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public Date getRejectedDate() {
        return rejectedDate;
    }

    public void setRejectedDate(Date rejectedDate) {
        this.rejectedDate = rejectedDate;
    }
}

