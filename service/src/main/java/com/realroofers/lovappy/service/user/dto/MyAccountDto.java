package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.model.UserPreference;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.validation.constraints.NotNull;

/**
 * Created by Eias Altawil on 5/6/17
 */

@Qualifier("myAccount")
@EqualsAndHashCode
public class MyAccountDto {
    @NotEmpty(message = "You should provide your email address.")
    @NotNull
    @Email(message = "This is not a valid email address.")
    private String email;

//    @NotNull
//    @Size(min = 6, message = "Min password length is 6.")
//    @NotEmpty(message = "Password cannot be empty.")
    private String password;

    private Boolean emailNotifications = false;
    private Boolean newLikesNotifications = false;
    private Boolean newMatchesNotifications = false;
    private Boolean newMessagesNotifications = false;
    private Boolean newSongsNotifications = false;
    private Boolean newGiftsNotifications = false;
    private Boolean newUnlockRequestNotifications = false;
    private Boolean songApproval = false;
    private Boolean songDenials = false;
    private Boolean newSongPurchase = false;
    private Boolean activated = false;
    private Boolean emailVerified = false;
    public MyAccountDto(){}
    
    public MyAccountDto(User user) {
        this.email = user.getEmail();
        this.activated = user.getActivated();
        //this.password = user.getPassword();
        this.emailNotifications = user.getEmailNotifications()  == null? true : user.getEmailNotifications();
        UserPreference userPreference = user.getUserPreference();
        if(userPreference!=null) {
            this.newLikesNotifications = userPreference.getNewLikesNotifications() == null ? false : userPreference.getNewLikesNotifications();
            this.newMatchesNotifications = userPreference.getNewMatchesNotifications() == null ? false : userPreference.getNewMatchesNotifications();
            this.newMessagesNotifications = userPreference.getNewMessagesNotifications() == null ? false : userPreference.getNewMessagesNotifications();
            this.newSongsNotifications = userPreference.getNewSongsNotifications() == null ? false : userPreference.getNewSongsNotifications();
            this.newGiftsNotifications = userPreference.getNewGiftsNotifications() == null ? false : userPreference.getNewGiftsNotifications();
            this.songApproval = userPreference.getSongApproval() == null ? false : userPreference.getSongApproval();
            this.newSongPurchase = userPreference.getNewSongPurchase() == null ? false : userPreference.getNewSongPurchase();
            this.songDenials = userPreference.getSongDenials() == null ? false : userPreference.getSongDenials();
            this.newUnlockRequestNotifications = userPreference.getNewUnlockRequestNotifications() == null ? false : userPreference.getNewUnlockRequestNotifications();
        }

        emailVerified = user.getEmailVerified() == null ? false : user.getEmailVerified();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEmailNotifications() {
        return emailNotifications;
    }

    public void setEmailNotifications(Boolean emailNotifications) {
        this.emailNotifications = emailNotifications;
    }

    public boolean isNewLikesNotifications() {
        return newLikesNotifications;
    }

    public void setNewLikesNotifications(boolean newLikesNotifications) {
        this.newLikesNotifications = newLikesNotifications;
    }

    public boolean isNewMatchesNotifications() {
        return newMatchesNotifications;
    }

    public void setNewMatchesNotifications(boolean newMatchesNotifications) {
        this.newMatchesNotifications = newMatchesNotifications;
    }

    public boolean isNewMessagesNotifications() {
        return newMessagesNotifications;
    }

    public void setNewMessagesNotifications(boolean newMessagesNotifications) {
        this.newMessagesNotifications = newMessagesNotifications;
    }

    public boolean isNewSongsNotifications() {
        return newSongsNotifications;
    }

    public void setNewSongsNotifications(boolean newSongsNotifications) {
        this.newSongsNotifications = newSongsNotifications;
    }

    public boolean isNewGiftsNotifications() {
        return newGiftsNotifications;
    }

    public void setNewGiftsNotifications(boolean newGiftsNotifications) {
        this.newGiftsNotifications = newGiftsNotifications;
    }

    public boolean isNewUnlockRequestNotifications() {
        return newUnlockRequestNotifications;
    }

    public void setNewUnlockRequestNotifications(boolean newUnlockRequestNotifications) {
        this.newUnlockRequestNotifications = newUnlockRequestNotifications;
    }

    public Boolean getActivated() {
        return activated;
    }

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public Boolean getSongApproval() {
        return songApproval;
    }

    public void setSongApproval(Boolean songApproval) {
        this.songApproval = songApproval;
    }

    public Boolean getSongDenials() {
        return songDenials;
    }

    public void setSongDenials(Boolean songDenials) {
        this.songDenials = songDenials;
    }

    public Boolean getNewSongPurchase() {
        return newSongPurchase;
    }

    public void setNewSongPurchase(Boolean newSongPurchase) {
        this.newSongPurchase = newSongPurchase;
    }
}
