package com.realroofers.lovappy.service.product.repo;

import com.realroofers.lovappy.service.product.model.BrandCommission;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BrandCommissionRepo extends JpaRepository<BrandCommission, Integer> {

    List<BrandCommission> findAllByActiveIsTrue();

    BrandCommission findTopByActiveIsTrueOrderByAffectiveDateAsc();
}
