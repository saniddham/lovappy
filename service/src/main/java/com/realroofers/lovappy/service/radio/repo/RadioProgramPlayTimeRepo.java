package com.realroofers.lovappy.service.radio.repo;

import com.realroofers.lovappy.service.radio.model.RadioProgramPlayTime;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Manoj on 17/12/2017.
 */
public interface RadioProgramPlayTimeRepo extends JpaRepository<RadioProgramPlayTime, Integer> {

    List<RadioProgramPlayTime> findAllByActiveIsTrue();
}
