package com.realroofers.lovappy.service.datingPlaces.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlace;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceReview;
import com.realroofers.lovappy.service.datingPlaces.model.PersonType;
import com.realroofers.lovappy.service.datingPlaces.model.PlacesAttachment;
import com.realroofers.lovappy.service.datingPlaces.support.DatingAgeRange;
import com.realroofers.lovappy.service.datingPlaces.support.NeighborHood;
import com.realroofers.lovappy.service.datingPlaces.support.PriceRange;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static com.realroofers.lovappy.service.datingPlaces.support.DatingPlaceUtil.mapToFoodTypeDto;
import static com.realroofers.lovappy.service.datingPlaces.support.DatingPlaceUtil.mapToPlaceAtmosphereDto;

/**
 * Created by Darrel Rayen on 10/17/17.
 */
@Data
@EqualsAndHashCode
public class DatingPlaceDto implements Serializable {

    private String datingPlacesId;
    private String placeName;
    private CloudStorageFileDto coverPicture;
    private String country;
    private String city;
    private String addressLine;
    private String addressLineTwo;
    private String zipCode;
    private String createEmail;
    private Gender gender;
    private DatingAgeRange ageRange;
    private PriceRange priceRange;
    private NeighborHood neighborHood;
    private Date createdDate;
    private Date deletedDate;
    private String state;
    private String password;
    private String userType;
    private ApprovalStatus placeApprovalStatus;
    private Double latitude;
    private Double longitude;
    private List<DatingPlaceReviewDto> datingPlaceReviewDtos = new ArrayList<>();
    private List<PlacesAttachmentDto> placesAttachmentDtos = new ArrayList<>();
    private String coverPictureUrl;// Google picture Url
    private List<String> placesUrlList; // Google places Url list
    private String isOpened; // Google specific attributes
    private String iconUrl; //Google places Attributes
    private String FoodTypes;
    private String PlaceAtmospheres;
    private String personTypes;
    private String placeDescription;
    private User owner;

    private Double overAllRating;
    private Double securityAverage;
    private Double parkingAverage;
    private Double transportAverage;
    private Double googleAverage;
    private Double averageRating;

    private Integer maleCount;
    private Integer femaleCount;
    private Integer bothCount;

    private Boolean isFeatured;
    private Boolean isMapFeatured;
    private Boolean isLovStampFeatured;
    private Boolean approved;
    private Boolean isPetsAllowed;
    private Boolean isGoodForMarriedCouple;
    private Boolean isClaimed;
    private Collection<FoodTypeDto> foodTypesCollection;
    private Collection<PlaceAtmosphereDto> atmosphereDtoCollection;
    private Collection<PersonType> personTypeCollection;

    public DatingPlaceDto() {
    }

    public DatingPlaceDto(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public DatingPlaceDto(DatingPlace datingPlace) {
        this.datingPlacesId = String.valueOf(datingPlace.getDatingPlacesId());
        this.placeName = datingPlace.getPlaceName();
        this.coverPicture = getCoverImageDto(datingPlace.getCoverPicture());
        this.country = datingPlace.getCountry();
        this.city = datingPlace.getCity();
        this.state = datingPlace.getState();
        this.addressLine = datingPlace.getAddressLine();
        this.addressLineTwo = datingPlace.getAddressLineTwo();
        this.zipCode = datingPlace.getZipCode();
        this.createEmail = datingPlace.getCreateEmail();
        this.isFeatured = datingPlace.getFeatured() == null ? false : datingPlace.getFeatured();
        this.gender = datingPlace.getGender();
        this.ageRange = datingPlace.getAgeRange();
        this.priceRange = datingPlace.getPriceRange();
        this.createdDate = datingPlace.getCreatedDate();
        this.deletedDate = datingPlace.getDeletedDate();
        this.placeApprovalStatus = datingPlace.getPlaceApprovalStatus();
        this.approved = datingPlace.getApproved() == null ? false : datingPlace.getApproved();
        this.datingPlaceReviewDtos = getPlacesReviewList(datingPlace.getDatingPlaceReviews());
        this.placesAttachmentDtos = getPlacesImageList(datingPlace.getPlacesAttachments());
        this.latitude = datingPlace.getLatitude();
        this.longitude = datingPlace.getLongitude();
        this.overAllRating = datingPlace.getAverageRating();
        this.averageRating = datingPlace.getAverageOverAllRatingByUser();
        this.isMapFeatured = datingPlace.getMapFeatured() == null ? false : datingPlace.getMapFeatured();
        this.isLovStampFeatured = datingPlace.getLoveStampFeatured() == null ? false : datingPlace.getLoveStampFeatured();
        this.placeDescription = datingPlace.getPlaceDescription();
        this.securityAverage = datingPlace.getSecurityAverage();
        this.parkingAverage = datingPlace.getParkingAverage();
        this.transportAverage = datingPlace.getTransportAverage();
        this.googleAverage = datingPlace.getGoogleAverage();
        this.maleCount = datingPlace.getSuitableForMaleCount();
        this.femaleCount = datingPlace.getSuitableForFemale();
        this.bothCount = datingPlace.getSuitableForBoth();
        this.neighborHood = datingPlace.getNeighborHood();
        this.isGoodForMarriedCouple = datingPlace.getGoodForMarriedCouple();
        this.isPetsAllowed = datingPlace.getPetsAllowed();
        this.foodTypesCollection = mapToFoodTypeDto(datingPlace.getFoodTypes());
        this.atmosphereDtoCollection = mapToPlaceAtmosphereDto(datingPlace.getPlaceAtmospheres());
        this.isClaimed = datingPlace.getClaimed() == null ? false : datingPlace.getClaimed();
        this.owner = datingPlace.getOwner();
        //this.personTypeCollection = datingPlace.getPersonTypes();
    }

    public DatingPlaceDto(Object[] objects){
        DatingPlace datingPlace = (DatingPlace) objects[0];
        this.datingPlacesId = String.valueOf(datingPlace.getDatingPlacesId());
        this.placeName = datingPlace.getPlaceName();
        this.coverPicture = getCoverImageDto(datingPlace.getCoverPicture());
        this.country = datingPlace.getCountry();
        this.city = datingPlace.getCity();
        this.state = datingPlace.getState();
        this.addressLine = datingPlace.getAddressLine();
        this.addressLineTwo = datingPlace.getAddressLineTwo();
        this.zipCode = datingPlace.getZipCode();
        this.createEmail = datingPlace.getCreateEmail();
        this.isFeatured = datingPlace.getFeatured() == null ? false : datingPlace.getFeatured();
        this.gender = datingPlace.getGender();
        this.ageRange = datingPlace.getAgeRange();
        this.priceRange = datingPlace.getPriceRange();
        this.createdDate = datingPlace.getCreatedDate();
        this.deletedDate = datingPlace.getDeletedDate();
        this.placeApprovalStatus = datingPlace.getPlaceApprovalStatus();
        this.approved = datingPlace.getApproved() == null ? false : datingPlace.getApproved();
        this.datingPlaceReviewDtos = getPlacesReviewList(datingPlace.getDatingPlaceReviews());
        this.placesAttachmentDtos = getPlacesImageList(datingPlace.getPlacesAttachments());
        this.latitude = datingPlace.getLatitude();
        this.longitude = datingPlace.getLongitude();
        this.overAllRating = datingPlace.getAverageRating();
        this.averageRating = datingPlace.getAverageOverAllRatingByUser();
        this.isMapFeatured = datingPlace.getMapFeatured() == null ? false : datingPlace.getMapFeatured();
        this.isLovStampFeatured = datingPlace.getLoveStampFeatured() == null ? false : datingPlace.getLoveStampFeatured();
        this.placeDescription = datingPlace.getPlaceDescription();
        this.securityAverage = datingPlace.getSecurityAverage();
        this.parkingAverage = datingPlace.getParkingAverage();
        this.transportAverage = datingPlace.getTransportAverage();
        this.googleAverage = datingPlace.getGoogleAverage();
        this.maleCount = datingPlace.getSuitableForMaleCount();
        this.femaleCount = datingPlace.getSuitableForFemale();
        this.bothCount = datingPlace.getSuitableForBoth();
        this.neighborHood = datingPlace.getNeighborHood();
        this.isGoodForMarriedCouple = datingPlace.getGoodForMarriedCouple();
        this.isPetsAllowed = datingPlace.getPetsAllowed();
        this.foodTypesCollection = mapToFoodTypeDto(datingPlace.getFoodTypes());
        this.atmosphereDtoCollection = mapToPlaceAtmosphereDto(datingPlace.getPlaceAtmospheres());
        this.isClaimed = datingPlace.getClaimed() == null ? false : datingPlace.getClaimed();
        this.owner = datingPlace.getOwner();
    }

    private List<PlacesAttachmentDto> getPlacesImageList(List<PlacesAttachment> placesAttachmentList) {
        List<PlacesAttachmentDto> placesAttachmentDtoList = new ArrayList<>();
        placesAttachmentList.forEach(eventLocationPicture -> {
            placesAttachmentDtoList.add(new PlacesAttachmentDto(eventLocationPicture));
        });
        return placesAttachmentDtoList;
    }

    private List<DatingPlaceReviewDto> getPlacesReviewList(List<DatingPlaceReview> datingPlaceReviewList) {
        List<DatingPlaceReviewDto> datingPlaceReviewDtos = new ArrayList<>();
        datingPlaceReviewList.forEach(placeReview -> {
            datingPlaceReviewDtos.add(new DatingPlaceReviewDto(placeReview));
        });
        return datingPlaceReviewDtos;
    }

    private CloudStorageFileDto getCoverImageDto(CloudStorageFile cloudStorageFile) {

        return new CloudStorageFileDto(cloudStorageFile);
    }

    public DatingPlace getDatingPlace() {
        DatingPlace place = new DatingPlace();
        BeanUtils.copyProperties(this, place);
        if (StringUtils.isNumeric(this.datingPlacesId)) {
            place.setDatingPlacesId(Integer.valueOf(this.datingPlacesId));
        }
        return place;
    }
}
