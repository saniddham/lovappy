package com.realroofers.lovappy.service.music.support;

/**
 * Created by Daoud Shaheen on 11/25/2017.
 */
public enum MusicProvider {

    LOVAPPY, JAMENDO
}
