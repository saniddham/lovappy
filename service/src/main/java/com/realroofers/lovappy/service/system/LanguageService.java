package com.realroofers.lovappy.service.system;

import com.realroofers.lovappy.service.system.dto.LanguageDto;

import java.util.Collection;

/**
 * Created by Eias Altawil on 6/3/2017
 */
public interface LanguageService {

    Collection<LanguageDto> getAllLanguages(Boolean enabled);
    Collection<LanguageDto> getAllLanguages();
    LanguageDto getLanguage(Integer id);
    LanguageDto getLanguageByCode(String code);
    LanguageDto addLanguage(LanguageDto languageDto);
    LanguageDto updateLanguage(LanguageDto languageDto);
    LanguageDto changeStatus(Integer languageID, Boolean enabled);
}
