package com.realroofers.lovappy.service.user.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Manoj on 11/02/2018.
 */
@Data
@Entity
@EqualsAndHashCode
@Table(name = "country")
public class Country extends BaseEntity<Integer> implements Serializable{

    @Column(name = "country_code")
    private String code;

    @Column(name = "country_name")
    private String name;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;

    @JsonIgnore
    @JoinColumn(name = "created_by")
    @ManyToOne
    private User createdBy;

    @JsonIgnore
    @JoinColumn(name = "updated_by")
    @ManyToOne
    private User updatedBy;
}
