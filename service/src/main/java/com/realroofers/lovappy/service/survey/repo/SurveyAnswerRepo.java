package com.realroofers.lovappy.service.survey.repo;

import com.realroofers.lovappy.service.survey.model.SurveyAnswer;
import org.springframework.data.repository.CrudRepository;

public interface SurveyAnswerRepo extends CrudRepository<SurveyAnswer, Integer> {
}
