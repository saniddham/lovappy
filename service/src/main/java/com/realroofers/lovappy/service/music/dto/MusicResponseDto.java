package com.realroofers.lovappy.service.music.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.realroofers.lovappy.service.music.model.MusicResponse;
import com.realroofers.lovappy.service.music.model.MusicResponseType;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EqualsAndHashCode
public class MusicResponseDto implements Serializable {

    private Integer id;
    private MusicExchangeMinDto musicExchange;
    private MusicResponseType type;
    private Date responseDate;
    private String response;
    private Boolean seen;

    public MusicResponseDto() {
    }

    public MusicResponseDto(MusicResponse musicExchangeResponse) {
        if (musicExchangeResponse.getId() != null) {
            this.id = musicExchangeResponse.getId();
        }
        this.musicExchange = new MusicExchangeMinDto(musicExchangeResponse.getMusicExchange());
        this.responseDate = musicExchangeResponse.getResponseDate();
        this.response = musicExchangeResponse.getResponse();
        this.type = musicExchangeResponse.getType();
        this.seen = musicExchangeResponse.getSeen();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MusicExchangeMinDto getMusicExchange() {
        return musicExchange;
    }

    public void setMusicExchange(MusicExchangeMinDto musicExchange) {
        this.musicExchange = musicExchange;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Boolean getSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }

    public MusicResponseType getType() {
        return type;
    }

    public void setType(MusicResponseType type) {
        this.type = type;
    }
}
