package com.realroofers.lovappy.service.datingPlaces.dto.mobile;

import com.realroofers.lovappy.service.datingPlaces.dto.DatingPlaceReviewDto;
import com.realroofers.lovappy.service.datingPlaces.dto.PersonTypeDto;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceReview;
import com.realroofers.lovappy.service.datingPlaces.support.DatingAgeRange;
import com.realroofers.lovappy.service.datingPlaces.support.PriceRange;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * Created by Darrel Rayen on 4/10/18.
 */
@Data
@EqualsAndHashCode
public class DatingPlaceReviewMobileDto implements Serializable {

    private Integer placesReviewId;
    private String placeId;
    private String reviewerComment;
    private String reviewerEmail;
    private String reviewerId;
    private Double parkingRating;
    private Double parkingAccessRating;
    private Double securityRating;
    private Double overAllRating;
    private Date reviewDate;
    private Double averageRating;
    private Gender suitableGender;
    private DatingAgeRange suitableAgeRange;
    private PriceRange suitablePriceRange;
    private Boolean isGoodForCouple;
    private String googlePlaceId;
    private String reviewZipCode;
    private String reviewerType;
    private String placeName;
    private Collection<PersonTypeDto> personTypes;

    public DatingPlaceReviewMobileDto(DatingPlaceReview datingPlaceReview) {
        this.placesReviewId = datingPlaceReview.getPlacesReviewId();
        if (datingPlaceReview.getPlaceId() != null) {
            this.placeId = datingPlaceReview.getPlaceId().getDatingPlacesId().toString();
        }
        this.reviewerComment = datingPlaceReview.getReviewerComment();
        this.reviewerEmail = datingPlaceReview.getReviewerEmail();
        this.parkingRating = datingPlaceReview.getParkingRating();
        this.parkingRating = datingPlaceReview.getParkingRating();
        this.securityRating = datingPlaceReview.getSecurityRating();
        this.parkingAccessRating = datingPlaceReview.getParkingAccessRating();
        this.reviewDate = datingPlaceReview.getReviewDate();
        this.averageRating = datingPlaceReview.getAverageRating();
        this.suitableAgeRange = datingPlaceReview.getSuitableAgeRange();
        this.suitableGender = datingPlaceReview.getSuitableGender();
        this.suitablePriceRange = datingPlaceReview.getSuitablePriceRange();
        this.isGoodForCouple = datingPlaceReview.getGoodForCouple() == null ? false : datingPlaceReview.getGoodForCouple();
        this.reviewerId = datingPlaceReview.getReviewerId();
        this.overAllRating = datingPlaceReview.getOverallRating();
        this.googlePlaceId = datingPlaceReview.getGooglePlaceId();
        this.reviewerType = datingPlaceReview.getReviewerType();
        this.placeName = datingPlaceReview.getPlaceName();
    }

    public DatingPlaceReviewMobileDto(DatingPlaceReviewDto datingPlaceReviewDto) {
        this.placesReviewId = datingPlaceReviewDto.getPlacesReviewId();
        this.reviewerComment = datingPlaceReviewDto.getReviewerComment();
        this.reviewerEmail = datingPlaceReviewDto.getReviewerEmail();
        this.parkingRating = datingPlaceReviewDto.getParkingRating();
        this.parkingRating = datingPlaceReviewDto.getParkingRating();
        this.securityRating = datingPlaceReviewDto.getSecurityRating();
        this.parkingAccessRating = datingPlaceReviewDto.getParkingAccessRating();
        this.reviewDate = datingPlaceReviewDto.getReviewDate();
        this.averageRating = datingPlaceReviewDto.getAverageRating();
        this.suitableAgeRange = datingPlaceReviewDto.getSuitableAgeRange();
        this.suitableGender = datingPlaceReviewDto.getSuitableGender();
        this.suitablePriceRange = datingPlaceReviewDto.getSuitablePriceRange();
        this.isGoodForCouple = datingPlaceReviewDto.getGoodForCouple() == null ? false : datingPlaceReviewDto.getGoodForCouple();
        this.reviewerId = datingPlaceReviewDto.getReviewerId();
        this.overAllRating = datingPlaceReviewDto.getOverAllRating();
        this.googlePlaceId = datingPlaceReviewDto.getGooglePlaceId();
        this.reviewerType = datingPlaceReviewDto.getReviewerType();
        this.placeName = datingPlaceReviewDto.getPlaceName();

    }

    public DatingPlaceReview convertToDatingPlaceReview() {
        DatingPlaceReview review = new DatingPlaceReview();
        BeanUtils.copyProperties(this, review);
        return review;
    }
}
