package com.realroofers.lovappy.service.radio.dto;

import com.realroofers.lovappy.service.ads.support.AdMediaType;
import com.realroofers.lovappy.service.core.TextAlign;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by Daoud Shaheen on 1/5/2018.
 */
@Data
@EqualsAndHashCode
public class RadioExtraInfoDto {
    private String mediaFileUrl;
    private String text;
    private String title;
    private String textColor;
    private TextAlign textAlign;
    private String url;
    private AdMediaType mediaType;

    public RadioExtraInfoDto(AdMediaType mediaType, String url, String mediaFileUrl, String text, String textColor, TextAlign textAlign) {
        this.mediaFileUrl = mediaFileUrl;
        this.text = text;
        this.textColor = textColor;
        this.url = url;
        this.mediaType = mediaType;
        this.textAlign = textAlign;
    }
}
