package com.realroofers.lovappy.service.questions.repo;

import com.realroofers.lovappy.service.questions.model.DailyQuestion;
import com.realroofers.lovappy.service.questions.model.UserResponse;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Darrel Rayen on 9/18/18.
 */
@Repository
public interface UserResponseRepo extends JpaRepository<UserResponse, Long> {

    Page<UserResponse> findAllByQuestionAndResponseApprovalStatus(DailyQuestion dailyQuestion, ApprovalStatus approvalStatus, Pageable pageable);

    Page<UserResponse> findAllByQuestion(DailyQuestion question, Pageable pageable);

    @Query("select ur from UserResponse ur where ur.user.userId=:userId and ur.responseApprovalStatus=:approvalStatus")
    List<UserResponse> findAllByUserAndApprovalStatus(@Param("userId") Integer userId, @Param("approvalStatus") ApprovalStatus approvalStatus);

    List<UserResponse> findAllByUserAndQuestion(User user, DailyQuestion question);

    @Query("select ur from UserResponse ur where ur.user.userId=:userId")
    List<UserResponse> findAllByUser(@Param("userId") Integer userId);

    @Query("select ur from UserResponse ur where ur.user.userId=:userId")
    Page<UserResponse> findAllByUser(@Param("userId") Integer userId, Pageable pageable);

    @Query("select ur from UserResponse ur where ur.user.userId=:userId and ur.responseApprovalStatus='Approved'")
    Page<UserResponse> findAllApprovedByUser(@Param("userId") Integer userId, Pageable pageable);

    @Query("select count (ur) from UserResponse ur where ur.user.userId=:userId")
    Integer findUserResponseCountByUserId(@Param("userId") Integer userId);

    List<UserResponse> findByIdIn(List<Long> responseIds);

}
