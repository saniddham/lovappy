package com.realroofers.lovappy.service.order.dto;

import com.realroofers.lovappy.service.order.model.CouponRule;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import com.realroofers.lovappy.service.order.support.RuleActionType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Date;

/**
 * Created by hasan on 17/11/2017.
 */

@Data
@ToString
@EqualsAndHashCode
public class CouponRuleDto {
    private Integer id;

    private String ruleDescription;
    private CouponCategory affectedCategory;
    private RuleActionType actionType;

    private Double discountValue;
    private Date creationDate;
    private Date expiryDate;

    public CouponRuleDto() {
    }

    public CouponRuleDto(CouponRule couponRule) {
        this.id = couponRule.getId();
        this.ruleDescription = couponRule.getRuleDescription();
        this.affectedCategory = couponRule.getAffectedCategory();
        this.actionType = couponRule.getActionType();
        this.discountValue = couponRule.getDiscountValue();
        this.creationDate = couponRule.getCreationDate();
        this.expiryDate = couponRule.getExpiryDate();
    }

    public CouponRuleDto(Integer id, String ruleDescription, CouponCategory affectedCategory, RuleActionType actionType, Double discountValue, Date creationDate, Date expiryDate) {
        this.id = id;
        this.ruleDescription = ruleDescription;
        this.affectedCategory = affectedCategory;
        this.actionType = actionType;
        this.discountValue = discountValue;
        this.creationDate = creationDate;
        this.expiryDate = expiryDate;
    }

}
