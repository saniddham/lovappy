package com.realroofers.lovappy.service.product.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.product.dto.BrandDto;
import com.realroofers.lovappy.service.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Manoj on 06/02/2018.
 */
@Entity
@Data
@EqualsAndHashCode
@Table(name = "product_brand")
public class Brand extends BaseEntity<Integer> implements Serializable {

    @NotBlank
    @Column(name = "brand_name")
    private String brandName;


    @Column(name = "brand_description")
    private String brandDescription;


    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;

    @JsonIgnore
    @JoinColumn(name = "created_by")
    @ManyToOne
    private User createdBy;

    public Brand() {
    }

    public Brand(BrandDto brand) {
        if(brand.getId()!=null){
            this.setId(brand.getId());
        }
        this.brandName = brand.getBrandName();
        this.brandDescription = brand.getBrandDescription();
        this.active = brand.getActive();
        if(brand.getCreatedBy()!=null) {
            this.createdBy = new User(brand.getCreatedBy().getID());
        }
    }
}
