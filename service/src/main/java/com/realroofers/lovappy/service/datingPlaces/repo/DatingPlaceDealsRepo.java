package com.realroofers.lovappy.service.datingPlaces.repo;

import com.realroofers.lovappy.service.datingPlaces.model.DatingPlace;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceDeals;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by Darrel Rayen on 1/22/18.
 */
@Repository
public interface DatingPlaceDealsRepo extends JpaRepository<DatingPlaceDeals, Integer> {

    @Query("select dd from DatingPlaceDeals dd where dd.approvalStatus=:approvalStatus and dd.id.datingPlace=:datingPlace")
    Page<DatingPlaceDeals> findAllByApprovalStatusAndDatingPlaceId(@Param("approvalStatus") ApprovalStatus approvalStatus, @Param("datingPlace") DatingPlace datingPlace, Pageable pageable);

    @Query("select dd from DatingPlaceDeals dd where dd.approvalStatus=:approvalStatus and dd.id.datingPlace=:datingPlace and dd.startDate >= :startDate")
    List<DatingPlaceDeals> findAllByApprovalStatusAndStartDateAndDatingPlace(@Param("approvalStatus") ApprovalStatus approvalStatus, @Param("startDate") Date startDate, @Param("datingPlace") DatingPlace datingPlace);

    @Query("select dd from DatingPlaceDeals dd where dd.approvalStatus=:approvalStatus and dd.id.datingPlace=:datingPlace and dd.startDate >= :startDate")
    Page<DatingPlaceDeals> findAllByApprovalStatusAndStartDateAndDatingPlacePaginated(@Param("approvalStatus") ApprovalStatus approvalStatus, @Param("startDate") Date startDate, @Param("datingPlace") DatingPlace datingPlace, Pageable pageable);
}
