package com.realroofers.lovappy.service.career;

import com.realroofers.lovappy.service.career.dto.CareerUserDto;

import java.util.List;

public interface CareerUserService {

   String addUser(CareerUserDto user, long vacancyId);
   CareerUserDto getUserById(long id);
   CareerUserDto searchUserByEmail(String email);
   List<CareerUserDto> getAllUsers();
   String editUser(CareerUserDto user);

}
