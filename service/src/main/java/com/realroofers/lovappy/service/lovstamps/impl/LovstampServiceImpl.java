package com.realroofers.lovappy.service.lovstamps.impl;

import com.google.common.base.Strings;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.cloud.repo.CloudStorageRepo;
import com.realroofers.lovappy.service.exception.ResourceNotFoundException;
import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.lovstamps.dto.LovastampDetailsDto;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampDto;
import com.realroofers.lovappy.service.lovstamps.dto.UserProfileLovstampsDto;
import com.realroofers.lovappy.service.lovstamps.model.Lovstamp;
import com.realroofers.lovappy.service.lovstamps.model.LovstampArchive;
import com.realroofers.lovappy.service.lovstamps.model.QLovstamp;
import com.realroofers.lovappy.service.lovstamps.repo.LovstampArchiveRepo;
import com.realroofers.lovappy.service.lovstamps.repo.LovstampRepo;
import com.realroofers.lovappy.service.radio.dto.RadioFilterDto;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.system.repo.LanguageRepo;
import com.realroofers.lovappy.service.user.dto.WithinRadiusDto;
import com.realroofers.lovappy.service.user.model.*;
import com.realroofers.lovappy.service.user.repo.*;
import com.realroofers.lovappy.service.user.support.ZodiacSigns;
import com.realroofers.lovappy.service.user.model.embeddable.Address;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.PrefHeight;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * Created by Daoud Shaheen on 12/31/2017.
 */
@Service
@Slf4j
public class LovstampServiceImpl implements LovstampService {


    private final LovstampRepo lovstampRepo;
    private final UserRepo userRepo;
    private final LanguageRepo languageRepo;
    private final UserPreferenceRepo userPreferenceRepo;
    private final UserProfileRepo userProfileRepo;
    private final LovstampArchiveRepo lovstampArchiveRepo;
    private final CloudStorageRepo cloudStorageRepo;
    private final RoleRepo roleRepo;
    private final UserRolesRepo userRolesRepo;
    private static final Logger LOGGER = LoggerFactory.getLogger(LovstampServiceImpl.class);

    public LovstampServiceImpl(LovstampRepo lovstampRepo, UserRepo userRepo, LanguageRepo languageRepo, UserPreferenceRepo userPreferenceRepo, UserProfileRepo userProfileRepo, LovstampArchiveRepo lovstampArchiveRepo, CloudStorageRepo cloudStorageRepo, RoleRepo roleRepo, UserRolesRepo userRolesRepo) {
        this.lovstampRepo = lovstampRepo;
        this.userRepo = userRepo;
        this.languageRepo = languageRepo;
        this.userPreferenceRepo = userPreferenceRepo;
        this.userProfileRepo = userProfileRepo;
        this.lovstampArchiveRepo = lovstampArchiveRepo;
        this.cloudStorageRepo = cloudStorageRepo;
        this.roleRepo = roleRepo;
        this.userRolesRepo = userRolesRepo;
    }

    @Transactional(readOnly = true)
    @Override
    public LovstampDto getLovstampByUserID(Integer userID) {

        Lovstamp lovstamp = lovstampRepo.findByUser(userRepo.findOne(userID));
        return lovstamp == null? null : new LovstampDto(lovstamp);
    }
    @Transactional(readOnly = true)
    @Override
    public LovastampDetailsDto getVoiceByUserID(Integer userID) {

        Lovstamp lovstamp = lovstampRepo.findByUser(userRepo.findOne(userID));
        if(lovstamp == null)
        {
            throw new ResourceNotFoundException("User doesnt record a voice");
        }
        return  new LovastampDetailsDto(lovstamp);
    }
    @Transactional(readOnly = true)
    @Override
    public LovstampDto getLovstampDetailsByUserID(Integer userID) {

        Lovstamp lovstamp = lovstampRepo.findByUser(userRepo.findOne(userID));
        return lovstamp == null? null : new LovstampDto(lovstamp, true);
    }



    @Override
    @Transactional(readOnly = true)
    public Page<LovstampDto> getLovstamps(Pageable pageable) {
        return lovstampRepo.findAll(Collections.singleton(roleRepo.findByName(Roles.USER.getValue())), pageable).map(LovstampDto::new);
    }

    @Override
    @Transactional
    public LovstampDto create(LovstampDto lovstampDto, boolean skipped) {

        return new LovstampDto(add(lovstampDto, skipped));
    }

    private Lovstamp add(LovstampDto lovstampDto, boolean skipped){

        User user = userRepo.findOne(lovstampDto.getUser().getID());
        Lovstamp lovstampByUser = lovstampRepo.findLovstampByUser(user);

        if (lovstampByUser == null) {

            lovstampByUser = new Lovstamp();
            lovstampByUser.setUser(user);

            lovstampByUser.setNumberOfFemaleListeners(0);
            lovstampByUser.setNumberOfMaleListeners(0);
            lovstampByUser.setNumberOfUpdates(0);
            lovstampByUser.setNumberOfListeners(0);
            Language language = languageRepo.findOne(lovstampDto.getLanguage().getId());
            lovstampByUser.setLanguage(language);
            user.getUserProfile().setLanguage(language);



        } else {
            //save old lovstamp to archive
            LovstampArchive archive = new LovstampArchive();
            archive.setAudioFileCloud(lovstampByUser.getAudioFileCloud());
            archive.setUserId(user.getUserId());
            archive.setCreated(new Date());
            lovstampArchiveRepo.save(archive);


            lovstampByUser.setNumberOfUpdates(lovstampByUser.getNumberOfUpdates()+1);
            lovstampByUser.setUpdated(new Date());
        }

        user.getUserProfile().setLovstampSkipped(skipped);
        userRepo.save(user);

        lovstampByUser.setSkip(skipped);
        lovstampByUser.setRecordSeconds(lovstampDto.getRecordSeconds());
        CloudStorageFile cloudStorageFile = null;
        if(lovstampDto.getAudioFileCloud().getId() != null )
            cloudStorageFile = cloudStorageRepo.findOne(lovstampDto.getAudioFileCloud().getId());

        if(cloudStorageFile == null) {
            cloudStorageFile = new CloudStorageFile();
            cloudStorageFile.setBucket(lovstampDto.getAudioFileCloud().getBucket());
            cloudStorageFile.setName(lovstampDto.getAudioFileCloud().getName());
            cloudStorageFile.setUrl(lovstampDto.getAudioFileCloud().getUrl());
        }
        lovstampByUser.setAudioFileCloud(cloudStorageFile);
        Lovstamp save = lovstampRepo.save(lovstampByUser);
        return save;
    }
    @Override
    @Transactional
    public LovastampDetailsDto save(LovstampDto lovstampDto, boolean skipped) {
        return new LovastampDetailsDto(add(lovstampDto, skipped));
    }
    @Transactional
    @Override
    public LovstampDto update(LovstampDto lovstampDto) {
        User user = userRepo.findOne(lovstampDto.getUser().getID());
        Lovstamp lovstamp = lovstampRepo.findLovstampByUser(user);

        if (lovstamp != null) {
            lovstamp.setUser(user);
            lovstamp.setRecordSeconds(lovstampDto.getRecordSeconds());
            lovstamp.setNumberOfUpdates(lovstamp.getNumberOfUpdates()+1);
            lovstamp.setUpdated(new Date());
            return new LovstampDto( lovstampRepo.save(lovstamp));
        }
        return null;
    }

    @Transactional(readOnly = true)
    @Override
    public Page<LovstampDto> getLovstampsByGender(Gender gender, Pageable pageable) {
        return lovstampRepo.findByUserUserProfileGenderAndUserRolesIn(gender, Collections.singleton(roleRepo.findByName(Roles.USER.getValue())), pageable).map(LovstampDto::new);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<LovstampDto> getAllLovstampsFiltered(Integer userID, Integer profileUserID, Boolean nextProfile, RadioFilterDto filter, Boolean useFilter, Pageable pageable) {

        if(useFilter){
            return getAllLovstampsByCustomFilter(userID, profileUserID, nextProfile, filter, pageable);
        }

        UserProfile userProfile = userRepo.findOne(userID).getUserProfile();
        UserPreference pref = userPreferenceRepo.findOne(userID);
        QLovstamp qLovstamp = QLovstamp.lovstamp;
//            List<Predicate> filters = new ArrayList<>();
        BooleanExpression currentUserIdFilter = QLovstamp.lovstamp.user.userId.notIn(userID);
        BooleanExpression completedProfileFilter = QLovstamp.lovstamp.user.userProfile.completed.eq(true);
        BooleanExpression activatedProfileFilter = QLovstamp.lovstamp.user.activated.eq(true);
        BooleanExpression approvedVoiceProfileFilter = QLovstamp.lovstamp.audioFileCloud.approved.eq(true);
        if (userProfile != null && pref != null) {

            //age filter
            BooleanExpression ageFilter = null;
            Integer minAge = 0, maxAge = 0;
            if (pref.getMinAge() != null && pref.getMaxAge() != null) {
                minAge = -(pref.getMinAge() - 5);
                maxAge = -(pref.getMaxAge() + 5);
            }

            //Get min date minus 5 years
            Calendar date1 = Calendar.getInstance();
            date1.setTime(new Date());
            date1.add(Calendar.YEAR, minAge);

            //Get max date plus 5 years
            Calendar date2 = Calendar.getInstance();
            date2.setTime(new Date());
            date2.add(Calendar.YEAR, maxAge);
            ageFilter= QLovstamp.lovstamp.user.userProfile.birthDate.between(date2.getTime(), date1.getTime());


            //Filter based on preferred height
            BooleanExpression heightFilter = null;
            PrefHeight height = pref.getHeight();

            if (height != null) {
                Double threeInchesInCentimeters = 3 * 2.54;
                if (userProfile.getHeightCm() != null) {
                    switch (height) {
                        case TALLER:
                            heightFilter = QLovstamp.lovstamp.user.userProfile.heightCm.gt(userProfile.getHeightCm() - threeInchesInCentimeters);
                            break;
                        case SHORTER:
                            heightFilter = QLovstamp.lovstamp.user.userProfile.heightCm.lt(userProfile.getHeightCm() + threeInchesInCentimeters);
                            break;

                        case SAME:
                            heightFilter = QLovstamp.lovstamp.user.userProfile.heightCm
                                    .between(userProfile.getHeightCm() + threeInchesInCentimeters, userProfile.getHeightCm() - threeInchesInCentimeters);
                            break;
                    }

                }
            }


            //Gender filter
            BooleanExpression genderFilter = null;

            Gender prefGender = pref.getGender();
            //If Male is seeking Male or Female is seeking Female or both are seeking both
            if (prefGender.equals(userProfile.getGender())) {
                genderFilter = QLovstamp.lovstamp.user.userProfile.gender.eq(prefGender)
                        .and(QLovstamp.lovstamp.user.userPreference.gender.eq(prefGender));
            } else {
                switch (prefGender) {
                    case MALE:
                        genderFilter = QLovstamp.lovstamp.user.userProfile.gender.eq(Gender.MALE)
                                .and(QLovstamp.lovstamp.user.userPreference.gender.eq(Gender.FEMALE));
                        break;
                    case FEMALE:
                        genderFilter = QLovstamp.lovstamp.user.userProfile.gender.eq(Gender.FEMALE)
                                .and(QLovstamp.lovstamp.user.userPreference.gender.eq(Gender.MALE));
                        break;
                    default:
                        genderFilter = QLovstamp.lovstamp.user.userPreference.gender.eq(Gender.BOTH);
                        break;
                }
            }


            //Language filter
            BooleanExpression languageFilter = null;

            BooleanExpression penisSizeFilter = null;

            BooleanExpression statusesFilter = null;

            //Zodiac filter
            BooleanExpression zodiacFilter = null;


            BooleanExpression nextUserIDFilter = null;
            if (profileUserID != null)
                nextUserIDFilter = nextProfile ? QLovstamp.lovstamp.user.userId.gt(profileUserID) :
                        QLovstamp.lovstamp.user.userId.lt(profileUserID);


            BooleanExpression distanceFilter = null;
//            NumberPath<Double> lat = QLovstamp.lovstamp.user.userProfile.address.latitude;
//            NumberPath<Double> lng = QLovstamp.lovstamp.user.userProfile.address.longitude;

            Address address = userProfile.getAddress();


            if(address != null && address.getLatitude() != null &&  address.getLongitude() != null ) {
                Double latitude = address.getLatitude();
                Double longitude = address.getLongitude();
                Integer radius = address.getLastMileRadiusSearch() != null ?  address.getLastMileRadiusSearch() : 300;
//                NumberPath<Double> distance = null;
//                NumberExpression<Double> formula =
//                        (acos(cos(radians(Expressions.constant(latitude)))
//                                .multiply(cos(radians(lat))
//                                        .multiply(cos(radians(lng).subtract(radians(Expressions.constant(longitude)))
//                                                .add(sin(radians(Expressions.constant(latitude)))
//                                                        .multiply(sin(radians(lat))))))))
//                                .multiply(Expressions.constant(3959)));
//
//                distanceFilter = formula.lt(radius);
                List<Integer> userIds = userProfileRepo.getUserIdsNearBy(latitude, longitude,radius);
                distanceFilter = QLovstamp.lovstamp.user.userId.in(userIds);
            }

//            BooleanExpression rolesFilter = QLovstamp.lovstamp.user.roles.any().in(userRolesRepo.findByIdRoleName(Roles.USER.getValue()));
            Predicate qLovstamps = QLovstamp.lovstamp.isNotNull()
                    .and(currentUserIdFilter
                            .and(completedProfileFilter)
                            .and(activatedProfileFilter)
                            .and(approvedVoiceProfileFilter)
                            .and(ageFilter)
//                    .and(rolesFilter)
                            .and(genderFilter)
                            .and(heightFilter)
                            .and(languageFilter)
                            .and(penisSizeFilter)
                            .and(statusesFilter)
                            .and(zodiacFilter)

                            .and(distanceFilter));

            return lovstampRepo
                    .findAll(qLovstamps, pageable)
                    .map(LovstampDto::new);

        }
        Predicate qLovstamps = QLovstamp.lovstamp.isNotNull()
                .and(currentUserIdFilter);

        return lovstampRepo
                .findAll(qLovstamps, pageable)
                .map(LovstampDto::new);
    }


    public Page<LovstampDto> getAllLovstampsByCustomFilter(Integer userID, Integer profileUserID, Boolean nextProfile, RadioFilterDto filter, Pageable pageable) {
        UserProfile userProfile = userID!=null ? userRepo.findOne(userID).getUserProfile() : null;
        QLovstamp qLovstamp = QLovstamp.lovstamp;
           //List<Predicate> filters = new ArrayList<>();
        //userID filter
        BooleanExpression completedProfileFilter = QLovstamp.lovstamp.user.userProfile.completed.eq(true);
        BooleanExpression activatedProfileFilter = QLovstamp.lovstamp.user.activated.eq(true);
        BooleanExpression approvedVoiceProfileFilter = QLovstamp.lovstamp.audioFileCloud.approved.eq(true);

        BooleanExpression filters = completedProfileFilter
                .and(activatedProfileFilter).and(approvedVoiceProfileFilter);
        BooleanExpression userIdFilter = null;
        BooleanExpression currentUserIdFilter = null;
        if(userID != null) {
             currentUserIdFilter = QLovstamp.lovstamp.user.userId.notIn(userID);
            filters.and(currentUserIdFilter);

        }
        if (filter.getUserId() != null) {
            if(userID != filter.getUserId()) {
                userIdFilter = QLovstamp.lovstamp.user.userId.eq(filter.getUserId());
                filters.and(userIdFilter);
                return lovstampRepo
                        .findAll(QLovstamp.lovstamp.isNotNull()
                                .and(completedProfileFilter
                                        .and(activatedProfileFilter).and(approvedVoiceProfileFilter)
                                        .and(userIdFilter)), pageable)
                        .map(LovstampDto::new);
            }
            else return new PageImpl<>(new ArrayList<>());
        }
        //age filter
        BooleanExpression ageFilter = null;
        Integer minAge = 0, maxAge = 0;
        if ( filter.getAgeFrom() != null && filter.getAgeTo() != null) {
            minAge = -filter.getAgeFrom();
            maxAge = -filter.getAgeTo();


            //Get min date minus 5 years
            Calendar date1 = Calendar.getInstance();
            date1.setTime(new Date());
            date1.add(Calendar.YEAR, minAge);

            //Get max date plus 5 years
            Calendar date2 = Calendar.getInstance();
            date2.setTime(new Date());
            date2.add(Calendar.YEAR, maxAge);
           ageFilter = QLovstamp.lovstamp.user.userProfile.birthDate.between(date2.getTime(), date1.getTime());
            filters.and(ageFilter);

        }
        //Gender filter
        BooleanExpression genderFilter = null;

        if (filter.getGender() != null && filter.getGender() != Gender.BOTH) {
            genderFilter = QLovstamp.lovstamp.user.userProfile.gender.eq(filter.getGender());
            filters.and(genderFilter);
        }


        //Filter based on preferred height
        BooleanExpression heightFilter = null;
        PrefHeight height = filter.getHeight();

        if (height != null) {
            Double threeInchesInCentimeters = 3 * 2.54;
            if (userProfile!=null && userProfile.getHeightCm() != null) {
                switch (height) {
                    case TALLER:
                        heightFilter = QLovstamp.lovstamp.user.userProfile.heightCm.gt(userProfile.getHeightCm() - threeInchesInCentimeters);
                        break;
                    case SHORTER:
                        heightFilter = QLovstamp.lovstamp.user.userProfile.heightCm.lt(userProfile.getHeightCm() + threeInchesInCentimeters);
                        break;

                    case SAME:
                        heightFilter = QLovstamp.lovstamp.user.userProfile.heightCm
                                .between(userProfile.getHeightCm() + threeInchesInCentimeters, userProfile.getHeightCm() - threeInchesInCentimeters);
                        break;
                }
                if(heightFilter != null)
                filters.and(heightFilter);
            }
        }


        //Language filter
        BooleanExpression languageFilter = null;

        if (!CollectionUtils.isEmpty(filter.getLanguages())) {
            languageFilter = QLovstamp.lovstamp.language.id.eq(-1);
            for (String s : filter.getLanguages()) {
                //languageIds.add(Integer.valueOf(s.trim()));
                languageFilter = languageFilter.or(QLovstamp.lovstamp.language.abbreviation.eq(s.trim()));
            }
            filters.and(languageFilter);
        }

        BooleanExpression penisSizeFilter = null;
        if (filter.getPenisSizeMatter() != null) {
            penisSizeFilter = QLovstamp.lovstamp.user.userPreference.penisSizeMatter.eq(filter.getPenisSizeMatter());
            filters.and(languageFilter);
        }

        BooleanExpression breastsSizeFilter = null;
        if (filter.getBreastsSize() != null) {
            breastsSizeFilter = QLovstamp.lovstamp.user.userPreference.bustSize.eq(filter.getBreastsSize());
            filters.and(breastsSizeFilter);
        }

        BooleanExpression buttsSizeFilter = null;
        if (filter.getButtsSize() != null) {
            buttsSizeFilter = QLovstamp.lovstamp.user.userPreference.buttSize.eq(filter.getButtsSize());
            filters.and(buttsSizeFilter);
        }


        BooleanExpression personalitiesFilter = null;
        HashMap<String, String> map1 = filter.getPersonalities();
        if (map1!=null&&!map1.isEmpty()) {
            List<Predicate> personalitiesFilters = new ArrayList<>();
            personalitiesFilter = QLovstamp.lovstamp.isNotNull();
            for (Map.Entry<String, String> entry : map1.entrySet()) {
                if (entry.getValue() != null) {
                    personalitiesFilters.add( QLovstamp.lovstamp.user.userProfile.personalities.contains(entry.getKey(), entry.getValue()));
                }
            }

            personalitiesFilter = personalitiesFilter.andAnyOf(personalitiesFilters.toArray(new Predicate[]{}));
            filters.and(personalitiesFilter);
        }

        BooleanExpression lifestylesFilter = null;

        HashMap<String, String> map2 = filter.getLifestyles();
        if (map2!=null&&!map2.isEmpty()) {
            List<Predicate> lifestylesFilters = new ArrayList<>();
            lifestylesFilter = QLovstamp.lovstamp.isNotNull();
            for (Map.Entry<String, String> entry : map2.entrySet()) {
                if (entry.getValue() != null) {
                    lifestylesFilters.add(  QLovstamp.lovstamp.user.userProfile.lifestyles.contains(entry.getKey(), entry.getValue()));
                }
            }
            lifestylesFilter = lifestylesFilter.andAnyOf(lifestylesFilters.toArray(new Predicate[]{}));
            filters.and(lifestylesFilter);
        }

        BooleanExpression statusesFilter = null;

        HashMap<String, String> map3 = filter.getStatuses();
        if (map3!=null&&!map3.isEmpty()) {
            List<Predicate> statusesFilters = new ArrayList<>();
            statusesFilter = QLovstamp.lovstamp.isNotNull();
            for (Map.Entry<String, String> entry : map3.entrySet()) {
                if (entry.getValue() != null) {
                    statusesFilters.add(QLovstamp.lovstamp.user.userProfile.statuses.contains(entry.getKey(), entry.getValue()));
                }
            }
            statusesFilter = statusesFilter.andAnyOf(statusesFilters.toArray(new Predicate[]{}));
            filters.and(statusesFilter);
        }

        //Zodiac filter
        BooleanExpression zodiacFilter = null;

        ZodiacSigns[] zodiacSigns = filter.getZodiacsigns();
        if (zodiacSigns != null && zodiacSigns.length>0) {

            zodiacFilter = QLovstamp.lovstamp.user.userProfile.zodiacSigns.in(zodiacSigns);
            filters.and(zodiacFilter);
        }


        BooleanExpression distanceFilter = null;
//            NumberPath<Double> lat = QLovstamp.lovstamp.user.userProfile.address.latitude;
//            NumberPath<Double> lng = QLovstamp.lovstamp.user.userProfile.address.longitude;

        WithinRadiusDto radiusDto;

        radiusDto = filter.getWithinRadius();

        if(radiusDto != null && radiusDto.getLatitude() != null &&  radiusDto.getLongitude() != null && radiusDto.getRadius() != null) {
            Double latitude = radiusDto.getLatitude();
            Double longitude = radiusDto.getLongitude();
            Integer radius = radiusDto.getRadius();
            List<Integer> userIds = userProfileRepo.getUserIdsNearBy(radiusDto.getLatitude(), radiusDto.getLongitude(), radiusDto.getRadius());
            distanceFilter = QLovstamp.lovstamp.user.userId.in(userIds);
            filters
                    .and(distanceFilter);
        }


        if(currentUserIdFilter == null) {
            Predicate qLovstamps = QLovstamp.lovstamp.isNotNull()
                    .andAnyOf(completedProfileFilter
                            .and(activatedProfileFilter)
                            .and(approvedVoiceProfileFilter)
                            .and(userIdFilter)
                            .and(genderFilter)
                            .and(ageFilter)
                            .and(heightFilter)
                            .and(languageFilter)
                            .and(penisSizeFilter)
                            .and(breastsSizeFilter)
                            .and(buttsSizeFilter)
                            .and(personalitiesFilter)
                            .and(lifestylesFilter)
                            .and(statusesFilter)
                            .and(zodiacFilter)
                            .and(distanceFilter));

            return lovstampRepo
                    .findAll(qLovstamps, pageable)
                    .map(LovstampDto::new);
        } else {
            Predicate qLovstamps = QLovstamp.lovstamp.isNotNull()
                    .and(completedProfileFilter
                            .and(currentUserIdFilter)
                            .and(activatedProfileFilter)
                            .and(approvedVoiceProfileFilter)
                            .and(userIdFilter)
//                            .and(rolesFilter)
                            //.and(ageFilter)
                            .and(genderFilter)
                            .and(heightFilter)
                            .and(ageFilter)
                            .and(languageFilter)
                            .and(penisSizeFilter)
                            .and(breastsSizeFilter)
                            .and(buttsSizeFilter)
                            .and(personalitiesFilter)
                            .and(lifestylesFilter)
                            .and(statusesFilter)
                            .and(zodiacFilter)

                            .and(distanceFilter));

            return lovstampRepo
                    .findAll(qLovstamps, pageable)
                    .map(LovstampDto::new);
        }

    }
    @Transactional(readOnly = true)
    @Override
    public UserProfileLovstampsDto getUserProfileLovstamps(Integer currentUser , Integer profileUserID) {
        LovstampDto nextLovdrop = null;
        LovstampDto prevLovdrop = null;

        Sort sortPrev = new Sort(Sort.Direction.DESC, "user.userId");
        Sort sortNext = new Sort(Sort.Direction.ASC, "user.userId");

        Page<LovstampDto> prev = getAllLovstampsFiltered(currentUser, profileUserID, false,
                new RadioFilterDto(), false, new PageRequest(0, 1, sortPrev));

        Page<LovstampDto> next = getAllLovstampsFiltered(currentUser,  profileUserID, true,
                new RadioFilterDto(), false, new PageRequest(0, 1, sortNext));


        // Next and previous lovdrops
        if (profileUserID != null) {

            if (prev.getContent().size() > 0)
                prevLovdrop = prev.getContent().get(0);

            if (next.getContent().size() > 0)
                nextLovdrop = next.getContent().get(0);
        }

        return new UserProfileLovstampsDto(prevLovdrop, nextLovdrop);
    }




    @Transactional
    @Override
    public void delete(Integer lovstampId) {
        lovstampRepo.delete(lovstampId);
    }

    @Transactional
    @Override
    public void incrementListenByCounter(Integer listionToUserId, Integer listenByUserId) {
        User listenTo = userRepo.findOne(listionToUserId);
        if(listenTo == null)
            return;
        Lovstamp lovstamp = lovstampRepo.findByUser(listenTo);

        //check if user who listen to lovdrop is the same as the owner
        if(listionToUserId.equals(listenByUserId)) {
            //dont do anything
            return;
        }

        User listenByUser = userRepo.findByListenLovstampSetIn(lovstamp);


        if (lovstamp != null) {
            //user not listen to this lovdrop before
            if (listenByUser == null) {
                listenByUser = userRepo.findOne(listenByUserId);
                UserProfile userProfile = listenByUser.getUserProfile();
                //increment general counter
                lovstamp.setNumberOfListeners(lovstamp.getNumberOfListeners() + 1);
                userProfile.setListenedToCount(userProfile.getListenedToCount() + 1);

                //increment based on gender
                Gender gender = listenByUser.getUserProfile().getGender();
                if (Gender.MALE.equals(gender)) {
                    lovstamp.setNumberOfMaleListeners(lovstamp.getNumberOfMaleListeners() + 1);
                    userProfile.setListenedToMaleCount(userProfile.getListenedToMaleCount() + 1);
                } else if (Gender.FEMALE.equals(gender)) {
                    lovstamp.setNumberOfFemaleListeners(lovstamp.getNumberOfFemaleListeners() + 1);
                    userProfile.setListenedToFemaleCount(userProfile.getListenedToFemaleCount() + 1);
                }

                lovstampRepo.save(lovstamp);
                userProfileRepo.save(userProfile);
                listenByUser.getListenLovstampSet().add(lovstamp);
                userRepo.save(listenByUser);

            }
        }
    }


    @Transactional
    @Override
    public void showLovstampInHomePage(Integer lovstampId, Boolean show) {
        Lovstamp lovstamp =  lovstampRepo.findOne(lovstampId);
        lovstamp.setShowInHome(show);
        lovstampRepo.save(lovstamp);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<LovstampDto> findByUserUserProfileIsAllowedProfilePicAndShowHome(boolean allowed, boolean showInHome, Pageable pageable) {

        return lovstampRepo.findByUserUserProfileIsAllowedProfilePicAndShowHome(allowed, Roles.USER.getValue(),showInHome, pageable).map(LovstampDto::new);
    }

}
