package com.realroofers.lovappy.service.datingPlaces.dto.mobile;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.datingPlaces.dto.*;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlace;
import com.realroofers.lovappy.service.datingPlaces.model.FoodType;
import com.realroofers.lovappy.service.datingPlaces.model.PersonType;
import com.realroofers.lovappy.service.datingPlaces.model.PlacesAttachment;
import com.realroofers.lovappy.service.datingPlaces.support.DatingAgeRange;
import com.realroofers.lovappy.service.datingPlaces.support.NeighborHood;
import com.realroofers.lovappy.service.datingPlaces.support.PriceRange;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static com.realroofers.lovappy.service.datingPlaces.support.DatingPlaceUtil.mapToFoodTypeDto;
import static com.realroofers.lovappy.service.datingPlaces.support.DatingPlaceUtil.mapToPlaceAtmosphereDto;

/**
 * Created by Darrel Rayen on 10/17/17.
 */
@Data
@EqualsAndHashCode
public class DatingPlaceMobileDto implements Serializable {

    private String datingPlacesId;
    private String placeName;
    private CloudStorageFileDto coverPicture;
    private String country;
    private String city;
    private String addressLine;
    private String addressLineTwo;
    private String zipCode;
    private String createEmail;
    private Gender gender;
    private DatingAgeRange ageRange;
    private PriceRange priceRange;
    private NeighborHood neighborHood;
    private Date createdDate;
    private Date deletedDate;
    private String state;
    private String password;
    private String userType;
    private ApprovalStatus placeApprovalStatus;
    private Double latitude;
    private Double longitude;
    private List<PlacesAttachmentDto> placesAttachmentDtos = new ArrayList<>();
    private String coverPictureUrl;// Google picture Url
    private List<String> placesUrlList; // Google places Url list
    private String isOpened; // Google specific attributes
    private String iconUrl; //Google places Attributes
    private String FoodTypes;
    private String PlaceAtmospheres;
    private String personTypes;
    private String placeDescription;
    private UserDto owner;

    private Double overAllRating;
    private Double securityAverage;
    private Double parkingAverage;
    private Double transportAverage;
    private Double googleAverage;
    private Double averageRatingByUser;

    private Integer maleCount;
    private Integer femaleCount;
    private Integer bothCount;

    private Boolean isFeatured;
    private Boolean isMapFeatured;
    private Boolean isLovStampFeatured;
    private Boolean approved;
    private Boolean isPetsAllowed;
    private Boolean isGoodForMarriedCouple;
    private Boolean isClaimed;
    private Collection<FoodTypeDto> foodTypesCollection;
    private Collection<PlaceAtmosphereDto> atmosphereDtoCollection;
    private Collection<PersonTypeDto> personTypeDtoCollection;
    private List<DatingPlaceReviewMobileDto> reviews;

    public DatingPlaceMobileDto(DatingPlace datingPlace) {
        this.datingPlacesId = String.valueOf(datingPlace.getDatingPlacesId());
        this.placeName = datingPlace.getPlaceName();
        this.coverPicture = getCoverImageDto(datingPlace.getCoverPicture());
        this.country = datingPlace.getCountry();
        this.city = datingPlace.getCity();
        this.state = datingPlace.getState();
        this.addressLine = datingPlace.getAddressLine();
        this.addressLineTwo = datingPlace.getAddressLineTwo();
        this.zipCode = datingPlace.getZipCode();
        this.createEmail = datingPlace.getCreateEmail();
        this.isFeatured = datingPlace.getFeatured() == null ? false : datingPlace.getFeatured();
        this.gender = datingPlace.getGender();
        this.ageRange = datingPlace.getAgeRange();
        this.priceRange = datingPlace.getPriceRange();
        this.createdDate = datingPlace.getCreatedDate();
        this.deletedDate = datingPlace.getDeletedDate();
        this.placeApprovalStatus = datingPlace.getPlaceApprovalStatus();
        this.approved = datingPlace.getApproved() == null ? false : datingPlace.getApproved();
        this.placesAttachmentDtos = getPlacesImageList(datingPlace.getPlacesAttachments());
        this.latitude = datingPlace.getLatitude();
        this.longitude = datingPlace.getLongitude();
        this.overAllRating = datingPlace.getAverageRating();
        this.averageRatingByUser = datingPlace.getAverageOverAllRatingByUser();
        this.isMapFeatured = datingPlace.getMapFeatured() == null ? false : datingPlace.getMapFeatured();
        this.isLovStampFeatured = datingPlace.getLoveStampFeatured() == null ? false : datingPlace.getLoveStampFeatured();
        this.placeDescription = datingPlace.getPlaceDescription();
        this.securityAverage = datingPlace.getSecurityAverage();
        this.parkingAverage = datingPlace.getParkingAverage();
        this.transportAverage = datingPlace.getTransportAverage();
        this.googleAverage = datingPlace.getGoogleAverage();
        this.maleCount = datingPlace.getSuitableForMaleCount();
        this.femaleCount = datingPlace.getSuitableForFemale();
        this.bothCount = datingPlace.getSuitableForBoth();
        this.neighborHood = datingPlace.getNeighborHood();
        this.isGoodForMarriedCouple = datingPlace.getGoodForMarriedCouple();
        this.isPetsAllowed = datingPlace.getPetsAllowed();
        this.foodTypesCollection = mapToFoodTypeDto(datingPlace.getFoodTypes());
        this.atmosphereDtoCollection = mapToPlaceAtmosphereDto(datingPlace.getPlaceAtmospheres());
        this.isClaimed = datingPlace.getClaimed() == null ? false : datingPlace.getClaimed();
        //this.owner = new UserDto(datingPlace.getOwner());
        //this.personTypeDtoCollection = getPersonTypeDtos(datingPlace.getPersonTypes());
    }

    public DatingPlaceMobileDto(DatingPlaceDto datingPlaceDto) {
        this.datingPlacesId = String.valueOf(datingPlaceDto.getDatingPlacesId());
        this.placeName = datingPlaceDto.getPlaceName();
        this.coverPicture = datingPlaceDto.getCoverPicture();
        this.coverPictureUrl = datingPlaceDto.getCoverPictureUrl();
        this.country = datingPlaceDto.getCountry();
        this.city = datingPlaceDto.getCity();
        this.state = datingPlaceDto.getState();
        this.addressLine = datingPlaceDto.getAddressLine();
        this.addressLineTwo = datingPlaceDto.getAddressLineTwo();
        this.zipCode = datingPlaceDto.getZipCode();
        this.createEmail = datingPlaceDto.getCreateEmail();
        this.isFeatured = datingPlaceDto.getIsFeatured() == null ? false : datingPlaceDto.getIsFeatured();
        this.gender = datingPlaceDto.getGender();
        this.ageRange = datingPlaceDto.getAgeRange();
        this.priceRange = datingPlaceDto.getPriceRange();
        this.createdDate = datingPlaceDto.getCreatedDate();
        this.deletedDate = datingPlaceDto.getDeletedDate();
        this.placeApprovalStatus = datingPlaceDto.getPlaceApprovalStatus();
        this.approved = datingPlaceDto.getApproved() == null ? false : datingPlaceDto.getApproved();
        //this.placesAttachmentDtos = datingPlaceDto.getPlacesAttachmentDtos(); //ToDO convert it
        this.placesUrlList = datingPlaceDto.getPlacesUrlList();
        this.latitude = datingPlaceDto.getLatitude();
        this.longitude = datingPlaceDto.getLongitude();
        this.overAllRating = datingPlaceDto.getOverAllRating();
        this.averageRatingByUser = datingPlaceDto.getAverageRating();
        this.isMapFeatured = datingPlaceDto.getIsMapFeatured() == null ? false : datingPlaceDto.getIsMapFeatured();
        this.isLovStampFeatured = datingPlaceDto.getIsLovStampFeatured() == null ? false : datingPlaceDto.getIsLovStampFeatured();
        this.placeDescription = datingPlaceDto.getPlaceDescription();
        this.securityAverage = datingPlaceDto.getSecurityAverage();
        this.parkingAverage = datingPlaceDto.getParkingAverage();
        this.transportAverage = datingPlaceDto.getTransportAverage();
        this.googleAverage = datingPlaceDto.getGoogleAverage();
        this.maleCount = datingPlaceDto.getMaleCount();
        this.femaleCount = datingPlaceDto.getFemaleCount();
        this.bothCount = datingPlaceDto.getBothCount();
        this.neighborHood = datingPlaceDto.getNeighborHood();
        this.isGoodForMarriedCouple = datingPlaceDto.getIsGoodForMarriedCouple();
        this.isPetsAllowed = datingPlaceDto.getIsPetsAllowed();
        this.isClaimed = datingPlaceDto.getIsClaimed() == null ? false : datingPlaceDto.getIsClaimed();
        this.atmosphereDtoCollection = datingPlaceDto.getAtmosphereDtoCollection();
        this.foodTypesCollection = datingPlaceDto.getFoodTypesCollection();
        //this.owner = new UserDto(datingPlaceDto.getOwner());
    }

    private CloudStorageFileDto getCoverImageDto(CloudStorageFile cloudStorageFile) {
        return new CloudStorageFileDto(cloudStorageFile);
    }

    private Collection<FoodTypeDto> getFoodTypeDtoList(Collection<FoodType> foodTypes) {
        Collection<FoodTypeDto> foodTypeDtos = new ArrayList<>();
        foodTypes.forEach(foodType -> {
            foodTypeDtos.add(new FoodTypeDto(foodType));
        });
        return foodTypeDtos;
    }

    private Collection<PersonTypeDto> getPersonTypeDtos(Collection<PersonType> personTypes) {
        Collection<PersonTypeDto> personTypeDtos = new ArrayList<>();
        personTypes.forEach(personType -> {
            personTypeDtos.add(new PersonTypeDto(personType));
        });
        return personTypeDtos;
    }

    private List<PlacesAttachmentDto> getPlacesImageList(List<PlacesAttachment> placesAttachmentList) {
        List<PlacesAttachmentDto> placesAttachmentDtoList = new ArrayList<>();
        placesAttachmentList.forEach(eventLocationPicture -> {
            placesAttachmentDtoList.add(new PlacesAttachmentDto(eventLocationPicture.getAttachmentId(), eventLocationPicture.getUrl(), new CloudStorageFileDto(eventLocationPicture.getPicture()), eventLocationPicture.getCreatedDate(), eventLocationPicture.getImageDescription()));
        });
        return placesAttachmentDtoList;
    }
}
