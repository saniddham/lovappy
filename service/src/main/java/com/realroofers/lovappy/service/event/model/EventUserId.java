package com.realroofers.lovappy.service.event.model;

import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@EqualsAndHashCode
public class EventUserId implements Serializable{


   private Event event;


   private User user;

    public EventUserId(){

    }

    public EventUserId(Event event, User user){
        this.event = event;
        this.user = user;
    }


    @ManyToOne
    @JoinColumn(name ="event_id")
    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }


    @ManyToOne
    @JoinColumn(name ="user_id")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof EventUserId))
            return false;
        EventUserId id = (EventUserId) obj;
        return id.getEvent().getEventId().equals(this.getEvent().getEventId())
                && id.getUser().getUserId().equals(this.getUser().getUserId());

    }

    @Override
    public int hashCode() {
        final int prime = 17;
        int result = 1;
        result = (prime * result) + (event == null ? 0 : event.getEventId().hashCode())
                    +(user == null ? 0 : user.getUserId().hashCode());

        return result;
    }
}
