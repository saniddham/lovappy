package com.realroofers.lovappy.service.user;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.user.dto.*;
import com.realroofers.lovappy.service.user.model.Role;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.model.UserRoles;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.vendor.dto.VendorDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

public interface UserService  {

    User getUserById(Integer userId);

    boolean isMusicianUser(Integer userId);

    boolean isAuthor(Integer userId);

    boolean isVendor(Integer userId);

    Collection<GrantedAuthority> getAuthorities(
            Collection<UserRoles> roles);
    Collection<GrantedAuthority> getAuthorities(
            Integer userId);
    User getUserByEmail(String email);

    User registerNewUser(UserDto userDto);
    UserDto registerNewUser(UserDto userDto,  WithinRadiusDto location);
    UserDto getUser(Integer userId);
    ToUser getToUser(Integer userId);
    Collection<UserDto> getAllUsers();

    Page<UserDto> findAllPageable(Pageable pageable);
    
    UserDto getUser(String email);


    User createUserWithRole(UserAuthDto dto,Roles role);

    UserAuthDto updateSocialMissingEmail(UserAuthDto userAuthDto);
    void update(UserAuthDto dto);

    void update(UserAuthDto dto,Roles role);

    UserAuthDto findOne(String socialId, String socialPlatform);

    boolean emailExists(String email);

    boolean emailExists(String email, Integer excludeId);

    UserDto setNewPassword(String key, String password);
    UserDto restNewPassword(String email, String password);

    MyAccountDto getMyAccount(Integer userID);

    MyAccountDto updateMyAccount(Integer userID, MyAccountDto myAccountDto);

    AdminProfileDto updateAdminProfile(Integer userID, AdminProfileDto adminProfileDto);

    UserDto verifyEmail(String key);

    void authenticate(String username, String password);

    String adminLogin(String username, String password);

    UserDto updateVerifyEmailKey(Integer userID, String key);

	Collection<UserDto> findWithUserRole();

    Collection<UserDto> getUserByRole(String role, int page, int limit);

    void revokeAdminRole(Integer adminId);

    User createNewAdmin(UserDto userDto);

    boolean unassignRole(String role, Integer userId);

    boolean assignRole(String role, Integer userId);

    boolean approveAmbassador(Integer userId);

    User getUserMostExchangeMessagesWithByUserId(Integer userId);

    void updateLastLogin(Integer userId);

    Page<AuthorDto> getAuthorsByActivated(Boolean activated, int page, int limit, Sort.Direction sorting);

    void activateAuthor(Integer userId, Boolean activate);

    boolean inviteAuthorByEmail(String email, String invitedEmail);

    AuthorDto getAuthor(Integer userId);

    User createNewAuthor(UserDto userDto);

    User updateAuthorDetails(Integer userId, CloudStorageFileDto cloudStorageFileDto, String zipcode, String authorName);
    List<Integer> getUserLikeIDs(Integer userId);

    String reportUser(UserFlagDto userFlag);

    Page<UserDto> getUsersByRole(String role, Pageable pageable);

    boolean updatePassword(Integer userId, String oldPassword, String newPassword);

    boolean updateVendorPassword(Integer userId, String oldPassword, String newPassword);

    Page<UserFlagDto> getUserComplaints(String searchEmail, Pageable pageable);

    Page<UserDto> getLastLoginUsers(String keyword, Pageable pageable, Date from , Date to);

    boolean setBanUserStatus(Integer userId, boolean status);

    boolean setPauseUserStatus(Integer userId, boolean status);

    RegisterUserDto getAmbassador(Integer userId);

    User createNewRegistration(Integer userId, RegisterUserDto registerUserDto);

    User createNewVendorRegistration(RegisterUserDto registerUserDto);

    User createRegistration(Integer userId, RegisterExistingUserDto registerExistingUserDto);

    Page<UserDto> getAllByKeyword(String role, String keyword, Pageable pageable);
    Page<UserReviewDto> findAllUsersWithLovdrop(String keyword, Pageable pageable);
    

    Integer countUsersByGender(Gender gender);

    void setTweeted(Boolean isTweeted, Integer userId);

    List<String> getRolesForUser(Integer userId);
    Page<UserDto> getMusicianByKeyword(String keyword, Pageable pageable);

    Page<UserDto> getVendorByKeyword(String keyword, Pageable pageable);

    Integer countArtistByDateBetween(Date fromDate, Date toDate);

    VendorDto findVendorByUserId(Integer userId);

    UserDto verifyMobile(Integer userId);
    String registerDeveloper(DeveloperRegistration registration);
    Set<Integer> filterUserBlockedByUserIds(Integer currentUserId, List<Integer> userIds);
    void activateUser(Integer userId, Boolean activate);

    void updateProfile(Integer userId, String email, String firstName, String lastName);

}
