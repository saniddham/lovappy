package com.realroofers.lovappy.service.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Eias Altawil on 5/26/2017
 */

@Component
public class FormValidator {

    private final LocalValidatorFactoryBean localValidatorFactoryBean;

    @Autowired
    public FormValidator(LocalValidatorFactoryBean localValidatorFactoryBean) {
        this.localValidatorFactoryBean = localValidatorFactoryBean;
    }

    public List<ErrorMessage> validate(Object t, Class<?>[] types) {
        javax.validation.Validator validator = localValidatorFactoryBean.getValidator();
        Set<ConstraintViolation<Object>> violations = validator.validate(t, types);
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (violations.size() > 0) {
            for (ConstraintViolation<Object> violation : violations) {
                errorMessages.add(new ErrorMessage(violation.getPropertyPath().toString(), violation.getMessage()));
            }
        }
        return errorMessages;
    }

    public List<ErrorMessage> validateExcel(int index, Object t, Class<?>[] types) {
        javax.validation.Validator validator = localValidatorFactoryBean.getValidator();
        Set<ConstraintViolation<Object>> violations = validator.validate(t, types);
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (violations.size() > 0) {
            for (ConstraintViolation<Object> violation : violations) {
                errorMessages.add(new ErrorMessage(violation.getPropertyPath().toString(), "Row - " + (index + 2) + " :: " + violation.getMessage()));
            }
        }
        return errorMessages;
    }
}
