package com.realroofers.lovappy.service.plan.model;

import com.realroofers.lovappy.service.event.support.EventStatus;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by hasan on 6/11/2017.
 */
@Entity
@EqualsAndHashCode
public class Plan {
    @Id
    @GeneratedValue
    private Integer planID;

    @Column(name = "plan_name", nullable = false)
    private String planName;

    @Column(name = "plan_subtitle", nullable = true)
    @Type(type = "text")
    private String planSubtitle;

    @Column(name = "color_scheme", nullable = false)
    private String colorScheme;

    private Boolean isValid;

    @OneToMany(mappedBy = "plan", cascade = CascadeType.ALL)
    private Collection<Feature> planFeatures;

    @Column(name = "plan_cost", nullable = false)
    private Integer cost;

    private String costDescription;

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public String getCostDescription() {
        return costDescription;
    }

    public void setCostDescription(String costDescription) {
        this.costDescription = costDescription;
    }

    public Integer getPlanID() {
        return planID;
    }

    public void setPlanID(Integer planID) {
        this.planID = planID;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getPlanSubtitle() {
        return planSubtitle;
    }

    public void setPlanSubtitle(String planSubtitle) {
        this.planSubtitle = planSubtitle;
    }

    public String getColorScheme() {
        return colorScheme;
    }

    public void setColorScheme(String colorScheme) {
        this.colorScheme = colorScheme;
    }

    public Collection<Feature> getPlanFeatures() {
        return planFeatures;
    }

    public void setPlanFeatures(Collection<Feature> planFeatures) {
        this.planFeatures = planFeatures;
    }

    public Boolean getIsValid() {
        return isValid;
    }

    public void setIsValid(Boolean valid) {
        isValid = valid;
    }
}
