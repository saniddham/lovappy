package com.realroofers.lovappy.service.notification.repo;

import com.realroofers.lovappy.service.notification.model.Notification;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Daoud Shaheen on 6/24/2017.
 */
public interface NotificationRepo extends JpaRepository<Notification, Long>{

    @Query("SELECT NEW Notification(nt.id, nt.content, nt.created, nt.type, un.seen, un.lastSeen) FROM Notification nt JOIN nt.usersNotifications un WHERE un.id.user = :user")
    Page<Notification> findPageByUser(@Param("user") User user, Pageable pageRequest);

    @Query("SELECT COUNT(nt.id) FROM Notification nt JOIN nt.usersNotifications un WHERE un.id.user = :user AND un.seen=:seen AND nt.type=:type")
    Integer countBySeenAndUserAndType(@Param("seen") Boolean seen, @Param("user") User user, @Param("type")NotificationTypes type);

    @Query("SELECT COUNT(nt.id) FROM Notification nt JOIN nt.usersNotifications un WHERE un.id.user = :user AND un.seen=:seen AND nt.type not in :types ")
    Integer countBySeenAndUserAndNotInTypes(@Param("seen") Boolean seen, @Param("user") User user, @Param("types")List<NotificationTypes> types);

    @Query("SELECT COUNT(nt.id) FROM Notification nt JOIN nt.usersNotifications un WHERE un.id.user = :user AND un.seen=:seen")
    Integer countBySeenAndUser(@Param("seen") Boolean seen, @Param("user") User user);

    @Query("SELECT NEW Notification(nt.id, nt.content, nt.created, nt.type, un.seen, un.lastSeen) FROM Notification nt JOIN nt.usersNotifications un WHERE un.id.user = :user AND nt.type=:type")
    Page<Notification> findPageByUserAndType(@Param("user") User user, @Param("type")NotificationTypes type, Pageable pageRequest);

    @Query("SELECT NEW Notification(nt.id, nt.content, nt.created, nt.type, un.seen, un.lastSeen) FROM Notification nt JOIN nt.usersNotifications un WHERE un.id.user = :user AND nt.type not in :types")
    Page<Notification> findPageByUserAndNotTypeIn(@Param("user") User user, @Param("types")List<NotificationTypes> types, Pageable pageRequest);

}
