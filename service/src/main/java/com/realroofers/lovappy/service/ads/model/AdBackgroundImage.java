package com.realroofers.lovappy.service.ads.model;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@Table(name="ad_background_image")
@EqualsAndHashCode
public class AdBackgroundImage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile imageFileCloud;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile featuredAdBackgroundFileCloud;

}
