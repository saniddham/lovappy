package com.realroofers.lovappy.service.news.repo;

import com.realroofers.lovappy.service.news.model.NewsCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NewsCategoryRepo extends JpaRepository<NewsCategory, Long> {
    public NewsCategory getOneByCategoryName(String categoryName);

    @Query(value = "SELECT cat FROM NewsCategory cat WHERE cat.status=1")
    public List<NewsCategory> getActiveNewsCatgeories();
}
