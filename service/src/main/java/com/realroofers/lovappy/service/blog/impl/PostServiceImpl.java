package com.realroofers.lovappy.service.blog.impl;

import com.github.slugify.Slugify;
import com.google.common.collect.Lists;
import com.querydsl.core.types.Predicate;
import com.realroofers.lovappy.service.blog.PostService;
import com.realroofers.lovappy.service.blog.dto.PostDto;
import com.realroofers.lovappy.service.blog.dto.PostShareStatisticsDto;
import com.realroofers.lovappy.service.blog.model.BlogTypes;
import com.realroofers.lovappy.service.blog.model.Category;
import com.realroofers.lovappy.service.blog.model.Post;
import com.realroofers.lovappy.service.blog.model.QPost;
import com.realroofers.lovappy.service.blog.repo.CategoryRepo;
import com.realroofers.lovappy.service.blog.repo.PostRepo;
import com.realroofers.lovappy.service.blog.support.PostStatus;
import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.core.SocialType;
import com.realroofers.lovappy.service.user.model.Role;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.repo.RoleRepo;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import com.realroofers.lovappy.service.util.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;

@Service
public class PostServiceImpl implements PostService {

    private static final String PARAGRAPH_SPLIT_REGEX = "(?m)(?=^\\s{4})";
    private static final String TITLE_REGEX = "[^a-zA-Z0-9]";


    private static final String group = "BLOG";

    private PostRepo postRepo;

    private UserRepo userRepo;

    private RoleRepo roleRepo;

    private CategoryRepo categoryRepo;

    private final CloudStorageService cloudStorageService;


    @Autowired
    public PostServiceImpl(CategoryRepo categoryRepo, RoleRepo roleRepo, UserRepo userRepo, PostRepo postRepo, CloudStorageService cloudStorageService) {
        this.postRepo = postRepo;
        this.roleRepo = roleRepo;
        this.userRepo = userRepo;
        this.categoryRepo = categoryRepo;
        this.cloudStorageService = cloudStorageService;
    }


    @Transactional
    public void save(Post post) {
        postRepo.save(post);
    }

    @Transactional
    @Override
    public PostDto save(PostDto postDto, Integer userId) {
        Post targetPost = new Post();
        targetPost.setPostAuthor(userRepo.findOne(userId));

        targetPost.setHtmlContent(postDto.getHtmlContent());
        setPostChanges(targetPost, postDto, false);



        if(postDto.getOriginalImage()!=null && postDto.getOriginalImage().getId()!=null){
            targetPost.setOriginalImage(cloudStorageService.findById(postDto.getOriginalImage().getId()));
        }
        Post savedPost = postRepo.save(targetPost);

        savedPost.setSlugUrl(createSlugUrl(targetPost.getPostTitle(), savedPost.getId()));

        return new PostDto(postRepo.save(savedPost));
    }

    private void setPostChanges(Post targetPost, PostDto postDto, boolean update) {
        if (StringUtils.hasText(postDto.getVideoLink()) && postDto.getVideoLink().length() > 1) {
            setVideo(postDto, targetPost);
        }

        targetPost.setKeywordsPhrases(postDto.getKeywordsPhrases());
        targetPost.setDraft(postDto.getDraft());
        targetPost.setWrittenByAdmin(postDto.isWrittenByAdmin());

        targetPost.setImageAlt(postDto.getImageAlt());
        targetPost.setPostContent(postDto.getPostContent());
        targetPost.setDescription(postDto.getDescription());
        targetPost.setKeywords(postDto.getKeywords());
        targetPost.setSeoTitle(postDto.getSeoTitle());
        Category category = null;
        if(postDto.getCategoryId() != null) {
            category = categoryRepo.findOne(postDto.getCategoryId());
            targetPost.setCategory(category);
        }
        if(postDto.getImage()!=null && postDto.getImage().getId()!=null){
            targetPost.setType(BlogTypes.GALLERY);
            targetPost.setImage(cloudStorageService.findById(postDto.getImage().getId()));
        }else {
            if(!update) {

                if(postDto.getCategoryId() != null && category.getImages().size() > 0) {
                    Random rand = new Random();
                    targetPost.setImage(category.getImages().get(rand.nextInt(category.getImages().size())));
                }
                targetPost.setType(BlogTypes.REGULAR);
            }
        }

        targetPost.setAuthorAnonymous(postDto.getAuthorAnonymous() == null ? false : postDto.getAuthorAnonymous());
        if (!update) {
            targetPost.setState(PostStatus.HIDDEN);
            targetPost.setCredit(0);
            targetPost.setViewCount(0);
            targetPost.setPostDate(new Date());
            targetPost.setPostTitle(postDto.getTitle());

        } else {
            if(!StringUtils.isEmpty(targetPost.getPostTitle()) && !targetPost.getPostTitle().equals(postDto.getTitle())){
                targetPost.setPostTitle(postDto.getTitle());
                targetPost.setSlugUrl(createSlugUrl(targetPost.getPostTitle(), targetPost.getId()));
            }
        }
    }

    public String createSlugUrl(String title, Integer postId) {
        if (StringUtils.isEmpty(title) || StringUtils.isEmpty(title.trim()))
            return "" + UUID.randomUUID();
        String slugUrl = title.trim().toLowerCase();
        Slugify slg = new Slugify();
        slg.withTransliterator(true);
        return slg.slugify(slugUrl) +"_"+postId;
    }

    @Transactional(readOnly = true)
    @Override
    public PostDto getMusicPost() {
        Page<PostDto> posts = findPostByKeyword("music", new PageRequest(0, 1));
        return posts.getTotalElements()  > 0 ? posts.getContent().get(0) : new PostDto();
    }

    /**
     * @param postDto
     * @param targetPost
     */
    private void setVideo(PostDto postDto, Post targetPost) {
        String link = postDto.getVideoLink();
        if (postDto.getVideoLink().contains("youtube")) {
            String newLink = "//www.youtube.com/embed/" + CommonUtils.findYoutubeVideoId(link);
            targetPost.setVideoLink(newLink);
        } else {
            targetPost.setVideoLink(postDto.getVideoLink());
        }
    }



    @Transactional
    public void update(Post post) {
        postRepo.save(post);
    }

    @Transactional
    @Override
    public PostDto update(Integer postId, PostDto postDto) {
        Post targetPost = postRepo.findOne(postId);
        setPostChanges(targetPost, postDto, true);
        targetPost.setHtmlContent(postDto.getHtmlContent());

        if(postDto.getOriginalImage()!=null && postDto.getOriginalImage().getId()!=null){
            targetPost.setOriginalImage(cloudStorageService.findById(postDto.getOriginalImage().getId()));
        }

       return new PostDto(postRepo.save(targetPost));
    }

    @Transactional
    @Override
    public PostDto updateByAdmin(Integer postId, PostDto postDto) {
        Post targetPost = postRepo.findOne(postId);
        postDto.setAuthorAnonymous(targetPost.getAuthorAnonymous());
        setPostChanges(targetPost, postDto, true);
        targetPost.setHtmlContent(postDto.getHtmlContent());

        if(postDto.getOriginalImage()!=null && postDto.getOriginalImage().getId()!=null){
            targetPost.setOriginalImage(cloudStorageService.findById(postDto.getOriginalImage().getId()));
        }

        return new PostDto(postRepo.save(targetPost));
    }
    @Transactional
    @Override
    public boolean updateStatus(Integer postId, PostStatus status) {
        Post post = postRepo.findOne(postId);
        boolean isApproved = false;
        if (post == null) {
            return false;
        } else {

            post.setState(status);
            isApproved = post.getApproved() == null ? false : post.getApproved();
            if(!isApproved && status.equals(PostStatus.ACTIVE)) {
                post.setApproved(true);
            }
            else if(isApproved && status.equals(PostStatus.DENIED)) {
                post.setApproved(false);
            }
            postRepo.save(post);
        }
        //updated success
        return isApproved;
    }


    @Transactional
    public void delete(Post post) {
        postRepo.delete(post);
    }


    @Transactional(readOnly = true)
    public PostDto findById(Integer ID) {
        Post post = postRepo.findOne(ID);
        if (post != null)
            return new PostDto(post);
        return new PostDto();
    }
    @Transactional(readOnly = true)
    public PostDto findBySlugUrl(String slugUrl) {
        Post post = postRepo.findBySlugUrl(slugUrl);
        if (post != null)
            return new PostDto(post);
        return new PostDto();
    }
    @Transactional
    @Override
    public PostDto getAndUpdateViewCountById(Integer viewCount, Integer ID) {
        Post post = postRepo.findOne(ID);
        if (post != null) {
            post.setViewCount(post.getViewCount() + 1);
            return new PostDto(postRepo.save(post));
        }
        return null;
    }

    @Transactional
    @Override
    public PostDto getAndUpdateViewCountBySlugUrl(Integer viewCount, String slugUrl) {
        Post post = postRepo.findBySlugUrl(slugUrl);
        if (post != null) {
            post.setViewCount(post.getViewCount() + 1);
            return new PostDto(postRepo.save(post));
        }
        return null;
    }
    @Transactional(readOnly = true)
    public PostDto getPost(Integer postId) {
        Post post = postRepo.findOne(postId);
        if (post != null) {
            return new PostDto(post);
        }
        return null;
    }


    @Override
    @Transactional(readOnly = true)
    public PostDto findTopOneOrderByViewCountDesc() {
        Post post = postRepo.findTop1ByStateOrderByViewCountDesc(PostStatus.ACTIVE);
        if (post != null) {
            return new PostDto(post);
        }
        return null;
    }

    @Transactional(readOnly = true)
    @Override
    public PostDto findTopOneOrderByPostDate() {
        Post post = postRepo.findTop1ByStateOrderByPostDateDesc(PostStatus.ACTIVE);
        if (post != null) {
            return new PostDto(post);
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PostDto> findMostViewed(int pageNo, int size) {
        QPost qPost = QPost.post;
        qPost.state.eq(PostStatus.ACTIVE);
        return postRepo.findAll(qPost.state.eq(PostStatus.ACTIVE),
                new PageRequest(pageNo - 1, size, new Sort(new Sort.Order(Sort.Direction.DESC, "viewCount"))))
                .map(PostDto::new);
    }


    @Transactional(readOnly = true)
    public List<PostDto> findByKeyword(String keyword) {
        List<PostDto> result = new ArrayList<PostDto>();
        QPost qPost = QPost.post;
        List<Post> posts = Lists.newArrayList(postRepo.findAll(
                qPost.postTitle.containsIgnoreCase(keyword)
                        .or(qPost.description.containsIgnoreCase(keyword))
                        .or(qPost.keywords.containsIgnoreCase(keyword))
                        .or(qPost.seoTitle.containsIgnoreCase(keyword))));
        if (posts != null) {
            for (Post post : posts) {
                result.add(new PostDto(post));
            }
        }
        return result;
    }


    @Transactional(readOnly = true)
    public List<Post> findByCategory(Integer categoryId) {
        QPost qPost = QPost.post;
        return Lists.newArrayList(postRepo.findAll(qPost.category.id.eq(categoryId)));
    }

    @Override
    public Page<PostDto> getPostsByCategory(Integer categoryId, int page, int limit) {
        Pageable pageRequest = new PageRequest(page - 1, limit);
        QPost qPost = QPost.post;
        return postRepo.findAll(qPost.category.id.eq(categoryId).and(qPost.state.eq(PostStatus.ACTIVE)), pageRequest).map(PostDto::new);
    }

    /**
     * @param text
     * @return
     */
    public static String[] parseText(String text) {
        if (StringUtils.hasText(text)) {
            String[] paragraphs = text.split(PARAGRAPH_SPLIT_REGEX);
            return paragraphs;
        }
        return null;
    }


    @Override
    @Transactional(readOnly = true)
    public List<PostDto> getTopRecentBlogs(int numberOfBlogs) {
        List<PostDto> result = new ArrayList<>();
        Pageable pageRequest = new PageRequest(0, numberOfBlogs, Sort.Direction.DESC, "postDate");
        Page<Post> posts = postRepo.findByState(PostStatus.ACTIVE, pageRequest);
        if (posts.getContent() != null) {
            for (Post post : posts.getContent()) {
                result.add(new PostDto(post));
            }
        }
        return result;
    }


    @Transactional
    @Override
    public boolean delete(Integer postId) {
        Post post = postRepo.findById(postId);
        if (post != null) {
            postRepo.delete(post);
            return true;
        }
        return false;
    }


    @Override
    @Transactional(readOnly = true)
    public Page<PostDto> findAllByPostDateAsc(int page, int limit) {

         return postRepo.findAllByOrderByPostDateAsc(new PageRequest(page - 1, limit)).map(PostDto::new);
    }


    @Override
    @Transactional(readOnly = true)
    public Page<PostDto> findAll(Pageable pageable) {
        Page<PostDto> posts = postRepo.findAll(pageable).map(PostDto::new);
        return posts;
    }


    @Transactional(readOnly = true)
    @Override
    public Page<PostDto> findPostByKeyword(String keyword, Pageable pageable) {
        QPost qPost = QPost.post;
        Predicate predicate;
        if ("anonymous".contains(keyword.toLowerCase())) {
            predicate =
                    qPost.state.eq(PostStatus.ACTIVE).and(
                            qPost.postTitle.containsIgnoreCase(keyword)
                                    .or(qPost.postContent.containsIgnoreCase(keyword))
                                    .or(qPost.description.containsIgnoreCase(keyword))
                                    .or(qPost.keywords.containsIgnoreCase(keyword))
                                    .or(qPost.seoTitle.containsIgnoreCase(keyword))
                                    .or(qPost.isAuthorAnonymous.eq("anonymous".contains(keyword.toLowerCase())))
                    );
        } else {
            predicate =
                    qPost.state.eq(PostStatus.ACTIVE).andAnyOf(


                            qPost.isAuthorAnonymous.eq(false).and(qPost.postAuthor.firstName.containsIgnoreCase(keyword)
                                    .or(qPost.postAuthor.lastName.containsIgnoreCase(keyword))
                                    .or(qPost.postAuthor.email.containsIgnoreCase(keyword).or(qPost.postTitle.containsIgnoreCase(keyword)
                                            .or(qPost.postContent.containsIgnoreCase(keyword))
                                            .or(qPost.description.containsIgnoreCase(keyword))
                                            .or(qPost.keywords.containsIgnoreCase(keyword))
                                            .or(qPost.seoTitle.containsIgnoreCase(keyword))))),

                            qPost.isAuthorAnonymous.eq(true).and(qPost.postTitle.containsIgnoreCase(keyword)
                                    .or(qPost.postContent.containsIgnoreCase(keyword))
                                    .or(qPost.description.containsIgnoreCase(keyword))
                                    .or(qPost.keywords.containsIgnoreCase(keyword))
                                    .or(qPost.seoTitle.containsIgnoreCase(keyword)))

                    );
        }
        Page<PostDto> posts = postRepo.findAll(
                predicate
                , pageable).map(PostDto::new);
        return posts;
    }

    @Transactional(readOnly = true)
    @Override
    public Page<PostDto> findPostByUserId(Integer userId, Integer page) {
        User user = userRepo.findOne(userId);
        return postRepo.findByPostAuthorOrderByIdDesc(user, new PageRequest(page - 1, 5)).map(PostDto::new);
    }

    @Transactional(readOnly = true)
    @Override
    public Integer getNumberOfPostByUserId(Integer userId) {
        User user = userRepo.findOne(userId);
        if (user == null)
            return 0;
        return postRepo.countByPostAuthor(user);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<PostDto> getAdminPosts(int page, int limit) {

        List<Role> roles = new ArrayList<>();
        roles.add(roleRepo.findByName(Roles.ADMIN.getValue()));
        roles.add(roleRepo.findByName(Roles.SUPER_ADMIN.getValue()));
        Pageable pageable = new PageRequest(page - 1, limit, Sort.Direction.DESC, "postDate");

        return postRepo.findByStateAndPostAuthorRolesIn(PostStatus.ACTIVE, roles, pageable).map(PostDto::new);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<PostDto> getFeaturedPosts(int page, int limit) {
        Pageable pageable = new PageRequest(page - 1, limit, Sort.Direction.DESC, "postDate");

        return postRepo.findByStateAndFeatured(PostStatus.ACTIVE, true, pageable).map(PostDto::new);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<PostDto> getPostByType(BlogTypes type, int page, int limit) {
        Pageable pageable = new PageRequest(page - 1, limit, Sort.Direction.DESC, "postDate");

        return postRepo.findByStateAndType(PostStatus.ACTIVE, type, pageable).map(PostDto::new);
    }

    @Transactional
    @Override
    public void setFeatured(Integer id) {
        Post post = postRepo.findOne(id);
        if(post != null ){
            post.setFeatured(post.getFeatured() == null || !post.getFeatured());
            postRepo.save(post);
        }
    }

    @Transactional
    @Override
    public void updateImage(Integer postId, CloudStorageFileDto cloudStorageFileDto) {
        Post post = postRepo.findOne(postId);

        if(post != null && cloudStorageFileDto.getUrl()!=null) {
            CloudStorageFile image = new CloudStorageFile();
            image.setName(cloudStorageFileDto.getName());
            image.setBucket(cloudStorageFileDto.getBucket());
            image.setUrl(cloudStorageFileDto.getUrl());

            post.setImage(image);
            post.setType(BlogTypes.GALLERY);

            postRepo.save(post);
        }
    }

    @Override
    public Page<PostShareStatisticsDto> getPostsStatestics(Pageable pageable) {
        return postRepo.findAll(pageable).map(PostShareStatisticsDto::new);
    }

    @Transactional
    @Override
    public void shareBySocialProvider(Integer postId, SocialType socialType) {
        Post post = postRepo.findOne(postId);
        if(post != null) {
            switch (socialType) {
                case EMAIl:
                    post.setEmailShares(post.getEmailShares() + 1);
                    break;
                case FACEBOOK:
                    post.setFacebookShares(post.getFacebookShares() + 1);
                    break;
                case GOOGLE:
                    post.setGoogleShares(post.getGoogleShares() + 1);
                    break;
                case PINTEREST:
                    post.setPinterestShares(post.getPinterestShares() + 1);
                    break;
                case LINKEDIN:
                    post.setLinkedinShares(post.getLinkedinShares() + 1);
                    break;
                case TWITTER:
                    post.setTwitterShares(post.getTwitterShares() + 1);
                    break;
            }

            postRepo.save(post);
        }
    }
}
