package com.realroofers.lovappy.service.vendor.impl;

import com.realroofers.lovappy.service.gift.dto.FilerObject;
import com.realroofers.lovappy.service.product.repo.ProductRepo;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.vendor.VendorLocationService;
import com.realroofers.lovappy.service.vendor.model.VendorLocation;
import com.realroofers.lovappy.service.vendor.repo.VendorLocationRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Manoj on 04/02/2018.
 */
@Service
public class VendorLocationServiceImpl implements VendorLocationService {

    private final VendorLocationRepository vendorLocationRepository;
    private final ProductRepo productService;
    private final UserService userService;

    public VendorLocationServiceImpl(VendorLocationRepository vendorLocationRepository, ProductRepo productService, UserService userService) {
        this.vendorLocationRepository = vendorLocationRepository;
        this.productService = productService;
        this.userService = userService;
    }

    @Override
    public List<VendorLocation> findAll() {
        return vendorLocationRepository.findAll();
    }

    @Override
    public List<VendorLocation> findAllActive() {
        return vendorLocationRepository.findAllByActiveIsTrueOrderByCompanyName();
    }

    @Override
    public VendorLocation create(VendorLocation vendorLocation) {
        return vendorLocationRepository.saveAndFlush(vendorLocation);
    }

    @Override
    public VendorLocation update(VendorLocation vendorLocation) {
        return vendorLocationRepository.save(vendorLocation);
    }

    @Override
    public void delete(Integer id) {
        if (id != null) {
            VendorLocation vendorLocation = findById(id);
            vendorLocation.setActive(false);
            update(vendorLocation);
        }
    }

    @Override
    public VendorLocation findById(Integer id) {
        return id != null ? vendorLocationRepository.findOne(id) : null;
    }

    @Override
    public String[] findLocationListByLoginUser(Integer loginUser) {

        User user = userService.getUserById(loginUser);

        if (user != null) {
            List<VendorLocation> locationList = vendorLocationRepository.findAllByActiveIsTrueAndCreatedByOrderByCompanyName(user);
            int i = locationList.size();
            int n = ++i;
            String[] locationStrList = new String[n];
            for (int cnt = 0; cnt < locationList.size(); cnt++) {
                locationStrList[cnt] = locationList.get(cnt).getStoreId();
            }
            return locationStrList;
        }

        return new String[0];
    }

    @Override
    public Page<VendorLocation> findLocationsByLoginUser(Integer loginUser, Pageable pageable, FilerObject filerObject) {
        User user = userService.getUserById(loginUser);

        String store = (filerObject != null && filerObject.getFilterType() != null && !filerObject.getSearchTerm().equals("")
                && filerObject.getFilterType().equalsIgnoreCase("store")) ? "%" + filerObject.getSearchTerm() + "%" : null;
        String company = (filerObject != null && filerObject.getFilterType() != null && !filerObject.getSearchTerm().equals("")
                && filerObject.getFilterType().equalsIgnoreCase("company")) ? "%" + filerObject.getSearchTerm() + "%" : null;
        String manager = (filerObject != null && filerObject.getFilterType() != null && !filerObject.getSearchTerm().equals("")
                && filerObject.getFilterType().equalsIgnoreCase("manager")) ? "%" + filerObject.getSearchTerm() + "%" : null;

        return vendorLocationRepository.findAllByCreatedByOrderByCompanyName(user, store, company, manager, pageable);
    }

    @Override
    public List<VendorLocation> findLocationsByLoginUser(Integer loginUser) {
        User user = userService.getUserById(loginUser);

        return vendorLocationRepository.findAllByActiveIsTrueAndCreatedByOrderByCompanyName(user);
    }

    @Override
    public long getVendorLocationCountByUser(Integer loginUser) {
        User user = userService.getUserById(loginUser);
        return vendorLocationRepository.countAllByCreatedBy(user);
    }

    @Override
    public List<VendorLocation> findAllVendorLocations(Integer userId) {
        User user = userService.getUserById(userId);
        return vendorLocationRepository.findAllByActiveIsTrueAndCreatedByOrderByCompanyName(user);
    }

    @Override
    public List<VendorLocation> findAllVendorLocationsNull() {

        return vendorLocationRepository.findAllByLatitudeIsNull();
    }
}
