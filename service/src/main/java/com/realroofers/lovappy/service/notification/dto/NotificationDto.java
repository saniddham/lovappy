package com.realroofers.lovappy.service.notification.dto;

import com.realroofers.lovappy.service.notification.model.Notification;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 6/24/2017.
 */
@Data
@ToString
@EqualsAndHashCode
public class NotificationDto implements Serializable {

    private Long id;
    private String content;

    private Boolean seen;

    private Date lastSeen;

    private NotificationTypes type;

    private Date createdOn;

    private Integer userId;

    private Long sourceId;

    public NotificationDto() {
    }

    public NotificationDto(String content, Boolean seen, Date lastSeen) {
        this.content = content;
        this.seen = seen;
        this.lastSeen = lastSeen;
    }
    public NotificationDto(Notification notification) {
        this.content = notification.getContent();
        this.seen = notification.getSeen();
        this.lastSeen = notification.getLastSeen();
        this.id=notification.getId();
        this.type=notification.getType();
        createdOn=notification.getCreatedOn();
        this.setUserId(notification.getUserId());
        this.sourceId = notification.getSourceId();

    }
}
