package com.realroofers.lovappy.service.lovstamps.model;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * Created by Daoud Shaheen on 2/2/2018.
 */
@Entity
@Table(name="lovstamp_archive")
@Data
@EqualsAndHashCode
public class LovstampArchive extends BaseEntity<Integer> {

    @Column(name = "record_file")
    private String recordFile;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn
    private CloudStorageFile audioFileCloud;

    private Integer userId;
}
