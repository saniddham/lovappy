package com.realroofers.lovappy.service.util;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.realroofers.lovappy.service.user.model.embeddable.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Eias Altawil on 6/16/2017
 */

@Component
public class AddressUtil {

    private static Logger LOG = LoggerFactory.getLogger(AddressUtil.class);

    @Value("${google.maps.api.key}")
    private String googleApiKey;

    public Address extractAddress(String zipCode){
        Address address = new Address();
        GeoApiContext context = new GeoApiContext().setApiKey(googleApiKey);
        try {
            GeocodingResult[] results =  GeocodingApi.geocode(context, zipCode).await();
            address = processResult(results);
        } catch (ApiException | InterruptedException | IOException e) {
            LOG.error(e.getMessage());
        }

        return address;
    }

    public Address extractAddress(LatLng location, Integer raduis){
        Address address = new Address();
        GeoApiContext context = new GeoApiContext().setApiKey(googleApiKey);
        try {
            GeocodingResult[] results =  GeocodingApi.reverseGeocode(context, location).await();
            address = processResult(results);
            address.setLastMileRadiusSearch(raduis);
        } catch (ApiException | InterruptedException | IOException e) {
            address.setLatitude(location.lat);
            address.setLongitude(location.lng);
        }

        address.setLastMileRadiusSearch(raduis);
        return address;
    }

    private Address processResult(GeocodingResult[] results){
        Address address = new Address();
        if(results != null && results.length > 0) {
            GeocodingResult result = results[0];

            address.setLongitude(result.geometry.location.lng);
            address.setLatitude(result.geometry.location.lat);
            address.setFullAddress(result.formattedAddress);

            for (AddressComponent addressComponent : result.addressComponents) {

                AddressComponentType componentType = addressComponent.types[0];
                if (componentType.equals(AddressComponentType.POSTAL_CODE)) {//ZipCode
                    address.setZipCode(addressComponent.longName);
                } else if (componentType.equals(AddressComponentType.LOCALITY)) {//City
                    address.setCity(addressComponent.longName);
                } else if (componentType.equals(AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1)) {//State
                    address.setState(addressComponent.longName);
                    address.setStateShort(addressComponent.shortName);
                } else if (componentType.equals(AddressComponentType.COUNTRY)) {//Country
                    address.setCountry(addressComponent.longName);
                    address.setCountryCode(addressComponent.shortName);
                }
            }
        }
        return address;
    }


    public static String getShortAddress(String stateShort) {
        return stateShort != null && stateShort.length() > 2? stateShort.substring(0, 2)  :  stateShort;
    }

    /**
     * Calculate distance between two points in latitude and longitude taking
     * into account height difference. If you are not interested in height
     * difference pass 0.0. Uses Haversine method as its base.
     *
     * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
     * el2 End altitude in meters
     * @returns Distance in Meters
     */
    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }




}
