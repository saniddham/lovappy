package com.realroofers.lovappy.service.blog.dto;


import com.realroofers.lovappy.service.blog.model.Post;
import lombok.EqualsAndHashCode;

/**
 * Created by Daoud Shaheen on 9/19/2017.
 */
@EqualsAndHashCode
public class PostShareStatisticsDto {

    private Integer postId;

    private String postTitle;

    private Integer facebookShares;

    private Integer linkedinShares;

    private Integer googleShares;

    private Integer twitterShares;

    private Integer emailShares;

    private Integer pinterestShares;

    public PostShareStatisticsDto() {
    }

    public PostShareStatisticsDto(Post post) {
        this.postId = post.getId();

        this.postTitle = post.getPostTitle();

        this.facebookShares = post.getFacebookShares();

        this.linkedinShares = post.getLinkedinShares();

        this.googleShares = post.getGoogleShares();

        this.twitterShares = post.getTwitterShares();

        this.emailShares = post.getEmailShares();

        this.pinterestShares = post.getPinterestShares();
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public Integer getFacebookShares() {
        return facebookShares;
    }

    public void setFacebookShares(Integer facebookShares) {
        this.facebookShares = facebookShares;
    }

    public Integer getLinkedinShares() {
        return linkedinShares;
    }

    public void setLinkedinShares(Integer linkedinShares) {
        this.linkedinShares = linkedinShares;
    }

    public Integer getGoogleShares() {
        return googleShares;
    }

    public void setGoogleShares(Integer googleShares) {
        this.googleShares = googleShares;
    }

    public Integer getTwitterShares() {
        return twitterShares;
    }

    public void setTwitterShares(Integer twitterShares) {
        this.twitterShares = twitterShares;
    }

    public Integer getEmailShares() {
        return emailShares;
    }

    public void setEmailShares(Integer emailShares) {
        this.emailShares = emailShares;
    }

    public Integer getPinterestShares() {
        return pinterestShares;
    }

    public void setPinterestShares(Integer pinterestShares) {
        this.pinterestShares = pinterestShares;
    }
}
