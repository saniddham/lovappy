package com.realroofers.lovappy.service.mail.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * Created by Daoud Shaheen on 9/8/2017.
 */
@Entity
@Table(name = "email_template_content")
@DynamicUpdate
@EqualsAndHashCode
public class EmailTemplateContent extends BaseEntity<Integer> {

    private String subject;

    @Column(columnDefinition = "TEXT")
    private String body;

    @Column(columnDefinition = "TEXT")
    private String htmlBody;

    private String language;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JsonBackReference
    @JoinColumn(name = "template_id")
    private EmailTemplate template;

    public EmailTemplateContent() {
    }

    public EmailTemplateContent(String subject, String body, String htmlBody, String language) {
        this.subject = subject;
        this.body = body;
        this.htmlBody = htmlBody;
        this.language = language;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getHtmlBody() {
        return htmlBody;
    }

    public void setHtmlBody(String htmlBody) {
        this.htmlBody = htmlBody;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public EmailTemplate getTemplate() {
        return template;
    }

    public void setTemplate(EmailTemplate template) {
        this.template = template;
    }
}
