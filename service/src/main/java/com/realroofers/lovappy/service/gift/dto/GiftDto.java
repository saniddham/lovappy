package com.realroofers.lovappy.service.gift.dto;

import com.realroofers.lovappy.service.product.dto.ProductDto;
import com.realroofers.lovappy.service.product.support.ProductAvailability;
import com.realroofers.lovappy.service.user.dto.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by Eias Altawil on 5/7/17
 */
@Data
@EqualsAndHashCode
public class GiftDto {

    private Integer id;

    private String location;
    private String productType;
    private String productName;
    private String productCode;
    private String brandName;
    private String productDescription;
    private Double price;
    private ProductAvailability productAvailability;
    private String imageURL1;
    private String imageURL2;
    private String imageURL3;
    private String searchKeywords;
    private String metaKeywords;
    private String metaDescription;
    private String productEan;
    private Double commissionEarned;
    private Boolean active = true;
    private Boolean approved = false;
    private UserDto approvedBy;
    private UserDto createdBy;
    private UserDto updatedBy;

    public GiftDto() {
    }


    public GiftDto(ProductDto gift) {
        if (gift != null) {
            this.id = gift.getId();
//            this.location = gift.getLocation();
            this.productType = gift.getProductType() != null ? gift.getProductType().getTypeName() : "";
            this.productName = gift.getProductName();
            this.productCode = gift.getProductCode();
            this.brandName = gift.getBrand() != null ? gift.getBrand().getBrandName() : "";
            this.productDescription = gift.getProductDescription();
            this.price = gift.getPrice().doubleValue();
            this.productAvailability = gift.getProductAvailability();
            this.imageURL1 = gift.getImage1() != null ? gift.getImage1().getUrl() : "";
            this.imageURL2 = gift.getImage2() != null ? gift.getImage2().getUrl() : "";
            this.imageURL3 = gift.getImage3() != null ? gift.getImage3().getUrl() : "";
//            this.searchKeywords = gift.getSearchKeywords();
//            this.metaKeywords = gift.getMetaKeywords();
//            this.metaDescription = gift.getMetaDescription();
            this.productEan = gift.getProductEan();
            this.commissionEarned = gift.getCommissionEarned() != null ? gift.getCommissionEarned().doubleValue() : 0d;
            this.active = gift.getActive();
            this.approved = gift.getApproved();

            this.approvedBy = gift.getApprovedBy();
            this.createdBy = gift.getCreatedBy();
            this.updatedBy = gift.getUpdatedBy();
        }
    }
}
