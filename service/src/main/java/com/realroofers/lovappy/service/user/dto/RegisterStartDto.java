package com.realroofers.lovappy.service.user.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.realroofers.lovappy.service.user.model.UserPreference;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.EqualsAndHashCode;

/**
 * 
 * @author mwiyono
 *
 */
@EqualsAndHashCode
public class RegisterStartDto {

	@NotNull
	private Gender gender;

	@NotNull
	private Gender prefGender;

	@NotNull
	private Date birthDate;

	@NotNull
	private AddressDto location;

	public RegisterStartDto() {
		this.location = new AddressDto();
		this.location.setLastMileRadiusSearch(300);
	}

	public RegisterStartDto(UserProfile profile, UserPreference pref) {
		gender = profile != null ? profile.getGender() : null;
		prefGender = pref != null ? pref.getGender() : null;

		if (profile != null) {
			if (profile.getAddress() != null) {
				this.location = AddressDto.toAddressDto(profile.getAddress());
			}
		}
		this.location = new AddressDto();
		this.location.setLastMileRadiusSearch(300);
	}



	public RegisterStartDto(Gender gender, Gender prefGender) {
		this.gender = gender;
		this.prefGender = prefGender;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Gender getPrefGender() {
		return prefGender;
	}

	public void setPrefGender(Gender prefGender) {
		this.prefGender = prefGender;
	}


	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public AddressDto getLocation() {
		return location;
	}

	public void setLocation(AddressDto location) {
		this.location = location;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		RegisterStartDto that = (RegisterStartDto) o;

		if (gender != that.gender) return false;
		if (birthDate != that.birthDate) return false;
		if (location != that.location) return false;
		return prefGender == that.prefGender;
	}

	@Override
	public int hashCode() {
		int result = gender != null ? gender.hashCode() : 0;
		result = 31 * result + (prefGender != null ? prefGender.hashCode() : 0);
		result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
		result = 31 * result + (location != null ? location.hashCode() : 0);
		return result;
	}

}
