package com.realroofers.lovappy.service.mail.repo;

import com.realroofers.lovappy.service.mail.model.EmailTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Daoud Shaheen on 9/6/2017.
 */
public interface EmailTemplateRepo extends JpaRepository<EmailTemplate, Integer> {
}
