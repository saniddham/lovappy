package com.realroofers.lovappy.service.upload;


import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.upload.dto.FileUploadDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Manoj
 */

public interface FileUploadService extends AbstractService<FileUploadDto, Integer> {

    Page<FileUploadDto> findAll(Pageable pageable, Integer userId);

    FileUploadDto findById(Integer id, Integer userId);
}
