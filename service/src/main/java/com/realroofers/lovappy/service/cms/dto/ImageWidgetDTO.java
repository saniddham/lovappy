package com.realroofers.lovappy.service.cms.dto;

import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 6/10/2017.
 */
@EqualsAndHashCode
public class ImageWidgetDTO implements Serializable {

    private Integer id;
    private String name;
    private String url;
    private String type;

    public ImageWidgetDTO(Integer id, String name, String url, String type) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
