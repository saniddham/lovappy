package com.realroofers.lovappy.service.ads.repo;

import com.realroofers.lovappy.service.ads.model.AdPricing;
import com.realroofers.lovappy.service.ads.support.AdMediaType;
import com.realroofers.lovappy.service.ads.support.AdType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


/**
 * @author Eias Altawil
 */
public interface AdPricingRepo extends JpaRepository<AdPricing, Integer> {

    List<AdPricing> findByAdTypeOrderByPriceAsc(AdType adType);

    List<AdPricing> findByAdTypeAndMediaTypeOrderByDurationAsc(AdType adType, AdMediaType contentType);

    AdPricing findByAdTypeAndMediaTypeAndDuration(AdType adType, AdMediaType mediaType, Integer duration);

    AdPricing findByAdTypeAndMediaType(AdType adType, AdMediaType mediaType);
}
