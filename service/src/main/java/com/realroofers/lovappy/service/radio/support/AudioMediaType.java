package com.realroofers.lovappy.service.radio.support;

public enum AudioMediaType {
    LOVSTAMP, ADVERTISEMENT, NEWS, COMEDY, PODCAST, MUSIC, AUDIO_BOOK
}