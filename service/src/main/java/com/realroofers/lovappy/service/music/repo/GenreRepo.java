package com.realroofers.lovappy.service.music.repo;

import com.realroofers.lovappy.service.music.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GenreRepo extends JpaRepository<Genre, Integer> {

    List<Genre> findByEnabled(Boolean enabled);
    Genre findByTitleIgnoreCase(String title);

    List<Genre> findByAddedToSurveyIsFalseAndEnabledIsTrue();
}
