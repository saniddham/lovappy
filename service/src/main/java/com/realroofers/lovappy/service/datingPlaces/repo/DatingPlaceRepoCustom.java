package com.realroofers.lovappy.service.datingPlaces.repo;

import com.realroofers.lovappy.service.datingPlaces.dto.DatingPlaceFilterDto;
import com.realroofers.lovappy.service.datingPlaces.dto.LocationDto;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlace;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.Query;

/**
 * Created by daoud on 1/4/2019.
 */
public interface DatingPlaceRepoCustom {

    Page<DatingPlace> findAllByAdvancedFilters(DatingPlaceFilterDto filterDto, LocationDto locationDto, Pageable pageable);
    Page<DatingPlace> findAllByAnyMatchAdvancedFilters(DatingPlaceFilterDto filterDto, LocationDto locationDto, Pageable pageable);
    Query generateQuery(boolean isCount, boolean isOr, DatingPlaceFilterDto filterDto, LocationDto locationDto);
}
