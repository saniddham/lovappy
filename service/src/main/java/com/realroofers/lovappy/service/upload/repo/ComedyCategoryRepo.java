package com.realroofers.lovappy.service.upload.repo;

import com.realroofers.lovappy.service.upload.model.ComedyCategory;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Manoj
 */
public interface ComedyCategoryRepo extends JpaRepository<ComedyCategory, Integer> {
}
