package com.realroofers.lovappy.service.user.dto;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.realroofers.lovappy.service.user.model.SocialProfile;
import com.realroofers.lovappy.service.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 3/3/2018.
 */
@Data
@ToString
@EqualsAndHashCode
public class SocialProfileDto implements Serializable {
    private String socialId;
    private String socialPlatform;
    private String firstName;
    private String lastName;
    private String name;
    private String imageUrl;
    private Integer userId;
    private String email;

    public SocialProfileDto() {
    }

    public SocialProfileDto(SocialProfile socialProfile) {
        this.socialId = socialProfile.getSocialId();
        this.socialPlatform = socialProfile.getSocialPlatform().getText();
        this.firstName = socialProfile.getFirstName();
        this.lastName = socialProfile.getLastName();
        this.name = socialProfile.getName();
        this.imageUrl = socialProfile.getImageUrl();

        if(socialProfile.getUser() != null) {
            this.userId = socialProfile.getUser().getUserId();
            this.email = socialProfile.getUser().getEmail();
        }

    }
}
