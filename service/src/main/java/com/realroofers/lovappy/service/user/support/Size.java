package com.realroofers.lovappy.service.user.support;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public enum Size {
    SMALL("small"), MEDIUM("medium"), LARGE("large"), DOESNTMATTER("doesn't matter");
    String text;

    Size(String text) {
        this.text = text;
    }

    @JsonCreator
    public static Size fromString(String string) {
        if ("small".equalsIgnoreCase(string)) {
            return SMALL;
        } else if ("medium".equalsIgnoreCase(string)) {
            return MEDIUM;
        } else if ("large".equalsIgnoreCase(string)) {
            return LARGE;
        } else if ("doesntmatter".equalsIgnoreCase(string)) {
            return DOESNTMATTER;
        } else if ("doesn't matter".equalsIgnoreCase(string)) {
            return DOESNTMATTER;
        } else {
            throw new IllegalArgumentException(string + " has no corresponding value");
        }
    }
}
