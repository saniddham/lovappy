package com.realroofers.lovappy.service.product;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.product.dto.RetailerCommissionDto;
import com.realroofers.lovappy.service.product.model.RetailerCommission;
import com.realroofers.lovappy.service.user.model.Country;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RetailerCommissionService extends AbstractService<RetailerCommissionDto, Integer> {

    Page<RetailerCommission> findAll(Pageable pageable);

    RetailerCommissionDto getCurrentCommission(Country country);
}
