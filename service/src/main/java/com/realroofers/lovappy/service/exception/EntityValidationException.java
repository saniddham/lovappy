package com.realroofers.lovappy.service.exception;

import com.realroofers.lovappy.service.validation.ErrorMessage;

import java.util.List;

/**
 * @author Eias Altawil
 */

public class EntityValidationException extends RuntimeException {
    private List<ErrorMessage> errorMessages;

    public EntityValidationException(List<ErrorMessage> errorMessages) {
        this.errorMessages = errorMessages;
    }
    public EntityValidationException(String error, List<ErrorMessage> errorMessages) {
        super(error);
        this.errorMessages = errorMessages;

    }
    public List<ErrorMessage> getErrorMessages() {
        return errorMessages;
    }
}
