package com.realroofers.lovappy.service.radio.support;

/**
 * Created by Manoj on 19/12/2017.
 */
public enum AudioInsertWhere {

    BEFORE("Before"), AFTER("After"), EVERY("Every"), BETWEEN("Between");

    private String name;

    AudioInsertWhere(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
