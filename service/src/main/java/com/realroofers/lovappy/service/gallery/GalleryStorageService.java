package com.realroofers.lovappy.service.gallery;

import java.util.Collection;

import com.realroofers.lovappy.service.gallery.dto.GalleryStorageFileDto;
import com.realroofers.lovappy.service.user.model.embeddable.Address;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GalleryStorageService {
	GalleryStorageFileDto add(GalleryStorageFileDto galleryStorageFileDto, Address address);
	Collection<GalleryStorageFileDto> getAllPhotos(Long count);
	Collection<GalleryStorageFileDto> getAllPhotos();
	Page<GalleryStorageFileDto> getAllPhotos(Pageable pageable);
	Page<GalleryStorageFileDto> getAllPhotosByStatus(Pageable pageable);
	void setActive(Integer id, ApprovalStatus status);
}
