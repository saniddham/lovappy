package com.realroofers.lovappy.service.questions.model;

import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Darrel Rayen on 11/4/18.
 */
@Entity
@Table(name = "buy_question_repsonses")
@EqualsAndHashCode
public class BuyResponse implements Serializable {

    @EmbeddedId
    private BuyerId buyerId;

    @ManyToOne
    @JoinColumn(name = "response_id")
    @MapsId("responseId")
    private UserResponse userResponse;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id")
    @MapsId("userId")
    private User user;

    public BuyResponse(UserResponse userResponse, User user) {
        this.userResponse = userResponse;
        this.user = user;
    }

    public BuyResponse() {
    }

    public BuyerId getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(BuyerId buyerId) {
        this.buyerId = buyerId;
    }

    public UserResponse getUserResponse() {
        return userResponse;
    }

    public void setUserResponse(UserResponse userResponse) {
        this.userResponse = userResponse;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
