package com.realroofers.lovappy.service.privatemessage;

import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.privatemessage.dto.PrivateMessageDto;
import com.realroofers.lovappy.service.privatemessage.model.PrivateMessage;
import com.realroofers.lovappy.service.privatemessage.support.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Collection;

/**
 * Created by Eias Altawil on 5/7/17
 */

public interface PrivateMessageService {

    Collection<PrivateMessageDto> getAllPrivateMessages();

    PrivateMessageDto getPrivateMessageByID(Integer ID);

    OrderDto sendPrivateMessage(PrivateMessageDto privateMessageDto, String coupon, PaymentMethodType paymentMethod);

    Collection<PrivateMessageDto> getReceivedPrivateMessages(Integer fromUserID);

    Collection<PrivateMessageDto> getSentPrivateMessages(Integer toUserID);

    Collection<PrivateMessageDto> getPrivateMessagesByFromUserAndToUser(Integer fromUserID, Integer toUserID);

    void updatePrivateMessageStatus(Integer privateMessageID, Status status);

    Integer countExchangedMessagesByUser(Integer userId);

    CalculatePriceDto calculatePrice(String coupon);

    Page<PrivateMessageDto> getSentPrivateMessages(Integer userId, PageRequest pageRequest);

    Page<PrivateMessageDto> getReceivedPrivateMessages(Integer userId, PageRequest pageRequest);

    Integer countSent(Integer userId);

    Integer countRecived(Integer userId);
    PrivateMessageDto sendPrivateMessageByCredit(PrivateMessageDto privateMessageDto);

}
