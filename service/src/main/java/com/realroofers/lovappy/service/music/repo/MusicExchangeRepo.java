package com.realroofers.lovappy.service.music.repo;

import com.realroofers.lovappy.service.music.model.Music;
import com.realroofers.lovappy.service.music.model.MusicExchange;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.Gender;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;

public interface MusicExchangeRepo extends JpaRepository<MusicExchange, Integer> {
    Integer countByFromUser(User fromUser);

    Integer countByToUserAndSeen(User fromUser, Boolean seen);

    Integer countByFromUserAndToUser(User fromUser, User toUser);

    Integer countByFromUserAndToUserAndMusic(User fromUser, User toUser, Music music);

    Integer countByToUser(User toUser);

    Integer countByFromUserAndToUserUserProfileGenderAndExchangeDateBetween(User fromUser, Gender gender, Date start, Date end);

    Page<MusicExchange> findByFromUser(User fromUser, Pageable pageable);

    Page<MusicExchange> findByToUser(User toUser, Pageable pageable);

    MusicExchange findTop1ByToUserAndFromUserAndMusicIdOrderByExchangeDateDesc(User userTo, User userFrom, Integer musicId);
}
