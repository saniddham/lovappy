package com.realroofers.lovappy.service.blog.model;

import com.realroofers.lovappy.service.blog.support.PostStatus;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "post")
@DynamicUpdate
@EqualsAndHashCode
public class Post implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6959386269145885147L;

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @Column(name = "credit")
    private Integer credit;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "post_date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date postDate;

    @Column(name = "post_title", nullable = false)
    private String postTitle;

    @Column(name = "post_content", nullable = false)
    @Type(type = "text")
    private String postContent;

    @Column(name = "html_content", nullable = false)
    @Type(type = "text")
    private String htmlContent;

    @Column(name = "keywords_phrases")
    @Type(type = "text")
    private String keywordsPhrases;

    @Column(name = "video_link")
    private String videoLink;

    @Column(name = "post_keywords")
    private String keywords;

    @Column(name = "post_description")
    private String description;

    @Column(name = "seo_title")
    private String seoTitle;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(name = "image_alt")
    private String imageAlt;

    @OneToOne(targetEntity = User.class, fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "post_author")
    private User postAuthor;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private PostStatus state;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private BlogTypes type;

    @Column(name = "featured", columnDefinition = "BIT(1) default 0")
    private Boolean featured;

    @Column(name = "view_count")
    private Integer viewCount;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile image;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "original_image")
    private CloudStorageFile originalImage;

    @Column(name = "is_author_anonymous", columnDefinition = "BIT(1) default 0")
    private Boolean isAuthorAnonymous;

    @Column(name = "written_by_admin", columnDefinition = "BIT(1) default 0")
    private Boolean writtenByAdmin;

    @Column(name = "approved", columnDefinition = "BIT(1) default 0")
    private Boolean approved;

    @Column(name = "draft", columnDefinition = "BIT(1) default 0")
    private Boolean draft;

    @Column(name = "facebook_shares", columnDefinition = "INT(11) default 0")
    private Integer facebookShares;

    @Column(name = "linkedin_shares", columnDefinition = "INT(11) default 0")
    private Integer linkedinShares;

    @Column(name = "google_shares", columnDefinition = "INT(11) default 0")
    private Integer googleShares;

    @Column(name = "twitter_shares", columnDefinition = "INT(11) default 0")
    private Integer twitterShares;

    @Column(name = "email_shares", columnDefinition = "INT(11) default 0")
    private Integer emailShares;

    @Column(name = "pinterest_shares", columnDefinition = "INT(11) default 0")
    private Integer pinterestShares;
    @Column(unique = true, name = "slug_url")
    private String slugUrl;


    public Post() {
        this.featured = false;
        this.approved = false;
        this.draft = false;
    }

    public Boolean getDraft() {
        return draft;
    }

    public void setDraft(Boolean draft) {
        this.draft = draft;
    }

    public CloudStorageFile getImage() {
        return image;
    }

    public void setImage(CloudStorageFile image) {
        this.image = image;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public boolean isHideAuthor() {
        return hideAuthor;
    }

    public void setHideAuthor(boolean hideAuthor) {
        this.hideAuthor = hideAuthor;
    }


    private transient boolean allowEdit;


    public String getImagAlt() {
        return imageAlt;
    }

    public void setImageAlt(String image_alt) {
        this.imageAlt = image_alt;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer iD) {
        id = iD;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date date) {
        this.postDate = date;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSeoTitle() {
        return seoTitle;
    }

    public void setSeoTitle(String seo_title) {
        this.seoTitle = seo_title;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public User getPostAuthor() {
        return postAuthor;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    @Transient
    private boolean hideAuthor;

    @Transient
    public String getAuthor() {
        if (postAuthor != null && postAuthor.getFirstName() != null) {
            return postAuthor.getFirstName();
        } else if (postAuthor == null) {
            return "Anonymous";
        }
        return "";
    }

    public Integer getFacebookShares() {
        return facebookShares;
    }

    public void setFacebookShares(Integer facebookShares) {
        this.facebookShares = facebookShares;
    }

    public Integer getLinkedinShares() {
        return linkedinShares;
    }

    public void setLinkedinShares(Integer linkedinShares) {
        this.linkedinShares = linkedinShares;
    }

    public Integer getGoogleShares() {
        return googleShares;
    }

    public void setGoogleShares(Integer googleShares) {
        this.googleShares = googleShares;
    }

    public Integer getTwitterShares() {
        return twitterShares;
    }

    public void setTwitterShares(Integer twitterShares) {
        this.twitterShares = twitterShares;
    }

    public Integer getEmailShares() {
        return emailShares;
    }

    public void setEmailShares(Integer emailShares) {
        this.emailShares = emailShares;
    }

    public Integer getPinterestShares() {
        return pinterestShares;
    }

    public void setPinterestShares(Integer pinterestShares) {
        this.pinterestShares = pinterestShares;
    }

    public void setPostAuthor(User post_author) {
        this.postAuthor = post_author;
    }

    public PostStatus getState() {
        return state;
    }

    public void setState(PostStatus state) {
        this.state = state;
    }

    public boolean isAllowEdit() {
        return allowEdit;
    }

    public void setAllowEdit(boolean allowEdit) {
        this.allowEdit = allowEdit;
    }


    public Integer getCredit() {
        return credit;
    }


    public void setCredit(Integer credit) {
        this.credit = credit;
    }


    public String getImageAlt() {
        return imageAlt;
    }

    public Boolean getAuthorAnonymous() {
        return isAuthorAnonymous;
    }

    public void setAuthorAnonymous(Boolean authorAnonymous) {
        isAuthorAnonymous = authorAnonymous;
    }

    public Boolean getWrittenByAdmin() {
        return writtenByAdmin;
    }

    public void setWrittenByAdmin(Boolean writtenByAdmin) {
        this.writtenByAdmin = writtenByAdmin;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    public BlogTypes getType() {
        return type;
    }

    public void setType(BlogTypes type) {
        this.type = type;
    }

    public Boolean getFeatured() {
        return featured;
    }

    public void setFeatured(Boolean featured) {
        this.featured = featured;
    }

    public String getSlugUrl() {
        return slugUrl;
    }

    public void setSlugUrl(String slugUrl) {
        this.slugUrl = slugUrl;
    }

    public String getKeywordsPhrases() {
        return keywordsPhrases;
    }

    public void setKeywordsPhrases(String keywordsPhrases) {
        this.keywordsPhrases = keywordsPhrases;
    }

    public CloudStorageFile getOriginalImage() {
        return originalImage != null ? originalImage : image;
    }

    public void setOriginalImage(CloudStorageFile originalImage) {
        this.originalImage = originalImage;
    }
}
