package com.realroofers.lovappy.service.privatemessage.support;

public enum Status {
    PAID("paid"), UNPAID("unpaid"), FREE("free");
    String text;

    Status(String text) {
        this.text = text;
    }
}