package com.realroofers.lovappy.service.music.support;

/**
 * Created by Daoud Shaheen on 11/18/2017.
 */
public enum MusicDownload {
    WEB, MOBILE
}
