package com.realroofers.lovappy.service.order;

import com.realroofers.lovappy.service.order.dto.PriceDto;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Daoud Shaheen on 6/1/2018.
 */
public interface PriceService {

    Page<PriceDto> getAll(Pageable pageable);

    void update(PriceDto priceDto);

    PriceDto get(Integer id);
    PriceDto getByType(OrderDetailType type);

}
