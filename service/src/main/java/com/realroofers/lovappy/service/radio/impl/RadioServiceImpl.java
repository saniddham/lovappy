package com.realroofers.lovappy.service.radio.impl;

import com.realroofers.lovappy.service.ads.AdsService;
import com.realroofers.lovappy.service.ads.dto.AdDto;
import com.realroofers.lovappy.service.ads.support.AdStatus;
import com.realroofers.lovappy.service.datingPlaces.DatingPlaceService;
import com.realroofers.lovappy.service.datingPlaces.dto.DatingPlaceDto;
import com.realroofers.lovappy.service.likes.LikeService;
import com.realroofers.lovappy.service.lovstamps.LovstampService;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampDto;
import com.realroofers.lovappy.service.radio.RadioService;
import com.realroofers.lovappy.service.radio.StartSoundService;
import com.realroofers.lovappy.service.radio.dto.RadioDto;
import com.realroofers.lovappy.service.radio.dto.RadioFilterDto;
import com.realroofers.lovappy.service.radio.dto.StartSoundDto;
import com.realroofers.lovappy.service.radio.support.RadioType;
import com.realroofers.lovappy.service.radio.support.StartSoundType;
import com.realroofers.lovappy.service.tips.TipsService;
import com.realroofers.lovappy.service.tips.dto.LoveTipDto;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.support.Gender;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Daoud Shaheen on 12/17/2017.
 */
@Service
public class RadioServiceImpl implements RadioService {

    private final LovstampService lovstampService;

    private final StartSoundService startSoundService;

    private final DatingPlaceService datingPlaceService;

    private final AdsService adsService;

    private final TipsService tipsService;

    private final LikeService likeService;
    private final UserService userService;

    public RadioServiceImpl(LovstampService lovstampService, StartSoundService startSoundService, DatingPlaceService datingPlaceService, AdsService adsService, TipsService tipsService, LikeService likeService, UserService userService) {
        this.lovstampService = lovstampService;
        this.startSoundService = startSoundService;
        this.datingPlaceService = datingPlaceService;
        this.adsService = adsService;
        this.tipsService = tipsService;
        this.likeService = likeService;
        this.userService = userService;
    }


    @Override
    public Page<RadioDto> getRadioItemsByGender(Gender gender, Integer page, Integer limit) {

        Page<AdDto> featuredAds = adsService.getRandomAds(true, AdStatus.APPROVED, new PageRequest(0, 4));
        int lovstampLimit = limit - featuredAds.getNumberOfElements();
        Page<LovstampDto> lovstamps = lovstampService.getLovstampsByGender(gender, new PageRequest(page - 1, lovstampLimit, Sort.Direction.DESC, "created"));

        if(lovstamps.getContent().size() == 0){
            return new PageImpl<>(new ArrayList<>(), new PageRequest(page - 1, lovstampLimit), lovstampLimit);
        }
        List<RadioDto> result = randomRadioPage(lovstamps.getContent(), featuredAds.getContent(), page, null, null);
        return new PageImpl<>(result,  new PageRequest(page - 1, lovstampLimit), lovstamps.getTotalElements());

    }

    @Override
    public Page<RadioDto> getAllRadioItems(Integer page, Integer limit) {
        Page<AdDto> featuredAds = adsService.getRandomAds(true, AdStatus.APPROVED, new PageRequest(0, 4));
        int lovstampLimit = limit - featuredAds.getNumberOfElements();
        Page<LovstampDto> lovstamps = lovstampService.getLovstamps(new PageRequest(page - 1, lovstampLimit, Sort.Direction.DESC, "created"));

        if(lovstamps.getContent().size() == 0){
            return new PageImpl<>(new ArrayList<>(), new PageRequest(page - 1, lovstampLimit), lovstampLimit);
        }
        List<RadioDto> result =  randomRadioPage(lovstamps.getContent(), featuredAds.getContent(), page, null, null);

        return new PageImpl<>(result, new PageRequest(page - 1, lovstampLimit), lovstamps.getTotalElements());
    }

    @Override
    public Page<RadioDto> getFilteredRadioItems(Integer userID, Boolean useFilter, RadioFilterDto filter, Integer page, Integer limit) {
        Page<AdDto> featuredAds = adsService.getRandomAds(true, AdStatus.APPROVED, new PageRequest(0, 4)); int lovstampLimit = limit - featuredAds.getNumberOfElements();

        Page<LovstampDto> lovstamps = lovstampService.getAllLovstampsFiltered(userID,
                null, null, filter, useFilter, new PageRequest(page - 1, lovstampLimit));
        if(page<=1 && lovstamps.getContent().size() == 0){
            return new PageImpl<>(new ArrayList<>(), new PageRequest(page - 1, lovstampLimit), lovstampLimit);
        }
        List<Integer> userIds = new ArrayList<>();
        for (LovstampDto lovstampDto :lovstamps.getContent()) {
            userIds.add(lovstampDto.getUser().getID());
        }
        List<Integer> likesUserIds = new ArrayList<>();
        Set<Integer> blocksUserIds = new HashSet<>();
        if(userID!=null && userIds.size() > 0) {
            likesUserIds = likeService.filterUserLikesByUserIds(userID, userIds);

            blocksUserIds = userService.filterUserBlockedByUserIds(userID, userIds);

        }
        List<RadioDto> result = new ArrayList<>();
        if(lovstamps.getSize()>1) {
            result = randomRadioPage(lovstamps.getContent(), featuredAds.getContent(), page, likesUserIds, blocksUserIds);
        }
        else {
            result = randomRadioPage(lovstamps.getContent(), new ArrayList<>(), page, likesUserIds, blocksUserIds);
        }
        //total number of elements = lovstamps.getTotalElements + number of featured ADs size * page
        return new PageImpl<>(result, new PageRequest(page - 1, lovstampLimit), lovstamps.getTotalElements());

    }

    public List<RadioDto> randomRadioPage(List<LovstampDto> lovstamps,  List<AdDto> featuredAdsList, int page, List<Integer> userLikeIds, Set<Integer> blocksUserIds){

        StartSoundDto maleStartRadio = startSoundService.getSoundByType(StartSoundType.MALE);
        StartSoundDto femaleStartRadio = startSoundService.getSoundByType(StartSoundType.FEMALE);

        List<RadioDto> result = new ArrayList<>();
        List<RadioDto> privateRadio = new ArrayList<>();
        int privateStamps = 0;
        for (LovstampDto lovstamp : lovstamps ) {
            RadioDto radio = new RadioDto(lovstamp);
            //set Gender Start
            if(lovstamp.getUser().getUserProfile().getGender().equals(Gender.MALE)) {

                radio.setStartSound(maleStartRadio);
            } else {
                radio.setStartSound(femaleStartRadio);
            }
            radio.setGender(lovstamp.getUser().getUserProfile().getGender());

            if(lovstamp.getUser().getUserProfile().getAllowedProfilePic() != null && lovstamp.getUser().getUserProfile().getAllowedProfilePic()
                    && lovstamp.getUser().getUserProfile().getProfilePhotoCloudFile() != null && lovstamp.getUser().getUserProfile().getProfilePhotoCloudFile().getApproved() != null &&
                    lovstamp.getUser().getUserProfile().getProfilePhotoCloudFile().getApproved()){
                   radio.setType(RadioType.PUBLIC_LOVSTAMP);

                   result.add(radio);
            } else {
                radio.setType(RadioType.PRIVATE_LOVSTAMP);
                privateStamps ++;
                privateRadio.add(radio);
            }

            if(userLikeIds != null && userLikeIds.size() > 0) {
                radio.setLiked(userLikeIds.contains(lovstamp.getUser().getID()));
            }
            if(blocksUserIds != null && blocksUserIds.size() > 0) {
                radio.setBlocked(blocksUserIds.contains(lovstamp.getUser().getID()));
            }
        }
        // Handel Private radio
        if(privateStamps > 0) {
            int index = 0;

            Page<DatingPlaceDto> datingPlaces = datingPlaceService.getAllFeaturedPlaces(new PageRequest(page - 1, 1));
            //set dating places
            for (DatingPlaceDto datingPlace : datingPlaces.getContent()) {
                privateRadio.get(index).setDatingPlace(datingPlace);
                index++;
            }

           // privateStamps -= datingPlaces.getNumberOfElements();
            if(privateStamps - index> 0) {
                Page<LoveTipDto> loveTipPage = tipsService.getAllByActive(new PageRequest(page - 1, 1),true);
                //set tips
                int tipNumber = page;
                for (LoveTipDto tip : loveTipPage.getContent()) {
                    privateRadio.get(index).setLovTip(tip, tipNumber ++ );
                    index++;
                }
             //   privateStamps -= loveTipPage.getNumberOfElements();
            }


            if(privateStamps - index > 0) {
                //TODO change it later when we have a lot of data
                Page<AdDto> adsPage = adsService.getRandomAds(false, AdStatus.APPROVED, new PageRequest(0, privateStamps - index));
               // privateStamps -= adsPage.getNumberOfElements();

                //set ads
                for (AdDto ad : adsPage.getContent()) {
                    privateRadio.get(index).setAd(ad);
                    index++;
                }
                //TODO this is workarround to set the
                Random random = new Random();
                while (privateStamps - index > 0) {
                    if (adsPage.getContent().size() > 0) {
                        AdDto ad = adsPage.getContent().get(random.nextInt(adsPage.getContent().size()));
                        privateRadio.get(index).setAd(ad);
                        index++;
                    }
                }
                }
                //privateStamps -= adsPage.getNumberOfElements();
            }



        //add the featured data
        StartSoundDto musicSound = startSoundService.getSoundByType(StartSoundType.MUSIC);
        StartSoundDto newsSound = startSoundService.getSoundByType(StartSoundType.NEWS);
        StartSoundDto audioBookSound = startSoundService.getSoundByType(StartSoundType.AUDIO_BOOK);
        StartSoundDto adsSound = startSoundService.getSoundByType(StartSoundType.ADVERTISEMENT);
        StartSoundDto comedySound = startSoundService.getSoundByType(StartSoundType.COMEDY);
        StartSoundDto podcastSound = startSoundService.getSoundByType(StartSoundType.PODCAST);
        for (AdDto ad : featuredAdsList) {

            RadioDto radio = new RadioDto(ad, null);

            switch (ad.getAdType()) {
                case ADVERTISEMENT:
                    radio.setStartSound(adsSound);
                    break;
                case NEWS:
                    radio.setStartSound(newsSound);
                    break;
                case MUSIC:
                    radio.setStartSound(musicSound);
                    break;
                case COMEDY:
                    radio.setStartSound(comedySound);
                    break;
                case PODCAST:
                    radio.setStartSound(podcastSound);
                    break;
                case AUDIO_BOOK:
                    radio.setStartSound(audioBookSound);
                    break;
            }


            result.add(radio);
        }

        result.addAll(privateRadio);
        Collections.shuffle(result);
        return result;
    }

    @Override
    public Page<RadioDto> getHomePublicLovRadio(Pageable pageable) {

        Page<LovstampDto> lovstamps = lovstampService.findByUserUserProfileIsAllowedProfilePicAndShowHome(true, true, pageable);

        List<RadioDto> radioList = new ArrayList<>();
        for (LovstampDto lovstamp: lovstamps.getContent()) {
            radioList.add(new RadioDto(lovstamp, null));
        }

        return new PageImpl<>(radioList, pageable, lovstamps.getTotalElements());
    }

    @Override
    public Page<RadioDto> getHomePrivateLovRadio(Pageable pageable) {
        Page<LovstampDto> lovstampsPage = lovstampService.findByUserUserProfileIsAllowedProfilePicAndShowHome(false, true, pageable);
        Page<AdDto> adsPage = adsService.getRandomAds(false, AdStatus.APPROVED, pageable);

        List<RadioDto> radioList = new ArrayList<>();
        int offset = 0;
        List<AdDto> adList = adsPage.getContent();
        for (LovstampDto lovstamp: lovstampsPage.getContent()) {
            if(offset < adList.size()) {
                radioList.add(new RadioDto(lovstamp, null, adList.get(offset)));
                offset ++;
            } else {
                radioList.add(new RadioDto(lovstamp, null));
            }
        }

        return new PageImpl<>(radioList, pageable, lovstampsPage.getTotalElements());
    }


}
