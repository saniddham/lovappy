package com.realroofers.lovappy.service.user.support;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public enum SubscriptionType {
    LITE("lite"), PREMIUM("premium");
    String text;

    SubscriptionType(String text) {
        this.text = text;
    }

    @JsonCreator
    public static SubscriptionType fromString(String string) {
        if ("lite".equalsIgnoreCase(string) || "lets go!".equalsIgnoreCase(string)) {
            return LITE;
        } else if ("premium".equalsIgnoreCase(string)) {
            return PREMIUM;
        } else {
            throw new IllegalArgumentException(string + " has no corresponding value");
        }
    }
}
