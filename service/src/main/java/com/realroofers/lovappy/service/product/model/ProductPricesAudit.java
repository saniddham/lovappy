package com.realroofers.lovappy.service.product.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.product.support.Upload;
import com.realroofers.lovappy.service.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Manoj on 30/09/2018.
 */
@Entity
@Data
@EqualsAndHashCode
@Table(name = "product_price_audit")
public class ProductPricesAudit extends BaseEntity<Integer> implements Serializable {

    @JoinColumn(name = "product")
    @ManyToOne
    Product product;

    @NotNull(message = "Price cannot be empty.", groups = {Upload.class})
    @Column(name = "price")
    private BigDecimal price;

    @JoinColumn(name = "created_by")
    @ManyToOne
    private User createdBy;

}
