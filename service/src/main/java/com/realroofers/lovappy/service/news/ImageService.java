package com.realroofers.lovappy.service.news;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ImageService {
    public void UploadImage(MultipartFile file, String fileName) throws IOException;

    public Resource getImage(String fileName);

    public void deleteImage(String fileName) throws IOException;
}