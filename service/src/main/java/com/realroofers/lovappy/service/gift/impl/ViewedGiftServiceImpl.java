package com.realroofers.lovappy.service.gift.impl;

import com.realroofers.lovappy.service.gift.ViewedGiftService;
import com.realroofers.lovappy.service.gift.dto.ViewedGiftDto;
import com.realroofers.lovappy.service.gift.model.ViewedGift;
import com.realroofers.lovappy.service.gift.repo.ViewedGiftRepo;
import com.realroofers.lovappy.service.user.UserService;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ViewedGiftServiceImpl implements ViewedGiftService {

    private final ViewedGiftRepo viewedGiftRepo;
    private final UserService userService;

    public ViewedGiftServiceImpl(ViewedGiftRepo viewedGiftRepo, UserService userService) {
        this.viewedGiftRepo = viewedGiftRepo;
        this.userService = userService;
    }


    @Override
    @Transactional(readOnly = true)
    public List<ViewedGiftDto> findAll() {
        List<ViewedGiftDto> purchasedGiftDtoList = new ArrayList<>();

        viewedGiftRepo.findAll().stream().forEach(viewedGift -> {
            purchasedGiftDtoList.add(new ViewedGiftDto(viewedGift));
        });

        return purchasedGiftDtoList;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ViewedGiftDto> findAllActive() {
        List<ViewedGiftDto> purchasedGiftDtoList = new ArrayList<>();

        viewedGiftRepo.findAll().stream().forEach(viewedGift -> {
            purchasedGiftDtoList.add(new ViewedGiftDto(viewedGift));
        });

        return purchasedGiftDtoList;
    }

    @Override
    @Transactional
    public ViewedGiftDto create(ViewedGiftDto viewedGiftDto) {
        ViewedGift viewedGift;
        ViewedGift product = viewedGiftRepo.findTopByUserAndProduct(userService.getUserById(viewedGiftDto.getUser().getID()), viewedGiftDto.getProduct());

        if (product != null) {
            product.setViewedOn(new Date());
            viewedGift = viewedGiftRepo.save(product);
        } else {
            viewedGiftDto.setViewedOn(new Date());
            viewedGift = viewedGiftRepo.saveAndFlush(new ViewedGift(viewedGiftDto));
            if (viewedGiftRepo.count() > 15) {
                ViewedGift gift = viewedGiftRepo.findTopByUserOrderByViewedOnDesc(viewedGift.getUser());
                delete(gift.getId());
            }
        }

        return new ViewedGiftDto(viewedGift);
    }

    @Override
    @Transactional
    public ViewedGiftDto update(ViewedGiftDto viewedGiftDto) {
        ViewedGift viewedGift = viewedGiftRepo.save(new ViewedGift(viewedGiftDto));
        return new ViewedGiftDto(viewedGift);
    }

    @Override
    @Transactional
    @Modifying
    public void delete(Integer id) {
        viewedGiftRepo.delete(id);
    }

    @Override
    @Transactional(readOnly = true)
    public ViewedGiftDto findById(Integer id) {

        ViewedGift gift = viewedGiftRepo.findOne(id);

        if (gift != null) {
            return new ViewedGiftDto(gift);
        }

        return null;
    }
}
