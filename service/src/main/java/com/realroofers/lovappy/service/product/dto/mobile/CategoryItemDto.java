package com.realroofers.lovappy.service.product.dto.mobile;

import com.realroofers.lovappy.service.product.model.ProductType;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class CategoryItemDto {

    private Integer id;
    private String categoryName;

    public CategoryItemDto() {
    }

    public CategoryItemDto(Integer id, String categoryName) {
        this.id = id;
        this.categoryName = categoryName;
    }

    public CategoryItemDto(ProductType productType) {
        if(productType.getId()!=null) {
            this.id = productType.getId();
        }
        this.categoryName = productType.getTypeName();
    }

}
