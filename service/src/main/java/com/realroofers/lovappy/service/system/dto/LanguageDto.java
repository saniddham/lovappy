package com.realroofers.lovappy.service.system.dto;

import com.realroofers.lovappy.service.system.model.Language;
import lombok.EqualsAndHashCode;

/**
 * Created by Eias Altawil on 6/3/2017
 */
@EqualsAndHashCode
public class LanguageDto {

    private Integer id;

    private String name;
    private String abbreviation;
    private Boolean enabled;

    public LanguageDto() {
    }

    public LanguageDto(Integer id) {
        this.id = id;
    }

    public LanguageDto(Language language) {
        if(language != null) {
            this.id = language.getId();
            this.name = language.getName();
            this.abbreviation = language.getAbbreviation();
            this.enabled = language.getEnabled();
        } else {
            this.name = "ENGLISH";
            this.abbreviation = "EN";
            this.enabled = true;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
