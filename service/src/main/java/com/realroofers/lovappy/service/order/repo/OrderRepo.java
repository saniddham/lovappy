package com.realroofers.lovappy.service.order.repo;

import com.realroofers.lovappy.service.order.model.ProductOrder;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Eias Altawil on 5/11/17
 */
public interface OrderRepo extends JpaRepository<ProductOrder, Long>, QueryDslPredicateExecutor<ProductOrder> {

    List<ProductOrder> findAllByUserOrderByDateDesc(User user);

    ProductOrder findByTransactionID(String transactionID);

    /*@Query("SELECT SUM(order_detail.price) FROM ProductOrder po " +
            "  JOIN po.ordersDetails od " +
            "  ON po.id = od.order_id JOIN order_detail d ON od.detail_id = d.id " +
            "WHERE po.user = ?1 ")
    List getTotalSpentByUserID(Integer userID);*/
}
