package com.realroofers.lovappy.service.career.model;

import com.realroofers.lovappy.service.career.dto.EmploymentDto;
import com.realroofers.lovappy.service.user.model.User;

import javax.persistence.*;
import java.util.Date;
import java.util.StringJoiner;

@Entity
@Table(name = "employment")
public class Employment {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    @Column(name = "empmnt_id")
    private long id;
    @Column(name = "business_name")
    private String businessName;
    @Column(name = "job_title")
    private String jobTitle;
    private String resposibilities;
    @Column(name = "reason")
    private int reasonForLeaving;
    @Column(name = "start_month")
    private int startMonth;
    @Column(name = "start_year")
    private int startYear;
    @Column(name = "end_month")
    private int endMonth;
    @Column(name = "end_year")
    private int endYear;
    @Column(name = "still_working")
    private int stillWorking;

    public Employment(EmploymentDto entry, CareerUser user) {
            this.businessName = entry.getBusinessName();
            this.jobTitle = entry.getJobTitle();
            this.reasonForLeaving = entry.getReasonForLeaving();
            this.resposibilities = entry.getResposibilities();
            this.startMonth = entry.getStartMonth();
            this.startYear = entry.getStartYear();
            this.endMonth = entry.getEndMonth();
            this.endYear = entry.getEndYear();
            this.stillWorking = entry.getStillWorking();
            this.user = user;
    }

    public Employment() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CareerUser getUser() {
        return user;
    }

    public void setUser(CareerUser user) {
        this.user = user;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private CareerUser user;

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getResposibilities() {
        return resposibilities;
    }

    public void setResposibilities(String resposibilities) {
        this.resposibilities = resposibilities;
    }

    public int getReasonForLeaving() {
        return reasonForLeaving;
    }

    public void setReasonForLeaving(int reasonForLeaving) {
        this.reasonForLeaving = reasonForLeaving;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getStillWorking() {
        return stillWorking;
    }

    public void setStillWorking(int stillWorking) {
        this.stillWorking = stillWorking;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Employment.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("businessName='" + businessName + "'")
                .add("jobTitle='" + jobTitle + "'")
                .add("resposibilities='" + resposibilities + "'")
                .add("reasonForLeaving=" + reasonForLeaving)
                .add("startMonth=" + startMonth)
                .add("startYear=" + startYear)
                .add("endMonth=" + endMonth)
                .add("endYear=" + endYear)
                .add("stillWorking=" + stillWorking)
                .add("user=" + user)
                .toString();
    }
}
