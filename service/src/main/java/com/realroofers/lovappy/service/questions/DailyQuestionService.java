package com.realroofers.lovappy.service.questions;

import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.questions.dto.DailyQuestionDto;
import com.realroofers.lovappy.service.questions.dto.UserResponseDto;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

/**
 * Created by Darrel Rayen on 9/18/18.
 */
public interface DailyQuestionService {

    Page<DailyQuestionDto> getAllQuestions(Pageable pageable);

    Page<DailyQuestionDto> getAllQuestionsWithUserId(Integer userId, Pageable pageable);

    DailyQuestionDto findQuestionById(Long id);

    DailyQuestionDto findLatestQuestion();

    DailyQuestionDto findQuestionByDate(Date date);

    DailyQuestionDto addQuestion(DailyQuestionDto question);

    DailyQuestionDto updateQuestion(DailyQuestionDto question);

    DailyQuestionDto updateQuestion(Long id, String question);

    Boolean deleteDailyQuestion(Long id);

    Page<UserResponseDto> findResponsesByQuestionId(Long id, Pageable pageable, ApprovalStatus approvalStatus);

    Page<UserResponseDto> findResponsesByQuestionId(Long id, Pageable pageable);

    UserResponseDto findByResponseId(Long id);

    List<UserResponseDto> findUserResponseByUserId(Integer userId);

    List<UserResponseDto> findPaidUserResponseByUserId(Integer userId, Integer buyerId);

    Page<UserResponseDto> findUserResponseByUserId(Integer userId, Pageable pageable);

    Page<UserResponseDto> findApprovedUserResponseByUserId(Integer userId, Pageable pageable);

    Page<UserResponseDto> findByResponseApprovalStatus(ApprovalStatus status);

    Boolean changeResponseApprovalStatus(Long responseId, ApprovalStatus approvalStatus);

    Boolean deleteResponse(Long responseId);

    Boolean deleteResponseMobile(Long responseId, Long questionId, Integer userId);

    UserResponseDto addUserResponse(UserResponseDto userResponse);

    UserResponseDto updateUserResponse(UserResponseDto userResponse);

    UserResponseDto updateUserResponseMobile(UserResponseDto userResponse, Integer userId);

    Integer findUserResponseCountByUserId(Integer userId);

    CalculatePriceDto calculatePrice(Integer quantity, String couponCode);

    void addPurchaseRecord(List<Long> responseIds, Integer userId);
}
