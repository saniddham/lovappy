package com.realroofers.lovappy.service.blog.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.realroofers.lovappy.service.blog.model.CategoryPostCount;
import com.realroofers.lovappy.service.blog.repo.PostRepo;
import com.realroofers.lovappy.service.cloud.repo.CloudStorageRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.realroofers.lovappy.service.blog.CategoryService;
import com.realroofers.lovappy.service.blog.dto.CategoryDto;
import com.realroofers.lovappy.service.blog.model.Category;
import com.realroofers.lovappy.service.blog.model.QCategory;
import com.realroofers.lovappy.service.blog.repo.CategoryRepo;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepo categoryRepo;

    @Autowired
    private CloudStorageRepo cloudStorageRepo;

    @Transactional
    public void save(Category category) {
        categoryRepo.save(category);
    }


    @Transactional
    @Override
    public Category get(Integer id) {
        return categoryRepo.findOne(id);
    }


    @Transactional
    @Override
    public CategoryDto getOne(Integer id) {
        return new CategoryDto(categoryRepo.findOne(id));
    }

    
    @Transactional
    @Override
    public void update(Category category) {
        categoryRepo.save(category);
    }
    
    @Transactional
    @Override
    public void delete(Category category) {
    		categoryRepo.delete(category);
    }

    @Transactional
    @Override
    public void delete(Integer id) {
   		categoryRepo.delete(categoryRepo.getOne(id));
    }

    @Transactional
    @Override
    public void addImage(Integer id, Long fileId) {
        Category category = categoryRepo.findOne(id);
        category.getImages().add(cloudStorageRepo.findOne(fileId));
        categoryRepo.save(category);
    }

    @Transactional
    @Override
    public void deleteImage(Integer id, Long fileId) {
        Category category = categoryRepo.findOne(id);
        category.getImages().remove(cloudStorageRepo.findOne(fileId));
        categoryRepo.save(category);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<CategoryPostCount> getCategoryPostsCount(int page, int limit) {
        Pageable pageRequest = new PageRequest(page-1, limit, new Sort(Sort.Direction.ASC, "category.name"));
        return categoryRepo.findCategoryPostsCount(pageRequest);
    }

    @Transactional(readOnly = true)
    public List<Category> getAllCategories() {
        return categoryRepo.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public Page<CategoryDto> getAllCategories(int page, int limit) {
        return categoryRepo.findAll(new PageRequest(page - 1, limit)).map(CategoryDto::new);
    }


    @Transactional(readOnly = true)
    @Override
    public List<CategoryDto> findAll() {
        return categoryRepo.findAll().stream().map(CategoryDto::new ).collect(Collectors.toList());
    }

    


	@Override
    @Transactional(readOnly = true)
	public Page<CategoryDto> findAll(Pageable pageable) {
    	Page<CategoryDto>  categories= categoryRepo.findAll(pageable).map(CategoryDto::new);
		return categories;
	}




    @Transactional(readOnly = true)
    @Override
    public List<CategoryDto> findActive() {
    	List<CategoryDto> result = new ArrayList<CategoryDto>();
    	QCategory qCategory = QCategory.category;
    	List<Category> cats =Lists.newArrayList(categoryRepo.findAll(qCategory.catState.equalsIgnoreCase("ACTIVE")));
    	if(cats!=null){
    		for (Category cat : cats) {
    			result.add(new CategoryDto(cat));
    		}
    	}
    	return result;

    	
    }


    @Transactional
    @Override
    public int deactivate(Integer categoryId) {
    	Category category=categoryRepo.findOne(categoryId);
    	if (category==null){
    		return 0;
    	} else {
    		category.setCatState("INACTIVE");
    		categoryRepo.save(category);
    	}
    	//updated success
    	return 1;
    }




    @Transactional
    @Override
    public List<CategoryDto> findCategory(String name) {
        List<CategoryDto> result=new ArrayList<CategoryDto>();
    	List<Category> cats =Lists.newArrayList(categoryRepo.findByName(name));
    	if(cats!=null){
    		for (Category cat : cats) {
    			result.add(new CategoryDto(cat));
    		}
    	}
    	return result;
    }

    
    @Transactional
    @Override
    public int activate(Integer categoryId) {
    	Category category=categoryRepo.findOne(categoryId);
    	if (category==null){
    		return 0;
    	} else {
    		category.setCatState("ACTIVE");
    		categoryRepo.save(category);
    	}
    	//updated success
    	return 1;
    }


    @Transactional(readOnly = true)
    @Override
	public Page<CategoryDto> find(String name, Pageable pageable) {
    	PageRequest request=new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort(new Sort.Order(Sort.Direction.DESC, "createdAt")));
    	QCategory qPost = QCategory.category;
    	Page<CategoryDto>  categories= categoryRepo.findAll(
				qPost.name.eq(name),request).map(CategoryDto::new);
    	return categories;
    }
}
