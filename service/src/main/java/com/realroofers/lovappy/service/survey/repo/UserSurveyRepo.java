package com.realroofers.lovappy.service.survey.repo;

import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.survey.model.UserSurvey;
import com.realroofers.lovappy.service.survey.support.SurveyStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by hasan on 9/8/2017.
 */
public interface UserSurveyRepo extends PagingAndSortingRepository<UserSurvey, Integer>,
        QueryDslPredicateExecutor<UserSurvey> {

    UserSurvey findByUser(User user);
    Page<UserSurvey> findAllBySurveyStatus(Pageable pageable, SurveyStatus surveyStatus);
}
