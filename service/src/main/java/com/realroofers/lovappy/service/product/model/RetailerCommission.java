package com.realroofers.lovappy.service.product.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.product.dto.RetailerCommissionDto;
import com.realroofers.lovappy.service.user.model.Country;
import com.realroofers.lovappy.service.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "retailer_commission")
@EqualsAndHashCode
public class RetailerCommission extends BaseEntity<Integer> implements Serializable {

    @Column(name = "percentage")
    private Double commissionPercentage;

    @Column(name = "affective_date")
    private Date affectiveDate;

    @ManyToOne
    @JoinColumn(name = "country")
    private Country country;

    @ManyToOne
    @JoinColumn(name = "created_by")
    private User createdBy;

    @ManyToOne
    @JoinColumn(name = "modified_by")
    private User modifiedBy;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;

    public RetailerCommission() {
    }

    public RetailerCommission(RetailerCommissionDto retailerCommissionDto) {
        if (retailerCommissionDto.getId() != null) {
            this.setId(retailerCommissionDto.getId());
        }
        this.commissionPercentage = retailerCommissionDto.getCommissionPercentage();
        this.affectiveDate = retailerCommissionDto.getAffectiveDate();
        this.country = retailerCommissionDto.getCountry();
        this.createdBy = retailerCommissionDto.getCreatedBy() != null ? new User(retailerCommissionDto.getCreatedBy().getID()) : null;
        this.modifiedBy = retailerCommissionDto.getModifiedBy() != null ? new User(retailerCommissionDto.getModifiedBy().getID()) : null;
        this.active = retailerCommissionDto.getActive();
        this.setUpdated(retailerCommissionDto.getUpdated());
    }
}
