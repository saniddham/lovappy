package com.realroofers.lovappy.service.datingPlaces.dto.mobile;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.datingPlaces.dto.*;
import com.realroofers.lovappy.service.datingPlaces.model.*;
import com.realroofers.lovappy.service.datingPlaces.support.DatingAgeRange;
import com.realroofers.lovappy.service.datingPlaces.support.NeighborHood;
import com.realroofers.lovappy.service.datingPlaces.support.PriceRange;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by Darrel Rayen on 10/17/17.
 */
@Data
@EqualsAndHashCode
public class AddDatingPlaceMobileDto implements Serializable {

    private String datingPlacesId;
    private String placeName;
    //private CloudStorageFileDto coverPicture;
    private String country;
    private String city;
    private String addressLine;
    private String addressLineTwo;
    private String zipCode;
    private String createEmail;
    private Gender gender;
    private DatingAgeRange ageRange;
    private PriceRange priceRange;
    private NeighborHood neighborHood;
    private Date createdDate;
    private Date deletedDate;
    private String state;
    private String userType;
    private ApprovalStatus placeApprovalStatus;
    private Double latitude;
    private Double longitude;
    //private List<PlacesAttachmentDto> placesAttachmentDtos = new ArrayList<>();
    private String placeDescription;
    //private UserDto owner;

    private Boolean isFeatured;
    private Boolean isMapFeatured;
    private Boolean isLovStampFeatured;
    private Boolean approved;
    private Boolean isPetsAllowed;
    private Boolean isGoodForMarriedCouple;
    private Boolean isClaimed;
    private Collection<FoodTypeDto> foodTypes;
    private Collection<PlaceAtmosphereDto> atmospheres;
    private Collection<PersonTypeDto> personTypes;
    private List<DatingPlaceReviewMobileDto> datingPlaceReviews;

    public AddDatingPlaceMobileDto() {
    }

/*    public AddDatingPlaceMobileDto(DatingPlace datingPlace) {
        this.datingPlacesId = String.valueOf(datingPlace.getDatingPlacesId());
        this.placeName = datingPlace.getPlaceName();
        //this.coverPicture = getCoverImageDto(datingPlace.getCoverPicture());
        this.country = datingPlace.getCountry();
        this.city = datingPlace.getCity();
        this.state = datingPlace.getState();
        this.addressLine = datingPlace.getAddressLine();
        this.addressLineTwo = datingPlace.getAddressLineTwo();
        this.zipCode = datingPlace.getZipCode();
        this.createEmail = datingPlace.getCreateEmail();
        this.isFeatured = datingPlace.getFeatured() == null ? false : datingPlace.getFeatured();
        this.gender = datingPlace.getGender();
        this.ageRange = datingPlace.getAgeRange();
        this.priceRange = datingPlace.getPriceRange();
        this.createdDate = datingPlace.getCreatedDate();
        this.deletedDate = datingPlace.getDeletedDate();
        this.placeApprovalStatus = datingPlace.getPlaceApprovalStatus();
        this.approved = datingPlace.getApproved() == null ? false : datingPlace.getApproved();
        //this.placesAttachmentDtos = getPlacesImageList(datingPlace.getPlacesAttachments());
        this.latitude = datingPlace.getLatitude();
        this.longitude = datingPlace.getLongitude();
        this.overAllRating = datingPlace.getAverageRating();
        this.isMapFeatured = datingPlace.getMapFeatured() == null ? false : datingPlace.getMapFeatured();
        this.isLovStampFeatured = datingPlace.getLoveStampFeatured() == null ? false : datingPlace.getLoveStampFeatured();
        this.placeDescription = datingPlace.getPlaceDescription();
        this.securityAverage = datingPlace.getSecurityAverage();
        this.parkingAverage = datingPlace.getParkingAverage();
        this.transportAverage = datingPlace.getTransportAverage();
        this.googleAverage = datingPlace.getGoogleAverage();
        this.maleCount = datingPlace.getSuitableForMaleCount();
        this.femaleCount = datingPlace.getSuitableForFemale();
        this.bothCount = datingPlace.getSuitableForBoth();
        this.neighborHood = datingPlace.getNeighborHood();
        this.isGoodForMarriedCouple = datingPlace.getGoodForMarriedCouple();
        this.isPetsAllowed = datingPlace.getPetsAllowed();
        this.foodTypes = getFoodTypeDtoList(datingPlace.getFoodType());
        this.atmospheres = getPlaceAtmosphereDtos(datingPlace.getPlaceAtmosphere());
        this.isClaimed = datingPlace.getClaimed() == null ? false : datingPlace.getClaimed();
        //this.owner = new UserDto(datingPlace.getOwner());
        this.personTypes = getPersonTypeDtos(datingPlace.getPersonTypes());
        //this.placeReviewDtos = getPlacesReviewList(datingPlace.getDatingPlaceReviews());
    }*/

    private CloudStorageFileDto getCoverImageDto(CloudStorageFile cloudStorageFile) {
        return new CloudStorageFileDto(cloudStorageFile);
    }

    private List<DatingPlaceReviewMobileDto> getPlacesReviewListFromDto(List<DatingPlaceReviewDto> datingPlaceReviewList) {
        List<DatingPlaceReviewMobileDto> datingPlaceReviewDtos = new ArrayList<>();
        datingPlaceReviewList.forEach(placeReview -> {
            datingPlaceReviewDtos.add(new DatingPlaceReviewMobileDto(placeReview));
        });
        return datingPlaceReviewDtos;
    }

    private List<DatingPlaceReviewMobileDto> getPlacesReviewList(List<DatingPlaceReview> datingPlaceReviewList) {
        List<DatingPlaceReviewMobileDto> datingPlaceReviewDtos = new ArrayList<>();
        datingPlaceReviewList.forEach(placeReview -> {
            datingPlaceReviewDtos.add(new DatingPlaceReviewMobileDto(placeReview));
        });
        return datingPlaceReviewDtos;
    }

    private Collection<FoodTypeDto> getFoodTypeDtoList(Collection<FoodType> foodTypes) {
        Collection<FoodTypeDto> foodTypeDtos = new ArrayList<>();
        foodTypes.forEach(foodType -> {
            foodTypeDtos.add(new FoodTypeDto(foodType));
        });
        return foodTypeDtos;
    }

    private Collection<PlaceAtmosphereDto> getPlaceAtmosphereDtos(Collection<PlaceAtmosphere> placeAtmospheres) {
        Collection<PlaceAtmosphereDto> placeAtmosphereDtos = new ArrayList<>();
        placeAtmospheres.forEach(placeAtmosphere -> {
            placeAtmosphereDtos.add(new PlaceAtmosphereDto(placeAtmosphere));
        });
        return placeAtmosphereDtos;
    }

    private Collection<PersonTypeDto> getPersonTypeDtos(Collection<PersonType> personTypes) {
        Collection<PersonTypeDto> personTypeDtos = new ArrayList<>();
        personTypes.forEach(personType -> {
            personTypeDtos.add(new PersonTypeDto(personType));
        });
        return personTypeDtos;
    }

    private List<PlacesAttachmentDto> getPlacesImageList(List<PlacesAttachment> placesAttachmentList) {
        List<PlacesAttachmentDto> placesAttachmentDtoList = new ArrayList<>();
        placesAttachmentList.forEach(eventLocationPicture -> {
            placesAttachmentDtoList.add(new PlacesAttachmentDto(eventLocationPicture.getAttachmentId(), eventLocationPicture.getUrl(), new CloudStorageFileDto(eventLocationPicture.getPicture()), eventLocationPicture.getCreatedDate(), eventLocationPicture.getImageDescription()));
        });
        return placesAttachmentDtoList;
    }
}
