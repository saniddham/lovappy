package com.realroofers.lovappy.service.privatemessage.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.privatemessage.model.PrivateMessage;
import com.realroofers.lovappy.service.privatemessage.support.Status;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * Created by Eias Altawil on 5/7/17
 */
@EqualsAndHashCode
public class PrivateMessageDto {

    private Integer id;
    private UserDto fromUser;
    private UserDto toUser;
    private String fileName;
    private CloudStorageFileDto audioFileCloud;
    private LanguageDto language;
    private Date sentAt;
    private Status status;

    public PrivateMessageDto() {}

    public PrivateMessageDto(PrivateMessage privateMessage) {
        if(privateMessage != null) {
            this.id = privateMessage.getId();
            this.fromUser = new UserDto(privateMessage.getFromUser());
            this.toUser = new UserDto(privateMessage.getToUser());
            this.audioFileCloud = new CloudStorageFileDto(privateMessage.getAudioFileCloud());
            this.language = new LanguageDto(privateMessage.getLanguage());
            this.sentAt = privateMessage.getSentAt();
            this.status = privateMessage.getStatus();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserDto getFromUser() {
        return fromUser;
    }

    public void setFromUser(UserDto fromUser) {
        this.fromUser = fromUser;
    }

    public UserDto getToUser() {
        return toUser;
    }

    public void setToUser(UserDto toUser) {
        this.toUser = toUser;
    }

    public CloudStorageFileDto getAudioFileCloud() {
        return audioFileCloud;
    }

    public void setAudioFileCloud(CloudStorageFileDto audioFileCloud) {
        this.audioFileCloud = audioFileCloud;
    }

    public LanguageDto getLanguage() {
        return language;
    }

    public void setLanguage(LanguageDto language) {
        this.language = language;
    }

    public Date getSentAt() {
        return sentAt;
    }

    public void setSentAt(Date sentAt) {
        this.sentAt = sentAt;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
