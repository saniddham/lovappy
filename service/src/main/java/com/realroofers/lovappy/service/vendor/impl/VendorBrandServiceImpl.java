package com.realroofers.lovappy.service.vendor.impl;

import com.realroofers.lovappy.service.vendor.VendorBrandService;
import com.realroofers.lovappy.service.vendor.dto.VendorBrandDto;
import com.realroofers.lovappy.service.vendor.model.VendorBrand;
import com.realroofers.lovappy.service.vendor.repo.VendorBrandRepo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class VendorBrandServiceImpl implements VendorBrandService {

    private VendorBrandRepo vendorBrandRepo;

    public VendorBrandServiceImpl(VendorBrandRepo vendorBrandRepo) {
        this.vendorBrandRepo = vendorBrandRepo;
    }

    @Transactional(readOnly = true)
    @Override
    public List<VendorBrandDto> findAll() {
        List<VendorBrandDto> list = new ArrayList<>();
        vendorBrandRepo.findAll().stream().forEach(vendorBrand -> list.add(new VendorBrandDto(vendorBrand)));
        return list;
    }

    @Transactional(readOnly = true)
    @Override
    public List<VendorBrandDto> findAllActive() {
        List<VendorBrandDto> list = new ArrayList<>();
        vendorBrandRepo.findAll().stream().forEach(vendorBrand -> list.add(new VendorBrandDto(vendorBrand)));
        return list;
    }

    @Transactional
    @Override
    public VendorBrandDto create(VendorBrandDto vendorBrandDto) throws Exception {
        return new VendorBrandDto(vendorBrandRepo.saveAndFlush(new VendorBrand(vendorBrandDto)));
    }

    @Transactional
    @Override
    public VendorBrandDto update(VendorBrandDto vendorBrandDto) throws Exception {
        return new VendorBrandDto(vendorBrandRepo.saveAndFlush(new VendorBrand(vendorBrandDto)));
    }

    @Override
    public void delete(Integer integer) throws Exception {

    }

    @Transactional(readOnly = true)
    @Override
    public VendorBrandDto findById(Integer integer) {
        VendorBrand brand = vendorBrandRepo.findOne(integer);
        return brand != null ? new VendorBrandDto(brand) : null;
    }
}
