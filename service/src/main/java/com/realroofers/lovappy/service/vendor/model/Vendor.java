package com.realroofers.lovappy.service.vendor.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.user.model.Country;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.vendor.dto.VendorDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Manoj on 31/01/2018.
 */
@Entity
@Data
@EqualsAndHashCode
@Table(name = "vendor")
public class Vendor extends BaseEntity<Integer> implements Serializable {

    @Enumerated
    @Column(name = "vendor_type")
    private VendorType vendorType;

    @Enumerated
    @Column(name = "verification_type")
    private VerificationType verificationType;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "primary_contact_person")
    private String primaryContactPerson;

    @Column(name = "primary_contact_number")
    private String primaryContactNumber;

    @Column(name = "primary_contact_email")
    private String primaryContactEmail;

    @Column(name = "alternate_contact_person")
    private String alternateContactPerson;

    @Column(name = "alternate_contact_email")
    private String alternateContactEmail;

    @Column(name = "contact_number")
    private String contactNumber;

    @Column(name = "address_line1")
    private String addressLine1;

    @Column(name = "address_line2")
    private String addressLine2;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "zip_code")
    private String zipCode;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "company_logo")
    private CloudStorageFile companyLogo;

    @Column(name = "is_approved", columnDefinition = "boolean default false", nullable = false)
    private Boolean approved = false;

    @Column(name = "registration_step_1", columnDefinition = "boolean default false", nullable = false)
    private Boolean registrationStep1 = false;

    @Column(name = "registration_step_2", columnDefinition = "boolean default false", nullable = false)
    private Boolean registrationStep2 = false;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;

    @Column(name = "mobile_verification_code")
    private String mobileVerificationCode;

    @JsonIgnore
    @JoinColumn(name = "user")
    @ManyToOne
    private User user;

    @JoinColumn(name = "country")
    @ManyToOne
    private Country country;

    public Vendor() {
    }

    public Vendor(VendorDto vendor) {
        if (vendor.getId() != null) {
            this.setId(vendor.getId());
        }
        this.vendorType = vendor.getVendorType();
        this.verificationType = vendor.getVerificationType();
        this.companyName = vendor.getCompanyName();
        this.primaryContactPerson = vendor.getPrimaryContactPerson();
        this.primaryContactNumber = vendor.getPrimaryContactNumber();
        this.primaryContactEmail = vendor.getPrimaryContactEmail();
        this.alternateContactPerson = vendor.getAlternateContactPerson();
        this.alternateContactEmail = vendor.getAlternateContactEmail();
        this.contactNumber = vendor.getContactNumber();
        this.addressLine1 = vendor.getAddressLine1();
        this.addressLine2 = vendor.getAddressLine2();
        this.state = vendor.getState();
        this.city = vendor.getCity();
        this.zipCode = vendor.getZipCode();
        if (vendor.getCompanyLogo() != null && vendor.getCompanyLogo().getId() != null) {
            this.companyLogo = new CloudStorageFile(vendor.getCompanyLogo().getId());
        }
        this.mobileVerificationCode = vendor.getMobileVerificationCode();
        if (vendor.getApproved() != null) {
            this.approved = vendor.getApproved();
        }
        if (vendor.getRegistrationStep1() != null) {
            this.registrationStep1 = vendor.getRegistrationStep1();
        }
        if (vendor.getRegistrationStep2() != null) {
            this.registrationStep2 = vendor.getRegistrationStep2();
        }
        if (vendor.getActive() != null) {
            this.active = vendor.getActive();
        }
        if (vendor.getUser() != null) {
            this.user = new User(vendor.getUser().getID());
        }
        this.country = vendor.getCountry();
    }

}
