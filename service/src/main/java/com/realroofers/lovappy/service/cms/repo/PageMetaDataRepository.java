package com.realroofers.lovappy.service.cms.repo;

import com.realroofers.lovappy.service.cms.model.Page;
import com.realroofers.lovappy.service.cms.model.PageMetaData;
import com.realroofers.lovappy.service.system.model.Language;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface PageMetaDataRepository extends JpaRepository<PageMetaData, Integer> {

    PageMetaData findByPageAndLanguage(Page page, Language language);

    Set<PageMetaData> findByPage(Page page);


}
