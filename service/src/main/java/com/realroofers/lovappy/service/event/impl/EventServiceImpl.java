package com.realroofers.lovappy.service.event.impl;

import com.google.maps.model.LatLng;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.cloud.repo.CloudStorageRepo;
import com.realroofers.lovappy.service.core.SocialType;
import com.realroofers.lovappy.service.event.EventService;
import com.realroofers.lovappy.service.event.dto.*;
import com.realroofers.lovappy.service.event.dto.mobile.AddEventMobileDto;
import com.realroofers.lovappy.service.event.dto.mobile.EventMobileDto;
import com.realroofers.lovappy.service.event.model.*;
import com.realroofers.lovappy.service.event.repo.*;
import com.realroofers.lovappy.service.event.support.EventDateUtil;
import com.realroofers.lovappy.service.event.support.EventStatus;
import com.realroofers.lovappy.service.event.support.UserEventStatus;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.CouponDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.util.EventLocationUtil;
import com.realroofers.lovappy.service.util.ListMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Tejaswi Venupalli on 8/29/17
 */

@Service
public class EventServiceImpl implements EventService {


    private static final Logger LOG = LoggerFactory.getLogger(EventServiceImpl.class);
    private EventRepo eventRepo;
    private EventCategoryRepo eventCategoryRepo;
    private EventUserRepo eventUserRepo;
    private CloudStorageRepo cloudStorageRepo;
    private EventLocationUtil eventLocationUtil;
    private UserRepo userRepo;
    private EventLocationPictureRepo eventLocationPictureRepo;
    private EventUserPictureRepo eventUserPictureRepo;
    private final OrderService orderService;

    @Autowired
    public EventServiceImpl(EventRepo eventRepo, EventCategoryRepo eventCategoryRepo, EventUserRepo eventUserRepo, CloudStorageRepo cloudStorageRepo,
                            EventLocationUtil eventLocationUtil, UserRepo userRepo, EventUserPictureRepo eventUserPictureRepo, EventLocationPictureRepo eventLocationPictureRepo, OrderService orderService) {
        this.eventRepo = eventRepo;
        this.eventCategoryRepo = eventCategoryRepo;
        this.eventUserRepo = eventUserRepo;
        this.cloudStorageRepo = cloudStorageRepo;
        this.eventLocationUtil = eventLocationUtil;
        this.userRepo = userRepo;
        this.eventUserPictureRepo = eventUserPictureRepo;
        this.eventLocationPictureRepo = eventLocationPictureRepo;
        this.orderService = orderService;
    }

    @Override
    @Transactional(readOnly = true)
    public EventDto findEventById(Integer id) {
        Event event = eventRepo.findOne(id);
        if (event != null) {
            EventDto dto = new EventDto(event);
            return dto;
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public EventMobileDto findMobileEventById(Integer eventId, Integer userId) {
        List<String> urlList = new ArrayList<>();
        Event event = eventRepo.findOne(eventId);
        User user = userRepo.findOne(userId);
        EventUserId eventUser = new EventUserId(event, user);
        EventUser one = eventUserRepo.findOne(eventUser);
        List<EventUserPicture> userPictureList = event.getEventPictures();
        userPictureList.forEach(eventUserPicture -> {
            urlList.add(eventUserPicture.getPicture().getUrl());
        });
        EventMobileDto mobileDto = new EventMobileDto(event);
        mobileDto.setAttendeesCount(eventUserRepo.findNoOfUserAttendees(eventId) + event.getOutsiderCount());
        mobileDto.setEventUserPictureUrl(urlList);
        if (one != null) {
            mobileDto.setPaid(true);
        } else {
            mobileDto.setPaid(false);
        }
        return mobileDto;
    }

    @Override
    @Transactional(readOnly = true)
    public List<EventDto> getEventsInNextWeek() {
        List<EventDto> eventDtoList = new ArrayList<>();
        List<Event> eventList = eventRepo.findAll();

        eventList.forEach(event -> {
            eventDtoList.add(new EventDto(event));
        });
        return eventDtoList;
    }

    @Override
    @Transactional(readOnly = true)
    public List<EventMobileDto> findMobileAllActiveEvents() {
        List<Event> eventList = eventRepo.findAll();
        List<EventMobileDto> mobileDtos = new ArrayList<>();
        eventList.forEach(event -> {
            mobileDtos.add(new EventMobileDto(event));
        });
        return mobileDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventMobileDto> findMobileAllActiveEventsPaginated(Pageable pageable, Integer userId) {
        Page<Object[]> eventPage = eventRepo.findByDateMobile(EventStatus.ACTIVE, ApprovalStatus.APPROVED, userId, pageable);
        return eventPage.map(EventMobileDto::new);
    }

    @Override
    public Page<EventMobileDto> findMobileAllActiveEventsByDate(Pageable pageable, Integer userId) {
        Page<Object[]> eventPage = eventRepo.findAllByEventApprovalStatusOrderByEventDateDesc(EventStatus.ACTIVE,
                ApprovalStatus.APPROVED, userId, pageable);
        return eventPage.map(EventMobileDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Event> getEventTitlesByCategory(String categoryName) {
        return eventRepo.findEventTitlesByCategory(categoryName);
    }

    @Override
    @Transactional(readOnly = true)
    public Integer getNoOfUserAttendees(Integer eventId) {
        Event event = eventRepo.findOne(eventId);
        return eventUserRepo.findNoOfUserAttendees(eventId) + event.getOutsiderCount();
    }

    @Override
    @Transactional(readOnly = true)
    public Integer getNoOfPaidAttendees(Integer eventId) {
        return eventUserRepo.findNoOfUserAttendees(eventId);
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isRegisteredForEvent(Integer eventId, Integer userId) {

        if (eventId == null || userId == null)
            return false;

        Event event = eventRepo.findOne(eventId);
        User user = userRepo.findOne(userId);
        EventUser eventUser = eventUserRepo.findOne(new EventUserId(event, user));
        return eventUser != null;

    }

    @Transactional(readOnly = true)
    @Override
    public EventDto updateStatus(Integer eventId, EventStatus status) {
        Event event = eventRepo.findOne(eventId);
        event.setEventStatus(status);
        return new EventDto(event);
    }


    @Override
    @Transactional
    public EventDto addEvent(EventDto eventDto) {
        Event event = new Event();

        event.setEventTitle(eventDto.getEventTitle());
        event.setEventDescription(eventDto.getEventDescription());
        event.setAmbassadorUserId(eventDto.getAmbassadorUserId());
        event.setOutsiderCount(eventDto.getOutsiderCount());
        event.setAdmissionCost(eventDto.getAdmissionCost());
        event.setEventDate(eventDto.getEventDate());
        event.setTimeZone(eventDto.getTimeZone());
        event.setEventStatus(EventStatus.INACTIVE);
        event.setEventApprovalStatus(ApprovalStatus.PENDING);

        EventLocationDto loc = eventDto.getEventLocation();
        if (loc != null && loc.getLongitude() != null && loc.getLatitude() != null) {
            LatLng location = new LatLng(loc.getLatitude(), loc.getLongitude());

            event.setEventLocation(EventLocationDto.toEventLocation(eventDto.getEventLocation()));
        }
        Collection<EventCategory> categories = new ArrayList<>();
        String[] cList = eventDto.getEventCategories().split(",");
        for (String category : cList) {
            categories.add(eventCategoryRepo.findByCategoryName(category));
        }
        event.setEventCategories(categories);
        event.setTargetedAudience(eventDto.getTargetedAudience());

        event.setStartTime(EventDateUtil.convertToDate(eventDto.getStartTime(), eventDto.getEventDate()));
        event.setFinishTime(EventDateUtil.convertToDate(eventDto.getFinishTime(), eventDto.getEventDate()));

        if (eventDto.getLocationPictures().size() > 0 && (eventDto.getLocationPictures().get(0).getPicture().getId() != null)) {
            EventLocationPicture pic = eventDto.getLocationPictures().get(0).getEventLocationPicture();
            pic.setPicture(cloudStorageRepo.findOne(eventDto.getLocationPictures().get(0).getPicture().getId()));
            event.getLocationPictures().add(pic);
            pic.setEventId(event);
        }

        Event result = eventRepo.save(event);
        return result == null ? null : new EventDto(result);

    }

    @Override
    @Transactional
    public EventMobileDto addMobileEvent(AddEventMobileDto eventDto) {
        Event event = null;
        if (eventDto.getEventId() != null) {
            event = eventRepo.findOne(eventDto.getEventId());
        } else {
            event = new Event();
        }
        event.setEventTitle(eventDto.getEventTitle());
        event.setEventDescription(eventDto.getEventDescription());
        event.setAmbassadorUserId(eventDto.getAmbassadorUserId());
        event.setOutsiderCount(eventDto.getOutsiderCount());
        event.setAdmissionCost(eventDto.getAdmissionCost());
        event.setEventDate(eventDto.getEventDate());
        event.setTimeZone(eventDto.getTimeZone());
        event.setEventStatus(EventStatus.INACTIVE);
        event.setEventApprovalStatus(ApprovalStatus.PENDING);
        event.setApproved(false);

        EventLocationDto loc = eventDto.getEventLocation();
        if (loc != null && loc.getLongitude() != null && loc.getLatitude() != null) {
            event.setEventLocation(EventLocationDto.toEventLocation(eventDto.getEventLocation()));
        }
        Collection<EventCategory> categories = new ArrayList<>();
        Collection<EventCategoryDto> categoryDtos = eventDto.getEventCategoryDtos();
        categoryDtos.forEach(eventCategoryDto -> {
            categories.add(eventCategoryRepo.findByCategoryName(eventCategoryDto.getCategoryName()));
        });
        event.setEventCategories(categories);
        event.setTargetedAudience(eventDto.getTargetedAudience());
        event.setStartTime(EventDateUtil.convertToDate(eventDto.getStartTime(), eventDto.getEventDate()));
        event.setFinishTime(EventDateUtil.convertToDate(eventDto.getFinishTime(), eventDto.getEventDate()));

/*        if (eventDto.getLocationPictures().size() > 0 && (eventDto.getLocationPictures().get(0).getPicture().getId() != null)) {
            EventLocationPicture pic = eventDto.getLocationPictures().get(0).getEventLocationPicture();
            pic.setPicture(cloudStorageRepo.findOne(eventDto.getLocationPictures().get(0).getPicture().getId()));
            event.getLocationPictures().add(pic);
            pic.setEventId(event);
        }*/
        Event result = eventRepo.save(event);
        return new EventMobileDto(result);
    }

    @Override
    @Transactional
    public EventDto updateEvent(EventDto eventDto) {
        Event event = eventRepo.findOne(eventDto.getEventId());

        if (eventDto.getEventTitle() != null) {
            event.setEventTitle(eventDto.getEventTitle());
        }
        if (eventDto.getEventDescription() != null) {
            event.setEventDescription(eventDto.getEventDescription());
        }
        if (eventDto.getEventLocationDescription() != null) {
            event.setEventLocationDescription(eventDto.getEventLocationDescription());
        }
        if (eventDto.getAttendeesLimit() != null) {
            event.setAttendeesLimit(eventDto.getAttendeesLimit());
        }
        if (eventDto.getAdmissionCost() != null) {
            event.setAdmissionCost(eventDto.getAdmissionCost());
        }
        if (eventDto.getOutsiderCount() != null) {
            event.setOutsiderCount(eventDto.getOutsiderCount());
        }
        if (eventDto.getEventStatus() != null) {
            event.setEventStatus(eventDto.getEventStatus());
        }
        if (eventDto.getEventDate() != null) {
            event.setEventDate(eventDto.getEventDate());
        }
        if (eventDto.getStartTime() != null) {
            event.setStartTime(EventDateUtil.convertToDate(eventDto.getStartTime(), eventDto.getEventDate()));
        }
        if (eventDto.getFinishTime() != null) {
            event.setFinishTime(EventDateUtil.convertToDate(eventDto.getFinishTime(), eventDto.getEventDate()));
        }
        if (eventDto.getApproved() != null) {
            event.setApproved(eventDto.getApproved());
        }
        if (eventDto.getEventAprrovalStatus() != null) {
            event.setEventApprovalStatus(eventDto.getEventAprrovalStatus());
        }
        if (eventDto.getEventLocation() != null) {
            EventLocationDto loc = eventDto.getEventLocation();
            if (loc != null && loc.getLongitude() != null && loc.getLatitude() != null) {
                LatLng location = new LatLng(loc.getLatitude(), loc.getLongitude());
                event.setEventLocation(EventLocationDto.toEventLocation(eventDto.getEventLocation()));
            }
        }
        Event result = eventRepo.saveAndFlush(event);
        return result == null ? null : new EventDto(result);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventDto> getAll(int evalPage, Integer pageSize) {
        Sort sort = new Sort(Sort.Direction.DESC, "eventDate");
        Page<Event> all = eventRepo.findAll(new PageRequest(evalPage, pageSize, sort));
        return all.map(EventDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventDto> getAllEvents(Pageable pageable) {

        Page<Event> eventDtos = eventRepo.findAllByOrderByEventIdDesc(pageable);
        return eventDtos.map(EventDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EventDto> getNearByEventsByDate(EventSearchDto eventSearchDto) {

        Date fromDate = EventDateUtil.getDateOnly();
        Calendar cal = Calendar.getInstance();
        cal.setTime(fromDate);
        cal.add(Calendar.DAY_OF_MONTH, 7);
        Date toDate = cal.getTime();

        List<EventDto> list = eventRepo.findNearByEvents(eventSearchDto.getLatitude(), eventSearchDto.getLongitude(), eventSearchDto.getRadius(), fromDate, toDate)
                .stream()
                .filter(event -> {
                    Function<Double, Double> toRadian = (n) -> n * Math.PI / 180;
                    Double lat1 = event.getEventLocation().getLatitude();
                    Double lon1 = event.getEventLocation().getLongitude();
                    Double lat2 = eventSearchDto.getLatitude();
                    Double lon2 = eventSearchDto.getLongitude();
                    int R = 3959; // Earths mean radius in miles. See https://en.wikipedia.org/wiki/Earth_radius
                    double φ1 = toRadian.apply(lat1);
                    double φ2 = toRadian.apply(lat2);
                    double Δφ = toRadian.apply(lat2 - lat1);
                    double Δλ = toRadian.apply(lon2 - lon1);
                    double a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
                            Math.cos(φ1) * Math.cos(φ2) *
                                    Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
                    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                    double d = R * c;
                    return d <= eventSearchDto.getRadius();
                })
                .map(event -> {
                    EventDto eventDto = new EventDto(event.getEventId(), event.getEventLocation(), EventDateUtil.convertToString(event.getStartTime()), event.getEventDate(), event.getEventTitle());
                    //eventDto.setLocationPictures(eventDto.getImageList(event.getLocationPictures()));
                    return eventDto;
                })
                .collect(Collectors.toList());
        return list;
    }

    @Override
    @Transactional(readOnly = true)
    public List<EventMobileDto> getNearByEvents(EventSearchDto eventSearchDto) {
        List<EventMobileDto> list = eventRepo.findNearByEventsList(eventSearchDto.getLatitude(), eventSearchDto.getLongitude(), eventSearchDto.getRadius())
                .stream()
                .filter(event -> {
                    Function<Double, Double> toRadian = (n) -> n * Math.PI / 180;
                    Double lat1 = event.getEventLocation().getLatitude();
                    Double lon1 = event.getEventLocation().getLongitude();
                    Double lat2 = eventSearchDto.getLatitude();
                    Double lon2 = eventSearchDto.getLongitude();
                    int R = 3959; // Earths mean radius in miles. See https://en.wikipedia.org/wiki/Earth_radius
                    double φ1 = toRadian.apply(lat1);
                    double φ2 = toRadian.apply(lat2);
                    double Δφ = toRadian.apply(lat2 - lat1);
                    double Δλ = toRadian.apply(lon2 - lon1);
                    double a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
                            Math.cos(φ1) * Math.cos(φ2) *
                                    Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
                    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                    double d = R * c;
                    return d <= eventSearchDto.getRadius();
                })
                .map(event -> {
                    EventMobileDto eventDto = new EventMobileDto(event);
                    return eventDto;
                })
                .collect(Collectors.toList());
        return list;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventDto> getNearByEventsPage(EventSearchDto eventSearchDto, Pageable pageable) {
        Date fromDate = EventDateUtil.getDateOnly();
        Calendar cal = Calendar.getInstance();
        cal.setTime(fromDate);
        cal.add(Calendar.DAY_OF_MONTH, 7);
        Date toDate = cal.getTime();

        Page<Event> events = eventRepo.findNearByEventsPage(eventSearchDto.getLatitude(), eventSearchDto.getLongitude(), eventSearchDto.getRadius(), fromDate, toDate, pageable);

        return events.map(EventDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventMobileDto> getNearByEventsMobile(EventSearchDto eventSearchDto, Integer userId, Pageable pageable) {
        Page<Object[]> events = eventRepo.findNearByEvents(userId, eventSearchDto.getLatitude(),
                eventSearchDto.getLongitude(), eventSearchDto.getRadius(), pageable);
        return events.map(EventMobileDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventUserPictureDto> getEventPhotos(Integer eventId, Pageable pageable) {
        Page<EventUserPicture> list = eventUserPictureRepo.findEventUserPicturesByEventId(eventId, pageable);
        Page<EventUserPictureDto> listDto = list.map(new Converter<EventUserPicture, EventUserPictureDto>() {
            @Override
            public EventUserPictureDto convert(EventUserPicture EventUserPicture) {
                EventUserPictureDto eventUserPictureDto = new EventUserPictureDto(EventUserPicture);
                return eventUserPictureDto;
            }
        });
        return listDto;
    }

    @Transactional(readOnly = true)
    @Override
    public Integer getNearByEventsCount(EventSearchDto eventSearchDto) {

        Date fromDate = EventDateUtil.getDateOnly();
        Calendar cal = Calendar.getInstance();
        cal.setTime(fromDate);
        cal.add(Calendar.DAY_OF_MONTH, 7);
        Date toDate = cal.getTime();

        Integer noOfEvents = eventRepo.findNearByEventsCount(eventSearchDto.getLatitude(), eventSearchDto.getLongitude(), eventSearchDto.getRadius(), fromDate, toDate);

        return noOfEvents;
    }

    @Transactional(readOnly = true)
    @Override
    public CalculatePriceDto calculatePrice(Integer eventID, String couponCode) {
        Double price = 0d;
        Double totalPrice = 0d;
        CouponDto coupon = null;

        Event event = eventRepo.findOne(eventID);
        if (event != null) {
            price = event.getAdmissionCost();
            totalPrice = price;

            Double discountValue = 0d;
            coupon = orderService.getCoupon(couponCode, CouponCategory.EVENT_ATTEND);
            if (coupon != null)
                discountValue = totalPrice * coupon.getDiscountPercent() * 0.01;

            totalPrice -= discountValue;
        }

        return new CalculatePriceDto(price, coupon, totalPrice);
    }

/*    @Override
    @Transactional
    public EventLocationPictureDto getEventLocation(Integer id) {
        EventLocationPicture eventLocationPicture = eventLocationPictureRepo.findByEventId(id);
        EventLocationPictureDto eventLocationPictureDto = new EventLocationPictureDto(eventLocationPicture);
        return eventLocationPictureDto;
    }*/


    @Override
    @Transactional
    public OrderDto registerUserForEvent(Integer eventId, Integer userId, String couponCode, PaymentMethodType paymentMethodType) {
        EventUser eventUser = new EventUser();
        EventUserId eventUserId = new EventUserId();
        eventUserId.setEvent(eventRepo.findOne(eventId));
        eventUserId.setUser(userRepo.findOne(userId));

        eventUser.setId(eventUserId);
        eventUser.setPaymentMade(true);
        eventUser.setRegistrationDate(new Date());
        eventUser.setStatus(UserEventStatus.REGISTERED);

        CalculatePriceDto calculatedPrice = calculatePrice(eventId, couponCode);
        eventUserRepo.save(eventUser);
        return orderService.addOrder(eventRepo.findOne(eventId), OrderDetailType.EVENT_ATTEND, calculatedPrice.getPrice(), 1,
                userId, couponCode, paymentMethodType);
    }

    @Override
    @Transactional
    public void registerUserForEvent(Integer eventId, Integer userId) {
        EventUser eventUser = new EventUser();
        EventUserId eventUserId = new EventUserId();
        eventUserId.setEvent(eventRepo.findOne(eventId));
        eventUserId.setUser(userRepo.findOne(userId));
        eventUser.setId(eventUserId);
        eventUser.setPaymentMade(true);
        eventUser.setRegistrationDate(new Date());
        eventUser.setStatus(UserEventStatus.REGISTERED);
        eventUserRepo.save(eventUser);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventMobileDto> findFutureEventsByUserId(String date, Integer userId, Pageable pageable) {
        Page<Object[]> eventPage = eventRepo.findFutureEventsByUserId(userId, EventDateUtil.convertToDateFormat(date), pageable);
        return eventPage.map(EventMobileDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventMobileDto> findPastEventsByUserId(String date, Integer userId, Pageable pageable) {
        Page<Object[]> eventPage = eventRepo.findPastEventsByUserId(userId, EventDateUtil.convertToDateFormat(date), pageable);
        return eventPage.map(EventMobileDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EventMobileDto> findFutureEventsListByUserId(String date, Integer userId, Pageable pageable) {
        List<Object[]> eventPage = eventRepo.findFutureEventsByUserId(userId, EventDateUtil.convertToDateFormat(date));
        return new ListMapper<>(eventPage).map(EventMobileDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EventMobileDto> findPastEventsListByUserId(String date, Integer userId) {
        List<Object[]> eventPage = eventRepo.findPastEventsByUserId(userId, EventDateUtil.convertToDateFormat(date));
        return new ListMapper<>(eventPage).map(EventMobileDto::new);
    }

    @Override
    @Transactional
    public Boolean addEventLocationPictures(Integer userId, Integer eventId, List<CloudStorageFileDto> list) {
        List<CloudStorageFile> savedImages = new ArrayList<>();
        EventUserPicture picture;
        for (CloudStorageFileDto dto : list) {
            picture = new EventUserPicture(eventRepo.findOne(eventId), userRepo.findOne(userId), cloudStorageRepo.findOne(dto.getId()));
            EventUserPicture savedPicture = eventUserPictureRepo.save(picture);
            savedImages.add(savedPicture.getPicture());
        }
        return list.size() == savedImages.size();
    }


    @Override
    @Transactional(readOnly = true)
    public Map<Date, List<EventDto>> getPreviousEvents() {
        List<Event> list = eventRepo.findPreviousEvents();
        List<EventDto> eventDtoList = new ArrayList<EventDto>();
/*        if (list == null) {
            list = eventRepo.findPreviousEvents(14);
        }
        if (list == null) {
            list = eventRepo.findPreviousEvents(30);
        }*/
        list.forEach(event -> {
            eventDtoList.add(new EventDto(event));
        });
        Map<Date, List<EventDto>> map = eventDtoList.stream().collect(Collectors.groupingBy(x -> x.getEventDate(), Collectors.mapping(x -> x, Collectors.toList())));
        return new TreeMap<>(map);
    }

    @Override
    @Transactional(readOnly = true)
    public Map<Date, List<EventDto>> getPreviousEvents(Date eventDate) {
        List<Event> list = eventRepo.findPreviousEventsByDate(eventDate);
        List<EventDto> eventDtosList = new ArrayList<EventDto>();
        list.forEach(event -> {
            eventDtosList.add(new EventDto(event));
        });
        Map<Date, List<EventDto>> map = eventDtosList.stream().collect(Collectors.groupingBy(x -> x.getEventDate(), Collectors.mapping(x -> x, Collectors.toList())));
        return new TreeMap<>(map);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EventMobileDto> findMobileEventsByDate(Date date) {
        List<Event> list = eventRepo.findPreviousEventsByDate(date);
        List<EventMobileDto> mobileDtoList = new ArrayList<>();
        list.forEach(event -> {
            mobileDtoList.add(new EventMobileDto(event));
        });
        return mobileDtoList;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventMobileDto> findMobilePreviousEvents(Pageable pageable) {
        Page<Event> eventPage = eventRepo.findPreviousEvents(pageable);
        return eventPage.map(EventMobileDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventDto> findAllFutureActiveEvents(Pageable pageable) {
        Page<Event> eventPage = eventRepo.findByDate(EventStatus.ACTIVE, ApprovalStatus.APPROVED, pageable);
        return eventPage.map(EventDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventDto> findAllByEventLocationAndEventStatus(Pageable pageable, String location) {

        Page<Event> eventPage = eventRepo.findAllByFilters(pageable, location);

        return eventPage.map(EventDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventMobileDto> findAllMobileEventByQuery(Pageable pageable, String location, Integer userId) {
        Page<Object[]> eventPage = eventRepo.findAllByMobileFilters(pageable, location, userId);
        return eventPage.map(EventMobileDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventDto> findEventsByEventDateGreaterThanEqual(Pageable pageable, String eventDate) {

        Date pickedDate = EventDateUtil.convertToDateFormat(eventDate);

        Page<Event> eventpage = eventRepo.findEventsByEventDateGreaterThanEqualAndEventStatusAndEventApprovalStatusOrderByEventDate
                (pageable, pickedDate, EventStatus.ACTIVE, ApprovalStatus.APPROVED);

        return eventpage.map(EventDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EventDto> findAllFutureActiveEvents() {

        List<Event> eventList = eventRepo.findByEventDateAndEventStatus(EventStatus.ACTIVE, ApprovalStatus.APPROVED);
        List<EventDto> eventDtoList = new ArrayList<>();
        if (eventList.size() >= 2) {
            int numberOfItem = 2;
            for (int index = 0; index < numberOfItem; index++) {
                if (!eventList.isEmpty()) {
                    eventDtoList.add(new EventDto(eventList.get(index)));
                }
            }
        } else {
            if (!eventList.isEmpty()) {
                eventDtoList.add(new EventDto(eventList.get(0)));
            }
        }
        return eventDtoList;
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isAmbassador(Integer userId) {
        List<String> userRoles = userRepo.findUserRolesforUserId(userId);
        for (String userRole : userRoles) {
            if (userRole.equalsIgnoreCase("EVENT_AMBASSADOR")) {
                return true;
            }
        }
        return false;
    }

    @Override
    @Transactional
    public List<Integer> cancelEvent(Integer eventId) {
        Event event = eventRepo.findOne(eventId);
        event.setEventStatus(EventStatus.CANCELLED);
        eventRepo.saveAndFlush(event);
        List<Integer> userList = eventUserRepo.finByAttendeesByEventId(eventId);
        if (!userList.isEmpty() && userList.size() != 0) {
            return userList;
        } else {
            return new ArrayList<Integer>();
        }

    }

    @Transactional
    @Override
    public void shareBySocialProvider(Integer eventId, SocialType socialType) {
        Event event = eventRepo.findOne(eventId);
        if (event != null) {
            switch (socialType) {
                case EMAIl:
                    event.setEmailShares(event.getEmailShares() + 1);
                    break;
                case FACEBOOK:
                    event.setFacebookShares(event.getFacebookShares() + 1);
                    break;
                case GOOGLE:
                    event.setGoogleShares(event.getGoogleShares() + 1);
                    break;
                case PINTEREST:
                    event.setPinterestShares(event.getPinterestShares() + 1);
                    break;
                case LINKEDIN:
                    event.setLinkedinShares(event.getLinkedinShares() + 1);
                    break;
                case TWITTER:
                    event.setTwitterShares(event.getTwitterShares() + 1);
                    break;
            }
            eventRepo.save(event);
        }
    }

    @Override
    @Transactional
    public boolean changeEventApprovalStatus(Integer eventId, ApprovalStatus status) {

        Event event = eventRepo.findOne(eventId);

        boolean isApproved = false;
        if (event == null) {
            return false;
        } else {

            event.setEventApprovalStatus(status);
            isApproved = event.getApproved() == null ? false : event.getApproved();
            if (!isApproved && status.equals(ApprovalStatus.APPROVED)) {
                event.setEventStatus(EventStatus.ACTIVE);
                event.setApproved(true);
            } else if (isApproved && status.equals(ApprovalStatus.REJECTED)) {
                event.setEventStatus(EventStatus.CANCELLED);
                event.setApproved(false);
            }
            eventRepo.save(event);
        }
        //updated success
        return isApproved;

    }

    @Override
    @Transactional
    public boolean deleteEvent(Integer eventID) {

        Event event = eventRepo.findOne(eventID);
        if (event != null) {

            List<EventLocationPicture> eventLocationPictureList = eventLocationPictureRepo.findByEventId(event);
            eventLocationPictureList.forEach(eventLocationPicture -> {
                eventLocationPictureRepo.delete(eventLocationPicture);
            });
            eventRepo.delete(eventID);
            return true;
        }

        return false;
    }

    @Override
    @Transactional
    public EventLocationPictureDto updateEventLocation(Integer eventId, CloudStorageFileDto cloudStorageFileDto) {
        Event event = eventRepo.findOne(eventId);
        CloudStorageFile cloudStorageFile = cloudStorageRepo.findOne(cloudStorageFileDto.getId());
        EventLocationPicture eventLocationPicture = event.getLocationPictures().get(0);
        eventLocationPicture.setPicture(cloudStorageFile);
        event.setEventApprovalStatus(ApprovalStatus.PENDING);
        event.setEventStatus(EventStatus.INACTIVE);
        event.getLocationPictures().add(0, eventLocationPicture);
        eventRepo.saveAndFlush(event);
        return new EventLocationPictureDto(eventLocationPicture);
    }


    @Override
    @Transactional
    public boolean deleteLocationImage(Integer eventId) {
        Event event = eventRepo.findOne(eventId);
        if (event != null) {
            event.getLocationPictures().remove(0);
            eventRepo.save(event);
            return true;
        }
        return false;
    }
}
