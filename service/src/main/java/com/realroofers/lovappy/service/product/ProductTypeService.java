package com.realroofers.lovappy.service.product;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.product.dto.ProductTypeDto;
import com.realroofers.lovappy.service.product.dto.mobile.CategoryItemDto;

import java.util.List;

/**
 * Created by Manoj on 01/02/2018.
 */
public interface ProductTypeService extends AbstractService<ProductTypeDto, Integer> {

    String[] getStringArray();

    ProductTypeDto findByName(String name);

    List<ProductTypeDto> findByAddedToSurveyIsFalse();

    List<CategoryItemDto> findAllForMobile();
}
