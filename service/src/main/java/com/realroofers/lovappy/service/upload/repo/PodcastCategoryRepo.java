package com.realroofers.lovappy.service.upload.repo;

import com.realroofers.lovappy.service.upload.model.PodcastCategory;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Manoj
 */
public interface PodcastCategoryRepo extends JpaRepository<PodcastCategory, Integer> {
}
