package com.realroofers.lovappy.service.blog.repo;


import com.realroofers.lovappy.service.blog.model.Category;
import com.realroofers.lovappy.service.blog.model.CategoryPostCount;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CategoryRepo extends JpaRepository<Category, Integer>, QueryDslPredicateExecutor<Category> {

	List<Category> findByName(String name);

	@Query("SELECT NEW com.realroofers.lovappy.service.blog.model.CategoryPostCount(p.category.id, p.category.name, count(p.id)) FROM com.realroofers.lovappy.service.blog.model.Post p WHERE p.state='ACTIVE' GROUP BY p.category.id")
	Page<CategoryPostCount> findCategoryPostsCount(Pageable pageRequest);

}
