package com.realroofers.lovappy.service.product.impl;

import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.gift.dto.FilerObject;
import com.realroofers.lovappy.service.gift.repo.PurchasedGiftRepo;
import com.realroofers.lovappy.service.gift.repo.ViewedGiftRepo;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.CouponDto;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import com.realroofers.lovappy.service.product.*;
import com.realroofers.lovappy.service.product.dto.*;
import com.realroofers.lovappy.service.product.dto.mobile.ItemDto;
import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.product.model.ProductPricesAudit;
import com.realroofers.lovappy.service.product.repo.ProductPricesAuditRepo;
import com.realroofers.lovappy.service.product.repo.ProductRepo;
import com.realroofers.lovappy.service.product.support.ProductAvailability;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.vendor.VendorLocationService;
import com.realroofers.lovappy.service.vendor.model.VendorLocation;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Manoj on 01/02/2018.
 */
@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepo productRepo;
    private final UserService userService;
    private final OrderService orderService;
    private final ViewedGiftRepo viewedGiftRepo;
    private final PurchasedGiftRepo purchasedGiftRepo;
    private final ProductTypeService productTypeService;
    private final BrandService brandService;
    private final CloudStorageService cloudStorageService;
    private final VendorLocationService vendorLocationService;
    private final ProductLocationService productLocationService;
    private final ProductPricesAuditRepo productPricesAuditRepo;


    public ProductServiceImpl(ProductRepo productRepo, UserService userService,
                              OrderService orderService, ViewedGiftRepo viewedGiftRepo,
                              PurchasedGiftRepo purchasedGiftRepo,
                              ProductTypeService productTypeService, BrandService brandService, CloudStorageService cloudStorageService, VendorLocationService vendorLocationService, ProductLocationService productLocationService, ProductPricesAuditRepo productPricesAuditRepo) {
        this.productRepo = productRepo;
        this.userService = userService;
        this.orderService = orderService;
        this.viewedGiftRepo = viewedGiftRepo;
        this.purchasedGiftRepo = purchasedGiftRepo;
        this.productTypeService = productTypeService;
        this.brandService = brandService;
        this.cloudStorageService = cloudStorageService;
        this.vendorLocationService = vendorLocationService;
        this.productLocationService = productLocationService;
        this.productPricesAuditRepo = productPricesAuditRepo;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductDto> findAll() {
        List<ProductDto> list = new ArrayList<>();
        productRepo.findAll().stream().forEach(product -> list.add(new ProductDto(product)));
        return list;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ItemDto> getProductForMobile(PageRequest pageRequest, String searchTerm, int brandId, int categoryId) {
        Page<ProductDto> all;

        if (brandId != 0 && categoryId != 0) {
            all = productRepo.findAllByNameAndCategoryAndBrand(searchTerm, categoryId, brandId, pageRequest);
        } else if (brandId != 0 && categoryId == 0) {
            all = productRepo.findAllByNameAndBrand(searchTerm, brandId, pageRequest);
        } else if (brandId == 0 && categoryId != 0) {
            all = productRepo.findAllByNameAndCategory(searchTerm, categoryId, pageRequest);
        } else {
            all = productRepo.findAllByName(searchTerm, pageRequest);
        }

        return all.map(ItemDto::new);
    }

    @Override
    public Page<ItemDto> getProductForMobile(PageRequest pageRequest, int categoryId) {
        Page<ProductDto> all;
        if (categoryId != 0) {
            all = productRepo.findAllByNameAndCategory("", categoryId, pageRequest);
        } else {
            all = productRepo.findAllByName("", pageRequest);
        }
        return all.map(ItemDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public ItemDto getProductForMobileById(Integer id) {
        Product product = productRepo.findOne(id);
        return product != null ? new ItemDto(product) : null;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductDto> findAllActive() {
        return productRepo.findAllByActiveIsTrueOrderByProductName();
    }

    @Transactional
    @Override
    public ProductDto create(ProductDto product) {
        Product savedProduct = productRepo.saveAndFlush(new Product(product));
        auditProducts(savedProduct);
        return new ProductDto(savedProduct);
    }

    private void auditProducts(Product savedProduct) {
        ProductPricesAudit productPricesAudit= new ProductPricesAudit();
        productPricesAudit.setProduct(savedProduct);
        productPricesAudit.setPrice(savedProduct.getPrice());
        productPricesAudit.setCreatedBy(savedProduct.getCreatedBy());
        productPricesAudit.setCreated(new Date());

        productPricesAuditRepo.save(productPricesAudit);
    }

    @Transactional
    @Override
    public ProductDto update(ProductDto product) {
        Product savedProduct = productRepo.save(new Product(product));
        auditProducts(savedProduct);
        return new ProductDto(savedProduct);
    }

    @Override
    public void delete(Integer integer) {
        throw new NotYetImplementedException();
    }

    @Override
    @Transactional(readOnly = true)
    public ProductDto findById(Integer id) {
        Product product = productRepo.findOne(id);
        return product != null ? new ProductDto(product) : null;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ProductDto> getAllProductsByName(String name, Pageable pageable) {
        return productRepo.findAllByName(name, pageable);
    }

    @Override
    public List<ProductDto> getAllProductsByCode(String code) {
        return productRepo.findAllByProductCode(code);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductDto> getAllProductsForSales() {
        return productRepo.findAllByActiveIsTrueAndApprovedIsTrueAndBlockedIsFalseOrderByProductName();
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductDto> getAllProductsByRecentlyViwed(Integer userId) {
        List<ProductDto> products = new ArrayList<>();
        if (userId != null) {
            User user = userService.getUserById(userId);
            viewedGiftRepo.findAllByUserOrderByViewedOnDesc(user).stream().forEach(viewedGift -> {
                ProductDto product = new ProductDto(viewedGift.getProduct());
                if (product != null) {
                    products.add(product);
                }
            });
        }
        return products;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductDto> getAllProductsRecentlyPurchased(Integer userId) {
        List<ProductDto> products = new ArrayList<>();
        if (userId != null) {
            User user = userService.getUserById(userId);
            purchasedGiftRepo.findAllByUserOrderByPurchasedOnDesc(user).stream().forEach(purchasedGift -> {
                ProductDto product = new ProductDto(purchasedGift.getProduct());
                if (product != null) {
                    products.add(product);
                }
            });
        }
        return products;
    }


    @Override
    @Transactional(readOnly = true)
    public Page<ProductDto> findAllByNameOrderByApproved(String name, Pageable pageable) {
        return productRepo.findAllByNameOrderByApproved(name, pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ProductDto> getAllProductsByNameAndByUser(FilerObject filerObject, Pageable pageable, User user) {

        String name = (filerObject != null && filerObject.getFilterType() != null && !filerObject.getSearchTerm().equals("")
                && filerObject.getFilterType().equalsIgnoreCase("name")) ? "%" + filerObject.getSearchTerm() + "%" : null;
        String code = (filerObject != null && filerObject.getFilterType() != null && !filerObject.getSearchTerm().equals("")
                && filerObject.getFilterType().equalsIgnoreCase("code")) ? "%" + filerObject.getSearchTerm() + "%" : null;

        return productRepo.findAllByNameAndCodeAndUser(name, code, user, pageable);
    }

    @Override
    public ProductDto updateProductStatus(Integer id, Integer userId) {
        ProductDto product = findById(id);
        product.setActive(!product.getActive());
        product.setUpdated(new Date());
        if (userId != null) {
            product.setUpdatedBy(new UserDto(userId));
        }

        return update(product);
    }

    @Transactional
    @Override
    public ProductDto addProduct(ProductRegDto productRegDto, Integer userId) {

        List<Integer> vendorLocations = productRegDto.getVendorLocations();

        ProductDto productDto = new ProductDto(productRegDto);
        productDto.setProductName(productRegDto.getProductName());
        productDto.setProductCode(productRegDto.getProductCode());
        productDto.setProductDescription(productRegDto.getProductDescription());
        productDto.setPrice(productRegDto.getPrice());
        productDto.setProductAvailability(ProductAvailability.IN_STOCK);

        if (productRegDto.getProductType() != null) {
            ProductTypeDto productTypeDto = productTypeService.findById(productRegDto.getProductType());
            productDto.setProductType(productTypeDto);
        }

        if (productRegDto.getBrand() != null) {
            BrandDto brandDto = brandService.findById(productRegDto.getBrand());
            productDto.setBrand(brandDto);
        }
        if (productRegDto.getImage1() != null) {
            CloudStorageFile cloudStorageFile = new CloudStorageFile(productRegDto.getImage1());
            productDto.setImage1(new CloudStorageFileDto(cloudStorageFile));
        }
        if (productRegDto.getImage2() != null) {
            CloudStorageFile cloudStorageFile = new CloudStorageFile(productRegDto.getImage2());
            productDto.setImage2(new CloudStorageFileDto(cloudStorageFile));
        }
        if (productRegDto.getImage3() != null) {
            CloudStorageFile cloudStorageFile = new CloudStorageFile(productRegDto.getImage3());
            productDto.setImage3(new CloudStorageFileDto(cloudStorageFile));
        }
        productDto.setProductEan(productRegDto.getProductEan());

        productDto.setCreatedBy(new UserDto(userId));
        final ProductDto product = create(productDto);

        vendorLocations.stream().forEach(id -> {
            VendorLocation location = vendorLocationService.findById(id);
            if (location != null) {
                ProductLocationDto productLocation = new ProductLocationDto();
                productLocation.setActive(true);
                productLocation.setProduct(product);
                productLocation.setLocation(location);
                try {
                    productLocationService.create(productLocation);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return product;
    }

    @Transactional
    @Override
    public ProductDto updateProduct(ProductRegDto productRegDto, Integer userId) {

        List<Integer> vendorLocations = productRegDto.getVendorLocations();

        ProductDto productDto = new ProductDto(productRegDto);
        productDto.setProductName(productRegDto.getProductName());
        productDto.setProductCode(productRegDto.getProductCode());
        productDto.setProductDescription(productRegDto.getProductDescription());
        productDto.setPrice(productRegDto.getPrice());
        productDto.setProductAvailability(ProductAvailability.IN_STOCK);
        productDto.setApproved(false);
        productDto.setUpdated(new Date());

        if (productRegDto.getProductType() != null) {
            ProductTypeDto productTypeDto = productTypeService.findById(productRegDto.getProductType());
            productDto.setProductType(productTypeDto);
        }

        if (productRegDto.getBrand() != null) {
            BrandDto brandDto = brandService.findById(productRegDto.getBrand());
            productDto.setBrand(brandDto);
        }
        if (productRegDto.getImage1() != null) {
            CloudStorageFile cloudStorageFile = new CloudStorageFile(productRegDto.getImage1());
            productDto.setImage1(new CloudStorageFileDto(cloudStorageFile));
        }
        if (productRegDto.getImage2() != null) {
            CloudStorageFile cloudStorageFile = new CloudStorageFile(productRegDto.getImage2());
            productDto.setImage2(new CloudStorageFileDto(cloudStorageFile));
        }
        if (productRegDto.getImage3() != null) {
            CloudStorageFile cloudStorageFile = new CloudStorageFile(productRegDto.getImage3());
            productDto.setImage3(new CloudStorageFileDto(cloudStorageFile));
        }
        productDto.setProductEan(productRegDto.getProductEan());

        productDto.setCreatedBy(new UserDto(userId));
        final ProductDto product = update(productDto);

        productLocationService.deleteAllByProduct(product.getId());

        vendorLocations.stream().forEach(id -> {
            VendorLocation location = vendorLocationService.findById(id);
            if (location != null) {
                ProductLocationDto productLocation = new ProductLocationDto();
                productLocation.setActive(true);
                productLocation.setProduct(product);
                productLocation.setLocation(location);
                try {
                    productLocationService.create(productLocation);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return product;
    }

    @Transactional
    @Override
    public ProductDto approveProduct(Integer id, Integer userId) {
        ProductDto product = findById(id);
        product.setApproved(true);
        product.setUpdated(new Date());
        if (userId != null) {
            product.setApprovedBy(new UserDto(userId));
        }

        return update(product);
    }

    @Transactional
    @Override
    public ProductDto updateProductStatusAdmin(Integer id, Integer userId) {
        ProductDto product = findById(id);
        product.setBlocked(!product.getBlocked());
        product.setUpdated(new Date());
        if (userId != null) {
            product.setBlockedBy(new UserDto(userId));
        }

        return update(product);
    }

    @Override
    public CalculatePriceDto calculatePrice(Integer giftId, String couponCode) {

        Product gift = productRepo.findOne(giftId);

        Double price = gift.getPrice().doubleValue();
        Double totalPrice = gift.getPrice().doubleValue();

        Double discountValue = 0d;
        CouponDto coupon = orderService.getCoupon(couponCode, CouponCategory.AD);
        if (coupon != null)
            discountValue = totalPrice * coupon.getDiscountPercent() * 0.01;

        totalPrice -= discountValue;

        return new CalculatePriceDto(price, coupon, totalPrice);
    }
}
