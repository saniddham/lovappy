package com.realroofers.lovappy.service.user.model;

import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.PrefHeight;
import com.realroofers.lovappy.service.user.support.Size;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@Entity
@Table(name = "user_preference")
@EqualsAndHashCode
public class UserPreference implements Serializable {

    @Id
    @Column(name = "user_id")
    private Integer userId;
    /*
     TODO: Associate with User table once Achike is done with security (login/registration)
    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    private User user;
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private Gender gender;

    @Type(type = "yes_no")
    @Column(length = 1)
    private Boolean penisSizeMatter;

    @Enumerated(EnumType.STRING)
    @Column(name = "breast_size")
    private Size bustSize;

    @Enumerated(EnumType.STRING)
    @Column(name = "butt_size")
    private Size buttSize;

    @Column(name = "minimum_age")
    private Integer minAge;

    @Column(name = "maximum_age")
    private Integer maxAge;

    @Enumerated(EnumType.STRING)
    @Column(name = "height_preference")
    private PrefHeight height;

    @ManyToMany
    @JoinTable(
            name = "user_preference_seeking_language",
            joinColumns = @JoinColumn(
                    name = "user_preference_user_id", referencedColumnName = "user_id"),
            inverseJoinColumns = @JoinColumn(
                    name = "seeking_language_id", referencedColumnName = "id"))
    private Collection<Language> seekingLanguage;

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private User user;

    @Column(name = "likes_notifications" , columnDefinition = "BIT(1) default 0")
    private Boolean newLikesNotifications = false;

    @Column(name = "matches_notifications" , columnDefinition = "BIT(1) default 0")
    private Boolean newMatchesNotifications = false;

    @Column(name = "messages_notifications" , columnDefinition = "BIT(1) default 0")
    private Boolean newMessagesNotifications = false;

    @Column(name = "songs_notifications" , columnDefinition = "BIT(1) default 0")
    private Boolean newSongsNotifications = false;

    @Column(name = "gifts_notifications" , columnDefinition = "BIT(1) default 0")
    private Boolean newGiftsNotifications = false;

    @Column(name = "unlock_requests_notifications", columnDefinition = "BIT(1) default 0")
    private Boolean newUnlockRequestNotifications = false;

    @Column(name = "song_approval", columnDefinition = "BIT(1) default 0")
    private Boolean songApproval = false;

    @Column(name = "new_song_purchase", columnDefinition = "BIT(1) default 0")
    private Boolean newSongPurchase = false;

    @Column(name = "song_denials", columnDefinition = "BIT(1) default 0")
    private Boolean songDenials = false;

    public UserPreference() {
        // Default constructor
    }

    public UserPreference(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Boolean getPenisSizeMatter() {
        return penisSizeMatter;
    }

    public void setPenisSizeMatter(Boolean penisSizeMatter) {
        this.penisSizeMatter = penisSizeMatter;
    }

    public Size getBustSize() {
        return bustSize;
    }

    public void setBustSize(Size bustSize) {
        this.bustSize = bustSize;
    }

    public Size getButtSize() {
        return buttSize;
    }

    public void setButtSize(Size buttSize) {
        this.buttSize = buttSize;
    }

    public Integer getMinAge() {
        return minAge;
    }

    public void setMinAge(Integer minAge) {
        this.minAge = minAge;
    }

    public Integer getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(Integer maxAge) {
        this.maxAge = maxAge;
    }

    public PrefHeight getHeight() {
        return height;
    }

    public void setHeight(PrefHeight height) {
        this.height = height;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Collection<Language> getSeekingLanguage() {
        return seekingLanguage;
    }

    public void setSeekingLanguage(Collection<Language> seekingLanguage) {
        this.seekingLanguage.clear();
        this.seekingLanguage.addAll(seekingLanguage);
    }

    public Boolean getNewLikesNotifications() {
        return newLikesNotifications;
    }

    public void setNewLikesNotifications(Boolean newLikesNotifications) {
        this.newLikesNotifications = newLikesNotifications;
    }

    public Boolean getNewMatchesNotifications() {
        return newMatchesNotifications;
    }

    public void setNewMatchesNotifications(Boolean newMatchesNotifications) {
        this.newMatchesNotifications = newMatchesNotifications;
    }

    public Boolean getNewMessagesNotifications() {
        return newMessagesNotifications;
    }

    public void setNewMessagesNotifications(Boolean newMessagesNotifications) {
        this.newMessagesNotifications = newMessagesNotifications;
    }

    public Boolean getNewSongsNotifications() {
        return newSongsNotifications;
    }

    public void setNewSongsNotifications(Boolean newSongsNotifications) {
        this.newSongsNotifications = newSongsNotifications;
    }

    public Boolean getNewGiftsNotifications() {
        return newGiftsNotifications;
    }

    public void setNewGiftsNotifications(Boolean newGiftsNotifications) {
        this.newGiftsNotifications = newGiftsNotifications;
    }

    public Boolean getNewUnlockRequestNotifications() {
        return newUnlockRequestNotifications;
    }

    public void setNewUnlockRequestNotifications(Boolean newUnlockRequestNotifications) {
        this.newUnlockRequestNotifications = newUnlockRequestNotifications;
    }

    public Boolean getSongApproval() {
        return songApproval;
    }

    public void setSongApproval(Boolean songApproval) {
        this.songApproval = songApproval;
    }

    public Boolean getNewSongPurchase() {
        return newSongPurchase;
    }

    public void setNewSongPurchase(Boolean newSongPurchase) {
        this.newSongPurchase = newSongPurchase;
    }

    public Boolean getSongDenials() {
        return songDenials;
    }

    public void setSongDenials(Boolean songDenials) {
        this.songDenials = songDenials;
    }
}
