package com.realroofers.lovappy.service.user.dto;

import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by Eias on 28-Apr-17
 */
@EqualsAndHashCode
public class SetNewPasswordForm {

    @NotNull
    @Size(min = 6, message = "Min password length is 6")
    @Pattern(regexp = "^([0-9]+[a-zA-Z]+|[a-zA-Z]+[0-9]+)[0-9a-zA-Z]*$", message = "Password should be mixed of letters and digits")
    private String password;

    private String token;

    public SetNewPasswordForm(){}

    public SetNewPasswordForm(String token){this.token = token;}

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
