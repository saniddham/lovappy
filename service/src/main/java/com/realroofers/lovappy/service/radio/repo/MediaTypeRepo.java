package com.realroofers.lovappy.service.radio.repo;

import com.realroofers.lovappy.service.radio.model.MediaType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Manoj on 17/12/2017.
 */
public interface MediaTypeRepo extends JpaRepository<MediaType,Integer> {

    List<MediaType> findAllByActiveIsTrueOrderByName();

    MediaType findByName(String name);
}
