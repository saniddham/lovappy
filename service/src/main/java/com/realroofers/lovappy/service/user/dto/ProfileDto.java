package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.model.UserRoles;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.util.StringUtils;

import java.io.Serializable;

/**
 * Created by daoud on 10/20/2018.
 */
@Data
@EqualsAndHashCode
public class ProfileDto implements Serializable {

    private Integer id;
    private String email;
    private String firstname;
    private String lastname;
    private String imageProfile;
    private Roles role;
    public ProfileDto() {
    }

    public ProfileDto(User user) {
        this.id = user.getUserId();
        this.email = user.getEmail();
        this.firstname = user.getFirstName();
        this.lastname = user.getLastName();
        if(user.getUserProfile().getProfilePhotoCloudFile() != null)
        this.imageProfile = user.getUserProfile().getProfilePhotoCloudFile().getUrl();
        for (UserRoles role : user.getRoles()) {
            this.role = Roles.valueOf(role.getId().getRole().getName());
            break;
        }
    }
    public ProfileDto(UserDto user) {
        this.id = user.getID();
        this.email = user.getEmail();
        this.firstname = user.getFirstName();
        this.lastname = user.getLastName();
        if(user.getUserProfile().getPhotoIdentificationCloudFile() != null) {
            this.imageProfile = user.getUserProfile().getPhotoIdentificationCloudFile().getUrl();
        }
        for (RoleDto role : user.getRoles()) {
         this.role = Roles.valueOf(role.getName());
         break;
        }
    }
    public String getImageUrl(){
        if(StringUtils.isEmpty(imageProfile)) {
            this.imageProfile = "/images/icons/upload_icons/upload_profile_sample.png";
        }
        return imageProfile;
    }
}
