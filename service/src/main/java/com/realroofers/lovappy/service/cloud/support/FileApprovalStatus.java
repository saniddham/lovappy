package com.realroofers.lovappy.service.cloud.support;

/**
 * @author Japheth Odonya
 * */
public enum FileApprovalStatus {
    APPROVEPROFILEPHOTO("approveprofilephoto"),REJECTPROFILEPHOTO("rejectprofilephoto"),
    APPROVEPROFILEAUDIO("approveprofileaudio"), REJECTPROFILEAUDIO("rejectprofileaudio"),
    APPROVEHANDSPHOTO("approvehandsphoto"), REJECTHANDSPHOTO("rejecthandsphoto"),
    APPROVEFEETPHOTO("approvefeetphoto"), REJECTFEETPHOTO("rejectfeetphoto"),
    APPROVELEGSPHOTO("approvelegsphoto"), REJECTLEGSPHOTO("rejectlegsphoto");

    private String status;

    FileApprovalStatus(String status){
        this.status = status;
    }

    public String getValue(){
        return status;
    }


}
