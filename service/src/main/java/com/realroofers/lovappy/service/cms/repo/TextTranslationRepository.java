package com.realroofers.lovappy.service.cms.repo;

import com.realroofers.lovappy.service.cms.model.TextWidget;
import com.realroofers.lovappy.service.cms.model.TextTranslation;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Daoud Shaheen on 6/10/2017.
 */
public interface TextTranslationRepository extends JpaRepository<TextTranslation, Integer> {
    TextTranslation findById(Integer id);
    TextTranslation findByTextWidgetAndLanguage(TextWidget textWidget, String language);
}
