package com.realroofers.lovappy.service.cms.model;

import com.realroofers.lovappy.service.system.model.Language;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "page_meta")
@EqualsAndHashCode
public class PageMetaData implements Serializable{

    private static final long serialVersionUID = 3447409517106084077L;

    @Id
    @GeneratedValue()
    private Integer id;

    @ManyToOne
    private Page page;

    @ManyToOne
    private Language language;

    @Column(length = 255)
    private String title;

    @Column(length = 65535, columnDefinition = "Text")
    private String description;

    @Column(length = 65535, columnDefinition = "Text")
    private String keywords;

    @Column(length = 65535, columnDefinition = "Text")
    private String abstractDescription;

    public PageMetaData() {
    }

    public PageMetaData(Page page, Language language, String title) {
        this.page = page;
        this.language = language;
        this.title = title;
    }

    public PageMetaData(Page page, Language language, String title, String description, String keywords, String abstractDescription) {
        this.page = page;
        this.language = language;
        this.title = title;
        this.description = description;
        this.keywords = keywords;
        this.abstractDescription = abstractDescription;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getAbstractDescription() {
        return abstractDescription;
    }

    public void setAbstractDescription(String abstractDescription) {
        this.abstractDescription = abstractDescription;
    }

    @Override
    public String toString() {
        return "PageMetaData{" +
                "page=" + page +
                ", language=" + language +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", keywords='" + keywords + '\'' +
                ", abstractDescription='" + abstractDescription + '\'' +
                '}';
    }
}
