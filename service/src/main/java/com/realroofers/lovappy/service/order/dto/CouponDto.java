package com.realroofers.lovappy.service.order.dto;

import com.realroofers.lovappy.service.order.model.Coupon;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import com.realroofers.lovappy.service.user.UserUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;
import java.util.stream.Collectors;
@Data
@ToString
@EqualsAndHashCode
public class CouponDto {

    private Integer id;
    private String couponCode;
    private CouponCategory category;
    private Integer discountPercent;
    private Date expiryDate;
    private Integer usageNumber;
    private String description;


    public CouponDto() {
    }

    public CouponDto(Coupon coupon) {
        this.id = coupon.getId();
        this.couponCode = coupon.getCouponCode();
        this.category = coupon.getCategory();
        this.discountPercent = coupon.getDiscountPercent()==null? 0 : coupon.getDiscountPercent();
        this.expiryDate = coupon.getExpiryDate();
        this.description = coupon.getDescription();
        //count number of uses by logged user, can't be more than 1
        if(coupon.getOrders() != null)
            try {
                this.usageNumber = coupon.getOrders()
                        .stream()
                        .filter(x -> x.getUser().getUserId().equals(UserUtil.INSTANCE.getCurrentUserId()))
                        .collect(Collectors.toList())
                        .size();
            }catch (Exception ex){

            }
    }

    public CouponDto(Integer id, String couponCode, CouponCategory category, Integer discountPercent, Date expiryDate,
                     String description) {
        this.id = id;
        this.couponCode = couponCode;
        this.category = category;
        this.discountPercent = discountPercent;
        this.expiryDate = expiryDate;
        this.description = description;
    }
}
