package com.realroofers.lovappy.service.radio.impl;

import com.realroofers.lovappy.service.radio.MediaTypeService;
import com.realroofers.lovappy.service.radio.model.MediaType;
import com.realroofers.lovappy.service.radio.repo.MediaTypeRepo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Manoj on 17/12/2017.
 */
@Service
public class MediaTypeServiceImpl implements MediaTypeService {

    private final MediaTypeRepo mediaTypeRepo;

    public MediaTypeServiceImpl(MediaTypeRepo mediaTypeRepo) {
        this.mediaTypeRepo = mediaTypeRepo;
    }

    @Override
    public List<MediaType> findAll() {
        return mediaTypeRepo.findAll();
    }

    @Override
    public List<MediaType> findAllActive() {
        return mediaTypeRepo.findAllByActiveIsTrueOrderByName();
    }

    @Override
    public MediaType create(MediaType mediaType) {
        return mediaTypeRepo.saveAndFlush(mediaType);
    }

    @Override
    public MediaType update(MediaType mediaType) {
        return mediaTypeRepo.save(mediaType);
    }

    @Override
    public void delete(Integer id) {
        mediaTypeRepo.delete(id);
    }

    @Override
    public MediaType findById(Integer id) {
        return mediaTypeRepo.findOne(id);
    }

    @Override
    public MediaType findByName(String name) {
        return mediaTypeRepo.findByName(name);
    }
}
