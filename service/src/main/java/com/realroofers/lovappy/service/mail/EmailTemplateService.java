package com.realroofers.lovappy.service.mail;

import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.dto.LovappyEmailDto;
import com.realroofers.lovappy.service.mail.support.EmailTypes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Daoud Shaheen on 9/6/2017.
 */
public interface EmailTemplateService {

    EmailTemplateDto getByIdAndLanguage(Integer templateId, String language);

    EmailTemplateDto getByTypeAndLanguage(EmailTypes type, String language);

    EmailTemplateDto getByNameAndLanguage(String name, String language);

    EmailTemplateDto create(EmailTemplateDto emailTemplateDto);

    Page<EmailTemplateDto> getListByLanguage(String language, Pageable pageable);

    void delete(Integer templateId);

    EmailTemplateDto update(Integer templateId, EmailTemplateDto emailTemplateDto);

    Page<LovappyEmailDto> getLovappyEmails(int page, int limit);

    void deleteAll();
}
