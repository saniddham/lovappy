package com.realroofers.lovappy.service.user;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.realroofers.lovappy.service.exception.GenericServiceException;
import com.realroofers.lovappy.service.user.dto.UserAuthDto;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public enum UserUtil {
    INSTANCE;

    public UserAuthDto getCurrentUser() {
        UserAuthDto user = null;
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        if (authentication != null && UserAuthDto.class.isAssignableFrom(authentication.getPrincipal().getClass())) {
            user = (UserAuthDto) authentication.getPrincipal();
        }
        return user;
    }

    public Integer getCurrentUserId() {
        UserAuthDto dto = getCurrentUser();
        return dto != null ? dto.getUserId() : null;
    }

    public int getLoggedInUserId() {
        UserAuthDto dto = getCurrentUser();
        if (dto == null) {
            throw new GenericServiceException("No logged in user found");
        }
        return dto.getUserId();
    }
}
