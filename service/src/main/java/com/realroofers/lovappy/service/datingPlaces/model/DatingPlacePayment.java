package com.realroofers.lovappy.service.datingPlaces.model;

import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Darrel Rayen on 10/17/17.
 */
@Entity
@EqualsAndHashCode
public class DatingPlacePayment {

    @Id
    @GeneratedValue
    private Integer datingPlacePaymentId;

    @OneToOne(targetEntity = DatingPlace.class,fetch = FetchType.LAZY, cascade =CascadeType.PERSIST)
    @JoinColumn(name = "place_id")
    private DatingPlace placeId;

    @Column(name = "paid_amount", nullable = false)
    private Double paidAmount;

    @Column(name = "payment_date", nullable = false)
    private Date paymentDate;

    public Integer getDatingPlacePaymentId() {
        return datingPlacePaymentId;
    }

    public void setDatingPlacePaymentId(Integer datingPlacePaymentId) {
        this.datingPlacePaymentId = datingPlacePaymentId;
    }

    public DatingPlace getPlaceId() {
        return placeId;
    }

    public void setPlaceId(DatingPlace placeId) {
        this.placeId = placeId;
    }

    public Double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(Double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }
}
