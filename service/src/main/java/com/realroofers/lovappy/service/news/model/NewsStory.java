package com.realroofers.lovappy.service.news.model;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/* news story : news image ,video,user Identification and advanced option is implemented as one to one relationship
        because of future purposes
 */

@Entity
@Table(name = "news_story")
@EqualsAndHashCode
public class NewsStory implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "story_id")
    private Long id;


    @Column(name = "story_title", nullable = false, unique = true)
    private String title;

    //Audio or video url
    @Column(name = "story_audio_video_url", columnDefinition = "VARCHAR(1024)")
    private String audioVideoUrl;

    //post content
    @Column(name = "story_contain", columnDefinition = "VARCHAR(2048)")
    private String contain;

    //post source eg: skynews,lovappy
    @Column(name = "story_source", nullable = false)
    private String source;

    @Column(name = "story_submit_date")
    private Date submitDate;

    @Column(name = "story_posted_date")
    private Date postedDate;

    @Column(name = "story_posted")
    private Boolean posted;

    @Column(name = "image_description", columnDefinition = "VARCHAR(1024)", nullable = false)
    private String imageDescription;


    @Column(name = "image_keywords")
    private String imageKeywords;



    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private NewsAdvancedOption newsAdvancedOption;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn
    private CloudStorageFile newsImage;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn
    private CloudStorageFile newsUserIdentification;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn
    private CloudStorageFile newsVideo;


    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "cat_id", nullable = false)
    private NewsCategory newsCategory;

    public NewsStory() {
    }

    public NewsStory(String title, String audioVideoUrl, String contain, String source, Date submitDate,
                     Date postedDate, Boolean posted, CloudStorageFile newsImage, NewsAdvancedOption newsAdvancedOption,
                     CloudStorageFile newsUserIdentification, CloudStorageFile newsVideo, NewsCategory newsCategory,
                     String imageDescription,String imageKeywords) {

        this.title = title;
        this.audioVideoUrl = audioVideoUrl;
        this.contain = contain;
        this.source = source;
        this.submitDate = submitDate;
        this.postedDate = postedDate;
        this.posted = posted;
        this.newsImage = newsImage;
        this.newsAdvancedOption = newsAdvancedOption;
        this.newsUserIdentification = newsUserIdentification;
        this.newsVideo = newsVideo;
        this.newsCategory = newsCategory;
        this.imageDescription=imageDescription;
        this.imageKeywords=imageKeywords;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAudioVideoUrl() {
        return audioVideoUrl;
    }

    public void setAudioVideoUrl(String audioVideoUrl) {
        this.audioVideoUrl = audioVideoUrl;
    }

    public String getContain() {
        return contain;
    }

    public void setContain(String contain) {
        this.contain = contain;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    public Boolean getPosted() {
        return posted;
    }

    public void setPosted(Boolean posted) {
        this.posted = posted;
    }


    public NewsAdvancedOption getNewsAdvancedOption() {
        return newsAdvancedOption;
    }

    public void setNewsAdvancedOption(NewsAdvancedOption newsAdvancedOption) {
        this.newsAdvancedOption = newsAdvancedOption;
    }

    public CloudStorageFile getNewsImage() {
        return newsImage;
    }

    public void setNewsImage(CloudStorageFile newsImage) {
        this.newsImage = newsImage;
    }

    public CloudStorageFile getNewsUserIdentification() {
        return newsUserIdentification;
    }

    public void setNewsUserIdentification(CloudStorageFile newsUserIdentification) {
        this.newsUserIdentification = newsUserIdentification;
    }

    public CloudStorageFile getNewsVideo() {
        return newsVideo;
    }

    public void setNewsVideo(CloudStorageFile newsVideo) {
        this.newsVideo = newsVideo;
    }

    public NewsCategory getNewsCategory() {
        return newsCategory;
    }

    public void setNewsCategory(NewsCategory newsCategory) {
        this.newsCategory = newsCategory;
    }

    public String getImageDescription() {
        return imageDescription;
    }

    public void setImageDescription(String imageDescription) {
        this.imageDescription = imageDescription;
    }

    public String getImageKeywords() {
        return imageKeywords;
    }

    public void setImageKeywords(String imageKeywords) {
        this.imageKeywords = imageKeywords;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("NewsStory{");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", audioVideoUrl='").append(audioVideoUrl).append('\'');
        sb.append(", contain='").append(contain).append('\'');
        sb.append(", source='").append(source).append('\'');
        sb.append(", submitDate=").append(submitDate);
        sb.append(", postedDate=").append(postedDate);
        sb.append(", posted=").append(posted);
        sb.append('}');
        return sb.toString();
    }
}
