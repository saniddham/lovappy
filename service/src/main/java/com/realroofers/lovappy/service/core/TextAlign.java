package com.realroofers.lovappy.service.core;

/**
 * Created by Daoud Shaheen on 1/22/2018.
 */
public enum TextAlign {
    CENTER, RIGHT, LEFT, TOP, BOTTOM
}
