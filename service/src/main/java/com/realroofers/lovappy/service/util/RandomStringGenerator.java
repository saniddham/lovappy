package com.realroofers.lovappy.service.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RandomStringGenerator {
    public static final int DEFAULT_LENGTH = 12;
	/** The array of characters to choose from */
	private static final char[] CHARS = "aAbBcCdDeEfFgGhHjJkKmMnNpPqQrRsStTuUvVwWxXyYzZ23456789_-@#$".toCharArray();
	private static final char[] NUMERIC_CHARS = "0123456789".toCharArray();

	private int length = DEFAULT_LENGTH;
	private int passwordLength = DEFAULT_LENGTH;
	private boolean allowDigits = true;
	private boolean allowLetters = true;
    private int passwordFormatBlockLength = 3;
    private String passwordFormatBlockDelimeter = "-";
    
 	public String generate() {
		return generate(getLength());
	}

	public String generatePassword() {
		return generate(getPasswordLength());
	}

	public String generate(int length) {
		return generate(length, isAllowLetters(), isAllowDigits());
	}

	public String generate(int length, boolean allowLetters, boolean allowDigits) {
	    char[] chars = allowDigits && !allowLetters ? NUMERIC_CHARS : CHARS;
		return RandomStringUtils.random(length, 0, chars.length, allowLetters, allowDigits, chars);
	}
	
	public String generateAscii(int length) {
		return RandomStringUtils.randomAscii(length);
	}

	/**
	 * Format the password to be in a user friendly format.
	 * We add a delimiter every # characters ("block"), where the block length and the delimiter are configurable properties.
	 * 
	 * An output for example: XXX-XXX
	 *  
	 *
	 * @param pswd
	 * @return
	 */
	public String formatPassword(String pswd){
	    StringBuilder sb = new StringBuilder(pswd);
	    int delimLength = getPasswordFormatBlockDelimeter().length(),
	        blockLength = getPasswordFormatBlockLength(),
	        i = getPasswordFormatBlockLength(); 
	    while(i<sb.length()){
	        sb.insert(i, getPasswordFormatBlockDelimeter());
	        i += blockLength+delimLength;
	    }
	    return sb.toString();
	}
	
	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getPasswordLength() {
		return passwordLength;
	}

	public void setPasswordLength(int passwordLength) {
		this.passwordLength = passwordLength;
	}

	public boolean isAllowDigits() {
		return allowDigits;
	}

	public void setAllowDigits(boolean allowDigits) {
		this.allowDigits = allowDigits;
	}

	public boolean isAllowLetters() {
		return allowLetters;
	}

	public void setAllowLetters(boolean allowLetters) {
		this.allowLetters = allowLetters;
	}

    public int getPasswordFormatBlockLength() {
        return passwordFormatBlockLength;
    }

    public void setPasswordFormatBlockLength(int passwordFormatBlockLength) {
        this.passwordFormatBlockLength = passwordFormatBlockLength;
    }

    public String getPasswordFormatBlockDelimeter() {
        return passwordFormatBlockDelimeter;
    }

    public void setPasswordFormatBlockDelimeter(String passwordFormatBlockDelimeter) {
        this.passwordFormatBlockDelimeter = passwordFormatBlockDelimeter;
    }
}