package com.realroofers.lovappy.service.order.repo;

import com.realroofers.lovappy.service.order.dto.CouponRuleDto;
import com.realroofers.lovappy.service.order.model.CouponRule;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by hasan on 17/11/2017.
 */
public interface CouponRuleRepo extends JpaRepository<CouponRule, Integer> {
    List<CouponRuleDto> findAllByExpiryDateAfter(Date nowDate);
    List<CouponRuleDto> findAllByExpiryDateAfterAndAffectedCategory(Date nowDate, CouponCategory couponCategory);
}
