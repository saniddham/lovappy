package com.realroofers.lovappy.service.music.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.realroofers.lovappy.service.ads.support.AgeRange;
import com.realroofers.lovappy.service.music.model.Music;
import com.realroofers.lovappy.service.music.support.MusicProvider;
import com.realroofers.lovappy.service.music.support.MusicStatus;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Eias Altawil on 8/25/17
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EqualsAndHashCode
public class MusicDto implements Serializable {
    private Integer id;
    private Long providerResourceId;
    private String title;
    private String fileUrl;
    private String previewFileUrl;
    private String coverImageUrl;
    private String downloadUrl;
    private String lyrics;
    private UserDto author;
    private GenreDto genre;
    private Double rating;
    private Double userRating;
    private Double price;
    private Boolean alreadyRated;
    private Gender gender;
    private AgeRange ageRange;
    private MusicStatus status;
    private Date createdOn;
    private String artistName;

    private Boolean advertised;
    private MusicProvider provider;

    public MusicDto() {
    }

    public MusicDto(Music music) {
        this.id = music.getId();
        this.title = music.getTitle();

        this.fileUrl = music.getFileUrl();
        this.previewFileUrl = music.getPreviewFileUrl();
        this.coverImageUrl = music.getCoverImageUrl();
        this.downloadUrl = music.getDownloadUrl();

        this.lyrics = music.getLyrics();
        this.author = new UserDto(music.getAuthor());
        this.genre = new GenreDto(music.getGenre());
        this.rating = music.getAvgRating();
        this.alreadyRated = false;
        this.gender = music.getGender();
        this.ageRange = music.getAgeRange();
        this.status = music.getStatus();
        this.createdOn = music.getCreated();
        this.artistName = music.getArtistName();
        this.advertised = music.getAdvertised() == null ? false : music.getAdvertised();
        this.providerResourceId = music.getProviderResourceId();

        this.provider = music.getProvider();
    }

    public MusicProvider getProvider() {
        return provider;
    }

    public void setProvider(MusicProvider provider) {
        this.provider = provider;
    }

    public Long getProviderResourceId() {
        return providerResourceId;
    }

    public void setProviderResourceId(Long providerResourceId) {
        this.providerResourceId = providerResourceId;
    }

    public Boolean getAdvertised() {
        return advertised;
    }

    public void setAdvertised(Boolean advertised) {
        this.advertised = advertised;
    }

    public void setAuthorUsername(User author) {
        if(StringUtils.isEmpty(author.getFirstName()) )
            this.artistName = author.getEmail();
        else {
            this.artistName = StringUtils.isEmpty(author.getLastName()) ? author.getFirstName() : author.getFirstName() + " " +
            author.getLastName();
        }
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getPreviewFileUrl() {
        return previewFileUrl;
    }

    public void setPreviewFileUrl(String previewFileUrl) {
        this.previewFileUrl = previewFileUrl;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public UserDto getAuthor() {
        return author;
    }

    public void setAuthor(UserDto author) {
        this.author = author;
    }

    public GenreDto getGenre() {
        return genre;
    }

    public void setGenre(GenreDto genre) {
        this.genre = genre;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Boolean getAlreadyRated() {
        return alreadyRated;
    }

    public void setAlreadyRated(Boolean alreadyRated) {
        this.alreadyRated = alreadyRated;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public AgeRange getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(AgeRange ageRange) {
        this.ageRange = ageRange;
    }

    public MusicStatus getStatus() {
        return status;
    }

    public void setStatus(MusicStatus status) {
        this.status = status;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public Double getUserRating() {
        return userRating;
    }

    public void setUserRating(Double userRating) {
        this.userRating = userRating;
    }
}
