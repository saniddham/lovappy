package com.realroofers.lovappy.service.datingPlaces.model;

import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Darrel Rayen on 8/14/18.
 */
@Entity
@EqualsAndHashCode
public class DatingPlaceFoodTypes implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "place_id")
    private DatingPlace placeId;

    @Id
    @ManyToOne
    @JoinColumn(name = "food_id")
    private FoodType foodType;

    public DatingPlaceFoodTypes() {
    }

    public DatingPlaceFoodTypes(DatingPlace placeId, FoodType foodType) {
        this.placeId = placeId;
        this.foodType = foodType;
    }

    public DatingPlace getPlaceId() {
        return placeId;
    }

    public void setPlaceId(DatingPlace placeId) {
        this.placeId = placeId;
    }

    public FoodType getFoodType() {
        return foodType;
    }

    public void setFoodType(FoodType foodType) {
        this.foodType = foodType;
    }
}
