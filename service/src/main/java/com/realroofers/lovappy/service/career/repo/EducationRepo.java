package com.realroofers.lovappy.service.career.repo;

import com.realroofers.lovappy.service.career.model.Education;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EducationRepo extends JpaRepository<Education, Long> {
}
