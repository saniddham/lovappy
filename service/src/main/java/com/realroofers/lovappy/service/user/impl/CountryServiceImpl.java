package com.realroofers.lovappy.service.user.impl;

import com.realroofers.lovappy.service.user.CountryService;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.model.Country;
import com.realroofers.lovappy.service.user.repo.CountryRepo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Manoj on 11/02/2018.
 */
@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepo countryRepo;
    private final UserService userService;

    public CountryServiceImpl(CountryRepo countryRepo, UserService userService) {
        this.countryRepo = countryRepo;
        this.userService = userService;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Country> findAll() {
        return countryRepo.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public List<Country> findAllActive() {
        return countryRepo.findAllByActiveIsTrueOrderByName();
    }

    @Transactional
    @Override
    public Country create(Country country) {
        return countryRepo.saveAndFlush(country);
    }

    @Transactional
    @Override
    public Country update(Country country) {
        return countryRepo.save(country);
    }

    @Transactional
    @Override
    public void delete(Integer integer) {
        Country country = findById(integer);
        country.setActive(false);
        country.setUpdated(new Date());
        country.setUpdatedBy(userService.getUserById(UserUtil.INSTANCE.getCurrentUserId()));
        countryRepo.save(country);
    }

    @Transactional(readOnly = true)
    @Override
    public Country findById(Integer integer) {
        return countryRepo.findOne(integer);
    }

    @Transactional(readOnly = true)
    @Override
    public Country findByCode(String code) {
        return countryRepo.findByCode(code);
    }

    @Transactional(readOnly = true)
    @Override
    public String[] getCountryNameList() {
        List<Country> countries = findAllActive();

        int i = countries.size();
        int n = ++i;
        String[] countryList = new String[n];
        for (int cnt = 0; cnt < countries.size(); cnt++) {
            countryList[cnt] = countries.get(cnt).getName();
        }
        return countryList;
    }
}
