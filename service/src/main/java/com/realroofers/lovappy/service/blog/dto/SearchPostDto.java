package com.realroofers.lovappy.service.blog.dto;

import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author mwiyono
 */
@EqualsAndHashCode
public class SearchPostDto  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3703470771188414191L;
	
	
	private String searchKey;

	public String getSearchKey() {
		return searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}
	

}
