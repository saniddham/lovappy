package com.realroofers.lovappy.service.product.repo;

import com.realroofers.lovappy.service.product.model.RetailerCommission;
import com.realroofers.lovappy.service.user.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RetailerCommissionRepo extends JpaRepository<RetailerCommission, Integer> {

    List<RetailerCommission> findAllByActiveIsTrue();

    RetailerCommission findTopByActiveIsTrueAndCountryOrderByAffectiveDateAsc(@Param("country")Country country);
}
