package com.realroofers.lovappy.service.blog;

import java.util.List;

import com.realroofers.lovappy.service.blog.model.RelayComment;

public interface RelayCommentService {
	
	void save(RelayComment relaycomment);

	List<RelayComment> getAllRelayComments();
}
