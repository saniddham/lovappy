package com.realroofers.lovappy.service.upload.impl;

import com.realroofers.lovappy.service.upload.ComedyCategoryService;
import com.realroofers.lovappy.service.upload.dto.ComedyCategoryDto;
import com.realroofers.lovappy.service.upload.model.ComedyCategory;
import com.realroofers.lovappy.service.upload.repo.ComedyCategoryRepo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manoj
 */
@Service
public class ComedyCategoryServiceImpl implements ComedyCategoryService {

    private final ComedyCategoryRepo uploadCategoryRepo;

    public ComedyCategoryServiceImpl(ComedyCategoryRepo uploadCategoryRepo) {
        this.uploadCategoryRepo = uploadCategoryRepo;
    }

    @Override
    public Page<ComedyCategoryDto> findAll(Pageable pageable) {
        Page<ComedyCategoryDto> list = null;

        Page<ComedyCategory> all = uploadCategoryRepo.findAll(pageable);
        list = all.map(ComedyCategoryDto::new);
        return list;
    }

    @Override
    public List<ComedyCategoryDto> findAll() {
        List<ComedyCategoryDto> list = new ArrayList<>();
        uploadCategoryRepo.findAll().stream().forEach(uploadCategory -> list.add(new ComedyCategoryDto(uploadCategory)));
        return list;
    }

    @Override
    public List<ComedyCategoryDto> findAllActive() {
        List<ComedyCategoryDto> list = new ArrayList<>();
        uploadCategoryRepo.findAll().stream().forEach(uploadCategory -> list.add(new ComedyCategoryDto(uploadCategory)));
        return list;
    }

    @Override
    public ComedyCategoryDto create(ComedyCategoryDto uploadCategoryDto) throws Exception {
        return new ComedyCategoryDto(uploadCategoryRepo.saveAndFlush(new ComedyCategory(uploadCategoryDto)));
    }

    @Override
    public ComedyCategoryDto update(ComedyCategoryDto uploadCategoryDto) throws Exception {
        return new ComedyCategoryDto(uploadCategoryRepo.save(new ComedyCategory(uploadCategoryDto)));
    }

    @Override
    public void delete(Integer integer) throws Exception {

    }

    @Override
    public ComedyCategoryDto findById(Integer id) {
        ComedyCategory uploadCategory = uploadCategoryRepo.findOne(id);
        return uploadCategory == null ? null : new ComedyCategoryDto(uploadCategory);
    }
}
