package com.realroofers.lovappy.service.core;

import java.util.List;

/**
 * Created by Manoj on 17/12/2017.
 */
public interface AbstractService<T, ID> {

    List<T> findAll();

    List<T> findAllActive();

    T create(T t) throws Exception;

    T update(T t) throws Exception;

    void delete(ID id) throws Exception;

    T findById(ID id);
}
