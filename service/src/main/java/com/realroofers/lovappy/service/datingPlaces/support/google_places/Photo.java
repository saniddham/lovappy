package com.realroofers.lovappy.service.datingPlaces.support.google_places;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.InputStream;

/**
 * Represents a referenced photo.
 */
public class Photo {
    private final String reference;
    private final int width, height;
    private InputStream image;

    protected Photo(String reference, int width, int height) {
        this.reference = reference;
        this.width = width;
        this.height = height;
    }

    /**
     * Returns the input stream of the image
     * must be called prior to calling this.
     *
     * @return input stream
     */
    public InputStream getInputStream() {
        return image;
    }

    /**
     * Returns an Image from the specified photo reference.
     *
     * @return image
     */
    public BufferedImage getImage() {
        try {
            return ImageIO.read(image);
        } catch (Exception e) {
            //throw new GooglePlacesException(e);
            return null;
        }
    }

    /**
     * Returns the reference token to the photo.
     *
     * @return reference token
     */
    public String getReference() {
        return reference;
    }

    /**
     * Returns the width of the photo.
     *
     * @return photo width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns the height of the photo.
     *
     * @return photo height
     */
    public int getHeight() {
        return height;
    }
}
