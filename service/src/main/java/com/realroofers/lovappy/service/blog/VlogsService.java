package com.realroofers.lovappy.service.blog;

import com.realroofers.lovappy.service.blog.dto.VlogsDto;
import com.realroofers.lovappy.service.blog.model.Post;
import com.realroofers.lovappy.service.blog.support.PostStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Daoud Shaheen on 8/8/2017.
 */
public interface VlogsService {

    VlogsDto createVlogs(VlogsDto vlogsDto);

    VlogsDto updateVlogs(VlogsDto vlogsDto);

    Page<VlogsDto> getVlogsList(int page, int limit);

    Page<VlogsDto> getSortedVlogsList(int page, int limit, Sort.Direction sorting);
    Page<VlogsDto> getSortedVlogsList(Pageable pageable);
    Page<VlogsDto> getActiveVlogsList(int page, int limit);

    void deleteVlogsById(Integer vlogsId);

    void updateStateById(PostStatus state, Integer vlogId);

    VlogsDto getVlogsById(Integer vlogsId);

    VlogsDto getActiveVlogsById(Integer vlogsId);

    void updateViewCount(Integer viewCount, Integer ID);
}
