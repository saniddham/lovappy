package com.realroofers.lovappy.service.mail.impl;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.*;
import com.google.common.base.Strings;
import com.realroofers.lovappy.service.mail.MailService;
import com.realroofers.lovappy.service.user.dto.AdminInvitationDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.PasswordResetRequest;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.repo.PasswordResetRequestRepo;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.UUID;
import java.util.function.BiFunction;


@Service
public class MailServiceImpl implements MailService {
    private UserRepo userRepo;
    private PasswordResetRequestRepo passwordResetRequestRepo;

    private static Logger LOG = LoggerFactory.getLogger(MailServiceImpl.class);

    @Value("${amazon.ses.accessKey}")
    private String amazonSesAccessKey;
    @Value("${amazon.ses.secret}")
    private String amazonSesSecret;

    private AmazonSimpleEmailServiceClient amazonSimpleEmailServiceClient;

    @Autowired
    public MailServiceImpl(UserRepo userRepo, PasswordResetRequestRepo passwordResetRequestRepo) {
        this.userRepo = userRepo;
        this.passwordResetRequestRepo = passwordResetRequestRepo;
    }

    @PostConstruct
    public void init() {
        AWSCredentials credentials = new BasicAWSCredentials(amazonSesAccessKey, amazonSesSecret);

        amazonSimpleEmailServiceClient = (AmazonSimpleEmailServiceClient) AmazonSimpleEmailServiceClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.US_EAST_1)
                .build();

    }

    @Override
    @Transactional
    public void sendResetEmail(String email, String subject,  BiFunction<User, PasswordResetRequest, String> contentGenerator) {
        try {
            String token = UUID.randomUUID().toString();

            User user = userRepo.findOneByEmail(email);
            if (user != null) {
                PasswordResetRequest request = new PasswordResetRequest();
                request.setUserID(user.getUserId());
                request.setToken(token);
                request.setCreatedAt(new Date());

                passwordResetRequestRepo.save(request);

                String content = contentGenerator.apply(user, request);

                sendMail(email, content, subject);
            }
        } catch (UsernameNotFoundException e) {
            LOG.error(e.getMessage());
        }
    }

    @Override
    public void sendMail(String recipient, String html, String subject) {
        send(recipient, html, subject, null, null);
    }

    @Override
    public void sendMail(String recipient, String html, String subject, String fromEmail, String fromName) {
        send(recipient, html, subject, fromEmail, fromName);
    }

    @Override
    public void sendInvitationEmail(String email, UserDto user, BiFunction<UserDto, AdminInvitationDto, String> contentGenerator) {
        try {

                AdminInvitationDto request = new AdminInvitationDto();
                request.setEmail(email);
                request.setPassword(user.getPassword());
                request.setUsername(user.getFirstName() + " " + user.getLastName());



                String content = contentGenerator.apply(user, request);

                sendMail(email, content, "Admin Invitation");

        } catch (UsernameNotFoundException e) {
            LOG.error(e.getMessage());
        }
    }


    public void send(String recipient, String html, String subject, String fromEmail, String fromName){
        if(Strings.isNullOrEmpty(recipient))
            return;

        fromEmail = fromEmail == null ? "no-reply@lovappy.com" : fromEmail;
        fromName = fromName == null ? "Lovappy" : fromName;
        String source = fromName + " <" + fromEmail + ">";
        // Construct an object to contain the recipient address.
        Destination destination = new Destination().withToAddresses(recipient);
        // Create the subject and body of the message.
        Content subjectContent = new Content().withData(subject);
        Content htmlBody = new Content().withData(html);
        Body body = new Body().withHtml(htmlBody);
        // Create a message with the specified subject and body.
        Message message = new Message().withSubject(subjectContent).withBody(body);
        // Assemble the email.
        SendEmailRequest request = new SendEmailRequest().withSource(source).withDestination(destination).withMessage(message);
        amazonSimpleEmailServiceClient.sendEmail(request);
    }
}
