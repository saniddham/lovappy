package com.realroofers.lovappy.service.user.dto;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang3.BooleanUtils;

import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.SubscriptionType;
import com.realroofers.lovappy.service.util.ZodiacCalendar;
import org.springframework.util.StringUtils;


/**
 * @author mwiyono
 */
@EqualsAndHashCode
@ToString
public class MemberProfileDto {

	private Integer userId;

	private Gender gender;

	private Date birthDate;

	private Integer heightFt;

	private Integer heightIn;

	private CloudStorageFileDto profilePhotoCloudFile;
	private CloudStorageFileDto handsCloudFile;
	private CloudStorageFileDto feetCloudFile;
	private CloudStorageFileDto legsCloudFile;
	
	private boolean handsFileSkipped;
	
	private boolean feetFileSkipped;
	
	private boolean legsFileSkipped;

	private Map<String, String> personalities=new HashMap<>();

	private Map<String, String> lifestyles=new HashMap<>();

	private Map<String, String> statuses=new HashMap<>();

	private SubscriptionType subscriptionType;

	private Boolean iliked=false;

	private Boolean matches=false;

    private List<UserDto> blockedUsers=new ArrayList<UserDto>();

    private List<Integer> blockedUserIds=new ArrayList<Integer>();
    
    private String zodiac;

    private String zodiacIcon;

    private AddressDto address;

	public MemberProfileDto() {
		super();
	}

	public MemberProfileDto(UserProfile model) {
		super();
		this.userId = model.getUserId();
		this.gender = model.getGender();
		this.birthDate = model.getBirthDate();
		this.heightFt = model.getHeightFt();
		this.heightIn = model.getHeightIn();

		this.handsCloudFile = new CloudStorageFileDto(model.getHandsCloudFile());
		this.feetCloudFile = new CloudStorageFileDto(model.getFeetCloudFile());
		this.legsCloudFile = new CloudStorageFileDto(model.getLegsCloudFile());
        this.profilePhotoCloudFile = new CloudStorageFileDto(model.getProfilePhotoCloudFile());
		this.handsFileSkipped = BooleanUtils.toBoolean(model.getHandsFileSkipped());
		this.feetFileSkipped = BooleanUtils.toBoolean(model.getFeetFileSkipped());
		this.legsFileSkipped = BooleanUtils.toBoolean(model.getLegsFileSkipped());

		//TODO enhance this will cause memory issue
		if (model.getBlockedUsers()!=null){
			List<User> users=model.getBlockedUsers();
			for (User user : users) {
				// do we need to populated as an object? where it will consume more memory? 
				// Id List is sufficient
//				this.blockedUsers.add(new UserDto(user));
				blockedUserIds.add( user.getUserId());
			}
		}
		
		if (getZodiac()!=null){
			this.zodiacIcon= getZodiac()+".png";
		}

		
		if (model.getPersonalities() != null) {
			this.personalities = new HashMap<>(model.getPersonalities());
		}
		if (model.getLifestyles() != null) {
			this.lifestyles = new HashMap<>(model.getLifestyles());
		}
		if (model.getStatuses() != null) {
			this.statuses = new HashMap<>(model.getStatuses());
		}

		this.subscriptionType = model.getSubscriptionType();

		if(model.getAddress() != null)
			this.address = AddressDto.toAddressDto(model.getAddress());
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Gender getGender() {
		return gender;
	}

	public String getProfilePhotoCloudFileUrl() {
		return this.profilePhotoCloudFile == null || StringUtils.isEmpty(this.profilePhotoCloudFile.getUrl())
				? "/images/icons/upload_icons/upload_profile_sample.png"
				: this.profilePhotoCloudFile.getUrl();
	}

	public String getGenderIcon() {
		if  (gender!=null && gender.equals(Gender.FEMALE)){
			return "girl_icon.png";
		} else if  (gender!=null && gender.equals(Gender.MALE)){
			return "boy_icon.png";
		} else if  (gender!=null && gender.equals(Gender.BOTH)){
			return "both_gender.png";
		}
		return "girl_icon.png";
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Integer getHeightFt() {
		return heightFt;
	}

	public void setHeightFt(Integer heightFt) {
		this.heightFt = heightFt;
	}

	public Integer getHeightIn() {
		return heightIn;
	}

	public void setHeightIn(Integer heightIn) {
		this.heightIn = heightIn;
	}

	public CloudStorageFileDto getProfilePhotoCloudFile() {
		return profilePhotoCloudFile;
	}

	public void setProfilePhotoCloudFile(CloudStorageFileDto profilePhotoCloudFile) {
		this.profilePhotoCloudFile = profilePhotoCloudFile;
	}

	public CloudStorageFileDto getHandsCloudFile() {
		return handsCloudFile;
	}

	public String getHandsCloudFileUrl(Boolean myProfile) {
		return this.handsCloudFile == null || StringUtils.isEmpty(this.handsCloudFile.getUrl())
				? (myProfile ? "/images/no_hand_photo.png" : "/images/photo-error.png")
				: this.handsCloudFile.getUrl();
	}

	public void setHandsCloudFile(CloudStorageFileDto handsCloudFile) {
		this.handsCloudFile = handsCloudFile;
	}

	public CloudStorageFileDto getFeetCloudFile() {
		return feetCloudFile;
	}

	public String getFeetCloudFileUrl(Boolean myProfile) {
		return this.feetCloudFile == null || StringUtils.isEmpty(this.feetCloudFile.getUrl())
				? (myProfile ? "/images/no_foot_photo.png" : "/images/photo-error.png")
				: this.feetCloudFile.getUrl();
	}

	public void setFeetCloudFile(CloudStorageFileDto feetCloudFile) {
		this.feetCloudFile = feetCloudFile;
	}

	public CloudStorageFileDto getLegsCloudFile() {
		return legsCloudFile;
	}

	public String getLegsCloudFileUrl(Boolean myProfile) {
		return this.legsCloudFile == null || StringUtils.isEmpty(this.legsCloudFile.getUrl())
				? (myProfile ? "/images/no_legs_photo.png" : "/images/photo-error.png")
				: this.legsCloudFile.getUrl();
	}

	public void setLegsCloudFile(CloudStorageFileDto legsCloudFile) {
		this.legsCloudFile = legsCloudFile;
	}

	public boolean isHandsFileSkipped() {
		return handsFileSkipped;
	}

	public void setHandsFileSkipped(boolean handsFileSkipped) {
		this.handsFileSkipped = handsFileSkipped;
	}

	public boolean isFeetFileSkipped() {
		return feetFileSkipped;
	}

	public void setFeetFileSkipped(boolean feetFileSkipped) {
		this.feetFileSkipped = feetFileSkipped;
	}

	public boolean isLegsFileSkipped() {
		return legsFileSkipped;
	}

	public void setLegsFileSkipped(boolean legsFileSkipped) {
		this.legsFileSkipped = legsFileSkipped;
	}

	public Map<String, String> getPersonalities() {
		return personalities;
	}

	public void setPersonalities(Map<String, String> personalities) {
		this.personalities = personalities;
	}

	public Map<String, String> getLifestyles() {
		return lifestyles;
	}

	public void setLifestyles(Map<String, String> lifestyles) {
		this.lifestyles = lifestyles;
	}

	public Map<String, String> getStatuses() {
		return statuses;
	}

	public void setStatuses(Map<String, String> statuses) {
		this.statuses = statuses;
	}

	public SubscriptionType getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(SubscriptionType subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public String getAge() {
		if (this.birthDate == null) {
			return "";
		}
		Calendar today = Calendar.getInstance();
		Calendar birthDate = Calendar.getInstance();
		birthDate.setTime(this.birthDate);
		if (birthDate.after(today)) {
			throw new IllegalArgumentException("You don't exist yet");
		}
		int todayYear = today.get(Calendar.YEAR);
		int birthDateYear = birthDate.get(Calendar.YEAR);
		int todayDayOfYear = today.get(Calendar.DAY_OF_YEAR);
		int birthDateDayOfYear = birthDate.get(Calendar.DAY_OF_YEAR);
		int todayMonth = today.get(Calendar.MONTH);
		int birthDateMonth = birthDate.get(Calendar.MONTH);
		int todayDayOfMonth = today.get(Calendar.DAY_OF_MONTH);
		int birthDateDayOfMonth = birthDate.get(Calendar.DAY_OF_MONTH);
		int age = todayYear - birthDateYear;

		// If birth date is greater than todays date (after 2 days adjustment of
		// leap year) then decrement age one year
		if ((birthDateDayOfYear - todayDayOfYear > 3) || (birthDateMonth > todayMonth)) {
			age--;

			// If birth date and todays date are of same month and birth day of
			// month is greater than todays day of month then decrement age
		} else if ((birthDateMonth == todayMonth) && (birthDateDayOfMonth > todayDayOfMonth)) {
			age--;
		}
		if (age==0){
			return "";
		} else 
		return age+"";
	}

	//doesThis member like me?
	public Boolean getIliked() {
		return iliked;
	}

	public void setIliked(Boolean iliked) {
		this.iliked = iliked;
	}

	public Boolean getMatches() {
		return matches;
	}

	public void setMatches(Boolean matches) {
		this.matches = matches;
	}

	public List<UserDto> getBlockedUsers() {
		return blockedUsers;
	}

	public void setBlockedUsers(List<UserDto> blockedUsers) {
		this.blockedUsers = blockedUsers;
	}


	public Boolean getBlockStatus(Integer memberId) {
		Boolean result=false;
		if (blockedUserIds!=null && blockedUserIds.size()>0){
			for (Integer blockedUserId : blockedUserIds) {
				if (blockedUserId.equals(memberId)){
					result=true;
					break;
				}
			}
			
		}
		return result;
	}


	public String getZodiac() {
		this.zodiac = (birthDate!=null?ZodiacCalendar.getZodiac(birthDate):null);
		return zodiac;
	}
	

	public String getZodiacIcon() {
		if (getZodiac() !=null){
			return getZodiac()+".png";
		} else {
			return null;
		}
	}

	public void setZodiac(String zodiac) {
		this.zodiac = zodiac;
	}

	public void setZodiacIcon(String zodiacIcon) {
		this.zodiacIcon = zodiacIcon;
	}

	public AddressDto getAddress() {
		return address;
	}

	public void setAddress(AddressDto address) {
		this.address = address;
	}
}
