package com.realroofers.lovappy.service.datingPlaces.model;

import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Darrel Rayen on 8/14/18.
 */
@Entity
@EqualsAndHashCode
public class DatingPlaceAtmospheres implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "place_id")
    private DatingPlace placeId;

    @Id
    @ManyToOne
    @JoinColumn(name = "atmosphere_id")
    private PlaceAtmosphere atmosphere;

    public DatingPlaceAtmospheres() {
    }

    public DatingPlace getPlaceId() {
        return placeId;
    }

    public void setPlaceId(DatingPlace placeId) {
        this.placeId = placeId;
    }

    public PlaceAtmosphere getAtmosphere() {
        return atmosphere;
    }

    public void setAtmosphere(PlaceAtmosphere atmosphere) {
        this.atmosphere = atmosphere;
    }
}
