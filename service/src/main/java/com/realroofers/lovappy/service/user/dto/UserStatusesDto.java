package com.realroofers.lovappy.service.user.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by Daoud Shaheen on 6/18/2018.
 */
@Data
public class UserStatusesDto implements Serializable{
    private String employed;
    private String risktaker;
    private String believeingod;
    private String wantkids;
    private String havekids;
    private String attractmany;

    public UserStatusesDto() {
    }

    public UserStatusesDto(Map<String, String> statuses) {
        this.employed = statuses.get("employed");
        this.risktaker = statuses.get("risktaker");
        this.believeingod = statuses.get("believeingod");
        this.wantkids = statuses.get("wantkids");
        this.havekids = statuses.get("havekids");
        this.attractmany = statuses.get("attractmany");
    }
}
