package com.realroofers.lovappy.service.cms;

import com.realroofers.lovappy.service.cms.dto.PageMetaDTO;

public interface MetaDataService {

    PageMetaDTO findPageMetaByTag(String pageTag);
}
