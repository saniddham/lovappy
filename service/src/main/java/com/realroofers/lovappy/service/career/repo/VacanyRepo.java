package com.realroofers.lovappy.service.career.repo;

import com.realroofers.lovappy.service.career.model.Vacancy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface VacanyRepo extends JpaRepository<Vacancy, Long> {
    Page<Vacancy> findByStatus(String status, Pageable pageable);
    @Query("Select c from Vacancy c where c.jobTitle LIKE  %?1% or c.positionDescription LIKE %?1%")
    Page<Vacancy> search(String valueis, Pageable pageable);
}
