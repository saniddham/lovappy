package com.realroofers.lovappy.service.gift.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.gift.dto.ViewedGiftDto;
import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Manoj on 02/02/2018.
 */
@Entity
@Data
@Table(name = "gift_viewed")
@EqualsAndHashCode
public class ViewedGift extends BaseEntity<Integer> implements Serializable {

    @JoinColumn(name = "product")
    @ManyToOne
    Product product;

    @JoinColumn(name = "user")
    @ManyToOne
    private User user;

    @CreationTimestamp
    @Column(name = "viewed_on", columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date viewedOn;

    public ViewedGift() {
    }

    public ViewedGift(ViewedGiftDto viewedGift) {
        if(viewedGift.getId()!=null) {
            this.setId(viewedGift.getId());
            viewedOn = viewedGift.getViewedOn();
        }else{
            viewedOn = new Date();
        }
        product = viewedGift.getProduct();
        user = new User(viewedGift.getUser().getID());
    }
}
