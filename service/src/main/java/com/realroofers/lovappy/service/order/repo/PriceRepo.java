package com.realroofers.lovappy.service.order.repo;

import com.realroofers.lovappy.service.order.model.Price;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Daoud Shaheen on 6/1/2018.
 */
public interface PriceRepo extends JpaRepository<Price, Integer> {
    Price findByType(OrderDetailType type);
}
