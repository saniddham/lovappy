package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampDto;
import com.realroofers.lovappy.service.lovstamps.model.Lovstamp;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.model.UserProfile;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.util.StringUtils;

/**
 * Created by Daoud Shaheen on 3/30/2018.
 */
@Data
@EqualsAndHashCode
public class UserReviewDto {
    private Integer userId;
    private LovstampDto lovstamp;

    private String email;

    private CloudStorageFileDto handsCloudFile;
    private CloudStorageFileDto feetCloudFile;
    private CloudStorageFileDto legsCloudFile;
    private CloudStorageFileDto profilePictureFile;
    private Boolean profileVisibility;

    private Boolean handsFileSkipped;
    private Boolean feetFileSkipped;
    private Boolean legsFileSkipped;

//    public UserReviewDto(User user, Lovstamp lovstamp) {
//        if (user != null) {
//            this.userId = user.getUserId();
//            this.email = user.getEmail();
//            UserProfile userProfile = user.getUserProfile();
//            this.handsCloudFile = new CloudStorageFileDto(userProfile.getHandsCloudFile());
//            this.feetCloudFile = new CloudStorageFileDto(userProfile.getFeetCloudFile());
//            this.legsCloudFile = new CloudStorageFileDto(userProfile.getLegsCloudFile());
//            this.profilePictureFile = new CloudStorageFileDto(userProfile.getProfilePhotoCloudFile());
//            this.profileVisibility = userProfile.getAllowedProfilePic();
//            this.handsFileSkipped = userProfile.getHandsFileSkipped();
//            this.feetFileSkipped = userProfile.getFeetFileSkipped();
//            this.legsFileSkipped = userProfile.getLegsFileSkipped();
//            this.lovstamp  = new LovstampDto();
//            if(lovstamp != null){
//            this.lovstamp.setAudioFileCloud(new CloudStorageFileDto(lovstamp.getAudioFileCloud()));
//            this.lovstamp.setNumberOfFemaleListeners(lovstamp.getNumberOfFemaleListeners());
//            this.lovstamp.setNumberOfMaleListeners(lovstamp.getNumberOfMaleListeners());
//            this.lovstamp.setNumberOfListeners(lovstamp.getNumberOfListeners());
//            this.lovstamp.setShowHome(lovstamp.getShowInHome());
//            this.lovstamp.setId(lovstamp.getId());
//        }
//        }
//    }


    public UserReviewDto(Lovstamp lovstamp) {
        if (lovstamp != null) {
            User user = lovstamp.getUser();
            this.userId = user.getUserId();
            this.email = user.getEmail();
            UserProfile userProfile = user.getUserProfile();
            this.handsCloudFile = new CloudStorageFileDto(userProfile.getHandsCloudFile());
            this.feetCloudFile = new CloudStorageFileDto(userProfile.getFeetCloudFile());
            this.legsCloudFile = new CloudStorageFileDto(userProfile.getLegsCloudFile());
            this.profilePictureFile = new CloudStorageFileDto(userProfile.getProfilePhotoCloudFile());
            this.profileVisibility = userProfile.getAllowedProfilePic();
            this.handsFileSkipped = userProfile.getHandsFileSkipped();
            this.feetFileSkipped = userProfile.getFeetFileSkipped();
            this.legsFileSkipped = userProfile.getLegsFileSkipped();
            this.lovstamp  = new LovstampDto();
            this.lovstamp.setAudioFileCloud(new CloudStorageFileDto(lovstamp.getAudioFileCloud()));
            this.lovstamp.setNumberOfFemaleListeners(lovstamp.getNumberOfFemaleListeners());
            this.lovstamp.setNumberOfMaleListeners(lovstamp.getNumberOfMaleListeners());
            this.lovstamp.setNumberOfListeners(lovstamp.getNumberOfListeners());
            this.lovstamp.setShowHome(lovstamp.getShowInHome());
            this.lovstamp.setId(lovstamp.getId());

        }
    }

    public String getProfilePhotoCloudFileUrl() {
        return this.profilePictureFile == null || StringUtils.isEmpty(this.profilePictureFile.getUrl())
                ? "/images/photo-error.png"
                : this.profilePictureFile.getUrl();
    }


    public String getPhotoIdentificationCloudFileUrl() {
        return this.profilePictureFile == null || StringUtils.isEmpty(this.profilePictureFile.getUrl())
                ? "/images/photo-error.png"
                : this.profilePictureFile.getUrl();
    }


    public String getHandsCloudFileUrl() {
        if (handsFileSkipped != null && handsFileSkipped)
            return "/images/skipped_hand_photo.png";

        return this.handsCloudFile == null || StringUtils.isEmpty(this.handsCloudFile.getUrl())
                ? "/images/photo-error.png"
                : this.handsCloudFile.getUrl();
    }

    public CloudStorageFileDto getHandsCloudFile() {
        return handsCloudFile;
    }

    public void setHandsCloudFile(CloudStorageFileDto handsCloudFile) {
        this.handsCloudFile = handsCloudFile;
    }

    public String getFeetCloudFileUrl() {
        if (feetFileSkipped != null && feetFileSkipped)
            return "/images/skipped_legs_photo.png";

        return this.feetCloudFile == null || StringUtils.isEmpty(this.feetCloudFile.getUrl())
                ? "/images/photo-error.png"
                : this.feetCloudFile.getUrl();
    }

    public String getLegsCloudFileUrl() {
        if (legsFileSkipped != null && legsFileSkipped)
            return "/images/skipped_legs_photo.png";

        return this.legsCloudFile == null || StringUtils.isEmpty(this.legsCloudFile.getUrl())
                ? "/images/photo-error.png"
                : this.legsCloudFile.getUrl();
    }
    public Boolean profilePhotoCloudFileExists() {
        return !(this.profilePictureFile == null || StringUtils.isEmpty(this.profilePictureFile.getUrl()));
    }

    public Boolean feetCloudFileExists() {
        if (feetFileSkipped != null && feetFileSkipped)
            return false;
        return !(this.feetCloudFile == null || StringUtils.isEmpty(this.feetCloudFile.getUrl()));
    }

    public Boolean handsCloudFileExists() {
        if (handsFileSkipped != null && handsFileSkipped)
            return false;

        return !(this.handsCloudFile == null || StringUtils.isEmpty(this.handsCloudFile.getUrl()));
    }

    public Boolean legsCloudFileExists() {
        if (legsFileSkipped != null && legsFileSkipped)
            return false;

        return !(this.legsCloudFile == null || StringUtils.isEmpty(this.legsCloudFile.getUrl()));
    }

}
