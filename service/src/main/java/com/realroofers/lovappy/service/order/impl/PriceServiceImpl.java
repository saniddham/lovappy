package com.realroofers.lovappy.service.order.impl;

import com.realroofers.lovappy.service.order.PriceService;
import com.realroofers.lovappy.service.order.dto.PriceDto;
import com.realroofers.lovappy.service.order.model.Price;
import com.realroofers.lovappy.service.order.repo.PriceRepo;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Daoud Shaheen on 6/1/2018.
 */
@Service
public class PriceServiceImpl implements PriceService {

    private final PriceRepo priceRepo;

    public PriceServiceImpl(PriceRepo priceRepo) {
        this.priceRepo = priceRepo;
    }

    @Transactional(readOnly = true)
    @Override
    public Page<PriceDto> getAll(Pageable pageable) {
        return priceRepo.findAll(pageable).map(PriceDto::new);
    }

    @Transactional
    @Override
    public void update(PriceDto priceDto) {
          Price price = priceRepo.findOne(priceDto.getId());
          if(price != null){
             price.setPrice(priceDto.getPrice());
             priceRepo.save(price);
          }
    }

    @Transactional(readOnly = true)
    @Override
    public PriceDto get(Integer id) {
        Price price = priceRepo.findOne(id);
        return price == null? null : new PriceDto(price);
    }

    @Override
    public PriceDto getByType(OrderDetailType type) {
        Price price = priceRepo.findByType(type);
        return price == null? null : new PriceDto(price);
    }
}
