package com.realroofers.lovappy.service.plan.dto;

import com.realroofers.lovappy.service.plan.model.Plan;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * Created by hasan on 7/11/2017.
 */
@EqualsAndHashCode
public class PlansDto {
    @NotNull
    private Integer planID;
    @NotNull(message = "Title is mandatory.")
    private String planName;
    @NotNull(message = "Add a subtitle to the plan.")
    private String subTitle;
    @NotNull
    private Boolean isValid;

    private String colorScheme;

    public PlansDto() {
    }

    public PlansDto(Plan plan) {
        this.planID = plan.getPlanID();
        this.planName = plan.getPlanName();
        this.subTitle = plan.getPlanSubtitle();
        this.colorScheme = plan.getColorScheme();
        this.isValid = plan.getIsValid();
    }

    public PlansDto(Integer planID, String planName, String subTitle, String colorScheme, Boolean isValid) {
        this.planID = planID;
        this.planName = planName;
        this.subTitle = subTitle;
        this.colorScheme = colorScheme;
        this.isValid = isValid;
    }

    public Integer getPlanID() {
        return planID;
    }

    public void setPlanID(Integer planID) {
        this.planID = planID;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getColorScheme() {
        return colorScheme;
    }

    public void setColorScheme(String colorScheme) {
        this.colorScheme = colorScheme;
    }

    public Boolean getValid() {
        return isValid;
    }

    public void setValid(Boolean valid) {
        isValid = valid;
    }
}
