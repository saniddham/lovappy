package com.realroofers.lovappy.service.questions.dto;

import com.realroofers.lovappy.service.questions.model.DailyQuestion;
import com.realroofers.lovappy.service.questions.model.UserResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darrel Rayen on 9/18/18.
 */
@Data
@EqualsAndHashCode
public class DailyQuestionDto implements Serializable {

    private Long questionId;
    private String question;
    private Long questionDate;
    private List<UserResponseDto> userResponseList;

    public DailyQuestionDto() {
    }

    public DailyQuestionDto(DailyQuestion dailyQuestion) {
        this.questionId = dailyQuestion.getId();
        this.question = dailyQuestion.getQuestion();
        this.questionDate = dailyQuestion.getQuestionDate().getTime();
        this.userResponseList = mapToResponseDto(dailyQuestion.getUserResponses());
    }

    public DailyQuestionDto(DailyQuestion dailyQuestion, Integer userId) {
        this.questionId = dailyQuestion.getId();
        this.question = dailyQuestion.getQuestion();
        this.questionDate = dailyQuestion.getQuestionDate().getTime();
        this.userResponseList = mapToResponseDto(dailyQuestion.getUserResponses(), userId);
    }

    private List<UserResponseDto> mapToResponseDto(List<UserResponse> userResponses) {
        List<UserResponseDto> responseList = new ArrayList<>();
        if (userResponses != null) {
            userResponses.forEach(response -> {
                responseList.add(new UserResponseDto(response));
            });
        }
        return responseList;
    }

    private List<UserResponseDto> mapToResponseDto(List<UserResponse> userResponses, Integer userId) {
        List<UserResponseDto> responseList = new ArrayList<>();
        if (userResponses != null) {
            userResponses.forEach(response -> {
                if (response.getUser().getUserId().equals(userId)) {
                    responseList.add(new UserResponseDto(response));
                }
            });
        }
        return responseList;
    }

}
