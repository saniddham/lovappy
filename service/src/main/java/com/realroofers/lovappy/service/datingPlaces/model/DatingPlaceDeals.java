package com.realroofers.lovappy.service.datingPlaces.model;

import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Darrel Rayen on 1/22/18.
 */
@Entity
@Table(name = "dating_place_deals")
@EqualsAndHashCode
public class DatingPlaceDeals implements Serializable{

    @Id
    @GeneratedValue
    private Integer placeDealId;

    private Double fullAmount;
    private Double discountValue;
    private Date startDate;
    private Date expiryDate;
    private String dealDescription;
    private Double dealPrice;

    @Enumerated(EnumType.STRING)
    private ApprovalStatus approvalStatus;

    @Column(name = "is_active", columnDefinition = "BIT(1) default 0")
    private Boolean isActive;

    @Column(name = "approved", columnDefinition = "BIT(1) default 0")
    private Boolean approved;

    @ManyToOne
    @JoinColumn(name = "place_id")
    private DatingPlace datingPlace;

    @ManyToOne
    @JoinColumn(name = "deal_id")
    private Deals deals;

    public DatingPlaceDeals() {
    }

    public Integer getPlaceDealId() {
        return placeDealId;
    }

    public void setPlaceDealId(Integer placeDealId) {
        this.placeDealId = placeDealId;
    }

    public Double getFullAmount() {
        return fullAmount;
    }

    public void setFullAmount(Double fullAmount) {
        this.fullAmount = fullAmount;
    }

    public Double getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(Double discountValue) {
        this.discountValue = discountValue;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getDealDescription() {
        return dealDescription;
    }

    public void setDealDescription(String dealDescription) {
        this.dealDescription = dealDescription;
    }

    public ApprovalStatus getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(ApprovalStatus approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public Double getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(Double dealPrice) {
        this.dealPrice = dealPrice;
    }

    public DatingPlace getDatingPlace() {
        return datingPlace;
    }

    public void setDatingPlace(DatingPlace datingPlace) {
        this.datingPlace = datingPlace;
    }

    public Deals getDeals() {
        return deals;
    }

    public void setDeals(Deals deals) {
        this.deals = deals;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }
}
