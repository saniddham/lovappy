package com.realroofers.lovappy.service.event.model;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Darrel Rayen on 8/30/17.
 */
@Entity
@Table(name = "event_user_picture")
@EqualsAndHashCode
public class EventUserPicture {

    @Id
    @GeneratedValue
    @Column
    private Integer id;

    @ManyToOne
    @JoinColumn(name ="event_id")
    private Event eventId;

    @ManyToOne
    @JoinColumn(name ="user_id")
    private User userId;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile picture;

    public EventUserPicture() {
    }
    public EventUserPicture(Event eventId, User userId, CloudStorageFile picture) {
        this.eventId =eventId;
        this.userId =userId;
        this.picture = picture;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Event getEventId() {
        return eventId;
    }

    public void setEventId(Event eventId) {
        this.eventId = eventId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public CloudStorageFile getPicture() {
        return picture;
    }

    public void setPicture(CloudStorageFile picture) {
        this.picture = picture;
    }
}
