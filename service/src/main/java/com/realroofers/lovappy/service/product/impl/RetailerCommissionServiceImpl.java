package com.realroofers.lovappy.service.product.impl;

import com.realroofers.lovappy.service.product.RetailerCommissionService;
import com.realroofers.lovappy.service.product.dto.RetailerCommissionDto;
import com.realroofers.lovappy.service.product.model.RetailerCommission;
import com.realroofers.lovappy.service.product.repo.RetailerCommissionRepo;
import com.realroofers.lovappy.service.user.model.Country;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class RetailerCommissionServiceImpl implements RetailerCommissionService {

    private final RetailerCommissionRepo retailerCommissionRepo;

    public RetailerCommissionServiceImpl(RetailerCommissionRepo retailerCommissionRepo) {
        this.retailerCommissionRepo = retailerCommissionRepo;
    }

    @Transactional(readOnly = true)
    @Override
    public List<RetailerCommissionDto> findAll() {
        List<RetailerCommissionDto> list = new ArrayList<>();
        retailerCommissionRepo.findAll().stream().forEach(commission -> list.add(new RetailerCommissionDto(commission)));
        return list;
    }

    @Transactional(readOnly = true)
    @Override
    public List<RetailerCommissionDto> findAllActive() {
        List<RetailerCommissionDto> list = new ArrayList<>();
        retailerCommissionRepo.findAllByActiveIsTrue().stream().forEach(commission -> list.add(new RetailerCommissionDto(commission)));
        return list;
    }

    @Transactional
    @Override
    public RetailerCommissionDto create(RetailerCommissionDto retailerCommissionDto) throws Exception {
        return new RetailerCommissionDto(retailerCommissionRepo.saveAndFlush(new RetailerCommission(retailerCommissionDto)));
    }

    @Transactional
    @Override
    public RetailerCommissionDto update(RetailerCommissionDto retailerCommissionDto) throws Exception {
        return new RetailerCommissionDto(retailerCommissionRepo.save(new RetailerCommission(retailerCommissionDto)));
    }

    @Transactional
    @Override
    public void delete(Integer integer) throws Exception {
        RetailerCommissionDto dto = findById(integer);
        if (dto != null) {
            dto.setActive(false);
            update(dto);
        }
    }

    @Override
    public RetailerCommissionDto findById(Integer integer) {
        RetailerCommission one = retailerCommissionRepo.findOne(integer);
        return one != null ? new RetailerCommissionDto(one) : null;
    }

    @Override
    public Page<RetailerCommission> findAll(Pageable pageable) {
        return retailerCommissionRepo.findAll(pageable);
    }

    @Override
    public RetailerCommissionDto getCurrentCommission(Country country) {
        RetailerCommission one = retailerCommissionRepo.findTopByActiveIsTrueAndCountryOrderByAffectiveDateAsc(country);
        return one != null ? new RetailerCommissionDto(one) : null;
    }
}
