package com.realroofers.lovappy.service.cms.impl;

import com.realroofers.lovappy.service.cms.MetaDataService;
import com.realroofers.lovappy.service.cms.dto.PageMetaDTO;
import com.realroofers.lovappy.service.cms.model.Page;
import com.realroofers.lovappy.service.cms.model.PageMetaData;
import com.realroofers.lovappy.service.cms.repo.PageMetaDataRepository;
import com.realroofers.lovappy.service.cms.repo.PageRepository;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.system.repo.LanguageRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service("metaDataService")
public class MetaDataServiceImpl implements MetaDataService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MetaDataServiceImpl.class);
    private final PageRepository pageRepository;
    private final PageMetaDataRepository pageMetaDataRepository;
    private final LanguageRepo languageRepo;

    public MetaDataServiceImpl(PageRepository pageRepository, PageMetaDataRepository pageMetaDataRepository, LanguageRepo languageRepo) {
        this.pageRepository = pageRepository;
        this.pageMetaDataRepository = pageMetaDataRepository;
        this.languageRepo = languageRepo;
    }


    @Override
    public PageMetaDTO findPageMetaByTag(String pageTag) {
        Page page = pageRepository.findByTag(pageTag);

        Locale locale = LocaleContextHolder.getLocale();

        String lang=locale.getLanguage().trim().toUpperCase();

        Language language;
        if(lang!=null && !lang.isEmpty()){
            language = languageRepo.findByAbbreviation(lang);
        }else {
            language = languageRepo.findByAbbreviation("EN");
        }

        PageMetaData pageMetaData =pageMetaDataRepository.findByPageAndLanguage(page,language);

        return new PageMetaDTO((pageMetaData != null && pageMetaData.getTitle() != null) ? pageMetaData.getTitle() : ""
                , (pageMetaData != null && pageMetaData.getDescription() != null) ? pageMetaData.getDescription() : ""
                , (pageMetaData != null && pageMetaData.getKeywords() != null) ? pageMetaData.getKeywords() : ""
                , (pageMetaData != null && pageMetaData.getAbstractDescription() != null) ? pageMetaData.getAbstractDescription() : ""
                , (pageMetaData != null && pageMetaData.getPage() != null) ? !pageMetaData.getPage().getActive() : false);
    }
}
