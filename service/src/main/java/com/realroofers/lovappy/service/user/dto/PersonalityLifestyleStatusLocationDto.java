package com.realroofers.lovappy.service.user.dto;

import java.util.HashMap;
import java.util.Map;

import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.EqualsAndHashCode;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@EqualsAndHashCode
public class PersonalityLifestyleStatusLocationDto {
    private Integer userId;
    private Gender gender;
    private Map<String, String> personalities;
    private Map<String, String> lifestyles;
    private Map<String, String> statuses;
//    private AddressDto location;

    public PersonalityLifestyleStatusLocationDto() {

    }

    public PersonalityLifestyleStatusLocationDto(UserProfile model) {
        if (model != null) {
            this.userId = model.getUserId();
            this.gender = model.getGender();
            if (model.getPersonalities() != null) {
                this.personalities = new HashMap<>(model.getPersonalities());
            }
            if (model.getLifestyles() != null) {
                this.lifestyles = new HashMap<>(model.getLifestyles());
            }
            if (model.getStatuses() != null) {
                this.statuses = new HashMap<>(model.getStatuses());
            }
//            if (model.getAddress() != null) {
//                this.location = AddressDto.toAddressDto(model.getAddress());
//            } else {
//                this.location = new AddressDto();
//                this.location.setLastMileRadiusSearch(300);
//            }
        }
    }

    public Integer getUserId() {
        return userId;
    }

    public Gender getGender() {
        return gender;
    }

    public Map<String, String> getPersonalities() {
        return personalities;
    }

    public void setPersonalities(Map<String, String> personalities) {
        this.personalities = personalities;
    }

    public Map<String, String> getLifestyles() {
        return lifestyles;
    }

    public void setLifestyles(Map<String, String> lifestyles) {
        this.lifestyles = lifestyles;
    }

    public Map<String, String> getStatuses() {
        return statuses;
    }

    public void setStatuses(Map<String, String> statuses) {
        this.statuses = statuses;
    }

//    public AddressDto getLocation() {
//        return location;
//    }
//
//    public void setLocation(AddressDto location) {
//        this.location = location;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonalityLifestyleStatusLocationDto that = (PersonalityLifestyleStatusLocationDto) o;

        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (gender != that.gender) return false;
        if (personalities != null ? !personalities.equals(that.personalities) : that.personalities != null) return false;
        if (lifestyles != null ? !lifestyles.equals(that.lifestyles) : that.lifestyles != null) return false;
        return statuses != null ? statuses.equals(that.statuses) : that.statuses == null ;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (personalities != null ? personalities.hashCode() : 0);
        result = 31 * result + (lifestyles != null ? lifestyles.hashCode() : 0);
        result = 31 * result + (statuses != null ? statuses.hashCode() : 0);
        return result;
    }
}
