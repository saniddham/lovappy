package com.realroofers.lovappy.service.mail.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.mail.support.EmailTypes;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by Daoud Shaheen on 9/6/2017.
 */
@Entity
@Table(name = "email_templates")
@DynamicUpdate
@EqualsAndHashCode
public class EmailTemplate extends BaseEntity<Integer> {

    private String name;

    @UpdateTimestamp
    private Date updated;

    private String templateUrl;

    private String fromEmail;

    private String imageUrl;

    private String fromName;

    @Enumerated(EnumType.STRING)
    private EmailTypes type;

    @Column(name = "enabled", columnDefinition = "BIT(1) default 0")
    private Boolean enabled;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getTemplateUrl() {
        return templateUrl;
    }

    public void setTemplateUrl(String templateUrl) {
        this.templateUrl = templateUrl;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public EmailTypes getType() {
        return type;
    }

    public void setType(EmailTypes type) {
        this.type = type;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
