package com.realroofers.lovappy.service.util;

import com.google.common.primitives.Doubles;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampDto;

import java.util.Comparator;

/**
 * Created by Eias Altawil on 7/8/2017
 */
public class LocationComparator implements Comparator<LovstampDto> {

    private Double originLat;
    private Double originLon;
    public LocationComparator(Double originLat, Double originLon){
        this.originLat = originLat;
        this.originLon = originLon;
    }

    @Override
    public int compare(LovstampDto o1, LovstampDto o2) {
        Double lat1 = o1.getUser().getUserProfile().getAddress().getLatitude();
        Double lon1 = o1.getUser().getUserProfile().getAddress().getLongitude();
        Double lat2 = o2.getUser().getUserProfile().getAddress().getLatitude();
        Double lon2 = o2.getUser().getUserProfile().getAddress().getLongitude();

        if(lat1 == null || lon1 == null || lat2 == null || lon2 == null )
            return 0;

        double distance1 = AddressUtil.distance(lat1, originLat, lon1, originLon, 0, 0);
        double distance2 = AddressUtil.distance(lat2, originLat, lon2, originLon, 0, 0);
        return Doubles.compare(distance1, distance2);
    }
}
