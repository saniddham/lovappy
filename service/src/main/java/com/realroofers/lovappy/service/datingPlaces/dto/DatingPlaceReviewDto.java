package com.realroofers.lovappy.service.datingPlaces.dto;

import com.realroofers.lovappy.service.datingPlaces.model.DatingPlace;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceReview;
import com.realroofers.lovappy.service.datingPlaces.support.DatingAgeRange;
import com.realroofers.lovappy.service.datingPlaces.support.PriceRange;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Darrel Rayen on 10/19/17.
 */
@EqualsAndHashCode
@ToString
public class DatingPlaceReviewDto implements Serializable {

    private Integer placesReviewId;
    private String reviewerName;
    private DatingPlace placeId;
    private String reviewerComment;
    private String reviewerEmail;
    private String reviewerId;
    private Double parkingRating;
    private Double parkingAccessRating;
    private Double securityRating;
    private Double overAllRating;
    private String password;
    private Date reviewDate;
    private Boolean approved;
    private ApprovalStatus reviewApprovalStatus;
    private Double averageRating;
    private Gender suitableGender;
    private DatingAgeRange suitableAgeRange;
    private PriceRange suitablePriceRange;
    private Boolean isGoodForCouple;
    private String googlePlaceId;
    private String reviewZipCode;
    private String reviewerType;
    private List<Integer> personTypes;
    private String placeName;
    private Boolean isFeatured;

    public DatingPlaceReviewDto() {

    }

    public DatingPlaceReviewDto(DatingPlaceReview datingPlaceReview) {
        this.placesReviewId = datingPlaceReview.getPlacesReviewId();
        this.placeId = datingPlaceReview.getPlaceId();
        this.reviewerComment = datingPlaceReview.getReviewerComment();
        this.reviewerEmail = datingPlaceReview.getReviewerEmail();
        this.parkingRating = datingPlaceReview.getParkingRating();
        this.parkingRating = datingPlaceReview.getParkingRating();
        this.securityRating = datingPlaceReview.getSecurityRating();
        this.parkingAccessRating = datingPlaceReview.getParkingAccessRating();
        this.reviewDate = datingPlaceReview.getReviewDate();
        this.reviewApprovalStatus = datingPlaceReview.getReviewApprovalStatus();
        this.approved = datingPlaceReview.getApproved() == null ? false : datingPlaceReview.getApproved();
        this.averageRating = datingPlaceReview.getAverageRating();
        this.suitableAgeRange = datingPlaceReview.getSuitableAgeRange();
        this.suitableGender = datingPlaceReview.getSuitableGender();
        this.suitablePriceRange = datingPlaceReview.getSuitablePriceRange();
        this.isGoodForCouple = datingPlaceReview.getGoodForCouple() == null ? false : datingPlaceReview.getGoodForCouple();
        this.reviewerId = datingPlaceReview.getReviewerId();
        this.overAllRating = datingPlaceReview.getOverallRating();
        this.googlePlaceId = datingPlaceReview.getGooglePlaceId();
        this.reviewerType = datingPlaceReview.getReviewerType();
        this.placeName = datingPlaceReview.getPlaceName();
        this.isFeatured = datingPlaceReview.getFeatured();
    }

    public DatingPlaceReview getDatingPlaceReview() {
        DatingPlaceReview review = new DatingPlaceReview();
        BeanUtils.copyProperties(this, review);
        return review;
    }

    public Integer getPlacesReviewId() {
        return placesReviewId;
    }

    public void setPlacesReviewId(Integer placesReviewId) {
        this.placesReviewId = placesReviewId;
    }

    public DatingPlace getPlaceId() {
        return placeId;
    }

    public void setPlaceId(DatingPlace placeId) {
        this.placeId = placeId;
    }

    public String getReviewerComment() {
        return reviewerComment;
    }

    public void setReviewerComment(String reviewerComment) {
        this.reviewerComment = reviewerComment;
    }

    public String getReviewerEmail() {
        return reviewerEmail;
    }

    public void setReviewerEmail(String reviewerEmail) {
        this.reviewerEmail = reviewerEmail;
    }

    public Double getParkingRating() {
        return parkingRating;
    }

    public void setParkingRating(Double parkingRating) {
        this.parkingRating = parkingRating;
    }

    public Double getParkingAccessRating() {
        return parkingAccessRating;
    }

    public void setParkingAccessRating(Double parkingAccessRating) {
        this.parkingAccessRating = parkingAccessRating;
    }

    public Double getSecurityRating() {
        return securityRating;
    }

    public void setSecurityRating(Double securityRating) {
        this.securityRating = securityRating;
    }

    public Date getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(Date reviewDate) {
        this.reviewDate = reviewDate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public ApprovalStatus getReviewApprovalStatus() {
        return reviewApprovalStatus;
    }

    public void setReviewApprovalStatus(ApprovalStatus reviewApprovalStatus) {
        this.reviewApprovalStatus = reviewApprovalStatus;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public Gender getSuitableGender() {
        return suitableGender;
    }

    public void setSuitableGender(Gender suitableGender) {
        this.suitableGender = suitableGender;
    }

    public DatingAgeRange getSuitableAgeRange() {
        return suitableAgeRange;
    }

    public void setSuitableAgeRange(DatingAgeRange suitableAgeRange) {
        this.suitableAgeRange = suitableAgeRange;
    }

    public PriceRange getSuitablePriceRange() {
        return suitablePriceRange;
    }

    public void setSuitablePriceRange(PriceRange suitablePriceRange) {
        this.suitablePriceRange = suitablePriceRange;
    }

    public Boolean getGoodForCouple() {
        return isGoodForCouple;
    }

    public void setGoodForCouple(Boolean goodForCouple) {
        isGoodForCouple = goodForCouple;
    }

    public String getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(String reviewerId) {
        this.reviewerId = reviewerId;
    }

    public Double getOverAllRating() {
        return overAllRating;
    }

    public void setOverAllRating(Double overAllRating) {
        this.overAllRating = overAllRating;
    }

    public String getGooglePlaceId() {
        return googlePlaceId;
    }

    public void setGooglePlaceId(String googlePlaceId) {
        this.googlePlaceId = googlePlaceId;
    }

    public String getReviewZipCode() {
        return reviewZipCode;
    }

    public void setReviewZipCode(String reviewZipCode) {
        this.reviewZipCode = reviewZipCode;
    }

    public String getReviewerType() {
        return reviewerType;
    }

    public void setReviewerType(String reviewerType) {
        this.reviewerType = reviewerType;
    }

    public List<Integer> getPersonTypes() {
        return personTypes;
    }

    public void setPersonTypes(List<Integer> personTypes) {
        this.personTypes = personTypes;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public Boolean getFeatured() {
        return isFeatured;
    }

    public void setFeatured(Boolean featured) {
        isFeatured = featured;
    }
}
