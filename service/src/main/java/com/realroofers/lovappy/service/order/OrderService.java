package com.realroofers.lovappy.service.order;

import com.realroofers.lovappy.service.order.dto.CouponDto;
import com.realroofers.lovappy.service.order.dto.CouponRuleDto;
import com.realroofers.lovappy.service.order.dto.OrderDetailDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;

import java.util.Collection;
import java.util.List;

/**
 * Created by Eias Altawil on 5/11/17
 */
public interface OrderService {

    OrderDto addOrder(Object object, OrderDetailType orderDetailType, Double price, Integer quantity, Integer userID,
                      String Coupon, PaymentMethodType paymentMethod);

    OrderDto addOrder(Integer productId, OrderDetailType orderDetailType, Double price, Integer quantity, Integer userID,
                      String Coupon, String paymentTransactionId, PaymentMethodType paymentMethod,String cardNo,String cardType);
    OrderDto addOrder(List<OrderDetailDto> orderItems, Double totalPrice,  Integer userID,
                      String Coupon, String paymentTransactionId, PaymentMethodType paymentMethod);

    Collection<OrderDto> getOrdersByUserID(Integer userID);

    Double getTotalSpentByUserID(Integer userID);

    OrderDto getOrderByID(Long id);

    OrderDto getOrderByTransactionID(String transactionID);

    OrderDto setOrderTransactionID(Long orderID, String transactionID);

    List<CouponDto> getAllCoupons();

    CouponDto getCoupon(Integer id);

    CouponDto getCoupon(String code, CouponCategory category);

    CouponDto saveCoupon(CouponDto coupon);

    void revertOrder(Long id);

    //region Coupon Rules
    CouponRuleDto saveRule(CouponRuleDto couponRuleDto);
    CouponRuleDto getCouponRule(Integer id);
    List<CouponRuleDto> getValidCouponRules();
    List<CouponRuleDto> getValidCouponRulesByCategory(CouponCategory couponCategory);

    List<CouponRuleDto> getAllCouponRules();
    //endregion
}
