package com.realroofers.lovappy.service.datingPlaces.model;

import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darrel Rayen on 12/8/17.
 */
@Entity
@Table(name = "place_atmosphere")
@EqualsAndHashCode
public class PlaceAtmosphere implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "atmosphere_id")
    private Integer id;
    private String atmosphereName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "atmosphere", cascade = CascadeType.ALL)
    private List<DatingPlaceAtmospheres> placeAtmospheres = new ArrayList<>();

    private Boolean enabled;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAtmosphereName() {
        return atmosphereName;
    }

    public void setAtmosphereName(String atmosphereName) {
        this.atmosphereName = atmosphereName;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
