package com.realroofers.lovappy.service.order.dto;

import com.realroofers.lovappy.service.order.model.Price;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by Daoud Shaheen on 6/1/2018.
 */
@Data
@EqualsAndHashCode
public class PriceDto {
    private Integer id;
    private double price;
    private OrderDetailType type;

    public PriceDto() {
    }
    public PriceDto(Price price) {
        this.id = price.getId();
        this.price =  price.getPrice();
        this.type = price.getType();
    }
}
