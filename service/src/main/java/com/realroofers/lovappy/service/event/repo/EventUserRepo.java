package com.realroofers.lovappy.service.event.repo;

import com.realroofers.lovappy.service.event.model.Event;
import com.realroofers.lovappy.service.event.model.EventUser;
import com.realroofers.lovappy.service.event.model.EventUserId;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Tejaswi Venupalli on 09/13/17
 */
@Repository
public interface EventUserRepo extends JpaRepository<EventUser, EventUserId> {

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM event_user eu WHERE eu.event_id =:eventId  ;")
    Integer findNoOfUserAttendees(@Param("eventId") Integer eventId);

    @Query(nativeQuery = true, value = "SELECT eu.user_id FROM event_user eu WHERE eu.event_id =:eventId  ;")
    List<Integer> finByAttendeesByEventId(@Param("eventId") Integer eventId);

//    EventUser findByEventAndUser(Event event, User user);

}
