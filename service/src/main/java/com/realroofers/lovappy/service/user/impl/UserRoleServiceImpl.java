package com.realroofers.lovappy.service.user.impl;

import com.realroofers.lovappy.service.user.UserRolesService;
import com.realroofers.lovappy.service.user.dto.UserRoleResponse;
import com.realroofers.lovappy.service.user.dto.UserRolesDto;
import com.realroofers.lovappy.service.user.model.*;
import com.realroofers.lovappy.service.user.repo.RoleRepo;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import com.realroofers.lovappy.service.user.repo.UserRolesRepo;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.util.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Daoud Shaheen on 5/12/2018.
 */
@Service
public class UserRoleServiceImpl implements UserRolesService {

    @Autowired
    private UserRolesRepo userRolesRepo;
    @Autowired
    private RoleRepo roleRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private RandomStringGenerator randomStringGenerator;

    @Transactional(readOnly = true)
    @Override
    public Page<UserRolesDto> getUserRequestsByEmail(String email, Pageable pageable) {
        Set<Role> roles =  new HashSet<>();
        roles.add(roleRepo.findByName(Roles.ADMIN.getValue()));
        roles.add(roleRepo.findByName(Roles.AUTHOR.getValue()));
        roles.add(roleRepo.findByName(Roles.DEVELOPER.getValue()));
        roles.add(roleRepo.findByName(Roles.MUSICIAN.getValue()));
        roles.add(roleRepo.findByName(Roles.VENDOR.getValue()));
        roles.add(roleRepo.findByName(Roles.EVENT_AMBASSADOR.getValue()));
        if(StringUtils.isEmpty(email))
            return userRolesRepo.findAll(QUserRoles.userRoles.id.role.in(roles), pageable).map(UserRolesDto::new);

        return userRolesRepo.findAll(QUserRoles.userRoles.id.user.email.containsIgnoreCase(email).and(QUserRoles.userRoles.id.role.in(roles)), pageable).map(UserRolesDto::new);
    }

    @Transactional
    @Override
    public boolean assignRole(Integer userId, Roles roleName) {
        Role role = roleRepo.findByName(roleName.getValue());
        User user = userRepo.findOne(userId);
        return setUserRole(user, role, ApprovalStatus.PENDING);
    }

    @Transactional
    @Override
    public boolean unAssignRole(Integer userId, Roles roleName) {
        Role role = roleRepo.findByName(roleName.getValue());
        User user = userRepo.findOne(userId);
        UserRolePK pk = new UserRolePK(user, role);
        UserRoles userRoles = userRolesRepo.findOne(pk);
        if(userRoles != null) {
            userRolesRepo.delete(userRoles);
            return true;
        }
        return false;
    }

    @Transactional
    @Override
    public UserRoleResponse approveRole(Integer userId, Integer roleId) {
        Role role = roleRepo.findOne(roleId);
        User user = userRepo.findOne(userId);
        UserRolePK pk = new UserRolePK(user, role);
        UserRoles userRoles = userRolesRepo.findOne(pk);
        if(userRoles == null) {
            userRoles = new UserRoles();
            userRoles.setId(pk);
            userRoles.setCreated(new Date());
        }
        boolean approved = userRoles.getApprovalStatus() != ApprovalStatus.APPROVED;
        userRoles.setApprovalStatus(ApprovalStatus.APPROVED);

        userRolesRepo.save(userRoles);

        return new UserRoleResponse(null, Roles.valueOf(role.getName()), user.getEmail(), approved);
    }
    @Transactional
    @Override
    public UserRoleResponse approveRoleAndGeneratePassword(Integer userId, Integer roleId) {
        boolean approved = true;
        Role role = roleRepo.findOne(roleId);
        User user = userRepo.findOne(userId);
        UserRolePK pk = new UserRolePK(user, role);
        UserRoles userRoles = userRolesRepo.findOne(pk);
        if(userRoles == null) {
            userRoles = new UserRoles();
            userRoles.setId(pk);
            userRoles.setCreated(new Date());
        }
        approved = userRoles.getApprovalStatus() != ApprovalStatus.APPROVED;
        userRoles.setApprovalStatus(ApprovalStatus.APPROVED);

        String clearPassword = randomStringGenerator.generate(12);
        userRoles.setPassphrase(passwordEncoder.encode(clearPassword));
        if(StringUtils.isEmpty(user.getPassword())) {
           user.setPassword(userRoles.getPassphrase());

        }
        user.setEmailVerified(true);

        userRolesRepo.save(userRoles);
        userRepo.save(user);
       return new UserRoleResponse(clearPassword, Roles.valueOf(role.getName()), user.getEmail(), approved);
    }
    @Transactional
    @Override
    public UserRoleResponse rejectRole(Integer userId, Integer roleId) {
        Role role = roleRepo.findOne(roleId);
        User user = userRepo.findOne(userId);
        UserRolePK pk = new UserRolePK(user, role);
        UserRoles userRoles = userRolesRepo.findOne(pk);
        if(userRoles == null) {
            userRoles = new UserRoles();
            userRoles.setId(pk);
            userRoles.setCreated(new Date());
        }
        boolean approved = userRoles.getApprovalStatus() != ApprovalStatus.REJECTED;
        userRoles.setApprovalStatus(ApprovalStatus.REJECTED);

        userRolesRepo.save(userRoles);
        return new UserRoleResponse(null, Roles.valueOf(role.getName()), user.getEmail(), approved);

    }


    private boolean setUserRole(User user, Role role, ApprovalStatus status) {
        UserRolePK pk = new UserRolePK(user, role);
        UserRoles userRoles = userRolesRepo.findOne(pk);
        if(userRoles == null) {
            userRoles = new UserRoles();
            userRoles.setId(pk);
            userRoles.setApprovalStatus(status);
            userRoles.setCreated(new Date());
            userRolesRepo.save(userRoles);
            return true;
        }
        return false;
    }
}
