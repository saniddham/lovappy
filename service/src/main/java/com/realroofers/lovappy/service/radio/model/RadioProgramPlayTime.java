package com.realroofers.lovappy.service.radio.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Manoj on 17/12/2017.
 */

@Data
@Entity
@Table(name = "radio_control_has_play_time")
@EqualsAndHashCode
public class RadioProgramPlayTime extends BaseEntity<Integer> implements Serializable {

    @ManyToOne
    @JoinColumn(name = "fk_radio_program_controls")
    private RadioProgramControl radioProgramControl;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_date_time")
    private Date startTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date_time")
    private Date endTime;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;


}
