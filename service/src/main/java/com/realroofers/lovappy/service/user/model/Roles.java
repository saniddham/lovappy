package com.realroofers.lovappy.service.user.model;

import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 6/23/2017.
 */
public enum Roles implements Serializable{
    SUPER_ADMIN("SUPER_ADMIN", "A"),
    ADMIN("ADMIN", "A"),
    USER("USER", "U"),
    AUTHOR("AUTHOR", "B"),
    EVENT_AMBASSADOR("EVENT_AMBASSADOR", "E"),
    COUPLE("COUPLE", "C"),
    MUSICIAN("MUSICIAN", "M"),
    VENDOR("VENDOR", "R"),
    PLACE_OWNER("PLACE_OWNER", "E"),
    DEVELOPER("DEVELOPER", "D");
    String value;
    String abb;
    Roles(String value, String abb) {
        this.value = value;
        this.abb = abb;
    }

    public String getValue(){
        return value;
    }
    public String getAbb(){
        return abb;
    }
}
