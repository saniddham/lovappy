package com.realroofers.lovappy.service.career.impl;

import com.realroofers.lovappy.service.career.EmploymentService;
import com.realroofers.lovappy.service.career.model.Employment;
import com.realroofers.lovappy.service.career.repo.EducationRepo;
import com.realroofers.lovappy.service.career.repo.EmploymentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class EmploymentImpl implements EmploymentService {
    @Autowired
    private EmploymentRepo employmentRepo;

    @Override
    public String addEmployments(Set<Employment> employments) {
        String status;
        try {
            employmentRepo.save(employments);
            status = "SuccessFully Saved";

        } catch (Exception e){
            System.out.println("error in inserting");
            System.out.println(e);;
            status = "Error Occured While Saving";
        }

        return status;
    }
}
