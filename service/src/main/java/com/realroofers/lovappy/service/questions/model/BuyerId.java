package com.realroofers.lovappy.service.questions.model;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by Darrel Rayen on 11/4/18.
 */
@Embeddable
public class BuyerId implements Serializable{

    private Integer userId;
    private Long responseId;

    public BuyerId(Integer userId, Long responseId) {
        this.userId = userId;
        this.responseId = responseId;
    }

    public BuyerId() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Long getResponseId() {
        return responseId;
    }

    public void setResponseId(Long responseId) {
        this.responseId = responseId;
    }
}
