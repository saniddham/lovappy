package com.realroofers.lovappy.service.datingPlaces.repo;

import com.realroofers.lovappy.service.datingPlaces.model.Deals;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Darrel Rayen on 1/18/18.
 */
@Repository
public interface DealsRepo extends JpaRepository<Deals, Integer> {

    @Query("SELECT dl FROM Deals dl where dl.startPrice < :price and :price <= dl.endPrice")
    List<Deals> isDealExists(@Param("price") Double price);

    @Query("SELECT dl FROM Deals dl where dl.startPrice < :price and :price <= dl.endPrice and dl.dealId<>:dealId")
    List<Deals> isDealExistsForUpdate(@Param("price") Double price, @Param("dealId") Integer dealId);

    @Query("SELECT dl FROM Deals dl where dl.startPrice < :fullAmount and :fullAmount <= dl.endPrice")
    Deals findAllByFullAmount(@Param("fullAmount") Double fullAmount);

    Deals findAllByStartPrice(Double startPrice);

}

