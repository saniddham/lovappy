package com.realroofers.lovappy.service.event.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class EventDateUtil {

    private static final Logger LOG = LoggerFactory.getLogger(EventDateUtil.class);

    public static Date convertToDate(String time) {

        String date = "1970/01/01" + " " + time;
        SimpleDateFormat sf = new SimpleDateFormat("yyyy/MM/dd hh:mm aa");
        Date converted = new Date();
        try {
            converted = sf.parse(date);
        } catch (Exception e) {
            LOG.error("Exception in parsing date  " + e.getMessage());
        }
        return converted;

    }

    public static Date convertToDate(String dateString, Date date1) {
        String test = convertDateToString(date1,"yyyy/MM/dd");
        String finalDate = test+" "+dateString;
        Date date = new Date();
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm aa", Locale.ENGLISH);
            date = dateFormat.parse(finalDate);
        } catch (ParseException e) {
            LOG.error("Error", "Error in Date Format");

        } catch (IllegalArgumentException e) {
            LOG.error("Pattern Error", "Date Pattern not found");
        }
        return date;
    }

    public static String convertToString(Date time) {

        SimpleDateFormat sf = new SimpleDateFormat("hh:mm aa");
        sf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String converted = null;
        try {
            converted = sf.format(time.getTime());
        } catch (Exception e) {
            LOG.error("Exception in parsing date to String  " + e.getMessage());
        }
        return converted;

    }

    public static String convertDateToString(Date date, String pattern) {

        StringBuilder dateString = new StringBuilder();
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, Locale.ENGLISH);
            dateString.append(dateFormat.format(date));

        } catch (IllegalArgumentException e) {
            LOG.error("Pattern Error", "Date Pattern not found");
        }

        return dateString.toString();
    }

    public static Date convertToDateFormat(String timeString) {

        SimpleDateFormat sf = new SimpleDateFormat("MM/dd/yyyy");
        Date converted = new Date();
        try {
            converted = sf.parse(timeString);
        } catch (Exception e) {
            LOG.error("Exception in parsing date  " + e.getMessage());
        }
        return converted;

    }


    public static Date getDateOnly() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();

    }
}
