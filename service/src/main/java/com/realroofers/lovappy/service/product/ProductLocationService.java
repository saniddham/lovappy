package com.realroofers.lovappy.service.product;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.product.dto.ProductLocationDto;
import com.realroofers.lovappy.service.product.model.ProductLocation;

import java.util.List;

public interface ProductLocationService extends AbstractService<ProductLocationDto, Integer> {

    ProductLocation findByProductandLocation(Integer productId, Integer locationId);

    void deleteAllByProduct(Integer productId);

    List<ProductLocation> findByProduct(Integer productId);

}
