package com.realroofers.lovappy.service.datingPlaces.support;

/**
 * Created by Darrel Rayen on 10/23/17.
 */
public enum DatingAgeRange {
    RANGE1(18, 29), RANGE2(30, 49), RANGE3(50, 100), RANGE4(0, 100);

    private int from;
    private int to;

    DatingAgeRange(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public static DatingAgeRange getRangeByAge(Integer age) {
        if (betweenExclusive(age, RANGE1.from, RANGE1.to)) {
            return RANGE1;
        } else if (betweenExclusive(age, RANGE2.from, RANGE2.to)) {
            return RANGE2;
        } else if (betweenExclusive(age, RANGE3.from, RANGE3.to)) {
            return RANGE3;
        }
        return null;
    }

    private static boolean betweenExclusive(int x, int min, int max) {
        return x > min && x < max;
    }
}
