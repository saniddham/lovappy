package com.realroofers.lovappy.service.event.repo;

import com.realroofers.lovappy.service.event.model.EventUserPicture;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by darrel on 8/31/17.
 */

public interface EventUserPictureRepo extends JpaRepository<EventUserPicture,Integer>, QueryDslPredicateExecutor<EventUserPicture> {

   @Query(nativeQuery = true, value = "SELECT eup.* FROM event_user_picture eup INNER JOIN event e ON eup.event_id = e.event_id AND e.event_date BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY) AND CURRENT_DATE ;")
   List<EventUserPicture> findPicturesForLastWeekEvents();

   @Query("SELECT eup FROM EventUserPicture eup WHERE eup.eventId.eventId=:eventId ")
   Page<EventUserPicture> findEventUserPicturesByEventId(@Param("eventId") Integer eventId, Pageable pageable);


}
