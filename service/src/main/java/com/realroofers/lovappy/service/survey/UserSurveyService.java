package com.realroofers.lovappy.service.survey;

import com.realroofers.lovappy.service.survey.dto.SurveyAnswerDto;
import com.realroofers.lovappy.service.survey.dto.SurveyQuestionDto;
import com.realroofers.lovappy.service.survey.dto.UserSurveyDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by hasan on 9/8/2017.
 */
public interface UserSurveyService {

    List<SurveyQuestionDto> getAllQuestions();
    Page<SurveyQuestionDto> getAllQuestions(Pageable pageable);
    SurveyQuestionDto getQuestion(Integer id);

    SurveyQuestionDto addQuestion(String title);
    SurveyQuestionDto updateQuestion(Integer questionID, String title);

    SurveyAnswerDto getAnswer(Integer id);
    SurveyAnswerDto addAnswer(Integer questionID, String title);
    SurveyAnswerDto updateAnswer(Integer answerID, String title);
    void deleteAnswer(Integer answerID);

    UserSurveyDto addUserSurvey(UserSurveyDto userSurveyDto);

    boolean isAnswered(Integer userId);
    boolean setAnswers(UserSurveyDto userSurveyDto);
    boolean dontShowSurvey();

    Page<UserSurveyDto> getUserSurvey(Pageable pageable);
    UserSurveyDto getUserSurvey(Integer userID);
}
