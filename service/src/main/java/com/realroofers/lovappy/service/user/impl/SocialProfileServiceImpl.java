package com.realroofers.lovappy.service.user.impl;

import com.realroofers.lovappy.service.core.SocialType;
import com.realroofers.lovappy.service.user.SocialProfileService;
import com.realroofers.lovappy.service.user.dto.SocialProfileDto;
import com.realroofers.lovappy.service.user.dto.UserAuthDto;
import com.realroofers.lovappy.service.user.model.*;
import com.realroofers.lovappy.service.user.repo.RoleRepo;
import com.realroofers.lovappy.service.user.repo.SocialProfileRepo;
import com.realroofers.lovappy.service.user.repo.UserProfileRepo;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Daoud Shaheen on 3/3/2018.
 */
@Service
public class SocialProfileServiceImpl implements SocialProfileService {

    private final SocialProfileRepo socialProfileRepo;
    private final UserRepo userRepo;
    private final UserProfileRepo userProfileRepo;
    private final RoleRepo roleRepo;
    public SocialProfileServiceImpl(SocialProfileRepo socialProfileRepo, UserRepo userRepo, UserProfileRepo userProfileRepo, RoleRepo roleRepo) {
        this.socialProfileRepo = socialProfileRepo;
        this.userRepo = userRepo;
        this.userProfileRepo = userProfileRepo;
        this.roleRepo = roleRepo;
    }

    @Override
    public List<SocialProfileDto> findAll() {
        return null;
    }

    @Override
    public List<SocialProfileDto> findAllActive() {
        return null;
    }

    @Transactional
    @Override
    public SocialProfileDto create(SocialProfileDto socialProfileDto) {
        SocialProfile socialProfile = socialProfileRepo.findBySocialIdAndSocialPlatform(socialProfileDto.getSocialId(), SocialType.fromString(socialProfileDto.getSocialPlatform()));
        if(socialProfile == null) {
            socialProfile = new SocialProfile();
            socialProfile.setCreated(new Date());
            socialProfile.setSocialId(socialProfileDto.getSocialId());
            socialProfile.setSocialPlatform(SocialType.fromString(socialProfileDto.getSocialPlatform()));
        }
        socialProfile.setFirstName(socialProfileDto.getFirstName());
        socialProfile.setLastName(socialProfileDto.getLastName());
        socialProfile.setName(socialProfileDto.getName());
        socialProfile.setImageUrl(socialProfileDto.getImageUrl());

        User user = userRepo.findOneByEmail(socialProfileDto.getEmail());
        if(user != null) {
            socialProfile.setUser(user);
        }
        return new SocialProfileDto(socialProfileRepo.save(socialProfile));
    }

    @Override
    @Transactional
    public int saveSocial(UserAuthDto dto, Roles role) {
        User user = new User();
        user.setEnabled(dto.isEnabled());
        user.setPassword(dto.getPassword());
        user.setEmail(dto.getEmail());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());

        UserRolePK pk = new UserRolePK(user, roleRepo.findByName(role.getValue()));
        UserRoles userRoles = new UserRoles();
        userRoles.setId(pk);
        userRoles.setApprovalStatus(ApprovalStatus.APPROVED);
        userRoles.setCreated(new Date());
        user.getRoles().add(userRoles);
        user.setEmailVerified(true);

        /*UserProfile userProfile = new UserProfile();
        user.setUserProfile(userProfile);

        userProfile.setUser(user);*/

        User savedUser = userRepo.save(user);
        Integer savedUserID = savedUser.getUserId();
////
        UserProfile userProfile = new UserProfile(savedUserID);
        userProfile.setUser(savedUser);
        userProfileRepo.save(userProfile);


        SocialProfile socialProfile = socialProfileRepo.findBySocialIdAndSocialPlatform(dto.getSocialId(), SocialType.fromString(dto.getSocialPlatform()));
        if(socialProfile == null) {
            socialProfile = new SocialProfile();
            socialProfile.setCreated(new Date());
            socialProfile.setSocialId(dto.getSocialId());
            socialProfile.setSocialPlatform(SocialType.fromString(dto.getSocialPlatform()));
        }
        socialProfile.setFirstName(dto.getFirstName());
        socialProfile.setLastName(dto.getLastName());
        socialProfile.setName(dto.getFirstName() + " " + dto.getLastName());
        socialProfile.setImageUrl(dto.getImageUrl());
        socialProfile.setUser(savedUser);
        return savedUserID;
    }

    @Transactional
    @Override
    public SocialProfileDto update(SocialProfileDto socialProfileDto) {
        SocialProfile socialProfile = socialProfileRepo.findBySocialIdAndSocialPlatform(socialProfileDto.getSocialId(), SocialType.fromString(socialProfileDto.getSocialPlatform()));
        if(socialProfile == null) {
            socialProfile = new SocialProfile();
            socialProfile.setCreated(new Date());
            socialProfile.setSocialId(socialProfileDto.getSocialId());
            socialProfile.setSocialPlatform(SocialType.fromString(socialProfileDto.getSocialPlatform()));
        }
        socialProfile.setFirstName(socialProfileDto.getFirstName());
        socialProfile.setLastName(socialProfileDto.getLastName());
        socialProfile.setName(socialProfileDto.getName());
        socialProfile.setImageUrl(socialProfileDto.getImageUrl());
        if(socialProfile.getUser() == null){
           User user = userRepo.findOne(socialProfileDto.getUserId());
           socialProfile.setUser(user);
        }
        return new SocialProfileDto(socialProfileRepo.save(socialProfile));
    }

    @Override
    public void delete(Integer integer) {

    }

    @Transactional(readOnly = true)
    @Override
    public SocialProfileDto findById(Integer integer) {
        SocialProfile socialProfile = socialProfileRepo.findOne(integer);
        return socialProfile != null ? new SocialProfileDto(socialProfile): null;
    }

    @Transactional(readOnly = true)
    @Override
    public SocialProfileDto getProfileBySocialIdAndProvider(String socialId, SocialType socialProvider) {
        SocialProfile socialProfile = socialProfileRepo.findBySocialIdAndSocialPlatform(socialId, socialProvider);
        return socialProfile != null ? new SocialProfileDto(socialProfile): null;
    }
}
