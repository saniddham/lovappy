package com.realroofers.lovappy.service.plan;

import com.realroofers.lovappy.service.plan.dto.FeatureDetailsDto;
import com.realroofers.lovappy.service.plan.dto.PlanDetailsDto;
import com.realroofers.lovappy.service.plan.dto.PlansDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by hasan on 7/11/2017.
 */
@Service
public interface PlansService {
    List<PlansDto> allPlans();

    List<PlanDetailsDto> allDetailedPlans(boolean onlyValid);

    PlanDetailsDto savePlan(PlanDetailsDto plansDto);

    PlanDetailsDto getPlanDetails(Integer planId, boolean hasValidFeatures);

    FeatureDetailsDto getFeatureByID(Integer id);

    FeatureDetailsDto saveFeature(FeatureDetailsDto featureDetailsDto);

    public List<PlansDto> getPlansByName();

    public List<FeatureDetailsDto> saveFeatures(List<FeatureDetailsDto> features);
}
