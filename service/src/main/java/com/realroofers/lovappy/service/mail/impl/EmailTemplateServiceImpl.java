package com.realroofers.lovappy.service.mail.impl;

import com.realroofers.lovappy.service.exception.ResourceNotFoundException;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.dto.LovappyEmailDto;
import com.realroofers.lovappy.service.mail.model.EmailTemplate;
import com.realroofers.lovappy.service.mail.model.EmailTemplateContent;
import com.realroofers.lovappy.service.mail.repo.EmailTemplateContentRepo;
import com.realroofers.lovappy.service.mail.repo.EmailTemplateRepo;
import com.realroofers.lovappy.service.mail.repo.LovappyEmailRepo;
import com.realroofers.lovappy.service.mail.support.EmailTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * Created by Daoud Shaheen on 9/6/2017.
 */
@Service
public class EmailTemplateServiceImpl implements EmailTemplateService {

    @Autowired
    private EmailTemplateRepo emailTemplateRepo;

    @Autowired
    private EmailTemplateContentRepo emailTemplateContentRepo;

    @Autowired
    private LovappyEmailRepo lovappyEmailRepo;

    @Transactional(readOnly = true)
    @Override
    public EmailTemplateDto getByIdAndLanguage(Integer templateId, String language) {
        EmailTemplateContent emailTemplateContent = emailTemplateContentRepo.findByLanguageAndTemplateId(language, templateId);
        return emailTemplateContent != null ? new EmailTemplateDto(emailTemplateContent) : null;
    }

    @Transactional(readOnly = true)
    @Override
    public EmailTemplateDto getByTypeAndLanguage(EmailTypes type, String language) {
        EmailTemplateContent emailTemplateContent = emailTemplateContentRepo.findByLanguageAndTemplateType(language, type);
        return emailTemplateContent != null ? new EmailTemplateDto(emailTemplateContent) : null;
    }

    @Transactional(readOnly = true)
    @Override
    public EmailTemplateDto getByNameAndLanguage(String name, String language) {
        EmailTemplateContent emailTemplateContent = emailTemplateContentRepo.findByLanguageAndTemplateName(language, name);
        return emailTemplateContent != null ? new EmailTemplateDto(emailTemplateContent) : null;
    }

    @Transactional
    @Override
    public EmailTemplateDto create(EmailTemplateDto emailTemplateDto) {
        EmailTemplate template = new EmailTemplate();
        template.setName(emailTemplateDto.getName());
        template.setEnabled(true);
        template.setCreated(new Date());
        template.setType(emailTemplateDto.getType());
        template.setFromEmail(emailTemplateDto.getFromEmail());
        template.setFromName(emailTemplateDto.getFromName());
        template.setTemplateUrl(emailTemplateDto.getTemplateUrl());
        template.setImageUrl(emailTemplateDto.getImageUrl());
        emailTemplateRepo.save(template);
        EmailTemplateContent emailTemplateContent = new EmailTemplateContent(emailTemplateDto.getSubject(),
                emailTemplateDto.getBody(),
                emailTemplateDto.getHtmlBody(),
                emailTemplateDto.getLanguage());
        emailTemplateContent.setTemplate(template);

        return new EmailTemplateDto(emailTemplateContentRepo.save(emailTemplateContent)) ;
    }

    @Transactional(readOnly = true)
    @Override
    public Page<EmailTemplateDto> getListByLanguage(String language, Pageable pageable) {
        return emailTemplateContentRepo.findByLanguage(language, pageable).map(EmailTemplateDto::new);
    }

    @Transactional
    @Override
    public void delete(Integer templateId) {
        emailTemplateRepo.delete(templateId);
    }

    @Transactional
    @Override
    public EmailTemplateDto update(Integer templateId, EmailTemplateDto emailTemplateDto) {
        EmailTemplate template = emailTemplateRepo.findOne(templateId);

        if(template != null) {
            template.setUpdated(new Date());
            template.setFromEmail(emailTemplateDto.getFromEmail());
            template.setFromName(emailTemplateDto.getFromName());
            template.setEnabled(!emailTemplateDto.getEnabled());

            if(!StringUtils.isEmpty(emailTemplateDto.getTemplateUrl()))
            template.setTemplateUrl(emailTemplateDto.getTemplateUrl());

            if(!StringUtils.isEmpty(emailTemplateDto.getImageUrl()))
            template.setImageUrl(emailTemplateDto.getImageUrl());

            emailTemplateRepo.save(template);
            EmailTemplateContent emailTemplateContent = emailTemplateContentRepo.findByLanguageAndTemplateId(emailTemplateDto.getLanguage(), template.getId());
            emailTemplateContent.setBody(emailTemplateDto.getBody());
            emailTemplateContent.setSubject(emailTemplateDto.getSubject());
            emailTemplateContent.setHtmlBody(emailTemplateDto.getHtmlBody());

            return new EmailTemplateDto( emailTemplateContentRepo.save(emailTemplateContent));
        }
        throw new ResourceNotFoundException("Email Template with Id " + templateId + " is not found");
    }

    @Transactional(readOnly = true)
    @Override
    public Page<LovappyEmailDto> getLovappyEmails(int page, int limit) {
        return lovappyEmailRepo.findAll(new PageRequest(page - 1, limit)).map(LovappyEmailDto::new);
    }

    @Transactional
    @Override
    public void deleteAll() {
        emailTemplateContentRepo.deleteAll();
        emailTemplateRepo.deleteAll();
    }
}
