package com.realroofers.lovappy.service.datingPlaces.support;

/**
 * Created by Darrel Rayen on 10/17/17.
 */
public enum PriceRange {

    FREE("Free"),
    CHEAP("Cheap"),
    EXPENSIVE("Expensive"),
    VERYEXPENSIVE("Veryexpensive"),
    NA("");

    String text;

    PriceRange(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
