package com.realroofers.lovappy.service.product.dto.mobile;

import com.realroofers.lovappy.service.product.model.Brand;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class BrandItemDto {

    private Integer id;
    private String brandName;

    public BrandItemDto() {
    }

    public BrandItemDto(Integer id, String brandName) {
        this.id = id;
        this.brandName = brandName;
    }

    public BrandItemDto(Brand brand) {
        if(brand.getId()!=null) {
            this.id = brand.getId();
        }
        this.brandName = brand.getBrandName();
    }

}
