package com.realroofers.lovappy.service.datingPlaces.support.google_places;

/**
 * This class contains method to retreive places using the Google Places API
 */

import com.realroofers.lovappy.service.datingPlaces.dto.DatingPlaceDto;
import com.realroofers.lovappy.service.datingPlaces.dto.DatingPlaceReviewDto;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;

@Component
public class DatingPlaceLocationUtil {

    private String BASE_URL = "https://maps.googleapis.com/maps/api/place/";
    private String API_URL_FORMAT_STRING = "%s%s/json?%s";

    @Value("${google.maps.api.key}")
    private String googleApiKey;

    //TODO use restTemplate to do that
    private JSONObject executePlaceUrl(String executeUrl) throws IOException {

        URL obj = new URL(executeUrl);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);
        String inputLine = null;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream(), Charset.forName("UTF-8")))) {
            StringBuilder content = new StringBuilder();

            while ((inputLine = reader.readLine()) != null) {
                content.append(inputLine);
            }

            inputLine = new String(content.toString().getBytes(Charset.forName("UTF-8")), Charset.forName("UTF-8"));
        }

        con.disconnect();
        JSONObject json = new JSONObject(inputLine);
        return json;
    }

    private String buildUrl(String methodName, String params) {

        return String.format(API_URL_FORMAT_STRING, BASE_URL, methodName, params);
    }

    private Comparator<DatingPlaceDto> sortByRatingDescending = (o1, o2) -> {
        if (o1.getOverAllRating().doubleValue() == o2.getOverAllRating().doubleValue())
            return 0;
        else if (o1.getOverAllRating() < o2.getOverAllRating())
            return 1;
        else
            return -1;
    };

    private Comparator<DatingPlaceDto> sortByRatingAscending = (o1, o2) -> {
        if (o1.getOverAllRating().doubleValue() == o2.getOverAllRating().doubleValue())
            return 0;
        else if (o1.getOverAllRating() > o2.getOverAllRating())
            return 1;
        else
            return -1;
    };

    private void parseLocationResults(JSONArray results, List<DatingPlaceDto> datingPlaceDtos) {

        for (int i = 0; i < results.length(); i++) {

            DatingPlaceDto datingPlaceDto = new DatingPlaceDto();
            JSONObject result = results.getJSONObject(i);
            JSONObject location = result.getJSONObject(GoogleNameConstants.OBJECT_GEOMETRY).getJSONObject(GoogleNameConstants.OBJECT_LOCATION);
            double latitude = location.getDouble(GoogleNameConstants.DOUBLE_LATITUDE);
            double longitude = location.getDouble(GoogleNameConstants.DOUBLE_LONGITUDE);
            String placeId = result.getString(GoogleNameConstants.STRING_PLACE_ID);
            String iconUrl = result.optString("icon", null);
            String placeName = result.optString(GoogleNameConstants.STRING_NAME);
            String address = result.optString(GoogleNameConstants.STRING_ADDRESS, null);
            Double rating = result.optDouble(GoogleNameConstants.DOUBLE_RATING, 0);
            String vicinity = result.optString(GoogleNameConstants.STRING_VICINITY, null);

            // see if the place is open, fail-safe if opening_hours is not present
            JSONObject hours = result.optJSONObject(GoogleNameConstants.OBJECT_HOURS);
            boolean hoursDefined = hours != null && hours.has("open_now");
            Status status = Status.NONE;
            if (hoursDefined) {
                boolean opened = hours.getBoolean("open_now");
                status = opened ? Status.OPENED : Status.CLOSED;
                datingPlaceDto.setIsOpened(status.toString());
            }

            // get the price level for the place, fail-safe if not defined
            boolean priceDefined = result.has("price_level");
            Price price = Price.NONE;
            if (priceDefined) {
                price = Price.values()[result.getInt("price_level")];
            }

            // the place "types"
            List<String> types = new ArrayList<>();
            JSONArray jsonTypes = result.optJSONArray("types");
            if (jsonTypes != null) {
                for (int a = 0; a < jsonTypes.length(); a++) {
                    types.add(jsonTypes.getString(a));
                }
            }
            JSONArray coverPicture = result.optJSONArray("photos");
            String imageUrl = null;

            if (coverPicture != null) {

                for (int a = 0; a < coverPicture.length(); a++) {
                    String imageReference = coverPicture.getJSONObject(a).getString(GoogleNameConstants.STRING_PHOTO_REFERENCE);
                    imageUrl = String.format("%sphoto?maxwidth=%s&photoreference=%s&key=%s", BASE_URL, 400, imageReference, googleApiKey);
                }
            }
            datingPlaceDto.setCoverPictureUrl(imageUrl);
            datingPlaceDto.setDatingPlacesId(placeId);
            datingPlaceDto.setPlaceName(placeName);
            if (vicinity != null) {
                datingPlaceDto.setAddressLine(vicinity);
            } else {
                datingPlaceDto.setAddressLine(address);
            }
            datingPlaceDto.setLatitude(latitude);
            datingPlaceDto.setLongitude(longitude);
            datingPlaceDto.setIconUrl(iconUrl);
            datingPlaceDto.setOverAllRating(rating);
            datingPlaceDto.setGoogleAverage(rating);
            datingPlaceDtos.add(datingPlaceDto);
        }
    }

    public List<DatingPlaceDto> getGooglePlacesListByLocation(Double currentLatitude, Double currentLongitude) throws IOException {

        List<DatingPlaceDto> datingPlaceDtos = new ArrayList<>();
        String executeUrlRestaurants = buildUrl(GoogleNameConstants.METHOD_NEARBY_SEARCH, String.format("location=%s,%s&rankby=%s&type=%s&language=en&key=%s", currentLatitude, currentLongitude, GoogleNameConstants.SEARCH_BY_DISTANCE, Types.TYPE_RESTAURANT, googleApiKey));
        String executeUrlMuseum = buildUrl(GoogleNameConstants.METHOD_NEARBY_SEARCH, String.format("location=%s,%s&rankby=%s&type=%s&language=en&key=%s", currentLatitude, currentLongitude, GoogleNameConstants.SEARCH_BY_DISTANCE, Types.TYPE_MUSEUM, googleApiKey));
        String executeUrlPark = buildUrl(GoogleNameConstants.METHOD_NEARBY_SEARCH, String.format("location=%s,%s&rankby=%s&type=%s&language=en&key=%s", currentLatitude, currentLongitude, GoogleNameConstants.SEARCH_BY_DISTANCE, Types.TYPE_AMUSEMENT_PARK, googleApiKey));

        getAllResults(40, executeUrlRestaurants, datingPlaceDtos);
        getAllResults(40, executeUrlPark, datingPlaceDtos);
        getAllResults(40, executeUrlMuseum, datingPlaceDtos);
        return datingPlaceDtos;
    }

    public List<DatingPlaceDto> getGooglePlacesListByLocationAndRating(Double currentLatitude, Double currentLongitude, Double radius, Boolean isDescending) throws IOException {

        List<DatingPlaceDto> datingPlaceDtos = new ArrayList<>();
        String executeUrlRestaurants = buildUrl(GoogleNameConstants.METHOD_NEARBY_SEARCH, String.format("location=%s,%s&radius=%s&type=%s&language=en&key=%s", currentLatitude, currentLongitude, radius, Types.TYPE_RESTAURANT, googleApiKey));
        String executeUrlMuseum = buildUrl(GoogleNameConstants.METHOD_NEARBY_SEARCH, String.format("location=%s,%s&radius=%s&type=%s&language=en&key=%s", currentLatitude, currentLongitude, radius, Types.TYPE_MUSEUM, googleApiKey));
        String executeUrlPark = buildUrl(GoogleNameConstants.METHOD_NEARBY_SEARCH, String.format("location=%s,%s&radius=%s&type=%s&language=en&key=%s", currentLatitude, currentLongitude, radius, Types.TYPE_AMUSEMENT_PARK, googleApiKey));

        getAllResults(40, executeUrlRestaurants, datingPlaceDtos);

        getAllResults(40, executeUrlPark, datingPlaceDtos);
        getAllResults(40, executeUrlMuseum, datingPlaceDtos);
        if(isDescending!=null) {
            if (isDescending) {
                Collections.sort(datingPlaceDtos, sortByRatingDescending);
            } else {
                Collections.sort(datingPlaceDtos, sortByRatingAscending);
            }
        }
        return datingPlaceDtos;
    }

    public List<DatingPlaceDto> getGooglePlacesRestaurantsList(Double currentLatitude, Double currentLongitude, Double radius, String query) throws IOException {
        List<DatingPlaceDto> datingPlaceDtos = new ArrayList<>();
        String executeUrlRestaurants;
        if (query != null) {
            executeUrlRestaurants = buildUrl(GoogleNameConstants.METHOD_TEXT_SEARCH, String.format("query=%s&radius=%s&location=%s,%s&type=%s&language=en&key=%s", query, radius, currentLatitude, currentLongitude, Types.TYPE_RESTAURANT, googleApiKey));
        } else {
            executeUrlRestaurants = buildUrl(GoogleNameConstants.METHOD_NEARBY_SEARCH, String.format("location=%s,%s&radius=%s&type=%s&language=en&key=%s", currentLatitude, currentLongitude, radius, Types.TYPE_RESTAURANT, googleApiKey));
        }
        JSONObject json = executePlaceUrl(executeUrlRestaurants);
        JSONArray results = json.getJSONArray(GoogleNameConstants.ARRAY_RESULTS);
        parseLocationResults(results, datingPlaceDtos);
        Collections.sort(datingPlaceDtos, sortByRatingDescending);
        return datingPlaceDtos;
    }

    public List<DatingPlaceDto> getGooglePlacesListByLocationAndText(Double radius, String query) throws IOException {

        List<DatingPlaceDto> datingPlaceDtos = new ArrayList<>();
        String executeUrlRestaurants;
        String executeUrlMuseum;
        String executeUrlPark;
        if (radius != null) {
            executeUrlRestaurants = buildUrl(GoogleNameConstants.METHOD_TEXT_SEARCH, String.format("query=%s&radius=%s&type=%s&language=en&key=%s", query, radius, Types.TYPE_RESTAURANT, googleApiKey));
            executeUrlMuseum = buildUrl(GoogleNameConstants.METHOD_TEXT_SEARCH, String.format("query=%s&radius=%s&type=%s&language=en&key=%s", query, radius, Types.TYPE_MUSEUM, googleApiKey));
            executeUrlPark = buildUrl(GoogleNameConstants.METHOD_TEXT_SEARCH, String.format("query=%s&radius=%s&type=%s&language=en&key=%s", query, radius, Types.TYPE_AMUSEMENT_PARK, googleApiKey));
        } else {
            executeUrlRestaurants = buildUrl(GoogleNameConstants.METHOD_TEXT_SEARCH, String.format("query=%s&type=%s&language=en&key=%s", query, Types.TYPE_RESTAURANT, googleApiKey));
            executeUrlMuseum = buildUrl(GoogleNameConstants.METHOD_TEXT_SEARCH, String.format("query=%s&type=%s&language=en&key=%s", query, Types.TYPE_MUSEUM, googleApiKey));
            executeUrlPark = buildUrl(GoogleNameConstants.METHOD_TEXT_SEARCH, String.format("query=%s&type=%s&language=en&key=%s", query, Types.TYPE_AMUSEMENT_PARK, googleApiKey));
        }

        getAllResults(40, executeUrlRestaurants, datingPlaceDtos);
        getAllResults(40, executeUrlPark, datingPlaceDtos);
        getAllResults(40, executeUrlMuseum, datingPlaceDtos);

        return datingPlaceDtos;
    }

    public List<DatingPlaceDto> getGooglePlacesListByLocationAndText(Double radius, String query, Double latitude, Double longitude) throws IOException {

        List<DatingPlaceDto> datingPlaceDtos = new ArrayList<>();
        String executeUrlRestaurants;
        String executeUrlMuseum;
        String executeUrlPark;
        if (radius != null) {
            executeUrlRestaurants = buildUrl(GoogleNameConstants.METHOD_TEXT_SEARCH, String.format("location=%s,%s&query=%s&radius=%s&type=%s&language=en&key=%s", latitude, longitude, query, radius, Types.TYPE_RESTAURANT, googleApiKey));
            executeUrlMuseum = buildUrl(GoogleNameConstants.METHOD_TEXT_SEARCH, String.format("location=%s,%s&query=%s&radius=%s&type=%s&language=en&key=%s", latitude, longitude, query, radius, Types.TYPE_MUSEUM, googleApiKey));
            executeUrlPark = buildUrl(GoogleNameConstants.METHOD_TEXT_SEARCH, String.format("location=%s,%s&query=%s&radius=%s&type=%s&language=en&key=%s", latitude, longitude, query, radius, Types.TYPE_AMUSEMENT_PARK, googleApiKey));
        } else {
            executeUrlRestaurants = buildUrl(GoogleNameConstants.METHOD_TEXT_SEARCH, String.format("location=%s,%s&query=%s&type=%s&language=en&key=%s", latitude, longitude, query, Types.TYPE_RESTAURANT, googleApiKey));
            executeUrlMuseum = buildUrl(GoogleNameConstants.METHOD_TEXT_SEARCH, String.format("location=%s,%s&query=%s&type=%s&language=en&key=%s", latitude, longitude, query, Types.TYPE_MUSEUM, googleApiKey));
            executeUrlPark = buildUrl(GoogleNameConstants.METHOD_TEXT_SEARCH, String.format("location=%s,%s&query=%s&type=%s&language=en&key=%s", latitude, longitude, query, Types.TYPE_AMUSEMENT_PARK, googleApiKey));
        }
        DatingPlaceDto datingPlaceDto = getGooglePlaceByText(query);
        if (datingPlaceDto.getDatingPlacesId() != null) {
            datingPlaceDtos.add(datingPlaceDto);
        }
        getAllResults(40, executeUrlRestaurants, datingPlaceDtos);
        getAllResults(40, executeUrlPark, datingPlaceDtos);
        getAllResults(40, executeUrlMuseum, datingPlaceDtos);
        return datingPlaceDtos;
    }

    public DatingPlaceDto getGooglePlaceByText(String query) throws IOException {
        DatingPlaceDto datingPlaceDto = new DatingPlaceDto();
        String executeUrl = buildUrl(
                GoogleNameConstants.METHOD_PLACE_FROM_TEXT,
                String.format("input=%s&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry,place_id&key=%s", query, googleApiKey));
        JSONObject json = executePlaceUrl(executeUrl);
        JSONArray results = json.getJSONArray("candidates");
        for (int x = 0; x < results.length(); x++) {
            JSONObject result = results.getJSONObject(x);
            JSONObject location = result.getJSONObject(GoogleNameConstants.OBJECT_GEOMETRY).getJSONObject(GoogleNameConstants.OBJECT_LOCATION);
            double latitude = location.getDouble(GoogleNameConstants.DOUBLE_LATITUDE);
            double longitude = location.getDouble(GoogleNameConstants.DOUBLE_LONGITUDE);
            String placeId = result.getString(GoogleNameConstants.STRING_PLACE_ID);
            String iconUrl = result.optString("icon", null);
            String placeName = result.optString(GoogleNameConstants.STRING_NAME);
            String address = result.optString(GoogleNameConstants.STRING_ADDRESS, null);
            Double rating = result.optDouble(GoogleNameConstants.DOUBLE_RATING, 0);

            // see if the place is open, fail-safe if opening_hours is not present
            JSONObject hours = result.optJSONObject(GoogleNameConstants.OBJECT_HOURS);
            boolean hoursDefined = hours != null && hours.has("open_now");
            Status status = Status.NONE;
            if (hoursDefined) {
                boolean opened = hours.getBoolean("open_now");
                status = opened ? Status.OPENED : Status.CLOSED;
                datingPlaceDto.setIsOpened(status.toString());
            }

            JSONArray coverPicture = result.optJSONArray("photos");
            String imageUrl = null;

            if (coverPicture != null) {
                for (int a = 0; a < coverPicture.length(); a++) {
                    String imageReference = coverPicture.getJSONObject(a).getString(GoogleNameConstants.STRING_PHOTO_REFERENCE);
                    imageUrl = String.format("%sphoto?maxwidth=%s&photoreference=%s&key=%s", BASE_URL, 400, imageReference, googleApiKey);
                }
            }
            datingPlaceDto.setCoverPictureUrl(imageUrl);
            datingPlaceDto.setDatingPlacesId(placeId);
            datingPlaceDto.setPlaceName(placeName);
            datingPlaceDto.setAddressLine(address);
            datingPlaceDto.setLatitude(latitude);
            datingPlaceDto.setLongitude(longitude);
            datingPlaceDto.setIconUrl(iconUrl);
            datingPlaceDto.setOverAllRating(rating);
            datingPlaceDto.setGoogleAverage(rating);
        }
        return datingPlaceDto;
    }

    public DatingPlaceDto getGooglePlaceDetails(String placeId) throws IOException {

        DatingPlaceDto datingPlaceDto = new DatingPlaceDto();
        String fields = GoogleNameConstants.ARRAY_ADDRESS_COMPONENTS + "," + GoogleNameConstants.STRING_ADDRESS + "," + GoogleNameConstants.OBJECT_GEOMETRY + "/" + GoogleNameConstants.OBJECT_LOCATION + "," + GoogleNameConstants.STRING_NAME + "," + GoogleNameConstants.OBJECT_HOURS + "/"
                + GoogleNameConstants.BOOLEAN_OPENED + "," + GoogleNameConstants.ARRAY_PHOTOS + ","
                + GoogleNameConstants.STRING_PLACE_ID + "," + GoogleNameConstants.DOUBLE_RATING + ","
                + GoogleNameConstants.ARRAY_REVIEWS + "/" + GoogleNameConstants.STRING_AUTHOR_NAME + ","
                + GoogleNameConstants.ARRAY_REVIEWS + "/" + GoogleNameConstants.DOUBLE_RATING + ","
                + GoogleNameConstants.ARRAY_REVIEWS + "/" + GoogleNameConstants.STRING_TEXT + ","
                + GoogleNameConstants.ARRAY_REVIEWS + "/" + GoogleNameConstants.LONG_TIME;
        String executeUrl = buildUrl(GoogleNameConstants.METHOD_DETAILS, String.format("placeid=%s&key=%s&fields=%s", placeId, googleApiKey, fields));
        JSONObject json = executePlaceUrl(executeUrl);
        JSONObject result = json.getJSONObject(GoogleNameConstants.OBJECT_RESULT);
        String placeName = result.getString(GoogleNameConstants.STRING_NAME);
        String id = result.getString(GoogleNameConstants.STRING_PLACE_ID);
        String formattedAddress = result.optString(GoogleNameConstants.STRING_ADDRESS, null);
        double rating = result.optDouble(GoogleNameConstants.DOUBLE_RATING, -1);

        JSONObject location = result.getJSONObject(GoogleNameConstants.OBJECT_GEOMETRY).getJSONObject(GoogleNameConstants.OBJECT_LOCATION);
        Double latitude = location.getDouble(GoogleNameConstants.DOUBLE_LATITUDE);
        Double longitude = location.getDouble(GoogleNameConstants.DOUBLE_LONGITUDE);

        JSONArray jsonPhotos = result.optJSONArray(GoogleNameConstants.ARRAY_PHOTOS);
        List<String> photos = new ArrayList<>();
        if (jsonPhotos != null) {
            for (int i = 0; i < jsonPhotos.length(); i++) {
                JSONObject jsonPhoto = jsonPhotos.getJSONObject(i);
                String photoReference = jsonPhoto.getString(GoogleNameConstants.STRING_PHOTO_REFERENCE);
                String imageUrl = String.format("%sphoto?maxwidth=%s&photoreference=%s&key=%s", BASE_URL, 400, photoReference, googleApiKey);
                photos.add(imageUrl);
            }
        }

        // see if the place is open, fail-safe if opening_hours is not present
        JSONObject hours = result.optJSONObject(GoogleNameConstants.OBJECT_HOURS);
        boolean hoursDefined = hours != null && hours.has("open_now");
        Status status;
        if (hoursDefined) {
            boolean opened = hours.getBoolean("open_now");
            status = opened ? Status.OPENED : Status.CLOSED;
            datingPlaceDto.setIsOpened(status.toString());
        }

        // Google Place Reviews
        JSONArray jsonReviews = result.optJSONArray(GoogleNameConstants.ARRAY_REVIEWS);
        List<DatingPlaceReviewDto> reviews = new ArrayList<>();
        if (jsonReviews != null) {
            for (int i = 0; i < jsonReviews.length(); i++) {
                JSONObject jsonReview = jsonReviews.getJSONObject(i);
                String author = jsonReview.optString(GoogleNameConstants.STRING_AUTHOR_NAME, null);
                int reviewRating = jsonReview.optInt(GoogleNameConstants.DOUBLE_RATING, -1);
                String comment = jsonReview.optString(GoogleNameConstants.STRING_TEXT, null);
                long time = jsonReview.optLong(GoogleNameConstants.LONG_TIME, 0);

                DatingPlaceReviewDto datingPlaceReviewDto = new DatingPlaceReviewDto();
                datingPlaceReviewDto.setReviewerName(author);
                datingPlaceReviewDto.setReviewerEmail(author);
                datingPlaceReviewDto.setAverageRating((double) reviewRating);
                datingPlaceReviewDto.setReviewApprovalStatus(ApprovalStatus.APPROVED);
                datingPlaceReviewDto.setReviewerComment(comment);
                Date date = new Date(time * 1000);
                datingPlaceReviewDto.setReviewDate(date);

                reviews.add(datingPlaceReviewDto);
            }
        }

        // address components
        JSONArray addrComponents = result.optJSONArray(GoogleNameConstants.ARRAY_ADDRESS_COMPONENTS);
        if (addrComponents != null) {
            for (int i = 0; i < addrComponents.length(); i++) {
                JSONObject ac = addrComponents.getJSONObject(i);

                JSONArray types = ac.optJSONArray(GoogleNameConstants.ARRAY_TYPES);
                if (types != null) {
                    for (int a = 0; a < types.length(); a++) {
                        if (types.getString(a).equalsIgnoreCase("locality")) {
                            datingPlaceDto.setCity(ac.optString(GoogleNameConstants.STRING_LONG_NAME, ""));
                        } else if (types.getString(a).equalsIgnoreCase("administrative_area_level_1")) {
                            datingPlaceDto.setState(ac.optString(GoogleNameConstants.STRING_LONG_NAME, ""));
                        } else if (types.getString(a).equalsIgnoreCase("country")) {
                            datingPlaceDto.setCountry(ac.optString(GoogleNameConstants.STRING_LONG_NAME, ""));
                        } else if (types.getString(a).equalsIgnoreCase("postal_code")) {
                            datingPlaceDto.setZipCode(ac.optString(GoogleNameConstants.STRING_LONG_NAME));
                        }
                    }
                }
            }
        }

        datingPlaceDto.setDatingPlacesId(id);
        datingPlaceDto.setPlaceName(placeName);
        datingPlaceDto.setAddressLine(formattedAddress);
        datingPlaceDto.setOverAllRating(rating);
        datingPlaceDto.setGoogleAverage(rating);
        datingPlaceDto.setLatitude(latitude);
        datingPlaceDto.setLongitude(longitude);
        datingPlaceDto.setDatingPlaceReviewDtos(reviews);
        datingPlaceDto.setPlacesUrlList(photos);
        if (photos.size() > 0) {
            datingPlaceDto.setCoverPictureUrl(photos.get(0));
        }
        return datingPlaceDto;
    }

    private void getAllResults(int numberOfResults, String uri, List<DatingPlaceDto> datingPlaceDtos) throws IOException {
        int limit = Math.min(numberOfResults, 60);
        int pages = (int) Math.ceil(limit / (double) 20);

        for (int i = 0; i < pages; i++) {
            JSONObject json = executePlaceUrl(uri);
            JSONArray results = json.getJSONArray(GoogleNameConstants.ARRAY_RESULTS);
            parseLocationResults(results, datingPlaceDtos);
            String nextPage = json.optString(GoogleNameConstants.STRING_NEXT_PAGE_TOKEN, null);

            // reduce the limit, update the uri and wait for token, but only if there are more pages to read
            if (nextPage != null && i < pages - 1) {
                limit -= 20;
                uri = String.format("%s%s/json?pagetoken=%s&key=%s",
                        BASE_URL, GoogleNameConstants.METHOD_NEARBY_SEARCH, nextPage, googleApiKey);
                //sleep(3000); // Page tokens have a delay before they are available
            } else {
                break;
            }
            //Collections.sort(datingPlaceDtos, sortByRatingDescending);
        }
    }
}
