package com.realroofers.lovappy.service.event;


import com.realroofers.lovappy.service.event.dto.EventCategoryDto;

import java.util.List;

/**
 * Created by Tejaswi Venupalli on 9/04/17
 */

public interface EventSupportService {

    List<EventCategoryDto> getEventCategories();

    EventCategoryDto getCategory(Integer id);

    EventCategoryDto addCategory(EventCategoryDto category);

    EventCategoryDto updateCategory(EventCategoryDto category);
}
