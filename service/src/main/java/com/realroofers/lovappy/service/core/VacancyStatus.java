package com.realroofers.lovappy.service.core;

public enum VacancyStatus {
    OPEN,
    CLOSED
}
