package com.realroofers.lovappy.service.radio.repo;

import com.realroofers.lovappy.service.radio.model.AudioType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Manoj on 17/12/2017.
 */
public interface AudioTypeRepo extends JpaRepository<AudioType, Integer> {

    List<AudioType> findAllByActiveIsTrueOrderByName();

    AudioType findByName(String name);
}
