package com.realroofers.lovappy.service.product.impl;

import com.realroofers.lovappy.service.product.ProductTypeService;
import com.realroofers.lovappy.service.product.dto.ProductTypeDto;
import com.realroofers.lovappy.service.product.dto.mobile.CategoryItemDto;
import com.realroofers.lovappy.service.product.model.ProductType;
import com.realroofers.lovappy.service.product.repo.ProductTypeRepo;
import com.realroofers.lovappy.service.util.SortOrder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manoj on 06/02/2018.
 */
@Service
public class ProductTypeServiceImpl implements ProductTypeService {

    private final ProductTypeRepo productTypeRepo;

    public ProductTypeServiceImpl(ProductTypeRepo productTypeRepo) {
        this.productTypeRepo = productTypeRepo;
    }

    @Transactional(readOnly = true)
    @Override
    public List<ProductTypeDto> findAll() {
        List<ProductTypeDto> list = new ArrayList<>();
        productTypeRepo.findAll(SortOrder.sortByAsc("typeName")).stream().forEach(productType -> list.add(new ProductTypeDto(productType)));
        return list;
    }

    @Transactional(readOnly = true)
    @Override
    public List<ProductTypeDto> findAllActive() {
        List<ProductTypeDto> list = new ArrayList<>();
        productTypeRepo.findAllByActiveIsTrue(SortOrder.sortByAsc("typeName")).stream().forEach(productType -> list.add(new ProductTypeDto(productType)));
        return list;
    }

    @Transactional(readOnly = true)
    @Override
    public String[] getStringArray() {

        List<ProductTypeDto> productTypes = findAllActive();
        int i = productTypes.size();
        int n = ++i;
        String[] productTypeStrList = new String[n];
        for (int cnt = 0; cnt < productTypes.size(); cnt++) {
            productTypeStrList[cnt] = productTypes.get(cnt).getTypeName();
        }
        return productTypeStrList;
    }


    @Transactional
    @Override
    public ProductTypeDto create(ProductTypeDto productType) {
        return new ProductTypeDto(productTypeRepo.saveAndFlush(new ProductType(productType)));
    }

    @Transactional
    @Override
    public ProductTypeDto update(ProductTypeDto productType) {
        return new ProductTypeDto(productTypeRepo.save(new ProductType(productType)));
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        ProductTypeDto productType = findById(id);
        productType.setActive(false);
        update(productType);
    }

    @Transactional(readOnly = true)
    @Override
    public ProductTypeDto findById(Integer id) {
        ProductType productType = productTypeRepo.findOne(id);
        return productType != null ? new ProductTypeDto(productType) : null;
    }

    @Transactional(readOnly = true)
    @Override
    public ProductTypeDto findByName(String name) {
        ProductType productType = productTypeRepo.findByTypeName(name);
        return productType != null ? new ProductTypeDto(productType) : null;
    }

    @Transactional(readOnly = true)
    @Override
    public List<ProductTypeDto> findByAddedToSurveyIsFalse() {
        List<ProductTypeDto> list = new ArrayList<>();
        productTypeRepo.findAllByAddedToSurveyIsFalseAndActiveIsTrue().stream().forEach(productType -> list.add(new ProductTypeDto(productType)));
        return list;
    }

    @Transactional(readOnly = true)
    @Override
    public List<CategoryItemDto> findAllForMobile() {
        List<CategoryItemDto> list = new ArrayList<>();
        list.add(new CategoryItemDto(0,"All"));
        productTypeRepo.findAllByActiveIsTrue().stream().forEach(productType -> list.add(new CategoryItemDto(productType)));
        return list;
    }
}
