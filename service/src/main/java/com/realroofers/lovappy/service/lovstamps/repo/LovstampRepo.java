package com.realroofers.lovappy.service.lovstamps.repo;

import com.realroofers.lovappy.service.lovstamps.model.Lovstamp;
import com.realroofers.lovappy.service.user.model.Role;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.Gender;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.Set;

/**
 * Created by Daoud Shaheen on 12/31/2017.
 */
public interface LovstampRepo extends JpaRepository<Lovstamp, Integer>, QueryDslPredicateExecutor<Lovstamp> {
    Lovstamp findLovstampByUser(User user);
    Lovstamp findByUser(User user);
    @Query("SELECT lovstamp FROM Lovstamp lovstamp JOIN lovstamp.user.roles userRoles WHERE lovstamp.user.userProfile.isAllowedProfilePic =:allowed AND userRoles.id.role = :role")
    Page<Lovstamp> findByUserUserProfileIsAllowedProfilePic(@Param("allowed") Boolean isAllowedProfilePic, @Param("role") Role role, Pageable pageable);

    @Query("SELECT lovstamp FROM Lovstamp lovstamp JOIN lovstamp.user.roles r WHERE lovstamp.user.userProfile.isAllowedProfilePic =:allowed AND lovstamp.showInHome=:home AND r.id.role.name =:role")
    Page<Lovstamp> findByUserUserProfileIsAllowedProfilePicAndShowHome(@Param("allowed") Boolean isAllowedProfilePic, @Param("role") String role, @Param("home") Boolean showHome, Pageable pageable);

    @Query("SELECT lovstamp FROM Lovstamp lovstamp JOIN lovstamp.user.roles r WHERE lovstamp.user.userProfile.gender =:gender AND lovstamp.user.userProfile.completed=true AND lovstamp.user.activated=true AND lovstamp.audioFileCloud.approved=true AND r.id.role in :roles")
    Page<Lovstamp> findByUserUserProfileGenderAndUserRolesIn(@Param("gender") Gender gender, @Param("roles") Set<Role> roles, Pageable pageable);

    @Query("SELECT lovstamp FROM Lovstamp lovstamp JOIN lovstamp.user.roles r WHERE r.id.role in :roles AND lovstamp.user.userProfile.completed=true AND lovstamp.user.activated=true AND lovstamp.audioFileCloud.approved=true")
    Page<Lovstamp> findAll(@Param("roles") Set<Role> roles, Pageable pageable);
}
