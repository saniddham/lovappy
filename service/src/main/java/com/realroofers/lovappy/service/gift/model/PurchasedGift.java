package com.realroofers.lovappy.service.gift.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.gift.dto.PurchasedGiftDto;
import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Manoj on 02/02/2018.
 */
@Entity
@Data
@Table(name = "gift_purchased")
@EqualsAndHashCode
public class PurchasedGift extends BaseEntity<Integer> implements Serializable {

    @JoinColumn(name = "product")
    @ManyToOne
    Product product;

    @JoinColumn(name = "user")
    @ManyToOne
    private User user;

    @CreationTimestamp
    @Column(name = "purchased_on", columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date purchasedOn;

    public PurchasedGift() {
    }

    public PurchasedGift(PurchasedGiftDto purchasedGift) {
        if(purchasedGift.getId()!=null) {
            this.setId(purchasedGift.getId());
            this.purchasedOn = purchasedGift.getPurchasedOn();
        }else{
            this.purchasedOn = new Date();
        }
        this.product = purchasedGift.getProduct();
        this.user = new User(purchasedGift.getUser().getID());
    }
}
