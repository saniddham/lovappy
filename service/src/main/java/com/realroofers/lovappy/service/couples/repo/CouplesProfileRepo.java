package com.realroofers.lovappy.service.couples.repo;

import com.realroofers.lovappy.service.couples.model.CouplesProfile;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by hasan on 16/10/2017.
 */
public interface CouplesProfileRepo extends JpaRepository<CouplesProfile, Integer> {

    @Query("Select u From User u JOIN u.userProfile p Where p.couplesProfile =:couple AND u !=:user ")
    User findPartner(@Param("user") User user, @Param("couple")CouplesProfile couplesProfile);
}
