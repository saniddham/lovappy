package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.BooleanUtils;

import com.realroofers.lovappy.service.user.model.UserProfile;
import org.springframework.util.StringUtils;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@EqualsAndHashCode
public class ProfilePicsDto {
    private Integer userId;

    private CloudStorageFileDto profilePhotoCloudFile;
    private CloudStorageFileDto photoIdentificationCloudFile;
    private CloudStorageFileDto handsCloudFile;
    private CloudStorageFileDto feetCloudFile;
    private CloudStorageFileDto legsCloudFile;

    private Boolean handsFileSkipped;
    private Boolean feetFileSkipped;
    private Boolean legsFileSkipped;

    public ProfilePicsDto(UserProfile model) {
        if (model != null) {
            this.userId = model.getUserId();

            this.handsCloudFile = new CloudStorageFileDto(model.getHandsCloudFile());
            this.feetCloudFile = new CloudStorageFileDto(model.getFeetCloudFile());
            this.legsCloudFile = new CloudStorageFileDto(model.getLegsCloudFile());
            this.profilePhotoCloudFile = new CloudStorageFileDto(model.getProfilePhotoCloudFile());
            this.photoIdentificationCloudFile = new CloudStorageFileDto(model.getPhotoIdentificationCloudFile());

            this.handsFileSkipped = model.getHandsFileSkipped()==null?false:model.getHandsFileSkipped();
            this.feetFileSkipped = model.getFeetFileSkipped() == null? false:model.getFeetFileSkipped();
            this.legsFileSkipped = model.getLegsFileSkipped() == null? false:model.getLegsFileSkipped();
        }
    }

    public Integer getUserId() {
        return userId;
    }

    public CloudStorageFileDto getProfilePhotoCloudFile() {
        return profilePhotoCloudFile;
    }

    public String getPhotoIdentificationCloudFile() {
        return this.photoIdentificationCloudFile == null || StringUtils.isEmpty(this.photoIdentificationCloudFile.getUrl())
                ? "/images/icons/upload_icons/upload_profile_sample.png"
                : this.photoIdentificationCloudFile.getUrl();
    }

    public void setPhotoIdentificationCloudFile(CloudStorageFileDto photoIdentificationCloudFile) {
        this.photoIdentificationCloudFile = photoIdentificationCloudFile;
    }

    public String getProfilePhotoCloudFileUrl() {
        return this.profilePhotoCloudFile == null || StringUtils.isEmpty(this.profilePhotoCloudFile.getUrl())
                ? "/images/icons/upload_icons/upload_profile_sample.png"
                : this.profilePhotoCloudFile.getUrl();
    }

    public void setProfilePhotoCloudFile(CloudStorageFileDto profilePhotoCloudFile) {
        this.profilePhotoCloudFile = profilePhotoCloudFile;
    }

    public CloudStorageFileDto getHandsCloudFile() {
        return handsCloudFile;
    }

    public String getHandsCloudFileUrl() {
        return this.handsCloudFile == null || StringUtils.isEmpty(this.handsCloudFile.getUrl())
                ? "/images/icons/upload_icons/upload_hand.png"
                : this.handsCloudFile.getUrl();
    }

    public void setHandsCloudFile(CloudStorageFileDto handsCloudFile) {
        this.handsCloudFile = handsCloudFile;
    }

    public CloudStorageFileDto getFeetCloudFile() {
        return feetCloudFile;
    }

    public String getFeetCloudFileUrl() {
        return this.feetCloudFile == null || StringUtils.isEmpty(this.feetCloudFile.getUrl())
                ? "/images/icons/upload_icons/upload_foot.png"
                : this.feetCloudFile.getUrl();
    }

    public void setFeetCloudFile(CloudStorageFileDto feetCloudFile) {
        this.feetCloudFile = feetCloudFile;
    }

    public CloudStorageFileDto getLegsCloudFile() {
        return legsCloudFile;
    }

    public String getLegsCloudFileUrl() {
        return this.legsCloudFile == null || StringUtils.isEmpty(this.legsCloudFile.getUrl())
                ? "/images/icons/upload_icons/upload_legs.png"
                : this.legsCloudFile.getUrl();
    }

    public void setLegsCloudFile(CloudStorageFileDto legsCloudFile) {
        this.legsCloudFile = legsCloudFile;
    }

    public boolean isHandsFileSkipped() {
        return handsFileSkipped;
    }

    public boolean isFeetFileSkipped() {
        return feetFileSkipped;
    }

    public boolean isLegsFileSkipped() {
        return legsFileSkipped;
    }
}
