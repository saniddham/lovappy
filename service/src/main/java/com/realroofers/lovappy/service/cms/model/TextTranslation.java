package com.realroofers.lovappy.service.cms.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * Created by Daoud Shaheen on 6/10/2017.
 */
@Entity
@Table(name="text_translations")
public class TextTranslation extends BaseEntity<Integer> {

    @Column(columnDefinition = "TEXT")
    private String content;
    private String title;
    private String language;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JsonBackReference
    @JoinColumn(name = "text_id")
    private TextWidget textWidget;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public TextWidget getTextWidget() {
        return textWidget;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTextWidget(TextWidget textWidget) {
        this.textWidget = textWidget;
    }
}
