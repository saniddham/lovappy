package com.realroofers.lovappy.service.survey.impl;

import com.realroofers.lovappy.service.survey.UserSurveyService;
import com.realroofers.lovappy.service.survey.dto.SurveyAnswerDto;
import com.realroofers.lovappy.service.survey.dto.SurveyQuestionDto;
import com.realroofers.lovappy.service.survey.dto.UserSurveyDto;
import com.realroofers.lovappy.service.survey.model.SurveyAnswer;
import com.realroofers.lovappy.service.survey.model.SurveyQuestion;
import com.realroofers.lovappy.service.survey.model.SurveyQuestionAnswer;
import com.realroofers.lovappy.service.survey.model.UserSurvey;
import com.realroofers.lovappy.service.survey.repo.SurveyAnswerRepo;
import com.realroofers.lovappy.service.survey.repo.SurveyQuestionRepo;
import com.realroofers.lovappy.service.survey.repo.UserSurveyRepo;
import com.realroofers.lovappy.service.survey.support.SurveyStatus;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Created by hasan on 9/8/2017.
 */
@Service
public class UserSurveyServiceImpl implements UserSurveyService {

    private final UserSurveyRepo userSurveyRepo;
    private final UserRepo userRepo;
    private final SurveyQuestionRepo surveyQuestionRepo;
    private final SurveyAnswerRepo surveyAnswerRepo;
    private Logger logger = LoggerFactory.getLogger(UserSurveyServiceImpl.class);

    @Autowired
    public UserSurveyServiceImpl(UserSurveyRepo userSurveyRepo, UserRepo userRepo,
                                 SurveyQuestionRepo surveyQuestionRepo, SurveyAnswerRepo surveyAnswerRepo) {
        this.userSurveyRepo = userSurveyRepo;
        this.userRepo = userRepo;
        this.surveyQuestionRepo = surveyQuestionRepo;
        this.surveyAnswerRepo = surveyAnswerRepo;
    }

    @Override
    @Transactional(readOnly = true)
    public List<SurveyQuestionDto> getAllQuestions() {
        List<SurveyQuestionDto> questions = new ArrayList<>();
        for (SurveyQuestion surveyQuestion : surveyQuestionRepo.findAll()) {
            questions.add(new SurveyQuestionDto(surveyQuestion));
        }

        return questions;
    }

    @Override
    public Page<SurveyQuestionDto> getAllQuestions(Pageable pageable) {
        return surveyQuestionRepo.findAll(pageable).map(SurveyQuestionDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public SurveyQuestionDto getQuestion(Integer id) {
        SurveyQuestion one = surveyQuestionRepo.findOne(id);
        return one == null ? null : new SurveyQuestionDto(one);
    }

    @Override
    @Transactional
    public SurveyQuestionDto addQuestion(String title) {
        SurveyQuestion question = new SurveyQuestion();
        question.setTitle(title);

        SurveyQuestion save = surveyQuestionRepo.save(question);
        return save == null ? null : new SurveyQuestionDto(save);
    }

    @Override
    @Transactional
    public SurveyQuestionDto updateQuestion(Integer questionID, String title) {
        SurveyQuestion surveyQuestion = surveyQuestionRepo.findOne(questionID);
        surveyQuestion.setTitle(title);

        SurveyQuestion save = surveyQuestionRepo.save(surveyQuestion);
        return save == null ? null : new SurveyQuestionDto(save);
    }

    @Override
    @Transactional(readOnly = true)
    public SurveyAnswerDto getAnswer(Integer id) {
        SurveyAnswer surveyAnswer = surveyAnswerRepo.findOne(id);
        return surveyAnswer == null ? null : new SurveyAnswerDto(surveyAnswer);
    }

    @Override
    @Transactional
    public SurveyAnswerDto addAnswer(Integer questionID, String title) {
        SurveyAnswer surveyAnswer = new SurveyAnswer();
        surveyAnswer.setQuestion(surveyQuestionRepo.findOne(questionID));
        surveyAnswer.setTitle(title);

        SurveyAnswer save = surveyAnswerRepo.save(surveyAnswer);

        return save == null ? null : new SurveyAnswerDto(save);
    }

    @Override
    @Transactional
    public SurveyAnswerDto updateAnswer(Integer answerID, String title) {
        SurveyAnswer surveyAnswer = surveyAnswerRepo.findOne(answerID);
        surveyAnswer.setTitle(title);

        SurveyAnswer save = surveyAnswerRepo.save(surveyAnswer);

        return save == null ? null : new SurveyAnswerDto(save);
    }

    @Override
    @Transactional
    public void deleteAnswer(Integer answerID) {
        surveyAnswerRepo.delete(answerID);
    }

    @Override
    @Transactional
    public UserSurveyDto addUserSurvey(UserSurveyDto userSurveyDto) {
        UserSurvey survey = new UserSurvey();
        survey.setUser(userRepo.findOne(userSurveyDto.getUserDto().getID()));

        List<SurveyQuestionAnswer> questionAnswers = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : userSurveyDto.getAnswers().entrySet()) {
            SurveyQuestion question = surveyQuestionRepo.findOne(entry.getKey());
            SurveyAnswer answer = surveyAnswerRepo.findOne(entry.getValue());
            SurveyQuestionAnswer questionAnswer = new SurveyQuestionAnswer();
            questionAnswer.setQuestion(question);
            questionAnswer.setAnswer(answer);
            questionAnswer.setUserSurvey(survey);

            questionAnswers.add(questionAnswer);
        }
        survey.setQuestionAnswers(questionAnswers);
        survey.setSurveyStatus(SurveyStatus.ANSWERED);

        UserSurvey save = userSurveyRepo.save(survey);

        return save == null ? null : new UserSurveyDto(save);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isAnswered(Integer userId) {
        UserSurvey userSurvey = userSurveyRepo.findByUser(userRepo.findOne(userId));
        if(userSurvey == null)
            return false;
        return userSurvey.getSurveyStatus().equals(SurveyStatus.ANSWERED) ||
                userSurvey.getSurveyStatus().equals(SurveyStatus.NOT_INTERESTED);
    }

    @Override
    @Transactional
    public boolean setAnswers(UserSurveyDto userSurveyDto) {
        try {
            UserSurvey survey = new UserSurvey();
            survey.setUser(userRepo.findOne(userSurveyDto.getUserDto().getID()));
            survey.setSurveyStatus(SurveyStatus.ANSWERED);
            userSurveyRepo.save(survey);
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    @Transactional
    public boolean dontShowSurvey() {
        try {
            UserSurvey survey = new UserSurvey();
            survey.setUser(userRepo.findOne(UserUtil.INSTANCE.getCurrentUserId()));
            survey.setSurveyStatus(SurveyStatus.NOT_INTERESTED);
            userSurveyRepo.save(survey);
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserSurveyDto> getUserSurvey(Pageable pageable) {
        return userSurveyRepo
                .findAllBySurveyStatus(pageable, SurveyStatus.ANSWERED)
                .map(UserSurveyDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public UserSurveyDto getUserSurvey(Integer userID) {
        UserSurvey userSurvey = userSurveyRepo.findByUser(userRepo.findOne(userID));
        return userSurvey == null ? null : new UserSurveyDto(userSurvey);
    }
}
