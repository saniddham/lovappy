package com.realroofers.lovappy.service.order.support;

import com.realroofers.lovappy.service.order.support.OrderDetailType;

/**
 * Created by Daoud Shaheen on 8/10/2018.
 */
public enum ProductType {

    music, gifts, events, messages, ads, datingplaces, credits, dailyquestion;

    public OrderDetailType toOrderType() {
        switch (this) {
            case events:
                return OrderDetailType.EVENT_ATTEND;
            case music:
                return OrderDetailType.MUSIC;
            case gifts:
                return OrderDetailType.GIFT;
            case messages:
                return OrderDetailType.PRIVATE_MESSAGE;
            case ads:
                return OrderDetailType.AD;
            case datingplaces:
                return OrderDetailType.DATING_PLACE;
            case credits:
                return OrderDetailType.CREDITS;
            case dailyquestion:
                return OrderDetailType.DAILY_QUESTION;
        }
        return null;
    }
}
