package com.realroofers.lovappy.service.user.support;

/**
 * Created by Daoud Shaheen on 1/20/2018.
 */
public enum ProfilePictureAccess {
    PUBLIC, PRIVATE, MANAGED
}
