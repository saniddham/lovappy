package com.realroofers.lovappy.service.upload;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.upload.dto.PodcastCategoryDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Manoj
 */
public interface PodcastCategoryService extends AbstractService<PodcastCategoryDto, Integer> {
    Page<PodcastCategoryDto> findAll(Pageable pageable);
}
