package com.realroofers.lovappy.service.survey.model;

import com.realroofers.lovappy.service.survey.support.SurveyStatus;
import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

/**
 * Created by hasan on 9/8/2017.
 */
@Entity
@EqualsAndHashCode
public class UserSurvey {
    @Id
    @GeneratedValue
    private Integer id;

    @OneToOne(targetEntity=User.class, fetch = FetchType.LAZY, cascade =CascadeType.PERSIST )
    @JoinColumn(name = "user")
    private User user;

    @Enumerated(EnumType.STRING)
    private SurveyStatus surveyStatus;

    @OneToMany(mappedBy = "userSurvey", cascade = { CascadeType.ALL })
    private List<SurveyQuestionAnswer> questionAnswers;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SurveyStatus getSurveyStatus() {
        return surveyStatus;
    }

    public void setSurveyStatus(SurveyStatus surveyStatus) {
        this.surveyStatus = surveyStatus;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<SurveyQuestionAnswer> getQuestionAnswers() {
        return questionAnswers;
    }

    public void setQuestionAnswers(List<SurveyQuestionAnswer> questionAnswers) {
        this.questionAnswers = questionAnswers;
    }
}
