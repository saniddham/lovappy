package com.realroofers.lovappy.service.vendor.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.Country;
import com.realroofers.lovappy.service.vendor.model.Vendor;
import com.realroofers.lovappy.service.vendor.model.VendorType;
import com.realroofers.lovappy.service.vendor.model.VerificationType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode
public class VendorDto implements Serializable {

    private Integer id;
    private VendorType vendorType;
    private VerificationType verificationType;
    private String companyName;
    private String primaryContactPerson;
    private String primaryContactNumber;
    private String primaryContactEmail;
    private String alternateContactPerson;
    private String alternateContactEmail;
    private String contactNumber;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String state;
    private String zipCode;
    private CloudStorageFileDto companyLogo;
    private Boolean approved;
    private Boolean registrationStep1;
    private Boolean registrationStep2;
    private Boolean active;
    private UserDto user;
    private Country country;
    private String mobileVerificationCode;
    private String mobileContent;

    public VendorDto() {
    }

    public VendorDto(Vendor vendor) {
        this.id = vendor.getId();
        this.vendorType = vendor.getVendorType();
        this.verificationType = vendor.getVerificationType();
        this.companyName = vendor.getCompanyName();
        this.primaryContactPerson = vendor.getPrimaryContactPerson();
        this.primaryContactNumber = vendor.getPrimaryContactNumber();
        this.primaryContactEmail = vendor.getPrimaryContactEmail();
        this.alternateContactPerson = vendor.getAlternateContactPerson();
        this.alternateContactEmail = vendor.getAlternateContactEmail();
        this.contactNumber = vendor.getContactNumber();
        this.addressLine1 = vendor.getAddressLine1();
        this.addressLine2 = vendor.getAddressLine2();
        this.city = vendor.getCity();
        this.state = vendor.getState();
        this.zipCode = vendor.getZipCode();
        this.companyLogo = vendor.getCompanyLogo() != null ? new CloudStorageFileDto(vendor.getCompanyLogo()) : null;
        this.approved = vendor.getApproved();
        this.registrationStep1 = vendor.getRegistrationStep1();
        this.registrationStep2 = vendor.getRegistrationStep2();
        this.active = vendor.getActive();
        this.user = new UserDto(vendor.getUser());
        this.country = vendor.getCountry();
        this.mobileVerificationCode = vendor.getMobileVerificationCode();
    }

    @Override
    public String toString() {
        return "VendorDto{" +
                "id=" + id +
                ", vendorType=" + vendorType +
                ", companyName='" + companyName + '\'' +
                '}';
    }
}
