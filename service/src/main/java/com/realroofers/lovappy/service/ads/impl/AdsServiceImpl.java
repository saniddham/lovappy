package com.realroofers.lovappy.service.ads.impl;

import com.google.common.collect.Lists;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.realroofers.lovappy.service.ads.AdsService;
import com.realroofers.lovappy.service.ads.dto.*;
import com.realroofers.lovappy.service.ads.model.*;
import com.realroofers.lovappy.service.ads.repo.AdBackgroundColorsRepo;
import com.realroofers.lovappy.service.ads.repo.AdBackgroundImagesRepo;
import com.realroofers.lovappy.service.ads.repo.AdPricingRepo;
import com.realroofers.lovappy.service.ads.repo.AdsRepo;
import com.realroofers.lovappy.service.ads.support.AdMediaType;
import com.realroofers.lovappy.service.ads.support.AdStatus;
import com.realroofers.lovappy.service.ads.support.AdType;
import com.realroofers.lovappy.service.ads.support.AgeRange;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.cloud.repo.CloudStorageRepo;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.CouponDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.repo.CouponRepo;
import com.realroofers.lovappy.service.order.repo.OrderRepo;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.user.model.UserPreference;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.repo.UserPreferenceRepo;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.util.ListMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Eias Altawil on 7/7/2017
 */

@Service
public class AdsServiceImpl implements AdsService {

    private final AdsRepo adsRepo;
    private final CloudStorageRepo cloudStorageRepo;

    private final UserRepo userRepo;
    private final UserPreferenceRepo userPreferenceRepo;

    private final AdBackgroundColorsRepo backgroundColorsRepo;
    private final AdBackgroundImagesRepo backgroundImagesRepo;

    private final AdPricingRepo adPricingRepo;

    private final OrderRepo orderRepo;
    private final OrderService orderService;
    private final CouponRepo couponRepo;

    @Autowired
    public AdsServiceImpl(AdsRepo adsRepo, CloudStorageRepo cloudStorageRepo, UserRepo userRepo,
                          UserPreferenceRepo userPreferenceRepo,
                          AdBackgroundColorsRepo backgroundColorsRepo, AdBackgroundImagesRepo backgroundImagesRepo,
                          AdPricingRepo adPricingRepo, OrderRepo orderRepo, OrderService orderService, CouponRepo couponRepo) {
        this.adsRepo = adsRepo;
        this.cloudStorageRepo = cloudStorageRepo;

        this.userRepo = userRepo;
        this.userPreferenceRepo = userPreferenceRepo;
        this.backgroundColorsRepo = backgroundColorsRepo;
        this.backgroundImagesRepo = backgroundImagesRepo;
        this.adPricingRepo = adPricingRepo;
        this.orderRepo = orderRepo;
        this.orderService = orderService;
        this.couponRepo = couponRepo;
    }


    @Override
    @Transactional
    public OrderDto addAd(AdDto ad) {
        Ad adEntity = new Ad();

        List<AdTargetArea> targetAreas = new ArrayList<>();
        if (ad.getTargetAreas() != null) {
            for (AdTargetAreaDto adTargetAreaDto : ad.getTargetAreas()) {
                AdTargetArea adTargetArea = new AdTargetArea();
                adTargetArea.setAd(adEntity);
                adTargetArea.setAddress(adTargetAreaDto.getAddress().toAddress());
                targetAreas.add(adTargetArea);
            }
        }
        adEntity.setTextAlign(ad.getTextAlign());
        adEntity.setEmail(ad.getEmail());
        adEntity.setLength(ad.getLength());
        adEntity.setPromote(ad.getPromote());
        adEntity.setCoupon(ad.getCoupon());

        if (ad.getMediaFileCloud() != null)
            adEntity.setMediaFileCloud(cloudStorageRepo.findOne(ad.getMediaFileCloud().getId()));
        adEntity.setMediaFileUrl(ad.getMediaFileUrl());
        adEntity.setAudioType(ad.getAudioType());
        adEntity.setAdType(ad.getAdType());
        adEntity.setMediaType(ad.getMediaType());
        adEntity.setTargetAreas(targetAreas);
        adEntity.setGender(ad.getGender());
        adEntity.setAgeRange(ad.getAgeRange());
        adEntity.setLanguage(new Language(ad.getLanguage().getId()));
        adEntity.setPreferences(ad.getPreferences());
        adEntity.setStatus(AdStatus.PENDING);
        adEntity.setUrl(ad.getUrl());
        adEntity.setCreated(new Date());
        adEntity.setPrice(ad.getPrice());
        adEntity.setFeatured(ad.getFeatured());

        adEntity.setText(ad.getText());
        adEntity.setTextColor(ad.getTextColor());

        if (ad.getBackgroundColorID() != null) {
            AdBackgroundColor backgroundColor = backgroundColorsRepo.findOne(ad.getBackgroundColorID());
            adEntity.setBackgroundColor(backgroundColor.getColor());
        }

        if (ad.getBackgroundImageID() != null) {
            CloudStorageFile backgroundImage = cloudStorageRepo.findOne(ad.getBackgroundImageID());
            adEntity.setBackgroundFileCloud(backgroundImage);
        }

        if (ad.getFeaturedAdBackgroundID() != null) {
            CloudStorageFile backgroundImage = cloudStorageRepo.findOne(ad.getFeaturedAdBackgroundID());
            adEntity.setFeaturedAdBackgroundFileCloud(backgroundImage);
        }

        adEntity = adsRepo.saveAndFlush(adEntity);
        return orderService.addOrder(adEntity, OrderDetailType.AD, ad.getPrice(), 1, null,
                ad.getCoupon(), PaymentMethodType.STRIPE);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AdDto> getAds(Integer userID, List<Integer> excludedAds, Pageable pageable) {
        Page<AdDto> list = null;

        UserProfile userProfile = userRepo.findOne(userID).getUserProfile();
        UserPreference pref = userPreferenceRepo.findOne(userID);
        if (userProfile != null && pref != null) {
            Iterable<Ad> excluded = adsRepo.findAll(excludedAds);

            LocalDate birthDate = userProfile.getBirthDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            int age = Period.between(birthDate, LocalDate.now()).getYears();

            BooleanExpression excludeFilter = QAd.ad.notIn(Lists.newArrayList(excluded));
            BooleanExpression genderFilter = QAd.ad.gender.eq(userProfile.getGender()).or(QAd.ad.gender.eq(Gender.BOTH));
            BooleanExpression ageFilter = null;

            AgeRange rangeByAge = AgeRange.getRangeByAge(age);
            if (rangeByAge != null)
                ageFilter = QAd.ad.ageRange.eq(rangeByAge);

            Predicate qAd = QAd.ad.isNotNull()
                    .and(genderFilter).and(excludeFilter).and(ageFilter)
                    .and(QAd.ad.status.eq(AdStatus.APPROVED));

            Page<Ad> all = adsRepo.findAll(qAd, pageable);
            list = all.map(AdDto::new);
        }

        return list;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AdDto> getAllAds(Integer page, Integer limit) {

        return adsRepo.findAll(new PageRequest(page - 1, limit)).map(AdDto::new);
    }


    @Override
    @Transactional(readOnly = true)
    public Page<AdDto> findAll(Pageable pageable) {

        return adsRepo.findAll(pageable).map(AdDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public AdDto getAd(Integer id) {
        Ad ad = adsRepo.findOne(id);
        return ad == null ? null : new AdDto(ad);
    }

    @Override
    @Transactional
    public AdDto changeAdStatus(Integer id, AdStatus status) {
        Ad ad = adsRepo.findOne(id);
        if (ad != null) {
            ad.setStatus(status);
            adsRepo.save(ad);
            return new AdDto(ad);
        }

        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdBackgroundColorDto> getAllBackgroundColors() {
        List<AdBackgroundColor> all = backgroundColorsRepo.findAll();
        return new ListMapper<>(all).map(AdBackgroundColorDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdBackgroundImageDto> getAllBackgroundImages() {
        List<AdBackgroundImage> all = backgroundImagesRepo.findAll();
        return new ListMapper<>(all).map(AdBackgroundImageDto::new);
    }

    @Override
    @Transactional
    public AdBackgroundColorDto addBackgroundColor(String color) {
        AdBackgroundColor adBackgroundColor = new AdBackgroundColor();
        adBackgroundColor.setColor(color);

        AdBackgroundColor save = backgroundColorsRepo.save(adBackgroundColor);

        return save == null ? null : new AdBackgroundColorDto(save);
    }

    @Override
    @Transactional
    public AdBackgroundImageDto addBackgroundImage(CloudStorageFileDto file, CloudStorageFileDto file2) {
        CloudStorageFile cloudStorageFile = new CloudStorageFile();
        cloudStorageFile.setBucket(file.getBucket());
        cloudStorageFile.setName(file.getName());
        cloudStorageFile.setUrl(file.getUrl());

        CloudStorageFile cloudStorageFile2 = new CloudStorageFile();
        cloudStorageFile2.setBucket(file2.getBucket());
        cloudStorageFile2.setName(file2.getName());
        cloudStorageFile2.setUrl(file2.getUrl());

        AdBackgroundImage adBackgroundImage = new AdBackgroundImage();
        adBackgroundImage.setImageFileCloud(cloudStorageFile);
        adBackgroundImage.setFeaturedAdBackgroundFileCloud(cloudStorageFile2);

        AdBackgroundImage save = backgroundImagesRepo.save(adBackgroundImage);

        return save == null ? null : new AdBackgroundImageDto(save);
    }

    @Override
    @Transactional
    public void deleteBackgroundColor(Integer id) {
        backgroundColorsRepo.delete(id);
    }

    @Override
    @Transactional
    public void deleteBackgroundImage(Integer id) {
        backgroundImagesRepo.delete(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdPricingDto> getAdPricing(AdType adType) {
        List<AdPricing> all;

        if (adType == null)
            all = adPricingRepo.findAll();
        else
            all = adPricingRepo.findByAdTypeOrderByPriceAsc(adType);

        return new ListMapper<>(all).map(AdPricingDto::new);
    }

    @Transactional(readOnly = true)
    @Override
    public AdPricingDto getAdPricingById(Integer id) {
        AdPricing adPricing = adPricingRepo.findOne(id);
        return adPricing == null ? null : new AdPricingDto(adPricing);
    }

    @Transactional
    @Override
    public AdPricingDto savePricing(AdPricingDto adPricing) {
        AdPricing pricing = null;

        if (adPricing.getId() != null)
            pricing = adPricingRepo.findOne(adPricing.getId());

        if (pricing == null)
            pricing = new AdPricing();

        pricing.setDuration(adPricing.getDuration());
        pricing.setPrice(adPricing.getPrice());
        pricing.setMediaType(adPricing.getMediaType());
        pricing.setAdType(adPricing.getAdType());
        AdPricing save = adPricingRepo.save(pricing);

        return save == null ? null : new AdPricingDto(save);
    }

    @Transactional(readOnly = true)
    @Override
    public CalculatePriceDto calculatePrice(AdType adType, AdMediaType mediaType, Integer duration, String couponCode) {
        Double price = 0.0;
        Double totalPrice = 0.0;
        AdPricing pricing;

        if (duration == null || mediaType == AdMediaType.TEXT) {
            pricing = adPricingRepo.findByAdTypeAndMediaType(adType, mediaType);
        } else {
            List<AdPricing> pricings = adPricingRepo.findByAdTypeAndMediaTypeOrderByDurationAsc(adType, mediaType);
            pricing = pricings.stream().filter(x -> x.getDuration() > duration).findFirst().orElse(null);
        }

        if (pricing != null) {
            price = pricing.getPrice();
            totalPrice = pricing.getPrice();
        }

        Double discountValue = 0.0;
        CouponDto coupon = orderService.getCoupon(couponCode, CouponCategory.AD);
        if (coupon != null)
            discountValue = totalPrice * coupon.getDiscountPercent() * 0.01;
        else {
            coupon = new CouponDto();
            coupon.setDiscountPercent(0);
        }
        totalPrice -= discountValue;

        return new CalculatePriceDto(price, coupon, totalPrice);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<AdDto> getFeaturedAdsByStatus(AdStatus status, Pageable pageable) {
        return adsRepo.findByFeaturedAndStatus(true, status, pageable).map(AdDto::new);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<AdDto> getRegularAdsByStatus(AdStatus status, Pageable pageable) {
        return adsRepo.findByFeaturedAndStatus(false, status, pageable).map(AdDto::new);
    }

    @Override
    public Page<AdDto> getRandomAds(Boolean featured, AdStatus status, Pageable pageable) {
        return adsRepo.findAllRandom(featured, status, pageable).map(AdDto::new);
    }
}
