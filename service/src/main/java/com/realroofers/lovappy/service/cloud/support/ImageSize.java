package com.realroofers.lovappy.service.cloud.support;

/**
 * @author Eias Altawil
 */
public enum ImageSize {

    SMALL("small"), MEDIUM("medium"), LARGE("large");

    String text;

    ImageSize(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
