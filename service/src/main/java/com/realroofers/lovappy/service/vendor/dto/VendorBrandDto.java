package com.realroofers.lovappy.service.vendor.dto;

import com.realroofers.lovappy.service.product.dto.BrandDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.vendor.model.VendorBrand;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class VendorBrandDto {

    private Integer id;
    private BrandDto brand;
    private VendorDto vendor;
    private Boolean approved = false;
    private Boolean active = true;
    private UserDto createdBy;
    private UserDto approvedBy;

    public VendorBrandDto() {
    }


    public VendorBrandDto(VendorBrand vendorBrand) {
        if (vendorBrand.getId() != null) {
            this.id = vendorBrand.getId();
        }
        if (vendorBrand.getBrand() != null) {
            this.brand = new BrandDto(vendorBrand.getBrand());
        }
        if (vendorBrand.getVendor() != null) {
            this.vendor = new VendorDto(vendorBrand.getVendor());
        }
        this.approved = vendorBrand.getApproved();
        this.active = vendorBrand.getActive();

        if (vendorBrand.getCreatedBy() != null) {
            this.createdBy = new UserDto(vendorBrand.getCreatedBy());
        }
        if (vendorBrand.getApprovedBy() != null) {
            this.approvedBy = new UserDto(vendorBrand.getApprovedBy());
        }
    }
}
