package com.realroofers.lovappy.service.user.model;

import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 1/18/2018.
 */
@Entity
@Table(name="profile_transactions")
@AssociationOverrides({
        @AssociationOverride(name = "id.userFrom",
                joinColumns = @JoinColumn(name = "user_from")),
        @AssociationOverride(name = "id.userTo",
                joinColumns = @JoinColumn(name = "user_to")) })
@DynamicUpdate
@EqualsAndHashCode
public class ProfileTransaction implements Serializable {

    @EmbeddedId
    private ProfileTransactionPK id;

    @CreationTimestamp
    @Column(name = "created", insertable = false, updatable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date created;

    private Integer transactionCount;

    private Date lastTransaction;

    private Boolean seen;

    @EmbeddedId
    public ProfileTransactionPK getId() {
        return id;
    }

    public void setId(ProfileTransactionPK id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Integer getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(Integer transactionCount) {
        this.transactionCount = transactionCount;
    }

    public Date getLastTransaction() {
        return lastTransaction;
    }

    public void setLastTransaction(Date lastTransaction) {
        this.lastTransaction = lastTransaction;
    }

    public Boolean getSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }
}
