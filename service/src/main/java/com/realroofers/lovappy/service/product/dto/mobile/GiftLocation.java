package com.realroofers.lovappy.service.product.dto.mobile;

import com.realroofers.lovappy.service.vendor.model.VendorLocation;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode
public class GiftLocation implements Serializable{

    private Integer id;
    private String locationName;

    public GiftLocation() {
    }

    public GiftLocation(VendorLocation vendorLocation) {
        if (vendorLocation.getId()!=null) {
            this.id = vendorLocation.getId();
        }

        StringBuilder stringBuilder=new StringBuilder();

        if(vendorLocation.getCompanyName()!=null){
            stringBuilder.append(vendorLocation.getCompanyName());
        }
        if(vendorLocation.getAddressLine1()!=null){
            stringBuilder.append(" :: ");
            stringBuilder.append(vendorLocation.getAddressLine1());
        }
        if(vendorLocation.getAddressLine2()!=null){
            stringBuilder.append("/ ");
            stringBuilder.append(vendorLocation.getAddressLine2());
        }
        if(vendorLocation.getState()!=null){
            stringBuilder.append("/ ");
            stringBuilder.append(vendorLocation.getState());
        }
        if(vendorLocation.getCountry()!=null){
            stringBuilder.append("/ ");
            stringBuilder.append(vendorLocation.getCountry());
        }

        this.locationName = stringBuilder.toString();
    }
}
