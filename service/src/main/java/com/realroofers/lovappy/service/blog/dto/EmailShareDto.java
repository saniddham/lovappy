package com.realroofers.lovappy.service.blog.dto;

import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 7/21/2017.
 */
@EqualsAndHashCode
public class EmailShareDto implements Serializable {

    private String email;
    private String name;
    private String shareWithEmail;
    private String recipientName;
    private String shareUrl;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShareWithEmail() {
        return shareWithEmail;
    }

    public void setShareWithEmail(String shareWithEmail) {
        this.shareWithEmail = shareWithEmail;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }
}
