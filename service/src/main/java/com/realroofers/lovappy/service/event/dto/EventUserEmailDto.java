package com.realroofers.lovappy.service.event.dto;


import com.realroofers.lovappy.service.event.model.EventUserEmail;
import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * Created by Tejaswi Venupalli on 8/28/17
 */
@EqualsAndHashCode
public class EventUserEmailDto {

    private Integer eventEmailId;
    private String emailId;
    private User userId;
    private String title;
    private String emailBody;
    private Date sentDate;


    public EventUserEmailDto(){

    }

    public EventUserEmailDto(EventUserEmail email){
        this.emailId = email.getEmailId();
        this.userId = email.getUserId();
        this.title = email.getTitle();
        this.emailBody = email.getEmailBody();
        this.sentDate = email.getSentDate();
    }

    public Integer getEventEmailId() {
        return eventEmailId;
    }

    public void setEventEmailId(Integer eventEmailId) {
        this.eventEmailId = eventEmailId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }
}
