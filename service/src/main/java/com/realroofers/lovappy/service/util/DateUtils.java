package com.realroofers.lovappy.service.util;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 12/31/2017.
 */
public class DateUtils {

    public static boolean daysBetween(Date fromDate, Date toDate, Integer days) {
        Calendar c = Calendar.getInstance();
        c.setTime(fromDate);
        c.add(Calendar.DATE, days);
        return c.getTime().compareTo(toDate) > 0;
    }
    public static boolean daysAfter(Date fromDate, Date toDate, Integer days) {
        Calendar c = Calendar.getInstance();
        c.setTime(fromDate);
        c.add(Calendar.DATE, days);
        return c.getTime().after(toDate);
    }
    public static Integer getAge(Date birthDate) {
        int age = 0;
        if (birthDate == null) {

            return age;
        }
        Calendar today = Calendar.getInstance();
        Calendar birthDate1 = Calendar.getInstance();
        birthDate1.setTime(birthDate);
        if (birthDate1.after(today)) {
            return age;
            //throw new IllegalArgumentException("You don't exist yet");
        }
        int todayYear = today.get(Calendar.YEAR);
        int birthDateYear = birthDate1.get(Calendar.YEAR);
        int todayDayOfYear = today.get(Calendar.DAY_OF_YEAR);
        int birthDateDayOfYear = birthDate1.get(Calendar.DAY_OF_YEAR);
        int todayMonth = today.get(Calendar.MONTH);
        int birthDateMonth = birthDate1.get(Calendar.MONTH);
        int todayDayOfMonth = today.get(Calendar.DAY_OF_MONTH);
        int birthDateDayOfMonth = birthDate1.get(Calendar.DAY_OF_MONTH);
        age = todayYear - birthDateYear;

        // If birth date is greater than todays date (after 2 days adjustment of leap year) then decrement age one year
        if ((birthDateDayOfYear - todayDayOfYear > 3) || (birthDateMonth > todayMonth)) {
            age--;

            // If birth date and todays date are of same month and birth day of month is greater than todays day of month then decrement age
        } else if ((birthDateMonth == todayMonth) && (birthDateDayOfMonth > todayDayOfMonth)) {
             age--;
        }

        return age;
    }
}
