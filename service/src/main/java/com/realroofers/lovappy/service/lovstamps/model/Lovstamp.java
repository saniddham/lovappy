package com.realroofers.lovappy.service.lovstamps.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.querydsl.core.annotations.QueryInit;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Daoud Shaheen on 12/31/2017.
 */

@Entity
@Table(name="lovdrop")
@Data
@EqualsAndHashCode
public class Lovstamp  extends BaseEntity<Integer> {

    @Column(name = "record_file")
    private String recordFile;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile audioFileCloud;

    @Column(name = "record_seconds")
    private Integer recordSeconds;

    @QueryInit("*.*")
    @OneToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "user")
    private User user;

    @ManyToOne
    @JoinColumn(name = "language")
    private Language language;

    @Column(name = "number_updates", columnDefinition="INT(11) default '0'", nullable = false)
    private Integer numberOfUpdates;

    @ManyToMany(mappedBy = "listenLovstampSet", fetch = FetchType.LAZY)
    @JsonBackReference
    private Set<User> listioners = new HashSet<>(0);

    @Column(name = "female_listeners", columnDefinition="INT(11) default '0'", nullable = false)
    private Integer numberOfFemaleListeners;

    @Column(name = "male_listeners", columnDefinition="INT(11) default '0'", nullable = false)
    private Integer numberOfMaleListeners;

    @Column(name = "num_listeners", columnDefinition="INT(11) default '0'", nullable = false)
    private Integer numberOfListeners;

    @Column(name = "show_home", columnDefinition = "BIT(1) default 0")
    private Boolean showInHome;

    @Column(name = "skip", columnDefinition = "BIT(1) default 0")
    private Boolean skip;
}
