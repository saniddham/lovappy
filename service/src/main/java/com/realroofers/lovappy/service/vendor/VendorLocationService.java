package com.realroofers.lovappy.service.vendor;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.gift.dto.FilerObject;
import com.realroofers.lovappy.service.vendor.model.VendorLocation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by Manoj on 04/02/2018.
 */
public interface VendorLocationService extends AbstractService<VendorLocation, Integer> {

    String[] findLocationListByLoginUser(Integer loginUser);

    Page<VendorLocation> findLocationsByLoginUser(Integer loginUser, Pageable pageable,FilerObject filerObject);

    List<VendorLocation> findLocationsByLoginUser(Integer loginUser);

    long getVendorLocationCountByUser(Integer loginUser);

    List<VendorLocation> findAllVendorLocations(Integer userId);

    List<VendorLocation> findAllVendorLocationsNull();
}
