package com.realroofers.lovappy.service.product.dto;

import com.realroofers.lovappy.service.product.dto.mobile.ItemDto;
import lombok.Data;

import java.util.List;

@Data
public class GiftResponsePage {

    List<ItemDto> content;
    long totalElements;
    long totalPages;
    long numberOfElements;
    long size;
    long number;
    String category;
}
