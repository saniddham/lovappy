package com.realroofers.lovappy.service.music.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class MusicResponseTemplate implements Serializable{

    @Id
    @GeneratedValue
    private Integer id;

    @Enumerated(EnumType.STRING)
    private MusicResponseType type;

    private String template;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MusicResponseType getType() {
        return type;
    }

    public void setType(MusicResponseType type) {
        this.type = type;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
}
