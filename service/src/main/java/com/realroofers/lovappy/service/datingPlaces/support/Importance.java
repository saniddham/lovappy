package com.realroofers.lovappy.service.datingPlaces.support;

/**
 * Created by Darrel Rayen on 1/6/18.
 */
public enum Importance {

    MUST("MUST", 4D),
    SHOULD("SHOULD", 3D),
    COULD("COULD", 1D),
    NA("", 0D);

    String text;
    Double value;
    Importance(String text, Double value) {
        this.text = text;
        this.value = value;
    }

    public Double getValue() {
        return value;
    }

    @Override
    public String toString() {
        return text;
    }
}
