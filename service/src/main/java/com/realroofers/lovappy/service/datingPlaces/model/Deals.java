package com.realroofers.lovappy.service.datingPlaces.model;

import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darrel Rayen on 1/17/18.
 */
@Entity
@Table(name = "deals")
@EqualsAndHashCode
public class Deals {

    @Id
    @GeneratedValue
    private Integer dealId;

    private Double startPrice;

    private Double endPrice;

    private Double feePercentage;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "deals", cascade = CascadeType.ALL)
    private List<DatingPlaceDeals> placeDeals = new ArrayList<>();

    public Integer getDealId() {
        return dealId;
    }

    public void setDealId(Integer dealId) {
        this.dealId = dealId;
    }

    public Double getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(Double startPrice) {
        this.startPrice = startPrice;
    }

    public Double getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(Double endPrice) {
        this.endPrice = endPrice;
    }

    public Double getFeePercentage() {
        return feePercentage;
    }

    public void setFeePercentage(Double feePercentage) {
        this.feePercentage = feePercentage;
    }

    public List<DatingPlaceDeals> getPlaceDeals() {
        return placeDeals;
    }

    public void setPlaceDeals(List<DatingPlaceDeals> placeDeals) {
        this.placeDeals = placeDeals;
    }
}
