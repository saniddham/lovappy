package com.realroofers.lovappy.service.lovstamps.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.lovstamps.model.Lovstamp;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;


/**
 * Created by Eias Altawil on 5/12/17
 */

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Data
@ToString
@EqualsAndHashCode
public class LovstampDto implements Serializable {

    private Integer id;
    private CloudStorageFileDto audioFileCloud;
    private Integer recordSeconds = 0;
    private UserDto user;
    private LanguageDto language;
    private Integer numberOfUpdates;
    private Integer numberOfFemaleListeners;
    private Integer numberOfMaleListeners;
    private Integer numberOfListeners;
    private Integer nextMemberId;
    private Integer prevMemberId;
    private Boolean attractive;
    private Boolean showHome;
    private Date createdOn;
    private Date updatedOn;
    private Boolean skip;


    public LovstampDto() {
    }

    public LovstampDto(Lovstamp lovstamp) {
        if(lovstamp != null) {
            this.id = lovstamp.getId();
            this.audioFileCloud = new CloudStorageFileDto(lovstamp.getAudioFileCloud());
            this.recordSeconds = lovstamp.getRecordSeconds();
            this.user = new UserDto(lovstamp.getUser());
            this.language = new LanguageDto(lovstamp.getLanguage());
            this.numberOfUpdates = lovstamp.getNumberOfUpdates();
            this.numberOfFemaleListeners = lovstamp.getNumberOfFemaleListeners() == null ? 0 : lovstamp.getNumberOfFemaleListeners();
            this.numberOfMaleListeners=  lovstamp.getNumberOfMaleListeners() == null ? 0 : lovstamp.getNumberOfMaleListeners();
            this.numberOfListeners = lovstamp.getNumberOfListeners();
            this.attractive= "Y".equals(lovstamp.getUser().getUserProfile().getStatuses().get("attractmany"));
            this.showHome = lovstamp.getShowInHome();
            this.createdOn = lovstamp.getCreated();
            this.updatedOn = lovstamp.getUpdated();
            this.skip = lovstamp.getSkip();
        }
    }

    public LovstampDto(Lovstamp lovstamp, Boolean noUser) {
        if(lovstamp != null) {
            this.id = lovstamp.getId();
            this.audioFileCloud = new CloudStorageFileDto(lovstamp.getAudioFileCloud());
            this.recordSeconds = lovstamp.getRecordSeconds();
            this.language = new LanguageDto(lovstamp.getLanguage());
            this.numberOfUpdates = lovstamp.getNumberOfUpdates();
            this.numberOfFemaleListeners = lovstamp.getNumberOfFemaleListeners() == null ? 0 : lovstamp.getNumberOfFemaleListeners();
            this.numberOfMaleListeners=  lovstamp.getNumberOfMaleListeners() == null ? 0 : lovstamp.getNumberOfMaleListeners();
            this.numberOfListeners = lovstamp.getNumberOfListeners();
            this.showHome = lovstamp.getShowInHome();
            this.createdOn = lovstamp.getCreated();
            this.updatedOn = lovstamp.getUpdated();
            this.skip = lovstamp.getSkip();
        }
    }
    public String getFormatedTime(){
        int hours = (int)Math.floor(recordSeconds / 3600);
        int mins = (int)Math.floor(recordSeconds / 60 % 60);
        int secs = (int)Math.floor(recordSeconds % 60);
        if(hours>0) {
            return hours+":" + (mins>=9?""+mins : "0"+mins)
                    +":" + (secs>=9?""+secs : "0"+secs);
        } else if(mins > 0){
            return (mins>=9?""+mins : "0"+mins)
                    +":" + (secs>=9?""+secs : "0"+secs);
        }
        return "00:"+ (secs>=9?""+secs : "0"+secs);
    }
}
