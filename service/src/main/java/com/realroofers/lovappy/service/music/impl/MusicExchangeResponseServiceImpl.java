package com.realroofers.lovappy.service.music.impl;

import com.realroofers.lovappy.service.music.MusicExchangeResponseService;
import com.realroofers.lovappy.service.music.dto.MusicResponseDto;
import com.realroofers.lovappy.service.music.model.MusicExchange;
import com.realroofers.lovappy.service.music.model.MusicResponse;
import com.realroofers.lovappy.service.music.model.MusicResponseTemplate;
import com.realroofers.lovappy.service.music.repo.MusicResponseRepo;
import com.realroofers.lovappy.service.music.repo.MusicResponseTemplateRepo;
import com.realroofers.lovappy.service.user.UserService;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MusicExchangeResponseServiceImpl implements MusicExchangeResponseService {

    private final MusicResponseRepo musicResponseRepo;
    private final MusicResponseTemplateRepo musicResponseTemplateRepo;
    private final UserService userService;

    public MusicExchangeResponseServiceImpl(MusicResponseRepo musicResponseRepo,
                                            MusicResponseTemplateRepo musicResponseTemplateRepo,
                                            UserService userService) {
        this.musicResponseRepo = musicResponseRepo;
        this.musicResponseTemplateRepo = musicResponseTemplateRepo;
        this.userService = userService;
    }

    @Override
    public MusicResponse sendResponse(MusicResponse musicResponseDto) {

        MusicResponseTemplate template = musicResponseTemplateRepo.findTopByType(musicResponseDto.getType());

        if (template != null) {
            musicResponseDto.setResponse(template.getTemplate());
            musicResponseDto.setResponseDate(new Date());
            musicResponseDto.setSeen(false);

            return musicResponseRepo.saveAndFlush(musicResponseDto);
        }

        return null;
    }

    @Override
    public MusicResponse update(MusicResponse musicResponse) {
        return musicResponseRepo.save(musicResponse);
    }

    @Override
    public Page<MusicResponseDto> getResponseForLoginUser(Integer currentUser, Pageable pageable) {

        User user = userService.getUserById(currentUser);

        Page<MusicResponse> responses = musicResponseRepo.findByMusicExchange_FromUser(user, pageable);

        return responses.map(MusicResponseDto::new);
    }

    @Override
    public MusicResponse getResponseForExchange(MusicExchange musicExchange) {
        return musicResponseRepo.findByMusicExchange(musicExchange);
    }

    @Override
    public MusicResponse findById(Integer musicExchange) {
        return musicResponseRepo.findOne(musicExchange);
    }

    @Override
    public List<MusicResponseDto> getInboxResponsesForLoginUser(Integer userId) {
        User user = userService.getUserById(userId);

        List<MusicResponse> responses = musicResponseRepo.findByMusicExchange_FromUserAndSeenIsFalse(user);

        List<MusicResponseDto> responseDtoList = new ArrayList<>();

        responses.stream().forEach(musicResponse -> responseDtoList.add(new MusicResponseDto(musicResponse)));
        return responseDtoList;
    }

    @Override
    public MusicResponseDto getOneInboxResponsesForLoginUser(Integer userId) {
        User user = userService.getUserById(userId);
        MusicResponse musicResponse = musicResponseRepo.findTopByMusicExchange_FromUserAndSeenIsFalse(user);

        return musicResponse != null ? new MusicResponseDto(musicResponse) : null;
    }
}
