package com.realroofers.lovappy.service.vendor.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.product.model.Brand;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.vendor.dto.VendorBrandDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode
@Table(name = "vendor_brand")
public class VendorBrand extends BaseEntity<Integer> {

    @JoinColumn(name = "brand")
    @ManyToOne
    private Brand brand;

    @JoinColumn(name = "vendor")
    @ManyToOne
    private Vendor vendor;

    @Column(name = "is_approved", columnDefinition = "boolean default false", nullable = false)
    private Boolean approved = false;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;

    @JoinColumn(name = "created_by")
    @ManyToOne
    private User createdBy;

    @JsonIgnore
    @JoinColumn(name = "approved_by")
    @ManyToOne
    private User approvedBy;


    public VendorBrand() {
    }


    public VendorBrand(VendorBrandDto vendorBrand) {
        if (vendorBrand.getId() != null) {
            this.setId(vendorBrand.getId());
        }
        if (vendorBrand.getBrand() != null) {
            this.brand = new Brand(vendorBrand.getBrand());
        }
        if (vendorBrand.getVendor() != null) {
            this.vendor = new Vendor(vendorBrand.getVendor());
        }
        this.approved = vendorBrand.getApproved();
        this.active = vendorBrand.getActive();

        if (vendorBrand.getCreatedBy() != null) {
            this.createdBy = new User(vendorBrand.getCreatedBy().getID());
        }
        if (vendorBrand.getApprovedBy() != null) {
            this.approvedBy = new User(vendorBrand.getApprovedBy().getID());
        }
    }
}
