package com.realroofers.lovappy.service.questions.model;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Darrel Rayen on 9/18/18.
 */
@Entity
@Table(name = "user_questions_responses")
@EqualsAndHashCode
public class UserResponse extends BaseEntity<Long> {

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Enumerated(EnumType.STRING)
    private ApprovalStatus responseApprovalStatus;

    private Date responseDate;

    private Integer recordSeconds;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile audioResponse;

    @ManyToOne
    @JoinColumn(name = "question_id", nullable = false)
    private DailyQuestion question;

    @ManyToOne
    @JoinColumn(name = "language")
    private Language language;


    public UserResponse() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DailyQuestion getQuestion() {
        return question;
    }

    public void setQuestion(DailyQuestion question) {
        this.question = question;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    public CloudStorageFile getAudioResponse() {
        return audioResponse;
    }

    public void setAudioResponse(CloudStorageFile audioResponse) {
        this.audioResponse = audioResponse;
    }

    public Integer getRecordSeconds() {
        return recordSeconds;
    }

    public void setRecordSeconds(Integer recordSeconds) {
        this.recordSeconds = recordSeconds;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public ApprovalStatus getResponseApprovalStatus() {
        return responseApprovalStatus;
    }

    public void setResponseApprovalStatus(ApprovalStatus responseApprovalStatus) {
        this.responseApprovalStatus = responseApprovalStatus;
    }
}
