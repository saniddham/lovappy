package com.realroofers.lovappy.service.user.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlace;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceClaim;
import com.realroofers.lovappy.service.event.model.EventUserPicture;
import com.realroofers.lovappy.service.likes.model.Like;
import com.realroofers.lovappy.service.lovstamps.model.Lovstamp;
import com.realroofers.lovappy.service.notification.model.UserNotification;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/*
 * User Model Class 
 * 
 * @Mehmet
 */
@Entity
@Table(name = "user")
@DynamicUpdate
@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Serializable {

    private static final long serialVersionUID = 5826218590381681812L;

    @Id
    @GeneratedValue
    private Integer userId;

    @Column(unique = true, nullable = false)
    private String email;
    private String password;
    private String title;
    private String firstName;
    private String lastName;
    private String phoneNumber;

    @Column(name = "num_logins", columnDefinition = "BIGINT(20) default 0")
    private Long loginsCount;

    @Column(name = "banned", columnDefinition = "BIT(1) default 0")
    private Boolean banned;

    @Column(name = "paused", columnDefinition = "BIT(1) default 0")
    private Boolean paused;

    @CreationTimestamp
    @Column(columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date createdOn;

    @Column(columnDefinition = "DATETIME")
    private Date modifiedOn;

    private boolean enabled;

    private Boolean emailNotifications;

    @Column(nullable = false)
    private Boolean emailVerified;

    @Column(columnDefinition = "boolean default false", nullable = false)
    private Boolean mobileVerified = false;

    private String emailVerificationKey;

    private Boolean isTweeted;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    private UserProfile userProfile;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    private UserPreference userPreference;

    @Column(name = "last_login")
    private Date lastLogin;

    @Column(name = "activated")
    private Boolean activated;

    @ManyToMany
    @JoinTable(
            name = "users_lovstamp_listenTo",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "userId"),
            inverseJoinColumns = @JoinColumn(
                    name = "lovdstamp_id", referencedColumnName = "id"))
    @JsonManagedReference
    private Set<Lovstamp> listenLovstampSet = new HashSet<>(0);

    @OneToMany
    private Set<UserNotification> usersNotifications = new HashSet<>(0);


    private String invitedBy;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "id.user")
    @JsonManagedReference
    private Set<UserRoles> roles = new HashSet<>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "id.likeBy")
    private Set<Like> likeBySet = new HashSet<>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "id.likeTo")
    private Set<Like> likeToSet = new HashSet<>(0);

    @Column(name = "my_likes_count", columnDefinition = "BIGINT(20) default 0")
    private Long myLikesCount;

    @Column(name = "like_me_count", columnDefinition = "BIGINT(20) default 0")
    private Long likeMeCount;

    @Column(name = "matches_count", columnDefinition = "BIGINT(20) default 0")
    private Long matchesCount;

    @OneToMany(mappedBy = "userId", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<EventUserPicture> eventPictures = new ArrayList<>();

    @OneToMany(mappedBy = "claimer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<DatingPlaceClaim> placeClaims = new ArrayList<>();

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<DatingPlace> ownedPlaces = new ArrayList<>();

    public User() {
        this.activated = true;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public User(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }


    public Set<UserRoles> getRoles() {
        return roles;
    }

    public void setRoles(Set<UserRoles> roles) {
        this.roles = roles;
    }

    public Boolean getEmailNotifications() {
        return emailNotifications;
    }

    public void setEmailNotifications(Boolean emailNotifications) {
        this.emailNotifications = emailNotifications;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public UserPreference getUserPreference() {
        return userPreference;
    }

    public void setUserPreference(UserPreference userPreference) {
        this.userPreference = userPreference;
    }

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getEmailVerificationKey() {
        return emailVerificationKey;
    }

    public void setEmailVerificationKey(String emailVerificationKey) {
        this.emailVerificationKey = emailVerificationKey;
    }

    public Set<Lovstamp> getListenLovstampSet() {
        return listenLovstampSet;
    }

    public void setListenLovstampSet(Set<Lovstamp> listenLovstampSet) {
        this.listenLovstampSet = listenLovstampSet;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "id.user")
    public Set<UserNotification> getUsersNotifications() {
        return usersNotifications;
    }

    public void setUsersNotifications(Set<UserNotification> usersNotifications) {
        this.usersNotifications = usersNotifications;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getInvitedBy() {
        return invitedBy;
    }

    public void setInvitedBy(String invitedBy) {
        this.invitedBy = invitedBy;
    }

    public Set<Like> getLikeBySet() {
        return likeBySet;
    }

    public void setLikeBySet(Set<Like> likeBySet) {
        this.likeBySet = likeBySet;
    }

    public Set<Like> getLikeToSet() {
        return likeToSet;
    }

    public void setLikeToSet(Set<Like> likeToSet) {
        this.likeToSet = likeToSet;
    }

    public Long getMyLikesCount() {
        return myLikesCount;
    }

    public void setMyLikesCount(Long myLikesCount) {
        this.myLikesCount = myLikesCount;
    }

    public Long getLikeMeCount() {
        return likeMeCount;
    }

    public void setLikeMeCount(Long likeMeCount) {
        this.likeMeCount = likeMeCount;
    }

    public Long getMatchesCount() {
        return matchesCount;
    }

    public void setMatchesCount(Long matchesCount) {
        this.matchesCount = matchesCount;
    }

    public Long getLoginsCount() {
        return loginsCount;
    }


    public void setLoginsCount(Long loginsCount) {
        this.loginsCount = loginsCount;
    }

    public Boolean getBanned() {
        return banned;
    }

    public void setBanned(Boolean banned) {
        this.banned = banned;
    }

    public Boolean getPaused() {
        return paused;
    }

    public void setPaused(Boolean paused) {
        this.paused = paused;
    }

    public Boolean getTweeted() {
        return isTweeted;
    }

    public void setTweeted(Boolean tweeted) {
        isTweeted = tweeted;
    }

    public List<EventUserPicture> getEventPictures() {
        return eventPictures;
    }

    public void setEventPictures(List<EventUserPicture> eventPictures) {
        this.eventPictures = eventPictures;
    }

    public List<DatingPlaceClaim> getPlaceClaims() {
        return placeClaims;
    }

    public void setPlaceClaims(List<DatingPlaceClaim> placeClaims) {
        this.placeClaims = placeClaims;
    }

    public List<DatingPlace> getOwnedPlaces() {
        return ownedPlaces;
    }

    public void setOwnedPlaces(List<DatingPlace> ownedPlaces) {
        this.ownedPlaces = ownedPlaces;
    }

    public Boolean getMobileVerified() {
        return mobileVerified;
    }

    public void setMobileVerified(Boolean mobileVerified) {
        this.mobileVerified = mobileVerified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (enabled != user.enabled) return false;
        if (!userId.equals(user.userId)) return false;
        if (!email.equals(user.email)) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (title != null ? !title.equals(user.title) : user.title != null) return false;
        if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) return false;
        if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(user.phoneNumber) : user.phoneNumber != null) return false;
        if (loginsCount != null ? !loginsCount.equals(user.loginsCount) : user.loginsCount != null) return false;
        if (banned != null ? !banned.equals(user.banned) : user.banned != null) return false;
        if (paused != null ? !paused.equals(user.paused) : user.paused != null) return false;
        if (createdOn != null ? !createdOn.equals(user.createdOn) : user.createdOn != null) return false;
        if (modifiedOn != null ? !modifiedOn.equals(user.modifiedOn) : user.modifiedOn != null) return false;
        if (emailNotifications != null ? !emailNotifications.equals(user.emailNotifications) : user.emailNotifications != null)
            return false;
        if (emailVerified != null ? !emailVerified.equals(user.emailVerified) : user.emailVerified != null)
            return false;
        if (mobileVerified != null ? !mobileVerified.equals(user.mobileVerified) : user.mobileVerified != null)
            return false;
        if (emailVerificationKey != null ? !emailVerificationKey.equals(user.emailVerificationKey) : user.emailVerificationKey != null)
            return false;
        if (isTweeted != null ? !isTweeted.equals(user.isTweeted) : user.isTweeted != null) return false;
        if (lastLogin != null ? !lastLogin.equals(user.lastLogin) : user.lastLogin != null) return false;
        if (myLikesCount != null ? !myLikesCount.equals(user.myLikesCount) : user.myLikesCount != null) return false;
        if (likeMeCount != null ? !likeMeCount.equals(user.likeMeCount) : user.likeMeCount != null) return false;
        return matchesCount != null ? matchesCount.equals(user.matchesCount) : user.matchesCount == null;
    }

    @Override
    public int hashCode() {
        int result = (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (loginsCount != null ? loginsCount.hashCode() : 0);
        result = 31 * result + (banned != null ? banned.hashCode() : 0);
        result = 31 * result + (paused != null ? paused.hashCode() : 0);
        result = 31 * result + (createdOn != null ? createdOn.hashCode() : 0);
        result = 31 * result + (modifiedOn != null ? modifiedOn.hashCode() : 0);
        result = 31 * result + (enabled ? 1 : 0);
        result = 31 * result + (emailNotifications != null ? emailNotifications.hashCode() : 0);
        result = 31 * result + (emailVerified != null ? emailVerified.hashCode() : 0);
        result = 31 * result + (mobileVerified != null ? mobileVerified.hashCode() : 0);
        result = 31 * result + (emailVerificationKey != null ? emailVerificationKey.hashCode() : 0);
        result = 31 * result + (isTweeted != null ? isTweeted.hashCode() : 0);
        result = 31 * result + (lastLogin != null ? lastLogin.hashCode() : 0);
        result = 31 * result + (myLikesCount != null ? myLikesCount.hashCode() : 0);
        result = 31 * result + (likeMeCount != null ? likeMeCount.hashCode() : 0);
        result = 31 * result + (matchesCount != null ? matchesCount.hashCode() : 0);
        return result;
    }
}
