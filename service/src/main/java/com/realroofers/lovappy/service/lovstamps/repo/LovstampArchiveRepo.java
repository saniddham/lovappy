package com.realroofers.lovappy.service.lovstamps.repo;

import com.realroofers.lovappy.service.lovstamps.model.Lovstamp;
import com.realroofers.lovappy.service.lovstamps.model.LovstampArchive;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

/**
 * Created by Daoud Shaheen on 12/31/2017.
 */
public interface LovstampArchiveRepo extends JpaRepository<LovstampArchive, Integer> {


}
