package com.realroofers.lovappy.service.ads.model;

import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name="ad_background_color")
@EqualsAndHashCode
public class AdBackgroundColor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String color;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
