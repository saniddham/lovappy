package com.realroofers.lovappy.service.music.repo;

import com.realroofers.lovappy.service.music.model.MusicExchange;
import com.realroofers.lovappy.service.music.model.MusicResponse;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MusicResponseRepo extends JpaRepository<MusicResponse, Integer> {

    Page<MusicResponse> findByMusicExchange_FromUser(User user, Pageable pageable);

    List<MusicResponse> findByMusicExchange_FromUserAndSeenIsFalse(User user);

    MusicResponse findTopByMusicExchange_FromUserAndSeenIsFalse(User user);

    MusicResponse findByMusicExchange(MusicExchange musicExchange);

}
