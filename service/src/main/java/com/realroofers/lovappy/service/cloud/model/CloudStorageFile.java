package com.realroofers.lovappy.service.cloud.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="cloud_storage_file")
@EqualsAndHashCode
public class CloudStorageFile extends BaseEntity<Long> {

    private String name;
    private String url;
    private String bucket;

    @OneToMany(mappedBy = "originalFile", cascade = { CascadeType.ALL })
    private List<ImageThumbnail> thumbnails;
    
    @Column(name = "approved", columnDefinition = "BIT(1) default 0")
    private Boolean approved;
    @Column(name = "rejected", columnDefinition = "BIT(1) default 0")
    private Boolean rejected;
    @CreationTimestamp
    @Column(name = "approved_date", nullable = true, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date approvedDate;
    @CreationTimestamp
    @Column(name = "rejected_date", nullable = true, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date rejectedDate;
    
    @ManyToOne
    @JoinColumn(name = "approved_by")
    private User approvedBy;
    @ManyToOne
    @JoinColumn(name = "rejected_by")
    private User rejectedBy;

    public CloudStorageFile() {

    }

    public CloudStorageFile(Long id) {
        setId(id);
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public List<ImageThumbnail> getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(List<ImageThumbnail> thumbnails) {
        this.thumbnails = thumbnails;
    }

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public Boolean getRejected() {
		return rejected;
	}

	public void setRejected(Boolean rejected) {
		this.rejected = rejected;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public Date getRejectedDate() {
		return rejectedDate;
	}

	public void setRejectedDate(Date rejectedDate) {
		this.rejectedDate = rejectedDate;
	}

	public User getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(User approvedBy) {
		this.approvedBy = approvedBy;
	}

	public User getRejectedBy() {
		return rejectedBy;
	}

	public void setRejectedBy(User rejectedBy) {
		this.rejectedBy = rejectedBy;
	}

    @Override
    public void onPrePersist() {
        super.onPrePersist();
        setApproved(false);
        setApproved(false);
    }

	
}
