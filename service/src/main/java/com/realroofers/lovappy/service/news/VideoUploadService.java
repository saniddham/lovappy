package com.realroofers.lovappy.service.news;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface VideoUploadService {
    public void UploadVideo(MultipartFile file, String fileName);

    public Resource getVideo(String fileName);

    public void deleteVideo(String fileName) throws IOException;

    public String uploadYoutubeVideo(String url);

   // public Boolean UploadVideoByUrl(String Url,String fileName);
}