package com.realroofers.lovappy.service.vendor.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.vendor.model.VendorType;
import com.realroofers.lovappy.service.vendor.model.VerificationType;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by Manoj on 16/02/2018.
 */
@Data
@EqualsAndHashCode
public class VendorRegistrationDto {

    private VendorType vendorType= VendorType.VENDOR;
    private VerificationType verificationType =VerificationType.SMS;
    private String companyName;
    private String primaryContactPerson;
    private String primaryContactNumber;
    private String primaryContactEmail;
    private String alternateContactPerson;
    private String alternateContactEmail;
    private String password;
    private String confirmPassword;

    private CloudStorageFileDto photoIdentificationCloudFile;

}
