package com.realroofers.lovappy.service.user.repo;

import com.realroofers.lovappy.service.user.dto.UserFlagDto;
import com.realroofers.lovappy.service.user.model.UserFlag;
import com.realroofers.lovappy.service.user.model.UserFlagPK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;


/**
 * Created by Daoud Shaheen on 8/2/2017.
 */
public interface UserFlagRepository extends JpaRepository<UserFlag, UserFlagPK>, QueryDslPredicateExecutor<UserFlag> {

    UserFlag findById(UserFlagPK id);

        @Query("SELECT NEW com.realroofers.lovappy.service.user.dto.UserFlagDto(" +
                "u.id.flagTo.userId, u.id.flagTo.email, u.id.flagBy.email, u.offensive, u.discriminatory, u.uncivil, u.harrassment, u.notRelevant, u.others, u.created ) FROM UserFlag u ")
    Page<UserFlagDto> findAllComplients(Pageable pageable);

    @Query("SELECT NEW com.realroofers.lovappy.service.user.dto.UserFlagDto(" +
            "u.id.flagTo.userId, u.id.flagTo.email, u.id.flagBy.email, u.offensive, u.discriminatory, u.uncivil, u.harrassment, u.notRelevant, u.others, u.created ) FROM UserFlag u " +
            " WHERE u.id.flagBy.email like %:email% or u.id.flagTo.email like %:email% ")
    Page<UserFlagDto> findAllComplientsByEmail(@Param("email") String email, Pageable pageable);
}
