package com.realroofers.lovappy.service.product;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.product.dto.BrandCommissionDto;
import com.realroofers.lovappy.service.product.model.BrandCommission;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BrandCommissionService extends AbstractService<BrandCommissionDto, Integer> {

    Page<BrandCommission> findAll(Pageable pageable);

    BrandCommissionDto getCurrentCommission();
}
