package com.realroofers.lovappy.service.music.repo;

import com.realroofers.lovappy.service.music.model.MusicResponseTemplate;
import com.realroofers.lovappy.service.music.model.MusicResponseType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MusicResponseTemplateRepo extends JpaRepository<MusicResponseTemplate, Integer> {

    MusicResponseTemplate findTopByType(MusicResponseType type);
}
