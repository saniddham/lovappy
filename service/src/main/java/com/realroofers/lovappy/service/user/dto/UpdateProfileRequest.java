package com.realroofers.lovappy.service.user.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.realroofers.lovappy.service.user.dto.UserLifeStylesDto;
import com.realroofers.lovappy.service.user.dto.UserPersonalityDto;
import com.realroofers.lovappy.service.user.dto.UserPreferenceDto;
import com.realroofers.lovappy.service.user.dto.UserStatusesDto;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.PrefHeight;
import com.realroofers.lovappy.service.user.support.Size;
import lombok.Data;

import java.util.Set;

/**
 * Created by Daoud Shaheen on 6/18/2018.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateProfileRequest {
    private Integer minAge;
    private Integer maxAge;
    private Gender prefGender;
    private PrefHeight prefHeight;
    private Set<String> seekingLanguages;
    private Set<String> speakinglanguages;
}
