package com.realroofers.lovappy.service.util;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.realroofers.lovappy.service.event.model.embeddable.EventLocation;
import com.realroofers.lovappy.service.user.model.embeddable.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by Tejaswi Venupalli on 9/07/17
 */
@Component
public class EventLocationUtil {

    private static Logger LOG = LoggerFactory.getLogger(EventLocationUtil.class);
    @Value("${google.maps.api.key}")
    private String googleApiKey;

    public EventLocation extractAddress(LatLng location){
        EventLocation eventLocation = new EventLocation();
        GeoApiContext context = new GeoApiContext().setApiKey(googleApiKey);
        try {
            GeocodingResult[] results =  GeocodingApi.reverseGeocode(context, location).await();
            eventLocation = processResult(results);
        } catch (ApiException | InterruptedException | IOException e) {
            LOG.error(e.getMessage());
        }

        return eventLocation;
    }



    private EventLocation processResult(GeocodingResult[] results){
        EventLocation eventLocation = new EventLocation();
        if(results != null && results.length > 0) {
            GeocodingResult result = results[0];

            eventLocation.setLongitude(result.geometry.location.lng);
            eventLocation.setLatitude(result.geometry.location.lat);
            eventLocation.setFullAddress(result.formattedAddress);

            for (AddressComponent addressComponent : result.addressComponents) {
                //AddressComponentType componentType = addressComponent.types[0];
                for( int i =0; i< addressComponent.types.length; i++) {
                    AddressComponentType componentType = addressComponent.types[i];

                    if (componentType.equals(AddressComponentType.POSTAL_CODE)) {//ZipCode
                        eventLocation.setZipCode(addressComponent.longName);
                    } else if (componentType.equals(AddressComponentType.LOCALITY)) {//City
                        eventLocation.setCity(addressComponent.longName);
                    } else if (componentType.equals(AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1)) {//State
                        eventLocation.setState(addressComponent.longName);
                        eventLocation.setStateShort(addressComponent.shortName);
                    } else if (componentType.equals(AddressComponentType.COUNTRY)) {//Country
                        eventLocation.setCountry(addressComponent.longName);
                    } else if (componentType.equals(AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_2)) {//Province
                        eventLocation.setProvince(addressComponent.longName);
                    } else if (componentType.equals(AddressComponentType.STREET_NUMBER)) { //Steet number
                        eventLocation.setStreetNumber(addressComponent.longName);
                    } else if (componentType.equals(AddressComponentType.PREMISE)) { //premise
                        eventLocation.setPremise(addressComponent.longName);
                    } else if (componentType.equals(AddressComponentType.ROUTE)) {// route
                        eventLocation.setRoute(addressComponent.longName);
                    }
                }
            }
        }
        return eventLocation;
    }
}
