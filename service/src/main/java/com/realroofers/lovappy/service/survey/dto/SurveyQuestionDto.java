package com.realroofers.lovappy.service.survey.dto;

import com.realroofers.lovappy.service.survey.model.SurveyAnswer;
import com.realroofers.lovappy.service.survey.model.SurveyQuestion;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode
public class SurveyQuestionDto {

    private Integer id;
    private String title;
    private List<SurveyAnswerDto> answers;

    public SurveyQuestionDto() {
    }

    public SurveyQuestionDto(SurveyQuestion question) {
        this.id = question.getId();
        this.title = question.getTitle();

        if (question.getAnswers() != null){
            answers = new ArrayList<>();
            for (SurveyAnswer answer : question.getAnswers()){
                answers.add(new SurveyAnswerDto(answer));
            }
        }

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<SurveyAnswerDto> getAnswers() {
        return answers;
    }

    public void setAnswers(List<SurveyAnswerDto> answers) {
        this.answers = answers;
    }
}
