package com.realroofers.lovappy.service.cms.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Daoud Shaheen on 6/6/2017.
 */
@Entity
@Table(name="image_widget")
@EqualsAndHashCode
public class ImageWidget extends Widget {
    private String url;
    private String type;
    @ManyToMany(mappedBy = "images")
    @JsonBackReference
    private Set<Page> pages = new HashSet<>(0);

    public ImageWidget() {
    }

    public ImageWidget(String name, String url, String type) {
        super(name);
        this.type = type;
        this.url = url;
    }

    public Set<Page> getPages() {
        return pages;
    }

    public void setPages(Set<Page> pages) {
        this.pages = pages;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ImageWidget that = (ImageWidget) o;

        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        return type != null ? type.equals(that.type) : that.type == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ImageWidget{" +
                "url='" + url + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
