package com.realroofers.lovappy.service.datingPlaces.model;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by Darrel Rayen on 10/6/18.
 */
@Embeddable
public class DatingPlacePersonTypeId implements Serializable {

    private Integer datingPlaceId;
    private Integer personTypeId;

    public DatingPlacePersonTypeId(Integer datingPlaceId, Integer personTypeId) {
        this.datingPlaceId = datingPlaceId;
        this.personTypeId = personTypeId;
    }

    public DatingPlacePersonTypeId() {
    }

    public Integer getDatingPlaceId() {
        return datingPlaceId;
    }

    public void setDatingPlaceId(Integer datingPlaceId) {
        this.datingPlaceId = datingPlaceId;
    }

    public Integer getPersonTypeId() {
        return personTypeId;
    }

    public void setPersonTypeId(Integer personTypeId) {
        this.personTypeId = personTypeId;
    }
}
