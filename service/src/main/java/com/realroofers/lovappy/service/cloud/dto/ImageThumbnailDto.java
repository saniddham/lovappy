package com.realroofers.lovappy.service.cloud.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.realroofers.lovappy.service.cloud.model.ImageThumbnail;
import com.realroofers.lovappy.service.cloud.support.ImageSize;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author Eias Altawil
 */
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@EqualsAndHashCode
public class ImageThumbnailDto implements Serializable{
    private Integer id;
    private CloudStorageFileDto file;
    private ImageSize size;

    public ImageThumbnailDto() {
    }

    public ImageThumbnailDto(Integer id, CloudStorageFileDto file, ImageSize size) {
        this.id = id;
        this.file = file;
        this.size = size;
    }

    public ImageThumbnailDto(ImageThumbnail thumbnail){
        this.id = thumbnail.getId();
        this.file = new CloudStorageFileDto(thumbnail.getFile(), true);
        this.size = thumbnail.getSize();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CloudStorageFileDto getFile() {
        return file;
    }

    public void setFile(CloudStorageFileDto file) {
        this.file = file;
    }

    public ImageSize getSize() {
        return size;
    }

    public void setSize(ImageSize size) {
        this.size = size;
    }
}
