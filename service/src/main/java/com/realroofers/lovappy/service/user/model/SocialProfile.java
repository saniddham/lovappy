package com.realroofers.lovappy.service.user.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.core.SocialType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * Created by Daoud Shaheen on 3/3/2018.
 */
@Entity
@Table(name = "user_social_profile" , uniqueConstraints = {@UniqueConstraint(columnNames = {"socialId", "socialPlatform"})})
@DynamicUpdate
@Data
@EqualsAndHashCode
public class SocialProfile extends BaseEntity<Integer> {
    private String socialId;
    @Enumerated(EnumType.STRING)
    private SocialType socialPlatform;
    private String firstName;
    private String lastName;
    private String name;
    private String imageUrl;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JsonManagedReference
    @JoinColumn(name = "user_id")
    private User user;
}
