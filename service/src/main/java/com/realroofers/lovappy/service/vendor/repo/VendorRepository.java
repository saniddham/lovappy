package com.realroofers.lovappy.service.vendor.repo;

import com.realroofers.lovappy.service.vendor.model.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Manoj on 18/02/2018.
 */
public interface VendorRepository extends JpaRepository<Vendor, Integer> {

    Vendor findByUser_UserId(Integer userId);
}
