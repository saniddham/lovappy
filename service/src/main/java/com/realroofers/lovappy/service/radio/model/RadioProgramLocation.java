package com.realroofers.lovappy.service.radio.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Manoj on 17/12/2017.
 */

@Data
@Entity
@Table(name = "radio_control_has_location")
@EqualsAndHashCode
public class RadioProgramLocation extends BaseEntity<Integer> implements Serializable {

    @ManyToOne
    @JoinColumn(name = "fk_radio_program_controls")
    private RadioProgramControl radioProgramControl;

    @Column(name = "location")
    private String location;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;


}
