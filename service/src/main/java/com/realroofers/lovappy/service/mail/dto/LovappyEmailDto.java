package com.realroofers.lovappy.service.mail.dto;

import com.realroofers.lovappy.service.mail.model.LovappyEmail;
import lombok.EqualsAndHashCode;

/**
 * Created by Daoud Shaheen on 9/7/2017.
 */
@EqualsAndHashCode
public class LovappyEmailDto {

    private Integer id;
    private String email;

    public LovappyEmailDto() {
    }
    public LovappyEmailDto(LovappyEmail lovappyEmail) {

        this.id = lovappyEmail.getId();
        this.email = lovappyEmail.getEmail();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
