package com.realroofers.lovappy.service.vendor.repo;

import com.realroofers.lovappy.service.vendor.model.VendorProductType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendorProductTypeRepo extends JpaRepository<VendorProductType, Integer> {
}
