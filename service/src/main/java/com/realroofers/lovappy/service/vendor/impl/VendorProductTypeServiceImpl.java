package com.realroofers.lovappy.service.vendor.impl;

import com.realroofers.lovappy.service.vendor.VendorProductTypeService;
import com.realroofers.lovappy.service.vendor.dto.VendorProductTypeDto;
import com.realroofers.lovappy.service.vendor.model.VendorProductType;
import com.realroofers.lovappy.service.vendor.repo.VendorProductTypeRepo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class VendorProductTypeServiceImpl implements VendorProductTypeService {

    private final VendorProductTypeRepo vendorProductTypeRepo;

    public VendorProductTypeServiceImpl(VendorProductTypeRepo vendorProductTypeRepo) {
        this.vendorProductTypeRepo = vendorProductTypeRepo;
    }

    @Transactional(readOnly = true)
    @Override
    public List<VendorProductTypeDto> findAll() {
        List<VendorProductTypeDto> list = new ArrayList<>();
        vendorProductTypeRepo.findAll().stream().forEach(vendorProductType -> list.add(new VendorProductTypeDto(vendorProductType)));
        return list;
    }

    @Transactional(readOnly = true)
    @Override
    public List<VendorProductTypeDto> findAllActive() {
        List<VendorProductTypeDto> list = new ArrayList<>();
        vendorProductTypeRepo.findAll().stream().forEach(vendorProductType -> list.add(new VendorProductTypeDto(vendorProductType)));
        return list;
    }

    @Transactional
    @Override
    public VendorProductTypeDto create(VendorProductTypeDto vendorProductTypeDto) throws Exception {
        return new VendorProductTypeDto(vendorProductTypeRepo.saveAndFlush(new VendorProductType(vendorProductTypeDto)));
    }

    @Transactional
    @Override
    public VendorProductTypeDto update(VendorProductTypeDto vendorProductTypeDto) throws Exception {
        return new VendorProductTypeDto(vendorProductTypeRepo.saveAndFlush(new VendorProductType(vendorProductTypeDto)));
    }

    @Override
    public void delete(Integer integer) throws Exception {

    }

    @Transactional(readOnly = true)
    @Override
    public VendorProductTypeDto findById(Integer integer) {
        VendorProductType vendorProductType = vendorProductTypeRepo.findOne(integer);
        return vendorProductType != null ? new VendorProductTypeDto(vendorProductType) : null;
    }
}
