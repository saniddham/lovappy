package com.realroofers.lovappy.service.product.repo;

import com.realroofers.lovappy.service.product.dto.ProductDto;
import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Manoj on 31/01/2018.
 */
public interface ProductRepo extends JpaRepository<Product, Integer> {

    List<ProductDto> findAllByActiveIsTrueAndApprovedIsTrueAndBlockedIsFalseOrderByProductName();

    Page<Product> findAllByActiveIsTrueAndApprovedIsTrueAndBlockedIsFalseOrderByProductName(Pageable pageable);


    List<ProductDto> findAllByActiveIsTrueOrderByProductName();

    List<ProductDto> findAllByProductCode(String productCode);

    @Query("Select new com.realroofers.lovappy.service.product.dto.ProductDto(p) from Product p where p.productName LIKE  %?1% and p.active = true and p.blocked=false and p.approved = true ORDER BY p.id DESC")
    Page<ProductDto> findAllByName(String name, Pageable pageable);

    @Query("Select new com.realroofers.lovappy.service.product.dto.ProductDto(p) from Product p where p.productName LIKE  %?1% and p.productType.id = ?2 and p.brand.id = ?3 and p.active = true and p.blocked=false and p.approved = true ORDER BY p.id DESC")
    Page<ProductDto> findAllByNameAndCategoryAndBrand(String name, int category, int brand, Pageable pageable);

    @Query("Select new com.realroofers.lovappy.service.product.dto.ProductDto(p) from Product p where p.productName LIKE  %?1% and p.productType.id = ?2 and p.active = true and p.blocked=false and p.approved = true ORDER BY p.id DESC")
    Page<ProductDto> findAllByNameAndCategory(String name, int category, Pageable pageable);

    @Query("Select new com.realroofers.lovappy.service.product.dto.ProductDto(p) from Product p where p.productName LIKE  %?1% and p.brand.id = ?2 and p.active = true and p.blocked=false and p.approved = true ORDER BY p.id DESC")
    Page<ProductDto> findAllByNameAndBrand(String name, int brand, Pageable pageable);

    @Query("Select new com.realroofers.lovappy.service.product.dto.ProductDto(p) from Product p where p.productName LIKE  %?1% ORDER BY p.approved,p.id DESC")
    Page<ProductDto> findAllByNameOrderByApproved(String name, Pageable pageable);

    @Query("Select new com.realroofers.lovappy.service.product.dto.ProductDto(p) from Product p where p.productName LIKE  %?1% AND p.createdBy= ?2 ORDER BY p.id DESC")
    Page<ProductDto> findAllByNameAndUser(String name, User user, Pageable pageable);

    @Query("Select new com.realroofers.lovappy.service.product.dto.ProductDto(p) from Product p where (:name is null or p.productName LIKE :name) AND (:code is null or p.productCode LIKE :code) AND p.createdBy= :user")
    Page<ProductDto> findAllByNameAndCodeAndUser(@Param(value = "name") String name,@Param(value = "code") String code,@Param(value = "user")  User user, Pageable pageable);

}
