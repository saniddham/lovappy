package com.realroofers.lovappy.service.radio;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.radio.model.RadioProgramLocation;

/**
 * Created by Manoj on 17/12/2017.
 */
public interface RadioProgramLocationService extends AbstractService<RadioProgramLocation, Integer> {
}
