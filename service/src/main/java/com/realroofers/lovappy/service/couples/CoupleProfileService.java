package com.realroofers.lovappy.service.couples;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.couples.dto.CoupleProfileRequest;
import com.realroofers.lovappy.service.couples.dto.CouplesProfileDto;
import com.realroofers.lovappy.service.user.dto.UserDto;

/**
 * Created by Daoud Shaheen on 7/15/2018.
 */
public interface CoupleProfileService {

    CloudStorageFileDto uploadPhoto(CloudStorageFileDto uploadImage, Integer userId);
    CouplesProfileDto signup(CoupleProfileRequest coupleProfileRequest, Integer userId);
    CouplesProfileDto update(CoupleProfileRequest coupleProfileRequest, Integer userId);
    UserDto registerPartner(Integer userId, String partnerEmail);
    CouplesProfileDto getCoupleProfile(Integer userId);
    UserDto partnerApprove(String  partnerEmail);
}
