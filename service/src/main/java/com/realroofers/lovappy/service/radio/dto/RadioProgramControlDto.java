package com.realroofers.lovappy.service.radio.dto;

import com.realroofers.lovappy.service.radio.support.AudioAction;
import com.realroofers.lovappy.service.radio.support.AudioInsertWhere;
import com.realroofers.lovappy.service.system.model.Language;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Manoj on 17/12/2017.
 */

@Data
@ToString
@EqualsAndHashCode
public class RadioProgramControlDto implements Serializable {

    private Integer mediaType;

    private Integer audioType;

    private Integer audioSubType;

    private AudioAction audioAction;

    private AudioInsertWhere insertWhere;

    private String gender;

    private Boolean ageBetween18And24;

    private Boolean ageBetween25And34;

    private Boolean ageBetween35And49;

    private Boolean ageAbove50;

    private Integer dailyLimit;

    private Integer lifetimeLimit;

    private Integer contentBefore;

    private Integer contentAfter;

    private Boolean playToAll;

    private Boolean playNext;

    private List<Language> languages;

    private List<TimeSlotDto> timeSlots;

    private List<LocationDto> locations;

}
