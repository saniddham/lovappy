package com.realroofers.lovappy.service.career.model;

import com.realroofers.lovappy.service.career.dto.VacancyDto;
import com.realroofers.lovappy.service.core.VacancyStatus;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.StringJoiner;

@Entity
@Table(name ="vacancies")
public class Vacancy {

    @ManyToMany(mappedBy = "vacancies")
    private Set<CareerUser>  applicants = new HashSet<CareerUser>(0);
    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    @Column(name = "vacancy_id")
    private long id;
    @Column(name = "job_title")
    private String jobTitle;
    private String status;
    @Column(name = "open_date", updatable=false)
    private Date openDate;
    @Column(name = "closing_date")
    private Date closingDate;
    private String city;
    private String state;
    private String country;
    private String description;
    private String keywords;
    @Column(name = "seo_title")
    private String seoTitle;
    @Column(name = "position_description")
    private String positionDescription;
    private String skillRequired;
    @Column(name = "skill_required")
    public String getSkillRequired() {
        return skillRequired;
    }
    private String compensation;

    public void setSkillRequired(String skillRequired) {
        this.skillRequired = skillRequired;
    }

    public Vacancy(long vacancyId) {
        this.id = vacancyId;
    }

    public Vacancy(VacancyDto vacancyDto) {
        this.city = vacancyDto.getCity();
        this.state = vacancyDto.getState();
        this.jobTitle = vacancyDto.getJobTitle();
        this.country = vacancyDto.getCountry();
        this.description = vacancyDto.getDescription();
        this.status = vacancyDto.getStatus();
        if (vacancyDto.getStatus() == null){
            this.status = VacancyStatus.OPEN.toString();
        }
        this.keywords = vacancyDto.getKeywords();
        this.seoTitle = vacancyDto.getSeoTitle();
        this.positionDescription = vacancyDto.getPositionDescription();
        this.skillRequired = vacancyDto.getSkillRequired();
        this.compensation = vacancyDto.getCompensation();
    }

    public Vacancy() {
    }

    public String getCompensation() {
        return compensation;
    }

    public void setCompensation(String compensation) {
        this.compensation = compensation;
    }

    public Vacancy(String jobTitle, String status, Date openDate, Date closingDate, String city, String state, String country) {
        this.jobTitle = jobTitle;
        this.status = status;
        this.openDate = openDate;
        this.closingDate = closingDate;
        this.city = city;
        this.state = state;
        this.country = country;
    }

    public Set<CareerUser> getApplicants() {
        return applicants;
    }

    public void setApplicants(Set<CareerUser> applicants) {
        this.applicants = applicants;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public Date getOpenDate() {
        return openDate;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getSeoTitle() {
        return seoTitle;
    }

    public void setSeoTitle(String seoTitle) {
        this.seoTitle = seoTitle;
    }

    public String getPositionDescription() {
        return positionDescription;
    }

    public void setPositionDescription(String positionDescription) {
        this.positionDescription = positionDescription;
    }

    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }

    public Date getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(Date closingDate) {
        this.closingDate = closingDate;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Vacancy.class.getSimpleName() + "[", "]")
                .add("jobTitle='" + jobTitle + "'")
                .add("status='" + status + "'")
                .add("openDate='" + openDate + "'")
                .add("closingDate='" + closingDate + "'")
                .add("city='" + city + "'")
                .add("state='" + state + "'")
                .add("country='" + country + "'")
                .toString();
    }
}
