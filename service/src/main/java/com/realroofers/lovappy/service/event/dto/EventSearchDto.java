package com.realroofers.lovappy.service.event.dto;

import lombok.EqualsAndHashCode;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author Tejaswi Venupalli
 */
@EqualsAndHashCode
public class EventSearchDto {

    @NotNull
    private Double latitude;
    @NotNull
    private Double longitude;
    @NotNull
    @Min(1)
    private Integer radius;

    public EventSearchDto() {
    }

    public EventSearchDto(Double latitude, Double longitude, Integer radius){
        this.longitude = longitude;
        this.latitude = latitude;
        this.radius = radius;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventSearchDto that = (EventSearchDto) o;

        if (!latitude.equals(that.latitude)) return false;
        if (!longitude.equals(that.longitude)) return false;
        return radius.equals(that.radius);
    }

    @Override
    public int hashCode() {
        int result = latitude.hashCode();
        result = 31 * result + longitude.hashCode();
        result = 31 * result + radius.hashCode();
        return result;
    }
}
