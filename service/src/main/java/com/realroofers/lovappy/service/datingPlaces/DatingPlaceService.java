package com.realroofers.lovappy.service.datingPlaces;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.datingPlaces.dto.*;
import com.realroofers.lovappy.service.datingPlaces.dto.mobile.*;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by Darrel Rayen on 10/19/17.
 */
public interface DatingPlaceService {

    DatingPlaceDto addDatingPlace(DatingPlaceDto datingPlaceDto);

    DatingPlaceMobileDto addDatingPlaceMobile(AddDatingPlaceMobileDto datingPlaceMobileDto) throws Exception;

    DatingPlaceDto findDatingPlaceById(Integer placeId);

    DatingPlaceDto findGoogleDatingPlaceByQuery(String query) throws IOException;

    DatingPlaceDto findGoogleDatingPlaceById(String placeId) throws IOException;

    DatingPlaceMobileDto findMobilePlaceById(Integer placeId);

    DatingPlaceMobileDto findMobileGooglePlaceById(String placeId) throws IOException;

    DatingPlaceDto updateDatingPlace(DatingPlaceDto datingPlaceDto);

    Page<DatingPlaceDto> getAllPlaces(Pageable pageable);

    List<DatingPlaceDto> getAllNearByPlaces(Double latitude, Double longitude);

    List<DatingPlaceMobileDto> getAllMobileNearByPlaces(Double latitude, Double longitude) throws IOException;

    Page<DatingPlaceDto> getAllPlacesByRating(Boolean isDescending, Double latitude, Double longitude, Pageable pageable) throws IOException;

    List<DatingPlaceDto> getAllPlacesListByRating(Boolean isDescending, Double latitude, Double longitude) throws IOException;

    List<DatingPlaceDto> getAllNearByPlacesByNameAndLocation(Double latitude, Double longitude, String placeName);

    List<DatingPlaceDto> getAllNearByPlacesByCityAndRadius(Double latitude, Double longitude, Double radius, String cityName);

    Boolean changePlaceApprovalStatus(Integer placeId, ApprovalStatus status);

    Boolean changeDealApprovalStatus(Integer datingDealId, ApprovalStatus status);

    DatingPlaceDealsDto findDatingDealById(Integer datingDealId);

    Boolean addDatingLocationPictures(List<CloudStorageFileDto> datingLocationPictures);

    Page<DatingPlaceDto> findPlacesByNewFilters(Pageable pageable, LocationDto locationDto);

    Page<DatingPlaceDto> findPlacesByAllFilters(Pageable pageable, DatingPlaceFilterDto filterDto, LocationDto locationDto) throws IOException;

    Page<DatingPlaceMobileDto> findPlacesByAllFiltersMobile(Pageable pageable, DatingPlaceFilterDto filterDto, LocationDto locationDto) ;

    List<DatingPlaceMobileDto> findPlacesByAllFiltersMobile(DatingPlaceFilterDto filterDto, LocationDto locationDto) throws IOException;

    Page<DatingPlaceDto> findPlacesByQuickFilters(int page, int limit, DatingPlaceFilterDto filterDto, LocationDto locationDto);

    List<DatingPlaceMobileDto> findPlacesByQuickFilters(DatingPlaceFilterDto filterDto, LocationDto locationDto) throws IOException;

    Page<DatingPlaceMobileDto> findPlacesByFilters(Pageable pageable, DatingPlaceMobileFilterDto filterDto) throws Exception;

    Page<DatingPlaceMobileDto> findPlacesBySearchTerm(Pageable pageable, String searchTerm, Double latitude, Double longitude, Double radius) throws Exception;

    DatingPlaceReviewDto addDatingPlaceReview(DatingPlaceReviewDto datingPlaceReviewDto, DatingPlaceDto datingPlaceDto) throws IOException;

    DatingPlaceReviewMobileDto addDatingPlaceReviewMobile(AddDatingPlaceReviewMobileDto placeReviewMobileDto, DatingPlaceDto datingPlaceDto);

    Page<DatingPlaceReviewDto> getAllReviews(Pageable pageable);

    Page<DatingPlaceReviewDto> getReviewsByDatingPlace(Pageable pageable, Integer placeId);

    Page<DatingPlaceReviewDto> getReviewsByGoogleDatingPlace(Pageable pageable, DatingPlaceDto datingPlaceDto);

    OrderDto addFeaturedDatingPlace(DatingPlaceDto datingPlaceDto, String coupon, PaymentMethodType paymentMethodType);

    Boolean changeReviewApprovalStatus(Integer reviewId, ApprovalStatus status);

    DatingPlaceDto getDatingPlaceByReviewId(Integer reviewId);

    DatingPlaceReviewDto findReviewById(Integer reviewid);

    Double getAverageSecurityRatingByPlace(Integer placeId);

    Double getAverageParkingRatingByPlace(Integer placeId);

    Double getAverageParkingAccessRatingByPlace(Integer reviewId);

    Double getAverageRatingByPlace(Integer placeId);

    Double getOverAllRatingByPlace(Integer placeId);

    boolean deletePlaceReview(Integer placeId);

    List<DatingPlaceDto> getFeaturedPlaces(LocationDto locationDto);

    CalculatePriceDto calculatePrice(DatingPlaceDto datingPlaceDto, String coupon);

    CalculatePriceDto calculatePrice(Integer datingplaceId, String couponCode);

    Integer getGenderCountByPlace(Integer placeId, Gender gender);

    Double calculatePlaceDeal(Integer placeId, Double fullAmount, Double discountAmout);

    Page<DatingPlaceDto> getAllFeaturedPlaces(Pageable pageable);
    Page<DatingPlaceDto> getAllNearByFeaturedPlaces(Pageable pageable, LocationDto locationDto);
    Page<DealsDto> getAllDealTypes(Pageable pageable);

    DealsDto addDeal(DealsDto dealsDto);

    Boolean isDealRangeExisting(Double price);

    DealsDto updateDeal(DealsDto dealsDto);

    DealsDto getdealById(Integer id);

    DealsDto getDealByFullAmount(Double amount);

    Boolean isDealRangeExistsFor(Double price, Integer id);

    DatingPlaceDealsDto addDatingDeal(DatingPlaceDealsDto placeDealsDto);

    DatingPlaceDealsDto updateDatingDeal(DatingPlaceDealsDto placeDealsDto);

    Page<DatingPlaceDealsDto> getActiveDealsByPlaceIdAndDate(Pageable pageable, Integer placeId, Date date);

    Page<DatingPlaceDealsMobileDto> getActiveDealsByPlaceIdAndDateMobile(Pageable pageable, Integer placeId, Date date);

    Integer getActiveDealsCountByPlaceIdAndDate(Integer placeId, Date date);

    Page<DatingPlaceDealsDto> getAllPlaceDeals(Pageable pageable);

    DatingPlaceClaimDto addClaim(DatingPlaceClaimDto claimDto);

    void addOwnerToExistingPlace(Integer placeId, Integer userId);

    Boolean isOwnerVerified(String messageId, String verificationCode);

    DatingPlaceClaimDto updateClaim(DatingPlaceClaimDto claimDto);

    Boolean isAlreadyClaimed(Integer placeId);

    DatingPlaceDto addOwnerToGooglePlace(DatingPlaceClaimDto claimDto, DatingPlaceDto datingGooglePlaceDto) throws IOException;

    DatingPlaceDto completeGoogleClaiming(DatingPlaceDto datingPlaceDto);

    DatingPlaceDto updateDatingPlaceByOwner(DatingPlaceDto datingPlaceDto);

    DatingPlaceDto addImagesToPlace(List<PlacesAttachmentDto> placesAttachmentDtoList);

    DatingPlaceClaimDto findPlaceClaimById(String messageId);

    Boolean featureDatingPlaceFromAdmin(Integer placeId, String featureType);

    DatingPlaceDto findPlaceWithHighestRating();

    void featurePlaceReview(Integer reviewId) throws Exception;

    DatingPlaceReviewDto findFeaturedReview();


}
