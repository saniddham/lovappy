package com.realroofers.lovappy.service.product.dto;

import com.realroofers.lovappy.service.product.model.BrandCommission;
import com.realroofers.lovappy.service.user.dto.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

@Data
@EqualsAndHashCode
public class BrandCommissionDto implements Serializable {

    private Integer id;
    private Double commissionPercentage;
    private Date affectiveDate;
    private String effectiveDate;
    private UserDto createdBy;
    private UserDto modifiedBy;
    private Boolean active = true;
    private Date created;
    private Date updated;

    public BrandCommissionDto() {
    }

    public BrandCommissionDto(BrandCommission brandCommission) {
        this.id = brandCommission.getId();
        this.commissionPercentage = brandCommission.getCommissionPercentage();
        this.affectiveDate = brandCommission.getAffectiveDate();
        this.createdBy = new UserDto(brandCommission.getCreatedBy());
        this.modifiedBy = new UserDto(brandCommission.getModifiedBy());
        this.active = brandCommission.getActive();
        this.created = brandCommission.getCreated();
        this.updated = brandCommission.getUpdated();
    }
}
