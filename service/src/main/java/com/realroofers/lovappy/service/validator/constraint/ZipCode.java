package com.realroofers.lovappy.service.validator.constraint;


import com.realroofers.lovappy.service.validator.ZipCodeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Created by Eias Altawil on 6/8/2017
 */
@Documented
@Constraint(validatedBy = ZipCodeValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ZipCode {

    String message() default "Zip Code is not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
