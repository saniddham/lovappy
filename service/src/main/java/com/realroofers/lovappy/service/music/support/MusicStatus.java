package com.realroofers.lovappy.service.music.support;

public enum MusicStatus {
    PENDING("PENDING"), APPROVED("APPROVED"), REJECTED("REJECTED");
    String text;

    MusicStatus(String text) {
        this.text = text;
    }
}
