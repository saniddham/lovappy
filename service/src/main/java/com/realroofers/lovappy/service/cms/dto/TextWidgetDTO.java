package com.realroofers.lovappy.service.cms.dto;

import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 6/10/2017.
 */
@EqualsAndHashCode
public class TextWidgetDTO implements Serializable {
    private Integer id;
    private String content;
    private String language;
    private String tag;

    public TextWidgetDTO(Integer id, String content, String language, String tag) {
        this.id = id;
        this.content = content;
        this.language = language;
        this.tag = tag;
    }
    public TextWidgetDTO() {
    }
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
