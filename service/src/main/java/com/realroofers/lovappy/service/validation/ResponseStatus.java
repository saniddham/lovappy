package com.realroofers.lovappy.service.validation;

/**
 * Created by Eias Altawil on 5/26/2017
 */
public enum ResponseStatus {
    FAIL(0), SUCCESS(1);
    Integer code;

    ResponseStatus(Integer code) {
        this.code = code;
    }
}
