package com.realroofers.lovappy.service.survey.dto;

import com.realroofers.lovappy.service.survey.model.SurveyQuestionAnswer;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class SurveyQuestionAnswerDto {

    private Integer id;
    private SurveyQuestionDto question;
    private SurveyAnswerDto answer;

    public SurveyQuestionAnswerDto(SurveyQuestionAnswer surveyQuestionAnswer) {
        this.id = surveyQuestionAnswer.getId();
        this.question = new SurveyQuestionDto(surveyQuestionAnswer.getQuestion());
        this.answer = new SurveyAnswerDto(surveyQuestionAnswer.getAnswer());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SurveyQuestionDto getQuestion() {
        return question;
    }

    public void setQuestion(SurveyQuestionDto question) {
        this.question = question;
    }

    public SurveyAnswerDto getAnswer() {
        return answer;
    }

    public void setAnswer(SurveyAnswerDto answer) {
        this.answer = answer;
    }
}
