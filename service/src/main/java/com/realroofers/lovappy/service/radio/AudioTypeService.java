package com.realroofers.lovappy.service.radio;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.radio.model.AudioType;

/**
 * Created by Manoj on 17/12/2017.
 */
public interface AudioTypeService extends AbstractService<AudioType, Integer> {
    AudioType findByName(String name);
}
