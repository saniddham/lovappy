package com.realroofers.lovappy.service.order.dto;

import com.realroofers.lovappy.service.order.support.ProductType;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by daoud on 11/17/2018.
 */
@Data
public class ChargeRequestDetails implements Serializable {
    private Integer quantity;
    private ProductType productType;
    private Long productId;
}
