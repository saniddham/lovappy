package com.realroofers.lovappy.service.system.impl;

import com.realroofers.lovappy.service.system.LanguageService;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.system.repo.LanguageRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Eias Altawil on 6/3/2017
 */

@Service
public class LanguageServiceImpl implements LanguageService {

    private final LanguageRepo languageRepo;

    @Autowired
    public LanguageServiceImpl(LanguageRepo languageRepo) {
        this.languageRepo = languageRepo;
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<LanguageDto> getAllLanguages(Boolean enabled) {
        Collection<LanguageDto> languages = new ArrayList<>();
        Iterable<Language> all =
                enabled == null
                ? languageRepo.findAllByOrderByNameAsc()
                : languageRepo.findByEnabledOrderByNameAsc(enabled);

        if (all != null) {
            for (Language language : all) {
                languages.add(new LanguageDto(language));
            }
        }

        return languages;
    }

    @Transactional(readOnly = true)
    @Override
    public Collection<LanguageDto> getAllLanguages() {
        return getAllLanguages(null);
    }

    @Override
    @Transactional(readOnly = true)
    public LanguageDto getLanguage(Integer id) {
        Language language = languageRepo.findOne(id);
        return new LanguageDto(language);
    }

    @Transactional(readOnly = true)
    @Override
    public LanguageDto getLanguageByCode(String code) {
        return new LanguageDto(languageRepo.findByAbbreviation(code));
    }

    @Override
    @Transactional
    public LanguageDto addLanguage(LanguageDto languageDto) {
        Language language = new Language();
        language.setName(languageDto.getName());
        language.setAbbreviation(languageDto.getAbbreviation());
        Language save = languageRepo.save(language);

        return new LanguageDto(save);
    }

    @Override
    @Transactional
    public LanguageDto updateLanguage(LanguageDto languageDto) {
        Language language = languageRepo.findOne(languageDto.getId());
        language.setName(languageDto.getName());
        language.setAbbreviation(languageDto.getAbbreviation());
        language.setEnabled(languageDto.getEnabled());

        Language save = languageRepo.save(language);

        return new LanguageDto(save);
    }

    @Override
    @Transactional
    public LanguageDto changeStatus(Integer languageID, Boolean enabled) {
        Language language = languageRepo.findOne(languageID);
        language.setEnabled(enabled);

        Language save = languageRepo.save(language);

        return new LanguageDto(save);
    }


}
