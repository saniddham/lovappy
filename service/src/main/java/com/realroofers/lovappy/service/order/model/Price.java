package com.realroofers.lovappy.service.order.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Daoud Shaheen on 6/1/2018.
 */
@Entity
@Data
@Table(name = "prices")
@EqualsAndHashCode
public class Price extends BaseEntity<Integer> {
    private Double price;
    @Column(unique = true)
    private OrderDetailType type;

}
