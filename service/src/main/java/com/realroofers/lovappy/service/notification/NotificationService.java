package com.realroofers.lovappy.service.notification;

import com.realroofers.lovappy.service.notification.dto.NotificationDto;
import com.realroofers.lovappy.service.notification.model.Notification;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Created by Daoud Shaheen on 6/24/2017.
 */
public interface NotificationService {

    void sendNotificationToAdmins(NotificationDto notificationDto);

    Page<NotificationDto> getPageOfNotificaionByUser(User user, int page, int limit);

    Page<NotificationDto> getPageOfNotificaionByUserAndType(User user, NotificationTypes type, int page, int limit);

    Integer countNotSeenNotificationsByUser(User user);

    Integer countNotSeenNotificationsByUserAndType(User user, NotificationTypes type);

    Integer countNotSeenNotificationsByUserExceptRegistrationAndBlog(User user);

    void setNotificaitonAsSeenByUser(Long notificaitonId, User user);

    void setAllNotificaitonAsSeenByUser(User user);

    Page<NotificationDto> getPageOfNotificaionByUserExceptRegistartionAndBlog(User user, int page, int limit);

    void setAllNotificaitonAsSeenByUserAndType(User user, NotificationTypes type);
    void setAllNotificaitonAsSeenByUserAndTypes(User user, List<NotificationTypes> type);


    NotificationDto getNotificationById(Long id);
}
