package com.realroofers.lovappy.service.upload.dto;

import com.realroofers.lovappy.service.ads.support.AdMediaType;
import com.realroofers.lovappy.service.ads.support.AdType;
import com.realroofers.lovappy.service.ads.support.AudioType;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.music.dto.GenreDto;
import com.realroofers.lovappy.service.upload.model.FileUpload;
import com.realroofers.lovappy.service.user.dto.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by Manoj
 */


@Data
@ToString
@EqualsAndHashCode
public class FileUploadDto implements Serializable {

    private Integer id;
    private String title;
    private String description;
    private AudioType audioType;
    private AdType type;
    private AdMediaType mediaType;
    private GenreDto genre;
    private String lyrics;
    private PodcastCategoryDto podcastCategory;
    private ComedyCategoryDto comedyCategory;
    private CloudStorageFileDto imageFile;
    private CloudStorageFileDto file;
    private Boolean approved;
    private Boolean active;
    private Boolean rejected;
    private Boolean userAgreement;
    private Boolean isAdvertisement;
    private UserDto createdBy;
    private UserDto approvedBy;
    private UserDto rejectedBy;
    private Double price;

    public FileUploadDto() {
    }

    public FileUploadDto(FileUpload fileUpload) {
        if (fileUpload.getId() != null)
            this.id = fileUpload.getId();
        this.title = fileUpload.getTitle();
        this.description = fileUpload.getDescription();
        this.audioType = fileUpload.getAudioType();
        this.type = fileUpload.getType();
        this.mediaType = fileUpload.getMediaType();
        this.genre = fileUpload.getGenre() != null ? new GenreDto(fileUpload.getGenre()) : null;
        this.lyrics = fileUpload.getLyrics();
        this.podcastCategory = fileUpload.getPodcastCategory() != null ? new PodcastCategoryDto(fileUpload.getPodcastCategory()) : null;
        this.comedyCategory = fileUpload.getComedyCategory() != null ? new ComedyCategoryDto(fileUpload.getComedyCategory()) : null;
        this.imageFile = fileUpload.getImageFile() != null ? new CloudStorageFileDto(fileUpload.getImageFile()) : null;
        this.file = fileUpload.getFile() != null ? new CloudStorageFileDto(fileUpload.getFile()) : null;
        this.approved = fileUpload.getApproved();
        this.active = fileUpload.getActive();
        this.rejected = fileUpload.getRejected();
        this.userAgreement = fileUpload.getUserAgreement();
        this.isAdvertisement = fileUpload.getIsAdvertisement();
        this.createdBy = fileUpload.getCreatedBy() != null ? new UserDto(fileUpload.getCreatedBy().getUserId()) : null;
        this.approvedBy = fileUpload.getApprovedBy() != null ? new UserDto(fileUpload.getApprovedBy().getUserId()) : null;
        this.rejectedBy = fileUpload.getRejectedBy() != null ? new UserDto(fileUpload.getRejectedBy().getUserId()) : null;
        this.price = fileUpload.getPrice();
    }
}
