package com.realroofers.lovappy.service.gallery.impl;

import java.util.ArrayList;
import java.util.Collection;

import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.realroofers.lovappy.service.gallery.GalleryStorageService;
import com.realroofers.lovappy.service.gallery.dto.GalleryStorageFileDto;
import com.realroofers.lovappy.service.gallery.model.GalleryStorageFile;
import com.realroofers.lovappy.service.gallery.repo.GalleryStorageRepo;
import com.realroofers.lovappy.service.user.model.embeddable.Address;
import com.realroofers.lovappy.service.util.AddressUtil;

@Service
public class GalleryStorageServiceImpl implements GalleryStorageService {
	private static final Logger LOGGER = LoggerFactory.getLogger(GalleryStorageService.class);

	  private final GalleryStorageRepo galleryStorageRepo;
	  private final AddressUtil addressUtil;

	    @Autowired
	    public GalleryStorageServiceImpl(GalleryStorageRepo galleryStorageRepo, AddressUtil addressUtil) {
	        this.galleryStorageRepo = galleryStorageRepo;
	        this.addressUtil = addressUtil;
	    }


	    @Override
	    @Transactional
	    public GalleryStorageFileDto add(GalleryStorageFileDto galleryStorageFileDto, Address address) {
	        GalleryStorageFile galleryStorageFile = new GalleryStorageFile();
	        galleryStorageFile.setName(galleryStorageFileDto.getName());
	        galleryStorageFile.setUrl(galleryStorageFileDto.getUrl());
	        galleryStorageFile.setBucket(galleryStorageFileDto.getBucket());
	        
	        galleryStorageFile.setPhotoCaption(galleryStorageFileDto.getPhotoCaption());
//	        galleryStorageFile.setZipCode(galleryStorageFileDto.getZipCode());
            galleryStorageFile.setAddress(address);
	        GalleryStorageFile save = galleryStorageRepo.save(galleryStorageFile);

	        return save == null ? null : new GalleryStorageFileDto(save, address);
	    }


	    @Transactional(readOnly = true)
		@Override
		public Collection<GalleryStorageFileDto> getAllPhotos(Long count) {
			 Collection<GalleryStorageFileDto> listFiles = new ArrayList<>();
		        Iterable<GalleryStorageFile> all = galleryStorageRepo.findAll();
		               
		        if (all != null) {
		            for (GalleryStorageFile galleryStorageFile : all) {
		            	Address address = galleryStorageFile.getAddress();
		            	listFiles.add(new GalleryStorageFileDto(galleryStorageFile, address) );
		            }
		        }

		        return listFiles;
		}


//		private Address getAddress(GalleryStorageFile galleryStorageFile) {
//			Address address = addressUtil.extractAddress(galleryStorageFile.getZipCode());
//        	if (address.getCity() == null || address.getCity().equals("null")) address.setCity("");
//        	if (address.getState() == null || address.getState().equals("null")) address.setState("");
//        	if (address.getCountry() == null || address.getCountry().equals("null")) address.setCountry("");
//        	return address;
//		}

	@Transactional(readOnly = true)
		@Override
		public Collection<GalleryStorageFileDto> getAllPhotos() {
			 Collection<GalleryStorageFileDto> listFiles = new ArrayList<>();
		        Iterable<GalleryStorageFile> all = galleryStorageRepo.findAll();
		               
		        if (all != null) {
		            for (GalleryStorageFile galleryStorageFile : all) {
		            	Address address = galleryStorageFile.getAddress();
		            	listFiles.add(new GalleryStorageFileDto(galleryStorageFile, address) );
		            }
		        }

		        return listFiles;
		}
	@Transactional(readOnly = true)
	@Override
	public Page<GalleryStorageFileDto> getAllPhotos(Pageable pageable) {
		return galleryStorageRepo.findAll(pageable).map(GalleryStorageFileDto::new);
	}
	@Transactional(readOnly = true)
	@Override
	public Page<GalleryStorageFileDto> getAllPhotosByStatus(Pageable pageable) {
		return galleryStorageRepo.findByStatus(ApprovalStatus.APPROVED, pageable).map(GalleryStorageFileDto::new);
	}

	@Transactional
	@Override
	public void setActive(Integer id, ApprovalStatus status) {
		GalleryStorageFile file = galleryStorageRepo.findOne(id);
		file.setStatus(status);
		galleryStorageRepo.save(file);
	}

}
