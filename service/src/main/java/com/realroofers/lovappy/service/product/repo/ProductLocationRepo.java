package com.realroofers.lovappy.service.product.repo;

import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.product.model.ProductLocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductLocationRepo extends JpaRepository<ProductLocation, Integer> {

    ProductLocation findByProductIdAndLocationId(@Param("productId")Integer productId,@Param("locationId")Integer locationId);

    List<ProductLocation> findAllByProduct(Product product);

    void deleteAllByProduct(Product product);
}
