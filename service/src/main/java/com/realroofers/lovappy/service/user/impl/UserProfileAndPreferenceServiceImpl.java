package com.realroofers.lovappy.service.user.impl;

import com.google.common.base.Strings;
import com.google.maps.model.LatLng;
import com.realroofers.lovappy.service.cloud.repo.CloudStorageRepo;
import com.realroofers.lovappy.service.couples.model.CouplesProfile;
import com.realroofers.lovappy.service.couples.repo.CouplesProfileRepo;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.system.repo.LanguageRepo;
import com.realroofers.lovappy.service.user.dto.*;
import com.realroofers.lovappy.service.user.model.*;
import com.realroofers.lovappy.service.user.repo.*;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.ZodiacSigns;
import com.realroofers.lovappy.service.util.AddressUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.realroofers.lovappy.service.user.UserProfileAndPreferenceService;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.util.UnitsConvert;

import java.util.*;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@Service
@Slf4j
public class UserProfileAndPreferenceServiceImpl implements UserProfileAndPreferenceService {
    private final UserProfileRepo userProfileRepo;
    private final UserPreferenceRepo userPrefRepo;
    private final LanguageRepo languageRepo;
    private AddressUtil addressUtil;
    private final UserRepo userRepo;
    private final RoleRepo roleRepo;
    private final CloudStorageRepo cloudStorageRepo;
    private final CouplesProfileRepo couplesProfileRepo;
    private final UserRolesRepo userRolesRepo;

    @Autowired
    public UserProfileAndPreferenceServiceImpl(UserProfileRepo userProfileRepo, UserPreferenceRepo userPrefRepo,
                                               LanguageRepo languageRepo, AddressUtil addressUtil, UserRepo userRepo,
                                               RoleRepo roleRepo, CloudStorageRepo cloudStorageRepo, CouplesProfileRepo couplesProfileRepo, UserRolesRepo userRolesRepo) {
        this.userProfileRepo = userProfileRepo;
        this.userPrefRepo = userPrefRepo;
        this.languageRepo = languageRepo;
        this.addressUtil = addressUtil;
        this.userRepo = userRepo;
        this.roleRepo = roleRepo;
        this.cloudStorageRepo = cloudStorageRepo;
        this.couplesProfileRepo = couplesProfileRepo;
        this.userRolesRepo = userRolesRepo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public GenderAndPrefGenderDto getGenderAndPrefGender(Integer userId) {
        GenderAndPrefGenderDto result = null;
        UserProfile profile = userProfileRepo.findOne(userId);
        UserPreference pref = userPrefRepo.findOne(userId);
        if (profile != null && pref != null) {
            result = new GenderAndPrefGenderDto(profile, pref);
        } else if (profile != null) {
            result = new GenderAndPrefGenderDto(profile, null);
        } else if (pref != null) {
            result = new GenderAndPrefGenderDto(null, pref.getGender());
        }
        return result;
    }
    @Transactional
    private void setUserRole(User user, Roles role, ApprovalStatus status) {
        UserRolePK pk = new UserRolePK(user, roleRepo.findByName(role.getValue()));
        if(userRolesRepo.exists(pk))
            return;

        UserRoles userRoles = new UserRoles();
        userRoles.setId(pk);
        if(role==Roles.USER){
            Role coupleRole = roleRepo.findByName(Roles.COUPLE.getValue());

            UserRoles coupleRoles = userRolesRepo.findOne(new UserRolePK(user, coupleRole));
            if(coupleRoles != null) {
                userRolesRepo.delete(coupleRoles);
                if(user.getUserProfile()!=null) {
                    user.getUserProfile().setCouplesProfile(null);
                    userProfileRepo.save(user.getUserProfile());
                }
            }
            userRoles.setApprovalStatus(ApprovalStatus.APPROVED);
        }else if(role==Roles.COUPLE){
            Role userRole = roleRepo.findByName(Roles.USER.getValue());

            UserRoles userR = userRolesRepo.findOne(new UserRolePK(user, userRole));
            if(userR != null)
            userRolesRepo.delete(userR);
            userRoles.setApprovalStatus(ApprovalStatus.APPROVED);
        }
        else {
            userRoles.setApprovalStatus(status);
        }
        userRoles.setCreated(new Date());
        userRolesRepo.save(userRoles);

    }

    @Override
    @Transactional(readOnly = true)
    public RegisterStartDto getRegisterStartDto(Integer userId) {
        RegisterStartDto result = null;
        UserProfile profile = userProfileRepo.findOne(userId);
        UserPreference pref = userPrefRepo.findOne(userId);
        result = new RegisterStartDto(profile, pref);
        return result;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void saveOrUpdate(Integer userId, GenderAndPrefGenderDto dto) {
        User user = userRepo.findOne(userId);

        if (user != null && Strings.isNullOrEmpty(dto.getUserType())) {
            setUserRole(user, Roles.USER, ApprovalStatus.APPROVED );
        } else if (user != null && !Strings.isNullOrEmpty(dto.getUserType())) {
            Set<Role> roles = new HashSet<>();
            if (Roles.AUTHOR.getValue().equals(dto.getUserType())) {
                setUserRole(user, Roles.AUTHOR, ApprovalStatus.PENDING );
            } else if (Roles.EVENT_AMBASSADOR.getValue().equals(dto.getUserType())) {
                setUserRole(user, Roles.EVENT_AMBASSADOR, ApprovalStatus.PENDING );
            }
        }
        userRepo.save(user);

        // Save Profiles
        UserProfile profile = userProfileRepo.findOne(userId);

        if (profile == null) {
            profile = new UserProfile(userId);
        }
        profile.setGender(dto.getGender());
        profile.setBirthDate(dto.getBirthDate());
        profile.setZodiacSigns(ZodiacSigns.getZodiac(dto.getBirthDate()));
//        AddressDto loc = dto.getLocation();
//        if (loc != null && loc.getLongitude() != null && loc.getLatitude() != null) {
//            LatLng location = new LatLng(loc.getLatitude(), loc.getLongitude());
//            profile.setAddress(addressUtil.extractAddress(location, dto.getLocation().getLastMileRadiusSearch()));
//        }

        userProfileRepo.save(profile);

        // Save Preferences
        UserPreference pref = userPrefRepo.findOne(userId);
        if (pref == null) {
            pref = new UserPreference(userId);
        }
        pref.setGender(dto.getPrefGender());
        userPrefRepo.save(pref);
    }

    @Transactional
    @Override
    public void update(Integer userId, GenderAndPrefGenderDto dto) {
        User user = userRepo.findOne(userId);

        if (user != null && Strings.isNullOrEmpty(dto.getUserType())) {
            setUserRole(user, Roles.USER, ApprovalStatus.APPROVED );
        } else if (user != null && !Strings.isNullOrEmpty(dto.getUserType())) {
            Set<Role> roles = new HashSet<>();
            if (Roles.AUTHOR.getValue().equals(dto.getUserType())) {
                setUserRole(user, Roles.AUTHOR, ApprovalStatus.PENDING );
            } else if (Roles.EVENT_AMBASSADOR.getValue().equals(dto.getUserType())) {
                setUserRole(user, Roles.EVENT_AMBASSADOR, ApprovalStatus.PENDING );
            }
        }
        userRepo.save(user);

        // Save Profiles
        UserProfile profile = userProfileRepo.findOne(userId);

        if (profile == null) {
            profile = new UserProfile(userId);
        }
        profile.setGender(dto.getGender());
        profile.setBirthDate(dto.getBirthDate());

        profile.setZodiacSigns(ZodiacSigns.getZodiac(dto.getBirthDate()));

        if (dto.getLocation() != null && dto.getLocation().getLongitude() != null && dto.getLocation().getLongitude() !=0&& dto.getLocation().getLatitude() != null&& dto.getLocation().getLatitude() != 0) {
            LatLng location = new LatLng(dto.getLocation().getLatitude(), dto.getLocation().getLongitude());
            profile.setAddress(addressUtil.extractAddress(location,  dto.getLocation().getLastMileRadiusSearch()));
        }

        userProfileRepo.save(profile);

        // Save Preferences
        UserPreference pref = userPrefRepo.findOne(userId);
        if (pref == null) {
            pref = new UserPreference(userId);
        }
        pref.setGender(dto.getPrefGender());
        userPrefRepo.save(pref);
    }


    @Override
    @Transactional
    public AddressDto saveLocation(Integer userId, AddressDto loc, Integer lastMileRadiusSearch) {
        UserProfile profile = userProfileRepo.findOne(userId);

        if (profile == null) {
            profile = new UserProfile(userId);
        }

        if (loc != null && loc.getLongitude() != null && loc.getLatitude() != null) {
            LatLng location = new LatLng(loc.getLatitude(), loc.getLongitude());
            profile.setAddress(addressUtil.extractAddress(location, lastMileRadiusSearch));
        }

        return AddressDto.toAddressDto(userProfileRepo.save(profile).getAddress());
    }
    @Override
    @Transactional
    public AddressDto saveAddress(Integer userId, AddressDto address) {
        UserProfile profile = userProfileRepo.findOne(userId);

        if (profile == null) {
            profile = new UserProfile(userId);
        }

        if (address != null && address.getLongitude() != null && address.getLatitude() != null) {
            profile.setAddress(address.toAddress());
        }

        return AddressDto.toAddressDto(userProfileRepo.save(profile).getAddress());
    }

    @Transactional
    @Override
    public void saveOrUpdate(Integer userId, Map<String, String> statuses, Set<String> seekingLanguages, Set<String> speakingLanguages, AgeHeightLangAndPrefSizeAgeHeightLangDto ageHeightLangAndPrefSizeAgeHeightLangDto, PersonalityLifestyleDto personalityLifestyleDto) {
        Gender prefGender = ageHeightLangAndPrefSizeAgeHeightLangDto.getPrefGender();
        if (prefGender == null) {
            throw new IllegalArgumentException("Pref Gender is required.");
        }
        // Save Profiles
        UserProfile profile = userProfileRepo.findOne(userId);
        if (profile == null) {
            profile = new UserProfile(userId);
        }
//        profile.setBirthDate(dto.getBirthDate());
//        profile.setLanguage(dto.getLanguage());
        /*profile.setHeightFt(dto.getHeightFt());
        profile.setHeightIn(dto.getHeightIn());*/
        if (ageHeightLangAndPrefSizeAgeHeightLangDto.getHeightCM() != null)
        {
            profile.setHeightCm(ageHeightLangAndPrefSizeAgeHeightLangDto.getHeightCM());
        } else if (ageHeightLangAndPrefSizeAgeHeightLangDto.getHeightFt() != null && ageHeightLangAndPrefSizeAgeHeightLangDto.getHeightIn() != null){
            profile.setHeightCm(UnitsConvert.feetToCentimeter(ageHeightLangAndPrefSizeAgeHeightLangDto.getHeightFt(), ageHeightLangAndPrefSizeAgeHeightLangDto.getHeightIn()));
        }

        if (speakingLanguages != null) {
            profile.setSpeakingLanguage(languageRepo.findByAbbreviationIn(speakingLanguages));
        }


        profile.setStatuses(statuses);

        profile.setPersonalities(personalityLifestyleDto.getPersonalities());
        profile.setLifestyles(personalityLifestyleDto.getLifestyles());
        userProfileRepo.save(profile);
        // Save Preferences
        UserPreference pref = userPrefRepo.findOne(userId);
        if (pref == null) {
            pref = new UserPreference(userId);
        }
        if (Gender.FEMALE.equals(prefGender)) {
            pref.setBustSize(ageHeightLangAndPrefSizeAgeHeightLangDto.getPrefBustSize());
            pref.setButtSize(ageHeightLangAndPrefSizeAgeHeightLangDto.getPrefButtSize());
            pref.setPenisSizeMatter(null);
        } else if (Gender.MALE.equals(prefGender)) {
            pref.setBustSize(null);
            pref.setButtSize(null);
            pref.setPenisSizeMatter(ageHeightLangAndPrefSizeAgeHeightLangDto.getPenisSizeMatter());
        } else {
            // Both
            pref.setBustSize(ageHeightLangAndPrefSizeAgeHeightLangDto.getPrefBustSize());
            pref.setButtSize(ageHeightLangAndPrefSizeAgeHeightLangDto.getPrefButtSize());
            pref.setPenisSizeMatter(ageHeightLangAndPrefSizeAgeHeightLangDto.getPenisSizeMatter());
        }
        pref.setMinAge(ageHeightLangAndPrefSizeAgeHeightLangDto.getPrefMinAge());
        pref.setMaxAge(ageHeightLangAndPrefSizeAgeHeightLangDto.getPrefMaxAge());
        pref.setHeight(ageHeightLangAndPrefSizeAgeHeightLangDto.getPrefHeight());
//        pref.setLanguage(dto.getPrefLanguage());

        if (seekingLanguages != null) {
            pref.setSeekingLanguage(languageRepo.findByAbbreviationIn(seekingLanguages));
        }

        userPrefRepo.save(pref);
    }

    @Transactional
    @Override
    public void updateProfile(Integer userId, UpdateProfileRequest profileRequest) {
        UserProfile profile = userProfileRepo.findOne(userId);

        if(profileRequest.getSpeakinglanguages() != null && profileRequest.getSpeakinglanguages().size()>0){
            profile.setSpeakingLanguage(languageRepo.findByAbbreviationIn(profileRequest.getSpeakinglanguages()));
        }
        userProfileRepo.save(profile);

        UserPreference pref = userPrefRepo.findOne(userId);
        if(profileRequest.getPrefGender() != null){
            pref.setGender(profileRequest.getPrefGender());
        }

        if(profileRequest.getPrefHeight() != null){
            pref.setHeight(profileRequest.getPrefHeight());
        }

        if(profileRequest.getMinAge() != null){
            pref.setMinAge(profileRequest.getMinAge());
        }
        if(profileRequest.getMaxAge() != null){
            pref.setMaxAge(profileRequest.getMaxAge());
        }
        if(profileRequest.getSeekingLanguages() != null&& profileRequest.getSeekingLanguages().size()>0){
            pref.setSeekingLanguage(languageRepo.findByAbbreviationIn(profileRequest.getSeekingLanguages()));
        }

        userPrefRepo.save(pref);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public AgeHeightLangAndPrefSizeAgeHeightLangDto getAgeHeightLangAndPrefSizeAgeHeightLang(Integer userId) {
        UserProfile profile = userProfileRepo.findOne(userId);
        UserPreference pref = userPrefRepo.findOne(userId);
        return new AgeHeightLangAndPrefSizeAgeHeightLangDto(profile, pref);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void saveOrUpdate(Integer userId, AgeHeightLangAndPrefSizeAgeHeightLangDto dto) {
        Gender prefGender = dto.getPrefGender();
        if (prefGender == null) {
            throw new IllegalArgumentException("Pref Gender is required.");
        }
        // Save Profiles
        UserProfile profile = userProfileRepo.findOne(userId);
        if (profile == null) {
            profile = new UserProfile(userId);
        }
//        profile.setBirthDate(dto.getBirthDate());
//        profile.setLanguage(dto.getLanguage());
        /*profile.setHeightFt(dto.getHeightFt());
        profile.setHeightIn(dto.getHeightIn());*/
        if (dto.getHeightCM() != null)
        {
        	profile.setHeightCm(dto.getHeightCM());
        } else if (dto.getHeightFt() != null && dto.getHeightIn() != null){
            profile.setHeightCm(UnitsConvert.feetToCentimeter(dto.getHeightFt(), dto.getHeightIn()));
        }

        userProfileRepo.save(profile);
        // Save Preferences
        UserPreference pref = userPrefRepo.findOne(userId);
        if (pref == null) {
            pref = new UserPreference(userId);
        }
        if (Gender.FEMALE.equals(prefGender)) {
            pref.setBustSize(dto.getPrefBustSize());
            pref.setButtSize(dto.getPrefButtSize());
            pref.setPenisSizeMatter(null);
        } else if (Gender.MALE.equals(prefGender)) {
            pref.setBustSize(null);
            pref.setButtSize(null);
            pref.setPenisSizeMatter(dto.getPenisSizeMatter());
        } else {
            // Both
            pref.setBustSize(dto.getPrefBustSize());
            pref.setButtSize(dto.getPrefButtSize());
            pref.setPenisSizeMatter(dto.getPenisSizeMatter());
        }
        pref.setMinAge(dto.getPrefMinAge());
        pref.setMaxAge(dto.getPrefMaxAge());
        pref.setHeight(dto.getPrefHeight());
//        pref.setLanguage(dto.getPrefLanguage());
        userPrefRepo.save(pref);
    }


    @Override
    @Transactional(readOnly = true)
    public StatusLangDto getStatusLang(Integer userId) {
        UserProfile profile = userProfileRepo.findOne(userId);
        UserPreference pref = userPrefRepo.findOne(userId);
        return new StatusLangDto(profile, pref);
//        return new StatusLangDto(userProfileRepo.findOne(userId));
    }

    @Override
    @Transactional
    public void setSeekingLove(Integer userId) {
        User user = userRepo.findOne(userId);
        // Save Profiles
        UserProfile profile = user.getUserProfile();
        CouplesProfile couplesProfile = profile.getCouplesProfile();
        List<UserProfile> userProfiles = null;
        if(couplesProfile == null) {
            userProfiles = Arrays.asList(user.getUserProfile());
        } else {
            userProfiles = couplesProfile.getCouples();
        }
        Role userRole = roleRepo.findByName(Roles.USER.getValue());
        Role coupleRole = roleRepo.findByName(Roles.COUPLE.getValue());
        for (UserProfile updatedUser: userProfiles) {


             UserRoles coupleRoles = userRolesRepo.findOne(new UserRolePK(updatedUser.getUser(), coupleRole));
            if(coupleRoles != null) {
                userRolesRepo.delete(coupleRoles);
            }

            UserRolePK pk = new UserRolePK(user, userRole);
            UserRoles userRoles = userRolesRepo.findOne(new UserRolePK(updatedUser.getUser(), userRole));

            if(userRoles == null) {
                userRoles = new UserRoles();
                userRoles.setId(pk);
                userRoles.setApprovalStatus(ApprovalStatus.APPROVED);
                userRoles.setCreated(new Date());
                userRolesRepo.save(userRoles);
            }

          updatedUser.setCouplesProfile(null);

        }
        userProfileRepo.save(userProfiles);
        if(couplesProfile != null) {
            couplesProfile.getCouples().removeAll(userProfiles);
            couplesProfileRepo.delete(couplesProfile);
        }

    }

    @Override
    @Transactional
    public void saveOrUpdate(Integer userId, StatusLangDto dto) {
        List<Integer> listA = new ArrayList<>();
        if (dto.getSpeakingLanguages() != null) {
            for (LanguageDto s : dto.getSpeakingLanguages())
                listA.add(s.getId());
        }

        List<Integer> listB = new ArrayList<>();
        if (dto.getSeekingLanguages() != null) {
            for (LanguageDto s : dto.getSeekingLanguages())
                listB.add(s.getId());
        }

        UserProfile profile = userProfileRepo.findOne(userId);
        if (profile == null) profile = new UserProfile(dto.getUserId());
        profile.setStatuses(dto.getStatuses());
        profile.setSpeakingLanguage(languageRepo.findByIdIn(listA));
        userProfileRepo.save(profile);

        UserPreference pref = userPrefRepo.findOne(userId);
        if (pref == null) pref = new UserPreference(userId);
        pref.setSeekingLanguage(languageRepo.findByIdIn(listB));
        userPrefRepo.save(pref);
    }

}
