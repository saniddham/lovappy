package com.realroofers.lovappy.service.gift;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.gift.dto.PurchasedGiftDto;

public interface PurchasedGiftService extends AbstractService<PurchasedGiftDto, Integer> {
}
