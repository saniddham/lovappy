package com.realroofers.lovappy.service.radio.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.radio.support.AudioAction;
import com.realroofers.lovappy.service.radio.support.AudioInsertWhere;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Manoj on 17/12/2017.
 */

@Data
@Entity
@Table(name = "radio_program_control")
@EqualsAndHashCode
public class RadioProgramControl extends BaseEntity<Integer> implements Serializable {

    @ManyToOne
    @JoinColumn(name = "fk_media_type")
    private MediaType mediaType;

    @ManyToOne
    @JoinColumn(name = "fk_audio_type")
    private AudioType audioType;

    @ManyToOne
    @JoinColumn(name = "fk_sub_type")
    private AudioSubType audioSubType;

    @Enumerated
    @Column(name = "audio_action")
    private AudioAction audioAction;

    @Enumerated
    @Column(name = "insert_where")
    private AudioInsertWhere insertWhere;

    @Column(name = "gender")
    private String gender;

    @Column(name = "daily_limit")
    private Integer dailyLimit;

    @Column(name = "lifetime_limit")
    private Integer lifetimeLimit;

    @Column(name = "content_before")
    private Integer contentBefore;

    @Column(name = "content_after")
    private Integer contentAfter;

    @Column(name = "age_18_to_24")
    private Boolean ageBetween18And24;

    @Column(name = "age_25_to_34")
    private Boolean ageBetween25And34;

    @Column(name = "age_35_to_49")
    private Boolean ageBetween35And49;

    @Column(name = "age_above_50")
    private Boolean ageAbove50;

    @Column(name = "play_to_all")
    private Boolean playToAll;

    @Column(name = "play_next")
    private Boolean playNext;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;


}
