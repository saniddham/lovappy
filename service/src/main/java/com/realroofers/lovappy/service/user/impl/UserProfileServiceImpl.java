package com.realroofers.lovappy.service.user.impl;

import com.google.maps.model.LatLng;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.exception.ResourceNotFoundException;
import com.realroofers.lovappy.service.exception.SaveOrUpdateException;
import com.realroofers.lovappy.service.likes.model.Like;
import com.realroofers.lovappy.service.likes.model.LikePK;
import com.realroofers.lovappy.service.likes.repo.LikeRepository;
import com.realroofers.lovappy.service.lovstamps.model.Lovstamp;
import com.realroofers.lovappy.service.lovstamps.repo.LovstampRepo;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.system.repo.LanguageRepo;
import com.realroofers.lovappy.service.user.UserProfileService;
import com.realroofers.lovappy.service.user.UserUtil;
import com.realroofers.lovappy.service.user.dto.*;
import com.realroofers.lovappy.service.user.model.*;
import com.realroofers.lovappy.service.user.model.embeddable.Address;
import com.realroofers.lovappy.service.user.repo.ProfileTransactionRepo;
import com.realroofers.lovappy.service.user.repo.UserProfileRepo;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.SubscriptionType;
import com.realroofers.lovappy.service.user.support.ZodiacSigns;
import com.realroofers.lovappy.service.util.AddressUtil;
import com.realroofers.lovappy.service.util.UnitsConvert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Service("userProfileService")
public class UserProfileServiceImpl implements UserProfileService {
    private static Logger LOG = LoggerFactory.getLogger(UserProfileServiceImpl.class);
    private final UserProfileRepo userProfileRepo;
    private final LanguageRepo languageRepo;
    private final UserRepo userRepo;
    private final AddressUtil addressUtil;
    private final LikeRepository likeRepository;

    private final LovstampRepo lovstampRepo;
    private final ProfileTransactionRepo profilePhotoTransactionRepo;

    public UserProfileServiceImpl(UserProfileRepo userProfileRepo, LikeRepository likeRepository,
                                  UserRepo userRepo, LanguageRepo languageRepo, AddressUtil addressUtil,
                                  LovstampRepo lovstampRepo, ProfileTransactionRepo profilePhotoTransactionRepo) {
        this.userProfileRepo = userProfileRepo;
        this.userRepo = userRepo;
        this.languageRepo = languageRepo;
        this.addressUtil = addressUtil;
        this.likeRepository = likeRepository;
        this.lovstampRepo = lovstampRepo;
        this.profilePhotoTransactionRepo = profilePhotoTransactionRepo;
    }

    @Override
    @Transactional(readOnly = true)
    public UserProfileDto findOne(Integer usedId) {
        LOG.debug("Get Profile");
        UserProfile userProfile = userProfileRepo.findOne(usedId);
        return userProfile == null ? null : new UserProfileDto(userProfile);
    }
    @Override
    @Transactional(readOnly = true)
    public String getStripeIdByUserId(Integer userId) {
        UserProfile userProfile = userProfileRepo.getOne(userId);
        return userProfile == null ? null : userProfile.getStripeCustomerId();
    }

    @Transactional
    @Override
    public void saveStripeCustomerId(Integer userId, String customerId) {
        UserProfile userProfile = userProfileRepo.findOne(userId);
        userProfile.setStripeCustomerId(customerId);
    }

    @Override
    @Transactional(readOnly = true)
    public UserProfileDto findByUserId(Integer id) {
        UserProfile userProfile = userProfileRepo.findByUserId(id);
        UserProfileDto userProfileDto = new UserProfileDto(userProfile);

        return userProfileDto;
    }

    @Override
    @Transactional(readOnly = true)
    public MemberProfileDto findFullOne(Integer usedId) {
        UserProfile userProfile = userProfileRepo.findOne(usedId);
        if(userProfile != null) {
            MemberProfileDto memberProfileDto = new MemberProfileDto(userProfile);
            memberProfileDto.setProfilePhotoCloudFile(new CloudStorageFileDto(userProfile.getProfilePhotoCloudFile()));
            return memberProfileDto;
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public MemberFullProfileDto getFullProfile(Integer usedId) {
        User user = userRepo.findOneByUserId(usedId);
        if(user != null) {

            Lovstamp  voice = lovstampRepo.findByUser(user);


            MemberFullProfileDto memberProfileDto = voice == null ? new MemberFullProfileDto(user) :  new MemberFullProfileDto(user, voice);
            return memberProfileDto;
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public MemberProfileDto findFullOneWihtAllowedProfilePic(Integer memberId, Integer currentId) {
        UserProfile userProfile = userProfileRepo.findOne(memberId);
        if(userProfile != null) {
           MemberProfileDto memberProfileDto = new MemberProfileDto(userProfile);

           return memberProfileDto;
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public CloudStorageFileDto getAllowedProfilePhoto(Integer memberId, Integer currentId) {
              CloudStorageFileDto cloudStorageFileDto =  new CloudStorageFileDto(userProfileRepo.findUserPicture(memberId, currentId));
   return cloudStorageFileDto;
    }
    @Override
    @Transactional
    public void saveOrUpdate(UserProfileDto dto) {
        UserProfile model = userProfileRepo.findOne(dto.getUserId());
        if (model == null) {
            model = new UserProfile(dto.getUserId());
            model.setSubscriptionType(SubscriptionType.LITE);
        }
        model.setUserId(dto.getUserId());
        model.setGender(dto.getGender());
        model.setBirthDate(dto.getBirthDate());
        model.setZodiacSigns(ZodiacSigns.getZodiac(dto.getBirthDate()));
        model.setAllowedProfilePic(dto.getAllowedProfilePic());
        if (dto.getHeightFt() != null && dto.getHeightIn() != null)
            model.setHeightCm(UnitsConvert.feetToCentimeter(dto.getHeightFt(), dto.getHeightIn()));

        userProfileRepo.save(model);
    }

    @Override
    @Transactional(readOnly = true)
    public PersonalityLifestyleStatusLocationDto getPersonalityLifestyleStatusLocation(Integer userId) {
        return new PersonalityLifestyleStatusLocationDto(userProfileRepo.findOne(userId));
    }

    @Override
    @Transactional
    public void saveOrUpdate(Integer userId, PersonalityLifestyleStatusLocationDto dto) {
        UserProfile model = userProfileRepo.findOne(userId);
        if (model == null) {
            model = new UserProfile(dto.getUserId());
            model.setSubscriptionType(SubscriptionType.LITE);
        }
        model.setPersonalities(dto.getPersonalities());
        model.setLifestyles(dto.getLifestyles());
        model.setStatuses(dto.getStatuses());
//        if (dto.getLocation() != null) {
//            model.setAddress(dto.getLocation().toAddress());
//        }
        userProfileRepo.save(model);
    }

    @Override
    @Transactional(readOnly = true)
    public PersonalityLifestyleDto getPersonalityLifestyle(Integer userId) {
        return new PersonalityLifestyleDto(userProfileRepo.findOne(userId));
    }

    @Override
    @Transactional
    public void saveOrUpdate(Integer userId, PersonalityLifestyleDto dto) {
        UserProfile model = userProfileRepo.findOne(userId);
        if (model == null) {
            model = new UserProfile(userId);
        }
        model.setPersonalities(dto.getPersonalities());
        model.setLifestyles(dto.getLifestyles());
        userProfileRepo.save(model);
    }

    @Override
    @Transactional
    public UserProfileDto update(Integer userID, Collection<LanguageDto> speakingLanguages, AddressDto address) {
        UserProfile userProfile = userProfileRepo.findOne(userID);

        List<Integer> listA = new ArrayList<>();
        if (speakingLanguages != null) {
            for (LanguageDto s : speakingLanguages)
                listA.add(s.getId());
        }

        if (userProfile != null) {
            userProfile.setSpeakingLanguage(languageRepo.findByIdIn(listA));

            LatLng location = new LatLng(address.getLatitude(), address.getLongitude());
            userProfile.setAddress(addressUtil.extractAddress(location, address.getLastMileRadiusSearch()));

            UserProfile save = userProfileRepo.save(userProfile);

            return new UserProfileDto(save);
        }

        return null;
    }


    @Override
    @Transactional(readOnly = true)
    public ProfilePicsDto getProfilePicsDto(Integer userId) {
        return new ProfilePicsDto(userProfileRepo.findOne(userId));
    }

    @Override
    @Transactional
    public void saveSubscriptionType(Integer userId, SubscriptionType subscriptionType) {
        UserProfile model = userProfileRepo.findOne(userId);
        if (model == null) {
            throw new SaveOrUpdateException("User with id " + userId + " not found.");
        }
        model.setCompleted(true);
        model.setSubscriptionType(subscriptionType);
        userProfileRepo.save(model);
    }

    @Override
    @Transactional(readOnly = true)
    public SubscriptionType getSubscriptionType(Integer userId) {
        UserProfile model = userProfileRepo.findOne(userId);
        return model != null ? model.getSubscriptionType() : null;
    }

    @Override
    @Transactional
    public void saveHandsPhoto(CloudStorageFileDto cloudStorageFile) {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        UserProfile model = userProfileRepo.findOne(userId);
        if (model == null) {
            throw new SaveOrUpdateException("User with id " + userId + " not found.");
        }

        CloudStorageFile file = toEntity(cloudStorageFile);
        model.setHandsCloudFile(file);
        model.setHandsFileSkipped(false);

        userProfileRepo.save(model);
    }

    @Override
    @Transactional
    public void saveFeetPhoto(CloudStorageFileDto cloudStorageFile) {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        UserProfile model = userProfileRepo.findOne(userId);
        if (model == null) {
            throw new SaveOrUpdateException("User with id " + userId + " not found.");
        }

        CloudStorageFile file = toEntity(cloudStorageFile);
        model.setFeetCloudFile(file);
        model.setFeetFileSkipped(false);

        userProfileRepo.save(model);
    }

    @Override
    @Transactional
    public void saveLegsPhoto(CloudStorageFileDto cloudStorageFile) {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        UserProfile model = userProfileRepo.findOne(userId);
        if (model == null) {
            throw new SaveOrUpdateException("User with id " + userId + " not found.");
        }

        CloudStorageFile file = toEntity(cloudStorageFile);
        model.setLegsCloudFile(file);
        model.setLegsFileSkipped(false);

        userProfileRepo.save(model);
    }

    @Override
    @Transactional
    public void saveProfilePhoto(Integer userId, CloudStorageFileDto cloudStorageFile) {

        UserProfile model = userProfileRepo.findOne(userId);
        if (model == null) {
            throw new SaveOrUpdateException("User with id " + userId + " not found.");
        }

        CloudStorageFile file = toEntity(cloudStorageFile);
        model.setProfilePhotoCloudFile(file);

        userProfileRepo.save(model);
    }

    @Override
    @Transactional
    public void saveIdentificationProfilePhoto(CloudStorageFileDto cloudStorageFile) {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        UserProfile model = userProfileRepo.findOne(userId);
        if (model == null) {
            throw new SaveOrUpdateException("User with id " + userId + " not found.");
        }

        CloudStorageFile file = toEntity(cloudStorageFile);
        model.setPhotoIdentificationCloudFile(file);

        userProfileRepo.save(model);

    }

    private CloudStorageFile toEntity(CloudStorageFileDto cloudStorageFile) {
        CloudStorageFile file = new CloudStorageFile();
        file.setName(cloudStorageFile.getName());
        file.setUrl(cloudStorageFile.getUrl());
        file.setBucket(cloudStorageFile.getBucket());
        return file;
    }

    @Override
    @Transactional
    public void skipHandsPhoto() {
        skipFile(profile -> {
            //profile.setHandsFileId(null); // TODO: Delete the file in file repo if existing
            profile.setHandsFileSkipped(true);
        });
    }
    @Transactional
    @Override
    public void skipAllPhotos() {
        skipFile(profile -> {
            profile.setFeetFileSkipped(true);
            profile.setHandsFileSkipped(true);
            profile.setLegsFileSkipped(true);
        });
    }

    @Transactional
    @Override
    public void skipVoice() {
        skipFile(profile -> {
            profile.setLovstampSkipped(true);
        });
    }

    @Override
    @Transactional
    public void skipFeetPhoto() {
        skipFile(profile -> {
            //profile.setFeetFileId(null); // TODO: Delete the file in file repo if existing
            profile.setFeetFileSkipped(true);
        });
    }

    @Override
    @Transactional
    public void skipLegsPhoto() {
        skipFile(profile -> {
            //profile.setLegsFileId(null); // TODO: Delete the file in file repo if existing
            profile.setLegsFileSkipped(true);
        });
    }
    @Transactional(readOnly =  true)
    @Override
    public CloudStorageFileDto showProfilePhotoForUser(Integer userId, Integer currentUserId) {
        CloudStorageFile profilePic = userProfileRepo.findUserPicture(userId, currentUserId);
        return profilePic == null ? null : new CloudStorageFileDto(profilePic);
    }

    private void skipFile(Consumer<UserProfile> consumer) {
        Integer userId = UserUtil.INSTANCE.getCurrentUserId();
        if (userId == null) {
            throw new AuthenticationServiceException("User is not currently logged in.");
        }
        UserProfile profile = userProfileRepo.findOne(userId);
        consumer.accept(profile);
        userProfileRepo.save(profile);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserLocationDto> getNearbyUsers(WithinRadiusDto searchDto) {
        int currentUserId = UserUtil.INSTANCE.getLoggedInUserId();
        if(searchDto.getPrefGender() == Gender.BOTH || searchDto.getPrefGender() == Gender.ALL) {
            return userProfileRepo.getNearbyUsers(searchDto.getLatitude(), searchDto.getLongitude(), searchDto.getRadius()).stream().filter(userProfile -> userProfile.getUserId() != currentUserId).map(profile -> {
                Address address = profile.getAddress();
                return new UserLocationDto(profile.getGender(), address.getFullAddress(), address.getLatitude(),
                        address.getLongitude());
            })
                    .collect(Collectors.toList());
        }
        return userProfileRepo.getNearbyUsers(searchDto.getLatitude(), searchDto.getLongitude(), searchDto.getRadius(),
                 searchDto.getPrefGender().name()).stream().filter(userProfile -> userProfile.getUserId() != currentUserId).map(profile -> {
            Address address = profile.getAddress();
            return new UserLocationDto(profile.getGender(), address.getFullAddress(), address.getLatitude(),
                    address.getLongitude());
        })
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Long countNearbyUsers(Integer userId, WithinRadiusDto searchDto) {
        if(searchDto.getPrefGender() == Gender.BOTH) {
            return userProfileRepo.countNearbyUsers(searchDto.getLatitude(), searchDto.getLongitude(), searchDto.getRadius(),
                    userId);

        }
            return userProfileRepo.countNearbyUsers(searchDto.getLatitude(), searchDto.getLongitude(), searchDto.getRadius(),
                    userId, searchDto.getPrefGender().name());
    }
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public int blockUser(Integer loginUserId, Integer blockedUserId) {
        UserProfile model = userProfileRepo.findOne(loginUserId);
        User loggedInUser = model.getUser();
        List<User> blockedUsers = model.getBlockedUsers();
        User blocked = userRepo.findOne(blockedUserId);
        blockedUsers.add(blocked);
        userProfileRepo.save(model);

        //unlike the user if

        LikePK id = new LikePK(loggedInUser, blocked);
        Like likeYou = likeRepository.findById(id);
        if (likeYou != null) {

            //matched
            if (likeRepository.findById(new LikePK(blocked, loggedInUser)) != null) {
                loggedInUser.setMatchesCount(loggedInUser.getMatchesCount() != null && loggedInUser.getMatchesCount() > 0 ? loggedInUser.getMatchesCount() - 1 : 0);
                blocked.setMatchesCount(blocked.getMatchesCount() != null && blocked.getMatchesCount() > 0 ? blocked.getMatchesCount() - 1 : 0);
            }


            loggedInUser.setMyLikesCount(loggedInUser.getMyLikesCount() == null ? 0 : loggedInUser.getMyLikesCount() - 1);
            blocked.setLikeMeCount(blocked.getLikeMeCount() == null ? 0 : blocked.getLikeMeCount() - 1);

            //save to save user counters
            likeRepository.save(likeYou);
            likeRepository.delete(likeYou);
        }
        return 1;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public int unblockUser(Integer loginUserId, Integer blockedUserId) {
        UserProfile model = userProfileRepo.findOne(loginUserId);
        List<User> blockedUsers = model.getBlockedUsers();
        User user = userRepo.findOne(blockedUserId);
        blockedUsers.remove(user);
        userProfileRepo.save(model);
        return 1;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public List<Integer> getBlockUserIdList(Integer loginUserId) {
        List<Integer> result = new ArrayList<Integer>();
        UserProfile model = userProfileRepo.findOne(loginUserId);
        List<User> blockedUsers = model.getBlockedUsers();
        if (blockedUsers != null && blockedUsers.size() > 0) {
            for (User user : blockedUsers) {
                result.add(user.getUserId());
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Boolean getBlockStatus(Integer loginUserId, Integer checkUserId) {

        UserProfile model = userProfileRepo.checkUserBlockStatus(loginUserId, checkUserId);

        return model != null;
    }

    @Override
    @Transactional
    public void updateUserProfileViewsAndAvgDistance(Integer userId, Integer viewerId) {
        try {
            UserProfile userProfile = userProfileRepo.findOne(userId);
            UserProfile viewerProfile = userProfileRepo.findOne(viewerId);

            Address userAddress = userProfile.getAddress();
            Address viewerAddress = viewerProfile.getAddress();

            //calculate the distance between the two users
            double distance = AddressUtil.distance(userAddress.getLatitude(),
                    viewerAddress.getLatitude(),
                    userAddress.getLongitude(),
                    viewerAddress.getLongitude(),
                    0, 0);


            // calculate the new avg distance for the user
            double avgDistance = userProfile.getAvgDistanceOfViewers() * userProfile.getViews() + distance;
            userProfile.setViews(userProfile.getViews() + 1);
            avgDistance = avgDistance / (double) userProfile.getViews();
            userProfile.setAvgDistanceOfViewers(avgDistance);

            userProfileRepo.save(userProfile);
        } catch (Exception ex) {
            LOG.error("User has no loction");
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Integer getTotalCommunicationsCount(Integer userID1, Integer userID2) {

//        int size1 = privateMessageService.getPrivateMessagesByFromUserAndToUser(userID1, userID2).size();
//        int size2 = privateMessageService.getPrivateMessagesByFromUserAndToUser(userID2, userID1).size();
//        int size3 = giftExchangeService.getGiftsByFromUserAndToUser(userID1, userID2).size();
//        int size4 = giftExchangeService.getGiftsByFromUserAndToUser(userID2, userID1).size();

        return 0;
    }

    @Transactional
    @Override
    public void enableProfileAccessView(Integer userId, Integer viewerId) {

        /*
          1) when the user is registered to the system  and choose all user this flag will be true and the list will be used to add disallowed users .
          2) on the other hand, when the user choose custom users the flag will be false and the list will be used to add the allowed users
         */
        UserProfile userProfile = userProfileRepo.findOne(userId);
        User viewer = userRepo.findOne(viewerId);
        if(userProfile.getAllowedProfilePic()) {
            userProfile.getAllowedProfileUsers().remove(viewer);
        } else {
            userProfile.getAllowedProfileUsers().add(viewer);
        }
        userProfileRepo.save(userProfile);
    }

    @Transactional
    @Override
    public void disableProfileAccessView(Integer userId, Integer viewerId) {
  /*
          1) when the user is registered to the system  and choose all user this flag will be true and the list will be used to add disallowed users .
          2) on the other hand, when the user choose custom users the flag will be false and the list will be used to add the allowed users
         */
        UserProfile userProfile = userProfileRepo.findOne(userId);
        User viewer = userRepo.findOne(viewerId);
        if(userProfile.getAllowedProfilePic()) {
            userProfile.getAllowedProfileUsers().add(viewer);
        } else {
            userProfile.getAllowedProfileUsers().remove(viewer);
        }
        userProfileRepo.save(userProfile);
    }

    @Transactional(readOnly = true)
    @Override
    public boolean isProfilePicAccessed(Integer userId, Integer viewerId) {
        UserProfile userProfile = userProfileRepo.findOne(userId);
        User viewer = userRepo.findOne(viewerId);
        if(userProfile.getAllowedProfilePic() != null && userProfile.getAllowedProfilePic()) {
            return !userProfile.getAllowedProfileUsers().contains(viewer);
        } else {
            return userProfile.getAllowedProfileUsers().contains(viewer);
        }
    }

    @Transactional
    @Override
    public void setProfileImageAccess(boolean access, Integer maxTransactions, Integer userId) {
        UserProfile userProfile =  userProfileRepo.findOne(userId);
        if(userProfile == null) {
            throw new ResourceNotFoundException("User not "+ userId +" found");
        }
        if(userProfile.getAllowedProfilePic() != access) {
            userProfile.setAllowedProfilePic(access);
            userProfile.getAllowedProfileUsers().clear();
        }
        userProfile.setMaxTransactions(maxTransactions);
        userProfileRepo.save(userProfile);
    }

    @Transactional(readOnly = true)
    @Override
    public Long countUserProfileImageAccess(Integer userId) {
        return userProfileRepo.countCustomUsersProfileAccess(userId);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<UserProfilePictureAccessDto> getUserProfileImageAccess(Integer userId, int page, int limit) {
        UserProfile userProfile = userProfileRepo.findByUserId(userId);
        if(userProfile != null) {

            if(userProfile.getAllowedProfilePic() != null && !userProfile.getAllowedProfilePic()) {
                return userProfileRepo.findCustomUsersProfileAccess(userId, new PageRequest(page - 1, limit)).map(UserProfilePictureAccessDto::new);
            }
            return userProfileRepo.findAllRevokedUsersProfileAccess(userId, new PageRequest(page - 1, limit)).map(UserProfilePictureAccessDto::new);

        }

        return new PageImpl<>(new ArrayList<>());
    }

    @Transactional
    @Override
    public boolean saveTransaction(Integer userFromId, Integer userToId) {
        User userTo = userRepo.findOne(userToId);
        UserProfile userProfileTo = userTo.getUserProfile();

        User userFrom = userRepo.findOne(userFromId);

        ProfileTransactionPK id = new ProfileTransactionPK(userFrom, userTo);
        ProfileTransaction profilePhotoTransaction = profilePhotoTransactionRepo.findOne(id);
        if (profilePhotoTransaction == null) {
            profilePhotoTransaction = new ProfileTransaction();
            profilePhotoTransaction.setId(id);
            profilePhotoTransaction.setTransactionCount(0);
            profilePhotoTransaction.setSeen(true);
            profilePhotoTransaction.setCreated(new Date());
        }
        profilePhotoTransaction.setLastTransaction(new Date());

        profilePhotoTransaction.setTransactionCount(profilePhotoTransaction.getTransactionCount() + 1);

        if(profilePhotoTransaction.getTransactionCount() == userProfileTo.getMaxTransactions()) {
           profilePhotoTransaction.setSeen(false);
           enableProfileAccessView(userToId, userFromId);
        }
        profilePhotoTransactionRepo.save(profilePhotoTransaction);
        Boolean notification = userTo.getUserPreference().getNewUnlockRequestNotifications() == null? false : userTo.getUserPreference().getNewUnlockRequestNotifications();
        return  (notification && (profilePhotoTransaction.getTransactionCount() == userProfileTo.getMaxTransactions()));
    }

    @Transactional(readOnly = true)
    @Override
    public boolean lockedByTransactionAlgorithem(Integer userFromId, Integer userToId) {
        User userTo = userRepo.findOne(userToId);
        UserProfile userProfileTo = userTo.getUserProfile();
        if(userProfileTo.getAllowedProfilePic() || userProfileTo.getMaxTransactions() == null || userProfileTo.getMaxTransactions() == 0)
            return false;

        User userFrom = userRepo.findOne(userFromId);

        ProfileTransactionPK id = new ProfileTransactionPK(userFrom, userTo);
        ProfileTransaction profilePhotoTransaction = profilePhotoTransactionRepo.findOne(id);
        if (profilePhotoTransaction == null) {
           return false;
        }
        Boolean notification = userTo.getUserPreference().getNewUnlockRequestNotifications() == null? false : userTo.getUserPreference().getNewUnlockRequestNotifications();

        return notification && (profilePhotoTransaction.getTransactionCount() == userProfileTo.getMaxTransactions());
    }

    @Transactional(readOnly = true)
    @Override
    public Long countPendingProfileAccess(Integer userId, Integer maxTransactions) {
        return profilePhotoTransactionRepo.countByIdToUserId(userId, maxTransactions);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<UserProfilePictureAccessDto> getPendingUserProfileImageAccess(Integer userId, Integer maxTransactions, Pageable pageable) {
        return profilePhotoTransactionRepo.findByIdToUserId(userId, maxTransactions, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public Long countUnseenProfileImageAccess(Integer userId) {
        return  profilePhotoTransactionRepo.countByIdToUserIdAndSeenFalse(userId);
    }

    @Transactional
    @Override
    public void setTransactionAsSeen(Integer userId) {
        profilePhotoTransactionRepo.updateByIdToUserIdAndSeenFalse(userId);
    }
    @Transactional(readOnly = true)
    @Override
    public Set<Integer> filterUserBlocksByUserIds(Integer currentUserId, List<Integer> userIds) {
        return userProfileRepo.findBlockedUserIdsByUserId(currentUserId, userIds);
    }
}
