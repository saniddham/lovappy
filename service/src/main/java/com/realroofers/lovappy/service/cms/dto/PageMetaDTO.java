package com.realroofers.lovappy.service.cms.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode
public class PageMetaDTO implements Serializable{

    private String title;
    private String description;
    private String keywords;
    private String abstractDescription;
    private Boolean active;

    public PageMetaDTO() {
    }

    public PageMetaDTO(String title, String description, String keywords, String abstractDescription, Boolean active) {
        this.title = title;
        this.description = description;
        this.keywords = keywords;
        this.abstractDescription = abstractDescription;
        this.active = active;
    }

    @Override
    public String toString() {
        return "PageMetaDTO{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", keywords='" + keywords + '\'' +
                ", abstractDescription='" + abstractDescription + '\'' +
                '}';
    }
}
