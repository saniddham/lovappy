package com.realroofers.lovappy.service.career;

import com.realroofers.lovappy.service.career.dto.VacancyDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface VacancyService {
    Page<VacancyDto> findAll(Pageable page);
    Page<VacancyDto> findByStatus(String status, Pageable page);
    Page<VacancyDto> search(String keyWord, Pageable page);
    VacancyDto findById(long id);
    String insertVacancy(VacancyDto vacancy);
    String deleteVacancy(long id);
    String saveVacancy(VacancyDto vacancy);
    String closeVacancy(long vacancyId, String status);
}
