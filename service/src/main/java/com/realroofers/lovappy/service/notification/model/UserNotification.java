package com.realroofers.lovappy.service.notification.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 6/24/2017.
 */
@Entity
@DynamicUpdate
@Table(name = "users_notification")
@AssociationOverrides({
        @AssociationOverride(name = "id.user",
                joinColumns = @JoinColumn(name = "user_id")),
        @AssociationOverride(name = "id.notification",
                joinColumns = @JoinColumn(name = "notification_id")) })
@EqualsAndHashCode
public class UserNotification {

    private UserNotificationPK id;

    @JsonIgnore
    @CreationTimestamp
    @Column(name = "created", insertable = false, updatable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date created;

    private Boolean seen;

    private Date lastSeen;

    public UserNotification() {
        created = new Date();
    }

    @EmbeddedId
    public UserNotificationPK getId(){
        return id;
    }
    public Boolean getSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }

    public Date getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }

    public void setId(UserNotificationPK id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
