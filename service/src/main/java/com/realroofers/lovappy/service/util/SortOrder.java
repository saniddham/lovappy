package com.realroofers.lovappy.service.util;

import org.springframework.data.domain.Sort;

/**
 * Created by Manoj on 06/02/2018.
 */
public class SortOrder {

    public static Sort sortByAsc(String property) {
        return new Sort(Sort.Direction.ASC, property);
    }
}
