package com.realroofers.lovappy.service.order.support;

/**
 * @author Eias Altawil
 */
public enum CouponCategory {
    GIFT("gift"), MUSIC("music"), MESSAGE("message"), AD("ad"), EVENT_ATTEND("event_attend"),
    DATING_PLACE("dating_place"), CREDITS("credits"), DAILY_QUESTION("daily_question");

    String text;

    CouponCategory(String text) {
        this.text = text;
    }
}
