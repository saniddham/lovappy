package com.realroofers.lovappy.service.career.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.StringJoiner;

public class EducationDto implements Serializable {
    private long id;
    private Integer startYear;
    private Integer startMonth;
    private Integer endYear;
    private Integer endMonth;
    private String schoolType;
    private Integer graduated;

    public EducationDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getStartYear() {
        return startYear;
    }

    public void setStartYear(Integer startYear) {
        this.startYear = startYear;
    }

    public Integer getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(Integer startMonth) {
        this.startMonth = startMonth;
    }

    public Integer getEndYear() {
        return endYear;
    }

    public void setEndYear(Integer endYear) {
        this.endYear = endYear;
    }

    public Integer getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(Integer endMonth) {
        this.endMonth = endMonth;
    }

    public String getSchoolType() {
        return schoolType;
    }

    public void setSchoolType(String schoolType) {
        this.schoolType = schoolType;
    }

    public Integer getGraduated() {
        return graduated;
    }

    public void setGraduated(Integer graduated) {
        this.graduated = graduated;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", EducationDto.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("startYear=" + startYear)
                .add("startMonth=" + startMonth)
                .add("endYear=" + endYear)
                .add("endMonth=" + endMonth)
                .add("schoolType='" + schoolType + "'")
                .add("graduated=" + graduated)
                .toString();
    }
}
