package com.realroofers.lovappy.service.survey.dto;

import com.realroofers.lovappy.service.survey.model.SurveyAnswer;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class SurveyAnswerDto {

    private Integer id;
    private String title;

    public SurveyAnswerDto(SurveyAnswer surveyAnswer) {
        this.id = surveyAnswer.getId();
        this.title = surveyAnswer.getTitle();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
