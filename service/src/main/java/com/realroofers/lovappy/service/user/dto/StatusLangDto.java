package com.realroofers.lovappy.service.user.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.system.model.Language;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.NotEmpty;

import com.realroofers.lovappy.service.user.model.UserPreference;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.support.Gender;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@EqualsAndHashCode
public class StatusLangDto {
    private Integer userId;
    private Gender gender;
    private Map<String, String> statuses;
    private Collection<LanguageDto> speakingLanguages;
    private Collection<LanguageDto> seekingLanguages;

    @NotEmpty
    private String language;

    @NotEmpty
    private String prefLanguage;


    public StatusLangDto() {

    }

    public StatusLangDto(UserProfile profile, UserPreference pref) {
        if (profile != null) {
            this.userId = profile.getUserId();
            this.gender = profile.getGender();
            if (profile.getStatuses() != null) {
                this.statuses = new HashMap<>(profile.getStatuses());
            }


            if (profile.getSpeakingLanguage() != null) {
                this.speakingLanguages = new ArrayList<>();
                this.language = "";
                for (Language language : profile.getSpeakingLanguage()) {
                    this.speakingLanguages.add(new LanguageDto(language));
                    this.language = this.language.concat(language.getId() + ",");
                }

                if(this.language.length() > 1)
                    this.language = this.language.substring(0, this.language.length() - 1);
            }
        }

        if (pref != null && pref.getSeekingLanguage() != null) {
            this.seekingLanguages = new ArrayList<>();
            this.prefLanguage = "";
            for (Language language : pref.getSeekingLanguage()) {
                this.seekingLanguages.add(new LanguageDto(language));
                this.prefLanguage = this.prefLanguage.concat(language.getId() + ",");
            }

            if(this.prefLanguage.length() > 1)
                this.prefLanguage = this.prefLanguage.substring(0, this.prefLanguage.length() - 1);
        }
    }

	public Integer getUserId() {
        return userId;
    }

    public Gender getGender() {
        return gender;
    }

    public Map<String, String> getStatuses() {
        return statuses;
    }

    public void setStatuses(Map<String, String> statuses) {
        this.statuses = statuses;
    }

    public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

    public String getLanguage() {
    	return language;
    }

    public String getPrefLanguage() {
        return prefLanguage;
    }

    public void setPrefLanguage(String prefLanguage) {
        this.prefLanguage = prefLanguage;
    }

    public Collection<LanguageDto> getSpeakingLanguages() {
        return speakingLanguages;
    }

    public void setSpeakingLanguages(Collection<LanguageDto> speakingLanguages) {
        this.speakingLanguages = speakingLanguages;
    }

    public Collection<LanguageDto> getSeekingLanguages() {
        return seekingLanguages;
    }

    public void setSeekingLanguages(Collection<LanguageDto> seekingLanguages) {
        this.seekingLanguages = seekingLanguages;
    }

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StatusLangDto that = (StatusLangDto) o;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (prefLanguage != null ? !prefLanguage.equals(that.prefLanguage) : that.prefLanguage != null) return false;
        if (language != null ? !language.equals(that.language) : that.language != null) return false;
        if (gender != that.gender) return false;
        return statuses != null ? statuses.equals(that.statuses) : that.statuses == null ;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (prefLanguage != null ? prefLanguage.hashCode() : 0);
        result = 31 * result + (language != null ? language.hashCode() : 0);
        result = 31 * result + (statuses != null ? statuses.hashCode() : 0);
        return result;
    }
}
