package com.realroofers.lovappy.service.datingPlaces.impl;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.cloud.repo.CloudStorageRepo;
import com.realroofers.lovappy.service.datingPlaces.DatingPlaceService;
import com.realroofers.lovappy.service.datingPlaces.dto.*;
import com.realroofers.lovappy.service.datingPlaces.dto.mobile.*;
import com.realroofers.lovappy.service.datingPlaces.model.*;
import com.realroofers.lovappy.service.datingPlaces.repo.*;
import com.realroofers.lovappy.service.datingPlaces.support.*;
import com.realroofers.lovappy.service.datingPlaces.support.google_places.DatingPlaceLocationUtil;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.CouponDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.util.ListMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.io.IOException;
import java.util.*;
import java.util.function.Predicate;

/**
 * Created by Darrel Rayen on 10/19/17.
 */
@Service
@Slf4j
//TODO this class need code cleanup it has many dead code(to be enhanced later)
public class DatingPlacesImpl implements DatingPlaceService {

    private final DatingPlaceRepo datingPlaceRepo;
    private final DatingPlaceReviewRepo datingPlaceReviewRepo;
    private final PlaceAtmosphereRepo placeAtmosphereRepo;
    private final CloudStorageRepo cloudStorageRepo;
    private final OrderService orderService;
    private final FoodTypeRepo foodTypeRepo;
    private final DealsRepo dealsRepo;
    private final DatingPlaceDealsRepo datingPlaceDealsRepo;
    private final DatingPlaceClaimRepo datingPlaceClaimRepo;
    private final UserRepo userRepo;
    private final PersonTypeRepo personTypeRepo;
    private final DatingPlaceLocationUtil datingPlaceLocationUtil;
    private final DatingPlacePersonTypesRepo datingPlacePersonTypesRepo;


    @Autowired
    public DatingPlacesImpl(DatingPlaceRepo datingPlaceRepo, DatingPlaceReviewRepo datingPlaceReviewRepo, PlaceAtmosphereRepo placeAtmosphereRepo, CloudStorageRepo cloudStorageRepo, OrderService orderService, FoodTypeRepo foodTypeRepo, DealsRepo dealsRepo, DatingPlaceDealsRepo datingPlaceDealsRepo, DatingPlaceLocationUtil datingPlaceLocationUtil, DatingPlaceClaimRepo datingPlaceClaimRepo, UserRepo userRepo, PersonTypeRepo personTypeRepo, DatingPlacePersonTypesRepo datingPlacePersonTypesRepo) {
        this.datingPlaceRepo = datingPlaceRepo;
        this.datingPlaceReviewRepo = datingPlaceReviewRepo;
        this.placeAtmosphereRepo = placeAtmosphereRepo;
        this.cloudStorageRepo = cloudStorageRepo;
        this.orderService = orderService;
        this.foodTypeRepo = foodTypeRepo;
        this.dealsRepo = dealsRepo;
        this.datingPlaceDealsRepo = datingPlaceDealsRepo;
        this.datingPlaceLocationUtil = datingPlaceLocationUtil;
        this.datingPlaceClaimRepo = datingPlaceClaimRepo;
        this.userRepo = userRepo;
        this.personTypeRepo = personTypeRepo;
        this.datingPlacePersonTypesRepo = datingPlacePersonTypesRepo;
    }

    @Value("${lovappy.cloud.bucket.images}")
    private String imagesBucket;

    @Override
    @Transactional(readOnly = true)
    public DatingPlaceDto findDatingPlaceById(Integer placeId) {
        return new DatingPlaceDto(datingPlaceRepo.findOne(placeId));
    }

    @Override
    public DatingPlaceDto findGoogleDatingPlaceById(String placeId) throws IOException {
        int maleCount = 0;
        int femaleCount = 0;
        int bothCount = 0;
        int allCount = 0;
        Double securityRating = 0D;
        Double parkingAccessRating = 0D;
        Double parkingRating = 0D;
        Double overAllRating = 0D;

        DatingPlaceDto datingPlaceDto = null;
        datingPlaceDto = datingPlaceLocationUtil.getGooglePlaceDetails(placeId);
        List<DatingPlaceReview> reviewList =
                datingPlaceReviewRepo.findAllByGooglePlaceIdAndReviewApprovalStatusAndReviewerType(
                        placeId, ApprovalStatus.APPROVED, "user");

        for (int x = 0; x < reviewList.size(); x++) {
            switch (reviewList.get(x).getSuitableGender()) {
                case BOTH:
                    bothCount += 1;
                    break;
                case MALE:
                    maleCount += 1;
                    break;
                case FEMALE:
                    femaleCount += 1;
                    break;
                case ALL:
                    allCount += 1;
                    break;
            }
            securityRating += reviewList.get(x).getSecurityRating();
            parkingAccessRating += reviewList.get(x).getParkingAccessRating();
            parkingRating += reviewList.get(x).getParkingRating();
            overAllRating += reviewList.get(x).getOverallRating();
        }
        datingPlaceDto.setMaleCount(maleCount);
        datingPlaceDto.setFemaleCount(femaleCount);
        datingPlaceDto.setBothCount(bothCount);
        datingPlaceDto.setAverageRating(datingPlaceReviewRepo.findAverageOverAllRatingByGooglePlaceId(placeId));
        if (securityRating != 0)
            datingPlaceDto.setSecurityAverage(securityRating / reviewList.size());
        if (parkingRating != 0)
            datingPlaceDto.setParkingAverage(parkingRating / reviewList.size());
        if (parkingAccessRating != 0)
            datingPlaceDto.setTransportAverage(parkingAccessRating / reviewList.size());
        if (overAllRating != 0)
            datingPlaceDto.setOverAllRating(overAllRating / reviewList.size());
        return datingPlaceDto;
    }

    @Override
    public DatingPlaceDto findGoogleDatingPlaceByQuery(String query) throws IOException {
        return datingPlaceLocationUtil.getGooglePlaceByText(query.replace(" ", "+"));
    }

    @Override
    @Transactional(readOnly = true)
    public DatingPlaceMobileDto findMobilePlaceById(Integer placeId) {
        List<DatingPlaceReviewMobileDto> reviewMobile = new ArrayList<>();
        DatingPlaceMobileDto datingPlaceMobileDto = new DatingPlaceMobileDto(datingPlaceRepo.findOne(placeId));
        List<DatingPlaceReview> reviews = datingPlaceReviewRepo.
                findAllByPlaceIdAndReviewApprovalStatusAndReviewerTypeList(placeId, ApprovalStatus.APPROVED);
        reviews.forEach(datingPlaceReview -> {
            reviewMobile.add(new DatingPlaceReviewMobileDto(datingPlaceReview));
        });
        datingPlaceMobileDto.setReviews(reviewMobile);
        return datingPlaceMobileDto;
    }

    @Override
    public DatingPlaceMobileDto findMobileGooglePlaceById(String placeId) throws IOException {
        int maleCount = 0;
        int femaleCount = 0;
        int bothCount = 0;
        int allCount = 0;
        Double securityRating = 0D;
        Double parkingAccessRating = 0D;
        Double parkingRating = 0D;
        Double overAllRating = 0D;

        List<DatingPlaceReviewMobileDto> reviewMobile = new ArrayList<>();
        DatingPlaceDto datingPlaceDto = datingPlaceLocationUtil.getGooglePlaceDetails(placeId);
        List<DatingPlaceReviewDto> reviewDtos = datingPlaceDto.getDatingPlaceReviewDtos();
        List<DatingPlaceReview> reviews = datingPlaceReviewRepo.
                findAllByGooglePlaceIdAndReviewApprovalStatusAndReviewerType(placeId, ApprovalStatus.APPROVED, "user");
        for (DatingPlaceReview review : reviews) {
            switch (review.getSuitableGender()) {
                case BOTH:
                    bothCount += 1;
                    break;
                case MALE:
                    maleCount += 1;
                    break;
                case FEMALE:
                    femaleCount += 1;
                    break;
                case ALL:
                    allCount += 1;
                    break;
            }
            securityRating += review.getSecurityRating();
            parkingAccessRating += review.getParkingAccessRating();
            parkingRating += review.getParkingRating();
            overAllRating += review.getOverallRating();
            reviewMobile.add(new DatingPlaceReviewMobileDto(review));
        }
        datingPlaceDto.setMaleCount(maleCount);
        datingPlaceDto.setFemaleCount(femaleCount);
        datingPlaceDto.setBothCount(bothCount);
        datingPlaceDto.setAverageRating(datingPlaceReviewRepo.findAverageOverAllRatingByGooglePlaceId(placeId));
        if (securityRating != 0)
            datingPlaceDto.setSecurityAverage(securityRating / reviews.size());
        if (parkingRating != 0)
            datingPlaceDto.setParkingAverage(parkingRating / reviews.size());
        if (parkingAccessRating != 0)
            datingPlaceDto.setTransportAverage(parkingAccessRating / reviews.size());
        if (overAllRating != 0)
            datingPlaceDto.setOverAllRating(overAllRating / reviews.size());
        reviewDtos.forEach(datingPlaceReviewDto -> {
            reviewMobile.add(new DatingPlaceReviewMobileDto(datingPlaceReviewDto));
        });
        DatingPlaceMobileDto mobileDto = new DatingPlaceMobileDto(datingPlaceDto);
        mobileDto.setReviews(reviewMobile);
        return mobileDto;
    }

    @Override
    @Transactional
    public DatingPlaceDto addDatingPlace(DatingPlaceDto datingPlaceDto) {

        DatingPlace datingPlace = new DatingPlace();
        datingPlace.setPlaceName(datingPlaceDto.getPlaceName());
        datingPlace.setZipCode(datingPlaceDto.getZipCode());
        datingPlace.setCreateEmail(datingPlaceDto.getCreateEmail());
        datingPlace.setGender(datingPlaceDto.getGender());
        datingPlace.setAgeRange(datingPlaceDto.getAgeRange());
        datingPlace.setPriceRange(datingPlaceDto.getPriceRange());
        datingPlace.setAddressLine(datingPlaceDto.getAddressLine());
        datingPlace.setAgeRange(datingPlaceDto.getAgeRange());
        datingPlace.setCity(datingPlaceDto.getCity());
        datingPlace.setState(datingPlaceDto.getState());
        datingPlace.setCountry(datingPlaceDto.getCountry());
        datingPlace.setAddressLineTwo(datingPlaceDto.getAddressLineTwo());
        datingPlace.setLatitude(datingPlaceDto.getLatitude());
        datingPlace.setLongitude(datingPlaceDto.getLongitude());
        datingPlace.setFeatured(false);
        datingPlace.setPlaceApprovalStatus(ApprovalStatus.PENDING);
        datingPlace.setGoodForMarriedCouple(datingPlaceDto.getIsGoodForMarriedCouple());

        if (datingPlaceDto.getGender().equals(Gender.MALE)) {
            datingPlace.setSuitableForMaleCount(1);
            datingPlace.setSuitableForFemale(0);
            datingPlace.setSuitableForBoth(0);
        } else if (datingPlaceDto.getGender().equals(Gender.FEMALE)) {
            datingPlace.setSuitableForFemale(1);
            datingPlace.setSuitableForMaleCount(0);
            datingPlace.setSuitableForBoth(0);
        } else if (datingPlaceDto.getGender().equals(Gender.BOTH)) {
            datingPlace.setSuitableForBoth(1);
            datingPlace.setSuitableForMaleCount(0);
            datingPlace.setSuitableForFemale(0);
        } else if (datingPlaceDto.getGender().equals(Gender.ALL)) {
            datingPlace.setSuitableForMaleCount(1);
            datingPlace.setSuitableForFemale(1);
            datingPlace.setSuitableForBoth(1);
        }

        if (datingPlaceDto.getCoverPicture().getId() != null) {
            CloudStorageFile image = cloudStorageRepo.findOne(datingPlaceDto.getCoverPicture().getId());
            datingPlace.setCoverPicture(image);
        }

        if (datingPlaceDto.getPlacesAttachmentDtos().size() > 0 && (datingPlaceDto.getPlacesAttachmentDtos().get(0).getPicture().getId() != null)) {
            PlacesAttachment placesAttachment = datingPlaceDto.getPlacesAttachmentDtos().get(0).getPlacesAttachment();
            placesAttachment.setPicture(cloudStorageRepo.findOne(datingPlaceDto.getPlacesAttachmentDtos().get(0).getPicture().getId()));
            placesAttachment.setUrl(cloudStorageRepo.findOne(datingPlaceDto.getPlacesAttachmentDtos().get(0).getPicture().getId()).getUrl());
            datingPlace.getPlacesAttachments().add(placesAttachment);
            placesAttachment.setPlaceId(datingPlace);
        }

        DatingPlaceReview datingPlaceReview = datingPlaceDto.getDatingPlaceReviewDtos().get(0).getDatingPlaceReview();
        datingPlaceReview.setSuitableAgeRange(datingPlaceDto.getAgeRange());
        datingPlaceReview.setSuitableGender(datingPlaceDto.getGender());
        datingPlaceReview.setSuitablePriceRange(datingPlaceDto.getPriceRange());
        datingPlaceReview.setGoodForCouple(datingPlaceDto.getIsGoodForMarriedCouple());


        if (datingPlaceDto.getUserType().equalsIgnoreCase("reviewer")) {
            if (datingPlaceDto.getDatingPlaceReviewDtos().size() > 0 && (datingPlaceDto.getDatingPlaceReviewDtos().get(0).getReviewerEmail() != null)) {
                Double security = datingPlaceDto.getDatingPlaceReviewDtos().get(0).getDatingPlaceReview().getSecurityRating();
                Double parking = datingPlaceDto.getDatingPlaceReviewDtos().get(0).getDatingPlaceReview().getParkingRating();
                Double parkingAccess = datingPlaceDto.getDatingPlaceReviewDtos().get(0).getDatingPlaceReview().getParkingAccessRating();

                datingPlaceReview.setAverageRating(getAverageRatingPerReview(security, parking, parkingAccess));
                datingPlace.getDatingPlaceReviews().add(datingPlaceReview);
                datingPlace.setAverageRating(getAverageRatingPerReview(security, parking, parkingAccess));
                datingPlace.setSecurityAverage(security);
                datingPlace.setParkingAverage(parking);
                datingPlace.setTransportAverage(parkingAccess);
                datingPlaceReview.setPlaceId(datingPlace);
            }

        } else if (datingPlaceDto.getUserType().equalsIgnoreCase("manager")) {
            //Todo replace with atmosphere ID
            String[] atmosphereList = datingPlaceDto.getPlaceAtmospheres().split(",");
            for (String atmo : atmosphereList) {
                PlaceAtmosphere atmosphere = placeAtmosphereRepo.findByAtmosphereNameAndEnabledTrue(atmo);
                DatingPlaceAtmospheres placeAtmospheres = new DatingPlaceAtmospheres();
                placeAtmospheres.setAtmosphere(atmosphere);
                placeAtmospheres.setPlaceId(datingPlace);
                datingPlace.getPlaceAtmospheres().add(placeAtmospheres);
            }
            String[] foodTypesList = datingPlaceDto.getFoodTypes().split(",");
            for (String foodType : foodTypesList) {
                FoodType foodTypes = foodTypeRepo.findByFoodTypeNameAndEnabledTrue(foodType);
                DatingPlaceFoodTypes datingPlaceFoodTypes = new DatingPlaceFoodTypes(datingPlace, foodTypes);
                datingPlace.getFoodTypes().add(datingPlaceFoodTypes);
            }
            datingPlace.getDatingPlaceReviews().add(datingPlaceReview);
            datingPlaceReview.setPlaceId(datingPlace);
            datingPlace.setNeighborHood(datingPlaceDto.getNeighborHood());
            datingPlace.setPlaceDescription(datingPlaceDto.getPlaceDescription());
            datingPlace.setPetsAllowed(datingPlaceDto.getIsPetsAllowed());
        }
        return new DatingPlaceDto(datingPlaceRepo.save(datingPlace));
    }

    @Override
    @Transactional
    public DatingPlaceMobileDto addDatingPlaceMobile(AddDatingPlaceMobileDto datingPlaceMobileDto) {
        DatingPlace datingPlace = new DatingPlace();
        datingPlace.setPlaceName(datingPlaceMobileDto.getPlaceName());
        datingPlace.setZipCode(datingPlaceMobileDto.getZipCode());
        datingPlace.setCreateEmail(datingPlaceMobileDto.getCreateEmail());
        datingPlace.setGender(datingPlaceMobileDto.getGender());
        datingPlace.setAgeRange(datingPlaceMobileDto.getAgeRange());
        datingPlace.setPriceRange(datingPlaceMobileDto.getPriceRange());
        datingPlace.setAddressLine(datingPlaceMobileDto.getAddressLine());
        datingPlace.setAgeRange(datingPlaceMobileDto.getAgeRange());
        datingPlace.setCity(datingPlaceMobileDto.getCity());
        datingPlace.setState(datingPlaceMobileDto.getState());
        datingPlace.setCountry(datingPlaceMobileDto.getCountry());
        datingPlace.setAddressLineTwo(datingPlaceMobileDto.getAddressLineTwo());
        datingPlace.setLatitude(datingPlaceMobileDto.getLatitude());
        datingPlace.setLongitude(datingPlaceMobileDto.getLongitude());
        datingPlace.setFeatured(false);
        datingPlace.setClaimed(false);
        datingPlace.setPlaceApprovalStatus(ApprovalStatus.PENDING);
        datingPlace.setGoodForMarriedCouple(datingPlaceMobileDto.getIsGoodForMarriedCouple());

        switch (datingPlaceMobileDto.getGender()) {
            case MALE:
                datingPlace.setSuitableForMaleCount(1);
                datingPlace.setSuitableForFemale(0);
                datingPlace.setSuitableForBoth(0);
                break;
            case FEMALE:
                datingPlace.setSuitableForFemale(1);
                datingPlace.setSuitableForMaleCount(0);
                datingPlace.setSuitableForBoth(0);
                break;
            case BOTH:
                datingPlace.setSuitableForBoth(1);
                datingPlace.setSuitableForMaleCount(0);
                datingPlace.setSuitableForFemale(0);
                break;
            case ALL:
                datingPlace.setSuitableForMaleCount(1);
                datingPlace.setSuitableForFemale(1);
                datingPlace.setSuitableForBoth(1);
                break;

        }

/*        if (datingPlaceMobileDto.getCoverPicture().getId() != null) {
            CloudStorageFile image = cloudStorageRepo.findOne(datingPlaceMobileDto.getCoverPicture().getId());
            datingPlace.setCoverPicture(image);
        }*/

/*        if (datingPlaceMobileDto.getPlacesAttachmentDtos().size() > 0 && (datingPlaceMobileDto.getPlacesAttachmentDtos().get(0).getPicture().getId() != null)) {
            PlacesAttachment placesAttachment = datingPlaceMobileDto.getPlacesAttachmentDtos().get(0).getPlacesAttachment();
            placesAttachment.setPicture(cloudStorageRepo.findOne(datingPlaceMobileDto.getPlacesAttachmentDtos().get(0).getPicture().getId()));
            placesAttachment.setUrl(cloudStorageRepo.findOne(datingPlaceMobileDto.getPlacesAttachmentDtos().get(0).getPicture().getId()).getUrl());
            datingPlace.getPlacesAttachments().add(placesAttachment);
            placesAttachment.setPlaceId(datingPlace);
        }*/

        DatingPlaceReview datingPlaceReview = datingPlaceMobileDto.getDatingPlaceReviews().get(0).convertToDatingPlaceReview();
        datingPlaceReview.setSuitableAgeRange(datingPlaceMobileDto.getAgeRange());
        datingPlaceReview.setSuitableGender(datingPlaceMobileDto.getGender());
        datingPlaceReview.setSuitablePriceRange(datingPlaceMobileDto.getPriceRange());
        datingPlaceReview.setGoodForCouple(datingPlaceMobileDto.getIsGoodForMarriedCouple());


        if (datingPlaceMobileDto.getUserType().equalsIgnoreCase("reviewer")) {
            if (datingPlaceMobileDto.getDatingPlaceReviews().size() > 0 && (datingPlaceMobileDto.getDatingPlaceReviews().get(0).getReviewerEmail() != null)) {
                Double security = datingPlaceMobileDto.getDatingPlaceReviews().get(0).convertToDatingPlaceReview().getSecurityRating();
                Double parking = datingPlaceMobileDto.getDatingPlaceReviews().get(0).convertToDatingPlaceReview().getParkingRating();
                Double parkingAccess = datingPlaceMobileDto.getDatingPlaceReviews().get(0).convertToDatingPlaceReview().getParkingAccessRating();

                datingPlaceReview.setAverageRating(getAverageRatingPerReview(security, parking, parkingAccess));
                datingPlace.getDatingPlaceReviews().add(datingPlaceReview);
                datingPlace.setAverageRating(getAverageRatingPerReview(security, parking, parkingAccess));
                datingPlace.setSecurityAverage(security);
                datingPlace.setParkingAverage(parking);
                datingPlace.setTransportAverage(parkingAccess);
                datingPlaceReview.setPlaceId(datingPlace);
            }

        } else if (datingPlaceMobileDto.getUserType().equalsIgnoreCase("manager")) {
            Collection<PlaceAtmosphereDto> placeAtmosphereDtos = datingPlaceMobileDto.getAtmospheres();
            placeAtmosphereDtos.forEach(placeAtmosphereDto -> {
                PlaceAtmosphere atmospheres = placeAtmosphereRepo.findByAtmosphereNameAndEnabledTrue(placeAtmosphereDto.getAtmosphereName());
                DatingPlaceAtmospheres placeAtmospheres = new DatingPlaceAtmospheres();
                placeAtmospheres.setAtmosphere(atmospheres);
                placeAtmospheres.setPlaceId(datingPlace);
                datingPlace.getPlaceAtmospheres().add(placeAtmospheres);
            });

            Collection<FoodTypeDto> foodTypeDtos = datingPlaceMobileDto.getFoodTypes();
            foodTypeDtos.forEach(foodTypeDto -> {
                FoodType foodTypes = foodTypeRepo.findByFoodTypeNameAndEnabledTrue(foodTypeDto.getFoodTypeName());
                DatingPlaceFoodTypes datingPlaceFoodTypes = new DatingPlaceFoodTypes(datingPlace, foodTypes);
                datingPlace.getFoodTypes().add(datingPlaceFoodTypes);
            });
            //datingPlace.getDatingPlaceReviews().add(datingPlaceReview);
            //datingPlaceReview.setPlaceId(datingPlace);
            datingPlace.setNeighborHood(datingPlaceMobileDto.getNeighborHood());
            datingPlace.setPlaceDescription(datingPlaceMobileDto.getPlaceDescription());
            datingPlace.setPetsAllowed(datingPlaceMobileDto.getIsPetsAllowed());
        }

        return new DatingPlaceMobileDto(datingPlaceRepo.save(datingPlace));
    }

    @Override
    @Transactional
    public DatingPlaceDto addImagesToPlace(List<PlacesAttachmentDto> placesAttachmentDtos) {
        DatingPlace datingPlace = datingPlaceRepo.findOne(placesAttachmentDtos.get(0).getPlaceId().getDatingPlacesId());
        List<PlacesAttachment> placesAttachments = new ArrayList<>();
        placesAttachmentDtos.forEach(placesAttachmentDto -> {
            PlacesAttachment placesAttachment = new PlacesAttachment();
            placesAttachment.setPicture(cloudStorageRepo.findOne(placesAttachmentDto.getPicture().getId()));
            placesAttachment.setUrl(placesAttachmentDto.getPicture().getUrl());
            placesAttachment.setPlaceId(datingPlace);
            placesAttachments.add(placesAttachment);
        });
        datingPlace.getPlacesAttachments().addAll(placesAttachments);
        return new DatingPlaceDto(datingPlaceRepo.saveAndFlush(datingPlace));
    }

    @Override
    @Transactional
    public OrderDto addFeaturedDatingPlace(DatingPlaceDto datingPlaceDto, String coupon, PaymentMethodType paymentMethodType) {

        DatingPlace datingPlace = new DatingPlace();
        datingPlace.setPlaceName(datingPlaceDto.getPlaceName());
        datingPlace.setZipCode(datingPlaceDto.getZipCode());
        datingPlace.setCreateEmail(datingPlaceDto.getCreateEmail());
        datingPlace.setGender(datingPlaceDto.getGender());
        datingPlace.setAgeRange(datingPlaceDto.getAgeRange());
        datingPlace.setPriceRange(datingPlaceDto.getPriceRange());
        datingPlace.setAddressLine(datingPlaceDto.getAddressLine());
        datingPlace.setAgeRange(datingPlaceDto.getAgeRange());
        datingPlace.setCity(datingPlaceDto.getCity());
        datingPlace.setState(datingPlaceDto.getState());
        datingPlace.setCountry(datingPlaceDto.getCountry());
        datingPlace.setAddressLineTwo(datingPlaceDto.getAddressLineTwo());
        datingPlace.setLatitude(datingPlaceDto.getLatitude());
        datingPlace.setLongitude(datingPlaceDto.getLongitude());
        datingPlace.setFeatured(datingPlaceDto.getIsMapFeatured() || datingPlaceDto.getIsLovStampFeatured());
        datingPlace.setPetsAllowed(datingPlaceDto.getIsPetsAllowed());
        datingPlace.setGoodForMarriedCouple(datingPlaceDto.getIsGoodForMarriedCouple());
        datingPlace.setPlaceApprovalStatus(ApprovalStatus.PENDING);
        datingPlace.setPlaceDescription(datingPlaceDto.getPlaceDescription());

        if (datingPlaceDto.getGender().equals(Gender.MALE)) {
            datingPlace.setSuitableForMaleCount(1);
            datingPlace.setSuitableForFemale(0);
            datingPlace.setSuitableForBoth(0);

        } else if (datingPlaceDto.getGender().equals(Gender.FEMALE)) {
            datingPlace.setSuitableForFemale(1);
            datingPlace.setSuitableForMaleCount(0);
            datingPlace.setSuitableForBoth(0);

        } else if (datingPlaceDto.getGender().equals(Gender.BOTH)) {
            datingPlace.setSuitableForBoth(1);
            datingPlace.setSuitableForMaleCount(0);
            datingPlace.setSuitableForFemale(0);
        } else if (datingPlaceDto.getGender().equals(Gender.ALL)) {
            datingPlace.setSuitableForBoth(1);
            datingPlace.setSuitableForMaleCount(1);
            datingPlace.setSuitableForFemale(1);
        }

        String[] atmosphereList = datingPlaceDto.getPlaceAtmospheres().split(",");
        for (String atmo : atmosphereList) {
            PlaceAtmosphere atmospheres = placeAtmosphereRepo.findByAtmosphereName(atmo);
            DatingPlaceAtmospheres placeAtmospheres = new DatingPlaceAtmospheres();
            placeAtmospheres.setAtmosphere(atmospheres);
            placeAtmospheres.setPlaceId(datingPlace);
            datingPlace.getPlaceAtmospheres().add(placeAtmospheres);
        }

        String[] foodTypesList = datingPlaceDto.getFoodTypes().split(",");
        for (String foodType : foodTypesList) {
            FoodType foodTypes = foodTypeRepo.findByFoodTypeNameAndEnabledTrue(foodType);
            DatingPlaceFoodTypes datingPlaceFoodTypes = new DatingPlaceFoodTypes(datingPlace, foodTypes);
            datingPlace.getFoodTypes().add(datingPlaceFoodTypes);
        }

        if (datingPlaceDto.getCoverPicture().getId() != null) {
            CloudStorageFile image = cloudStorageRepo.findOne(datingPlaceDto.getCoverPicture().getId());
            datingPlace.setCoverPicture(image);
        }

        if (datingPlaceDto.getPlacesAttachmentDtos().size() > 0 && (datingPlaceDto.getPlacesAttachmentDtos().get(0).getPicture().getId() != null)) {
            PlacesAttachment placesAttachment = datingPlaceDto.getPlacesAttachmentDtos().get(0).getPlacesAttachment();
            placesAttachment.setPicture(cloudStorageRepo.findOne(datingPlaceDto.getPlacesAttachmentDtos().get(0).getPicture().getId()));
            placesAttachment.setUrl(cloudStorageRepo.findOne(datingPlaceDto.getPlacesAttachmentDtos().get(0).getPicture().getId()).getUrl());
            datingPlace.getPlacesAttachments().add(placesAttachment);
            placesAttachment.setPlaceId(datingPlace);
        }

        datingPlace.setMapFeatured(datingPlaceDto.getIsMapFeatured());
        datingPlace.setLoveStampFeatured(datingPlaceDto.getIsLovStampFeatured());

        CalculatePriceDto calculatePrice = calculatePrice(datingPlaceDto, coupon);

        return orderService.addOrder(datingPlace, OrderDetailType.DATING_PLACE, calculatePrice.getPrice(),
                1, null, coupon, paymentMethodType);
    }

    @Override
    @Transactional
    public DatingPlaceDto updateDatingPlace(DatingPlaceDto datingPlaceDto) {
        DatingPlace datingPlace = datingPlaceRepo.findOne(Integer.valueOf(datingPlaceDto.getDatingPlacesId()));

        if (!datingPlaceDto.getPlaceName().isEmpty()) {
            datingPlace.setPlaceName(datingPlaceDto.getPlaceName());
        }
        if (!datingPlaceDto.getCity().isEmpty()) {
            datingPlace.setCity(datingPlaceDto.getCity());
        }
        if (!datingPlaceDto.getCountry().isEmpty()) {
            datingPlace.setCountry(datingPlaceDto.getCountry());
        }
        if (!datingPlaceDto.getState().isEmpty()) {
            datingPlace.setState(datingPlaceDto.getState());
        }
        if (datingPlaceDto.getGender() != null) {
            datingPlace.setGender(datingPlaceDto.getGender());
        }
        if (datingPlaceDto.getPriceRange() != null) {
            datingPlace.setPriceRange(datingPlaceDto.getPriceRange());
        }
        if (datingPlaceDto.getAgeRange() != null) {
            datingPlace.setAgeRange(datingPlaceDto.getAgeRange());
        }
        if (!datingPlaceDto.getAddressLine().isEmpty()) {
            datingPlace.setAddressLine(datingPlaceDto.getAddressLine());
        }
        if (!datingPlaceDto.getAddressLineTwo().isEmpty()) {
            datingPlace.setAddressLineTwo(datingPlaceDto.getAddressLineTwo());
        }
        if (datingPlaceDto.getPlaceAtmospheres() != null && !datingPlaceDto.getPlaceAtmospheres().isEmpty()) {
            String[] atmosphereList = datingPlaceDto.getPlaceAtmospheres().split(",");
            for (String atmo : atmosphereList) {
                PlaceAtmosphere atmospheres = placeAtmosphereRepo.findByAtmosphereNameAndEnabledTrue(atmo);
                DatingPlaceAtmospheres placeAtmospheres = new DatingPlaceAtmospheres();
                placeAtmospheres.setAtmosphere(atmospheres);
                placeAtmospheres.setPlaceId(datingPlace);
                datingPlace.getPlaceAtmospheres().add(placeAtmospheres);
            }
        }
        if (datingPlaceDto.getFoodTypes() != null && !datingPlaceDto.getFoodTypes().isEmpty()) {
            String[] foodTypesList = datingPlaceDto.getFoodTypes().split(",");
            for (String foodType : foodTypesList) {
                FoodType foodTypes = foodTypeRepo.findByFoodTypeNameAndEnabledTrue(foodType);
                DatingPlaceFoodTypes datingPlaceFoodTypes = new DatingPlaceFoodTypes(datingPlace, foodTypes);
                datingPlace.getFoodTypes().add(datingPlaceFoodTypes);
            }

        }
        return new DatingPlaceDto(datingPlaceRepo.saveAndFlush(datingPlace));
    }

    @Override
    @Transactional
    public DatingPlaceDto updateDatingPlaceByOwner(DatingPlaceDto datingPlaceDto) {
        DatingPlace datingPlace = datingPlaceRepo.findOne(Integer.parseInt(datingPlaceDto.getDatingPlacesId()));
        datingPlace.setPlaceName(datingPlaceDto.getPlaceName());
        datingPlace.setZipCode(datingPlaceDto.getZipCode());
        datingPlace.setGender(datingPlaceDto.getGender());
        datingPlace.setAgeRange(datingPlaceDto.getAgeRange());
        datingPlace.setPriceRange(datingPlaceDto.getPriceRange());
        datingPlace.setAddressLine(datingPlaceDto.getAddressLine());
        datingPlace.setAgeRange(datingPlaceDto.getAgeRange());
        datingPlace.setCity(datingPlaceDto.getCity());
        datingPlace.setState(datingPlaceDto.getState());
        datingPlace.setCountry(datingPlaceDto.getCountry());
        if (datingPlaceDto.getAddressLineTwo() != null && !datingPlaceDto.getAddressLineTwo().isEmpty()) {
            datingPlace.setAddressLineTwo(datingPlaceDto.getAddressLineTwo());
        }
        datingPlace.setLatitude(datingPlaceDto.getLatitude());
        datingPlace.setLongitude(datingPlaceDto.getLongitude());
        if (datingPlaceDto.getIsGoodForMarriedCouple() != null) {
            datingPlace.setGoodForMarriedCouple(datingPlaceDto.getIsGoodForMarriedCouple());
        }
        if (datingPlaceDto.getPlaceAtmospheres() != null && !datingPlaceDto.getPlaceAtmospheres().isEmpty()) {
            String[] atmosphereList = datingPlaceDto.getPlaceAtmospheres().split(",");
            for (String atmo : atmosphereList) {
                PlaceAtmosphere atmosphere = placeAtmosphereRepo.findByAtmosphereNameAndEnabledTrue(atmo);
                DatingPlaceAtmospheres placeAtmospheres = new DatingPlaceAtmospheres();
                placeAtmospheres.setAtmosphere(atmosphere);
                placeAtmospheres.setPlaceId(datingPlace);
                datingPlace.getPlaceAtmospheres().add(placeAtmospheres);
            }
        }
        if (datingPlaceDto.getFoodTypes() != null && !datingPlaceDto.getFoodTypes().isEmpty()) {
            String[] foodTypesList = datingPlaceDto.getFoodTypes().split(",");
            for (String foodType : foodTypesList) {
                FoodType foodTypes = foodTypeRepo.findByFoodTypeNameAndEnabledTrue(foodType);
                DatingPlaceFoodTypes datingPlaceFoodTypes = new DatingPlaceFoodTypes(datingPlace, foodTypes);
                datingPlace.getFoodTypes().add(datingPlaceFoodTypes);
            }
        }
        return new DatingPlaceDto(datingPlaceRepo.saveAndFlush(datingPlace));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DatingPlaceDto> getAllPlaces(Pageable pageable) {

        Page<DatingPlace> datingPlaces = datingPlaceRepo.findAllByOrderByDatingPlacesIdDesc(pageable);
        return datingPlaces.map(DatingPlaceDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DatingPlaceDto> getAllNearByPlaces(Double latitude, Double longitude) {
        List<DatingPlaceDto> datingPlaceDtos = new ArrayList<>();
        try {
            datingPlaceDtos.addAll(datingPlaceLocationUtil.getGooglePlacesListByLocation(latitude, longitude));
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<DatingPlace> datingPlaces = datingPlaceRepo.findNearByDatingPlacesByLatitudeAndLongitudeList(latitude, longitude);
        datingPlaces.forEach(datingPlace -> {
            datingPlaceDtos.add(new DatingPlaceDto(datingPlace));
        });
        return datingPlaceDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DatingPlaceDto> getAllPlacesByRating(Boolean isDescending, Double latitude, Double longitude, Pageable pageable) throws IOException {
        Page<DatingPlaceDto> datingPlaceDtos;
        LocationDto locationDto = new LocationDto(latitude, longitude);
        if (isDescending) {
            datingPlaceDtos = getGoogleResultsCombinedNew(datingPlaceRepo.findNearByDatingPlacesByLatitudeAndLongitude(latitude, longitude, pageable).map(DatingPlaceDto::new), pageable, locationDto, true);
        } else {
            datingPlaceDtos = getGoogleResultsCombinedNew(datingPlaceRepo.findNearByDatingPlacesByLatitudeAndLongitudeAsc(latitude, longitude, pageable).map(DatingPlaceDto::new), pageable, locationDto, false);
        }
        return datingPlaceDtos;
    }

    @Override
    public List<DatingPlaceDto> getAllPlacesListByRating(Boolean isDescending, Double latitude, Double longitude) throws IOException {
        List<DatingPlace> datingPlaces;
        List<DatingPlaceDto> datingPlaceDtos = new ArrayList<>();
        if (isDescending) {
            datingPlaces = datingPlaceRepo.findNearByDatingPlacesByLatitudeAndLongitudeList(latitude, longitude);
            datingPlaces.forEach(datingPlace -> {
                datingPlaceDtos.add(new DatingPlaceDto(datingPlace));
            });
            datingPlaceDtos.addAll(datingPlaceLocationUtil.getGooglePlacesListByLocationAndRating(latitude, longitude, 50000D, true));
        } else {
            datingPlaces = datingPlaceRepo.findNearByDatingPlacesByLatitudeAndLongitudeListAsc(latitude, longitude);
            datingPlaces.forEach(datingPlace -> {
                datingPlaceDtos.add(new DatingPlaceDto(datingPlace));
            });
            datingPlaceDtos.addAll(datingPlaceLocationUtil.getGooglePlacesListByLocationAndRating(latitude, longitude, 50000D, false));
        }
        return datingPlaceDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public List<DatingPlaceMobileDto> getAllMobileNearByPlaces(Double latitude, Double longitude) throws IOException {
        List<DatingPlaceDto> datingPlaceDtos = new ArrayList<>();
        List<DatingPlaceMobileDto> mobileDtoList = new ArrayList<>();
        datingPlaceDtos.addAll(datingPlaceLocationUtil.getGooglePlacesListByLocation(latitude, longitude));
        datingPlaceDtos.forEach(datingPlaceDto -> {
            mobileDtoList.add(new DatingPlaceMobileDto(datingPlaceDto));
        });
        List<DatingPlace> datingPlaces = datingPlaceRepo.findNearByDatingPlacesByLatitudeAndLongitudeList(latitude, longitude);
        datingPlaces.forEach(datingPlace -> {
            mobileDtoList.add(new DatingPlaceMobileDto(datingPlace));
        });
        return mobileDtoList;
    }

    @Override
    @Transactional(readOnly = true)
    public List<DatingPlaceDto> getAllNearByPlacesByNameAndLocation(Double latitude, Double longitude, String placeName) {
        List<DatingPlaceDto> datingPlaceDtos = new ArrayList<>();
        try {
            datingPlaceDtos.addAll(datingPlaceLocationUtil.getGooglePlacesListByLocationAndText(5000D, placeName.replace(" ", "+"), latitude, longitude));
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<DatingPlace> datingPlaces = datingPlaceRepo.findNearByDatingPlacesByLatitudeAndLongitudeList(latitude, longitude);
        datingPlaces.forEach(datingPlace -> {
            //datingPlaceDtos.add(new DatingPlaceDto(datingPlace));
        });
        return datingPlaceDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public List<DatingPlaceDto> getAllNearByPlacesByCityAndRadius(Double latitude, Double longitude, Double radius, String cityName) {
        List<DatingPlaceDto> datingPlaceDtos = new ArrayList<>();
        try {
            datingPlaceDtos.addAll(datingPlaceLocationUtil.getGooglePlacesListByLocationAndText(radius * 1609.34, cityName.replace(" ", "+")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<DatingPlace> datingPlaces = datingPlaceRepo.findNearByDatingPlacesByRadiusList(latitude, longitude, radius, cityName);
        datingPlaces.forEach(datingPlace -> {
            //datingPlaceDtos.add(new DatingPlaceDto(datingPlace));
        });
        return datingPlaceDtos;
    }

    @Override
    public Boolean addDatingLocationPictures(List<CloudStorageFileDto> datingLocationPictures) {
        return null;
    }


    @Override
    @Transactional
    public DatingPlaceReviewDto addDatingPlaceReview(DatingPlaceReviewDto datingPlaceReviewDto, DatingPlaceDto datingPlaceDto) throws IOException {
        DatingPlace datingPlace = datingPlaceReviewDto.getPlaceId();
        DatingPlaceReview datingPlaceReview = new DatingPlaceReview();
        datingPlaceReview.setReviewerComment(datingPlaceReviewDto.getReviewerComment());
        datingPlaceReview.setReviewerEmail(datingPlaceReviewDto.getReviewerEmail());
        datingPlaceReview.setSecurityRating(datingPlaceReviewDto.getSecurityRating());
        datingPlaceReview.setParkingRating(datingPlaceReviewDto.getParkingRating());
        datingPlaceReview.setParkingAccessRating(datingPlaceReviewDto.getParkingAccessRating());
        datingPlaceReview.setOverallRating(datingPlaceReviewDto.getOverAllRating());
        datingPlaceReview.setSuitablePriceRange(datingPlaceReviewDto.getSuitablePriceRange());
        datingPlaceReview.setSuitableGender(datingPlaceReviewDto.getSuitableGender());
        datingPlaceReview.setSuitableAgeRange(datingPlaceReviewDto.getSuitableAgeRange());
        datingPlaceReview.setGoodForCouple(datingPlaceReviewDto.getGoodForCouple());
        datingPlaceReview.setAverageRating(getAverageRatingPerReview(datingPlaceReviewDto.getSecurityRating(), datingPlaceReviewDto.getParkingRating(), datingPlaceReviewDto.getParkingAccessRating()));
        datingPlaceReview.setReviewApprovalStatus(ApprovalStatus.PENDING);
        datingPlaceReview.setReviewerType("user");
        datingPlaceReview.setPlaceId(datingPlace);

        if (datingPlaceReviewDto.getGooglePlaceId() != null) {
            datingPlaceReview.setGooglePlaceId(datingPlaceReviewDto.getGooglePlaceId());
            //save location to database
            datingPlace = datingPlaceRepo.findByExternalPlaceIdAndProvider(datingPlaceReviewDto.getGooglePlaceId(), DatingPlaceProvider.GOOGLE);
            if (datingPlace == null) {
                datingPlaceDto.setAgeRange(datingPlaceReviewDto.getSuitableAgeRange());
                datingPlaceDto.setGender(datingPlaceReviewDto.getSuitableGender());
                datingPlaceDto.setIsGoodForMarriedCouple(datingPlaceReviewDto.getGoodForCouple());
                datingPlaceDto.setPriceRange(datingPlaceReviewDto.getSuitablePriceRange());
                datingPlace = fromGoogleDto2Entity(datingPlaceDto);
                datingPlaceReview.setPlaceId(datingPlaceRepo.save(datingPlace));
            }
            datingPlaceReview.setPlaceName(datingPlaceDto.getPlaceName());
        }

        if (datingPlaceReviewRepo.findFirst1ByReviewerEmail(datingPlaceReviewDto.getReviewerEmail()) != null) {
            datingPlaceReview.setReviewerId(datingPlaceReviewRepo.findFirst1ByReviewerEmail(datingPlaceReviewDto.getReviewerEmail()).getReviewerId());
        } else {
            datingPlaceReview.setReviewerId(DatingPlaceUtil.generateRandomId(8));
        }
        DatingPlaceReview review = datingPlaceReviewRepo.saveAndFlush(datingPlaceReview);
        if (datingPlaceReviewDto.getPersonTypes() != null && !datingPlaceReviewDto.getPersonTypes().isEmpty() && datingPlace != null) {
            for (Integer personTypeId : datingPlaceReviewDto.getPersonTypes()) {
                PersonType type = personTypeRepo.findOne(personTypeId);
                DatingPlacePersonTypeId id = new DatingPlacePersonTypeId(datingPlace.getDatingPlacesId(), personTypeId);
                DatingPlacePersonTypes placePersonTypes = new DatingPlacePersonTypes(datingPlace, type);
                placePersonTypes.setId(id);
                datingPlacePersonTypesRepo.save(placePersonTypes);
            }
        }
        // Update datingplace review stats
        updateDatingPlaceStats(datingPlace, datingPlaceReviewDto.getSuitableGender());
        return new DatingPlaceReviewDto(review);
    }

    @Override
    @Transactional
    public DatingPlaceReviewMobileDto addDatingPlaceReviewMobile(AddDatingPlaceReviewMobileDto placeReviewMobileDto, DatingPlaceDto datingPlaceDto) {
        DatingPlaceReview datingPlaceReview = new DatingPlaceReview();
        DatingPlace datingPlace = null;
        if (StringUtils.isNumeric(placeReviewMobileDto.getPlaceId())) {
            datingPlace = datingPlaceRepo.findOne(Integer.parseInt(placeReviewMobileDto.getPlaceId()));

            datingPlaceReview.setPlaceName(datingPlace.getPlaceName());
            if (placeReviewMobileDto.getPersonTypes() != null) {
                for (PersonTypeDto personType : placeReviewMobileDto.getPersonTypes()) {
                    PersonType type = personTypeRepo.findOne(personType.getId());
                    DatingPlacePersonTypeId id = new DatingPlacePersonTypeId(datingPlace.getDatingPlacesId(), type.getId());
                    DatingPlacePersonTypes placePersonTypes = new DatingPlacePersonTypes(datingPlace, type);
                    placePersonTypes.setId(id);
                    datingPlacePersonTypesRepo.save(placePersonTypes);
                }
            }
        } else {
            datingPlaceReview.setGooglePlaceId(placeReviewMobileDto.getPlaceId());
            datingPlaceReview.setPlaceName(datingPlaceDto.getPlaceName());
            datingPlaceReview.setGooglePlaceId(placeReviewMobileDto.getGooglePlaceId());
            //save google location to database
            datingPlace = datingPlaceRepo.findByExternalPlaceIdAndProvider(placeReviewMobileDto.getGooglePlaceId(), DatingPlaceProvider.GOOGLE);
            if (datingPlace == null) {
                datingPlaceDto.setAgeRange(placeReviewMobileDto.getSuitableAgeRange());
                datingPlaceDto.setGender(placeReviewMobileDto.getSuitableGender());
                datingPlaceDto.setIsGoodForMarriedCouple(placeReviewMobileDto.getIsGoodForCouple());
                datingPlaceDto.setPriceRange(placeReviewMobileDto.getSuitablePriceRange());
                datingPlace = fromGoogleDto2Entity(datingPlaceDto);
                datingPlace = datingPlaceRepo.save(datingPlace);
            }
        }
        datingPlaceReview.setPlaceId(datingPlace);
        datingPlaceReview.setReviewerComment(placeReviewMobileDto.getReviewerComment());
        datingPlaceReview.setReviewerEmail(placeReviewMobileDto.getReviewerEmail());
        datingPlaceReview.setSecurityRating(placeReviewMobileDto.getSecurityRating());
        datingPlaceReview.setParkingRating(placeReviewMobileDto.getParkingRating());
        datingPlaceReview.setParkingAccessRating(placeReviewMobileDto.getParkingAccessRating());
        datingPlaceReview.setOverallRating(placeReviewMobileDto.getOverAllRating());
        datingPlaceReview.setSuitablePriceRange(placeReviewMobileDto.getSuitablePriceRange());
        datingPlaceReview.setSuitableGender(placeReviewMobileDto.getSuitableGender());
        datingPlaceReview.setSuitableAgeRange(placeReviewMobileDto.getSuitableAgeRange());
        datingPlaceReview.setGoodForCouple(placeReviewMobileDto.getIsGoodForCouple());
        datingPlaceReview.setAverageRating(getAverageRatingPerReview(placeReviewMobileDto.getSecurityRating(), placeReviewMobileDto.getParkingRating(), placeReviewMobileDto.getParkingAccessRating()));
        datingPlaceReview.setReviewApprovalStatus(ApprovalStatus.PENDING);
        datingPlaceReview.setReviewerType("user");


        if (datingPlaceReviewRepo.findFirst1ByReviewerEmail(placeReviewMobileDto.getReviewerEmail()) != null) {
            datingPlaceReview.setReviewerId(datingPlaceReviewRepo.findFirst1ByReviewerEmail(placeReviewMobileDto.getReviewerEmail()).getReviewerId());
        } else {
            datingPlaceReview.setReviewerId(DatingPlaceUtil.generateRandomId(8));
        }

        DatingPlaceReviewMobileDto datingPlaceReviewMobileDto = new DatingPlaceReviewMobileDto(datingPlaceReviewRepo.save(datingPlaceReview));

        // Update datingplace review stats
        updateDatingPlaceStats(datingPlace, datingPlaceReviewMobileDto.getSuitableGender());
        return datingPlaceReviewMobileDto;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DatingPlaceReviewDto> getAllReviews(Pageable pageable) {
        Page<DatingPlaceReview> datingPlaceReviews = datingPlaceReviewRepo.findAllByOrderByPlacesReviewIdDesc(pageable);
        return datingPlaceReviews.map(DatingPlaceReviewDto::new);
    }

    @Override
    @Transactional
    public Boolean changePlaceApprovalStatus(Integer placeId, ApprovalStatus status) {
        DatingPlace datingPlace = datingPlaceRepo.findOne(placeId);
        boolean isApproved = false;
        if (datingPlace == null) {
            return false;
        } else {
            datingPlace.setPlaceApprovalStatus(status);
            isApproved = datingPlace.getApproved() == null ? false : datingPlace.getApproved();
            if (!isApproved && status.equals(ApprovalStatus.APPROVED)) {
                datingPlace.setApproved(true);
            } else if (isApproved && status.equals(ApprovalStatus.REJECTED)) {
                datingPlace.setApproved(false);
            }
            datingPlaceRepo.save(datingPlace);
        }
        return isApproved;
    }

    @Override
    @Transactional
    public Boolean changeDealApprovalStatus(Integer datingDealId, ApprovalStatus status) {
        DatingPlaceDeals datingPlaceDeals = datingPlaceDealsRepo.findOne(datingDealId);
        boolean isApproved = false;
        if (datingPlaceDeals == null) {
            return false;
        } else {
            datingPlaceDeals.setApprovalStatus(status);
            isApproved = datingPlaceDeals.getApproved() == null ? false : datingPlaceDeals.getApproved();
            if (!isApproved && status.equals(ApprovalStatus.APPROVED)) {
                datingPlaceDeals.setApproved(true);
            } else if (isApproved && status.equals(ApprovalStatus.REJECTED)) {
                datingPlaceDeals.setApproved(false);
            }
            datingPlaceDealsRepo.save(datingPlaceDeals);
        }
        return isApproved;
    }

    @Override
    @Transactional(readOnly = true)
    public DatingPlaceDealsDto findDatingDealById(Integer datingDealId) {
        return new DatingPlaceDealsDto(datingPlaceDealsRepo.findOne(datingDealId));
    }

    @Override
    @Transactional
    public Boolean changeReviewApprovalStatus(Integer reviewId, ApprovalStatus status) {
        DatingPlaceReview placeReview = datingPlaceReviewRepo.findOne(reviewId);
        boolean isApproved = false;
        if (placeReview.getReviewerEmail() == null) {
            return false;
        } else {
            placeReview.setReviewApprovalStatus(status);
            isApproved = placeReview.getApproved() == null ? false : placeReview.getApproved();
            if (!isApproved && status.equals(ApprovalStatus.APPROVED)) {
                placeReview.setApproved(true);
                datingPlaceReviewRepo.save(placeReview);

//                if (placeReview.getPlaceId() != null && !placeReview.getReviewerType().equalsIgnoreCase("owner")) {
//                    DatingPlace datingPlace = datingPlaceRepo.findOne(placeReview.getPlaceId().getDatingPlacesId());
//                    datingPlace.setAverageRating(datingPlaceReviewRepo.findAverageRatingByPlaceId(datingPlace));
//                    datingPlace.setSecurityAverage(datingPlaceReviewRepo.findAllAverageSecurityRatingByPlaceId(datingPlace));
//                    datingPlace.setParkingAverage(datingPlaceReviewRepo.findAllAverageParkingRatingByPlaceId(datingPlace));
//                    datingPlace.setTransportAverage(datingPlaceReviewRepo.findAllAverageTransportRatingByPlaceId(datingPlace));
//                    datingPlace.setSuitableForMaleCount(datingPlaceReviewRepo.getGenderCountByPlaceId(datingPlace, Gender.MALE));
//                    datingPlace.setSuitableForFemale(datingPlaceReviewRepo.getGenderCountByPlaceId(datingPlace, Gender.FEMALE));
//                    datingPlace.setSuitableForBoth(datingPlaceReviewRepo.getGenderCountByPlaceId(datingPlace, Gender.BOTH));
//                    datingPlace.setSuitableForAll(datingPlaceReviewRepo.getGenderCountByPlaceId(datingPlace, Gender.ALL));
//                    datingPlace.setAverageOverAllRatingByUser(datingPlaceReviewRepo.findAverageOverAllRatingByPlaceId(datingPlace));
//                    datingPlaceRepo.save(datingPlace);
//                }
            } else if (isApproved && status.equals(ApprovalStatus.REJECTED)) {
                placeReview.setApproved(false);
                datingPlaceReviewRepo.save(placeReview);
            }
        }
        return isApproved;
    }

    @Transactional
    private void updateDatingPlaceStats(DatingPlace datingPlace, Gender gender) {
        switch (gender) {
            case FEMALE:
                datingPlace.setSuitableForFemale(datingPlace.getSuitableForFemale() + 1);
                break;
            case MALE:
                datingPlace.setSuitableForMaleCount(datingPlace.getSuitableForMaleCount() + 1);
                break;
            case BOTH:
                datingPlace.setSuitableForBoth(datingPlace.getSuitableForBoth() + 1);
                break;
            case ALL:
                datingPlace.setSuitableForBoth(datingPlace.getSuitableForBoth() + 1);
                datingPlace.setSuitableForMaleCount(datingPlace.getSuitableForMaleCount() + 1);
                datingPlace.setSuitableForFemale(datingPlace.getSuitableForFemale() + 1);
                break;

        }
        datingPlace.setAverageRating(datingPlaceReviewRepo.findAverageRatingByPlaceId(datingPlace));
        datingPlace.setSecurityAverage(datingPlaceReviewRepo.findAllAverageSecurityRatingByPlaceId(datingPlace));
        datingPlace.setParkingAverage(datingPlaceReviewRepo.findAllAverageParkingRatingByPlaceId(datingPlace));
        datingPlace.setTransportAverage(datingPlaceReviewRepo.findAllAverageTransportRatingByPlaceId(datingPlace));
        datingPlace.setAverageOverAllRatingByUser(datingPlaceReviewRepo.findAverageOverAllRatingByPlaceId(datingPlace));
        datingPlaceRepo.save(datingPlace);
    }

    @Override
    @Transactional(readOnly = true)
    public DatingPlaceClaimDto findPlaceClaimById(String messageId) {
        DatingPlaceClaim placeClaim = datingPlaceClaimRepo.findDistinctByContent(messageId);
        return new DatingPlaceClaimDto(placeClaim);
    }

    @Override
    @Transactional(readOnly = true)
    public DatingPlaceReviewDto findReviewById(Integer reviewid) {
        return new DatingPlaceReviewDto(datingPlaceReviewRepo.findOne(reviewid));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DatingPlaceReviewDto> getReviewsByDatingPlace(Pageable pageable, Integer placeId) {
        Page<DatingPlaceReview> datingPlaceReviews = datingPlaceReviewRepo.findAllByPlaceIdAndReviewApprovalStatusAndReviewerType(pageable, placeId, ApprovalStatus.APPROVED);
        return datingPlaceReviews.map(DatingPlaceReviewDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DatingPlaceReviewDto> getReviewsByGoogleDatingPlace(Pageable pageable, DatingPlaceDto datingPlaceDto) {

        List<DatingPlaceReviewDto> datingPlaceReviewDtos = datingPlaceDto.getDatingPlaceReviewDtos();
        List<DatingPlaceReview> datingPlaceReviewList = datingPlaceReviewRepo.findAllByGooglePlaceIdAndReviewApprovalStatusOrderByReviewDateDesc(datingPlaceDto.getDatingPlacesId(), ApprovalStatus.APPROVED);
        datingPlaceReviewList.forEach(datingPlaceReview -> {

            datingPlaceReviewDtos.add(new DatingPlaceReviewDto(datingPlaceReview));
        });
        datingPlaceReviewDtos.sort(DatingPlaceUtil.sortByDateDescending);
        return new PageImpl<>(datingPlaceReviewDtos, pageable, datingPlaceReviewDtos.size());
    }

    @Override
    @Transactional(readOnly = true)
    public Double getAverageSecurityRatingByPlace(Integer placeId) {
        Double totalSecurityRating = 0.0;
        DatingPlace datingPlace = datingPlaceRepo.findOne(placeId);
        List<Double> securityRatings = datingPlaceReviewRepo.findAllSecurityRatingByPlaceId(datingPlace);

        for (int x = 0; x <= securityRatings.size() - 1; x++) {
            totalSecurityRating = totalSecurityRating + securityRatings.get(x);
        }
        return totalSecurityRating / securityRatings.size();
    }

    @Override
    @Transactional(readOnly = true)
    public Double getAverageParkingRatingByPlace(Integer placeId) {
        Double totalSecurityRating = 0.0;
        DatingPlace datingPlace = datingPlaceRepo.findOne(placeId);
        List<Double> securityRatings = datingPlaceReviewRepo.findAllParkingRatingByPlaceId(datingPlace);

        for (int x = 0; x <= securityRatings.size() - 1; x++) {
            totalSecurityRating = totalSecurityRating + securityRatings.get(x);
        }
        return totalSecurityRating / securityRatings.size();
    }

    @Override
    @Transactional(readOnly = true)
    public Double getAverageParkingAccessRatingByPlace(Integer reviewId) {
        Double totalSecurityRating = 0.0;
        DatingPlace datingPlace = datingPlaceRepo.findOne(reviewId);
        List<Double> securityRatings = datingPlaceReviewRepo.findAllParkingAccessRatingByPlaceId(datingPlace);

        for (int x = 0; x <= securityRatings.size() - 1; x++) {
            totalSecurityRating = totalSecurityRating + securityRatings.get(x);
        }
        return totalSecurityRating / securityRatings.size();
    }

    @Override
    @Transactional
    public boolean deletePlaceReview(Integer reviewId) {

        DatingPlaceReview datingPlaceReview = datingPlaceReviewRepo.findOne(reviewId);

        if (datingPlaceReview != null) {
            datingPlaceReviewRepo.delete(datingPlaceReview);
            return true;
        }
        return false;
    }

    @Override
    @Transactional(readOnly = true)
    public Double getAverageRatingByPlace(Integer placeId) {
        Double totalAverage = 0.0;
        DatingPlace datingPlace = datingPlaceRepo.findOne(placeId);
        List<Double> averageRatingList = datingPlaceReviewRepo.findAllAverageRatingByPlaceId(datingPlace);

        for (int x = 0; x <= averageRatingList.size() - 1; x++) {
            totalAverage = totalAverage + averageRatingList.get(x);
        }
        return totalAverage / averageRatingList.size();
    }

    @Override
    @Transactional(readOnly = true)
    public Double getOverAllRatingByPlace(Integer placeId) {
        DatingPlace datingPlace = datingPlaceRepo.findOne(placeId);
        return datingPlaceReviewRepo.findOverAllRatingByPlaceId(datingPlace);
    }

    @Override
    @Transactional(readOnly = true)
    public DatingPlaceDto getDatingPlaceByReviewId(Integer reviewId) {
        DatingPlace datingPlace = datingPlaceReviewRepo.findDatingPlaceByDatingPlacesReviewId(reviewId);
        if (datingPlace != null) {
            DatingPlaceDto datingPlaceDto = new DatingPlaceDto(datingPlace);
            return datingPlaceDto;
        } else {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DatingPlaceDto> findPlacesByNewFilters(Pageable pageable, LocationDto locationDto) {
        Page<DatingPlaceDto> datingPlaceDtos;
        //Todo detect string search & it should search all locations
        if (locationDto.getLatitude() != null && locationDto.getLongitude() != null) {

            //Get Lovappy places from database in Descending Order in user location
            Page<DatingPlaceDto> temp = datingPlaceRepo.getDefaultPlaces(pageable, locationDto.getLatitude(), locationDto.getLongitude(), 50D).map(DatingPlaceDto::new);
            //if the the user doesn't have places near by get the all the places
            if (temp.getTotalElements() == 0) {

                Page<DatingPlaceDto> tempResults = datingPlaceRepo.findAllByPlaceApprovalStatus(ApprovalStatus.APPROVED, pageable).map(DatingPlaceDto::new);

                datingPlaceDtos = getGoogleResultsCombinedNew(tempResults, pageable, locationDto, null);
            } else {
                datingPlaceDtos = getGoogleResultsCombinedNew(temp, pageable, locationDto, null);
            }

        } else {

            datingPlaceDtos = datingPlaceRepo.findAllByPlaceApprovalStatus(ApprovalStatus.APPROVED, pageable).map(DatingPlaceDto::new);
        }
        return datingPlaceDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DatingPlaceDto> findPlacesByAllFilters(Pageable pageable, DatingPlaceFilterDto filterDto, LocationDto locationDto) {
        Page<DatingPlaceDto> datingPlaceDtos = datingPlaceRepo.findAllByAdvancedFilters(filterDto, locationDto, pageable).map(DatingPlaceDto::new);

        if (locationDto.getLatitude() != null && locationDto.getLongitude() != null) {
            if (datingPlaceDtos.getTotalElements() == 0) {
//                filterDto.setLatitude(null);
//                filterDto.setLongitude(null);
                datingPlaceDtos = datingPlaceRepo.findAllByAnyMatchAdvancedFilters(filterDto, locationDto, pageable).map(DatingPlaceDto::new);
            }
        }else {
            locationDto.setPlaceName("");
            locationDto.setLatitude(0.0);
            locationDto.setLongitude(0.0);
        }
      //  datingPlaceDtos = getGoogleResultsCombinedNew(datingPlaceDtos, pageable, locationDto, null);

        return getGoogleResultsCombinedNew(datingPlaceDtos, pageable, locationDto, true);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DatingPlaceMobileDto> findPlacesByAllFiltersMobile(Pageable pageable, DatingPlaceFilterDto filterDto, LocationDto locationDto) {
        Page<DatingPlaceDto> datingPlaceDtos = findPlacesByAllFilters(pageable, filterDto, locationDto);
        return datingPlaceDtos.map(DatingPlaceMobileDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DatingPlaceMobileDto> findPlacesByAllFiltersMobile(DatingPlaceFilterDto filterDto, LocationDto locationDto) throws IOException {
        List<DatingPlaceMobileDto> datingPlaceMobileList;
        Query query = datingPlaceRepo.generateQuery(false, false, filterDto, locationDto);
        List<DatingPlace> list = query.getResultList();
        if (list.isEmpty()) {
//            filterDto.setLatitude(null);
//            filterDto.setLongitude(null);
            Query globalQuery = datingPlaceRepo.generateQuery(false, true, filterDto, locationDto);
            List<DatingPlace> globalList = globalQuery.getResultList();
            datingPlaceMobileList = new ListMapper<>(globalList).map(DatingPlaceMobileDto::new);
        } else {
            datingPlaceMobileList = new ListMapper<>(list).map(DatingPlaceMobileDto::new);
        }
        return datingPlaceMobileList;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DatingPlaceDto> findPlacesByQuickFilters(int page, int limit, DatingPlaceFilterDto filterDto, LocationDto locationDto) {
        Page<DatingPlaceDto> datingPlaceDtos;
        Pageable pageable = new PageRequest(page - 1, limit);
        if (filterDto.getWorldWide()) {
            if (filterDto.getGender() != null && filterDto.getGender() != Gender.NA) {
                datingPlaceDtos = getDatingPlacesByGender(filterDto.getGender(), pageable).map(DatingPlaceDto::new);
            } else {
                datingPlaceDtos = datingPlaceRepo.findPlacesByRatingDesc(pageable).map(DatingPlaceDto::new);
            }//Todo add google place for any location
            locationDto.setPlaceName("");
            locationDto.setLatitude(0.0);
            locationDto.setLongitude(0.0);
            // datingPlaceDtos = getGoogleResultsCombinedNew(datingPlaceDtos, pageable, locationDto, true);
        } else if (filterDto.getLocal()) {

            if (filterDto.getGender() != null && filterDto.getGender() != Gender.NA) {
                datingPlaceDtos = getDatingPlacesByGenderAndLocation(filterDto.getGender(), locationDto, pageable).map(DatingPlaceDto::new);
            } else {
                datingPlaceDtos = datingPlaceRepo.getDefaultPlaces(pageable, locationDto.getLatitude(), locationDto.getLongitude(), 50D).map(DatingPlaceDto::new);
            }
            if (StringUtils.isEmpty(locationDto.getPlaceName()))
                locationDto.setPlaceName("");

            //datingPlaceDtos = getGoogleResultsCombinedNew(datingPlaceDtos, pageable, locationDto, true);
        } else if (filterDto.getFeatured()) {

            if (filterDto.getGender() != null && filterDto.getGender() != Gender.NA) {
                datingPlaceDtos = getNearByFeaturedPlacesByGender(filterDto.getGender(), locationDto, pageable).map(DatingPlaceDto::new);
               if(datingPlaceDtos.getTotalElements() == 0) {
                   datingPlaceDtos = getFeaturedDatingPlacesByGender(filterDto.getGender(), pageable).map(DatingPlaceDto::new);
               }
            } else {
                datingPlaceDtos = datingPlaceRepo.findNearByFeaturedPlacesByRatingDesc(pageable, locationDto.getLatitude(), locationDto.getLongitude(), 20D).map(DatingPlaceDto::new);
                if(datingPlaceDtos.getTotalElements() == 0) {
                    datingPlaceDtos = datingPlaceRepo.findFeaturedPlacesByRatingDesc(pageable).map(DatingPlaceDto::new);
                }
            }
            if (StringUtils.isEmpty(locationDto.getPlaceName()))
                locationDto.setPlaceName("");

           return datingPlaceDtos;
        }
        else if (filterDto.getGender() != null && filterDto.getGender() != Gender.NA) {
            datingPlaceDtos = getDatingPlacesByGender(filterDto.getGender(), pageable).map(DatingPlaceDto::new);
        } else {
            datingPlaceDtos = datingPlaceRepo.findPlacesByAllGendersDesc(pageable).map(DatingPlaceDto::new);
        }
        datingPlaceDtos = getGoogleResultsCombinedNew(datingPlaceDtos, pageable, locationDto, true);
        return datingPlaceDtos;
    }

    private Page<DatingPlace> getDatingPlacesByGender(Gender gender, Pageable pageable) {
        switch (gender) {
            case ALL:
                return datingPlaceRepo.findPlacesByAllGendersDesc(pageable);
            case MALE:
                return datingPlaceRepo.findPlacesByMaleGendersDesc(pageable);
            case FEMALE:
                return datingPlaceRepo.findPlacesByFemaleGendersDesc(pageable);
            case BOTH:
                return datingPlaceRepo.findPlacesByBothGendersDesc(pageable);
        }
        return datingPlaceRepo.findPlacesByRatingDesc(pageable);
    }

    private Page<DatingPlace> getDatingPlacesByGenderAndLocation(Gender gender, LocationDto locationDto, Pageable pageable) {
        switch (gender) {
            case ALL:
                return datingPlaceRepo.findByAllGenderAndLocation(pageable, locationDto.getLatitude(), locationDto.getLongitude(), locationDto.getRadius());
            case MALE:
                return datingPlaceRepo.findByMaleGenderAndLocation(pageable, locationDto.getLatitude(), locationDto.getLongitude(), locationDto.getRadius());
            case FEMALE:
                return datingPlaceRepo.findByFemaleGenderAndLocation(pageable, locationDto.getLatitude(), locationDto.getLongitude(), locationDto.getRadius());
            case BOTH:
                return datingPlaceRepo.findByBothGenderAndLocation(pageable, locationDto.getLatitude(), locationDto.getLongitude(), locationDto.getRadius());
        }
        return datingPlaceRepo.findByAllGenderAndLocation(pageable, locationDto.getLatitude(), locationDto.getLongitude(), locationDto.getRadius());
    }
    private Page<DatingPlace> getNearByFeaturedPlacesByGender(Gender gender, LocationDto locationDto, Pageable pageable) {
        switch (gender) {
            case ALL:
                return datingPlaceRepo.findFeaturedByAllGenderAndLocation(pageable, locationDto.getLatitude(), locationDto.getLongitude(), locationDto.getRadius());
            case MALE:
                return datingPlaceRepo.findFeaturedByMaleGenderAndLocation(pageable, locationDto.getLatitude(), locationDto.getLongitude(), locationDto.getRadius());
            case FEMALE:
                return datingPlaceRepo.findFeaturedByFemaleGenderAndLocation(pageable, locationDto.getLatitude(), locationDto.getLongitude(), locationDto.getRadius());
            case BOTH:
                return datingPlaceRepo.findFeaturedByBothGenderAndLocation(pageable, locationDto.getLatitude(), locationDto.getLongitude(), locationDto.getRadius());
        }
        return datingPlaceRepo.findFeaturedByAllGenderAndLocation(pageable, locationDto.getLatitude(), locationDto.getLongitude(), locationDto.getRadius());
    }

    private Page<DatingPlace> getFeaturedDatingPlacesByGender(Gender gender, Pageable pageable) {
        switch (gender) {
            case ALL:
                return datingPlaceRepo.findFeaturedPlacesByAllGendersDesc(pageable);
            case MALE:
                return datingPlaceRepo.findFeaturedPlacesByMaleGendersDesc(pageable);
            case FEMALE:
                return datingPlaceRepo.findFeaturedPlacesByFemaleGendersDesc(pageable);
            case BOTH:
                return datingPlaceRepo.findFeaturedPlacesByBothGendersDesc(pageable);
        }
        return datingPlaceRepo.findFeaturedPlacesByRatingDesc(pageable);
    }
    @Override
    @Transactional(readOnly = true)
    public List<DatingPlaceMobileDto> findPlacesByQuickFilters(DatingPlaceFilterDto filterDto, LocationDto locationDto) throws IOException {
        List<DatingPlaceDto> datingPlaceDtos;
        if (filterDto.getWorldWide()) {
            datingPlaceDtos = new ListMapper<>(datingPlaceRepo.getSearchedPlaces(DatingPlaceUtil.returnEmptyIfNull(locationDto.getPlaceName()), filterDto.getAgeRange(), filterDto.getGender().toString(), filterDto.getPriceRange().toString(),
                    getRatingValue(filterDto.getSecurity()), getRatingValue(filterDto.getParking()),
                    getRatingValue(filterDto.getTransport()), filterDto.getPetsAllowed(),
                    filterDto.getNeighborhood().toString(), filterDto.getGoodForCouple())).map(DatingPlaceDto::new);
            //Todo add google place for any location
        } else if (filterDto.getLocal()) {
            datingPlaceDtos = new ListMapper<>(datingPlaceRepo.getSearchedPlacesByLocation(DatingPlaceUtil.returnEmptyIfNull(locationDto.getPlaceName()),
                    filterDto.getAgeRange(),
                    filterDto.getGender().toString(), filterDto.getPriceRange().toString(),
                    getRatingValue(filterDto.getSecurity()), getRatingValue(filterDto.getParking()),
                    getRatingValue(filterDto.getTransport()), filterDto.getPetsAllowed(),
                    filterDto.getNeighborhood().toString(), filterDto.getGoodForCouple(),
                    locationDto.getLatitude(), locationDto.getLongitude(), 50D)).map(DatingPlaceDto::new);
            datingPlaceDtos.addAll(datingPlaceLocationUtil.getGooglePlacesListByLocation(locationDto.getLatitude(), locationDto.getLongitude()));
        } else {
            datingPlaceDtos = new ListMapper<>(datingPlaceRepo.findAllByGenderTest(filterDto.getGender().toString())).map(DatingPlaceDto::new);
        }
        return new ListMapper<>(datingPlaceDtos).map(DatingPlaceMobileDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DatingPlaceMobileDto> findPlacesByFilters(Pageable pageable, DatingPlaceMobileFilterDto datingPlaceFilterDto) {
        Page<DatingPlace> placePage = datingPlaceRepo.getSearchedPlaces(pageable,
                DatingPlaceUtil.returnEmptyIfNull(datingPlaceFilterDto.getPlaceName()), datingPlaceFilterDto.getAgeRange(),
                datingPlaceFilterDto.getGender().toString(), datingPlaceFilterDto.getPriceRange().toString(),
                getRatingValue(datingPlaceFilterDto.getSecurity()), getRatingValue(datingPlaceFilterDto.getParking()),
                getRatingValue(datingPlaceFilterDto.getTransport()), datingPlaceFilterDto.getPetsAllowed(),
                datingPlaceFilterDto.getNeighborhood().toString(), datingPlaceFilterDto.getGoodForCouple());
        return placePage.map(DatingPlaceMobileDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DatingPlaceMobileDto> findPlacesBySearchTerm(Pageable pageable, String searchTerm, Double latitude, Double longitude, Double radius) throws IOException {
        Page<DatingPlace> datingPlaces = datingPlaceRepo.getSearchedPlaces(pageable, searchTerm, "", "", "", 1D, 1D, 1D, null, "", null);
        LocationDto locationDto = new LocationDto();
        locationDto.setLatitude(latitude);
        locationDto.setLongitude(longitude);
        locationDto.setRadius(radius);
        locationDto.setPlaceName(searchTerm.replace(" ", "+"));
        Page<DatingPlaceDto> datingPlaceDtos = getGoogleResultsCombinedNew(datingPlaces.map(DatingPlaceDto::new), pageable, locationDto, true);
        return datingPlaceDtos.map(DatingPlaceMobileDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DatingPlaceDto> getFeaturedPlaces(LocationDto locationDto) {
        List<DatingPlace> datingPlaces = datingPlaceRepo.findAllByIsFeaturedTrueAndPlaceApprovalStatusOrderByDatingPlacesIdDesc(ApprovalStatus.APPROVED);
        List<DatingPlaceDto> googleFeaturesPlaces = new ArrayList<>();
        try {
            if (locationDto.getPlaceName() != null) {
                googleFeaturesPlaces = datingPlaceLocationUtil.getGooglePlacesRestaurantsList(locationDto.getLatitude(), locationDto.getLongitude(), 15000D, locationDto.getPlaceName().replace(" ", "+"));
            } else if (locationDto.getCityName() != null) {
                googleFeaturesPlaces = datingPlaceLocationUtil.getGooglePlacesRestaurantsList(locationDto.getLatitude(), locationDto.getLongitude(), 15000D, locationDto.getCityName().replace(" ", "+"));
            } else {
                googleFeaturesPlaces = datingPlaceLocationUtil.getGooglePlacesRestaurantsList(locationDto.getLatitude(), locationDto.getLongitude(), 15000D, null);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        List<DatingPlaceDto> datingPlaceDtos = new ArrayList<>();
        datingPlaces.forEach(datingPlace -> {

            datingPlaceDtos.add(new DatingPlaceDto(datingPlace));
        });
        return datingPlaceDtos.size() == 0 ? googleFeaturesPlaces : datingPlaceDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public CalculatePriceDto calculatePrice(DatingPlaceDto datingPlaceDto, String couponCode) {
        Double price = 0d;

        if (datingPlaceDto.getIsMapFeatured()) {
            price += 5d;
        }

        if (datingPlaceDto.getIsLovStampFeatured()) {
            price += 8d;
        }

        Double totalPrice = price;

        Double discountValue = 0d;
        CouponDto coupon = orderService.getCoupon(couponCode, CouponCategory.AD);
        if (coupon != null)
            discountValue = totalPrice * coupon.getDiscountPercent() * 0.01;

        totalPrice -= discountValue;

        return new CalculatePriceDto(price, coupon, totalPrice);
    }

    @Override
    @Transactional(readOnly = true)
    public CalculatePriceDto calculatePrice(Integer datingplaceId, String couponCode) {
        DatingPlace datingPlace = datingPlaceRepo.findOne(datingplaceId);
        Double price = 0d;

        if (datingPlace.getMapFeatured()) {
            price += 5d;
        }

        if (datingPlace.getLoveStampFeatured()) {
            price += 8d;
        }

        Double totalPrice = price;

        Double discountValue = 0d;
        CouponDto coupon = orderService.getCoupon(couponCode, CouponCategory.AD);
        if (coupon != null)
            discountValue = totalPrice * coupon.getDiscountPercent() * 0.01;

        totalPrice -= discountValue;

        return new CalculatePriceDto(price, coupon, totalPrice);
    }

    @Override
    @Transactional(readOnly = true)
    public Integer getGenderCountByPlace(Integer placeId, Gender gender) {
        Integer count = 0;
        DatingPlace datingPlace = datingPlaceRepo.findOne(placeId);
        count = datingPlaceReviewRepo.getGenderCountByPlaceId(datingPlace, gender);
        return count != null ? count : 0;
    }

    @Override
    @Transactional
    public Double calculatePlaceDeal(Integer placeId, Double fullAmount, Double discountAmout) {
        return null;
    }

    @Transactional(readOnly = true)
    @Override
    public Page<DatingPlaceDto> getAllFeaturedPlaces(Pageable pageable) {
        return datingPlaceRepo.findByLoveStampFeatured(true, pageable).map(DatingPlaceDto::new);
    }
    @Transactional(readOnly = true)
    @Override
    public Page<DatingPlaceDto> getAllNearByFeaturedPlaces(Pageable pageable, LocationDto locationDto) {
    if(locationDto != null && locationDto.getLatitude()!=null && locationDto.getLongitude() !=null){
        return datingPlaceRepo.findNearByFeaturedPlacesByRatingDesc(pageable, locationDto.getLatitude(), locationDto.getLongitude(), locationDto.getRadius()).map(DatingPlaceDto::new);
    }
        return datingPlaceRepo.findFeaturedPlacesByRatingDesc(pageable).map(DatingPlaceDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DealsDto> getAllDealTypes(Pageable pageable) {
        return dealsRepo.findAll(pageable).map(DealsDto::new);
    }


    @Override
    @Transactional
    public DealsDto addDeal(DealsDto dealsDto) {
        Deals deals = new Deals();
        deals.setStartPrice(dealsDto.getStartPrice());
        deals.setEndPrice(dealsDto.getEndPrice());
        deals.setFeePercentage(dealsDto.getFeePercentage() / 100);
        Deals deals1 = dealsRepo.save(deals);
        return new DealsDto(deals1);
    }

    @Override
    @Transactional
    public DealsDto updateDeal(DealsDto dealsDto) {
        Deals deals = dealsRepo.findOne(dealsDto.getDealId());
        deals.setStartPrice(dealsDto.getStartPrice());
        deals.setEndPrice(dealsDto.getEndPrice());
        deals.setFeePercentage(dealsDto.getFeePercentage());
        return new DealsDto(dealsRepo.saveAndFlush(deals));
    }

    @Override
    @Transactional(readOnly = true)
    public DealsDto getdealById(Integer id) {
        return new DealsDto(dealsRepo.findOne(id));
    }

    @Override
    @Transactional(readOnly = true)
    public DealsDto getDealByFullAmount(Double amount) {
        return new DealsDto(dealsRepo.findAllByFullAmount(amount));
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isDealRangeExisting(Double price) {
        List<Deals> dealsList = dealsRepo.isDealExists(price);
        return dealsList.size() != 0;
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isDealRangeExistsFor(Double price, Integer id) {
        List<Deals> dealsList = dealsRepo.isDealExistsForUpdate(price, id);
        return dealsList.size() != 0;
    }

    @Override
    @Transactional(readOnly = true)
    public Integer getActiveDealsCountByPlaceIdAndDate(Integer placeId, Date date) {
        DatingPlace datingPlace = datingPlaceRepo.findOne(placeId);
        List<DatingPlaceDeals> dealsList = datingPlaceDealsRepo.findAllByApprovalStatusAndStartDateAndDatingPlace(ApprovalStatus.APPROVED, date, datingPlace);
        return dealsList.size();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DatingPlaceDealsDto> getAllPlaceDeals(Pageable pageable) {
        Page<DatingPlaceDeals> datingPlaceDeals = datingPlaceDealsRepo.findAll(pageable);
        return datingPlaceDeals.map(DatingPlaceDealsDto::new);
    }

    @Override
    @Transactional
    public DatingPlaceDealsDto addDatingDeal(DatingPlaceDealsDto placeDealsDto) {
        DatingPlaceDeals placeDeals = new DatingPlaceDeals();
        DatingPlace place = datingPlaceRepo.findOne(placeDealsDto.getPlaceId());
        Deals deals = dealsRepo.findAllByFullAmount(placeDealsDto.getFullAmount());
        placeDeals.setDatingPlace(place);
        placeDeals.setDeals(deals);
        placeDeals.setFullAmount(placeDealsDto.getFullAmount());
        placeDeals.setDiscountValue(placeDealsDto.getDiscountValue());
        placeDeals.setStartDate(placeDealsDto.getStartDate());
        placeDeals.setExpiryDate(placeDealsDto.getExpiryDate());
        placeDeals.setDealDescription(placeDealsDto.getDealDescription());
        placeDeals.setDealPrice(DatingPlaceUtil.calculateDealFee(placeDealsDto.getDiscountValue(), deals.getFeePercentage()));
        placeDeals.setApprovalStatus(ApprovalStatus.PENDING);
        return new DatingPlaceDealsDto(datingPlaceDealsRepo.save(placeDeals));
    }

    @Override
    @Transactional
    public DatingPlaceDealsDto updateDatingDeal(DatingPlaceDealsDto placeDealsDto) {
        DatingPlaceDeals datingPlaceDeals = datingPlaceDealsRepo.findOne(placeDealsDto.getDealId());
        return new DatingPlaceDealsDto(datingPlaceDealsRepo.saveAndFlush(datingPlaceDeals));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DatingPlaceDealsDto> getActiveDealsByPlaceIdAndDate(Pageable pageable, Integer placeId, Date date) {
        DatingPlace datingPlace = datingPlaceRepo.findOne(placeId);
        Page<DatingPlaceDeals> dealsPage = datingPlaceDealsRepo.findAllByApprovalStatusAndStartDateAndDatingPlacePaginated(ApprovalStatus.APPROVED, date, datingPlace, pageable);
        return dealsPage.map(DatingPlaceDealsDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DatingPlaceDealsMobileDto> getActiveDealsByPlaceIdAndDateMobile(Pageable pageable, Integer placeId, Date date) {
        DatingPlace datingPlace = datingPlaceRepo.findOne(placeId);
        Page<DatingPlaceDeals> dealsPage = datingPlaceDealsRepo.findAllByApprovalStatusAndStartDateAndDatingPlacePaginated(ApprovalStatus.APPROVED, date, datingPlace, pageable);
        return dealsPage.map(DatingPlaceDealsMobileDto::new);
    }

    @Override
    @Transactional
    public DatingPlaceClaimDto addClaim(DatingPlaceClaimDto claimDto) {
        DatingPlaceClaim datingPlaceClaim = new DatingPlaceClaim();
        if (DatingPlaceUtil.isInteger(claimDto.getPlaceId())) {
            DatingPlace datingPlace = datingPlaceRepo.findOne(Integer.parseInt(claimDto.getPlaceId()));
            datingPlaceClaim.setPlaceId(datingPlace);
        } else {
            datingPlaceClaim.setGooglePlaceId(claimDto.getPlaceId());
        }
        User user = userRepo.findOne(claimDto.getClaimerId());
        datingPlaceClaim.setSendTo(claimDto.getSendTo());
        datingPlaceClaim.setClaimer(user);
        datingPlaceClaim.setVerificationCode(DatingPlaceUtil.generateRandomId(6));
        datingPlaceClaim.setVerificationType(claimDto.getVerificationType());
        return new DatingPlaceClaimDto(datingPlaceClaimRepo.saveAndFlush(datingPlaceClaim));
    }

    @Override
    @Transactional
    public void addOwnerToExistingPlace(Integer placeId, Integer userId) {
        DatingPlace datingPlace = datingPlaceRepo.findOne(placeId);
        User owner = userRepo.findOne(userId);
        datingPlace.setClaimed(true);
        datingPlace.setOwner(owner);
        datingPlaceRepo.save(datingPlace);
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isOwnerVerified(String messageId, String verificationCode) {
        List<DatingPlaceClaim> placeClaim = datingPlaceClaimRepo.findAllByContentAndVerificationCode(messageId, verificationCode);
        return placeClaim.size() == 1 && placeClaim.get(0).getVerificationCode().equals(verificationCode);
    }

    @Override
    @Transactional
    public DatingPlaceClaimDto updateClaim(DatingPlaceClaimDto claimDto) {
        DatingPlaceClaim claim = datingPlaceClaimRepo.findOne(claimDto.getClaimId());
        claim.setContent(claimDto.getContent());
        return new DatingPlaceClaimDto(claim);
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isAlreadyClaimed(Integer placeId) {
        return datingPlaceRepo.findOne(placeId).getOwner() != null;
    }


    @Override
    @Transactional
    public Boolean featureDatingPlaceFromAdmin(Integer placeId, String featureType) {
        DatingPlace datingPlace = datingPlaceRepo.findOne(placeId);
        datingPlace.setFeatured(true);
        if (featureType.equalsIgnoreCase("map"))
            datingPlace.setMapFeatured(true);
        else if (featureType.equalsIgnoreCase("stamp"))
            datingPlace.setLoveStampFeatured(true);
        return datingPlaceRepo.saveAndFlush(datingPlace) != null;
    }

    @Override
    @Transactional(readOnly = true)
    public DatingPlaceDto findPlaceWithHighestRating() {
        List<DatingPlace> datingPlaces = datingPlaceRepo.findAllByMaximumAverageRating();
        if (datingPlaces.isEmpty()) {
            return new DatingPlaceDto();
        } else {
            return new DatingPlaceDto(datingPlaces.get(0));
        }
    }

    private DatingPlace fromGoogleDto2Entity(DatingPlaceDto datingPlaceDto) {
        DatingPlace datingPlace = new DatingPlace();
        datingPlace.setPlaceName(datingPlaceDto.getPlaceName());
        datingPlace.setExternalPlaceId(datingPlaceDto.getDatingPlacesId());
        datingPlace.setProvider(DatingPlaceProvider.GOOGLE);
        datingPlace.setAddressLine(datingPlaceDto.getAddressLine());
        datingPlace.setApproved(true);
        datingPlace.setAgeRange(datingPlaceDto.getAgeRange());
        datingPlace.setGender(datingPlaceDto.getGender());
        datingPlace.setGoodForMarriedCouple(datingPlaceDto.getIsGoodForMarriedCouple());
        datingPlace.setPriceRange(datingPlaceDto.getPriceRange());
        datingPlace.setLatitude(datingPlaceDto.getLatitude());
        datingPlace.setLongitude(datingPlaceDto.getLongitude());
        datingPlace.setCity(datingPlaceDto.getCity() == null ? "" : datingPlaceDto.getCity());
        datingPlace.setState(datingPlaceDto.getState() == null ? "" : datingPlaceDto.getState());
        datingPlace.setCountry(datingPlaceDto.getCountry() == null ? "" : datingPlaceDto.getCountry());
        datingPlace.setZipCode(datingPlaceDto.getZipCode() == null ? "" : datingPlaceDto.getZipCode());
        datingPlace.setCreateEmail(datingPlaceDto.getCreateEmail());
        datingPlace.setAverageRating(datingPlaceDto.getOverAllRating());
        datingPlace.setGoogleAverage(datingPlaceDto.getGoogleAverage());
        datingPlace.setPlaceApprovalStatus(ApprovalStatus.APPROVED);
        datingPlace.setSuitableForMaleCount(0);
        datingPlace.setSuitableForBoth(0);
        datingPlace.setSuitableForFemale(0);

        // Save google image url to database
        if (datingPlaceDto.getCoverPictureUrl() != null) {
            CloudStorageFile coverImage = new CloudStorageFile();
            coverImage.setName("google-images");
            coverImage.setUrl(datingPlaceDto.getCoverPictureUrl());
            coverImage.setBucket(imagesBucket);
            CloudStorageFile savedCoverImage = cloudStorageRepo.save(coverImage);
            datingPlace.setCoverPicture(savedCoverImage);
        }

        if (datingPlaceDto.getPlacesUrlList() != null && datingPlaceDto.getPlacesUrlList().size() > 0) {
            datingPlaceDto.getPlacesUrlList().forEach(placeUrl -> {
                CloudStorageFile placeImage = new CloudStorageFile();
                placeImage.setName("google-images");
                placeImage.setUrl(placeUrl);
                placeImage.setBucket(imagesBucket);
                CloudStorageFile savedPlaceImage = cloudStorageRepo.save(placeImage);
                PlacesAttachment placesAttachment = new PlacesAttachment();
                placesAttachment.setUrl(placeUrl);
                placesAttachment.setPicture(savedPlaceImage);
                datingPlace.getPlacesAttachments().add(placesAttachment);
                placesAttachment.setPlaceId(datingPlace);
            });
        }
        return datingPlace;
    }

    @Override
    @Transactional
    public DatingPlaceDto addOwnerToGooglePlace(DatingPlaceClaimDto claimDto, DatingPlaceDto datingPlaceDto) throws IOException {
        User user = userRepo.findOne(claimDto.getClaimerId());


        DatingPlace datingPlace = fromGoogleDto2Entity(datingPlaceDto);
        datingPlace.setOwner(user);
        datingPlace.setCreateEmail(user.getEmail());
        datingPlace.setOwnerContactNumber(claimDto.getSendTo());
        //Save google reviews to local database
        if (datingPlaceDto.getDatingPlaceReviewDtos() != null && datingPlaceDto.getDatingPlaceReviewDtos().size() > 0) {
            datingPlaceDto.getDatingPlaceReviewDtos().forEach(datingPlaceReviewDto -> {
                DatingPlaceReview placeReview = new DatingPlaceReview();
                placeReview.setReviewerEmail(datingPlaceReviewDto.getReviewerName());
                placeReview.setReviewerComment(datingPlaceReviewDto.getReviewerComment());
                placeReview.setReviewerType("google");
                placeReview.setReviewDate(datingPlaceReviewDto.getReviewDate());
                placeReview.setAverageRating(datingPlaceReviewDto.getAverageRating());
                placeReview.setOverallRating(datingPlaceReviewDto.getOverAllRating());
                placeReview.setGooglePlaceId(datingPlace.getExternalPlaceId());
                placeReview.setReviewApprovalStatus(ApprovalStatus.APPROVED);
                placeReview.setReviewerId(datingPlaceReviewDto.getReviewerName());
                placeReview.setApproved(true);
                datingPlace.getDatingPlaceReviews().add(placeReview);
                placeReview.setPlaceId(datingPlace);
            });

        }
        return new DatingPlaceDto(datingPlaceRepo.save(datingPlace));
    }

    @Override
    @Transactional
    public DatingPlaceDto completeGoogleClaiming(DatingPlaceDto datingPlaceDto) {
        DatingPlace datingPlace = datingPlaceRepo.findOne(Integer.parseInt(datingPlaceDto.getDatingPlacesId()));
        datingPlace.setPlaceName(datingPlaceDto.getPlaceName());
        datingPlace.setZipCode(datingPlaceDto.getZipCode());
        datingPlace.setGender(datingPlaceDto.getGender());
        datingPlace.setAgeRange(datingPlaceDto.getAgeRange());
        datingPlace.setPriceRange(datingPlaceDto.getPriceRange());
        datingPlace.setAddressLine(datingPlaceDto.getAddressLine());
        datingPlace.setAgeRange(datingPlaceDto.getAgeRange());
        datingPlace.setCity(datingPlaceDto.getCity());
        datingPlace.setState(datingPlaceDto.getState());
        datingPlace.setCountry(datingPlaceDto.getCountry());
        datingPlace.setAddressLineTwo(datingPlaceDto.getAddressLineTwo());
        datingPlace.setLatitude(datingPlaceDto.getLatitude());
        datingPlace.setLongitude(datingPlaceDto.getLongitude());
        datingPlace.setFeatured(false);
        datingPlace.setPlaceApprovalStatus(ApprovalStatus.PENDING);
        datingPlace.setGoodForMarriedCouple(datingPlaceDto.getIsGoodForMarriedCouple());

        if (datingPlaceDto.getGender().equals(Gender.MALE)) {
            datingPlace.setSuitableForMaleCount(1);
            datingPlace.setSuitableForFemale(0);
            datingPlace.setSuitableForBoth(0);
        } else if (datingPlaceDto.getGender().equals(Gender.FEMALE)) {
            datingPlace.setSuitableForFemale(1);
            datingPlace.setSuitableForMaleCount(0);
            datingPlace.setSuitableForBoth(0);
        } else if (datingPlaceDto.getGender().equals(Gender.BOTH)) {
            datingPlace.setSuitableForBoth(1);
            datingPlace.setSuitableForMaleCount(0);
            datingPlace.setSuitableForFemale(0);
        }

        List<DatingPlaceReview> existingReviewList = datingPlaceReviewRepo.findAllByGooglePlaceIdAndReviewApprovalStatusOrderByReviewDateDesc(datingPlace.getExternalPlaceId(), ApprovalStatus.APPROVED);
        existingReviewList.forEach(datingPlaceReview -> {
            datingPlaceReview.setPlaceId(datingPlace);
            datingPlaceReviewRepo.saveAndFlush(datingPlaceReview);
        });

        if (datingPlaceDto.getCoverPicture().getId() != null) {
            CloudStorageFile image = cloudStorageRepo.findOne(datingPlaceDto.getCoverPicture().getId());
            datingPlace.setCoverPicture(image);
        }

        if (datingPlaceDto.getPlacesAttachmentDtos().size() > 0 && (datingPlaceDto.getPlacesAttachmentDtos().get(0).getPicture().getId() != null)) {
            PlacesAttachment placesAttachment = datingPlaceDto.getPlacesAttachmentDtos().get(0).getPlacesAttachment();
            placesAttachment.setPicture(cloudStorageRepo.findOne(datingPlaceDto.getPlacesAttachmentDtos().get(0).getPicture().getId()));
            placesAttachment.setUrl(cloudStorageRepo.findOne(datingPlaceDto.getPlacesAttachmentDtos().get(0).getPicture().getId()).getUrl());
            datingPlace.getPlacesAttachments().add(placesAttachment);
            placesAttachment.setPlaceId(datingPlace);
        }

        DatingPlaceReview datingPlaceReview = new DatingPlaceReview();
        datingPlaceReview.setSuitableAgeRange(datingPlaceDto.getAgeRange());
        datingPlaceReview.setSuitableGender(datingPlaceDto.getGender());
        datingPlaceReview.setSuitablePriceRange(datingPlaceDto.getPriceRange());
        datingPlaceReview.setGoodForCouple(datingPlaceDto.getIsGoodForMarriedCouple());
        datingPlaceReview.setReviewerComment("");
        datingPlaceReview.setReviewerEmail(datingPlace.getCreateEmail());
        datingPlaceReview.setReviewerType("owner");
        datingPlaceReview.setReviewApprovalStatus(ApprovalStatus.APPROVED);

        String[] atmosphereList = datingPlaceDto.getPlaceAtmospheres().split(",");
        for (String atmo : atmosphereList) {
            PlaceAtmosphere atmospheres = placeAtmosphereRepo.findByAtmosphereNameAndEnabledTrue(atmo);
            DatingPlaceAtmospheres placeAtmospheres = new DatingPlaceAtmospheres();
            placeAtmospheres.setAtmosphere(atmospheres);
            placeAtmospheres.setPlaceId(datingPlace);
            datingPlace.getPlaceAtmospheres().add(placeAtmospheres);
        }

        String[] foodTypesList = datingPlaceDto.getFoodTypes().split(",");
        for (String foodType : foodTypesList) {
            FoodType foodTypes = foodTypeRepo.findByFoodTypeNameAndEnabledTrue(foodType);
            DatingPlaceFoodTypes datingPlaceFoodTypes = new DatingPlaceFoodTypes(datingPlace, foodTypes);
            datingPlace.getFoodTypes().add(datingPlaceFoodTypes);
        }
        datingPlace.getDatingPlaceReviews().add(datingPlaceReview);
        datingPlaceReview.setPlaceId(datingPlace);
        datingPlace.setNeighborHood(datingPlaceDto.getNeighborHood());
        datingPlace.setPlaceDescription(datingPlaceDto.getPlaceDescription());
        datingPlace.setPetsAllowed(datingPlaceDto.getIsPetsAllowed());

        return new DatingPlaceDto(datingPlaceRepo.saveAndFlush(datingPlace));
    }

    @Override
    @Transactional
    public void featurePlaceReview(Integer reviewId) throws Exception {
        List<DatingPlaceReview> reviewList = datingPlaceReviewRepo.findAllByIsFeatured(true);
        if (!reviewList.isEmpty()) {
            reviewList.forEach(datingPlaceReview -> {
                datingPlaceReview.setFeatured(false);
                datingPlaceReviewRepo.saveAndFlush(datingPlaceReview);
            });
        }
        DatingPlaceReview placeReview = datingPlaceReviewRepo.findOne(reviewId);
        if (placeReview != null) {
            placeReview.setFeatured(true);
            placeReview.setReviewApprovalStatus(ApprovalStatus.APPROVED);
            datingPlaceReviewRepo.saveAndFlush(placeReview);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public DatingPlaceReviewDto findFeaturedReview() {
        List<DatingPlaceReview> reviewList = datingPlaceReviewRepo.findAllByIsFeatured(true);
        if (!reviewList.isEmpty()) {
            return new DatingPlaceReviewDto(reviewList.get(0));
        }
        return null;
    }

    //Non Database Related Methods from here on
    private Double getAverageRatingPerReview(Double securityRating, Double parkingRating, Double parkingAccessRating) {
        return (securityRating + parkingRating + parkingAccessRating) / 3;
    }

    private Double getRatingValue(Importance importance) {
        Double rating = 0D;
        switch (importance) {
            case MUST:
                rating = 4D;
                break;
            case SHOULD:
                rating = 3D;
                break;
            case COULD:
                rating = 1D;
                break;
        }
        return rating;
    }

    private Page<DatingPlaceDto> getGoogleResultsCombinedNew(Page<DatingPlaceDto> existingList, Pageable pageable, LocationDto locationDto, Boolean isDescending) {

        try {
            List<DatingPlaceDto> finalList = new ArrayList<>();
            //TODO urgent enhance this
            List<DatingPlace> claimedPlaces = datingPlaceRepo.findAllByExternalPlaceIdIsNotNull();
            List<DatingPlaceDto> googlePlacesListTemp;
            if (locationDto.getRadius() != null && locationDto.getPlaceName() != null) {
                Double radiusInMeters = locationDto.getRadius() * 1609.34;
                if (radiusInMeters <= 30) {
                    googlePlacesListTemp = datingPlaceLocationUtil.getGooglePlacesListByLocationAndText(radiusInMeters, locationDto.getPlaceName().replace(" ", "+"), locationDto.getLatitude(), locationDto.getLongitude());
                } else {
                    googlePlacesListTemp = datingPlaceLocationUtil.getGooglePlacesListByLocationAndText(16000D, locationDto.getPlaceName().replace(" ", "+"), locationDto.getLatitude(), locationDto.getLongitude());
                }
                claimedPlaces.forEach(claimedPlace -> {
                    Predicate<DatingPlaceDto> placeDtoPredicate = p -> Objects.equals(p.getDatingPlacesId(), claimedPlace.getExternalPlaceId());
                    googlePlacesListTemp.removeIf(placeDtoPredicate);
                });
            } else {
                if (isDescending != null) {
                    googlePlacesListTemp = datingPlaceLocationUtil.getGooglePlacesListByLocationAndRating(locationDto.getLatitude(), locationDto.getLongitude(), 16000D, isDescending);
                } else {
                    googlePlacesListTemp = datingPlaceLocationUtil.getGooglePlacesListByLocation(locationDto.getLatitude(), locationDto.getLongitude());
                }
                //googlePlacesListTemp = datingPlaceLocationUtil.getGooglePlacesListByLocation(locationDto.getLatitude(), locationDto.getLongitude());
                claimedPlaces.forEach(claimedPlace -> {
                    Predicate<DatingPlaceDto> placeDtoPredicate = p -> Objects.equals(p.getDatingPlacesId(), claimedPlace.getExternalPlaceId());
                    googlePlacesListTemp.removeIf(placeDtoPredicate);
                });

            }
            List<DatingPlaceDto> googlePlacesList = new ArrayList<>();
            Integer totalGoogleNumber = googlePlacesListTemp.size();
            googlePlacesListTemp.forEach(datingPlaceDto -> {
                int maleCount = 0;
                int femaleCount = 0;
                int bothCount = 0;
                int allCount = 0;
                Double securityRating = 0D;
                Double parkingAccessRating = 0D;
                Double parkingRating = 0D;
                Double overAllRating = 0D;
                List<DatingPlaceReview> reviews = datingPlaceReviewRepo.
                        findAllByGooglePlaceIdAndReviewApprovalStatusAndReviewerType(datingPlaceDto.getDatingPlacesId(), ApprovalStatus.APPROVED, "user");
                Page<Object[]> ageList = datingPlaceReviewRepo.findMaxAgeRange(datingPlaceDto.getDatingPlacesId(), new PageRequest(0, 1));
                DatingAgeRange maxAgeRange = null;
                if (ageList.getTotalElements() != 0) {
                    maxAgeRange = (DatingAgeRange) ageList.getContent().get(0)[0];
                }
                Page<Object[]> priceList = datingPlaceReviewRepo.findMaxPriceRange(datingPlaceDto.getDatingPlacesId(), new PageRequest(0, 1));
                PriceRange maxPriceRange = null;
                if (priceList.getTotalElements() != 0) {
                    maxPriceRange = (PriceRange) priceList.getContent().get(0)[0];
                }
                for (DatingPlaceReview review : reviews) {
                    switch (review.getSuitableGender()) {
                        case BOTH:
                            bothCount += 1;
                            break;
                        case MALE:
                            maleCount += 1;
                            break;
                        case FEMALE:
                            femaleCount += 1;
                            break;
                        case ALL:
                            allCount += 1;
                            break;
                    }
                    securityRating += review.getSecurityRating();
                    parkingAccessRating += review.getParkingAccessRating();
                    parkingRating += review.getParkingRating();
                    overAllRating += review.getOverallRating();
                }
                datingPlaceDto.setMaleCount(maleCount);
                datingPlaceDto.setFemaleCount(femaleCount);
                datingPlaceDto.setBothCount(bothCount);
                datingPlaceDto.setAgeRange(maxAgeRange);
                datingPlaceDto.setPriceRange(maxPriceRange);
                if (securityRating != 0)
                    datingPlaceDto.setSecurityAverage(securityRating / reviews.size());
                if (parkingRating != 0)
                    datingPlaceDto.setParkingAverage(parkingRating / reviews.size());
                if (parkingAccessRating != 0)
                    datingPlaceDto.setTransportAverage(parkingAccessRating / reviews.size());
                if (overAllRating != 0)
                    datingPlaceDto.setOverAllRating(overAllRating / reviews.size());
                googlePlacesList.add(datingPlaceDto);
            });

            if(totalGoogleNumber ==0)
                return existingList;
            //in case the size of the dateplaces list that comes from database is the same as page size return the the data from db
            if (existingList.getContent().size() == pageable.getPageSize()) {
                return new PageImpl<DatingPlaceDto>(existingList.getContent(), pageable, totalGoogleNumber + existingList.getTotalElements());

            }
            //we have  places from db but less than the size so load the remaining from google
            else if (existingList.getContent().size() > 0 && existingList.getContent().size() < pageable.getPageSize() && totalGoogleNumber > 0) {

                Integer pageDiff = pageable.getPageSize() - existingList.getContent().size();
                finalList.addAll(existingList.getContent());
                int limit = googlePlacesList.size() > pageDiff ? pageDiff : googlePlacesList.size() - 1;
                finalList.addAll(googlePlacesList.subList(0, limit));

                //no result from database so load from google
            } else if (existingList.getContent().size() == 0 && totalGoogleNumber > 0) {
                Integer pageDiff = (pageable.getPageNumber() + 1) - existingList.getTotalPages();
                Integer remainingElements = Math.toIntExact(pageable.getPageSize() * existingList.getTotalPages() - existingList.getTotalElements());

                if (pageDiff == 1) {
                    int test = remainingElements + pageable.getPageSize();
                    if (googlePlacesList.size() > 0 && test <= googlePlacesList.size() - 1) {
                        finalList.addAll(googlePlacesList.subList(remainingElements, googlePlacesList.size() > (remainingElements + pageable.getPageSize()) ? remainingElements + pageable.getPageSize() : googlePlacesList.size() - 1));
                    } else {
                        finalList.addAll(googlePlacesList);
                    }
                } else if (pageDiff > 1) {
                    //get the offset
                    int fromElement = remainingElements + pageable.getPageSize() * (pageDiff - 1);
                    int toElement = remainingElements + (pageable.getPageSize() * (pageDiff - 1)) + pageable.getPageSize();

                    if (fromElement < googlePlacesList.size() && toElement < googlePlacesList.size()) {
                        finalList.addAll(googlePlacesList.subList(fromElement, toElement));
                    } else if (fromElement < googlePlacesList.size() && toElement >= googlePlacesList.size()) {
                        finalList.addAll(googlePlacesList.subList(fromElement, googlePlacesList.size() - 1));
                    }
                }
            }
            Page<DatingPlaceDto> page = new PageImpl<>(finalList, pageable, existingList.getTotalElements() + totalGoogleNumber);
            return page;
        } catch (IOException ex) {
            log.error("Can't get data from google ");
            //return the same list
            return existingList;
        }
    }
}
