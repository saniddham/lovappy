package com.realroofers.lovappy.service.questions.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Darrel Rayen on 9/18/18.
 */
@Entity
@Table(name = "daily_questions")
@EqualsAndHashCode
public class DailyQuestion extends BaseEntity<Long> {

    private String question;

    private Date questionDate;

    @OneToMany(mappedBy = "question", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UserResponse> userResponses;

    public DailyQuestion() {
    }

    public DailyQuestion(String question, Date questionDate) {
        this.question = question;
        this.questionDate = questionDate;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Date getQuestionDate() {
        return questionDate;
    }

    public void setQuestionDate(Date questionDate) {
        this.questionDate = questionDate;
    }

    public List<UserResponse> getUserResponses() {
        return userResponses;
    }

    public void setUserResponses(List<UserResponse> userResponses) {
        this.userResponses = userResponses;
    }
}
