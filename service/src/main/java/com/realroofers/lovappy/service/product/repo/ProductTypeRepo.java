package com.realroofers.lovappy.service.product.repo;

import com.realroofers.lovappy.service.product.model.ProductType;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Manoj on 06/02/2018.
 */
public interface ProductTypeRepo extends JpaRepository<ProductType,Integer>{

    ProductType findByTypeName(String name);

    List<ProductType> findAllByActiveIsTrue(Sort sort);

    List<ProductType> findAllByAddedToSurveyIsFalseAndActiveIsTrue();

    List<ProductType> findAllByActiveIsTrue();
}
