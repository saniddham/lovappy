package com.realroofers.lovappy.service.career.dto;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.StringJoiner;

public class EmploymentDto implements Serializable {

    private long id;
    private String businessName;
    private String jobTitle;
    private String resposibilities;
    private Integer reasonForLeaving;
    private Integer startMonth;
    private Integer startYear;
    private Integer endMonth;
    private Integer endYear;
    private Integer stillWorking;

    public EmploymentDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getResposibilities() {
        return resposibilities;
    }

    public void setResposibilities(String resposibilities) {
        this.resposibilities = resposibilities;
    }

    public Integer getReasonForLeaving() {
        return reasonForLeaving;
    }

    public void setReasonForLeaving(Integer reasonForLeaving) {
        this.reasonForLeaving = reasonForLeaving;
    }

    public Integer getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(Integer startMonth) {
        this.startMonth = startMonth;
    }

    public Integer getStartYear() {
        return startYear;
    }

    public void setStartYear(Integer startYear) {
        this.startYear = startYear;
    }

    public Integer getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(Integer endMonth) {
        this.endMonth = endMonth;
    }

    public Integer getEndYear() {
        return endYear;
    }

    public void setEndYear(Integer endYear) {
        this.endYear = endYear;
    }

    public Integer getStillWorking() {
        return stillWorking;
    }

    public void setStillWorking(Integer stillWorking) {
        this.stillWorking = stillWorking;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", EmploymentDto.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("businessName='" + businessName + "'")
                .add("jobTitle='" + jobTitle + "'")
                .add("resposibilities='" + resposibilities + "'")
                .add("reasonForLeaving=" + reasonForLeaving)
                .add("startMonth=" + startMonth)
                .add("startYear=" + startYear)
                .add("endMonth=" + endMonth)
                .add("endYear=" + endYear)
                .add("stillWorking=" + stillWorking)
                .toString();
    }
}
