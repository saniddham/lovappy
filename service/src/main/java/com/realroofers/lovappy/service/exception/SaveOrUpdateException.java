package com.realroofers.lovappy.service.exception;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public class SaveOrUpdateException extends RuntimeException {
    public SaveOrUpdateException(Throwable cause) {
        super(cause);
    }

    public SaveOrUpdateException(String message) {
        super(message);
    }
}
