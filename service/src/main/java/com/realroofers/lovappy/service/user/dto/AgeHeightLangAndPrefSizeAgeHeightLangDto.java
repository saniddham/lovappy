package com.realroofers.lovappy.service.user.dto;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.realroofers.lovappy.service.validator.UserRegister;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Range;

import com.realroofers.lovappy.service.user.model.UserPreference;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.PrefHeight;
import com.realroofers.lovappy.service.user.support.Size;
import com.realroofers.lovappy.service.validator.constraint.CheckSizeByPrefGender;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@CheckSizeByPrefGender
@EqualsAndHashCode
public class AgeHeightLangAndPrefSizeAgeHeightLangDto {

    @NotNull(groups = {UserRegister.class})
    private Gender prefGender;
//
//    @NotNull(message = "Height is required", groups = {UserRegister.class})
//    @Range(min = 1, max = 7, groups = {UserRegister.class})
    private Integer heightFt;

//    @NotNull(message = "Height is required", groups = {UserRegister.class})
//    @Range(min = 1, max = 11, groups = {UserRegister.class})
    private Integer heightIn;
    
    private Double heightCM;

    // Preference Info
    private Boolean penisSizeMatter;
    private Size prefBustSize;
    private Size prefButtSize;

    @NotNull(message = "Min age is required", groups = {UserRegister.class})
    @Min(value = 18, groups = {UserRegister.class})
    private Integer prefMinAge;

    @NotNull(message = "Min age is required", groups = {UserRegister.class})
    @Min(value = 18, groups = {UserRegister.class})
    private Integer prefMaxAge;

    @NotNull(message = "Preferred height is required", groups = {UserRegister.class})
    private PrefHeight prefHeight;

    public AgeHeightLangAndPrefSizeAgeHeightLangDto() {}

    public AgeHeightLangAndPrefSizeAgeHeightLangDto(UserProfile profile, UserPreference pref) {
        if (profile != null) {
            heightFt = profile.getHeightFt() != null ? profile.getHeightFt() : 5;
            heightIn = profile.getHeightIn() != null ? profile.getHeightIn() : 9;
            heightCM = profile.getHeightCm() != null ? profile.getHeightCm() : 0;
        }
        if (pref != null) {
            prefGender = pref.getGender();
            penisSizeMatter = pref.getPenisSizeMatter();
            prefBustSize = pref.getBustSize();
            prefButtSize = pref.getButtSize();
            prefMinAge = pref.getMinAge();
            prefMaxAge = pref.getMaxAge();
            prefHeight = pref.getHeight();
        }
    }

    public AgeHeightLangAndPrefSizeAgeHeightLangDto(Integer heightFt, Integer heightIn, Double heightCM,
                                                    Boolean penisSizeMatter, Size prefBustSize, Size prefButtSize,
                                                    Integer prefMinAge, Integer prefMaxAge, PrefHeight prefHeight,
                                                    Gender prefGender) {
        this.heightFt = heightFt;
        this.heightIn = heightIn;
        this.heightCM = heightCM;
        this.penisSizeMatter = penisSizeMatter;
        this.prefBustSize = prefBustSize;
        this.prefButtSize = prefButtSize;
        this.prefMinAge = prefMinAge;
        this.prefMaxAge = prefMaxAge;
        this.prefHeight = prefHeight;
        this.prefGender = prefGender;
    }

    public Gender getPrefGender() {
        return prefGender;
    }

    public void setPrefGender(Gender prefGender) {
        this.prefGender = prefGender;
    }

    public Integer getHeightFt() {
        return heightFt;
    }

    public void setHeightFt(Integer heightFt) {
        this.heightFt = heightFt;
    }

    public Integer getHeightIn() {
        return heightIn;
    }

    public void setHeightIn(Integer heightIn) {
        this.heightIn = heightIn;
    }

    public Boolean getPenisSizeMatter() {
        return penisSizeMatter;
    }

    public void setPenisSizeMatter(Boolean penisSizeMatter) {
        this.penisSizeMatter = penisSizeMatter;
    }

    public Size getPrefBustSize() {
        return prefBustSize;
    }

    public void setPrefBustSize(Size prefBustSize) {
        this.prefBustSize = prefBustSize;
    }

    public Size getPrefButtSize() {
        return prefButtSize;
    }

    public void setPrefButtSize(Size prefButtSize) {
        this.prefButtSize = prefButtSize;
    }

    public Integer getPrefMinAge() {
        return prefMinAge;
    }

    public void setPrefMinAge(Integer prefMinAge) {
        this.prefMinAge = prefMinAge;
    }

    public Integer getPrefMaxAge() {
        return prefMaxAge;
    }

    public void setPrefMaxAge(Integer prefMaxAge) {
        this.prefMaxAge = prefMaxAge;
    }

    public PrefHeight getPrefHeight() {
        return prefHeight;
    }

    public void setPrefHeight(PrefHeight prefHeight) {
        this.prefHeight = prefHeight;
    }

	public Double getHeightCM() {
		return heightCM;
	}

	public void setHeightCM(Double heightCM) {
		this.heightCM = heightCM;
	}
    
    
}
