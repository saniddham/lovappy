package com.realroofers.lovappy.service.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.realroofers.lovappy.service.validator.constraint.CheckAtLeastOneNotNull;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public class CheckAtLeastOneNotNullValidator implements ConstraintValidator<CheckAtLeastOneNotNull, Object> {
    private static final Logger _LOG = LoggerFactory.getLogger(CheckAtLeastOneNotNullValidator.class);
    private String[] fieldNames;

    public void initialize(CheckAtLeastOneNotNull constraintAnnotation) {
        this.fieldNames = constraintAnnotation.fieldNames();
    }

    public boolean isValid(Object object, ConstraintValidatorContext constraintContext) {
        if (object == null) {
            return true;
        }

        boolean valid = false;
        for (String fieldName : fieldNames) {
            try {
                Object property = PropertyUtils.getProperty(object, fieldName);
                if (property != null && (!(property instanceof String) || StringUtils.isNotBlank((String) property))) {
                    valid = true;
                    break;
                }
            } catch (Exception e) {
                _LOG.warn(e.getMessage());
            }
        }
        return valid;
    }

}