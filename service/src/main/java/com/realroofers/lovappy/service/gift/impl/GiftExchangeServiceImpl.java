package com.realroofers.lovappy.service.gift.impl;

import com.realroofers.lovappy.service.gift.GiftExchangeService;
import com.realroofers.lovappy.service.gift.dto.FilerObject;
import com.realroofers.lovappy.service.gift.dto.GiftExchangeDto;
import com.realroofers.lovappy.service.gift.dto.SaleByType;
import com.realroofers.lovappy.service.gift.dto.TopSellingItem;
import com.realroofers.lovappy.service.gift.dto.mobile.GiftExchangeInboxDto;
import com.realroofers.lovappy.service.gift.model.GiftExchange;
import com.realroofers.lovappy.service.gift.model.QGiftExchange;
import com.realroofers.lovappy.service.gift.repo.GiftExchangeRepo;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.product.BrandCommissionService;
import com.realroofers.lovappy.service.product.RetailerCommissionService;
import com.realroofers.lovappy.service.product.dto.BrandCommissionDto;
import com.realroofers.lovappy.service.product.dto.RetailerCommissionDto;
import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.product.repo.ProductRepo;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import com.realroofers.lovappy.service.vendor.VendorLocationService;
import com.realroofers.lovappy.service.vendor.VendorService;
import com.realroofers.lovappy.service.vendor.dto.VendorDto;
import com.realroofers.lovappy.service.vendor.model.VendorLocation;
import com.realroofers.lovappy.service.vendor.model.VendorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by Eias Altawil on 5/13/17
 */

@Service
public class GiftExchangeServiceImpl implements GiftExchangeService {

    private final GiftExchangeRepo giftExchangeRepo;
    private final ProductRepo giftRepo;
    private final UserRepo userRepo;
    private final BrandCommissionService brandCommissionService;
    private final RetailerCommissionService retailerCommissionService;
    private final VendorService vendorService;
    private final VendorLocationService vendorLocationService;

    @Autowired
    public GiftExchangeServiceImpl(GiftExchangeRepo giftExchangeRepo, ProductRepo giftRepo,
                                   UserRepo userRepo, BrandCommissionService brandCommissionService,
                                   RetailerCommissionService retailerCommissionService, VendorService vendorService, VendorLocationService vendorLocationService) {
        this.giftExchangeRepo = giftExchangeRepo;
        this.giftRepo = giftRepo;
        this.userRepo = userRepo;
        this.brandCommissionService = brandCommissionService;
        this.retailerCommissionService = retailerCommissionService;
        this.vendorService = vendorService;
        this.vendorLocationService = vendorLocationService;
    }

    @Override
    @Transactional
    public GiftExchangeDto sendGift(Integer giftID, Integer fromUserID, Integer toUserID, OrderDto orderDto) {

        Product product = giftRepo.findOne(giftID);
        VendorDto vendorDto = null;
        Double commissionPercentage = 0d;
        Double commissionAmount;
        Double productPrice = 0d;
        if (product != null) {
            productPrice = orderDto.getTotalPrice();
            vendorDto = vendorService.findByUser(product.getCreatedBy().getUserId());
        }

        GiftExchange giftExchange = new GiftExchange();
        giftExchange.setGift(product);
        giftExchange.setTransactionID(orderDto.getTransactionID());
        giftExchange.setCardNumber(orderDto.getCardNo());
        giftExchange.setPaymentMethod(orderDto.getCardType().toUpperCase()+" X"+orderDto.getCardNo());

        if (vendorDto != null) {
            if (vendorDto.getVendorType().equals(VendorType.VENDOR)) {
                BrandCommissionDto currentCommission = brandCommissionService.getCurrentCommission();
                if (currentCommission != null) {
                    commissionPercentage = currentCommission.getCommissionPercentage();
                }
            } else {
                RetailerCommissionDto currentCommission = retailerCommissionService.getCurrentCommission(vendorDto.getCountry());
                if (currentCommission != null) {
                    commissionPercentage = currentCommission.getCommissionPercentage();
                }
            }
        }

        giftExchange.setFromUser(userRepo.findOne(fromUserID));
        giftExchange.setToUser(userRepo.findOne(toUserID));
        giftExchange.setSentAt(new Date());
        giftExchange.setSoldPrice(productPrice);
        giftExchange.setCommissionPercentage(commissionPercentage);
        commissionAmount = (productPrice * commissionPercentage) / 100;
        giftExchange.setCommission(commissionAmount);
        giftExchange.setEarned(productPrice - commissionAmount);
        giftExchange.setRedeemed(false);

        GiftExchange save = giftExchangeRepo.save(giftExchange);

        return new GiftExchangeDto(save);
    }

    @Override
    public GiftExchangeDto findById(Integer giftId) {
        GiftExchange gift = giftExchangeRepo.findOne(giftId);
        return gift != null ? new GiftExchangeDto(gift) : null;
    }

    @Override
    public GiftExchangeDto redeem(Integer giftId, Integer userId, Integer locationId) throws Exception {
        VendorLocation location = vendorLocationService.findById(locationId);

        if (location == null)
            throw new NullPointerException();

        GiftExchange giftExchange = giftExchangeRepo.findOne(giftId);

        Product gift = giftExchange.getGift();

        String redeemCode = giftExchange.getId() +"P"+ gift.getId();

        giftExchange.setRedeemCode(redeemCode);
        giftExchange.setRedeemed(true);
        giftExchange.setRedeemOn(new Date());
        giftExchange.setRedeemAt(location);

        return new GiftExchangeDto(giftExchangeRepo.save(giftExchange));
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<GiftExchangeDto> getAllReceivedGifts(Integer userID) {
        Collection<GiftExchangeDto> gifts = new ArrayList<>();
        List<GiftExchange> giftExchanges = giftExchangeRepo.findAllByToUserOrderBySentAtDesc(userRepo.findOne(userID));

        for (GiftExchange giftExchange : giftExchanges) {
            gifts.add(new GiftExchangeDto(giftExchange));
        }

        return gifts;
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<GiftExchangeDto> getAllSentGifts(Integer userID) {
        Collection<GiftExchangeDto> gifts = new ArrayList<>();
        List<GiftExchange> giftExchanges = giftExchangeRepo.findAllByFromUserOrderBySentAtDesc(userRepo.findOne(userID));

        for (GiftExchange giftExchange : giftExchanges) {
            gifts.add(new GiftExchangeDto(giftExchange));
        }

        return gifts;
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<GiftExchangeDto> findAllByProductUser(Integer userID) {
        Collection<GiftExchangeDto> gifts = new ArrayList<>();
        List<GiftExchange> giftExchanges = giftExchangeRepo.findAllByProductUser(userRepo.findOne(userID));

        for (GiftExchange giftExchange : giftExchanges) {
            gifts.add(new GiftExchangeDto(giftExchange));
        }

        return gifts;
    }

    @Override
    public Page<GiftExchange> findAllByProductUser(Integer userID, Pageable pageable, FilerObject filerObject) {

        String itemName = (filerObject != null && filerObject.getFilterType()!=null && !filerObject.getSearchTerm().equals("")
                && filerObject.getFilterType().equalsIgnoreCase("product")) ? "%"+filerObject.getSearchTerm()+"%" : null;
        String redeemCode = (filerObject != null && filerObject.getFilterType()!=null && !filerObject.getSearchTerm().equals("")
                && filerObject.getFilterType().equalsIgnoreCase("redeem")) ? "%"+filerObject.getSearchTerm()+"%" : null;

        return giftExchangeRepo.findAllPagesByProductUser(userRepo.findOne(userID), itemName, redeemCode, pageable);

    }

    @Override
    public Page<GiftExchange> findAllSales(Pageable pageable, FilerObject filerObject) {

        String itemName = (filerObject != null && filerObject.getFilterType()!=null && !filerObject.getSearchTerm().equals("")
                && filerObject.getFilterType().equalsIgnoreCase("product")) ? "%"+filerObject.getSearchTerm()+"%" : null;
        String redeemCode = (filerObject != null && filerObject.getFilterType()!=null && !filerObject.getSearchTerm().equals("")
                && filerObject.getFilterType().equalsIgnoreCase("redeem")) ? "%"+filerObject.getSearchTerm()+"%" : null;

        return giftExchangeRepo.findAllSales(itemName, redeemCode, pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public List<GiftExchangeDto> getGiftsByFromUserAndToUser(Integer fromUserID, Integer toUserID) {
        List<GiftExchangeDto> giftDtos = new ArrayList<>();
        QGiftExchange qGiftExchange = QGiftExchange.giftExchange;

        Iterable<GiftExchange> gifts = giftExchangeRepo.findAll(
                qGiftExchange.fromUser.userId.eq(fromUserID)
                        .and(qGiftExchange.toUser.userId.eq(toUserID)));

        for (GiftExchange gift : gifts)
            giftDtos.add(new GiftExchangeDto(gift));

        return giftDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public Double getTotalSalesByVendor(Integer userId) {
        Double totalSalesByVendor = giftExchangeRepo.getTotalSalesByVendor(userId);
        if (totalSalesByVendor == null) {
            return 0d;
        } else {
            return totalSalesByVendor;
        }
    }


    @Override
    @Transactional(readOnly = true)
    public Double getTotalCommissionByVendor(Integer userId) {
        Double totalSalesByVendor = giftExchangeRepo.getTotalCommissionByVendor(userId);
        if (totalSalesByVendor == null) {
            return 0d;
        } else {
            return totalSalesByVendor;
        }
    }

    @Override
    public Double getTotalCostForUser(Integer userId) {
        Double totalSalesByVendor = giftExchangeRepo.getTotalCostForUser(userId);
        if (totalSalesByVendor == null) {
            return 0d;
        } else {
            return totalSalesByVendor;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public TopSellingItem getTopSellingItemByVendor(Integer userId) {
        List<TopSellingItem> topSellingItemByVendor = giftExchangeRepo.getTopSellingItemByVendor(userId);

        return topSellingItemByVendor.size() > 0 ? topSellingItemByVendor.get(0) : null;
    }

    @Override
    @Transactional(readOnly = true)
    public List<SaleByType> getSaleByGenderForVendor(Integer userId) {
        return giftExchangeRepo.getSaleByGenderForVendor(userId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<SaleByType> getSaleByCategoryForVendor(Integer userId) {
        return giftExchangeRepo.getSaleByCategoryForVendor(userId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<SaleByType> getSaleByBrandForVendor(Integer userId) {
        return giftExchangeRepo.getSaleByBrandForVendor(userId);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<GiftExchangeInboxDto> getGiftExchangeInboxMobile(Integer userId, PageRequest pageable) {
        Page<GiftExchange> giftList = giftExchangeRepo.findAllByToUser(userRepo.findOne(userId), pageable);

        return giftList.map(giftExchange -> new GiftExchangeInboxDto(giftExchange, "inbox"));
    }

    @Override
    public long getGiftExchangeInboxCount(Integer userId) {
        return giftExchangeRepo.countAllByToUser(userRepo.findOne(userId));
    }

    @Override
    public long getGiftExchangeOutboxCount(Integer userId) {
        return giftExchangeRepo.countAllByFromUser(userRepo.findOne(userId));
    }

    @Override
    @Transactional(readOnly = true)
    public GiftExchangeInboxDto getGiftExchangeInboxMobile(Integer id) {
        GiftExchange gift = giftExchangeRepo.findOne(id);
        return gift != null ? new GiftExchangeInboxDto(gift, "inbox") : null;
    }

    @Override
    public Page<GiftExchangeInboxDto> getGiftExchangeOutboxMobile(Integer userId, PageRequest pageable) {
        Page<GiftExchange> giftList = giftExchangeRepo.findAllByFromUser(userRepo.findOne(userId), pageable);

        return giftList.map(giftExchange -> new GiftExchangeInboxDto(giftExchange, "outbox"));
    }
}
