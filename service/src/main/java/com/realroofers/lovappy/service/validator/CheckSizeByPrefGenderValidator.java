package com.realroofers.lovappy.service.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.realroofers.lovappy.service.user.dto.AgeHeightLangAndPrefSizeAgeHeightLangDto;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.validator.constraint.CheckSizeByPrefGender;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public class CheckSizeByPrefGenderValidator
    implements ConstraintValidator<CheckSizeByPrefGender, Object> {
    private static final Logger _LOG = LoggerFactory.getLogger(CheckSizeByPrefGenderValidator.class);

    @Override
    public void initialize(CheckSizeByPrefGender constraintAnnotation) {
        //
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context) {
    	_LOG.debug(object.toString());
        if (object == null || !(object instanceof AgeHeightLangAndPrefSizeAgeHeightLangDto)) {
            return true;
        }
        AgeHeightLangAndPrefSizeAgeHeightLangDto dto = (AgeHeightLangAndPrefSizeAgeHeightLangDto) object;
        return dto.getPrefGender() == null || Gender.BOTH.equals(dto.getPrefGender())
            || (Gender.FEMALE.equals(dto.getPrefGender()) && dto.getPrefBustSize() != null && dto.getPrefButtSize() != null)
            || (Gender.MALE.equals(dto.getPrefGender()) && dto.getPenisSizeMatter() != null);
    }
}
