package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 10/9/2017.
 */
@EqualsAndHashCode
public class UserProfilePictureAccessDto implements Serializable {

    private Integer userId;
    private String city;
    private Integer age;
    private Gender gender;
    private String language;
    private Boolean access;
    private Integer transactionCount;

    public UserProfilePictureAccessDto() {
    }

    public UserProfilePictureAccessDto(Integer userId, String city, Date birthDate, Gender gender, String language,
                                       Integer transactionCount) {
        this.userId = userId;
        this.city = city;
        setAge(birthDate);
        this.gender = gender;
        this.language=language;
        this.access = true;
        this.transactionCount = transactionCount;
    }

    public UserProfilePictureAccessDto(UserProfile userProfile) {
        this.userId = userProfile.getUserId();
        if (userProfile.getAddress() != null)
            this.city = userProfile.getAddress().getCity();
        else
            this.city = "";

        setAge(userProfile.getBirthDate());
        this.gender = userProfile.getGender();
        setLanguage(userProfile.getSpeakingLanguage());
        this.access = true;
    }

    public Integer getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(Integer transactionCount) {
        this.transactionCount = transactionCount;
    }

    public void setLanguage(Collection<Language> languages) {
        String sl = "";
        if (languages != null) {
            for (Language languageDto : languages) {
                sl = sl.concat(languageDto.getAbbreviation() + ", ");
            }
        }

        this.language = sl.length() > 2 ? sl.substring(0, sl.length() - 2) : "";
    }

    public String getUserDetails() {
        return getAge() + " / " + ((getCity() == null) ? "" : getCity()) + " / " + getLanguage();
    }

    public void setAge(Date birthDate) {
        if (birthDate == null) {
            this.age = 0;
            return;
        }
        Calendar today = Calendar.getInstance();
        Calendar birthDate1 = Calendar.getInstance();
        birthDate1.setTime(birthDate);
        if (birthDate1.after(today)) {
            this.age = 0;
            //throw new IllegalArgumentException("You don't exist yet");
        }
        int todayYear = today.get(Calendar.YEAR);
        int birthDateYear = birthDate1.get(Calendar.YEAR);
        int todayDayOfYear = today.get(Calendar.DAY_OF_YEAR);
        int birthDateDayOfYear = birthDate1.get(Calendar.DAY_OF_YEAR);
        int todayMonth = today.get(Calendar.MONTH);
        int birthDateMonth = birthDate1.get(Calendar.MONTH);
        int todayDayOfMonth = today.get(Calendar.DAY_OF_MONTH);
        int birthDateDayOfMonth = birthDate1.get(Calendar.DAY_OF_MONTH);
        this.age = todayYear - birthDateYear;

        // If birth date is greater than todays date (after 2 days adjustment of leap year) then decrement age one year
        if ((birthDateDayOfYear - todayDayOfYear > 3) || (birthDateMonth > todayMonth)) {
            this.age--;

            // If birth date and todays date are of same month and birth day of month is greater than todays day of month then decrement age
        } else if ((birthDateMonth == todayMonth) && (birthDateDayOfMonth > todayDayOfMonth)) {
            this.age--;
        }
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }


    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Boolean getAccess() {
        return access;
    }

    public void setAccess(Boolean access) {
        this.access = access;
    }
}
