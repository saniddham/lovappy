package com.realroofers.lovappy.service.blog.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;

@Entity(name="email_messages")
@Table(name="email_messages")
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode
public class EmailMessages implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3445462761414808029L;

	@Id
	@Column(name="email_messages_id")
	@GeneratedValue
	private Integer emailMessagesId;
	
	@Column(name="action")
	private String action;		
	
	@Column(name="title")
	private String title;	
	
	@Column(name="body")
	private String body;	
	
	@Column(name="created_on")
	private Date createdOn;

	public EmailMessages(String action, String title, String body) {
		super();
		this.action = action;
		this.title = title;
		this.body = body;
		this.createdOn = new Date();
	}	
	
	public EmailMessages() {}

	public Integer getEmailMessagesId() {
		return emailMessagesId;
	}

	public void setEmailMessagesId(Integer emailMessagesId) {
		this.emailMessagesId = emailMessagesId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public String toString() {
		return "EmailMessages [emailMessagesId=" + emailMessagesId
				+ ", action=" + action + ", title=" + title + ", body=" + body
				+ ", createdOn=" + createdOn + "]";
	}

}

