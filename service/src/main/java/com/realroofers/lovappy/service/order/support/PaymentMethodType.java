package com.realroofers.lovappy.service.order.support;

/**
 * Created by daoud on 11/7/2018.
 */
public enum PaymentMethodType {
    SQUARE, STRIPE;
}
