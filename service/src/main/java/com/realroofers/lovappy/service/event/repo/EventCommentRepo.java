package com.realroofers.lovappy.service.event.repo;

import com.realroofers.lovappy.service.event.model.EventComment;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by darrel on 8/31/17.
 */

public interface EventCommentRepo extends JpaRepository<EventComment,Integer> {
}
