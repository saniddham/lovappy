package com.realroofers.lovappy.service.gallery.repo;

import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.realroofers.lovappy.service.gallery.model.GalleryStorageFile;
/***
 * @author Japheth Odonya
 * 
 * */
public interface GalleryStorageRepo extends JpaRepository<GalleryStorageFile, Integer>, QueryDslPredicateExecutor<GalleryStorageFile> {

    Page<GalleryStorageFile> findByStatus(ApprovalStatus status, Pageable pageable);
}
