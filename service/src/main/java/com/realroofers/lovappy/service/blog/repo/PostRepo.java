package com.realroofers.lovappy.service.blog.repo;

import com.realroofers.lovappy.service.blog.model.BlogTypes;
import com.realroofers.lovappy.service.blog.model.Post;
import com.realroofers.lovappy.service.blog.support.PostStatus;
import com.realroofers.lovappy.service.user.model.Role;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PostRepo extends JpaRepository<Post, Integer>, QueryDslPredicateExecutor<Post> {

	 Post findById(Integer id);

	Post findBySlugUrl(String slugUrl);

	 Page<Post> findAllByOrderByPostDateAsc(Pageable pageRequest);

	 Page<Post> findByState(PostStatus state, Pageable pageRequest);

	 Post findTop1ByStateOrderByViewCountDesc(PostStatus state);

	@Modifying
	@Query(nativeQuery =  true, value = "update post p set p.state = 'INACTIVE' where p.state = 'PENDING'")
	int updateStatues();

	Page<Post> findAll(Pageable pageable);

	Page<Post> findByPostAuthorOrderByIdDesc(User user, Pageable pageable);

	Integer countByPostAuthor(User user);

	@Query("SELECT p FROM com.realroofers.lovappy.service.blog.model.Post p JOIN p.postAuthor.roles r Where r.id.role in :roles AND p.state = :state")
	Page<Post> findByStateAndPostAuthorRolesIn(@Param("state") PostStatus state, @Param("roles") List<Role> roles, Pageable pageable);

	Page<Post> findByStateAndFeatured(PostStatus state, Boolean featured, Pageable pageable);

	Post findTop1ByStateOrderByPostDateDesc(PostStatus state);

	Page<Post> findByStateAndType(PostStatus state, BlogTypes type, Pageable pageable);
}
