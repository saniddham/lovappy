package com.realroofers.lovappy.service.gift.dto.mobile;

import com.realroofers.lovappy.service.gift.model.GiftExchange;
import com.realroofers.lovappy.service.product.dto.mobile.BrandItemDto;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.dto.UserProfileDto;
import com.realroofers.lovappy.service.vendor.model.VendorLocation;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.text.SimpleDateFormat;

@Data
@EqualsAndHashCode
public class GiftExchangeInboxDto implements Serializable {

    private Integer id;
    private String userDetails;
    private String gender;
    private Integer giftId;
    private String sentDate;
    private boolean redeemed;
    private Integer toUser;
    private Integer fromUser;
    private Double price;
    private String orderNumber;
    private String itemName;
    private String imageUrl;
    private String redeemCode;
    private String redeemAt;
    private String paymentMethod;
    private String cardNumber;
    private VendorLocation redeemLocation;
    private BrandItemDto brand;
    private String refNumber;
    private String transactionID;

    public GiftExchangeInboxDto(GiftExchange giftExchange, String type) {
        final SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        this.id = giftExchange.getId();

        UserDto user = new UserDto(type.equalsIgnoreCase("inbox") ? giftExchange.getFromUser() : giftExchange.getToUser());
        UserProfileDto userProfile = user.getUserProfile();


        String age = " ", state = " ", lang = " ";

        if (userProfile != null) {
            age = userProfile.getAge() != null ? userProfile.getAge().toString() : " ";

            if (userProfile.getAddress() != null && userProfile.getAddress().getStateShort() != null) {
                state = userProfile.getAddress().getStateShort();
            }
            if (userProfile.getSpeakingLanguages() != null && !userProfile.getSpeakingLanguages().isEmpty()) {
                lang = userProfile.getSpeakingLanguages().stream().findFirst().map(LanguageDto::getAbbreviation).orElse(" ");
            }
        }
        if(giftExchange.getGift().getBrand()!=null){
            this.brand= new BrandItemDto(giftExchange.getGift().getBrand());
        }
        if(giftExchange.getRefNumber()!=null){
            this.refNumber= giftExchange.getRefNumber();
        }

        this.userDetails = age + "/" + state + "/" + lang;

        this.gender = userProfile.getGender() != null ? userProfile.getGender().toString() : " ";

        this.giftId = giftExchange.getGift().getId();
        this.sentDate = giftExchange.getSentAt() != null ? formatter.format(giftExchange.getSentAt()) : "";
        this.redeemed = giftExchange.getRedeemed() != null ? giftExchange.getRedeemed() : false;
        this.toUser = giftExchange.getToUser().getUserId();
        this.fromUser = giftExchange.getFromUser().getUserId();
        this.paymentMethod = giftExchange.getPaymentMethod();
        this.cardNumber = giftExchange.getCardNumber();


        this.itemName = giftExchange.getGift().getProductName();
        this.orderNumber = giftExchange.getRefNumber();
        this.price = giftExchange.getGift().getPrice().doubleValue();
        if (giftExchange.getGift().getImage1() != null) {
            this.imageUrl = giftExchange.getGift().getImage1().getUrl();
        }
        this.transactionID=giftExchange.getTransactionID();
        this.redeemCode = giftExchange.getRedeemCode();
        if (giftExchange.getRedeemAt() != null) {
            VendorLocation vendorLocation = giftExchange.getRedeemAt();
            this.redeemLocation = vendorLocation;
            StringBuilder stringBuilder = new StringBuilder();

            if (vendorLocation.getCompanyName() != null) {
                stringBuilder.append(vendorLocation.getCompanyName());
            }
            if (vendorLocation.getAddressLine1() != null) {
                stringBuilder.append(" :: ");
                stringBuilder.append(vendorLocation.getAddressLine1());
            }
            if (vendorLocation.getAddressLine2() != null) {
                stringBuilder.append("/ ");
                stringBuilder.append(vendorLocation.getAddressLine2());
            }
            if (vendorLocation.getState() != null) {
                stringBuilder.append("/ ");
                stringBuilder.append(vendorLocation.getState());
            }
            if (vendorLocation.getCountry() != null) {
                stringBuilder.append("/ ");
                stringBuilder.append(vendorLocation.getCountry());
            }

            this.redeemAt = stringBuilder.toString();
        }
    }
}
