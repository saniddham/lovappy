package com.realroofers.lovappy.service.gallery.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.user.model.embeddable.Address;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name = "gallery_storage_file")
@Data
@EqualsAndHashCode
public class GalleryStorageFile extends BaseEntity<Integer>{

	private String name;
	private String url;
	private String bucket;
	private String photoCaption;
//	private String zipCode;
    private ApprovalStatus status;
	@Embedded
	private Address address;
}
