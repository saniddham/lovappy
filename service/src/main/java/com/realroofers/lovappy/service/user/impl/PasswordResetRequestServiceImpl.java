package com.realroofers.lovappy.service.user.impl;

import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.realroofers.lovappy.service.user.PasswordResetRequestService;
import com.realroofers.lovappy.service.user.model.PasswordResetRequest;
import com.realroofers.lovappy.service.user.repo.PasswordResetRequestRepo;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@Service
public class PasswordResetRequestServiceImpl implements PasswordResetRequestService {
    @Autowired
    private PasswordResetRequestRepo resetRequestRepo;

    @Override
    @Transactional
    public int generateAndSaveRequest(Integer userId) {
        String token = UUID.randomUUID().toString();
        PasswordResetRequest request = new PasswordResetRequest();
        request.setUserID(userId);
        request.setToken(token);
        request.setCreatedAt(new Date());
        return resetRequestRepo.save(request).getID();
    }
}
