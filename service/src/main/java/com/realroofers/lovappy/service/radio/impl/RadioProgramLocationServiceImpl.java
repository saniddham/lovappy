package com.realroofers.lovappy.service.radio.impl;

import com.realroofers.lovappy.service.radio.RadioProgramLocationService;
import com.realroofers.lovappy.service.radio.model.RadioProgramLocation;
import com.realroofers.lovappy.service.radio.repo.RadioProgramLocationRepo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Manoj on 17/12/2017.
 */
@Service
public class RadioProgramLocationServiceImpl implements RadioProgramLocationService {

    private final RadioProgramLocationRepo radioProgramLocationRepo;

    public RadioProgramLocationServiceImpl(RadioProgramLocationRepo radioProgramLocationRepo) {
        this.radioProgramLocationRepo = radioProgramLocationRepo;
    }

    @Override
    public List<RadioProgramLocation> findAll() {
        return radioProgramLocationRepo.findAll();
    }

    @Override
    public List<RadioProgramLocation> findAllActive() {
        return radioProgramLocationRepo.findAllByActiveIsTrue();
    }

    @Override
    public RadioProgramLocation create(RadioProgramLocation radioProgramLocation) {
        return radioProgramLocationRepo.saveAndFlush(radioProgramLocation);
    }

    @Override
    public RadioProgramLocation update(RadioProgramLocation radioProgramLocation) {
        return radioProgramLocationRepo.save(radioProgramLocation);
    }

    @Override
    public void delete(Integer id) {
        radioProgramLocationRepo.delete(id);
    }

    @Override
    public RadioProgramLocation findById(Integer id) {
        return radioProgramLocationRepo.findOne(id);
    }
}
