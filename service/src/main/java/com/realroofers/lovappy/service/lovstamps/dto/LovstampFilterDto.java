package com.realroofers.lovappy.service.lovstamps.dto;

import com.realroofers.lovappy.service.user.dto.WithinRadiusDto;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.PrefHeight;
import com.realroofers.lovappy.service.user.support.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;

@Data
@EqualsAndHashCode
public class LovstampFilterDto {

    private Integer userId;
    private Integer ageFrom;
    private Integer ageTo;
    private Gender gender;
    private PrefHeight height;
    private String languages;
    private Boolean penisSizeMatter;
    private Size breastsSize;
    private Size buttsSize;
    private HashMap<String, String> personalities;
    private HashMap<String, String> lifestyles;
    private HashMap<String, String> statuses;
    private WithinRadiusDto withinRadiusDto;


    public LovstampFilterDto() {
    }

    public LovstampFilterDto(Integer userId, Integer ageFrom, Integer ageTo, Gender gender, PrefHeight height,
                             String languages, Boolean penisSizeMatter, Size breastsSize, Size buttsSize,
                             HashMap<String, String> personalities, HashMap<String, String> lifestyles,
                             HashMap<String, String> statuses, WithinRadiusDto withinRadiusDto) {
        this.userId = userId;
        this.ageFrom = ageFrom;
        this.ageTo = ageTo;
        this.gender = gender;
        this.height = height;
        this.languages = languages;
        this.penisSizeMatter = penisSizeMatter;
        this.breastsSize = breastsSize;
        this.buttsSize = buttsSize;
        this.personalities = personalities;
        this.lifestyles = lifestyles;
        this.statuses = statuses;
        this.withinRadiusDto = withinRadiusDto;
    }

}