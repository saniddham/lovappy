package com.realroofers.lovappy.service.datingPlaces.support;

import com.realroofers.lovappy.service.datingPlaces.dto.FoodTypeDto;
import com.realroofers.lovappy.service.datingPlaces.dto.PersonTypeDto;
import com.realroofers.lovappy.service.datingPlaces.dto.PlaceAtmosphereDto;

import java.util.List;

/**
 * Created by Darrel Rayen on 12/9/17.
 */
public interface DatingPlaceSupportService {

    List<FoodTypeDto> getAllFoodTypesEnabled();

    List<FoodTypeDto> getAllFoodTypes();

    FoodTypeDto getFoodTypeById(Integer id);

    FoodTypeDto addFoodType(FoodTypeDto foodTypeDto);

    FoodTypeDto updateFoodType(FoodTypeDto foodTypeDto);

    List<PlaceAtmosphereDto> getAllAtmospheresEnabled();

    List<PlaceAtmosphereDto> findAllAtmospheresByPlaceID(Integer id);

    List<PlaceAtmosphereDto> getAllAtmospheres();

    PlaceAtmosphereDto getAtmosphereById(Integer id);

    PlaceAtmosphereDto addPlaceAtmosphere(PlaceAtmosphereDto placeAtmosphereDto);

    PlaceAtmosphereDto updatePlaceAtmosphere(PlaceAtmosphereDto placeAtmosphereDto);

    PersonTypeDto addPersonType(PersonTypeDto personTypeDto);

    PersonTypeDto updatePersonType(PersonTypeDto personTypeDto);

    List<PersonTypeDto> getAllPersonTypes();

    List<PersonTypeDto> getAllPersonTypesEnabled();

    PersonTypeDto getPersonTypeById(Integer id);
}
