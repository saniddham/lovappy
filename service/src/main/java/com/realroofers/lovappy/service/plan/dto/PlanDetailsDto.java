package com.realroofers.lovappy.service.plan.dto;

import com.realroofers.lovappy.service.plan.model.Feature;
import com.realroofers.lovappy.service.plan.model.Plan;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by hasan on 7/11/2017.
 */
@EqualsAndHashCode
public class PlanDetailsDto {
    @NotNull
    private Integer planID;
    @NotNull(message = "Title is mandatory.")
    private String planName;
    @NotNull(message = "Add a subtitle to the plan.")
    private String subTitle;
    @NotNull
    private Boolean isValid = true;

    private String colorScheme;

    private Collection<Feature> planFeatures;

    private Integer cost;

    private String costDescription;



    public PlanDetailsDto() {
    }

    public PlanDetailsDto(Plan plan) {
        this.planID = plan.getPlanID();
        this.planName = plan.getPlanName();
        this.subTitle = plan.getPlanSubtitle();
        this.colorScheme = plan.getColorScheme();
        this.isValid = plan.getIsValid();
        this.cost = plan.getCost();
        this.costDescription = plan.getCostDescription();

        if (plan.getPlanFeatures() != null && !plan.getPlanFeatures().isEmpty()) {
            this.planFeatures = new ArrayList<>();
            this.planFeatures = plan.getPlanFeatures();
        }
    }

    public PlanDetailsDto(Integer id, String planName, String subTitle, String colorScheme, Collection<Feature> planFeatures, Boolean isValid
    , Integer cost, String costDescription) {
        this.planID = id;
        this.planName = planName;
        this.subTitle = subTitle;
        this.colorScheme = colorScheme;
        this.isValid = isValid;
        this.cost = cost;
        this.costDescription = costDescription;
        if (planFeatures != null && !planFeatures.isEmpty()) {
            this.planFeatures = new ArrayList<>();
            this.planFeatures = planFeatures;
        }
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public String getCostDescription() {
        return costDescription;
    }

    public void setCostDescription(String costDescription) {
        this.costDescription = costDescription;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getColorScheme() {
        return colorScheme;
    }

    public void setColorScheme(String colorScheme) {
        this.colorScheme = colorScheme;
    }

    public Collection<Feature> getPlanFeatures() {
        return planFeatures;
    }


    public Integer getPlanID() {
        return planID;
    }

    public void setPlanID(Integer planID) {
        this.planID = planID;
    }

    public Boolean getIsValid() {
        return isValid;
    }

    public void setIsValid(Boolean valid) {
        isValid = valid;
    }

    public void setPlanFeatures(Collection<Feature> planFeatures) {
        this.planFeatures = planFeatures;
    }
}
