package com.realroofers.lovappy.service.questions.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.questions.model.UserResponse;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Darrel Rayen on 9/18/18.
 */
@Data
@EqualsAndHashCode
public class UserResponseDto implements Serializable {

    private Long responseId;
    private ApprovalStatus approvalStatus;
    private Date responseDate;
    private CloudStorageFileDto audioResponse;
    private Integer userId;
    private Long questionId;
    private String questionString;
    private Integer recordSeconds;
    private Integer languageId;
    private Boolean isPurchased;

    public UserResponseDto() {
    }

    public UserResponseDto(UserResponse response) {
        this.responseId = response.getId();
        this.approvalStatus = response.getResponseApprovalStatus();
        this.responseDate = response.getResponseDate();
        this.audioResponse = new CloudStorageFileDto(response.getAudioResponse());
        this.userId = response.getUser().getUserId();
        this.questionId = response.getQuestion().getId();
        this.questionString = response.getQuestion().getQuestion();
        this.recordSeconds = response.getRecordSeconds();
        this.languageId = response.getLanguage().getId();
    }

    public Boolean getIsPurchased() {
        return isPurchased == null ? false : isPurchased;
    }
}
