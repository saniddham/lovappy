package com.realroofers.lovappy.service.vendor.repo;

import com.realroofers.lovappy.service.vendor.model.VendorBrand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendorBrandRepo extends JpaRepository<VendorBrand, Integer> {
}
