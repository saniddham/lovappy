package com.realroofers.lovappy.service.product.impl;

import com.realroofers.lovappy.service.product.ProductLocationService;
import com.realroofers.lovappy.service.product.dto.ProductLocationDto;
import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.product.model.ProductLocation;
import com.realroofers.lovappy.service.product.repo.ProductLocationRepo;
import com.realroofers.lovappy.service.product.repo.ProductRepo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductLocationServiceImpl implements ProductLocationService {

    private final ProductLocationRepo productLocationRepo;
    private final ProductRepo productRepo;

    public ProductLocationServiceImpl(ProductLocationRepo productLocationRepo, ProductRepo productRepo) {
        this.productLocationRepo = productLocationRepo;
        this.productRepo = productRepo;
    }


    @Transactional(readOnly = true)
    @Override
    public List<ProductLocationDto> findAll() {
        List<ProductLocationDto> productLocationList = new ArrayList<>();
        productLocationRepo.findAll().stream().forEach(productLocation -> productLocationList.add(new ProductLocationDto(productLocation)));
        return productLocationList;
    }

    @Transactional(readOnly = true)
    @Override
    public List<ProductLocationDto> findAllActive() {
        List<ProductLocationDto> productLocationList = new ArrayList<>();
        productLocationRepo.findAll().stream().forEach(productLocation -> productLocationList.add(new ProductLocationDto(productLocation)));
        return productLocationList;
    }

    @Transactional
    @Override
    public ProductLocationDto create(ProductLocationDto productLocationDto) throws Exception {
        return new ProductLocationDto(productLocationRepo.saveAndFlush(new ProductLocation(productLocationDto)));
    }

    @Transactional
    @Override
    public ProductLocationDto update(ProductLocationDto productLocationDto) throws Exception {
        return new ProductLocationDto(productLocationRepo.saveAndFlush(new ProductLocation(productLocationDto)));
    }

    @Transactional
    @Modifying
    @Override
    public void delete(Integer integer) throws Exception {
        productLocationRepo.delete(integer);
    }

    @Transactional(readOnly = true)
    @Override
    public ProductLocationDto findById(Integer integer) {
        ProductLocation productLocation = productLocationRepo.findOne(integer);
        return productLocation != null ? new ProductLocationDto(productLocation) : null;
    }

    @Transactional(readOnly = true)
    @Override
    public ProductLocation findByProductandLocation(Integer productId, Integer locationId) {
        return productLocationRepo.findByProductIdAndLocationId(productId, locationId);
    }

    @Modifying
    @Override
    public void deleteAllByProduct(Integer productId) {
        Product one = productRepo.findOne(productId);

        productLocationRepo.deleteAllByProduct(one);

    }


    @Transactional(readOnly = true)
    @Override
    public List<ProductLocation> findByProduct(Integer productId) {

        Product one = productRepo.findOne(productId);

        return productLocationRepo.findAllByProduct(one);
    }
}
