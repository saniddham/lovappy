package com.realroofers.lovappy.service.blog.repo;

import com.realroofers.lovappy.service.blog.model.Vlogs;
import com.realroofers.lovappy.service.blog.support.PostStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;


/**
 * Created by Daoud Shaheen on 8/8/2017.
 */
public interface VlogsRepository extends JpaRepository<Vlogs, Integer>, QueryDslPredicateExecutor<Vlogs> {

    Page<Vlogs> findByState(PostStatus state, Pageable pageable);

    Vlogs findByIdAndState(Integer id, PostStatus state);

    Vlogs findById(Integer id);
}
