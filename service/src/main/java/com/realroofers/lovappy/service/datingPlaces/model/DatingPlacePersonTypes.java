package com.realroofers.lovappy.service.datingPlaces.model;

import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Darrel Rayen on 8/14/18.
 */
@Entity
@EqualsAndHashCode
public class DatingPlacePersonTypes implements Serializable {

    @EmbeddedId
    private DatingPlacePersonTypeId id;

    @ManyToOne
    @JoinColumn(name = "place_id")
    @MapsId("datingPlaceId")
    private DatingPlace placeId;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "type_id")
    @MapsId("personTypeId")
    private PersonType personType;

    public DatingPlacePersonTypes() {
    }

    public DatingPlacePersonTypes(DatingPlace placeId, PersonType personType) {
        this.placeId = placeId;
        this.personType = personType;
    }

    public DatingPlacePersonTypeId getId() {
        return id;
    }

    public void setId(DatingPlacePersonTypeId id) {
        this.id = id;
    }

    public DatingPlace getPlaceId() {
        return placeId;
    }

    public void setPlaceId(DatingPlace placeId) {
        this.placeId = placeId;
    }

    public PersonType getPersonType() {
        return personType;
    }

    public void setPersonType(PersonType personType) {
        this.personType = personType;
    }
}
