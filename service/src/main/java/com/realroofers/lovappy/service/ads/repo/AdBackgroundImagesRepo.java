package com.realroofers.lovappy.service.ads.repo;

import com.realroofers.lovappy.service.ads.model.AdBackgroundImage;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Eias Altawil
 */
public interface AdBackgroundImagesRepo extends JpaRepository<AdBackgroundImage, Integer> {
}
