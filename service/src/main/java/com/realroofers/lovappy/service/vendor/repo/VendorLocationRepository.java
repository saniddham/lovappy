package com.realroofers.lovappy.service.vendor.repo;

import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.vendor.model.VendorLocation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Manoj on 04/02/2018.
 */
public interface VendorLocationRepository extends JpaRepository<VendorLocation, Integer> {

    @Query("select v from VendorLocation v where v.createdBy=:user and (:store is null or v.storeId like :store) and (:company is null or v.companyName like :company) and (:manager is null or v.storeManager like :manager)")
    Page<VendorLocation> findAllByCreatedByOrderByCompanyName(@Param("user") User createdBy, @Param("store") String store, @Param("company") String company, @Param("manager") String manager, Pageable pageable);

    List<VendorLocation> findAllByActiveIsTrueAndCreatedByOrderByCompanyName(User createdBy);

    List<VendorLocation> findAllByActiveIsTrueOrderByCompanyName();

    List<VendorLocation> findAllByLatitudeIsNull();

    long countAllByCreatedBy(User createdBy);
}
