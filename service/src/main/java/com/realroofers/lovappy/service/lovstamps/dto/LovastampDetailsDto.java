package com.realroofers.lovappy.service.lovstamps.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.lovstamps.model.Lovstamp;
import com.realroofers.lovappy.service.system.dto.LanguageDto;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * Created by Daoud Shaheen on 7/13/2018.
 */
@Data
@ToString
public class LovastampDetailsDto {
    private Integer id;
    private CloudStorageFileDto audioFileCloud;
    private Integer recordSeconds = 0;
    private LanguageDto language;
    private Integer numberOfFemaleListeners;
    private Integer numberOfMaleListeners;
    private Integer numberOfListeners;
    private Date updatedOn;
    private Boolean skip;

    public LovastampDetailsDto() {
    }

    public LovastampDetailsDto(Lovstamp lovstamp) {
        if(lovstamp != null) {
            this.id = lovstamp.getId();
            this.audioFileCloud = new CloudStorageFileDto(lovstamp.getAudioFileCloud());
            this.recordSeconds = lovstamp.getRecordSeconds();
            this.language = new LanguageDto(lovstamp.getLanguage());
            this.numberOfFemaleListeners = lovstamp.getNumberOfFemaleListeners() == null ? 0 : lovstamp.getNumberOfFemaleListeners();
            this.numberOfMaleListeners=  lovstamp.getNumberOfMaleListeners() == null ? 0 : lovstamp.getNumberOfMaleListeners();
            this.numberOfListeners = lovstamp.getNumberOfListeners();
            this.skip = lovstamp.getSkip();
            this.updatedOn = lovstamp.getUpdated()==null?lovstamp.getCreated() : lovstamp.getUpdated();
        }
    }


    public String getFormatedTime() {
        if (recordSeconds != null) {
            int hours = (int) Math.floor(recordSeconds / 3600);
            int mins = (int) Math.floor(recordSeconds / 60 % 60);
            int secs = (int) Math.floor(recordSeconds % 60);
            if (hours > 0) {
                return hours + ":" + (mins >= 9 ? "" + mins : "0" + mins)
                        + ":" + (secs >= 9 ? "" + secs : "0" + secs);
            } else if (mins > 0) {
                return (mins >= 9 ? "" + mins : "0" + mins)
                        + ":" + (secs >= 9 ? "" + secs : "0" + secs);
            }
            return "00:" + (secs >= 9 ? "" + secs : "0" + secs);
        }
        return "00:00";
    }
}
