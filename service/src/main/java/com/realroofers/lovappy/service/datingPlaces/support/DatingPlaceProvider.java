package com.realroofers.lovappy.service.datingPlaces.support;

/**
 * Created by daoud on 12/29/2018.
 */
public enum DatingPlaceProvider {
    LOVAPPY, GOOGLE;
}
