package com.realroofers.lovappy.service.news.impl;

import com.realroofers.lovappy.service.news.NewsCategoryService;
import com.realroofers.lovappy.service.news.NewsCrawlerService;
import com.realroofers.lovappy.service.news.NewsStoryService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Date;

@Service
public class NewsCrawlerServiceImpl implements NewsCrawlerService {

    @Autowired
    private NewsStoryService newsStoryService;

    @Autowired
    private NewsCategoryService newsCategoryService;


    @Transactional
    @Override
    public void getNewsFromOtherSources() {

        org.jsoup.nodes.Document doc = null;
        try {
            doc = (org.jsoup.nodes.Document) Jsoup
                    .connect("https://news.google.com/search?q=love%2Crelationship&hl=en-US&gl=US&ceid=US%3Aen").get();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        Elements elem = doc.select("article.MQsxIb");

        for (Element e : elem) {
            String date = e.select("time[class=WW6dff]").attr("datetime");
            System.out.println(e.select("h3").text());
            System.out.println("https://news.google.com/" + e.select("a").attr("href"));
            System.out.println(e.select("span[class=KbnJ8]").text());
            System.out.println(e.select("p[class=HO8did]").text());
            System.out.println("--------");
            long unixSeconds = 0;
            try {
                unixSeconds = Long.parseLong(date.substring(8).trim());
            } catch (Exception e1) {
                // TODO: handle exception
            }
            Date dt = new Date(unixSeconds * 1000L);

            // save article
            //NewsStoryDto newsStoryDto = new NewsStoryDto();
            /*NewsStory newsStory = new NewsStory();
            newsStory.setTitle(e.select("h3").text());
            newsStory.setAudioVideoUrl("https://news.google.com/" + e.select("a").attr("href"));
            newsStory.setSubmitDate(dt);
            newsStory.setPosted(false);
            newsStory.setSource(e.select("span[class=KbnJ8]").text());
            newsStory.setNewsCategory(newsCategoryService.getNewsCategoryByName("Dating"));
            newsStory.setContain(e.select("p[class=HO8did]").text());

            if (newsStoryService.getNewsStoryByTitle(newsStory.getTitle()) == null) {
                newsStoryService.saveNewsStory(newsStory);
            }*/

        }

    }
}
