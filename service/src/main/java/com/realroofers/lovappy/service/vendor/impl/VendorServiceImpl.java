package com.realroofers.lovappy.service.vendor.impl;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.cloud.repo.CloudStorageRepo;
import com.realroofers.lovappy.service.product.dto.BrandDto;
import com.realroofers.lovappy.service.product.dto.ProductTypeDto;
import com.realroofers.lovappy.service.product.model.Brand;
import com.realroofers.lovappy.service.product.model.ProductType;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.vendor.VendorService;
import com.realroofers.lovappy.service.vendor.dto.BrandRegistrationDto;
import com.realroofers.lovappy.service.vendor.dto.VendorDto;
import com.realroofers.lovappy.service.vendor.model.Vendor;
import com.realroofers.lovappy.service.vendor.model.VendorBrand;
import com.realroofers.lovappy.service.vendor.model.VendorProductType;
import com.realroofers.lovappy.service.vendor.repo.VendorBrandRepo;
import com.realroofers.lovappy.service.vendor.repo.VendorProductTypeRepo;
import com.realroofers.lovappy.service.vendor.repo.VendorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manoj on 18/02/2018.
 */
@Service("vendorService")
public class VendorServiceImpl implements VendorService {

    private final VendorRepository vendorRepository;
    private final VendorProductTypeRepo vendorProductTypeRepo;
    private final VendorBrandRepo vendorBrandRepo;
    private final CloudStorageRepo cloudStorageRepo;

    @Autowired
    public VendorServiceImpl(VendorRepository vendorRepository, VendorProductTypeRepo vendorProductTypeRepo, VendorBrandRepo vendorBrandRepo, CloudStorageRepo cloudStorageRepo) {
        this.vendorRepository = vendorRepository;
        this.vendorProductTypeRepo = vendorProductTypeRepo;
        this.vendorBrandRepo = vendorBrandRepo;
        this.cloudStorageRepo = cloudStorageRepo;
    }

    @Transactional(readOnly = true)
    @Override
    public List<VendorDto> findAll() {
        List<VendorDto> list = new ArrayList<>();
        vendorRepository.findAll().stream().forEach(vendor -> {
            list.add(new VendorDto(vendor));
        });
        return list;
    }

    @Transactional(readOnly = true)
    @Override
    public List<VendorDto> findAllActive() {
        List<VendorDto> list = new ArrayList<>();
        vendorRepository.findAll().stream().forEach(vendor -> {
            list.add(new VendorDto(vendor));
        });
        return list;
    }

    @Transactional
    @Override
    public VendorDto create(VendorDto vendor) throws Exception {
        Vendor vendor1 = new Vendor(vendor);
        CloudStorageFile cloudStorageFile = cloudStorageRepo.findOne(vendor.getCompanyLogo().getId());
        if (cloudStorageFile != null) {
            vendor1.setCompanyLogo(cloudStorageFile);
        }

        return new VendorDto(vendorRepository.saveAndFlush(vendor1));
    }

    @Transactional
    @Override
    public VendorDto update(VendorDto vendor) throws Exception {
        Vendor vendor1 = new Vendor(vendor);
        CloudStorageFile cloudStorageFile = cloudStorageRepo.findOne(vendor.getCompanyLogo().getId());
        if (cloudStorageFile != null) {
            vendor1.setCompanyLogo(cloudStorageFile);
        }

        return new VendorDto(vendorRepository.save(vendor1));
    }

    @Transactional
    @Override
    public void delete(Integer integer) throws Exception {
        VendorDto vendor = findById(integer);
        if (vendor != null) {
            vendor.setActive(false);
            update(vendor);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public VendorDto findById(Integer integer) {
        Vendor vendor = vendorRepository.findOne(integer);
        return vendor != null ? new VendorDto(vendor) : null;
    }

    @Transactional(readOnly = true)
    @Override
    public VendorDto findByUser(Integer user) {
        Vendor vendor = vendorRepository.findByUser_UserId(user);
        return vendor != null ? new VendorDto(vendor) : null;
    }


    @Override
    public List<VendorDto> findByGiftId(Integer giftId) {
        //todo need to discuss this with Jocelyn
        return new ArrayList<>();
    }

    @Transactional
    @Override
    public VendorDto registerBrand(Integer userId, BrandRegistrationDto brandRegistrationDto) throws Exception {
        VendorDto vendor = findByUser(userId);

        VendorDto finalVendor = vendor;
        brandRegistrationDto.getProductTypeList().stream().forEach(productTypeId -> {

            ProductTypeDto productTypeDto = new ProductTypeDto(productTypeId);

            VendorProductType vendorProductType = new VendorProductType();
            vendorProductType.setProductType(new ProductType(productTypeDto));
            vendorProductType.setVendor(new Vendor(finalVendor));
            vendorProductType.setActive(true);
            vendorProductType.setCreatedBy(new User(userId));
            vendorProductTypeRepo.saveAndFlush(vendorProductType);
        });

        brandRegistrationDto.getBrandList().stream().forEach(brandId -> {

            BrandDto brandDto=new BrandDto(brandId);

            VendorBrand vendorBrand = new VendorBrand();
            vendorBrand.setActive(true);
            vendorBrand.setBrand(new Brand(brandDto));
            vendorBrand.setVendor(new Vendor(finalVendor));
            vendorBrand.setCreatedBy(new User(userId));
            vendorBrandRepo.saveAndFlush(vendorBrand);
        });


        vendor.setAddressLine1(brandRegistrationDto.getAddressLine1());
        vendor.setAddressLine2(brandRegistrationDto.getAddressLine2());
        vendor.setCity(brandRegistrationDto.getCity());
        vendor.setZipCode(brandRegistrationDto.getZipCode());
        vendor.setCountry(brandRegistrationDto.getCountry());
        vendor.setRegistrationStep1(true);

        vendor = update(vendor);

        return vendor;
    }
}
