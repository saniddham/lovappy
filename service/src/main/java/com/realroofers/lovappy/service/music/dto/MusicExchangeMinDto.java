package com.realroofers.lovappy.service.music.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.realroofers.lovappy.service.music.model.MusicExchange;
import com.realroofers.lovappy.service.user.dto.UserDto;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EqualsAndHashCode
public class MusicExchangeMinDto implements Serializable {

    private Integer id;
    private MusicMinDto music;
    private UserDto fromUser;
    private UserDto toUser;
    private Date exchangeDate;
    private String revceivedLyrics;
    private Boolean userResponded;
    private Boolean seen;

    public MusicExchangeMinDto() {
    }

    public MusicExchangeMinDto(MusicExchange musicExchange) {
        this.id = musicExchange.getId();
        this.music = new MusicMinDto(musicExchange.getMusic());
        this.fromUser = new UserDto(musicExchange.getFromUser());
        this.toUser = new UserDto(musicExchange.getToUser());
        this.exchangeDate = musicExchange.getExchangeDate();
        this.revceivedLyrics = musicExchange.getLyrics();
        this.seen = musicExchange.getSeen();
        this.userResponded = musicExchange.getUserResponded();
    }

    public String getRevceivedLyrics() {
        return revceivedLyrics;
    }

    public void setRevceivedLyrics(String revceivedLyrics) {
        this.revceivedLyrics = revceivedLyrics;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MusicMinDto getMusic() {
        return music;
    }

    public void setMusic(MusicMinDto music) {
        this.music = music;
    }

    public UserDto getFromUser() {
        return fromUser;
    }

    public void setFromUser(UserDto fromUser) {
        this.fromUser = fromUser;
    }

    public UserDto getToUser() {
        return toUser;
    }

    public void setToUser(UserDto toUser) {
        this.toUser = toUser;
    }

    public Date getExchangeDate() {
        return exchangeDate;
    }

    public void setExchangeDate(Date exchangeDate) {
        this.exchangeDate = exchangeDate;
    }

//    public String getUserToDetails() {
//        return getToUser().getUserProfile().getAge() + " / " + getToUser().getUserProfile().getAddress().getCity() + " / " + getToUser().getUserProfile().getLanguage();
//    }
//
//    public String getUserFromDetails() {
//        return getFromUser().getUserProfile().getAge() + " / " + getFromUser().getUserProfile().getAddress().getCity() + " / " + getFromUser().getUserProfile().getLanguage();
//    }

    public Boolean getUserResponded() {
        return userResponded;
    }

    public void setUserResponded(Boolean userResponded) {
        this.userResponded = userResponded;
    }

    public Boolean getSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
