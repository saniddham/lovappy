package com.realroofers.lovappy.service.datingPlaces.repo;

import com.realroofers.lovappy.service.datingPlaces.model.DatingPlacePersonTypeId;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlacePersonTypes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Darrel Rayen on 10/11/18.
 */
@Repository
public interface DatingPlacePersonTypesRepo extends JpaRepository<DatingPlacePersonTypes,DatingPlacePersonTypeId>{

}
