package com.realroofers.lovappy.service.career.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.realroofers.lovappy.service.career.model.Vacancy;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.StringJoiner;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VacancyDto implements Serializable {

    private long id;
    @NotNull
    private String jobTitle;
    private String description;
    private String city;
    private String state;
    private String status;
    private String country;
    private String openDate;
    private String closeDate;
    private String positionDescription;
    private String seoTitle;
    private String keywords;
    private String skillRequired;
    private String compensation;
    private int noOfApplicants;

    public VacancyDto() {
    }

    public int getNoOfApplicants() {
        return noOfApplicants;
    }

    public void setNoOfApplicants(int noOfApplicants) {
        this.noOfApplicants = noOfApplicants;
    }

    public String getCompensation() {
        return compensation;
    }

    public void setCompensation(String compensation) {
        this.compensation = compensation;
    }

    public VacancyDto(Vacancy dto) {

        this.id = dto.getId();
        this.jobTitle = dto.getJobTitle();
        this.description = dto.getDescription();
        this.city = dto.getCity();
        this.state = dto.getState();
        this.status = dto.getStatus();
        this.country = dto .getCountry();
        this.positionDescription = dto.getPositionDescription();
        this.keywords = dto.getKeywords();
        this.seoTitle = dto.getSeoTitle();
        this.skillRequired = dto.getSkillRequired();
        if (dto.getOpenDate() != null){
            this.openDate = (new SimpleDateFormat("MM/dd/yyyy").format(dto.getOpenDate())).toString();
        } else {
            this.openDate = "Not Specified";
        }

        if (dto.getClosingDate() != null){
            this.closeDate = dto.getClosingDate().toString();
        } else {
            this.closeDate = "Not Specified";
        }
        this.noOfApplicants = dto.getApplicants().size();
        this.compensation = dto.getCompensation();
    }

    public String getSkillRequired() {
        return skillRequired;
    }

    public void setSkillRequired(String skillRequired) {
        this.skillRequired = skillRequired;
    }

    public String getPositionDescription() {
        return positionDescription;
    }

    public void setPositionDescription(String positionDescription) {
        this.positionDescription = positionDescription;
    }

    public String getSeoTitle() {
        return seoTitle;
    }

    public void setSeoTitle(String seoTitle) {
        this.seoTitle = seoTitle;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", VacancyDto.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("jobTitle='" + jobTitle + "'")
                .add("description='" + description + "'")
                .add("city='" + city + "'")
                .add("state='" + state + "'")
                .add("status='" + status + "'")
                .add("country='" + country + "'")
                .add("openDate='" + openDate + "'")
                .add("closeDate='" + closeDate + "'")
                .toString();
    }
}
