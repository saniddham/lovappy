package com.realroofers.lovappy.service.tips;


import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.tips.dto.LoveTipDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Daoud Shaheen on 1/1/2018.
 *
 *  love tips services
 */
public interface TipsService extends AbstractService<LoveTipDto, Integer>{

    Page<LoveTipDto> getAll(Pageable pageable);

    Page<LoveTipDto> getAllByActive(Pageable pageable, Boolean active);

    void setActive(Integer I, boolean active);
}
