package com.realroofers.lovappy.service.cloud.repo;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import org.springframework.data.repository.CrudRepository;

public interface CloudStorageRepo extends CrudRepository<CloudStorageFile, Long> {
}
