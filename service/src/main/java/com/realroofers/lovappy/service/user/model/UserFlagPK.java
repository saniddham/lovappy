package com.realroofers.lovappy.service.user.model;

import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 8/2/2017.
 */
@Embeddable
@EqualsAndHashCode
public class UserFlagPK implements Serializable {

    private User flagBy;

    private User flagTo;

    public UserFlagPK() {
    }

    public UserFlagPK(User flagBy, User flagTo) {
        this.flagBy = flagBy;
        this.flagTo = flagTo;
    }


    @ManyToOne
    @JoinColumn(name = "flag_by")
    public User getFlagBy() {
        return flagBy;
    }

    public void setFlagBy(User flagBy) {
        this.flagBy = flagBy;
    }

    @ManyToOne
    @JoinColumn(name = "flag_to")
    public User getFlagTo() {
        return flagTo;
    }

    public void setFlagTo(User flagTo) {
        this.flagTo = flagTo;
    }
}
