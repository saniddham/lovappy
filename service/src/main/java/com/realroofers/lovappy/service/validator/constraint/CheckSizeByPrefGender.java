package com.realroofers.lovappy.service.validator.constraint;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.realroofers.lovappy.service.validator.CheckSizeByPrefGenderValidator;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@Target({TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = CheckSizeByPrefGenderValidator.class)
@Documented
public @interface CheckSizeByPrefGender {
    String message() default "{com.realroofers.lovappy.service.validator.constraint.CheckAgeHeightLangAndPrefSizeAgeHeightLangDto}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
