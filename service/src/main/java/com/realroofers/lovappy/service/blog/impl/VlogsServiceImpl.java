package com.realroofers.lovappy.service.blog.impl;

import com.realroofers.lovappy.service.blog.VlogsService;
import com.realroofers.lovappy.service.blog.dto.VlogsDto;
import com.realroofers.lovappy.service.blog.model.Vlogs;
import com.realroofers.lovappy.service.blog.repo.VlogsRepository;
import com.realroofers.lovappy.service.blog.support.PostStatus;
import com.realroofers.lovappy.service.core.VideoProvider;
import com.realroofers.lovappy.service.util.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 * Created by Daoud Shaheen on 8/8/2017.
 */

@Service
public class VlogsServiceImpl implements VlogsService {

    @Autowired
    private VlogsRepository vlogsRepository;

    @Transactional
    @Override
    public VlogsDto createVlogs(VlogsDto vlogsDto) {
        Vlogs vlogs = new Vlogs();
        vlogs.setTitle(vlogsDto.getTitle());
        vlogs.setCaptions(vlogsDto.getCaptions());
        vlogs.setKeywords(vlogsDto.getKeywords());
        vlogs.setState(PostStatus.INACTIVE);
        if (StringUtils.hasText(vlogsDto.getVideoUrl()) && vlogsDto.getVideoUrl().length() > 1) {
            setVideo(vlogsDto, vlogs);
        }
        return new VlogsDto(vlogsRepository.save(vlogs));
    }

    private void setVideo(VlogsDto vlogsDto, Vlogs vlogs) {
        String link = vlogsDto.getVideoUrl();
        String videoId = CommonUtils.findYoutubeVideoId(link);
        if (!StringUtils.isEmpty(videoId)) {
            String newLink = "//www.youtube.com/embed/" + videoId;
            vlogs.setVideoLink(newLink);
            vlogs.setVideoId(videoId);
            vlogs.setVideoProvider(VideoProvider.YOUTUBE);
        } else {
            vlogs.setVideoLink(vlogsDto.getVideoUrl());
            vlogs.setVideoProvider(VideoProvider.OTHER);
        }
    }

    @Transactional
    @Override
    public VlogsDto updateVlogs(VlogsDto vlogsDto) {
        Vlogs vlogs = vlogsRepository.findById(vlogsDto.getId());
        if (vlogs != null) {

            if (!StringUtils.isEmpty(vlogsDto.getTitle()))
                vlogs.setTitle(vlogsDto.getTitle());

            if (!StringUtils.isEmpty(vlogsDto.getCaptions()))
                vlogs.setCaptions(vlogsDto.getCaptions());

            if (!StringUtils.isEmpty(vlogsDto.getKeywords()))
                vlogs.setKeywords(vlogsDto.getKeywords());

            if (!StringUtils.hasText(vlogsDto.getVideoUrl()) && vlogsDto.getVideoUrl().length() > 1) {
                setVideo(vlogsDto, vlogs);
            }

            return new VlogsDto(vlogsRepository.save(vlogs));
        }
        return null;
    }

    @Transactional(readOnly = true)
    @Override
    public Page<VlogsDto> getVlogsList(int page, int limit) {
        Pageable pageRequest = new PageRequest(page - 1, limit, Sort.Direction.DESC, "created");

        return vlogsRepository.findAll(pageRequest).map(VlogsDto::new);
    }

    @Override
    public Page<VlogsDto> getSortedVlogsList(int page, int limit, Sort.Direction sorting) {
        Pageable pageRequest = null;
        if(sorting != null) {
            pageRequest =  new PageRequest(page - 1, limit, sorting, "created");
        } else {
            pageRequest =  new PageRequest(page - 1, limit, Sort.Direction.DESC, "created");
        }
        return vlogsRepository.findAll(pageRequest).map(VlogsDto::new);
    }
    @Transactional(readOnly = true)
    @Override
    public Page<VlogsDto> getSortedVlogsList(Pageable pageable) {
            return vlogsRepository.findAll(pageable).map(VlogsDto::new);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<VlogsDto> getActiveVlogsList(int page, int limit) {
        Pageable pageRequest = new PageRequest(page - 1, limit, Sort.Direction.DESC, "created");

        return vlogsRepository.findByState(PostStatus.ACTIVE, pageRequest).map(VlogsDto::new);
    }


    @Transactional
    @Override
    public void deleteVlogsById(Integer vlogsId) {
        Vlogs vlogs = vlogsRepository.findOne(vlogsId);
        if (vlogs != null) {
            vlogsRepository.delete(vlogs);
        }
    }

    @Transactional
    @Override
    public void updateStateById(PostStatus state, Integer vlogId) {
        Vlogs vlogs = vlogsRepository.findOne(vlogId);
        if (vlogs != null) {
            vlogs.setState(state);
            vlogsRepository.save(vlogs);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public VlogsDto getVlogsById(Integer vlogsId) {
        return new VlogsDto(vlogsRepository.findOne(vlogsId));
    }

    @Transactional(readOnly = true)
    @Override
    public VlogsDto getActiveVlogsById(Integer vlogsId) {
        return new VlogsDto(vlogsRepository.findByIdAndState(vlogsId, PostStatus.ACTIVE));
    }

    @Transactional
    @Override
    public void updateViewCount(Integer viewCount, Integer ID) {
        Vlogs vlogs = vlogsRepository.findOne(ID);
        if (vlogs != null) {
            vlogs.setViewCount(vlogs.getViewCount()==null? 1 : vlogs.getViewCount() + 1);
            vlogsRepository.save(vlogs);
        }
    }
}
