/**
 * Package com.realroofers.lovappy.service.user is a package all user related classes (DAO, entity and service).
 */
package com.realroofers.lovappy.service.user;