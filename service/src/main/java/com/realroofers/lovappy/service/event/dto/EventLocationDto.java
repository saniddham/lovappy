package com.realroofers.lovappy.service.event.dto;

import com.realroofers.lovappy.service.event.model.embeddable.EventLocation;
import lombok.EqualsAndHashCode;
import org.springframework.beans.BeanUtils;

/**
 * Created by Tejaswi Venupalli on 9/07/17
 */
@EqualsAndHashCode
public class EventLocationDto {

    private String zipCode;
    private String country;
    private String state; // or region
    private String stateShort;
    private String province; // or county
    private String city;
    private String streetNumber;
    private String fullAddress;
    private Double latitude;
    private Double longitude;
    private String premise;
    private String route;
    private String floorRoom;
    private String term;


    public EventLocationDto(){


    }
    public EventLocationDto(Double latitude, Double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public EventLocationDto(String zipCode, String country, String state, String stateShort, String province, String city, String streetNumber, String fullAddress, Double latitude, Double longitude, String premise, String route, String floorRoom) {
        this.zipCode = zipCode;
        this.country = country;
        this.state = state;
        this.stateShort = stateShort;
        this.province = province;
        this.city = city;
        this.streetNumber = streetNumber;
        this.fullAddress = fullAddress;
        this.latitude = latitude;
        this.longitude = longitude;
        this.premise = premise;
        this.route = route;
        this.floorRoom = floorRoom;
    }

    public static EventLocationDto toEventLocationDto(EventLocation location){
        EventLocationDto locationDto = new EventLocationDto();
        if(location != null){
            BeanUtils.copyProperties(location, locationDto);
        }
        return locationDto;
    }

    public static EventLocation toEventLocation(EventLocationDto eventLocationDto){
        EventLocation eventLocation = new EventLocation();
        eventLocation.setZipCode(eventLocationDto.getZipCode());
        eventLocation.setCountry(eventLocationDto.getCountry());
        eventLocation.setState(eventLocationDto.getState());
        eventLocation.setStateShort(eventLocationDto.getStateShort());
        eventLocation.setProvince(eventLocationDto.getProvince());
        eventLocation.setCity(eventLocationDto.getCity());
        eventLocation.setStreetNumber(eventLocationDto.getStreetNumber());
        eventLocation.setFullAddress(eventLocationDto.getFullAddress());
        eventLocation.setLatitude(eventLocationDto.getLatitude());
        eventLocation.setLongitude(eventLocationDto.getLongitude());
        eventLocation.setPremise(eventLocationDto.getPremise());
        eventLocation.setRoute(eventLocationDto.getRoute());
        eventLocation.setFloorRoom(eventLocationDto.getFloorRoom());
        return eventLocation;
    }
    public EventLocation toEventLocation(){
        EventLocation location = new EventLocation();
        BeanUtils.copyProperties(this,location);
        return location;
    }


    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateShort() {
        return stateShort;
    }

    public void setStateShort(String stateShort) {
        this.stateShort = stateShort;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getPremise() {
        return premise;
    }

    public void setPremise(String premise) {
        this.premise = premise;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getFloorRoom() {
        return floorRoom;
    }

    public void setFloorRoom(String floorRoom) {
        this.floorRoom = floorRoom;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventLocationDto that = (EventLocationDto) o;

        if (zipCode != null ? !zipCode.equals(that.zipCode) : that.zipCode != null) return false;
        if (country != null ? !country.equals(that.country) : that.country != null) return false;
        if (state != null ? !state.equals(that.state) : that.state != null) return false;
        if (province != null ? !province.equals(that.province) : that.province != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (streetNumber != null ? !streetNumber.equals(that.streetNumber) : that.streetNumber != null) return false;
        if (fullAddress != null ? !fullAddress.equals(that.fullAddress) : that.fullAddress != null) return false;
        if (latitude != null ? !latitude.equals(that.latitude) : that.latitude != null) return false;
        return longitude != null ? longitude.equals(that.longitude) : that.longitude == null;
    }

    @Override
    public int hashCode() {
        int result = zipCode != null ? zipCode.hashCode() : 0;
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (province != null ? province.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (streetNumber != null ? streetNumber.hashCode() : 0);
        result = 31 * result + (fullAddress != null ? fullAddress.hashCode() : 0);
        result = 31 * result + (latitude != null ? latitude.hashCode() : 0);
        result = 31 * result + (longitude != null ? longitude.hashCode() : 0);
        return result;
    }
}
