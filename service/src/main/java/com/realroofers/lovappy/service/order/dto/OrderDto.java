package com.realroofers.lovappy.service.order.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.realroofers.lovappy.service.order.model.OrderDetail;
import com.realroofers.lovappy.service.order.model.ProductOrder;
import com.realroofers.lovappy.service.user.dto.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by Eias Altawil on 5/11/17
 */
@Data
@ToString
@EqualsAndHashCode
public class OrderDto {

    private Long id;
    @JsonIgnore
    private UserDto user;
    private Date date;
    @JsonIgnore
    private Collection<OrderDetailDto> orderDetails;
    private String transactionID;
    private Double totalPrice = 0.0;
    private CouponDto coupon;
    private String cardType;
    private String cardNo;

    public OrderDto() {
    }

    public OrderDto(ProductOrder productOrder) {
        this.id = productOrder.getId();
        this.user = new UserDto(productOrder.getUser());
        this.date = productOrder.getDate();
        this.transactionID = productOrder.getTransactionID();
        this.cardNo=productOrder.getCardNo();
        this.cardType=productOrder.getCardType();

        if(productOrder.getCoupon() != null)
            this.coupon = new CouponDto(productOrder.getCoupon());



        if(productOrder.getOrdersDetails() != null){
            this.orderDetails = new ArrayList<>();

            for (OrderDetail orderDetail : productOrder.getOrdersDetails()){
                this.orderDetails.add(new OrderDetailDto(orderDetail));
                this.totalPrice += orderDetail.getPrice();
            }


            //Calculate total amount
            if (productOrder.getCoupon() != null){
                Double discountValue = this.totalPrice * coupon.getDiscountPercent() * 0.01;
                this.totalPrice -= discountValue;
            }
        }
    }
}
