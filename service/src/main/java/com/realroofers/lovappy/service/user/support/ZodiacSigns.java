package com.realroofers.lovappy.service.user.support;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 2/22/2018.
 */
public enum ZodiacSigns {
    capricorn, aquarius, pisces, aries, taurus, gemini, cancer, leo, virgo, libra, scorpio,
    sagittarius, none;


    public static ZodiacSigns getZodiac(Date date) {
        if(date == null)
            return null;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return getZodiac(cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
    }
    public static ZodiacSigns getZodiac(int month, int day) {
        if ((month == 11) && (day >= 22) || (month == 0) && (day <= 19)) {
            return capricorn;
        } else if ((month == 0) && (day >= 20) || (month == 1) && (day <= 18)) {
            return aquarius;
        } else if ((month == 1) && (day >= 19) || (month == 2) && (day <= 20)) {
            return pisces;
        } else if ((month == 2) && (day >= 21) || (month == 3) && (day <= 19)) {
            return aries;
        } else if ((month == 3) && (day >= 20) || (month == 4) && (day <= 20)) {
            return taurus;
        } else if ((month == 4) && (day >= 21) || (month == 5) && (day <= 20)) {
            return gemini;
        } else if ((month == 5) && (day >= 21) || (month == 6) && (day <= 22)) {
            return cancer;
        } else if ((month == 6) && (day >= 23) || (month == 7) && (day <= 22)) {
            return leo;
        } else if ((month == 7) && (day >= 23) || (month == 8) && (day <= 21)) {
            return virgo;
        } else if ((month == 8) && (day >= 22) || (month == 9) && (day <= 21)) {
            return libra;
        } else if ((month == 9) && (day >= 24) || (month == 10) && (day <= 22)) {
            return scorpio;
        } else if ((month == 10) && (day >= 23) || (month == 11) && (day <= 21)) {
            return sagittarius;
        } else {
            return none;

        }

    }
}
