package com.realroofers.lovappy.service.event.model;

import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Tejaswi Venupalli on 8/28/17
 */
@Entity
@Table(name ="event_comment")
@EqualsAndHashCode
public class EventComment {

    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "event_id")
    private Event eventId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User userId;

    @Type(type = "text")
    private String commentDescription;

    @Column(columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date commentPostedDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Event getEventId() {
        return eventId;
    }

    public void setEventId(Event eventId) {
        this.eventId = eventId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public String getCommentDescription() {
        return commentDescription;
    }

    public void setCommentDescription(String commentDescription) {
        this.commentDescription = commentDescription;
    }

    public Date getCommentPostedDate() {
        return commentPostedDate;
    }

    public void setCommentPostedDate(Date commentPostedDate) {
        this.commentPostedDate = commentPostedDate;
    }
}
