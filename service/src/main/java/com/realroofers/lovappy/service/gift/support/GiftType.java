package com.realroofers.lovappy.service.gift.support;

/**
 * Created by Eias Altawil on 5/21/17
 */
public enum GiftType {
    ONLINE("online"), TANGIBLE("tangible");

    String text;

    GiftType(String text) {
        this.text = text;
    }
}
