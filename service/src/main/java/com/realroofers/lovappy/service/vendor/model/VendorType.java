package com.realroofers.lovappy.service.vendor.model;

import java.io.Serializable;

/**
 * Created by Manoj on 22/02/2018.
 */
public enum VendorType implements Serializable{

    BRAND, VENDOR
}
