package com.realroofers.lovappy.service.datingPlaces.support;

/**
 * Created by Darrel Rayen on 1/4/18.
 */
public enum NeighborHood {
    RURAL("Rural"),
    URBAN("Urban"),
    SUBURBAN("Suburban"),
    NA("NA");

    String text;

    NeighborHood(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
