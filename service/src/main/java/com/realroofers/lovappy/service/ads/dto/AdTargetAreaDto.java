package com.realroofers.lovappy.service.ads.dto;

import com.realroofers.lovappy.service.ads.model.AdTargetArea;
import com.realroofers.lovappy.service.user.dto.AddressDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by Eias Altawil on 7/9/2017
 */
@Data
@ToString
@EqualsAndHashCode
public class AdTargetAreaDto {
    private Integer id;
    private AddressDto address;

    public AdTargetAreaDto() {
    }

    public AdTargetAreaDto(AdTargetArea adTargetArea) {
        this.id = adTargetArea.getId();
        this.address = AddressDto.toAddressDto(adTargetArea.getAddress());
    }

    public AdTargetAreaDto(Integer id, AddressDto address) {
        this.id = id;
        this.address = address;
    }
}
