package com.realroofers.lovappy.service.ads.support;

public enum AdType {
    ADVERTISEMENT, COMEDY, NEWS, PODCAST, MUSIC, AUDIO_BOOK
}
