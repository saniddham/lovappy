package com.realroofers.lovappy.service.cms.dto;

import com.realroofers.lovappy.service.cms.model.VideoWidget;
import com.realroofers.lovappy.service.core.VideoProvider;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * Created by Daoud Shaheen on 6/10/2017.
 */
@Data
@EqualsAndHashCode
public class VideoWidgetDTO implements Serializable {

    private Integer id;
    private String name;
    private String url;
    private String providerId;
    private VideoProvider provider;

    public VideoWidgetDTO(Integer id, String name, String url, VideoProvider provider) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.provider = provider;
    }
    public VideoWidgetDTO(Integer id, String name, String url, VideoProvider provider, String providerId) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.provider = provider;
        this.providerId = providerId;
    }
    public VideoWidgetDTO() {
    }

    public VideoWidgetDTO(VideoWidget videoWidget) {
        this.id = videoWidget.getId();
        this.name = videoWidget.getName();
        this.url = videoWidget.getUrl();
        this.providerId = videoWidget.getProviderId();
        this.provider = videoWidget.getProvider();

    }

}
