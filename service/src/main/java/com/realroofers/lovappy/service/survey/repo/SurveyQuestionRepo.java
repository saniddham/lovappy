package com.realroofers.lovappy.service.survey.repo;

import com.realroofers.lovappy.service.survey.model.SurveyQuestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface SurveyQuestionRepo extends JpaRepository<SurveyQuestion, Integer> {
}
