package com.realroofers.lovappy.service.couples.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.validator.UserRegister;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 8/4/2018.
 */
@Data
@ToString
public class CoupleProfileRequest implements Serializable {
    @NotNull(message = "Selecting your gender is mandatory.", groups = {UserRegister.class})
    private Gender gender;
    @NotNull(message = "Selecting partner gender is mandatory..", groups = {UserRegister.class})
    private Gender coupleGender;
    @NotNull(message = "Adding your birth date is mandatory.", groups = {UserRegister.class})
    private Date birthDate;
    @NotNull(message = "Adding your anniversary date is mandatory.", groups = {UserRegister.class})
    private Date anniversaryDate;
    @NotNull(message = "Providing years is mandatory.", groups = {UserRegister.class})
    private Integer yearsTogether;

    public CoupleProfileRequest() {
    }
}
