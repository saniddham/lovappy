package com.realroofers.lovappy.service.order.dto;

import com.realroofers.lovappy.service.ads.dto.AdDto;
import com.realroofers.lovappy.service.datingPlaces.dto.DatingPlaceDto;
import com.realroofers.lovappy.service.event.dto.EventDto;
import com.realroofers.lovappy.service.gift.dto.GiftDto;
import com.realroofers.lovappy.service.music.dto.MusicDto;
import com.realroofers.lovappy.service.order.model.OrderDetail;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.privatemessage.dto.PrivateMessageDto;
import com.realroofers.lovappy.service.product.dto.ProductDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by Eias Altawil on 5/11/17
 */
@Data
@ToString
@EqualsAndHashCode
public class OrderDetailDto {

    private Long id;
    private Long originalId;

    private OrderDetailType type;
    private Double price;
    private Integer quantity;

    public OrderDetailDto() {
    }

    public OrderDetailDto(OrderDetail orderDetail) {
        this.id = orderDetail.getId();
        this.originalId = orderDetail.getOriginalId();
        this.type = orderDetail.getType();
        this.price = orderDetail.getPrice();
        this.quantity = orderDetail.getQuantity();
    }
}
