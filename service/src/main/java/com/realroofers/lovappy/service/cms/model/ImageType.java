package com.realroofers.lovappy.service.cms.model;

/**
 * Created by Daoud Shaheen on 6/11/2017.
 */
public enum ImageType {
    BANNER, BACKGROUND;
}
