package com.realroofers.lovappy.service.product.dto;

import com.realroofers.lovappy.service.product.model.ProductType;
import com.realroofers.lovappy.service.user.dto.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class ProductTypeDto {

    private Integer id;
    private String typeName;
    private String typeDescription;
    private Boolean active = true;
    private Boolean addedToSurvey = true;
    private UserDto createdBy;

    public ProductTypeDto() {
    }

    public ProductTypeDto(Integer id) {
        this.id = id;
    }

    public ProductTypeDto(ProductType productType) {
        if(productType.getId()!=null) {
            this.id = productType.getId();
        }
        this.typeName = productType.getTypeName();
        this.typeDescription = productType.getTypeDescription();
        this.active = productType.getActive();
        this.addedToSurvey = productType.getAddedToSurvey();
        this.createdBy = new UserDto(productType.getCreatedBy());
    }
}
