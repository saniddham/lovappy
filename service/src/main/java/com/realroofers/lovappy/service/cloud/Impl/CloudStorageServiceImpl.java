package com.realroofers.lovappy.service.cloud.Impl;

import com.realroofers.lovappy.service.cloud.CloudStorageService;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.dto.ImageThumbnailDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.cloud.model.ImageThumbnail;
import com.realroofers.lovappy.service.cloud.repo.CloudStorageRepo;
import com.realroofers.lovappy.service.cloud.support.FileApprovalStatus;
import com.realroofers.lovappy.service.cloud.support.FileApprovalValue;
import com.realroofers.lovappy.service.lovstamps.model.Lovstamp;
import com.realroofers.lovappy.service.lovstamps.repo.LovstampRepo;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.repo.UserProfileRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;


@Service
public class CloudStorageServiceImpl implements CloudStorageService {

    private final CloudStorageRepo cloudStorageRepo;
    private final LovstampRepo lovstampRepo;
	private final UserProfileRepo userProfileRepo;
    private static final Logger LOGGER = LoggerFactory.getLogger(CloudStorageService.class);

    @Autowired
    public CloudStorageServiceImpl(CloudStorageRepo cloudStorageRepo, LovstampRepo lovstampRepo, UserProfileRepo userProfileRepo) {
        this.cloudStorageRepo = cloudStorageRepo;
		this.lovstampRepo = lovstampRepo;
		this.userProfileRepo = userProfileRepo;
	}

    @Override
    @Transactional
    public CloudStorageFileDto add(CloudStorageFileDto cloudStorageFileDto) {
        CloudStorageFile cloudStorageFile = new CloudStorageFile();
        cloudStorageFile.setName(cloudStorageFileDto.getName());
		cloudStorageFile.setApproved(false);
		cloudStorageFile.setRejected(false);
        cloudStorageFile.setUrl(cloudStorageFileDto.getUrl());
        cloudStorageFile.setBucket(cloudStorageFileDto.getBucket());

        if (cloudStorageFileDto.getThumbnails() != null) {
            for (ImageThumbnailDto thumbnail : cloudStorageFileDto.getThumbnails()) {
                ImageThumbnail imageThumbnail = new ImageThumbnail();

                CloudStorageFile file = new CloudStorageFile();
                file.setName(thumbnail.getFile().getName());
                file.setUrl(thumbnail.getFile().getUrl());
                file.setBucket(thumbnail.getFile().getBucket());

                imageThumbnail.setFile(file);
                imageThumbnail.setOriginalFile(cloudStorageFile);
                imageThumbnail.setSize(thumbnail.getSize());

                if(cloudStorageFile.getThumbnails() == null)
                    cloudStorageFile.setThumbnails(new ArrayList<>());

                cloudStorageFile.getThumbnails().add(imageThumbnail);
            }
        }

        CloudStorageFile save = cloudStorageRepo.save(cloudStorageFile);

        return save == null ? null : new CloudStorageFileDto(save);
    }


	@Override
	@Transactional
	public CloudStorageFile update(CloudStorageFile cloudStorageFile) {
		cloudStorageFile = cloudStorageRepo.save(cloudStorageFile);
		return cloudStorageFile;
	}


	@Override
	@Transactional
	public CloudStorageFile findById(Long id) {
		return cloudStorageRepo.findOne(id);
	}


	@Override
	@Transactional
	public Boolean approveUserFile(Integer userId, Integer currentUserId, FileApprovalStatus approvalStatus,
                                   FileApprovalValue approvalValue) {
		UserProfile userProfile = userProfileRepo.findByUserId(userId);
		CloudStorageFile cloudStorageFileDto = null;
		Lovstamp lovstamp = null;
		CloudStorageFile cloudStorageFile = null;
		if (approvalStatus.equals(FileApprovalStatus.APPROVEPROFILEPHOTO)){
			cloudStorageFileDto = userProfile.getProfilePhotoCloudFile();
			cloudStorageFile = approveFile( cloudStorageRepo.findOne(cloudStorageFileDto.getId()), FileApprovalValue.APPROVE);
		}
		if (approvalStatus.equals(FileApprovalStatus.REJECTPROFILEPHOTO)){
			cloudStorageFileDto = userProfile.getProfilePhotoCloudFile();
			cloudStorageFile = approveFile( cloudStorageRepo.findOne(cloudStorageFileDto.getId()), FileApprovalValue.REJECT);
		}
		if (approvalStatus.equals(FileApprovalStatus.APPROVEPROFILEAUDIO)){
			lovstamp = lovstampRepo.findLovstampByUser(userProfile.getUser());
			cloudStorageFile = approveFile(lovstamp.getAudioFileCloud(), FileApprovalValue.APPROVE);
		}
		if (approvalStatus.equals(FileApprovalStatus.REJECTPROFILEAUDIO)){
			lovstamp = lovstampRepo.findLovstampByUser(userProfile.getUser());
			cloudStorageFile = approveFile(lovstamp.getAudioFileCloud(), FileApprovalValue.REJECT);
		}
		if (approvalStatus.equals(FileApprovalStatus.APPROVEHANDSPHOTO)){
			cloudStorageFileDto = userProfile.getHandsCloudFile();
			cloudStorageFile = approveFile(cloudStorageRepo.findOne(cloudStorageFileDto.getId()), FileApprovalValue.APPROVE);
		}
		if (approvalStatus.equals(FileApprovalStatus.REJECTHANDSPHOTO)){
			cloudStorageFileDto = userProfile.getHandsCloudFile();
			cloudStorageFile = approveFile(cloudStorageRepo.findOne(cloudStorageFileDto.getId()), FileApprovalValue.REJECT);
		}

		if (approvalStatus.equals(FileApprovalStatus.APPROVEFEETPHOTO)){
			cloudStorageFileDto = userProfile.getFeetCloudFile();
			cloudStorageFile = approveFile(cloudStorageRepo.findOne(cloudStorageFileDto.getId()), FileApprovalValue.APPROVE);
		}

		if (approvalStatus.equals(FileApprovalStatus.REJECTFEETPHOTO)){
			cloudStorageFileDto = userProfile.getFeetCloudFile();
			cloudStorageFile = approveFile(cloudStorageRepo.findOne(cloudStorageFileDto.getId()), FileApprovalValue.REJECT);
		}
		if (approvalStatus.equals(FileApprovalStatus.APPROVELEGSPHOTO)){
			cloudStorageFileDto = userProfile.getLegsCloudFile();
			cloudStorageFile = approveFile(cloudStorageFileDto, FileApprovalValue.APPROVE);
		}
		if (approvalStatus.equals(FileApprovalStatus.REJECTLEGSPHOTO)){
			cloudStorageFileDto = userProfile.getLegsCloudFile();
			cloudStorageFile = approveFile(cloudStorageFileDto, FileApprovalValue.REJECT);
		}

		if ((lovstamp == null) && (cloudStorageFileDto == null)) return false;

        return cloudStorageFile != null;
	}


	private CloudStorageFile approveFile(CloudStorageFile cloudStorageFile, FileApprovalValue approvalValue) {
		if (approvalValue.equals(FileApprovalValue.APPROVE)){
			cloudStorageFile.setApproved(true);
			cloudStorageFile.setApprovedDate(Calendar.getInstance().getTime());
			cloudStorageFile.setApprovedBy(null);
			cloudStorageFile.setRejected(false);
		} else {
			cloudStorageFile.setRejected(true);
			cloudStorageFile.setRejectedDate(Calendar.getInstance().getTime());
			cloudStorageFile.setRejectedBy(null);
			cloudStorageFile.setApproved(false);
			cloudStorageFile = update(cloudStorageFile);
		}
		return update(cloudStorageFile);
	}

	@Transactional(readOnly = true)
	@Override
	public Boolean profileFileProvided(Integer userId, FileApprovalStatus photoApprovalStatus) {
		UserProfile userProfile = userProfileRepo.findByUserId(userId);
		CloudStorageFile cloudStorageFileDto = null;
		Lovstamp lovstamp = null;
		switch (photoApprovalStatus){
		case APPROVEPROFILEPHOTO:
			cloudStorageFileDto = userProfile.getProfilePhotoCloudFile();
		case REJECTPROFILEPHOTO:
			cloudStorageFileDto = userProfile.getProfilePhotoCloudFile();
		case APPROVEPROFILEAUDIO:
			lovstamp = lovstampRepo.findLovstampByUser(userProfile.getUser());
		case REJECTPROFILEAUDIO:
			lovstamp = lovstampRepo.findLovstampByUser(userProfile.getUser());
		case APPROVEHANDSPHOTO:
			cloudStorageFileDto = userProfile.getHandsCloudFile();
		case REJECTHANDSPHOTO:
			cloudStorageFileDto = userProfile.getHandsCloudFile();
		case APPROVEFEETPHOTO:
			cloudStorageFileDto = userProfile.getFeetCloudFile();
		case REJECTFEETPHOTO:
			cloudStorageFileDto = userProfile.getFeetCloudFile();
		case APPROVELEGSPHOTO:
			cloudStorageFileDto = userProfile.getLegsCloudFile();
		case REJECTLEGSPHOTO:
			cloudStorageFileDto = userProfile.getLegsCloudFile();
		}

        return !((lovstamp == null) && (cloudStorageFileDto == null));
	}

	@Transactional
	@Override
	public void delete(Long id) {
	cloudStorageRepo.delete(id);
	}


	@Transactional
	public CloudStorageFileDto findDtoById(Long id)
	{
		CloudStorageFile storage=cloudStorageRepo.findOne(id);
		CloudStorageFileDto dto=new CloudStorageFileDto();
		dto.setId(storage.getId());
		dto.setApproved(storage.getApproved());
		dto.setBucket(storage.getBucket());
		dto.setUrl(storage.getUrl());
		dto.setName(storage.getName());
		return dto;
	}

}
