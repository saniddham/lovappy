package com.realroofers.lovappy.service.order.model;

import com.realroofers.lovappy.service.ads.model.Ad;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.datingPlaces.model.DatingPlace;
import com.realroofers.lovappy.service.event.model.Event;
import com.realroofers.lovappy.service.music.model.Music;
import com.realroofers.lovappy.service.privatemessage.model.PrivateMessage;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.product.model.Product;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * Created by Eias Altawil on 5/11/17
 */

@Entity
@Table(name="order_detail")
@Data
@DynamicUpdate
public class OrderDetail extends BaseEntity<Long> {
    @Column(name = "original_id")
    private Long originalId;

    @Enumerated(EnumType.STRING)
    private OrderDetailType type;

    @Column(nullable = false)
    private Double price;
    @Column(nullable = false)
    private Integer quantity;
}
