package com.realroofers.lovappy.service.questions.repo;

import com.realroofers.lovappy.service.questions.model.DailyQuestion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * Created by Darrel Rayen on 9/18/18.
 */
@Repository
public interface DailyQuestionRepo extends JpaRepository<DailyQuestion, Long> {

    DailyQuestion findFirstByQuestionDate(Date date);

    DailyQuestion findTopByOrderByIdDesc();

    @Query("select dq,ur from DailyQuestion dq left outer join UserResponse ur ON dq = ur.question")
    Page<Object[]> findAllQuestionsByUserId(Pageable pageable);

}
