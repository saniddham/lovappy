package com.realroofers.lovappy.service.cloud;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.cloud.support.FileApprovalStatus;
import com.realroofers.lovappy.service.cloud.support.FileApprovalValue;

public interface CloudStorageService {
	CloudStorageFileDto add(CloudStorageFileDto cloudStorageFileDto);

	CloudStorageFile update(CloudStorageFile cloudStorageFile);

	CloudStorageFile findById(Long id);

	Boolean approveUserFile(Integer userId, Integer currentUserId, FileApprovalStatus approvalStatus,
							FileApprovalValue approvalValue);
	Boolean profileFileProvided(Integer userId, FileApprovalStatus photoApprovalStatus);

	void delete(Long id);

	CloudStorageFileDto findDtoById(Long id);
}
