package com.realroofers.lovappy.service.user;

import com.realroofers.lovappy.service.user.dto.*;

import java.util.Map;
import java.util.Set;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public interface UserProfileAndPreferenceService {
    AddressDto saveAddress(Integer userId, AddressDto address);
    GenderAndPrefGenderDto getGenderAndPrefGender(Integer userId);

    void saveOrUpdate(Integer userId, GenderAndPrefGenderDto dto);
    void update(Integer userId, GenderAndPrefGenderDto dto);

    AgeHeightLangAndPrefSizeAgeHeightLangDto getAgeHeightLangAndPrefSizeAgeHeightLang(Integer userId);

    void saveOrUpdate(Integer userId, AgeHeightLangAndPrefSizeAgeHeightLangDto dto);

	RegisterStartDto getRegisterStartDto(Integer userId);

	void saveOrUpdate(Integer userId, StatusLangDto dto);

	StatusLangDto getStatusLang(Integer userId);

    void setSeekingLove(Integer userId);
    AddressDto saveLocation(Integer userId, AddressDto loc, Integer lastMileRadiusSearch);

    void saveOrUpdate(Integer userId, Map<String, String> statuses, Set<String> seekingLanguages, Set<String> speakingLanguages, AgeHeightLangAndPrefSizeAgeHeightLangDto ageHeightLangAndPrefSizeAgeHeightLangDto, PersonalityLifestyleDto personalityLifestyleDto);

    void updateProfile(Integer userId, UpdateProfileRequest profileRequest);
}
