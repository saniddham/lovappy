package com.realroofers.lovappy.service.radio.support;

public enum RadioType {
    PUBLIC_LOVSTAMP, PRIVATE_LOVSTAMP, FEATURED_VIDEO, FEATURED_AUDIO
}