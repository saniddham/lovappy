package com.realroofers.lovappy.service.user.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import org.springframework.beans.BeanUtils;

import com.realroofers.lovappy.service.user.model.embeddable.Address;
import org.springframework.util.StringUtils;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode
public class AddressDto {
    private String zipCode;
    private String country;
    private String countryCode;
    private String state; // or region
    private String stateShort;
    private String province; // or county
    private String city;
    private String streetNumber;
    private String fullAddress;
    private Double latitude;
    private Double longitude;
    private String lastAddressSearch;
    private Integer lastMileRadiusSearch;
    private String shortAddress;
    public AddressDto() {
    }

    public AddressDto(Double latitude, Double longitude, Integer radius) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.lastMileRadiusSearch = radius;
    }

    public static AddressDto toAddressDto(Address address) {
        AddressDto result = new AddressDto();
        if (address != null)
            BeanUtils.copyProperties(address, result);

        if (result.lastMileRadiusSearch == null)
            result.lastMileRadiusSearch = 300;
        if (result.latitude == null)
            result.latitude = 0.0;
        if (result.longitude == null)
            result.longitude = 0.0;

        return result;
    }

    public Address toAddress() {
        Address result = new Address();
        BeanUtils.copyProperties(this, result);
        return result;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateShort() {
        return stateShort != null && stateShort.length() > 4? stateShort.substring(0, 4)  :  stateShort;
    }

    public void setStateShort(String stateShort) {
        this.stateShort = stateShort;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getLastAddressSearch() {
        return lastAddressSearch;
    }

    public void setLastAddressSearch(String lastAddressSearch) {
        this.lastAddressSearch = lastAddressSearch;
    }

    public Integer getLastMileRadiusSearch() {
        return lastMileRadiusSearch;
    }

    public void setLastMileRadiusSearch(Integer lastMileRadiusSearch) {
        this.lastMileRadiusSearch = lastMileRadiusSearch;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getShortAddress() {
        String address = "";
        if(getLatitude() !=null && getLatitude() != 0 &&
                getLongitude() !=null && getLongitude() != 0) {
            if (!StringUtils.isEmpty(city)) {
                address = city;
            }
            if (!StringUtils.isEmpty(stateShort)) {
                address += ", " + stateShort;
            }
            if (!StringUtils.isEmpty(zipCode)) {
                address += ", " + zipCode;
            }
            if (!StringUtils.isEmpty(countryCode)) {
               address += ", " + countryCode;


            } else {
                if (country.equals("United States"))
                    address += ", US";
                else address += ", " + country;
//        }
            }
        }
        return address;
    }

    public void setShortAddress(String shortAddress) {
        this.shortAddress = shortAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AddressDto that = (AddressDto) o;

        if (zipCode != null ? !zipCode.equals(that.zipCode) : that.zipCode != null) return false;
        if (country != null ? !country.equals(that.country) : that.country != null) return false;
        if (state != null ? !state.equals(that.state) : that.state != null) return false;
        if (province != null ? !province.equals(that.province) : that.province != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (streetNumber != null ? !streetNumber.equals(that.streetNumber) : that.streetNumber != null) return false;
        if (fullAddress != null ? !fullAddress.equals(that.fullAddress) : that.fullAddress != null) return false;
        if (latitude != null ? !latitude.equals(that.latitude) : that.latitude != null) return false;
        if (longitude != null ? !longitude.equals(that.longitude) : that.longitude != null) return false;
        if (lastAddressSearch != null ? !lastAddressSearch.equals(that.lastAddressSearch)
            : that.lastAddressSearch != null) return false;
        return lastMileRadiusSearch != null ? lastMileRadiusSearch.equals(that.lastMileRadiusSearch)
            : that.lastMileRadiusSearch == null;
    }

    @Override
    public int hashCode() {
        int result = zipCode != null ? zipCode.hashCode() : 0;
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (province != null ? province.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (streetNumber != null ? streetNumber.hashCode() : 0);
        result = 31 * result + (fullAddress != null ? fullAddress.hashCode() : 0);
        result = 31 * result + (latitude != null ? latitude.hashCode() : 0);
        result = 31 * result + (longitude != null ? longitude.hashCode() : 0);
        result = 31 * result + (lastAddressSearch != null ? lastAddressSearch.hashCode() : 0);
        result = 31 * result + (lastMileRadiusSearch != null ? lastMileRadiusSearch.hashCode() : 0);
        return result;
    }
}
