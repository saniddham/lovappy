package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.user.support.Gender;
import lombok.EqualsAndHashCode;
import org.jsondoc.core.annotation.ApiObject;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
@ApiObject
public class WithinRadiusDto {
    @NotNull
    private Double latitude;
    @NotNull
    private Double longitude;
    @NotNull
    @Min(1)
    private Integer radius;

    private Gender prefGender;

    public WithinRadiusDto() {
    }

    public WithinRadiusDto(Double latitude, Double longitude, Integer radius, Gender prefGender) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
        this.prefGender = prefGender;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public Gender getPrefGender() {
        return prefGender;
    }

    public void setPrefGender(Gender prefGender) {
        this.prefGender = prefGender;
    }
}
