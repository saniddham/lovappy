package com.realroofers.lovappy.service.product.dto;

import com.realroofers.lovappy.service.product.model.ProductLocation;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.vendor.model.VendorLocation;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class ProductLocationDto {

    private Integer id;
    private ProductDto product;
    private VendorLocation location;
    private Boolean active = true;
    private UserDto createdBy;


    public ProductLocationDto() {
    }

    public ProductLocationDto(ProductLocation productLocation) {
        if (productLocation.getId() != null) {
            this.setId(productLocation.getId());
        }
        if(productLocation.getLocation()!=null) {
            this.location = productLocation.getLocation();
        }
        if(productLocation.getProduct()!=null) {
            this.product = new ProductDto(productLocation.getProduct());
        }
        this.active = productLocation.getActive();
        if(productLocation.getCreatedBy()!=null) {
            this.createdBy = new UserDto(productLocation.getCreatedBy());
        }
    }
}
