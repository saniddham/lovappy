package com.realroofers.lovappy.service.credits.impl;

import com.realroofers.lovappy.service.credits.CreditService;
import com.realroofers.lovappy.service.credits.dto.CreditDto;
import com.realroofers.lovappy.service.credits.model.UserCreditPK;
import com.realroofers.lovappy.service.credits.model.UserCredits;
import com.realroofers.lovappy.service.credits.repo.UserCreditRepo;
import com.realroofers.lovappy.service.exception.GenericServiceException;
import com.realroofers.lovappy.service.exception.ResourceNotFoundException;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.CouponDto;
import com.realroofers.lovappy.service.order.model.Price;
import com.realroofers.lovappy.service.order.repo.PriceRepo;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.repo.UserProfileRepo;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by Daoud Shaheen on 8/10/2018.
 */
@Service
public class CreditServiceImpl implements CreditService{

    private final UserCreditRepo userCreditRepo;
    private final UserRepo userRepo;
    private final PriceRepo priceRepo;
    private final UserProfileRepo userProfileRepo;
    public CreditServiceImpl(UserCreditRepo userCreditRepo, UserRepo userRepo, PriceRepo priceRepo, UserProfileRepo userProfileRepo) {
        this.userCreditRepo = userCreditRepo;
        this.userRepo = userRepo;
        this.priceRepo = priceRepo;
        this.userProfileRepo = userProfileRepo;
    }

    @Transactional
    @Override
    public CreditDto addCredit(Integer userId, Integer balance) {
       UserProfile userProfile = userProfileRepo.findOne(userId);
       if(userProfile == null) {
           throw new ResourceNotFoundException("User (" + userId+ ") is not found");
       }
        userProfile.setCreditBalance(userProfile.getCreditBalance() == null? balance : userProfile.getCreditBalance() + balance);
        userProfileRepo.save(userProfile);


        return new CreditDto(userProfile.getCreditBalance(), null, priceRepo.findByType(OrderDetailType.CREDITS).getPrice());
    }
    @Transactional
    @Override
    public CreditDto sendUser(Integer fromUserId, Integer toUserId, Integer balance) {
        User fromUser = userRepo.findOne(fromUserId);
        if(fromUser == null) {
            throw new ResourceNotFoundException("User (" + fromUserId+ ") is not found");
        }
        User toUser = userRepo.findOne(toUserId);
        if(toUser == null) {
            throw new ResourceNotFoundException("User (" + toUserId+ ") is not found");
        }
        //subtract the blance form real big profile
        UserProfile userProfile = fromUser.getUserProfile();
        if((userProfile.getCreditBalance() - balance) <0){
            throw new ResourceNotFoundException("You don't have enough credits to send");
        }
        UserCreditPK userCreditPK = new UserCreditPK(toUser, fromUser);
        UserCredits userCredits = userCreditRepo.findOne(userCreditPK);

        if(userCredits == null){
            userCredits = new UserCredits();
           userCredits.setId(userCreditPK);
           userCredits.setCreated(new Date());
            userCredits.setBalance(balance);
        } else {
            userCredits.setUpdated(new Date());
            userCredits.setBalance(userCredits.getBalance() + balance);
        }
        userCreditRepo.save(userCredits);
        userProfile.setCreditBalance(userProfile.getCreditBalance() - balance);
        userProfileRepo.save(userProfile);
        return new CreditDto(userProfile.getCreditBalance(), 0,  priceRepo.findByType(OrderDetailType.CREDITS).getPrice());
    }

    @Transactional
    @Override
    public CreditDto chargeByCredit(Integer fromUserId, Integer toUserId, Integer balance) {
        User fromUser = userRepo.findOne(fromUserId);
        if(fromUser == null) {
            throw new ResourceNotFoundException("User (" + fromUserId+ ") is not found");
        }
        User toUser = userRepo.findOne(toUserId);
        if(toUser == null) {
            throw new ResourceNotFoundException("User (" + toUserId+ ") is not found");
        }
        //subtract the blance form real big profile
        UserProfile userProfile = fromUser.getUserProfile();

        UserCreditPK userCreditPK = new UserCreditPK(toUser, fromUser);
        UserCredits userCredits = userCreditRepo.findOne(userCreditPK);

        if(userCredits != null && userCredits.getBalance() >balance){
            userCredits.setUpdated(new Date());
            userCredits.setBalance(userCredits.getBalance() - balance);
            userCreditRepo.save(userCredits);
            return new CreditDto(userProfile.getCreditBalance(), userCredits.getBalance(),  priceRepo.findByType(OrderDetailType.CREDITS).getPrice());
        } else if(userProfile.getCreditBalance() > balance){
            userProfile.setCreditBalance(userProfile.getCreditBalance() - balance);
            userProfileRepo.save(userProfile);
            return new CreditDto(userProfile.getCreditBalance(), 0,  priceRepo.findByType(OrderDetailType.CREDITS).getPrice());
        }


        throw new ResourceNotFoundException("You don't have enough credits");


    }

    @Transactional(readOnly = true)
    @Override
    public CreditDto getCredit(Integer userId) {
        UserProfile userProfile = userProfileRepo.findOne(userId);
        if(userProfile == null) {
            throw new ResourceNotFoundException("User (" + userId+ ") is not found");
        }

        return new CreditDto(userProfile.getCreditBalance(), null,  priceRepo.findByType(OrderDetailType.CREDITS).getPrice());
    }

    @Transactional(readOnly = true)
    @Override
    public CreditDto getCreditByToUser(Integer fromUserId, Integer toUserId) {
        User fromUser = userRepo.findOne(fromUserId);
        if(fromUser == null) {
            throw new ResourceNotFoundException("User (" + fromUserId+ ") is not found");
        }
        User toUser = userRepo.findOne(toUserId);
        if(toUser == null) {
            throw new ResourceNotFoundException("User (" + toUserId+ ") is not found");
        }
        UserCreditPK userCreditPK = new UserCreditPK(fromUser, toUser);
        UserCredits userCredits = userCreditRepo.findOne(userCreditPK);

        if(userCredits != null)
        return new CreditDto(fromUser.getUserProfile().getCreditBalance(), userCredits.getBalance(),  priceRepo.findByType(OrderDetailType.CREDITS).getPrice());

        return new CreditDto(fromUser.getUserProfile().getCreditBalance());
    }

    @Transactional(readOnly = true)
    @Override
    public CalculatePriceDto calculatePrice(CouponDto coupon, Integer quantity) {

        Price price1 = priceRepo.findByType(OrderDetailType.CREDITS);

        Double price = price1 != null ? price1.getPrice() : 1.99d;
        Double totalPrice = price * quantity;
        Double discountValue = 0d;
        if (coupon != null)
            discountValue = totalPrice * coupon.getDiscountPercent() * 0.01;

        totalPrice -= discountValue;

        return new CalculatePriceDto(price, coupon, totalPrice);
    }
}
