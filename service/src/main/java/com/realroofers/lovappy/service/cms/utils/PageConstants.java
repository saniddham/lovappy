package com.realroofers.lovappy.service.cms.utils;

/**
 * Created by Daoud Shaheen on 6/11/2017.
 */
public interface PageConstants {
    //HOME Page Constants
    String HOMEPAGE_HEADER_H1 = "homepage_header_h1";
    String HOMEPAGE_HEADER_H5 ="homepage_header_h5";
    String HOMEPAGE_ABOUT_TEXT_H1 ="homepage_about_text_h1";
    String HOMEPAGE_ABOUT_TEXT_P ="homepage_about_text_p";

    String VIDEO_HOMEPAGE_TITLE = "video_homepage_title";
    String VIDEO_HOMEPAGE_VIDEO = "video_homepage_video";
    String VIDEO_HOMEPAGE_MOBILE_VIDEO = "video_homepage_mobile_video";
    String VIDEO_HOMEPAGE_LOVRADIO_BG = "video_homepage_lovradio_bg";
    String VIDEO_HOMEPAGE_LOVRADIO_TITLE = "video_homepage_lovradio_title";
    String VIDEO_HOMEPAGE_LOVRADIO_SUB_TITLE = "video_homepage_lovradio_sub_title";
    String VIDEO_HOMEPAGE_LOVRADIO_EXAMPLE = "video_homepage_lovradio_example";

    String ALT_HOMEPAGE_COVER_BG = "alt_homepage_cover_bg";
    String ALT_HOMEPAGE_COVER_HEADER = "alt_homepage_cover_bg";
    String ALT_HOMEPAGE_COVER_SUB_HEADER = "alt_homepage_cover_sub_header";
    String ALT_HOMEPAGE_COVER_LOVRADIO_EXAMPLE = "alt_homepage_cover_lovradio_example";
    String ALT_HOMEPAGE_LOVE_LISTENING_HEADER = "alt_homepage_love_listening_header";
    String ALT_HOMEPAGE_LOVE_LISTENING_SUB_HEADER = "alt_homepage_love_listening_sub_header";

    String ALT_HOMEPAGE_LOVBLOG_TITLE = "alt_homepage_lovblog_title";
    String ALT_HOMEPAGE_LOVBLOG_SUB_TITLE = "alt_homepage_lovblog_sub_title";
    String ALT_HOMEPAGE_LOVBLOG_FEATURED_VLOG = "alt_homepage_lovblog_featured_vlog";

    String ALT_HOMEPAGE_LOVAPPY_CORPORATION = "alt_homepage_lovappy_corporation";
    String ALT_HOMEPAGE_LOVAPPY_CORPORATION_DESCRIPTION= "alt_homepage_lovappy_corporation_description";

    String ALT_HOMEPAGE_LOVAPPY_APP_TITLE= "alt_homepage_lovappy_app_title";
    String ALT_HOMEPAGE_LOVAPPY_APP_DESCRIPTION= "alt_homepage_lovappy_app_description";

    String ALT_HOMEPAGE_LOVAPPY_APP_ANDROID_LINK= "alt_homepage_lovappy_app_android_link";
    String ALT_HOMEPAGE_LOVAPPY_APP_APPLE_LINK= "alt_homepage_lovappy_app_apple_link";

    //set blogs texts
    String HOMEPAGE_FEATURED_FIRST_BLOG ="homepage_featured_first_blog";

    //set blogs bacgrounds
    String HOMEPAGE_FEATURED_FIRST_BG = "homepage_featured_first_bg";
    String HOMEPAGE_FEATURED_SECOND_BG ="homepage_featured_second_bg";
    String HOMEPAGE_FEATURED_THIRD_BG ="homepage_featured_third_bg";
    String HOMEPAGE_FEATURED_FOURTH_BG ="homepage_featured_fourth_bg";
    String HOMEPAGE_FEATURED_FIFTH_BG ="homepage_featured_fifth_bg";
    String HOMEPAGE_FEATURED_SIXTH_BG ="homepage_featured_sixth_bg";

    //about us Constants
    String ABOUT_US_DESCRIPTION = "about_us_description";
    String ABOUT_US_HEADER = "about_us_header";
    String ABOUT_US_BG = "about_us_bg";

    //contact us constants
    String CONTACT_US_ADDRESS = "contact_us_address";
    String CONTACT_US_NUMBER = "contact_us_number";
    String CONTACT_US_EMAIL = "contact_us_email";

    //features page constants
    String FEATURES_RECORD_VOICE_DESCRIPTION = "features_record_voice_description";
    String FEATURES_SEND_SONG_DESCRIPTION = "features_send_song_description";
    String FEATURES_SEND_GIFT_DESCRIPTION = "features_send_gift_description";

    //login page constants
    String MAIN_LOGIN_BG = "main_login_bg";

    //forgot password page constants
    String FORGOT_PASSWORD_DESCRIPTION = "forgot_password_description";

    //mobile page constants
    String MOBILE_HEADER_TEXT = "mobile_header_text";
    String MOBILE_DESCRIPTION_TEXT = "mobile_description_text";
    String LEFT_MOBILE_PAGE_IMAGE = "left_mobile_page_image";

    //Lite page constants
    String LITEPAGE_PARAGRAPH_H5 = "litepage_paragraph_h5";

    //Prime page constants
    String PRIMEPAGE_PARAGRAPH_H5 = "primepage_paragraph_h5";

    //Plan and Price Page constants
    String PLAN_PRICE_PAGE = "plan_price_page";
    String PLAN_PRICE_COVER_IMAGE = "plan_price_cover_image";
    String PLAN_PRICE_COVER_TEXT = "plan_price_cover_text";
    String PLAN_PRICE_COVER_DESCRIPTION = "plan_price_cover_description";
    String PLAN_PRICE_RULES = "plan_price_rules";

    //login page constants
    String SUBMIT_BLOG_COVER = "submit_blog_cover";
    String EVENT_HEADER_COVER= "event_header_cover";
    String EVENT_AMBASSADOR_INFO = "event-ambassador-info";

    //Landing Pages

    String MUSIC_PAGE_TITLE = "music_page_title";
    String MUSIC_PAGE_SUB_TITLE_A = "music_page_sub_title_a";
    String MUSIC_PAGE_SUB_TITLE_B = "music_page_sub_title_b";


    String MUSIC_LANDING_PAGE_IMAGE_A = "music_landing_page_image_a";
    String MUSIC_LANDING_PAGE_IMAGE_B = "music_landing_page_image_b";
    String MUSIC_LANDING_PAGE_TITLE = "music_landing_page_title";
    String MUSIC_LANDING_PAGE_SUB_TITLE_A = "music_landing_page_sub_title_a";
    String MUSIC_LANDING_PAGE_SUB_TITLE_B = "music_landing_page_sub_title_b";

    String MUSICIAN_LANDING_PAGE_TITLE = "musician_landing_page_title";
    String MUSICIAN_LANDING_PAGE_DESCRIPTION = "musician_landing_page_description";
    String MUSICIAN_LANDING_PAGE_IMAGE = "musician_landing_page_image";

    String MUSIC_USER_LANDING_PAGE_TITLE = "music_user_landing_page_title";
    String MUSIC_USER_LANDING_PAGE_DESCRIPTION = "music_user_landing_page_description";
    String MUSIC_USER_LANDING_PAGE_IMAGE = "music_user_landing_page_image";


    String GIFTS_LANDING_PAGE_IMAGE = "gifts_landing_page_image";
    String GIFTS_LANDING_PAGE_TITLE = "gifts_landing_page_title";
    String GIFTS_LANDING_PAGE_DESCRIPTION = "gifts_landing_page_description";
    String BLOG_LANDING_PAGE_IMAGE = "blog_landing_page_image";
    String BLOG_LANDING_PAGE_TITLE = "blog_landing_page_title";
    String BLOG_LANDING_PAGE_DESCRIPTION = "blog_landing_page_description";


    String EVENTS_LANDING_PAGE_IMAGE = "events_landing_page_image";
    String EVENTS_LANDING_PAGE_TITLE = "events_landing_page_title";
    String EVENTS_LANDING_PAGE_DESCRIPTION = "events_landing_page_description";

    //errors
    String SUSPENDED_ERROR = "suspended_error";
    String AMBASSADOR_APPROVAL_ERROR = "not_approved";
    String INVALID_PASSWORD_ERROR = "invalid_password";

    //ads
    String ADVERTISE_WITH_US = "advertise-with-us";
    String ADS_TEXT_STEP1 = "ads_text_step1";
    String ADS_AUDIO_STEP1 = "ads_audio_step1";
    String ADS_VIDEO_STEP1 = "ads_video_step1";
}
