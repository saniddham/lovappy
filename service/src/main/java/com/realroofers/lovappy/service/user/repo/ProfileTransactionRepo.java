package com.realroofers.lovappy.service.user.repo;

import com.realroofers.lovappy.service.user.dto.UserProfilePictureAccessDto;
import com.realroofers.lovappy.service.user.model.ProfileTransaction;
import com.realroofers.lovappy.service.user.model.ProfileTransactionPK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Daoud Shaheen on 1/18/2018.
 */
public interface ProfileTransactionRepo extends JpaRepository<ProfileTransaction, ProfileTransactionPK> {

    @Query("SELECT COUNT(pt) FROM ProfileTransaction pt JOIN pt.id.userTo u WHERE u.userId=:userId AND pt.seen=false")
    Long countByIdToUserIdAndSeenFalse(@Param("userId") Integer userId);

    @Query(nativeQuery = true, value = "update profile_transactions set profile_transactions.seen = 1 where user_to =:userId ;")
    @Transactional
    @Modifying
    void updateByIdToUserIdAndSeenFalse(@Param("userId") Integer userId);

    @Query("SELECT NEW com.realroofers.lovappy.service.user.dto.UserProfilePictureAccessDto(ufrom.userId, " +
            " ufrom.userProfile.address.city, ufrom.userProfile.birthDate, ufrom.userProfile.gender, 'EN', pt.transactionCount) FROM ProfileTransaction pt" +
            " JOIN pt.id.userTo uto JOIN pt.id.userFrom ufrom WHERE uto.userId=:userId AND pt.transactionCount<:maxTrans and" +
            " ufrom.userId not in(Select u.userId FROM UserProfile up JOIN up.allowedProfileUsers u Where up.userId=:userId AND up.isAllowedProfilePic=false)")
    Page<UserProfilePictureAccessDto> findByIdToUserId(@Param("userId") Integer userId, @Param("maxTrans") Integer maxTrans, Pageable pageable);

    @Query("SELECT COUNT(pt) FROM ProfileTransaction pt JOIN pt.id.userTo uto JOIN pt.id.userFrom ufrom WHERE uto.userId=:userId AND pt.transactionCount<:maxTrans  and" +
            " ufrom.userId not in(Select u.userId FROM UserProfile up JOIN up.allowedProfileUsers u Where up.userId=:userId AND up.isAllowedProfilePic=false)")
    Long countByIdToUserId(@Param("userId") Integer userId, @Param("maxTrans") Integer maxTrans);

}
