package com.realroofers.lovappy.service.music.support;

public enum MusicAdType {
    BANNER("BANNER"), FEATURED("FEATURED");
    String text;

    MusicAdType(String text) {
        this.text = text;
    }
}
