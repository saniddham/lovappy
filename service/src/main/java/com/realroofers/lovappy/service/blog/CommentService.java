
package com.realroofers.lovappy.service.blog;

import java.util.List;

import com.realroofers.lovappy.service.blog.model.Comment;

public interface CommentService {
	void save(Comment comment);
	List<Comment> getAllComments();
	void update(Comment comment);
	List<Comment> findByPostId(Integer postId);
}
