package com.realroofers.lovappy.service.datingPlaces.model;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Darrel Rayen on 10/17/17.
 */
@Entity
@Table(name = "places_attachment")
@EqualsAndHashCode
public class PlacesAttachment {

    @Id
    @GeneratedValue
    private Integer attachmentId;

    @ManyToOne
    @JoinColumn(name = "place_id")
    private DatingPlace placeId;

    @Column(name = "url", nullable = false)
    private String url;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile picture;

    @CreationTimestamp
    @Column(name = "created_date", nullable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date createdDate;

    private String imageDescription;

/*    @CreatedBy
    private User createdBy;*/

    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public DatingPlace getPlaceId() {
        return placeId;
    }

    public void setPlaceId(DatingPlace placeId) {
        this.placeId = placeId;
    }

    public CloudStorageFile getPicture() {
        return picture;
    }

    public void setPicture(CloudStorageFile picture) {
        this.picture = picture;
    }

    public String getImageDescription() {
        return imageDescription;
    }

    public void setImageDescription(String imageDescription) {
        this.imageDescription = imageDescription;
    }
}
