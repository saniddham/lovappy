package com.realroofers.lovappy.service.event.model;

import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Tejaswi Venupalli on 8/28/17
 */

@Entity
@Table(name = "event_category")
@EqualsAndHashCode
public class EventCategory {

    @Id
    @GeneratedValue
    @Column(name = "category_id")
    private Integer id;
    private String categoryName;

    @ManyToMany(mappedBy = "eventCategories")
    private Collection<Event> events = new ArrayList<>();

    private Boolean enabled;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Collection<Event> getEvents() {
        return events;
    }

    public void setEvents(Collection<Event> events) {
        this.events = events;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
