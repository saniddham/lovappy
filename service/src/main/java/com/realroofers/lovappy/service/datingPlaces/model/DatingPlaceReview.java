package com.realroofers.lovappy.service.datingPlaces.model;

import com.realroofers.lovappy.service.datingPlaces.support.DatingAgeRange;
import com.realroofers.lovappy.service.datingPlaces.support.PriceRange;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Darrel Rayen on 10/17/17.
 */
@Entity
@Table(name = "dating_place_review")
@EqualsAndHashCode
public class DatingPlaceReview implements Serializable {

    @Id
    @GeneratedValue
    private Integer placesReviewId;

    @ManyToOne
    @JoinColumn(name = "place_id")
    private DatingPlace placeId;

    @Column(name = "reviewer_comment", nullable = false)
    private String reviewerComment;

    @Column(name = "reviewer_email", nullable = false)
    private String reviewerEmail;

    private Double parkingRating;

    private Double parkingAccessRating;

    private Double securityRating;

    private Double overallRating;

    @Column(name = "review_date", nullable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    @CreationTimestamp
    private Date reviewDate;

    @Column(name = "approved", columnDefinition = "BIT(1) default 0")
    private Boolean approved;

    @Enumerated(EnumType.STRING)
    private ApprovalStatus reviewApprovalStatus;

    @Enumerated(EnumType.STRING)
    private Gender suitableGender;

    @Enumerated(EnumType.STRING)
    private DatingAgeRange suitableAgeRange;

    @Enumerated(EnumType.STRING)
    private PriceRange suitablePriceRange;

    @Column(name = "is_good_for_couple", columnDefinition = "BIT(1) default 0")
    private Boolean isGoodForCouple;

    private Double averageRating;

    private String reviewerId;

    private String googlePlaceId;

    private String reviewerType;

    private String placeName;

    @Column(name = "is_featured", columnDefinition = "BIT(1) default 0")
    private Boolean isFeatured;

    public Integer getPlacesReviewId() {
        return placesReviewId;
    }

    public void setPlacesReviewId(Integer placesReviewId) {
        this.placesReviewId = placesReviewId;
    }

    public DatingPlace getPlaceId() {
        return placeId;
    }

    public void setPlaceId(DatingPlace placeId) {
        this.placeId = placeId;
    }

    public String getReviewerComment() {
        return reviewerComment;
    }

    public void setReviewerComment(String reviewerComment) {
        this.reviewerComment = reviewerComment;
    }

    public String getReviewerEmail() {
        return reviewerEmail;
    }

    public void setReviewerEmail(String reviewerEmail) {
        this.reviewerEmail = reviewerEmail;
    }

    public Double getParkingRating() {
        return parkingRating;
    }

    public void setParkingRating(Double parkingRating) {
        this.parkingRating = parkingRating;
    }

    public Double getParkingAccessRating() {
        return parkingAccessRating;
    }

    public void setParkingAccessRating(Double parkingAccessRating) {
        this.parkingAccessRating = parkingAccessRating;
    }

    public Double getSecurityRating() {
        return securityRating;
    }

    public void setSecurityRating(Double securityRating) {
        this.securityRating = securityRating;
    }

    public Date getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(Date reviewDate) {
        this.reviewDate = reviewDate;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public ApprovalStatus getReviewApprovalStatus() {
        return reviewApprovalStatus;
    }

    public void setReviewApprovalStatus(ApprovalStatus reviewApprovalStatus) {
        this.reviewApprovalStatus = reviewApprovalStatus;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }

    public Gender getSuitableGender() {
        return suitableGender;
    }

    public void setSuitableGender(Gender suitableGender) {
        this.suitableGender = suitableGender;
    }

    public DatingAgeRange getSuitableAgeRange() {
        return suitableAgeRange;
    }

    public void setSuitableAgeRange(DatingAgeRange suitableAgeRange) {
        this.suitableAgeRange = suitableAgeRange;
    }

    public PriceRange getSuitablePriceRange() {
        return suitablePriceRange;
    }

    public void setSuitablePriceRange(PriceRange suitablePriceRange) {
        this.suitablePriceRange = suitablePriceRange;
    }

    public Boolean getGoodForCouple() {
        return isGoodForCouple;
    }

    public void setGoodForCouple(Boolean goodForCouple) {
        isGoodForCouple = goodForCouple;
    }

    public String getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(String reviewerId) {
        this.reviewerId = reviewerId;
    }

    public Double getOverallRating() {
        return overallRating;
    }

    public void setOverallRating(Double overallRating) {
        this.overallRating = overallRating;
    }

    public String getGooglePlaceId() {
        return googlePlaceId;
    }

    public void setGooglePlaceId(String googlePlaceId) {
        this.googlePlaceId = googlePlaceId;
    }

    public String getReviewerType() {
        return reviewerType;
    }

    public void setReviewerType(String reviewerType) {
        this.reviewerType = reviewerType;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public Boolean getFeatured() {
        return isFeatured;
    }

    public void setFeatured(Boolean featured) {
        isFeatured = featured;
    }
}
