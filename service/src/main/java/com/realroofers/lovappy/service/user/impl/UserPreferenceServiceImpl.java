package com.realroofers.lovappy.service.user.impl;

import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.PrefHeight;
import com.realroofers.lovappy.service.user.support.Size;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.realroofers.lovappy.service.user.UserPreferenceService;
import com.realroofers.lovappy.service.user.dto.UserPreferenceDto;
import com.realroofers.lovappy.service.user.model.UserPreference;
import com.realroofers.lovappy.service.user.repo.UserPreferenceRepo;

@Service
public class UserPreferenceServiceImpl implements UserPreferenceService {

	private static Logger LOG = LoggerFactory.getLogger(UserPreferenceServiceImpl.class);

	private UserPreferenceRepo userPreferenceRepo;

	@Autowired
	public UserPreferenceServiceImpl(UserPreferenceRepo userPreferenceRepo) {
		this.userPreferenceRepo = userPreferenceRepo;
	}

	@Override
	@Transactional(readOnly = true)
	public UserPreferenceDto findOne(Integer usedId) {
		LOG.debug("Get Preference");
		try {
			UserPreference userPreference = userPreferenceRepo.findOne(usedId);
			return (userPreference != null ? new UserPreferenceDto(userPreference) : null);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	@Transactional
	public UserPreferenceDto update(Integer userID, Boolean penisSizeMatter, Size prefBustSize, Size prefButtSize) {
		UserPreference userPreference = userPreferenceRepo.findOne(userID);
		if(userPreference != null){
			userPreference.setPenisSizeMatter(penisSizeMatter);
			userPreference.setBustSize(prefBustSize);
			userPreference.setButtSize(prefButtSize);

			UserPreference save = userPreferenceRepo.save(userPreference);
			return new UserPreferenceDto(save);
		}

		return null;
	}

	@Override
	@Transactional
	public UserPreferenceDto update(Integer userID, Integer minAge, Integer maxAge, PrefHeight height, Gender gender) {
		UserPreference userPreference = userPreferenceRepo.findOne(userID);
		if(userPreference != null){
			userPreference.setMinAge(minAge);
			userPreference.setMaxAge(maxAge);
			userPreference.setHeight(height);
			userPreference.setGender(gender);

			UserPreference save = userPreferenceRepo.save(userPreference);
			return new UserPreferenceDto(save);
		}

		return null;
	}
}
