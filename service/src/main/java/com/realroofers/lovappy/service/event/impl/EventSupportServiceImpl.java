package com.realroofers.lovappy.service.event.impl;

import com.realroofers.lovappy.service.event.EventSupportService;
import com.realroofers.lovappy.service.event.dto.EventCategoryDto;
import com.realroofers.lovappy.service.event.model.EventCategory;
import com.realroofers.lovappy.service.event.repo.EventCategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tejaswi Venupalli on 9/04/17
 */
@Service
public class EventSupportServiceImpl implements EventSupportService{

    private EventCategoryRepo categoryRepo;

    @Autowired
    public EventSupportServiceImpl(EventCategoryRepo categoryRepo){
        this.categoryRepo = categoryRepo;
    }

    @Override
    @Transactional(readOnly = true)
    public List<EventCategoryDto> getEventCategories() {
        List<EventCategoryDto> categories = new ArrayList<>();

        List<EventCategory> all = categoryRepo.findAll();
        for(EventCategory category : all){
            categories.add(new EventCategoryDto(category));
        }

        return categories;
    }

    @Override
    @Transactional(readOnly = true)
    public EventCategoryDto getCategory(Integer id) {
        EventCategory category = categoryRepo.findOne(id);
        return category == null ? null : new EventCategoryDto(category);
    }

    @Override
    @Transactional
    public EventCategoryDto addCategory(EventCategoryDto category) {
        EventCategory c = new EventCategory();
        c.setCategoryName(category.getCategoryName());
        c.setEnabled(true);

        EventCategory save = categoryRepo.save(c);

        return save == null ? null : new EventCategoryDto(c);
    }

    @Override
    @Transactional
    public EventCategoryDto updateCategory(EventCategoryDto category) {
        EventCategory one = categoryRepo.findOne(category.getId());
        if (one != null){
            one.setCategoryName(category.getCategoryName());
            one.setEnabled(category.getEnabled());

            EventCategory save = categoryRepo.save(one);

            return save == null ? null : new EventCategoryDto(save);
        }

        return null;
    }
}
