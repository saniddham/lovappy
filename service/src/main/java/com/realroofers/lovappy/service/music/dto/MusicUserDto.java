package com.realroofers.lovappy.service.music.dto;

import com.realroofers.lovappy.service.music.model.MusicUser;
import com.realroofers.lovappy.service.user.dto.UserDto;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 9/23/2017.
 */
@EqualsAndHashCode
public class MusicUserDto implements Serializable {

    private UserDto user;

    private MusicDto music;

    private Date purchasedOn;

    private Boolean received;

    private Integer mobileDownloads;

    private Integer webDownloads;

    private Integer maxDownloads;

    private Boolean purchased;

    private Integer rating;

    public MusicUserDto() {
    }

    public MusicUserDto(MusicUser musicUser) {
        this.user = new UserDto(musicUser.getId().getUser());
        this.music = new MusicDto(musicUser.getId().getMusic());
        this.received = musicUser.getReceived() == null ? false : musicUser.getReceived();
        this.purchased = musicUser.getPurchased();
        this.maxDownloads = musicUser.getMaxDownloads();
        this.mobileDownloads = musicUser.getMobileDownloads();
        this.webDownloads = musicUser.getWebDownloads();
        this.purchasedOn = musicUser.getPurchasedOn();
        this.rating = musicUser.getRating();
    }

    public MusicDto getMusic() {
        return music;
    }

    public void setMusic(MusicDto music) {
        this.music = music;
    }

    public Date getPurchasedOn() {
        return purchasedOn;
    }

    public void setPurchasedOn(Date purchasedOn) {
        this.purchasedOn = purchasedOn;
    }

    public Integer getMobileDownloads() {
        return mobileDownloads;
    }

    public void setMobileDownloads(Integer mobileDownloads) {
        this.mobileDownloads = mobileDownloads;
    }

    public Integer getWebDownloads() {
        return webDownloads;
    }

    public void setWebDownloads(Integer webDownloads) {
        this.webDownloads = webDownloads;
    }

    public Integer getMaxDownloads() {
        return maxDownloads;
    }

    public void setMaxDownloads(Integer maxDownloads) {
        this.maxDownloads = maxDownloads;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public Boolean getReceived() {
        return received;
    }

    public void setReceived(Boolean received) {
        this.received = received;
    }

    public Boolean getPurchased() {
        return purchased;
    }

    public void setPurchased(Boolean purchased) {
        this.purchased = purchased;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rate) {
        this.rating = rate;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
