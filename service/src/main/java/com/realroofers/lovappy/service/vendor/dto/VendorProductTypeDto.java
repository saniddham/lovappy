package com.realroofers.lovappy.service.vendor.dto;

import com.realroofers.lovappy.service.product.dto.ProductTypeDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.vendor.model.VendorProductType;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class VendorProductTypeDto {

    private Integer id;
    private ProductTypeDto productType;
    private VendorDto vendor;
    private Boolean approved = false;
    private Boolean active = true;
    private UserDto createdBy;
    private UserDto approvedBy;

    public VendorProductTypeDto() {
    }

    public VendorProductTypeDto(VendorProductType vendorProductType) {
        if (vendorProductType.getId() != null) {
            this.id = vendorProductType.getId();
        }
        if (vendorProductType.getProductType() != null) {
            this.productType = new ProductTypeDto(vendorProductType.getProductType());
        }
        if (vendorProductType.getVendor()!=null) {
            this.vendor = new VendorDto(vendorProductType.getVendor());
        }
        this.approved = vendorProductType.getApproved();
        this.active = vendorProductType.getActive();

        if (vendorProductType.getCreatedBy()!=null) {
            this.createdBy = new UserDto(vendorProductType.getCreatedBy());
        }
        if (vendorProductType.getApprovedBy()!=null){
            this.approvedBy = new UserDto(vendorProductType.getApprovedBy());
        }
    }
}
