package com.realroofers.lovappy.service.credits.model;

import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 8/10/2018.
 */
@Entity
@DynamicUpdate
@Table(name = "users_credits")
@AssociationOverrides({
        @AssociationOverride(name = "id.music",
                joinColumns = @JoinColumn(name = "music_id")),
        @AssociationOverride(name = "id.user",
                joinColumns = @JoinColumn(name = "user_id")) })
@EqualsAndHashCode
public class UserCredits {
    private UserCreditPK id;

    @CreationTimestamp
    @Column(name = "created", insertable = false, updatable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date created;

    private Integer balance;

    @UpdateTimestamp
    private Date updated;


    @EmbeddedId
    public UserCreditPK getId() {
        return id;
    }

    public void setId(UserCreditPK id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
