package com.realroofers.lovappy.service.upload;

import com.realroofers.lovappy.service.core.AbstractService;
import com.realroofers.lovappy.service.upload.dto.ComedyCategoryDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Manoj
 */
public interface ComedyCategoryService extends AbstractService<ComedyCategoryDto, Integer> {
    Page<ComedyCategoryDto> findAll(Pageable pageable);
}
