package com.realroofers.lovappy.service.couples.impl;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.cloud.repo.CloudStorageRepo;
import com.realroofers.lovappy.service.couples.CoupleProfileService;
import com.realroofers.lovappy.service.couples.dto.CoupleProfileRequest;
import com.realroofers.lovappy.service.couples.dto.CouplesProfileDto;
import com.realroofers.lovappy.service.couples.model.CouplesProfile;
import com.realroofers.lovappy.service.couples.repo.CouplesProfileRepo;
import com.realroofers.lovappy.service.exception.ResourceNotFoundException;
import com.realroofers.lovappy.service.gallery.model.GalleryStorageFile;
import com.realroofers.lovappy.service.gallery.repo.GalleryStorageRepo;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.*;
import com.realroofers.lovappy.service.user.repo.*;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.ZodiacSigns;
import com.realroofers.lovappy.service.util.RandomStringGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Daoud Shaheen on 7/15/2018.
 */
@Service
@Slf4j
public class CoupleProfileServiceImpl implements CoupleProfileService{

    @Autowired
    private CouplesProfileRepo couplesProfileRepo;
    @Autowired
    private CloudStorageRepo cloudStorageRepo;
    @Autowired
    private GalleryStorageRepo galleryStorageRepo;
    @Autowired
    private UserProfileRepo userProfileRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private RoleRepo roleRepo;
    @Autowired
    private UserRolesRepo userRolesRepo;

    @Autowired
    private UserPreferenceRepo userPreferenceRepo;
    @Autowired
    private RandomStringGenerator randomStringGenerator;

    @Transactional
    @Override
    public CloudStorageFileDto uploadPhoto(CloudStorageFileDto uploadImage, Integer userId) {
        User user = userRepo.findOne(userId);

        UserProfile userProfile = user.getUserProfile();
        if(userProfile == null) {
            throw new ResourceNotFoundException("You should continue the registration proccess");
        }
        CouplesProfile couplesProfile = userProfile.getCouplesProfile();
        CloudStorageFile cloudStorageFile = cloudStorageRepo.findOne(uploadImage.getId());
        if(couplesProfile != null) {

            if (cloudStorageFile != null) {
                couplesProfile.setCouplePhotoImage(cloudStorageFile);
                couplesProfileRepo.save(couplesProfile);
            }
        }
        else {
            throw new ResourceNotFoundException("Couple profile is not exist");
        }


        //save to happy gallery
        GalleryStorageFile galleryStorageFile = new GalleryStorageFile();
        galleryStorageFile.setStatus(ApprovalStatus.PENDING);
        galleryStorageFile.setUrl(cloudStorageFile.getUrl());
        galleryStorageFile.setCreated(new Date());
        galleryStorageRepo.save(galleryStorageFile);
        return uploadImage;
    }

    @Transactional
    private void setUserRole(User user, Roles role, ApprovalStatus status) {
        UserRolePK pk = new UserRolePK(user, roleRepo.findByName(role.getValue()));
        UserRoles userRoles = new UserRoles();
        userRoles.setId(pk);
        if(role==Roles.USER){
            Role coupleRole = roleRepo.findByName(Roles.COUPLE.getValue());

            UserRoles coupleRoles = userRolesRepo.findOne(new UserRolePK(user, coupleRole));
            if(coupleRoles != null) {
                userRolesRepo.delete(coupleRoles);
            }
            userRoles.setApprovalStatus(ApprovalStatus.APPROVED);
        }else if(role==Roles.COUPLE){
            Role userRole = roleRepo.findByName(Roles.USER.getValue());

            UserRoles userR = userRolesRepo.findOne(new UserRolePK(user, userRole));
            if(userR != null)
                userRolesRepo.delete(userR);
            userRoles.setApprovalStatus(ApprovalStatus.APPROVED);
        }
        else {
            userRoles.setApprovalStatus(status);
        }
        userRoles.setCreated(new Date());
        userRolesRepo.save(userRoles);

    }

    @Transactional
    @Override
    public CouplesProfileDto signup(CoupleProfileRequest couplesRegistrationDto, Integer userId) {
        log.info("couple registration {}", couplesRegistrationDto);
        User user = userRepo.findOne(userId);
        setUserRole(user, Roles.COUPLE, ApprovalStatus.APPROVED );
        user = userRepo.save(user);
        // Save Profiles
        UserProfile profile = userProfileRepo.findOne(userId);
        if (profile == null) {
            profile = new UserProfile(userId);
        }

        CouplesProfile couplesProfile = profile.getCouplesProfile();
        if(couplesProfile == null){
            couplesProfile = new CouplesProfile();
            couplesProfile.setStatus(ApprovalStatus.PENDING);
        }

        if(couplesRegistrationDto.getGender() != null)
            profile.setGender(couplesRegistrationDto.getGender());
        if(couplesRegistrationDto.getBirthDate() != null) {
            profile.setBirthDate(couplesRegistrationDto.getBirthDate());
            profile.setZodiacSigns(ZodiacSigns.getZodiac(couplesRegistrationDto.getBirthDate()));
        }
        couplesProfile.setAnniversaryDate(couplesRegistrationDto.getAnniversaryDate());

        couplesProfile.setFirstDate(getDate(couplesRegistrationDto.getYearsTogether()));
        couplesProfile.getCouples().add(profile);
        couplesProfileRepo.save(couplesProfile);
        profile.setCouplesProfile(couplesProfile);
        userProfileRepo.save(profile);

        // Save Preferences
        UserPreference pref = userPreferenceRepo.findOne(userId);
        if (pref == null) {
            pref = new UserPreference(userId);
        }
        pref.setGender(couplesRegistrationDto.getCoupleGender());
        return new CouplesProfileDto(couplesProfile, user,   userPreferenceRepo.save(pref), null, null);
    }

    private Date getDate(Integer numberOfyears) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR) - numberOfyears;
        return  new GregorianCalendar(year,0,1).getTime();
    }

    @Transactional
    @Override
    public CouplesProfileDto update(CoupleProfileRequest coupleProfileRequest, Integer userId) {
        log.info("couple update {}", coupleProfileRequest);
        User user = userRepo.findOne(userId);

        // Save Profiles
        UserProfile profile = userProfileRepo.findOne(userId);
        if (profile == null) {
            profile = new UserProfile(userId);
        }

        CouplesProfile couplesProfile = profile.getCouplesProfile();
        if(couplesProfile == null){
            couplesProfile = new CouplesProfile();
            couplesProfile.setStatus(ApprovalStatus.PENDING);
        }

        if(coupleProfileRequest.getGender() != null)
            profile.setGender(coupleProfileRequest.getGender());
        if(coupleProfileRequest.getBirthDate() != null) {
            profile.setBirthDate(coupleProfileRequest.getBirthDate());
            profile.setZodiacSigns(ZodiacSigns.getZodiac(coupleProfileRequest.getBirthDate()));
        }
        if(coupleProfileRequest.getYearsTogether() != null)
            couplesProfile.setFirstDate(getDate(coupleProfileRequest.getYearsTogether()));
        if(coupleProfileRequest.getAnniversaryDate() != null)
            couplesProfile.setAnniversaryDate(coupleProfileRequest.getAnniversaryDate());

        couplesProfileRepo.save(couplesProfile);
        userProfileRepo.save(profile);

        // Save Preferences
        UserPreference pref = userPreferenceRepo.findOne(userId);
        if (pref == null) {
            pref = new UserPreference(userId);
        }
        if(coupleProfileRequest.getCoupleGender() != null)
            pref.setGender(coupleProfileRequest.getCoupleGender());
        userPreferenceRepo.save(pref);
        user.setUserPreference(pref);
        User partner = couplesProfileRepo.findPartner(user, couplesProfile);
        String partnerEmail = null;
        Integer partnerId = null;
        if(partner != null) {
            partnerEmail = partner.getEmail();
            partnerId = partner.getUserId();
        }
        return new CouplesProfileDto(couplesProfile, user, user.getUserPreference(), partnerEmail, partnerId);
    }

    @Transactional
    @Override
    public UserDto registerPartner(Integer userId, String partnerEmail) {

        User user = userRepo.findOneByUserId(userId);
        User partner = userRepo.findOneByEmail(partnerEmail);
        String password = null;
        if(partner == null) {
            partner = new User();
            partner.setEmail(partnerEmail);
            password = randomStringGenerator.generatePassword();
            partner.setPassword(password);
            partner.setEmailVerified(false);
        } else {
            if(partner.getUserProfile() != null && partner.getUserProfile().getCouplesProfile() !=null){
                CouplesProfile couplesProfile = partner.getUserProfile().getCouplesProfile();
                if(couplesProfile.getCouples().contains(user.getUserProfile())){
                    if(couplesProfile.getStatus() == ApprovalStatus.APPROVED)
                    return null;
                    else {
                        couplesProfile.setUpdated(new Date());
                        couplesProfileRepo.save(couplesProfile);
                        return new UserDto(partner);
                    }
                } else {
                    partner.getUserProfile().setCouplesProfile(null);
                }
            }
        }
        setUserRole(partner, Roles.COUPLE, ApprovalStatus.PENDING);
        partner  = userRepo.save(partner);
        // Save Profiles
        UserPreference pref = userPreferenceRepo.findOne(userId);
        UserProfile partnerProfile = userProfileRepo.findOne(partner.getUserId());
        if (partnerProfile == null) {
            partnerProfile = new UserProfile(partner.getUserId());
            partnerProfile.setGender(pref.getGender());
        } else {
            //check if the first partner enter the correct values of the second one
            if (partnerProfile.getGender() != pref.getGender()){
                pref.setGender(partnerProfile.getGender());
                userPreferenceRepo.save(pref);
            }
        }

        CouplesProfile couplesProfile = user.getUserProfile().getCouplesProfile();


        partnerProfile.setCouplesProfile(couplesProfile);
        userProfileRepo.save(partnerProfile);

        List<UserProfile> userProfiles = couplesProfile.getCouples();
        for (UserProfile updatedUser: userProfiles) {
            if(updatedUser.getUserId() == partnerProfile.getUserId() || updatedUser.getUserId() == user.getUserId()) {
                continue;
            }
            setUserRole(updatedUser.getUser(), Roles.USER, ApprovalStatus.APPROVED);
            updatedUser.setCouplesProfile(null);

        }
        userProfileRepo.save(userProfiles);

        //remove all couple partners if there are before
        couplesProfile.getCouples().removeAll(userProfiles);
        //add the new couples users
        couplesProfile.getCouples().add(partnerProfile);
        couplesProfile.getCouples().add(user.getUserProfile());
        couplesProfile.setStatus(ApprovalStatus.PENDING);
        couplesProfile.setUpdated(new Date());
        couplesProfileRepo.save(couplesProfile);
        // Save Preferences
        UserPreference partnerPref = userPreferenceRepo.findOne(partner.getUserId());
        if (partnerPref == null) {
            partnerPref = new UserPreference(userId);
        }
        partnerPref.setGender(user.getUserPreference().getGender());
        userPreferenceRepo.save(partnerPref);


        UserDto userDto = new UserDto(partner);
        userDto.setPassword(password);
        return userDto;
    }


    @Transactional(readOnly = true)
    @Override
    public CouplesProfileDto getCoupleProfile(Integer userId) {
        User user = userRepo.findOneByUserId(userId);
        UserProfile profile = user.getUserProfile();

        if(profile == null) {
            throw new ResourceNotFoundException("There is no profile for this user " + userId);
        }
        CouplesProfile couplesProfile = profile.getCouplesProfile();
        if(couplesProfile == null) {
            return null;
        }

        User partner = couplesProfileRepo.findPartner(user, couplesProfile);
        String partnerEmail = null;
        Integer partnerId = null;
        if(partner != null) {
            partnerEmail = partner.getEmail();
            partnerId = partner.getUserId();
        }
        CouplesProfileDto result = new CouplesProfileDto(couplesProfile, user, user.getUserPreference(), partnerEmail, partnerId);

        return result;
    }

    @Transactional(readOnly = true)
    @Override
    public UserDto partnerApprove(String  partnerEmail) {
        User partner = userRepo.findOneByEmail(partnerEmail);
        if(partner.getUserProfile() == null || partner.getUserProfile().getCouplesProfile() == null)
            return null;
        CouplesProfile couplesProfile = partner.getUserProfile().getCouplesProfile();
        couplesProfile.setStatus(ApprovalStatus.APPROVED);
        return new UserDto(partner);
    }
}
