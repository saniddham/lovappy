package com.realroofers.lovappy.service.event.model;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Tejaswi Venupalli on 8/28/2017.
 */

@Entity
@Table(name = "event_location_picture")
@EqualsAndHashCode
public class EventLocationPicture {

    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    @JoinColumn(name ="event_id")
    private Event eventId;


    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile picture;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Event getEventId() {
        return eventId;
    }

    public void setEventId(Event eventId) {
        this.eventId = eventId;
    }

    public CloudStorageFile getPicture() {
        return picture;
    }

    public void setPicture(CloudStorageFile picture) {
        this.picture = picture;
    }
}
