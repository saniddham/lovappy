package com.realroofers.lovappy.service.plan.repo;

import com.realroofers.lovappy.service.plan.dto.FeatureDetailsDto;
import com.realroofers.lovappy.service.plan.model.Feature;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by hasan on 9/11/2017.
 */
public interface FeatureRepo extends JpaRepository<Feature, Integer> {

    List<FeatureDetailsDto> findByIsValid(Boolean isValid);

}
