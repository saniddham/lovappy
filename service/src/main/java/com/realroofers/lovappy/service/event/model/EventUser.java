package com.realroofers.lovappy.service.event.model;

import com.realroofers.lovappy.service.event.support.UserEventStatus;
import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Tejaswi Venupalli on 8/28/17
 */
@Entity
@Table(name = "event_user")
@AssociationOverrides({
        @AssociationOverride(name = "id.event",
                joinColumns = @JoinColumn(name = "event_id")),
        @AssociationOverride(name = "id.user",
                joinColumns = @JoinColumn(name = "user_id")) })
@EqualsAndHashCode
public class EventUser {


    private EventUserId id;
    @Enumerated(EnumType.STRING)
    private UserEventStatus status;

    @Column(columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date registrationDate;

    @Column(columnDefinition = "DATETIME")
    private Date cancellationDate;

    private Boolean isPaymentMade;

    @EmbeddedId
    public EventUserId getId() {
        return id;
    }

    public void setId(EventUserId id) {
        this.id = id;
    }

    public UserEventStatus getStatus() {
        return status;
    }

    public void setStatus(UserEventStatus status) {
        this.status = status;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Date getCancellationDate() {
        return cancellationDate;
    }

    public void setCancellationDate(Date cancellationDate) {
        this.cancellationDate = cancellationDate;
    }

    public Boolean getPaymentMade() {
        return isPaymentMade;
    }

    public void setPaymentMade(Boolean paymentMade) {
        isPaymentMade = paymentMade;
    }
}
