package com.realroofers.lovappy.service.gift;

import com.realroofers.lovappy.service.gift.dto.GiftDto;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;

import java.util.Collection;

/**
 * Created by Eias Altawil on 5/7/17
 */
public interface GiftService {

    Collection<GiftDto> getAllGifts();

    GiftDto getGiftByID(Integer giftID);

    OrderDto purchaseGift(Integer fromUserID, Integer toUserID, Integer giftID, Integer quantitys, String coupon, PaymentMethodType paymentMethodType);

    CalculatePriceDto calculatePrice(GiftDto gift, String coupon);
}
