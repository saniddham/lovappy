package com.realroofers.lovappy.service.event.dto;

import com.realroofers.lovappy.service.event.model.EventCategory;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class EventCategoryDto {

    private Integer id;
    private String categoryName;
    private Boolean enabled;

    public EventCategoryDto() {
    }

    public EventCategoryDto(EventCategory category) {
        this.id = category.getId();
        this.categoryName = category.getCategoryName();
        this.enabled = category.getEnabled();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
