package com.realroofers.lovappy.service.order.dto;

import com.realroofers.lovappy.service.order.support.ProductType;
import lombok.Data;
import java.io.Serializable;
import java.util.List;

@Data
public class ChargeRequest implements Serializable {
 
    public enum Currency {
        EUR, USD
    }
    private String description;
    private Double amount;
    private Currency currency;
    private String email;
    private String coupon;
    private Integer quantity;
    private List<ChargeRequestDetails> chargeRequestDetailsList;

    @Override
    public String toString() {
        return "ChargeRequest{" +
                "description='" + description + '\'' +
                ", amount=" + amount +
                ", currency=" + currency +
                ", email='" + email + '\'' +
                ", coupon='" + coupon + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}