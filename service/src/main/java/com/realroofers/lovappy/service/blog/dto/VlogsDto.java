package com.realroofers.lovappy.service.blog.dto;

import com.realroofers.lovappy.service.blog.model.Vlogs;
import com.realroofers.lovappy.service.blog.support.PostStatus;
import com.realroofers.lovappy.service.core.VideoProvider;
import lombok.EqualsAndHashCode;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 8/8/2017.
 */
@EqualsAndHashCode
public class VlogsDto {

    private Integer id;

    private String title;

    private String keywords;

    private String captions;

    private Date created;

    private String videoUrl;

    private PostStatus state;

    private String videoId;

    private VideoProvider videoProvider;

    public VlogsDto(Integer id, String title, String keywords, String captions) {
        this.id = id;
        this.title = title;
        this.keywords = keywords;
        this.captions = captions;
        this.created = created;
    }

    public VlogsDto() {
    }
    public VlogsDto(Vlogs vlog) {

        this.id = vlog.getId();
        this.title = vlog.getTitle();
        this.keywords= vlog.getKeywords();
        this.captions = vlog.getCaptions();
        this.state = vlog.getState();
        this.videoUrl = vlog.getVideoLink();
        this.created = vlog.getCreated();
        this.videoId = vlog.getVideoId();
        this.videoProvider = vlog.getVideoProvider();

    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getCaptions() {
        return captions;
    }

    public void setCaptions(String captions) {
        this.captions = captions;
    }

    public Date getCreated() {
        return created;
    }

    public String getPostedOn() {
        SimpleDateFormat formatPostOnDate = new SimpleDateFormat("MM-dd-yyyy");
        if(created ==null)
            return "";
        String formatted = formatPostOnDate.format( getCreated());
        return formatted;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public PostStatus getState() {
        return state;
    }

    public void setState(PostStatus state) {
        this.state = state;
    }

    public String getAuthor(){
        return "Lovappy";
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public VideoProvider getVideoProvider() {
        return videoProvider;
    }

    public void setVideoProvider(VideoProvider videoProvider) {
        this.videoProvider = videoProvider;
    }

    public String getThumbnail() {
        if (videoProvider.equals(VideoProvider.YOUTUBE))
        return "https://img.youtube.com/vi/"+videoId+"/0.jpg";

        //todo get other thumbnail
        return videoId;
    }
}
