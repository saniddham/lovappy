package com.realroofers.lovappy.service.product.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.product.dto.BrandCommissionDto;
import com.realroofers.lovappy.service.user.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@EqualsAndHashCode
@Table(name = "brand_commission")
public class BrandCommission extends BaseEntity<Integer> implements Serializable {

    @Column(name = "percentage")
    private Double commissionPercentage;

    @Column(name = "affective_date")
    private Date affectiveDate;

    @ManyToOne
    @JoinColumn(name = "created_by")
    private User createdBy;

    @ManyToOne
    @JoinColumn(name = "modified_by")
    private User modifiedBy;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;


    public BrandCommission() {
    }

    public BrandCommission(BrandCommissionDto brandCommissionDto) {
        if (brandCommissionDto.getId() != null) {
            this.setId(brandCommissionDto.getId());
        }
        this.commissionPercentage = brandCommissionDto.getCommissionPercentage();
        this.affectiveDate = brandCommissionDto.getAffectiveDate();
        this.createdBy = brandCommissionDto.getCreatedBy() != null ? new User(brandCommissionDto.getCreatedBy().getID()) : null;
        this.modifiedBy = brandCommissionDto.getModifiedBy() != null ? new User(brandCommissionDto.getModifiedBy().getID()) : null;
        this.active = brandCommissionDto.getActive();
        this.setUpdated(brandCommissionDto.getUpdated());
    }
}
