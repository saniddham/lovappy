package com.realroofers.lovappy.service.user.dto;


import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.notification.model.NotificationTypes;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.validator.UserRegister;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Tejaswi Venupalli on 8/30/2017.
 */
@EqualsAndHashCode
@Data
public class RegisterExistingUserDto {

    @NotNull(message = "Please provide your full name", groups = {UserRegister.class})
    private String name;

    @NotNull(message = "Please provide your phone number", groups = {UserRegister.class})
    private String phoneNumber;

    private Gender gender;

    private Date birthDate;


    private CloudStorageFileDto photoIdentificationCloudFile;

    @Enumerated(EnumType.STRING)
    private ApprovalStatus approvalStatus;

    NotificationTypes notificationType;

    Roles role;

    public RegisterExistingUserDto(){

    }
    public RegisterExistingUserDto(User user){
        this(user, user.getUserProfile());

    }
    public RegisterExistingUserDto(User user, UserProfile userProfile){
            this.name = user!= null ? user.getFirstName()+' '+user.getLastName() : null;
            this.phoneNumber = user!= null ? user.getPhoneNumber() : null;
            this.photoIdentificationCloudFile = userProfile!= null ? new CloudStorageFileDto(userProfile.getPhotoIdentificationCloudFile()):null;
    }
}
