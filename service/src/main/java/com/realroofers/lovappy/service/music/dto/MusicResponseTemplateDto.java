package com.realroofers.lovappy.service.music.dto;

import com.realroofers.lovappy.service.music.model.MusicResponseType;

import java.io.Serializable;

public class MusicResponseTemplateDto implements Serializable{

    private Integer id;

    private MusicResponseType type;

    private String template;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MusicResponseType getType() {
        return type;
    }

    public void setType(MusicResponseType type) {
        this.type = type;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
}
