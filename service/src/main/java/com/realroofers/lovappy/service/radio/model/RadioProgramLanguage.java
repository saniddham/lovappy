package com.realroofers.lovappy.service.radio.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.system.model.Language;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Manoj on 17/12/2017.
 */

@Data
@Entity
@Table(name = "radio_control_has_language")
@EqualsAndHashCode
public class RadioProgramLanguage  extends BaseEntity<Integer> implements Serializable {

    @ManyToOne
    @JoinColumn(name = "fk_radio_program_controls")
    private RadioProgramControl radioProgramControl;

    @ManyToOne
    @JoinColumn(name = "fk_language")
    private Language language;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;


}
