package com.realroofers.lovappy.service.vendor.model;

import java.io.Serializable;

public enum VerificationType implements Serializable {
    SMS,CALL
}
