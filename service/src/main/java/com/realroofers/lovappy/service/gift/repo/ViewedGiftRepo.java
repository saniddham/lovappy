package com.realroofers.lovappy.service.gift.repo;

import com.realroofers.lovappy.service.gift.model.ViewedGift;
import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Manoj on 02/02/2018.
 */
public interface ViewedGiftRepo extends JpaRepository<ViewedGift,Integer> {

    ViewedGift findTopByUserOrderByViewedOnDesc(User user);

    List<ViewedGift> findAllByUserOrderByViewedOnDesc(User user);

    ViewedGift findTopByUserAndProduct(@Param("user") User user, @Param("product")Product product);
}
