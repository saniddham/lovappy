package com.realroofers.lovappy.service.user.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.realroofers.lovappy.service.user.model.UserPreference;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public interface UserPreferenceRepo extends JpaRepository<UserPreference, Integer>, QueryDslPredicateExecutor<UserPreference> {
}
