package com.realroofers.lovappy.service.user.model.embeddable;

import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@Embeddable
@EqualsAndHashCode
public class Address implements Serializable {
    private String zipCode;
    private String country;
    private String countryCode;
    private String state; // or region
    private String stateShort;
    private String province; // or county
    private String city;
    private String streetNumber;
    private String fullAddress;
    private Double latitude;
    private Double longitude;
    private String lastAddressSearch;
    private Integer lastMileRadiusSearch;

    public Address() {
        latitude = 0.0;
        longitude = 0.0;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateShort() {
        return stateShort;
    }

    public void setStateShort(String stateShort) {
        this.stateShort = stateShort;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getLastAddressSearch() {
        return lastAddressSearch;
    }

    public void setLastAddressSearch(String lastAddressSearch) {
        this.lastAddressSearch = lastAddressSearch;
    }

    public Integer getLastMileRadiusSearch() {
        return lastMileRadiusSearch;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setLastMileRadiusSearch(Integer lastMileRadiusSearch) {
        this.lastMileRadiusSearch = lastMileRadiusSearch;
    }
}
