package com.realroofers.lovappy.service.util;

import com.google.maps.model.LatLng;
import com.realroofers.lovappy.service.ads.repo.AdsRepo;
import com.realroofers.lovappy.service.blog.PostService;
import com.realroofers.lovappy.service.blog.repo.PostRepo;
import com.realroofers.lovappy.service.cms.model.*;
import com.realroofers.lovappy.service.cms.repo.PageMetaDataRepository;
import com.realroofers.lovappy.service.cms.repo.PageRepository;
import com.realroofers.lovappy.service.cms.repo.WidgetRepository;
import com.realroofers.lovappy.service.cms.utils.PageConstants;
import com.realroofers.lovappy.service.core.VideoProvider;
import com.realroofers.lovappy.service.datingPlaces.model.*;
import com.realroofers.lovappy.service.datingPlaces.repo.*;
import com.realroofers.lovappy.service.datingPlaces.support.google_places.DatingPlaceLocationUtil;
import com.realroofers.lovappy.service.event.model.EventCategory;
import com.realroofers.lovappy.service.event.repo.EventCategoryRepo;
import com.realroofers.lovappy.service.gallery.model.GalleryStorageFile;
import com.realroofers.lovappy.service.gallery.repo.GalleryStorageRepo;
import com.realroofers.lovappy.service.lovstamps.model.LovstampSample;
import com.realroofers.lovappy.service.lovstamps.repo.LovstampRepo;
import com.realroofers.lovappy.service.lovstamps.repo.LovstampSampleRepo;
import com.realroofers.lovappy.service.lovstamps.support.SampleTypes;
import com.realroofers.lovappy.service.mail.EmailTemplateService;
import com.realroofers.lovappy.service.mail.dto.EmailTemplateDto;
import com.realroofers.lovappy.service.mail.model.LovappyEmail;
import com.realroofers.lovappy.service.mail.repo.LovappyEmailRepo;
import com.realroofers.lovappy.service.mail.support.EmailTemplateConstants;
import com.realroofers.lovappy.service.mail.support.EmailTypes;
import com.realroofers.lovappy.service.music.model.MusicResponseTemplate;
import com.realroofers.lovappy.service.music.model.MusicResponseType;
import com.realroofers.lovappy.service.music.repo.MusicResponseTemplateRepo;
import com.realroofers.lovappy.service.order.model.Price;
import com.realroofers.lovappy.service.order.repo.PriceRepo;
import com.realroofers.lovappy.service.plan.PlansService;
import com.realroofers.lovappy.service.plan.dto.FeatureDetailsDto;
import com.realroofers.lovappy.service.plan.dto.PlanDetailsDto;
import com.realroofers.lovappy.service.plan.dto.PlansDto;
import com.realroofers.lovappy.service.plan.repo.PlanRepo;
import com.realroofers.lovappy.service.product.ProductTypeService;
import com.realroofers.lovappy.service.product.dto.ProductTypeDto;
import com.realroofers.lovappy.service.radio.AudioSubTypeService;
import com.realroofers.lovappy.service.radio.AudioTypeService;
import com.realroofers.lovappy.service.radio.MediaTypeService;
import com.realroofers.lovappy.service.radio.model.AudioSubType;
import com.realroofers.lovappy.service.radio.model.AudioType;
import com.realroofers.lovappy.service.radio.model.MediaType;
import com.realroofers.lovappy.service.radio.repo.RadioProgramControlRepo;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.system.repo.LanguageRepo;
import com.realroofers.lovappy.service.user.CountryService;
import com.realroofers.lovappy.service.user.model.*;
import com.realroofers.lovappy.service.user.model.embeddable.Address;
import com.realroofers.lovappy.service.user.repo.RoleRepo;
import com.realroofers.lovappy.service.user.repo.UserProfileRepo;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import com.realroofers.lovappy.service.user.repo.UserRolesRepo;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.vendor.VendorLocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;

/**
 * Created by Eias Altawil on 5/6/17
 */

@Component
public class InitialDataLoader implements
        ApplicationListener<ContextRefreshedEvent> {
    private static final Logger LOGGER = LoggerFactory.getLogger(InitialDataLoader.class);
    private boolean alreadySetup = false;

    private final UserRepo userRepo;
    private final UserProfileRepo userProfileRepo;
    private final RoleRepo roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final LanguageRepo languageRepo;
    private final PageRepository pageRepository;
    private final WidgetRepository widgetRepository;
    private final LovappyEmailRepo lovappyEmailRepo;
    private final EmailTemplateService emailTemplateService;
    private final EventCategoryRepo eventCategoryRepo;
    private final PlanRepo planRepo;
    private final AdsRepo adsRepo;
    private final FoodTypeRepo foodTypeRepo;
    private final PlaceAtmosphereRepo placeAtmosphereRepo;
    private final DealsRepo dealsRepository;
    private final PersonTypeRepo personTypeRepo;

    private final PlansService plansService;
    private final PageMetaDataRepository pageMetaDataRepository;


    private final LovstampSampleRepo lovstampSampleRepo;
    private final MediaTypeService mediaTypeService;
    private final AudioTypeService audioTypeService;
    private final AudioSubTypeService audioSubTypeService;
    private final GalleryStorageRepo galleryStorageRepo;
    private final CountryService countryService;
    private final ProductTypeService productTypeService;
    private final AddressUtil addressUtil;
    private final PostRepo postRepo;
    private final PostService postService;
    private final PriceRepo priceRepo;
    private final RoleRepo roleRepo;
    private final UserRolesRepo userRolesRepo;
    private final VendorLocationService vendorLocationService;
    private final MusicResponseTemplateRepo musicResponseTemplateRepo;
    private final DatingPlaceRepo datingPlaceRepo;
    private final DatingPlaceReviewRepo datingPlaceReviewRepo;
    private final DatingPlaceLocationUtil locationUtil;

    @Autowired
    public InitialDataLoader(UserRepo userRepository, UserProfileRepo userProfileRepo, RoleRepo roleRepository,
                             PasswordEncoder passwordEncoder, LanguageRepo languageRepo,
                             PageRepository pageRepository, WidgetRepository widgetRepository,
                             LovappyEmailRepo lovappyEmailRepo,
                             EmailTemplateService emailTemplateService, EventCategoryRepo eventCategoryRepo, PostService postService,
                             PlanRepo planRepo, AdsRepo adsRepo, PersonTypeRepo personTypeRepo,
                             PlansService plansService, FoodTypeRepo foodTypeRepo, PlaceAtmosphereRepo placeAtmosphereRepo,
                             DealsRepo dealsRepository, PageMetaDataRepository pageMetaDataRepository,
                             RadioProgramControlRepo radioProgramControlRepo, LovstampSampleRepo lovstampSampleRepo,
                             MediaTypeService mediaTypeService, AudioTypeService audioTypeService,
                             AudioSubTypeService audioSubTypeService, GalleryStorageRepo galleryStorageRepo,
                             CountryService countryService, ProductTypeService productTypeService,
                             AddressUtil addressUtil, LovstampRepo lovstampRepo, PostRepo postRepo, PostService postService1,
                             PriceRepo priceRepo, RoleRepo roleRepo, UserRolesRepo userRolesRepo, VendorLocationService vendorLocationService, MusicResponseTemplateRepo musicResponseTemplateRepo, DatingPlaceRepo datingPlaceRepo, DatingPlaceReviewRepo datingPlaceReviewRepo, DatingPlaceLocationUtil locationUtil) {
        this.userProfileRepo = userProfileRepo;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.languageRepo = languageRepo;
        this.personTypeRepo = personTypeRepo;
        this.lovstampSampleRepo = lovstampSampleRepo;
        this.pageRepository = pageRepository;
        this.widgetRepository = widgetRepository;
        this.userRepo = userRepository;
        this.lovappyEmailRepo = lovappyEmailRepo;
        this.emailTemplateService = emailTemplateService;
        this.eventCategoryRepo = eventCategoryRepo;
        this.planRepo = planRepo;
        this.adsRepo = adsRepo;
        this.plansService = plansService;
        this.foodTypeRepo = foodTypeRepo;
        this.placeAtmosphereRepo = placeAtmosphereRepo;
        this.dealsRepository = dealsRepository;
        this.pageMetaDataRepository = pageMetaDataRepository;
        this.mediaTypeService = mediaTypeService;
        this.audioTypeService = audioTypeService;
        this.audioSubTypeService = audioSubTypeService;
        this.galleryStorageRepo = galleryStorageRepo;
        this.countryService = countryService;
        this.productTypeService = productTypeService;
        this.addressUtil = addressUtil;
        this.postRepo = postRepo;
        this.postService = postService1;
        this.priceRepo = priceRepo;
        this.roleRepo = roleRepo;
        this.userRolesRepo = userRolesRepo;
        this.vendorLocationService = vendorLocationService;
        this.musicResponseTemplateRepo = musicResponseTemplateRepo;
        this.datingPlaceRepo = datingPlaceRepo;
        this.datingPlaceReviewRepo = datingPlaceReviewRepo;
        this.locationUtil = locationUtil;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {

        alreadySetup = false;
        if (alreadySetup)
            return;


//        createRoleIfNotFound(Roles.SUPER_ADMIN.getValue());
//        createRoleIfNotFound(Roles.AUTHOR.getValue());
//        createRoleIfNotFound(Roles.ADMIN.getValue());
//        createRoleIfNotFound(Roles.USER.getValue());
//        createRoleIfNotFound(Roles.EVENT_AMBASSADOR.getValue());
//        createRoleIfNotFound(Roles.COUPLE.getValue());
//        createRoleIfNotFound(Roles.MUSICIAN.getValue());
//        createRoleIfNotFound(Roles.PLACE_OWNER.getValue());
//        createRoleIfNotFound(Roles.VENDOR.getValue());
//        createRoleIfNotFound(Roles.DEVELOPER.getValue());
//        Role adminRole = roleRepository.findByName(Roles.ADMIN.getValue());
//        User user = userRepo.findOneByEmail("admin@lovappy.com");
//        if (user == null) {
//            user = new User();
//            user.setFirstName("Admin");
//            user.setLastName("Admin");
//            user.setPassword(passwordEncoder.encode("Adm!n@LoVAppy"));
//            user.setEmail("admin@lovappy.com");
//            user.setEmailVerified(true);
//            Set<UserRoles> roles = new HashSet<>();
//            user.setRoles(roles);
//            setUserRole(user,Roles.ADMIN, ApprovalStatus.APPROVED);
//
//            user.setEnabled(true);
//
//            userRepo.save(user);
//        }
//
//        Role superRole = roleRepository.findByName(Roles.SUPER_ADMIN.getValue());
//        User superAdmin = userRepo.findOneByEmail("super@lovappy.com");
//        if (superAdmin == null) {
//            superAdmin = new User();
//            superAdmin.setFirstName("Super");
//            superAdmin.setLastName("Admin");
//            superAdmin.setPassword(passwordEncoder.encode("Adm!n@LoVAppy"));
//            superAdmin.setEmail("super@lovappy.com");
//            superAdmin.setEmailVerified(true);
//            Set<UserRoles> roles = new HashSet<>();
//            user.setRoles(roles);
//            setUserRole(superAdmin,Roles.SUPER_ADMIN, ApprovalStatus.APPROVED);
//            superAdmin.setEnabled(true);
//
//            userRepo.save(superAdmin);
//        }
//        Set<Role> roles = new HashSet<>();
//        roles.add(adminRole);
//        roles.add(superRole);
//
//        List<User> admins = userRepo.findByRolesIn(roles, new PageRequest(0, 100)).getContent();
//        for (User admin : admins) {
//            UserProfile userProfile = userProfileRepo.findOne(admin.getUserId());
//            if (userProfile == null) {
//                userProfile = new UserProfile(admin.getUserId());
//                userProfileRepo.save(userProfile);
//
//            }
//        }


//        createLanguageIfNotFound("English", "EN");
//        createLanguageIfNotFound("Mandarin", "MAN");
//        createLanguageIfNotFound("Spanish", "ES");
//        createLanguageIfNotFound("Arabic", "AR");
//        createLanguageIfNotFound("Bengali", "BN");
//        createLanguageIfNotFound("Hindi", "HI");
//        createLanguageIfNotFound("Russian", "RU");
//        createLanguageIfNotFound("Portuguese", "PT");
//        createLanguageIfNotFound("Japanese", "JA");
//        createLanguageIfNotFound("Deutsch", "DE");
//        createLanguageIfNotFound("Chinese, Wu", "WU");
//        createLanguageIfNotFound("Javanese", "JV");
//        createLanguageIfNotFound("Korean", "KO");
//        createLanguageIfNotFound("French", "FR");
//        createLanguageIfNotFound("Turkish", "TR");
//        createLanguageIfNotFound("Vietnamese", "VI");
//        createLanguageIfNotFound("Telegu", "TE");
//        createLanguageIfNotFound("Cantonese", "CAN");
//        createLanguageIfNotFound("Merathi", "MR");
//        createLanguageIfNotFound("Tamil", "TA");
//        createLanguageIfNotFound("Italian", "IT");
//        createLanguageIfNotFound("Urdu", "UR");
//        createLanguageIfNotFound("Gujarati", "GU");
//        createLanguageIfNotFound("Polish", "PL");
//        createLanguageIfNotFound("Ukranian", "UR");
//        createLanguageIfNotFound("Farsi", "FA");
//        createLanguageIfNotFound("Punjabi", "PA");
//        createLanguageIfNotFound("Greek", "EL");
//        createLanguageIfNotFound("Thai", "TH");
//        createLanguageIfNotFound("Dutch", "NL");

//        //Check if Pages are existing if not createOrUpdate new one
//        initializeHomePage();
//
//        //Home pages
//        initializeVideoHomepage();
//        initializeAlternativeHomepage();
//        initializeVideoHomepage();
//
//
//        //Landing Pages
//        initializeMusicLandingPages();
//        initializeBlogLandingPage();
//        initializeEventsLandingPage();
//        initializeGiftsLandingPage();
//
//        // initialize Login page
//        initializeLoginPage();
//        // initialize Forgot Password page
//        initializeForgotPasswordPage();
//        // initialize Prime page
//        initializePrimePage();
//
//        // initialize Lite page
//        initializeLitePage();
//
//        // initialize Plans & Pricing page
//        initializePlansAndPricingPage();
//
//        // initialize Features page
//        initializeFeaturesPage();
//        // initialize Mobile page
//        initializeMobilePage();
//
//        // initialize about us page
//        initializeAboutUsPage();
//
//        // initialize contact us page
//        initializeContactUsPage();
//
//        // initialize Upload Music page
//        initializeUploadMusic();
//
//        // initialize Gift Music Survey page
//        initializeGiftMusicSurvey();
//
//        // initialize How This Works page
//        initializeHowThisWorksPage();
//
//        // initialize Our Advertisers page
//        initializeOurAdvertisersPage();
//
//        // initialize Advertise With Us page
//        initializeAdvertiseWithUsPage();
//
//        // initialize All PodCasts page
//        initializeAllPodCastsPage();
//        // initialize All PodCasts page
//        initializeSubmitBlogPage();
//
//        initializeEventsListPage();
//        initializeFaqPage();
//        initializeAdsTermPage();
//        initializeDevTermPage();
//        initializePodcastTermPage();
//
//        initializeMusicTermPage();
//
//        initializeComedyTermPage();
//
//        initializeNewsTermPage();
//
//        initializeEventsTermPage();
//        initializeAudioBookTermPage();
//        initializeErrorsPage();
//
//        initializeFeaturesRecordVoicePage();
//
//        initializeDatingPlacesPage();
//
//        initializeHappyCouplePage();
//
//        initializeAllRadioPage();
//
//        createLovappyEmails();
//
 //    createEmailTemplates();
//
//
//        createEventCategories();
//
//        initializePlansDetails();
//
//        createFoodTypes();
//
//        createAtomospheres();
//
//        createPersonTypes();
//
//        createRadioControlsMasterData();
//
//        createDeals();
//        setLovstampSamples();
//        setDefaultLovstampSamples();
//        updatePageName();
//        setUserNullprofileAccessToFalse();
//        setPagesLanguages();
//        setGalleryNullvaluesToFalse();
//
//        createCountry();
//        addProductType();
//        setUserRoleToCompletedProfiles();
//        createPrices();

//        initializePageMetaData();
//        initializeMusicianTermPage();
//                initializeVendorTermPage();
//        updateVendorLocationToDefault();
//
//        addMusicTemplate(MusicResponseType.THANKS,"Thanks for the Song!");
//        addMusicTemplate(MusicResponseType.THANKS_AND_NEW_SONG,"Thank you! Can you send another Song?");
        //setUserActivated();
//        updateDefaultUserAverageRatingDatingPlaces();
//        updateDefaultOverAllRatingReviews();
//        updatePlaceNamesInRatingReviews();
//        updateReviewStatusesInRatingReviews();
        //setUserLocationNotNull();
        initializeMusicUpload();
        initializeMusicianPage();
        migratePlacesCounters();
        alreadySetup = true;
    }

    @Transactional
    public void migratePlacesCounters() {
        List<DatingPlace> datingPlaceList = datingPlaceRepo.findAll();
        for (DatingPlace datingPlace: datingPlaceList) {
            if(datingPlace.getSuitableForMaleCount() == null)
            datingPlace.setSuitableForMaleCount(0);
            if(datingPlace.getSuitableForFemale() == null)
                datingPlace.setSuitableForFemale(0);
            if(datingPlace.getSuitableForBoth() == null)
                datingPlace.setSuitableForBoth(0);

            if(datingPlace.getFeatured() == null){
                datingPlace.setFeatured(false);
            }
            datingPlaceRepo.save(datingPlace);
        }
    }

    @Transactional
    public void updateDefaultUserAverageRatingDatingPlaces() {
        List<DatingPlace> datingPlaceList = datingPlaceRepo.findAllByAverageOverAllRatingByUserIsNull();
        datingPlaceList.forEach(datingPlace -> {
            datingPlace.setAverageOverAllRatingByUser(datingPlace.getAverageRating());
            datingPlaceRepo.saveAndFlush(datingPlace);
        });
    }

    @Transactional
    public void updateDefaultOverAllRatingReviews() {
        List<DatingPlaceReview> datingPlaceReviewList = datingPlaceReviewRepo.findAllByOverallRatingIsNull();
        datingPlaceReviewList.forEach(datingPlaceReview -> {
            datingPlaceReview.setOverallRating(datingPlaceReview.getAverageRating());
            datingPlaceReviewRepo.saveAndFlush(datingPlaceReview);
        });
    }

    @Transactional
    public void updatePlaceNamesInRatingReviews() {
        List<DatingPlaceReview> datingPlaceReviewList = datingPlaceReviewRepo.findAllByPlaceNameIsNull();
        datingPlaceReviewList.forEach(datingPlaceReview -> {
            if (datingPlaceReview.getPlaceId() != null) {
                datingPlaceReview.setPlaceName(datingPlaceReview.getPlaceId().getPlaceName());
                datingPlaceReviewRepo.saveAndFlush(datingPlaceReview);
            } else {
                if (datingPlaceReview.getGooglePlaceId() != null) {
                    try {
                        datingPlaceReview.setPlaceName(locationUtil.getGooglePlaceDetails(datingPlaceReview.getGooglePlaceId()).getPlaceName());
                        datingPlaceReviewRepo.saveAndFlush(datingPlaceReview);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Transactional
    public void updateReviewStatusesInRatingReviews() {
        List<DatingPlaceReview> datingPlaceReviewList = datingPlaceReviewRepo.findAllByReviewApprovalStatusIsNullAndReviewerTypeEquals("owner");
        datingPlaceReviewList.forEach(datingPlaceReview -> {
            datingPlaceReview.setReviewApprovalStatus(ApprovalStatus.APPROVED);
            datingPlaceReviewRepo.saveAndFlush(datingPlaceReview);
        });
    }

    @Transactional
    public void updateVendorLocationToDefault() {
        vendorLocationService.findAllVendorLocationsNull().stream().forEach(v -> {
            if (v.getLatitude() == null) {
                v.setLatitude(37.724703);
                v.setLongitude(-97.465846);
                try {
                    vendorLocationService.update(v);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Transactional
    public void createPrices() {
        List<Price> prices = new ArrayList<>();
        Price price = new Price();
//        price.setPrice(1.5);
//        price.setType(OrderDetailType.AD);
//        prices.add(price);
//        price = new Price();
//        price.setPrice(1.5);
//        price.setType(OrderDetailType.PRIVATE_MESSAGE);
//        prices.add(price);
//
//        price = new Price();
//        price.setPrice(1.5);
//        price.setType(OrderDetailType.DATING_PLACE);
//        prices.add(price);
//
//        price = new Price();
//        price.setPrice(1.5);
//        price.setType(OrderDetailType.EVENT_ATTEND);
//        prices.add(price);
//
//        price = new Price();
//        price.setPrice(1.5);
//        price.setType(OrderDetailType.GIFT);
//        prices.add(price);
//        price = priceRepo.findByType(OrderDetailType.CREDITS);
//        if(price == null) {
//            price=new Price();
//            price.setPrice(1.99);
//            price.setType(OrderDetailType.CREDITS);
//            prices.add(price);
//            priceRepo.save(prices);
//        }
    }

    private void setUserRole(User user, Roles role, ApprovalStatus status) {
        UserRolePK pk = new UserRolePK(user, roleRepo.findByName(role.getValue()));
        UserRoles userRoles = new UserRoles();
        userRoles.setId(pk);
        if (role.equals(Roles.USER.getValue()) || role.equals(Roles.COUPLE.getValue())) {
            userRoles.setApprovalStatus(ApprovalStatus.APPROVED);
        } else {
            userRoles.setApprovalStatus(status);
        }
        userRoles.setCreated(new Date());
        //user.getRoles().add(userRoles);
        userRolesRepo.save(userRoles);

    }


    @Transactional
    public void setGalleryNullvaluesToFalse() {
        List<GalleryStorageFile> files = galleryStorageRepo.findAll();
        for (GalleryStorageFile ad : files) {

            try {
                ad.setAddress(addressUtil.extractAddress(ad.getAddress().getZipCode()));
            } catch (Exception ex) {

            }

            galleryStorageRepo.save(ad);

        }
    }

    @Transactional
    public void setUserActivated() {

        List<User> users = userRepo.findAll();
        for (User user : users) {

            user.setActivated(true);
            userRepo.save(user);
        }

    }
    @Transactional
    public void setUserRoleToCompletedProfiles() {

        List<User> users = userRepo.findAll();
        for (User user : users) {

            if (user.getUserProfile() != null && user.getUserProfile().getCompleted()) {
                boolean hasUserRole = false;
                for (UserRoles roles : user.getRoles()) {
                    if (roles.getId().getRole().getName().equals(Roles.USER.getValue())) {
                        hasUserRole = true;
                    }
                    if (roles.getId().getRole().getName().equals(Roles.COUPLE.getValue())) {
                        hasUserRole = true;
                    }
                }
                if (!hasUserRole) {
                    setUserRole(user, Roles.USER, ApprovalStatus.APPROVED);
                    //  userRepo.save(user);
                }
            }

        }
    }
    @Transactional
    public void setUserLocationNotNull() {

        List<User> users = userRepo.findAll();
        for (User user : users) {
            UserProfile userProfile = user.getUserProfile();
            if(userProfile == null) {
                userProfile = new UserProfile(user.getUserId());
                user.setUserProfile(userProfile);
            }
            UserPreference userPreference = user.getUserPreference();
            userProfile.setCompleted( userPreference!=null && hasCompletedGenderRegistration(userProfile, userPreference) && hasCompletedAgeheightsizelangRegistration(userProfile, userPreference)
            &&hasCompletedPersonalitylifestyleRegistration(userProfile)
            && hasCompletedPersonalitystatusRegistration(userProfile) && hasCompletedProfilepicsRegistration(user.getUserId(), userProfile));
            if(userProfile.getAddress() == null){
                userProfile.setAddress(new Address());

            } else{

                if(userProfile.getAddress().getLatitude() == null) {
                    userProfile.getAddress().setLatitude(0.0);
                } if(userProfile.getAddress().getLongitude() == null) {
                    userProfile.getAddress().setLongitude(0.0);
                }



            }
            if(userProfile.getAddress().getLatitude() != 0 &&userProfile.getAddress().getLongitude() != 0 )
                userProfile.setAddress(addressUtil.extractAddress(new LatLng(userProfile.getAddress().getLatitude(), userProfile.getAddress().getLongitude()), 300));

            userProfileRepo.save(userProfile);
            if(userPreference == null){
                user.setUserPreference(new UserPreference(user.getUserId()));
                userRepo.save(user);
            }
        }

    }


    private boolean hasCompletedGenderRegistration(UserProfile prof, UserPreference pref) {
        return prof.getGender() != null && pref.getGender() != null && prof.getBirthDate() != null && prof.getAddress() != null
                && prof.getAddress().getLongitude() != null && prof.getAddress().getLongitude() != null;
    }

    private boolean hasCompletedAgeheightsizelangRegistration(UserProfile prof, UserPreference pref) {
        return prof.getBirthDate() != null && pref.getHeight() != null && ( (pref.getGender() == Gender.MALE && pref.getPenisSizeMatter() != null) ||
                (pref.getGender() == Gender.FEMALE && pref.getBustSize() != null && pref.getButtSize() != null) ||
                (pref.getGender() == Gender.BOTH && pref.getBustSize() != null && pref.getButtSize() != null && pref.getPenisSizeMatter() != null)  )  ;
    }

    private boolean hasCompletedPersonalitylifestyleRegistration(UserProfile prof) {
        return prof.getPersonalities().size() > 0 && prof.getLifestyles().size() > 0;
        // no longer the status for the new layout && prof.getStatuses().size() > 0; // TODO: Do the checking using the actual sizes
    }



    private boolean hasCompletedPersonalitystatusRegistration(UserProfile prof) {
        return prof.getStatuses().size() > 0; // TODO: Do the checking using the actual sizes
    }

    private boolean hasCompletedProfilepicsRegistration(Integer userId, UserProfile prof) {
        return ((prof.getHandsCloudFile() != null &&prof.getHandsCloudFile().getUrl() != null )|| (prof.getHandsFileSkipped() != null && prof.getHandsFileSkipped()))
                && ((prof.getFeetCloudFile() != null &&prof.getFeetCloudFile().getUrl() != null) || (prof.getFeetFileSkipped() != null &&prof.getFeetFileSkipped()))
                && ((prof.getLegsCloudFile() != null && prof.getLegsCloudFile().getUrl() != null )|| (prof.getLegsFileSkipped() != null && prof.getLegsFileSkipped()));
    }
    @Transactional
    public void setPagesLanguages() {
        List<Page> pages = pageRepository.findAll();
        for (Page page : pages) {

            //set texts
            Set<TextWidget> texts = page.getTextContents();
            for (TextWidget textWidget : texts) {

                if (textWidget.getTranslations().size() == 1) {
                    for (Language lang : languageRepo.findAll()) {
                        if (lang.getAbbreviation().equals("EN"))
                            continue;

                        TextTranslation translation = new TextTranslation();
                        translation.setTitle("");
                        translation.setContent("");
                        translation.setCreated(new Date());
                        translation.setLanguage(lang.getAbbreviation());
                        translation.setTextWidget(textWidget);
                        textWidget.getTranslations().add(translation);
                    }
                }
            }
            //set metadata

            for (Language lang : languageRepo.findAll()) {
                PageMetaData pageMetaData = pageMetaDataRepository.findByPageAndLanguage(page, lang);
                if (pageMetaData == null) {
                    pageMetaData = new PageMetaData(page, lang, "");
                    pageMetaDataRepository.save(pageMetaData);
                }

            }
        }

        pageRepository.save(pages);

    }

    @Transactional
    public void setLovstampSamples() {
        Language language = languageRepo.findByAbbreviation("EN");
        List<LovstampSample> samples = lovstampSampleRepo.findAll();
        for (LovstampSample stamp : samples
                ) {

            if (stamp.getType() == null) {
                stamp.setType(SampleTypes.SAMPLE);
                lovstampSampleRepo.save(stamp);
            }
        }


    }

    @Transactional
    public void setDefaultLovstampSamples() {

        Language language = languageRepo.findByAbbreviation("EN");
        org.springframework.data.domain.Page<LovstampSample> maleLoveStampPage = lovstampSampleRepo.findByLanguageAndGenderAndType(
                language,
                Gender.MALE, SampleTypes.DEFAULT, new PageRequest(0, 1));

        if (maleLoveStampPage.getContent().size() == 0) {
            LovstampSample malelovstampSample = new LovstampSample();
            malelovstampSample.setType(SampleTypes.DEFAULT);
            malelovstampSample.setGender(Gender.MALE);
            malelovstampSample.setLanguage(language);
            malelovstampSample.setCreated(new Date());
            lovstampSampleRepo.save(malelovstampSample);
        }
        org.springframework.data.domain.Page<LovstampSample> femaleLoveStampPage = lovstampSampleRepo.findByLanguageAndGenderAndType(
                language,
                Gender.FEMALE, SampleTypes.DEFAULT, new PageRequest(0, 1));
        if (femaleLoveStampPage.getContent().size() == 0) {
            LovstampSample femalelovstampSample = new LovstampSample();
            femalelovstampSample.setType(SampleTypes.DEFAULT);
            femalelovstampSample.setGender(Gender.FEMALE);
            femalelovstampSample.setLanguage(language);
            femalelovstampSample.setCreated(new Date());
            lovstampSampleRepo.save(femalelovstampSample);
        }

    }


    @Transactional
    public void setUserNullprofileAccessToFalse() {
        List<UserProfile> users = userProfileRepo.findAll();
        for (UserProfile userProfile : users) {
            if (userProfile.getGender() != null)
                userProfile.setCompleted(true);
            else {
                userProfile.setCompleted(false);
            }
            userProfileRepo.save(userProfile);
        }
    }

    private void initializePlansDetails() {
        if (plansService.allPlans().size() > 0)
            return;

        plansService.savePlan(new PlanDetailsDto(null, "Lite", "",
                "#2a2a2a", null, true, 0, "Free"));
        plansService.savePlan(new PlanDetailsDto(null, "Prime", "Coming Soon",
                "#ec743b", null, true, 500, "Coming Soon"));

        List<PlansDto> plans = planRepo.findByIsValid(true);

        List<FeatureDetailsDto> features = null;
        for (int x = 0; x < plans.size(); x++) {
            features = new ArrayList<>();
            features.add(new FeatureDetailsDto(null, plansService.allPlans().get(x).getPlanID(),
                    "Buy and send music", null, null, true));
            features.add(new FeatureDetailsDto(null, plansService.allPlans().get(x).getPlanID(),
                    "Use the map to locate your potential mates", null, null, true));
            features.add(new FeatureDetailsDto(null, plansService.allPlans().get(x).getPlanID(),
                    "Use private messages", null, null, true));
            features.add(new FeatureDetailsDto(null, plansService.allPlans().get(x).getPlanID(),
                    "Sign up for events", null, null, true));
            features.add(new FeatureDetailsDto(null, plansService.allPlans().get(x).getPlanID(),
                    "Send gifts", null, null, true));
            features.add(new FeatureDetailsDto(null, plansService.allPlans().get(x).getPlanID(),
                    "Check the best dating places around", null, null, true));
            features.add(new FeatureDetailsDto(null, plansService.allPlans().get(x).getPlanID(),
                    "Become an editor in LovBlog", null, null, true));
            plansService.saveFeatures(features);
        }


    }

    private void createEventCategories() {
        createEventCategoryIfNotFound("Outdoors");
        createEventCategoryIfNotFound("Indoor Social");
        createEventCategoryIfNotFound("Dance");
        createEventCategoryIfNotFound("Exercise/Workout");
        createEventCategoryIfNotFound("Spiritual");
        createEventCategoryIfNotFound("Music");
        createEventCategoryIfNotFound("Food/Dining");
        createEventCategoryIfNotFound("Travel");
    }

    private void createFoodTypes() {
        createFoodTypeIfNotFound("American");
        createFoodTypeIfNotFound("Mediterranean");
        createFoodTypeIfNotFound("Latin American");
        createFoodTypeIfNotFound("Asian");
        createFoodTypeIfNotFound("Fusion");
        createFoodTypeIfNotFound("Mexican");
        createFoodTypeIfNotFound("European");

    }

    private void createAtomospheres() {
        createPlaceAtmosphereIfNotFound("Serves Alcohol");
        createPlaceAtmosphereIfNotFound("Marijuana Friendly");
        createPlaceAtmosphereIfNotFound("Smoking allowed");
        createPlaceAtmosphereIfNotFound("Ouit Ambiance");
        createPlaceAtmosphereIfNotFound("Live Music");
        createPlaceAtmosphereIfNotFound("Outdoors");
        createPlaceAtmosphereIfNotFound("Bar/Lounge");
        createPlaceAtmosphereIfNotFound("Dancing");
        createPlaceAtmosphereIfNotFound("Sporting");
        createPlaceAtmosphereIfNotFound("Games/Entertainment");
        createPlaceAtmosphereIfNotFound("Restaurant");
        createPlaceAtmosphereIfNotFound("Families");
        createPlaceAtmosphereIfNotFound("Edgy");
        createPlaceAtmosphereIfNotFound("Retro");
        createPlaceAtmosphereIfNotFound("Ethnic");
        createPlaceAtmosphereIfNotFound("Public Park");
        createPlaceAtmosphereIfNotFound("Theater/Cinema");
    }

    private void createDeals() {
        createDealsIfNotFound(0D, 25D, 0.05);
        createDealsIfNotFound(25D, 50D, 0.055);
        createDealsIfNotFound(50D, 75D, 0.060);
        createDealsIfNotFound(75D, 100D, 0.065);
        createDealsIfNotFound(100D, 150D, 0.070);
        createDealsIfNotFound(150D, 200D, 0.075);
        createDealsIfNotFound(200D, 250D, 0.080);
        createDealsIfNotFound(250D, 300D, 0.1);
        createDealsIfNotFound(300D, 350D, 0.105);
        createDealsIfNotFound(350D, 400D, 0.11);

        createDealsIfNotFound(400D, 450D, 0.115);
        createDealsIfNotFound(450D, 500D, 0.12);
        createDealsIfNotFound(500D, 600D, 0.125);
        createDealsIfNotFound(600D, 700D, 0.13);
        createDealsIfNotFound(700D, 800D, 0.135);
        createDealsIfNotFound(800D, 900D, 0.14);
        createDealsIfNotFound(900D, 1000D, 0.135);
        createDealsIfNotFound(1000D, 1250D, 0.13);
        createDealsIfNotFound(1250D, 1500D, 0.125);
        createDealsIfNotFound(1500D, 1750D, 0.12);

        createDealsIfNotFound(1750D, 2000D, 0.115);
        createDealsIfNotFound(2000D, 2500D, 0.11);
        createDealsIfNotFound(2500D, 3000D, 0.105);
        createDealsIfNotFound(3000D, 3000D, 0.10);
    }

    private void createPersonTypes() {
        createPersonTypeIfNotFound("Morning Person");
        createPersonTypeIfNotFound("Night Owl");
        createPersonTypeIfNotFound("Artsy");
        createPersonTypeIfNotFound("Music Lover");
        createPersonTypeIfNotFound("Extrovert");
        createPersonTypeIfNotFound("Introvert");
        createPersonTypeIfNotFound("Outdoorsy");
        createPersonTypeIfNotFound("Indoorsy");
        createPersonTypeIfNotFound("Easy-going");
        createPersonTypeIfNotFound("Sophisticated");
        createPersonTypeIfNotFound("Foodie");
        createPersonTypeIfNotFound("Sporty/Athletic");
    }

    @Transactional
    void initializeAlternativeHomepage() {
        Page alternativeHome = createPageIfNotFound("Lovappy | The New Dating", "alternative_home");

        Set<ImageWidget> homeImages = new HashSet<>();
        createImageIfNotFound("/home3/lovappy-radiobg.jpg", ImageType.BACKGROUND, PageConstants.ALT_HOMEPAGE_COVER_BG, homeImages);


        Set<TextWidget> homeText = new HashSet<>();
        createTextIfNotFound(PageConstants.ALT_HOMEPAGE_COVER_HEADER, "Lovappy Radio", homeText, "");
        createTextIfNotFound(PageConstants.ALT_HOMEPAGE_COVER_SUB_HEADER, "Communicate Using Only Your Voice.", homeText, "");
        createTextIfNotFound(PageConstants.ALT_HOMEPAGE_COVER_LOVRADIO_EXAMPLE, "HERE ARE SOME EXAMPLES OF WHAT YOU CAN FIND INSIDE LOVAPPY RADIO.", homeText, "");

        createTextIfNotFound(PageConstants.ALT_HOMEPAGE_LOVE_LISTENING_HEADER, "LOVE LISTENING", homeText, "");
        createTextIfNotFound(PageConstants.ALT_HOMEPAGE_LOVE_LISTENING_SUB_HEADER, "Speak Up", homeText, "");

        createTextIfNotFound(PageConstants.ALT_HOMEPAGE_LOVBLOG_TITLE, "LovBlog", homeText, "");
        createTextIfNotFound(PageConstants.ALT_HOMEPAGE_LOVBLOG_SUB_TITLE, "Share Your Story with Lovappy", homeText, "");
        createTextIfNotFound(PageConstants.ALT_HOMEPAGE_LOVBLOG_FEATURED_VLOG, "#", homeText, "");

        createTextIfNotFound(PageConstants.ALT_HOMEPAGE_LOVAPPY_CORPORATION, "The Lovappy Corporation Loves Love", homeText, "");
        createTextIfNotFound(PageConstants.ALT_HOMEPAGE_LOVAPPY_CORPORATION_DESCRIPTION, "We believe that the world is complex and full of distratctions. It is our goal to continually improve our technology to assist you in finding the one and only important part of life, Start with our free app and begin a good, old-fashioned courting process, We will not let you fall into the traps that are everywhere in today's world. With LovAppy Lite, your courting process forcecs you into looking deeper into potential mates. Not efficient enough for you? Consider our coming soon, American-style arranged marriage algorithm.", homeText, "");

        createTextIfNotFound(PageConstants.ALT_HOMEPAGE_LOVAPPY_APP_TITLE, "SIGN UP. Find Love. \n" +
                "Keep Dating", homeText, "");
        createTextIfNotFound(PageConstants.ALT_HOMEPAGE_LOVAPPY_APP_DESCRIPTION, "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", homeText, "");

        createTextIfNotFound(PageConstants.ALT_HOMEPAGE_LOVAPPY_APP_ANDROID_LINK, "https://play.google.com/store/apps/details?id=com.lfp.laligafantasy", homeText, "");
        createTextIfNotFound(PageConstants.ALT_HOMEPAGE_LOVAPPY_APP_APPLE_LINK, "https://itunes.apple.com/us/app/espn-live-sports-scores/id317469184?mt=8", homeText, "");


        boolean isNeedUpdate = false;
        if (homeText.size() > 0) {
            isNeedUpdate = true;
            alternativeHome.getTextContents().addAll(homeText);
        }
        if (homeImages.size() > 0) {
            isNeedUpdate = true;
            alternativeHome.getImages().addAll(homeImages);
        }
        if (isNeedUpdate) {
            pageRepository.save(alternativeHome);
        }

        createPageMetaIfNotFound(alternativeHome, "EN", alternativeHome.getName());
        createPageMetaIfNotFound(alternativeHome, "ES", alternativeHome.getName());


    }

    @Transactional
    void initializeVideoHomepage() {
        Page videoHome = createPageIfNotFound("Lovappy | The New Dating", "video_home");

        Set<ImageWidget> homeImages = new HashSet<>();
        createImageIfNotFound("/home/radiobg.jpg", ImageType.BACKGROUND, PageConstants.VIDEO_HOMEPAGE_LOVRADIO_BG, homeImages);

        Set<VideoWidget> videoWidgets = new HashSet<>();
        createVideoIfNotFound("https://storage.googleapis.com/lovappy-static/media/lovappy-video.webm", VideoProvider.OTHER,
                PageConstants.VIDEO_HOMEPAGE_VIDEO, videoWidgets, "");
        createVideoIfNotFound("https://www.youtube.com/embed/sSeHxiAGLhk", VideoProvider.YOUTUBE,
                PageConstants.VIDEO_HOMEPAGE_MOBILE_VIDEO, videoWidgets, "sSeHxiAGLhk");


        Set<TextWidget> homeText = new HashSet<>();
        createTextIfNotFound(PageConstants.VIDEO_HOMEPAGE_TITLE, "LOVAPPY", homeText, "");
        createTextIfNotFound(PageConstants.VIDEO_HOMEPAGE_LOVRADIO_TITLE, "Lovappy Radio", homeText, "");
        createTextIfNotFound(PageConstants.VIDEO_HOMEPAGE_LOVRADIO_SUB_TITLE, "Communicate Using Only Your Voice", homeText, "");
        createTextIfNotFound(PageConstants.VIDEO_HOMEPAGE_LOVRADIO_EXAMPLE, "HERE ARE SOME EXAMPLES OF WHAT YOU CAN FIND INSIDE LOVAPPY RADIO", homeText, "");


        boolean isNeedUpdate = false;
        if (homeText.size() > 0) {
            isNeedUpdate = true;
            videoHome.getTextContents().addAll(homeText);
        }
        if (homeImages.size() > 0) {
            isNeedUpdate = true;
            videoHome.getImages().addAll(homeImages);
        }
        if (videoWidgets.size() > 0) {
            isNeedUpdate = true;
            videoHome.getVideos().addAll(videoWidgets);
        }
        if (isNeedUpdate) {
            pageRepository.save(videoHome);
        }
        createPageMetaIfNotFound(videoHome, "EN", videoHome.getName());
        createPageMetaIfNotFound(videoHome, "ES", videoHome.getName());


    }


    @Transactional
    void initializeHomePage() {
        Page homePage = createPageIfNotFound("Home", "home");
        Set<ImageWidget> homeImages = new HashSet<>();
        createImageIfNotFound("/images/home-banner.png", ImageType.BANNER, "home_banner1", homeImages);
        createImageIfNotFound("/images/home-banner2.png", ImageType.BANNER, "home_banner2", homeImages);
        createImageIfNotFound("/images/home-banner3.png", ImageType.BANNER, "home_banner3", homeImages);
        createImageIfNotFound("/images/home-banner4.png", ImageType.BANNER, "home_banner4", homeImages);

        createImageIfNotFound("/images/lips.png", ImageType.BACKGROUND, PageConstants.HOMEPAGE_FEATURED_FIRST_BG, homeImages);
        createImageIfNotFound("/images/wed_pic.png", ImageType.BACKGROUND, PageConstants.HOMEPAGE_FEATURED_SECOND_BG, homeImages);
        createImageIfNotFound("/images/rings.png", ImageType.BACKGROUND, PageConstants.HOMEPAGE_FEATURED_THIRD_BG, homeImages);
        createImageIfNotFound("/images/heart.png", ImageType.BACKGROUND, PageConstants.HOMEPAGE_FEATURED_FOURTH_BG, homeImages);
        createImageIfNotFound("/images/lips.png", ImageType.BACKGROUND, PageConstants.HOMEPAGE_FEATURED_FIFTH_BG, homeImages);
        createImageIfNotFound("/images/stand.png", ImageType.BACKGROUND, PageConstants.HOMEPAGE_FEATURED_SIXTH_BG, homeImages);

        Set<TextWidget> homeTexts = new HashSet<>();
        createTextIfNotFound(PageConstants.HOMEPAGE_HEADER_H1, "dating without distractions", homeTexts, "");
        createTextIfNotFound(PageConstants.HOMEPAGE_HEADER_H5, "We believe that a voice is for life...lovappy it.", homeTexts, "");
        createTextIfNotFound(PageConstants.HOMEPAGE_ABOUT_TEXT_H1, "The LovAppy Corporation loves love.", homeTexts, "");
        createTextIfNotFound(PageConstants.HOMEPAGE_ABOUT_TEXT_P, "We believe that the world is complex and full of distractions. It is our goal to continually improve our technology to assist you in finding the one and only important part of life. Start with our free app and begin a good , old-fashioned courting process. We will not let you fall into the traps that are everywhere in today’s world. With LovAppy Lite, your courting forces you into looking deeper into potential mates. Not efficient enough for you? Consider our coming soon, American-style arranged marriage algorithm.", homeTexts, "");
        createTextIfNotFound(PageConstants.HOMEPAGE_FEATURED_FIRST_BLOG, "LovBlog", homeTexts, "");


        boolean isNeedUpdate = false;
        if (homeTexts.size() > 0) {
            isNeedUpdate = true;
            homePage.getTextContents().addAll(homeTexts);
        }
        if (homeImages.size() > 0) {
            isNeedUpdate = true;
            homePage.getImages().addAll(homeImages);
        }
        if (isNeedUpdate) {
            pageRepository.save(homePage);
        }
        createPageMetaIfNotFound(homePage, "EN", homePage.getName());
        createPageMetaIfNotFound(homePage, "ES", homePage.getName());
    }

    @Transactional
    void initializeMusicLandingPages() {
        Page music = createPageIfNotFound("Music", "music");
        Page musicLanding = createPageIfNotFound("Music Landing", "music_landing");
        Page musicianLanding = createPageIfNotFound("Music For Musician Landing", "musician_landing");
        Page musicUsersLanding = createPageIfNotFound("Music For Users Landing", "music_users_landing");

        Set<ImageWidget> musicImages = new HashSet<>();
        Set<ImageWidget> musicianImages = new HashSet<>();
        Set<ImageWidget> musicUsersImages = new HashSet<>();
        createImageIfNotFound("/landing-pages/music-landing-A.jpg", ImageType.BACKGROUND, PageConstants.MUSIC_LANDING_PAGE_IMAGE_A, musicImages);
        createImageIfNotFound("/landing-pages/music-landing-B.jpg", ImageType.BACKGROUND, PageConstants.MUSIC_LANDING_PAGE_IMAGE_B, musicImages);
        createImageIfNotFound("/landing-pages/music-musician.jpg", ImageType.BACKGROUND, PageConstants.MUSICIAN_LANDING_PAGE_IMAGE, musicianImages);
        createImageIfNotFound("/landing-pages/music-user.jpg", ImageType.BACKGROUND, PageConstants.MUSIC_USER_LANDING_PAGE_IMAGE, musicUsersImages);

        Set<TextWidget> musicPageText = new HashSet<>();
        Set<TextWidget> musicText = new HashSet<>();
        Set<TextWidget> musicianText = new HashSet<>();
        Set<TextWidget> musicUsersText = new HashSet<>();

        createTextIfNotFound(PageConstants.MUSIC_PAGE_TITLE, "MUSIC IS LOVE PICK A SONG", musicPageText, "");
        createTextIfNotFound(PageConstants.MUSIC_PAGE_SUB_TITLE_A, "Send It Or Publish Your Own Music For Sale", musicPageText, "");
        createTextIfNotFound(PageConstants.MUSIC_PAGE_SUB_TITLE_B, "Just For $1.99 - You get a copy, they get a copy", musicPageText, "");

        createTextIfNotFound(PageConstants.MUSIC_LANDING_PAGE_TITLE, "SELL, DOWNLOAD AND SEND MUSIC ON LOVAPPY", musicText, "");
        createTextIfNotFound(PageConstants.MUSIC_LANDING_PAGE_SUB_TITLE_A, "A MUSICIAN? SELL YOUR MUSIC HERE", musicText, "");
        createTextIfNotFound(PageConstants.MUSIC_LANDING_PAGE_SUB_TITLE_B, "LOVE MUSIC? SEND & DOWNLOAD MUSIC HERE", musicText, "");

        createTextIfNotFound(PageConstants.MUSICIAN_LANDING_PAGE_TITLE, "share your passion & make a profit.", musicianText, "");
        createTextIfNotFound(PageConstants.MUSICIAN_LANDING_PAGE_DESCRIPTION, "Earn royalties on every purchase made of your original song while helping others connect.", musicianText, "");

        createTextIfNotFound(PageConstants.MUSIC_USER_LANDING_PAGE_TITLE, "Buy and Send Songs on Lovappy Music.", musicUsersText, "");
        createTextIfNotFound(PageConstants.MUSIC_USER_LANDING_PAGE_DESCRIPTION, "Songs available from artists big and small, guaranteed you’ll find the one that says what you need to say", musicUsersText, "");


        boolean isMusicPageNeedUpdate = false;
        boolean isMusicNeedUpdate = false;
        boolean isMusicianNeedUpdate = false;
        boolean isMusicUserNeedUpdate = false;

        //Music
        if (musicPageText.size() > 0) {
            isMusicPageNeedUpdate = true;
            music.getTextContents().addAll(musicPageText);
        }
        if (isMusicPageNeedUpdate) {
            pageRepository.save(music);
        }

        //Music Landing
        if (musicText.size() > 0) {
            isMusicNeedUpdate = true;
            musicLanding.getTextContents().addAll(musicText);
        }
        if (musicImages.size() > 0) {
            isMusicNeedUpdate = true;
            musicLanding.getImages().addAll(musicImages);
        }
        if (isMusicNeedUpdate) {
            pageRepository.save(musicLanding);
        }
        //Musician Landing
        if (musicianText.size() > 0) {
            isMusicianNeedUpdate = true;
            musicianLanding.getTextContents().addAll(musicianText);
        }
        if (musicianImages.size() > 0) {
            isMusicianNeedUpdate = true;
            musicianLanding.getImages().addAll(musicianImages);
        }
        if (isMusicianNeedUpdate) {
            pageRepository.save(musicianLanding);
        }
        //Music User Landing
        if (musicUsersText.size() > 0) {
            isMusicUserNeedUpdate = true;
            musicUsersLanding.getTextContents().addAll(musicUsersText);
        }
        if (musicUsersImages.size() > 0) {
            isMusicUserNeedUpdate = true;
            musicUsersLanding.getImages().addAll(musicUsersImages);
        }
        if (isMusicUserNeedUpdate) {
            pageRepository.save(musicUsersLanding);
        }
        createPageMetaIfNotFound(music, "EN", "Lovappy | Browse and Share Music");
        createPageMetaIfNotFound(musicLanding, "EN", "Lovappy | Browse and Share Music");
        createPageMetaIfNotFound(musicianLanding, "EN", "Lovappy | Browse and Share Music");
        createPageMetaIfNotFound(musicUsersLanding, "EN", "Lovappy | Browse and Share Music");

        createPageMetaIfNotFound(music, "ES", "Lovappy | Browse and Share Music");
        createPageMetaIfNotFound(musicLanding, "ES", "Lovappy | Browse and Share Music");
        createPageMetaIfNotFound(musicianLanding, "ES", "Lovappy | Browse and Share Music");
        createPageMetaIfNotFound(musicUsersLanding, "ES", "Lovappy | Browse and Share Music");

    }

    @Transactional
    void initializeBlogLandingPage() {
        Page blogLanding = createPageIfNotFound("Blog Landing", "blog_landing");
        Set<ImageWidget> blogImages = new HashSet<>();
        createImageIfNotFound("/landing-pages/blog.png", ImageType.BACKGROUND, "blog_bg", blogImages);

        Set<TextWidget> blogText = new HashSet<>();
        createTextIfNotFound(PageConstants.BLOG_LANDING_PAGE_TITLE, "Share your love stories ", blogText, "");
        createTextIfNotFound(PageConstants.BLOG_LANDING_PAGE_DESCRIPTION, "Browse others stories, share them and write yours. With LovBlog, you are in the right place for love articles.", blogText, "");

        boolean isNeedUpdate = false;
        if (blogText.size() > 0) {
            isNeedUpdate = true;
            blogLanding.getTextContents().addAll(blogText);
        }
        if (blogImages.size() > 0) {
            isNeedUpdate = true;
            blogLanding.getImages().addAll(blogImages);
        }
        if (isNeedUpdate) {
            pageRepository.save(blogLanding);
        }
        createPageMetaIfNotFound(blogLanding, "EN", "LovBlog | Blog with LovBlog");
        createPageMetaIfNotFound(blogLanding, "ES", "LovBlog | Blog with LovBlog");
    }

    @Transactional
    void initializeGiftsLandingPage() {
        Page giftLanding = createPageIfNotFound("Gift Landing", "gift_landing");
        Set<ImageWidget> giftImages = new HashSet<>();
        createImageIfNotFound("/landing-pages/gift.png", ImageType.BACKGROUND, "gift_bg", giftImages);

        Set<TextWidget> giftText = new HashSet<>();
        createTextIfNotFound(PageConstants.GIFTS_LANDING_PAGE_TITLE, "EXPRESS LOVE WITH GIFTS ", giftText, "");
        createTextIfNotFound(PageConstants.GIFTS_LANDING_PAGE_DESCRIPTION, "Gifts free space.", giftText, "");

        boolean isNeedUpdate = false;
        if (giftText.size() > 0) {
            isNeedUpdate = true;
            giftLanding.getTextContents().addAll(giftText);
        }
        if (giftImages.size() > 0) {
            isNeedUpdate = true;
            giftLanding.getImages().addAll(giftImages);
        }
        if (isNeedUpdate) {
            pageRepository.save(giftLanding);
        }
        createPageMetaIfNotFound(giftLanding, "EN", "Lovappy | LovGifts");
        createPageMetaIfNotFound(giftLanding, "ES", "Lovappy | LovGifts");
    }

    @Transactional
    void initializeEventsLandingPage() {
        Page eventLanding = createPageIfNotFound("Events Landing", "event_landing");
        Set<ImageWidget> eventsImages = new HashSet<>();
        createImageIfNotFound("/images/landing-pages/events.png", ImageType.BACKGROUND, PageConstants.EVENTS_LANDING_PAGE_IMAGE, eventsImages);

        Set<TextWidget> eventsText = new HashSet<>();
        createTextIfNotFound(PageConstants.EVENTS_LANDING_PAGE_TITLE, "CREATE/ATTEND EVENTS.", eventsText, "");
        createTextIfNotFound(PageConstants.EVENTS_LANDING_PAGE_DESCRIPTION, "LovEvents let ambassadors to coordinate and plan events with the ability to specify the preferred audience. Sign up for events that interest you.", eventsText, "");

        TextWidget textWidget = widgetRepository.findByTextName(PageConstants.GIFTS_LANDING_PAGE_DESCRIPTION);
        eventLanding.getTextContents().remove(textWidget);
        pageRepository.save(eventLanding);
        boolean isNeedUpdate = false;
        if (eventsText.size() > 0) {
            isNeedUpdate = true;
            eventLanding.getTextContents().addAll(eventsText);
        }
        if (eventsImages.size() > 0) {
            isNeedUpdate = true;
            eventLanding.getImages().addAll(eventsImages);
        }
        if (isNeedUpdate) {
            pageRepository.save(eventLanding);
        }
        createPageMetaIfNotFound(eventLanding, "EN", "Lovappy | Events");
        createPageMetaIfNotFound(eventLanding, "ES", "Lovappy | Events");
    }


    @Transactional
    void initializeLoginPage() {
        Page loginPage = createPageIfNotFound("Login", "login");
        Set<ImageWidget> loginImages = new HashSet<>();
        createImageIfNotFound("/images/new-login-bg.png", ImageType.BACKGROUND, PageConstants.MAIN_LOGIN_BG, loginImages);
        if (loginImages.size() > 0) {
            loginPage.getImages().addAll(loginImages);
            pageRepository.save(loginPage);
        }
        createPageMetaIfNotFound(loginPage, "EN", loginPage.getName());
        createPageMetaIfNotFound(loginPage, "ES", loginPage.getName());
    }

    @Transactional
    void initializeSubmitBlogPage() {
        Page submitBlog = createPageIfNotFound("Submit Blog", "submit-blog");
        Set<ImageWidget> submitBlogImages = new HashSet<>();
        createImageIfNotFound("/images/blog/blog-submitbg.png", ImageType.BACKGROUND, PageConstants.SUBMIT_BLOG_COVER, submitBlogImages);
        if (submitBlogImages.size() > 0) {
            submitBlog.getImages().addAll(submitBlogImages);
            pageRepository.save(submitBlog);
        }
        createPageMetaIfNotFound(submitBlog, "EN", "LovBlog | Blog with LovBlog");
        createPageMetaIfNotFound(submitBlog, "ES", "LovBlog | Blog with LovBlog");
    }

    @Transactional
    void initializeEventsListPage() {
        Page eventHeader = createPageIfNotFound("Event Header", "event-header");
        Set<ImageWidget> eventHeaderImages = new HashSet<>();
        createImageIfNotFound("/images/events-header.jpg", ImageType.BACKGROUND, PageConstants.EVENT_HEADER_COVER, eventHeaderImages);

        Set<TextWidget> ambassadorText = new HashSet<>();
        createTextIfNotFound(PageConstants.EVENT_AMBASSADOR_INFO, "An Ambassador for LovAppy must be a special person, someone we trust.\n" +
                "\n" +
                "Represent our company and believe in our technology.\n" +
                "\n" +
                "Hopeful for our future and want to see people connect.\n" +
                "\n" +
                "Ambassador duties include but are not limited to:\n" +
                "Organizing and/or attending marketing events\n" +
                "\n" +
                "Becoming an Ambassador is the beginning of an interview for a paid employment job with LovAppy.*\n" +
                "\n" +
                "*In some cases we compensate Ambassadors for event.", ambassadorText, "");

        boolean isNeedUpdate = false;
        if (ambassadorText.size() > 0) {
            isNeedUpdate = true;
            eventHeader.getTextContents().addAll(ambassadorText);
        }
        if (eventHeaderImages.size() > 0) {
            eventHeader.getImages().addAll(eventHeaderImages);
        }
        if (isNeedUpdate) {
            pageRepository.save(eventHeader);
        }
        createPageMetaIfNotFound(eventHeader, "EN", "Lovappy | Events'");
        createPageMetaIfNotFound(eventHeader, "ES", "Lovappy | Events'");

    }

    @Transactional
    void initializeForgotPasswordPage() {
        Page forgotPasswordPage = createPageIfNotFound("Forgot Password", "forgot-password");
        Set<TextWidget> forgotPasswordTextWidgetSet = new HashSet<>();

        createTextIfNotFound(PageConstants.FORGOT_PASSWORD_DESCRIPTION,
                "Enter your email address below. <br/> <br/> You should receive an email shortly that includes a password reset link.",
                forgotPasswordTextWidgetSet, "");

        if (forgotPasswordTextWidgetSet.size() > 0) {
            forgotPasswordPage.getTextContents().addAll(forgotPasswordTextWidgetSet);
            pageRepository.save(forgotPasswordPage);
        }
        createPageMetaIfNotFound(forgotPasswordPage, "EN", forgotPasswordPage.getName());
        createPageMetaIfNotFound(forgotPasswordPage, "ES", forgotPasswordPage.getName());

    }

    @Transactional
    void initializePrimePage() {
        Page primePage = createPageIfNotFound("Prime", "prime");

        Set<TextWidget> primeTexts = new HashSet<>();
        createTextIfNotFound(PageConstants.PRIMEPAGE_PARAGRAPH_H5, "We believe that the world is complex and full of distractions. It is our goal to continually improve" +
                "our technology to assist you in finding the one and only important part of life. Start with our free" +
                "app and begin a good , old-fashioned courting process. We will not let you fall into the traps" +
                "that are everywhere in today’s world. With LovAppy Lite, your courting forces you into looking" +
                "deeper into potential mates. Not efficient enough for you? Consider our coming soon," +
                "American-style arranged marriage algorithm.", primeTexts, "");
        Set<ImageWidget> primeImages = new HashSet<>();
        createImageIfNotFound("/images/banner-2.png", ImageType.BANNER, "prime-banner1", primeImages);

        boolean isNeedUpdate = false;
        if (primeTexts.size() > 0) {
            isNeedUpdate = true;
            primePage.getTextContents().addAll(primeTexts);
        }
        if (primeImages.size() > 0) {
            isNeedUpdate = true;
            primePage.getImages().addAll(primeImages);
        }
        if (isNeedUpdate) {
            pageRepository.save(primePage);
        }
        createPageMetaIfNotFound(primePage, "EN", "Lovappy");
        createPageMetaIfNotFound(primePage, "ES", "Lovappy");
    }

    @Transactional
    void initializeFaqPage() {
        Page faqPage = createPageIfNotFound("Faq", "faq");
        pageRepository.save(faqPage);
        createPageMetaIfNotFound(faqPage, "EN", "Lovappy");
        createPageMetaIfNotFound(faqPage, "ES", "Lovappy");
    }

    @Transactional
    void initializeAdsTermPage() {
        Page adsTermsPage = createPageIfNotFound("Ads Terms", "ads-terms");
        pageRepository.save(adsTermsPage);
        createPageMetaIfNotFound(adsTermsPage, "EN", "Lovappy");
        createPageMetaIfNotFound(adsTermsPage, "ES", "Lovappy");
    }

    @Transactional
    void initializeMusicianTermPage() {
        Page adsTermsPage = createPageIfNotFound("Musician Terms", "musician-terms");
        pageRepository.save(adsTermsPage);
        createPageMetaIfNotFound(adsTermsPage, "EN", "Lovappy");
        createPageMetaIfNotFound(adsTermsPage, "ES", "Lovappy");
    }

    @Transactional
    void initializeVendorTermPage() {
        Page adsTermsPage = createPageIfNotFound("Vendor Terms", "vendor-terms");
        pageRepository.save(adsTermsPage);
        createPageMetaIfNotFound(adsTermsPage, "EN", "Lovappy");
        createPageMetaIfNotFound(adsTermsPage, "ES", "Lovappy");
    }

    @Transactional
    void initializeMusicUpload() {
        Page adsTermsPage = createPageIfNotFound("Music Upload", "music-uploads");
        pageRepository.save(adsTermsPage);


        Set<TextWidget> textWidgets = new HashSet<>();

        createTextIfNotFound("Parameters_1", "Parameter 1", textWidgets, "");
        createTextIfNotFound("Parameters_2", "Parameter 2", textWidgets, "");
        createTextIfNotFound("Parameters_3", "Parameter 3", textWidgets, "");

        if (textWidgets.size() > 0) {
            adsTermsPage.getTextContents().addAll(textWidgets);
            pageRepository.save(adsTermsPage);
        }
        createPageMetaIfNotFound(adsTermsPage, "EN", "Lovappy");
        createPageMetaIfNotFound(adsTermsPage, "ES", "Lovappy");
    }

    @Transactional
    void initializeMusicianPage() {
        Page page = createPageIfNotFound("Musician Registration", "musician_registration");
        pageRepository.save(page);
        Set<TextWidget> textWidgets = new HashSet<>();

        createTextIfNotFound("RoyaltyMSG", "*Earn 30% royalty on each song download Payout are made on a monthly basis via paypal.", textWidgets, "");
        createTextIfNotFound("Terms_1", "Terms 1", textWidgets, "");
        createTextIfNotFound("Terms_2", "Terms 2", textWidgets, "");
        createTextIfNotFound("Terms_2", "Terms 3", textWidgets, "");

        if (textWidgets.size() > 0) {
            page.getTextContents().addAll(textWidgets);
            pageRepository.save(page);
        }
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");
    }

    @Transactional
    void initializeDevTermPage() {
        Page adsTermsPage = createPageIfNotFound("Developer Terms", "dev-terms");
        pageRepository.save(adsTermsPage);
        createPageMetaIfNotFound(adsTermsPage, "EN", "Lovappy");
        createPageMetaIfNotFound(adsTermsPage, "ES", "Lovappy");
    }

    @Transactional
    void initializePodcastTermPage() {
        Page podcastTermsPage = createPageIfNotFound("Podcast Terms", "podcast-terms");
        pageRepository.save(podcastTermsPage);
        createPageMetaIfNotFound(podcastTermsPage, "EN", "Lovappy");
        createPageMetaIfNotFound(podcastTermsPage, "ES", "Lovappy");
    }

    @Transactional
    void initializeMusicTermPage() {
        Page musicTermsPage = createPageIfNotFound("Music Terms", "music-terms");
        pageRepository.save(musicTermsPage);
        createPageMetaIfNotFound(musicTermsPage, "EN", "Lovappy");
        createPageMetaIfNotFound(musicTermsPage, "ES", "Lovappy");
    }

    @Transactional
    void initializeComedyTermPage() {
        Page comedyTermsPage = createPageIfNotFound("Comedy Terms", "comedy-terms");
        pageRepository.save(comedyTermsPage);
        createPageMetaIfNotFound(comedyTermsPage, "EN", "Lovappy");
        createPageMetaIfNotFound(comedyTermsPage, "ES", "Lovappy");
    }

    @Transactional
    void initializeNewsTermPage() {
        Page newsTermsPage = createPageIfNotFound("News Terms", "news-terms");
        pageRepository.save(newsTermsPage);
        createPageMetaIfNotFound(newsTermsPage, "EN", "Lovappy");
        createPageMetaIfNotFound(newsTermsPage, "ES", "Lovappy");
    }


    @Transactional
    void initializeEventsTermPage() {
        Page newsTermsPage = createPageIfNotFound("Event Terms", "event-terms");
        pageRepository.save(newsTermsPage);
        createPageMetaIfNotFound(newsTermsPage, "EN", "Lovappy");
        createPageMetaIfNotFound(newsTermsPage, "ES", "Lovappy");
    }

    @Transactional
    void initializeAudioBookTermPage() {
        Page newsTermsPage = createPageIfNotFound("Audio Book Terms", "audiobook-terms");
        pageRepository.save(newsTermsPage);
        createPageMetaIfNotFound(newsTermsPage, "EN", newsTermsPage.getName());
        createPageMetaIfNotFound(newsTermsPage, "ES", newsTermsPage.getName());
    }

    @Transactional
    void initializeDatingPlacesPage() {
        Page newPage = createPageIfNotFound("Dating Places", "dating-places");
        pageRepository.save(newPage);
        createPageMetaIfNotFound(newPage, "EN", "Lovappy | Dating place");
        createPageMetaIfNotFound(newPage, "ES", "Lovappy | Dating place");
    }

    @Transactional
    void initializeHappyCouplePage() {
        Page newPage = createPageIfNotFound("Happy Couples", "happy-couples");
        pageRepository.save(newPage);
        createPageMetaIfNotFound(newPage, "EN", "Lovappy | Happy Couples");
        createPageMetaIfNotFound(newPage, "ES", "Lovappy | Happy Couples");
    }

    @Transactional
    void initializeLitePage() {
        Page litePage = createPageIfNotFound("Lite", "lite");

        Set<TextWidget> liteTexts = new HashSet<>();
        createTextIfNotFound(PageConstants.LITEPAGE_PARAGRAPH_H5, "We believe that the world is complex and full of distractions. It is our goal to continually improve" +
                "our technology to assist you in finding the one and only important part of life. Start with our free" +
                "app and begin a good , old-fashioned courting process. We will not let you fall into the traps" +
                "that are everywhere in today’s world. With LovAppy Lite, your courting forces you into looking" +
                "deeper into potential mates. Not efficient enough for you? Consider our coming soon," +
                "American-style arranged marriage algorithm.", liteTexts, "");
        Set<ImageWidget> liteImages = new HashSet<>();
        createImageIfNotFound("/images/banner.png", ImageType.BANNER, "lite-banner1", liteImages);

        boolean isNeedUpdate = false;
        if (liteTexts.size() > 0) {
            isNeedUpdate = true;
            litePage.getTextContents().addAll(liteTexts);
        }
        if (liteImages.size() > 0) {
            isNeedUpdate = true;
            litePage.getImages().addAll(liteImages);
        }
        if (isNeedUpdate) {
            pageRepository.save(litePage);
        }
        createPageMetaIfNotFound(litePage, "EN", "Lovappy");
        createPageMetaIfNotFound(litePage, "ES", "Lovappy");
    }

    @Transactional
    void initializeErrorsPage() {
        Page errorPage = createPageIfNotFound("Errors", "errors");
        Set<TextWidget> errorsText = new HashSet<>();
        createTextIfNotFound(PageConstants.SUSPENDED_ERROR,
                "You're account with Lovappy has been suspended. If you feel this is an error, please contact us at admin@lovappy.com", errorsText, "Suspended!");
        createTextIfNotFound("not_verified",
                "A verification email has been sent to you, please activate your account with the link attached.", errorsText, "\n" +
                        "Email Verification!");
        createTextIfNotFound(PageConstants.AMBASSADOR_APPROVAL_ERROR,
                "Your ambassador account has not been approved. Please check for an approval email.", errorsText, "\n" +
                        "Ambassador Approval");

        createTextIfNotFound(PageConstants.INVALID_PASSWORD_ERROR,
                "Please make sure you enter the correct email and password", errorsText, "\n" +
                        "Invalid email or password");


        if (errorsText.size() > 0) {
            errorPage.getTextContents().addAll(errorsText);
            pageRepository.save(errorPage);
        }
    }

    @Transactional
    void initializePlansAndPricingPage() {
        Page plansAndPricingPage = createPageIfNotFound("Plans & Pricing", "plans-pricing");
        Set<TextWidget> plansAndPricingTexts = new HashSet<>();
        createTextIfNotFound(PageConstants.PLAN_PRICE_COVER_TEXT,
                "PRICING OVERVIEW", plansAndPricingTexts, "");

        createTextIfNotFound(PageConstants.PLAN_PRICE_COVER_DESCRIPTION,
                "Free space to text Free space to text Free space to text Free space to text" +
                        "Free space to textFree space to text", plansAndPricingTexts, "");

        TextWidget textWidget = widgetRepository.findByTextName(PageConstants.PLAN_PRICE_RULES);
        plansAndPricingPage.getTextContents().remove(textWidget);

        Set<ImageWidget> plansAndPricingImages = new HashSet<>();
        createImageIfNotFound("/images/plans_pricing/bg.jpg", ImageType.BACKGROUND, PageConstants.PLAN_PRICE_COVER_IMAGE, plansAndPricingImages);

        boolean isNeedUpdate = false;
        if (plansAndPricingTexts.size() > 0) {
            isNeedUpdate = true;
            plansAndPricingPage.getTextContents().addAll(plansAndPricingTexts);
        }
        if (plansAndPricingImages.size() > 0) {
            isNeedUpdate = true;
            plansAndPricingPage.getImages().addAll(plansAndPricingImages);
        }
        if (isNeedUpdate) {
            pageRepository.save(plansAndPricingPage);
        }
        createPageMetaIfNotFound(plansAndPricingPage, "EN", "Lovappy | Plans & Pricing");
        createPageMetaIfNotFound(plansAndPricingPage, "ES", "Lovappy | Plans & Pricing");


    }

    @Transactional
    void initializeFeaturesPage() {
        Page featuresPage = createPageIfNotFound("Features", "features");
        Set<TextWidget> featuresTexts = new HashSet<>();
        createTextIfNotFound(PageConstants.FEATURES_RECORD_VOICE_DESCRIPTION, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco", featuresTexts, "");
        createTextIfNotFound(PageConstants.FEATURES_SEND_GIFT_DESCRIPTION, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco", featuresTexts, "");
        createTextIfNotFound(PageConstants.FEATURES_SEND_SONG_DESCRIPTION, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco", featuresTexts, "");
        if (featuresTexts.size() > 0) {
            featuresPage.getTextContents().addAll(featuresTexts);
            pageRepository.save(featuresPage);
        }
        createPageMetaIfNotFound(featuresPage, "EN", featuresPage.getName());
        createPageMetaIfNotFound(featuresPage, "ES", featuresPage.getName());
    }

    @Transactional
    void initializeMobilePage() {
        Page mobilePage = createPageIfNotFound("Mobile", "mobile");
        Set<TextWidget> mobileTexts = new HashSet<>();
        createTextIfNotFound(PageConstants.MOBILE_HEADER_TEXT, "Download our Mobile App", mobileTexts, "");
        createTextIfNotFound(PageConstants.MOBILE_DESCRIPTION_TEXT, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco", mobileTexts, "");
        Set<ImageWidget> mobileImages = new HashSet<>();
        createImageIfNotFound("/images/left-mobile-page.png", ImageType.BACKGROUND, PageConstants.LEFT_MOBILE_PAGE_IMAGE, mobileImages);

        boolean isNeedUpdate = false;
        if (mobileTexts.size() > 0) {
            isNeedUpdate = true;
            mobilePage.getTextContents().addAll(mobileTexts);
        }
        if (mobileImages.size() > 0) {
            isNeedUpdate = true;
            mobilePage.getImages().addAll(mobileImages);
        }
        if (isNeedUpdate) {
            pageRepository.save(mobilePage);
        }
        createPageMetaIfNotFound(mobilePage, "EN", "Lovappy Mobile Application");
        createPageMetaIfNotFound(mobilePage, "ES", "Lovappy Mobile Application");
    }

    @Transactional
    void initializeAboutUsPage() {
        Page aboutUsPage = createPageIfNotFound("About Us", "about-us");
        Set<TextWidget> aboutUsTexts = new HashSet<>();
        createTextIfNotFound(PageConstants.ABOUT_US_HEADER, "The LovAppy Corporation loves love.", aboutUsTexts, "");
        createTextIfNotFound(PageConstants.ABOUT_US_DESCRIPTION, "We believe that the world is complex and full of distratctions. It is our goal to continually improve our technology to assist you in finding the one and only important part of life, Start with our free app and begin a good, old-fashioned courting process, We will not let you fall into the traps that are everywhere in today's world. With LovAppy Lite, your courting process forcecs you into looking deeper into potential mates. Not efficient enough for you? Consider our coming soon, American-style arranged marriage algorithm.", aboutUsTexts, "");

        Set<ImageWidget> aboutUsImages = new HashSet<>();
        createImageIfNotFound("/images/about-header.png", ImageType.BACKGROUND, PageConstants.ABOUT_US_BG, aboutUsImages);
        boolean isNeedUpdate = false;
        if (aboutUsTexts.size() > 0) {
            isNeedUpdate = true;
            aboutUsPage.getTextContents().addAll(aboutUsTexts);
        }
        if (aboutUsImages.size() > 0) {
            isNeedUpdate = true;
            aboutUsPage.getImages().addAll(aboutUsImages);
        }
        if (isNeedUpdate) {
            pageRepository.save(aboutUsPage);
        }
        createPageMetaIfNotFound(aboutUsPage, "EN", "Lovappy | " + aboutUsPage.getName());
        createPageMetaIfNotFound(aboutUsPage, "ES", "Lovappy | " + aboutUsPage.getName());
    }

    @Transactional
    void initializeContactUsPage() {
        Page contactUsPage = createPageIfNotFound("Contact Us", "contact-us");
        Set<TextWidget> contactUsTexts = new HashSet<>();

        createTextIfNotFound(PageConstants.CONTACT_US_ADDRESS, "1510 Corlies Avenue Neptune, NJ 07753", contactUsTexts, "");
        createTextIfNotFound(PageConstants.CONTACT_US_NUMBER, "(800) 574-7802", contactUsTexts, "");
        createTextIfNotFound(PageConstants.CONTACT_US_EMAIL, "info@lovappy.com", contactUsTexts, "");
        if (contactUsTexts.size() > 0) {
            contactUsPage.getTextContents().addAll(contactUsTexts);
            pageRepository.save(contactUsPage);
        }
        createPageMetaIfNotFound(contactUsPage, "EN", "Lovappy | " + contactUsPage.getName());
        createPageMetaIfNotFound(contactUsPage, "ES", "Lovappy | " + contactUsPage.getName());


    }

    @Transactional
    void initializeFeaturesRecordVoicePage() {
        Page featuresRecordVoicePage = createPageIfNotFound("Features Record Voice", "features-record-voice");
        Set<VideoWidget> videoWidgets = new HashSet<>();

        createVideoIfNotFound("https://www.youtube.com/embed/5qm8PH4xAss", VideoProvider.YOUTUBE, "record-voice-video", videoWidgets, "5qm8PH4xAss");
        if (videoWidgets.size() > 0) {
            featuresRecordVoicePage.getVideos().addAll(videoWidgets);
            pageRepository.save(featuresRecordVoicePage);
        }


        createPageMetaIfNotFound(featuresRecordVoicePage, "EN", "Lovappy | " + featuresRecordVoicePage.getName());
        createPageMetaIfNotFound(featuresRecordVoicePage, "ES", "Lovappy | " + featuresRecordVoicePage.getName());
    }

    @Transactional
    void initializeUploadMusic() {
        Page page = createPageIfNotFound("Upload Music", "upload-music");
        createPageMetaIfNotFound(page, "EN", "Lovappy | " + page.getName());
        createPageMetaIfNotFound(page, "ES", "Lovappy | " + page.getName());
    }

    @Transactional
    void initializeGiftMusicSurvey() {
        Page page = createPageIfNotFound("Gift/Music Survey", "gift-music-survey");
        createPageMetaIfNotFound(page, "EN", "Lovappy | " + page.getName());
        createPageMetaIfNotFound(page, "ES", "Lovappy | " + page.getName());
    }

    @Transactional
    void initializeHowThisWorksPage() {
        Page page = createPageIfNotFound("How This Works", "how-this-works");
        createPageMetaIfNotFound(page, "EN", "Lovappy | " + page.getName());
        createPageMetaIfNotFound(page, "ES", "Lovappy | " + page.getName());
    }

    @Transactional
    void initializeOurAdvertisersPage() {
        Page page = createPageIfNotFound("Our Advertisers", "our-advertisers");
        createPageMetaIfNotFound(page, "EN", page.getName());
        createPageMetaIfNotFound(page, "ES", page.getName());
    }

    @Transactional
    void initializeAdvertiseWithUsPage() {
        Page page = createPageIfNotFound("Advertise With Us", PageConstants.ADVERTISE_WITH_US);

        if (page.getSettings().size() == 0) {
            Map<String, String> settings = new HashMap<>();
            settings.put("text_limit", "120");
            settings.put("audio_limit", "120");
            settings.put("video_limit", "120");

            page.setSettings(settings);
            pageRepository.save(page);
        }

        Set<TextWidget> pageTexts = new HashSet<>();
        createTextIfNotFound(PageConstants.ADS_TEXT_STEP1, "TEXT PRICE, GUIDELINES, CHAR LIMIT 120", pageTexts, "");
        createTextIfNotFound(PageConstants.ADS_AUDIO_STEP1, "AUDIO PRICE, GUIDELINES, TIME LIMITS, UPLOAD OR RECORD, ADD TEXT , CHAR LIMIT 120", pageTexts, "");
        createTextIfNotFound(PageConstants.ADS_VIDEO_STEP1, "VIDEO PRICE, GUIDELINES, TIME LIMITs, UPLOAD, RECORD, OR URL", pageTexts, "");
        if (pageTexts.size() > 0) {
            page.getTextContents().addAll(pageTexts);
            pageRepository.save(page);
        }
        createPageMetaIfNotFound(page, "EN", "Lovappy | " + page.getName());
        createPageMetaIfNotFound(page, "ES", "Lovappy | " + page.getName());
    }

    @Transactional
    void initializeAllPodCastsPage() {
        Page page = createPageIfNotFound("All PodCasts", "all-podcasts");
        createPageMetaIfNotFound(page, "EN", "Lovappy | " + page.getName());
        createPageMetaIfNotFound(page, "ES", "Lovappy | " + page.getName());
    }

    @Transactional
    void initializeAllRadioPage() {
        Page page = createPageIfNotFound("Lovappy Radio", "radio");
        createPageMetaIfNotFound(page, "EN", "Lovappy | " + page.getName());
        createPageMetaIfNotFound(page, "ES", "Lovappy | " + page.getName());
    }

    @Transactional
    void createRoleIfNotFound(String name) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role();
            role.setName(name);
            roleRepository.save(role);
        }
    }

    @Transactional
    void updateRoleIfFound(String name, String newName) {
        Role role = roleRepository.findByName(name);
        if (role != null) {
            role.setName(newName);
            roleRepository.save(role);
        }
    }

    @Transactional
    void createLanguageIfNotFound(String name, String abbreviation) {
        Language language = languageRepo.findByNameAndAbbreviation(name, abbreviation);
        if (language == null) {
            language = new Language();
            language.setName(name);
            language.setAbbreviation(abbreviation);
            language.setEnabled(true);
            languageRepo.save(language);
        }
    }

    @Transactional
    Page createPageIfNotFound(String pageName, String pageTag) {

        Page page = pageRepository.findByTag(pageTag);
        if (page == null) {
            page = new Page();
            page.setName(pageName);
            page.setTag(pageTag);
            page = pageRepository.save(page);
        }
        return page;
    }

    @Transactional
    void createImageIfNotFound(String url, ImageType type, String tag, Set<ImageWidget> imageWidgetSet) {
        ImageWidget imageWidget = widgetRepository.findByImageName(tag);
        if (imageWidget == null) {
            imageWidget = new ImageWidget();
            imageWidget.setType(type.name().toLowerCase());
            imageWidget.setUrl(url);
            imageWidget.setName(tag);
            imageWidgetSet.add(widgetRepository.save(imageWidget));
        }
    }

    @Transactional
    void createVideoIfNotFound(String url, VideoProvider provider, String tag, Set<VideoWidget> videoWidgetSet, String providerId) {
        VideoWidget videoWidget = widgetRepository.findByVideoName(tag);
        if (videoWidget == null) {
            videoWidget = new VideoWidget();
            videoWidget.setProvider(provider);
            videoWidget.setUrl(url);
            videoWidget.setProviderId(providerId);
            videoWidget.setName(tag);
            videoWidgetSet.add(widgetRepository.save(videoWidget));
        }
    }

    @Transactional
    void createTextIfNotFound(String tag, String content, Set<TextWidget> textWidgetSet, String title) {
        TextWidget textWidget = widgetRepository.findByTextName(tag);
        if (textWidget == null) {
            textWidget = new TextWidget();
            textWidget.setName(tag);

            TextTranslation textTranslation = new TextTranslation();
            textTranslation.setContent(content);
            textTranslation.setLanguage("EN");
            textTranslation.setTitle(title);
            textTranslation.setTextWidget(textWidget);

            textWidget.getTranslations().add(textTranslation);
            textWidget.setName(tag);
            textWidgetSet.add(widgetRepository.save(textWidget));
        }
    }

    @Transactional
    void createPageMetaIfNotFound(Page page, String languageStr, String title) {
        Language language = languageRepo.findByAbbreviation(languageStr);
        PageMetaData pageMetaData = pageMetaDataRepository.findByPageAndLanguage(page, language);
        if (pageMetaData == null) {
            pageMetaData = new PageMetaData(page, language, title);
            pageMetaDataRepository.saveAndFlush(pageMetaData);
        }
    }


    @Transactional
    void createLovappyEmails() {
        String[] emails = {
                "cs@lovappy.com",
                "blogging@lovappy.com", "reset@lovappy.com", "sharing@lovappy.com",
                "ambassadors@lovappy.com", "noreply@lovappy.com", "music@lovappy.com",
                "events@lovappy.com", "complaints@lovappy.com", "urgent@lovappy.com",
                "mark@lovappy.com", "help@lovappy.com"
        };

        for (String email : emails) {
            LovappyEmail lovappyEmail = lovappyEmailRepo.findByEmail(email);
            if (lovappyEmail == null) {
                lovappyEmail = new LovappyEmail();
                lovappyEmail.setCreated(new Date());
                lovappyEmail.setEmail(email);

                lovappyEmailRepo.save(lovappyEmail);
            }
        }


    }

    private void createEmailTemplates() {
        // emailTemplateService.deleteAll();
        //createOrUpdate account verfication email template
        createEmailTemplate(EmailTypes.VERFICATION,
                "Author Verification",
                "LovAppy Author Account Verification",
                "Please confirm your email.  Thank you for joining LovAppy. To access your account and enjoy the full benefits just click the button below.",
                "EN",
                "blogging@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        createEmailTemplate(EmailTypes.VERFICATION,
                "Account Verification",
                "LovAppy Account Verification",
                "Please confirm your email.  Thank you for joining LovAppy. To access your account and enjoy the full benefits just click the button below.",
                "EN",
                "cs@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        createEmailTemplate(EmailTypes.DEACTIVATION,
                "Account Deactivation",
                "LovAppy Account Deactivation",
                "",
                "EN",
                "cs@lovappy.com",
                "Lovappy",
                "email/deactivation_account_email");

        //createOrUpdate reset password email template
        createEmailTemplate(
                EmailTypes.RESET_PASSWORD,
                "Reset Password",
                "LovAppy Password Reset",
                "A request to reset the password for your LovAppy account was recently submitted. Use the link below to reset your password.",
                "EN",
                "reset@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        //createOrUpdate password confirmation email template
        createEmailTemplate(
                EmailTypes.CONFIRMATION_PASSWORD,
                "New Password Confirmation",
                "LovAppy Password Confirmation",
                "You are receiving this notification because the password on your LovAppy account has recently been changed. If you did not initiate this change, please contact member support at cs@lovappy.com.",
                "EN",
                "reset@lovappy.com",
                "Lovappy",
                "email/default_email");

        //createOrUpdate LOvStamp Share email template
        createEmailTemplate(
                EmailTypes.EMAIL_SHARE,
                "LovStamp Share",
                "Someone shared a LovStamp with you",
                "Check out this LovStamp on LovAppy that someone shared with you.",
                "EN",
                "sharing@lovappy.com",
                "Lovappy",
                "email/default_email_share");

        //createOrUpdate Blog Share email template
        createEmailTemplate(
                EmailTypes.EMAIL_SHARE,
                "Blog Share",
                "Check out this Blog on LovAppy",
                "Check out this Blog on LovAppy that someone shared with you.",
                "EN",
                "sharing@lovappy.com",
                "Lovappy",
                "email/default_email_share");

        //createOrUpdate Event Share email template
        createEmailTemplate(
                EmailTypes.EMAIL_SHARE,
                "Event Share",
                "Check out this Event on LovAppy",
                "Check out this Event on LovAppy that someone shared with you.",
                "EN",
                "sharing@lovappy.com",
                "Lovappy",
                "email/default_email_share");
        //createOrUpdate blog INVITATION email template
        createEmailTemplate(
                EmailTypes.EMAIL_INVITATION,
                "Blog Invitation",
                "Come Blog on LovAppy",
                "Register on LovAppy for an Author account and share your stories with the LovAppy community.",
                "EN",
                "blogging@lovappy.com",
                "Lovappy",
                "email/blog_invitation");

        //createOrUpdate Ambassador Approval email template
        createEmailTemplate(
                EmailTypes.EMAIL_APPROVAL,
                "Ambassador Approval",
                "Ambassador Request Approved",
                "Your request to become a LovAppy Ambassador has been approved.  Click below to host your first event.",
                "EN",
                "ambassadors@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        //createOrUpdate Ambassador denial email template
        createEmailTemplate(
                EmailTypes.EMAIL_DENIAL,
                "Ambassador Denial",
                "Ambassador Request Denied",
                "Your request to become a LovAppy Ambassador has been denied.",
                "EN",
                "ambassadors@lovappy.com",
                "Lovappy",
                "email/default_email");

        //createOrUpdate Developer Approval email template
        createEmailTemplate(
                EmailTypes.EMAIL_APPROVAL,
                "Developer Approval",
                "Developer Request Approved",
                "Your request to become a LovAppy Developer has been approved. Use the following credentials to get API access.",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/approval_password");
        //createOrUpdate Developer denial email template
        createEmailTemplate(
                EmailTypes.EMAIL_DENIAL,
                "Developer Denial",
                "Developer Request Denied",
                "Your request to become a LovAppy Developer has been denied.",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/default_email");
        //createOrUpdate New Likes email template
        createEmailTemplate(
                EmailTypes.EMAIL_INTERACTION,
                "New Likes",
                "You have a new Like on Lovappy",
                "You have a new Like from someone on LovAppy! Click below to see who it is.",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        //createOrUpdate New Likes email template
        createEmailTemplate(
                EmailTypes.EMAIL_INTERACTION,
                "New Matches",
                "You have a new Match on Lovappy",
                "You have a new Match on LovAppy! Click below to see who it is.",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        //createOrUpdate Unlock email template
        createEmailTemplate(
                EmailTypes.EMAIL_INTERACTION,
                EmailTemplateConstants.PHOTO_UNLOCK,
                "You got a chance to see User Photo",
                "Your photo has been unlocked to user #NUMBER.  Click below to see who it is.",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        //createOrUpdate New Likes email template
        createEmailTemplate(
                EmailTypes.EMAIL_INTERACTION,
                "New Matches",
                "You have a new Match on Lovappy",
                "You have a new Match on LovAppy! Click below to see who it is.",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        //createOrUpdate New Messages email template
        createEmailTemplate(
                EmailTypes.EMAIL_INTERACTION,
                "New Messages",
                "Someone sent you a Private Message",
                "You have received a new private recorded message from User #NUMBER.  Click below to listen.",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");


        //createOrUpdate New Songs email template
        createEmailTemplate(
                EmailTypes.EMAIL_INTERACTION,
                "New Songs",
                "Someone sent you a Song",
                "You have received a new song from User #NUMBER.  Click below to listen and download.",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        //createOrUpdate Photo Unlock Requests email template
        createEmailTemplate(
                EmailTypes.EMAIL_ACCEPT_DENY,
                "Photo Unlock Requests",
                "Somone wants to see your Profile Photo",
                "You and User #NUMBER have communicated with each other a minimum of 6 times.  Now they are requesting to view your profile photo.  Click below to approve or deny their request.",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/email_accept_denial");

        //createOrUpdate Submitted Blog Approval email template
        createEmailTemplate(
                EmailTypes.EMAIL_SUBMITION,
                "Submitted Blog Approval",
                "Your blog has been approved",
                "Your blog submission \"TITLE\" has been approved.  Click below to view it live on LovBlog.",
                "EN",
                "blogging@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        //createOrUpdate Submitted Blog Denial email template
        createEmailTemplate(
                EmailTypes.EMAIL_SUBMITION,
                "Submitted Blog Denial",
                "Your blog has been denied",
                "Your blog submission \"TITLE\" has been denied.  Click below to edit it.",
                "EN",
                "blogging@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        //createOrUpdate Submitted ads Approval email template
        createEmailTemplate(
                EmailTypes.EMAIL_SUBMITION,
                "Submitted Ad Approval",
                "Your ad has been approved",
                "Your ad submission \"TITLE\" has been approved.",
                "EN",
                "ads@lovappy.com",
                "Lovappy",
                "email/default_email");

        //createOrUpdate Submitted ads Denial email template
        createEmailTemplate(
                EmailTypes.EMAIL_SUBMITION,
                "Submitted Ad Denial",
                "Your ad has been denied",
                "Your ad submission \"TITLE\" has been rejected.",
                "EN",
                "ads@lovappy.com",
                "Lovappy",
                "email/default_email");


        //createOrUpdate Submitted music Approval email template
        createEmailTemplate(
                EmailTypes.EMAIL_SUBMITION,
                "Submitted Music Approval",
                "Your music has been approved",
                "Your original song submission \"TITLE\" has been approved.  Click below to view it in the LovAppy Music Store.",
                "EN",
                "music@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        //createOrUpdate Submitted Music Denial email template
        createEmailTemplate(
                EmailTypes.EMAIL_SUBMITION,
                "Submitted Music Denial",
                "Your music has been denied",
                "Your original song submission \"TITLE\" has been denied.  Click below to edit it.",
                "EN",
                "music@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");


        //createOrUpdate Submitted event Approval email template
        createEmailTemplate(
                EmailTypes.EMAIL_SUBMITION,
                "Event Approved",
                "Your event has been approved",
                "Your event submission \"TITLE\" has been approved.  Click below to view it live on LovAppy.",
                "EN",
                "events@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        //createOrUpdate Submitted Music Denial email template
        createEmailTemplate(
                EmailTypes.EMAIL_SUBMITION,
                "Submitted Event Denial",
                "Your event has been denied",
                "Your event submission \"TITLE\" has been denied.  Click below to edit it.",
                "EN",
                "events@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        //createOrUpdate Ambassador Approval email template
        createEmailTemplate(
                EmailTypes.EMAIL_CANCELLATION,
                "Event Cancellation",
                "Event Cancelled",
                "We regret to inform you the event has been cancelled due to unavoidable issues.",
                "EN",
                "events@lovappy.com",
                "Lovappy",
                "email/cancel_email");

        //createOrUpdate Submitted Review Approval email template
        createEmailTemplate(
                EmailTypes.EMAIL_SUBMITION,
                "Submitted Review Approval",
                "Your Review has been Approved",
                "Your Review submission has been approved.  Click below to view it live on lovAppy.",
                "EN",
                "place@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        //createOrUpdate Submitted Review Denial email template
        createEmailTemplate(
                EmailTypes.EMAIL_DENIAL,
                "Submitted Review Denial",
                "Your Review has been Denied",
                "We regret to inform you that your review has been rejected due to our Community Standards.",
                "EN",
                "place@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        //createOrUpdate Dating place Approval email template
        createEmailTemplate(
                EmailTypes.EMAIL_SUBMITION,
                "Submitted Dating Place Approval",
                "Your Dating Place has been Approved",
                "Your Dating Place has been approved.  Click below to view it live on lovAppy.",
                "EN",
                "place@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        //createOrUpdate Dating Place Denial email template
        createEmailTemplate(
                EmailTypes.EMAIL_DENIAL,
                "Submitted Dating Place Denial",
                "Your Dating Place has been Rejected",
                "We regret to inform you that your Dating Place has been declined due to our Community Standards.",
                "EN",
                "place@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");

        //createOrUpdate payment confirmation email template
        createEmailTemplate(
                EmailTypes.DEFAULT_EMAIL,
                EmailTemplateConstants.PAYMENT_CONFIRMATION,
                "Payment Confirmation",
                "<table align=\"center\" width=\"700\" cellpadding=\"0\" cellspacing=\"0\">\n" +
                        "                            \n" +
                        "                            <tbody><tr>\n" +
                        "                                <td align=\"center\">\n" +
                        "                                    <h2 style=\"text-align:center\">Thanks for your payment</h2>\n" +
                        "                                </td>\n" +
                        "                            </tr>\n" +
                        "                            <tr>\n" +
                        "                                <td align=\"center\" style=\"color:black\">&nbsp;</td>\n" +
                        "                            </tr>\n" +
                        "                            <tr>\n" +
                        "                                <td><b>Transaction Id:</b>&nbsp;<span> ##ORDER_NUMBER## </span></td>\n" +
                        "                            </tr>\n" +
                        "                            <tr>\n" +
                        "                                <td><b>Payment Date:</b>&nbsp;<span> ##DATE## </span></td>\n" +
                        "                            </tr>\n" +
                        "                            <tr>\n" +
                        "                                <td><b>Amount:</b>&nbsp;<span> ##AMOUNT## </span></td>\n" +
                        "                            </tr>\n" +
                        "                            <tr>\n" +
                        "\n" +
                        "                                <td>&nbsp;</td>\n" +
                        "                            </tr>\n" +
                        "                            <tr>\n" +
                        "\n" +
                        "                                <td>Your order is confirmed and you're all set</td>\n" +
                        "                            </tr>\n" +
                        "                        </tbody></table>",
                "EN",
                "financial@lovappy.com",
                "Lovappy Financial",
                "email/default_email");

        createEmailTemplate(
                EmailTypes.EMAIL_APPROVAL,
                EmailTemplateConstants.COUPLE_APPROVAL,
                "Couples Approval Request ",
                "{{partnerEmail}} invite you to continue dating on Lovappy.  Click below to approve that!",
                "EN",
                "info@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");
        //Add photo missing mail
        createEmailTemplate(
                EmailTypes.DEFAULT_EMAIL,
                EmailTemplateConstants.PHOTO_MISSING,
                "Photo Missing",
                "You did not upload ##PHOTO## . Please upload one.",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/default_email");

        //Add photo rejected email
        createEmailTemplate(
                EmailTypes.DEFAULT_EMAIL,
                EmailTemplateConstants.PHOTO_REJECTED,
                "Photo Rejected",
                "Your ##PHOTO## has been rejected. Please upload a new one.",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/default_email");

        //Add photo rejected email
        createEmailTemplate(
                EmailTypes.DEFAULT_EMAIL,
                EmailTemplateConstants.VOICE_REJECTED,
                EmailTemplateConstants.VOICE_REJECTED,
                "Your ##VOICE## has been rejected. Please upload a new one.",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/default_email");
        //Add skip photo email
        createEmailTemplate(
                EmailTypes.DEFAULT_EMAIL,
                EmailTemplateConstants.PHOTO_SKIPPED,
                "Photo Skipped",
                "We know you skipped uploading a ##PHOTO##, but your profile would look nicer with more photos.",
                "EN",
                "cs@lovappy.com",
                "Lovappy",
                "email/default_email");

        //Gift section emails
        createEmailTemplate(
                EmailTypes.GIFT_SENT,
                EmailTemplateConstants.GIFT_SENT,
                "Gift Sent",
                "<h3 style=\"text-align: center;\">They’ll be notifies of their gift and can choose to pick it up.</h3>\n" +
                        " <p style=\"text-align: center; color: black;\">While we cannot guarantee that the user will redeem this gift (or respond to your gesture)," +
                        " <br /> we ask that you remain <b>hopeful</b>! </p>",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/gift/gift_sent");

        createEmailTemplate(
                EmailTypes.GIFT_RECEIVED,
                EmailTemplateConstants.GIFT_RECEIVED,
                "Gift Received",
                " <h3 style=\"text-align: center;\">Click below to see what it is and where you can pick it up!</h3>",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/gift/gift_received");

        createEmailTemplate(
                EmailTypes.GIFT_SOLD,
                EmailTemplateConstants.GIFT_SOLD,
                "Gift Sold",
                "*If the item is not redeemed within 7 days, no founds will be released  to you and the order will be considered cancelled. No follow-up email will be send.",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/gift/gift_brand");

        createEmailTemplate(
                EmailTypes.GIFT_REDEEM,
                EmailTemplateConstants.GIFT_REDEEM,
                "Gift Redeemed",
                "Congratulations, Lovappy has just received redeem confirmation for one of your items.",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/gift/gift_redeem");


        //createOrUpdate New Likes email template
        createEmailTemplate(
                EmailTypes.EMAIL_INTERACTION,
                EmailTemplateConstants.NEW_NOTIFICATION,
                "You have a new Action",
                "You have a new Notification on LovAppy! Click below to see who it is.",
                "EN",
                "noreply@lovappy.com",
                "Lovappy",
                "email/default_email_with_btn");
    }

    private void createEmailTemplate(EmailTypes type, String name, String subject, String body, String language,
                                     String emailFrom, String fromName, String templateUrl) {

        EmailTemplateDto emailTemplateDto = emailTemplateService.getByNameAndLanguage(name, language);
        if (emailTemplateDto == null) {
            emailTemplateDto = new EmailTemplateDto();
            emailTemplateDto.setBody(body);
            emailTemplateDto.setLanguage(language);
            emailTemplateDto.setName(name);
            emailTemplateDto.setSubject(subject);
            emailTemplateDto.setType(type);
            emailTemplateDto.setFromEmail(emailFrom);
            emailTemplateDto.setFromName(fromName);
            emailTemplateDto.setTemplateUrl(templateUrl);
            emailTemplateDto.setImageUrl("https://www.googleapis.com/download/storage/v1/b/lovappy-images-testing/o/1506544873524.jpg?generation=1506544878393320&alt=media");
            emailTemplateService.create(emailTemplateDto);
        } else {
            emailTemplateDto.setType(type);
            emailTemplateDto.setTemplateUrl(templateUrl);
            emailTemplateDto.setImageUrl("https://www.googleapis.com/download/storage/v1/b/lovappy-images-testing/o/1506544873524.jpg?generation=1506544878393320&alt=media");
            emailTemplateService.update(emailTemplateDto.getId(), emailTemplateDto);
        }
    }

    @Transactional
    public void createEventCategoryIfNotFound(String name) {
        EventCategory category = eventCategoryRepo.findByCategoryName(name);
        if (category == null) {
            category = new EventCategory();
            category.setCategoryName(name);
            category.setEnabled(true);

            eventCategoryRepo.save(category);
        }
    }

    @Transactional
    public void createFoodTypeIfNotFound(String name) {
        FoodType foodType = foodTypeRepo.findByFoodTypeName(name);
        if (foodType == null) {
            foodType = new FoodType();
            foodType.setFoodTypeName(name);
            foodType.setEnabled(true);
            foodTypeRepo.save(foodType);
        }
    }

    @Transactional
    public void createPlaceAtmosphereIfNotFound(String name) {
        PlaceAtmosphere placeAtmosphere = placeAtmosphereRepo.findByAtmosphereName(name);
        if (placeAtmosphere == null) {
            placeAtmosphere = new PlaceAtmosphere();
            placeAtmosphere.setAtmosphereName(name);
            placeAtmosphere.setEnabled(true);
            placeAtmosphereRepo.save(placeAtmosphere);
        }
    }

    @Transactional
    public void createDealsIfNotFound(Double startPrice, Double endPrice, Double feePercentage) {
        Deals dealsList = dealsRepository.findAllByStartPrice(startPrice);
        if (dealsList == null) {
            Deals deals = new Deals();
            deals.setStartPrice(startPrice);
            deals.setEndPrice(endPrice);
            deals.setFeePercentage(feePercentage);
            dealsRepository.save(deals);
        }
    }

    @Transactional
    public void createPersonTypeIfNotFound(String personTypeName) {
        PersonType personType = personTypeRepo.findByPersonTypeName(personTypeName);
        if (personType == null) {
            PersonType type = new PersonType();
            type.setPersonTypeName(personTypeName);
            type.setEnabled(true);
            personTypeRepo.saveAndFlush(type);
        }
    }


    @Transactional
    public void createRadioControlsMasterData() {
        createRadioMediaTypeIfNotFound("Audio File");
        createRadioMediaTypeIfNotFound("DJ File");
        createRadioMediaTypeIfNotFound("Other File");

        createRadioTypeIfNotFound("Ad");
        createRadioTypeIfNotFound("News");
        createRadioTypeIfNotFound("Podcast");
        createRadioTypeIfNotFound("Music");
        createRadioTypeIfNotFound("Audio Book");
        createRadioTypeIfNotFound("Comedy");
        createRadioTypeIfNotFound("Other");

        createRadioSubTypeIfNotFound("Local");
        createRadioSubTypeIfNotFound("World");
        createRadioSubTypeIfNotFound("Other");

    }

    private void createRadioMediaTypeIfNotFound(String name) {
        MediaType mediaType = mediaTypeService.findByName(name);
        if (mediaType == null) {
            mediaType = new MediaType(name, "");
            try {
                mediaTypeService.create(mediaType);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void createRadioTypeIfNotFound(String name) {
        AudioType audioType = audioTypeService.findByName(name);
        if (audioType == null) {
            audioType = new AudioType(name, "");
            try {
                audioTypeService.create(audioType);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void createRadioSubTypeIfNotFound(String name) {
        AudioSubType audioType = audioSubTypeService.findByName(name);
        if (audioType == null) {
            audioType = new AudioSubType(name, "");
            try {
                audioSubTypeService.create(audioType);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void updatePageName() {
        updateIfInvalidPageName("Video Home", "video_home");
        updateIfInvalidPageName("Alternative Home", "alternative_home");
        updateIfInvalidPageName("Musicians Landing", "musician_landing");
        updateIfInvalidPageName("Music Users Landing", "music_users_landing");
    }

    private void updateIfInvalidPageName(String pageName, String tagName) {
        Page page = pageRepository.findByTag(tagName);
        if (page != null) {
            if (!page.getName().trim().equalsIgnoreCase(pageName.trim())) {
                page.setName(pageName);
                pageRepository.save(page);
            }
        }
    }

    private void addMusicTemplate(MusicResponseType type, String template) {
        MusicResponseTemplate byType = musicResponseTemplateRepo.findTopByType(type);
        if (byType == null) {
            MusicResponseTemplate musicResponseTemplate = new MusicResponseTemplate();
            musicResponseTemplate.setType(type);
            musicResponseTemplate.setTemplate(template);
            musicResponseTemplateRepo.saveAndFlush(musicResponseTemplate);
        }
    }


    @Transactional
    public void createCountry() {
        String[] countryCodes = Locale.getISOCountries();

        for (String countryCode : countryCodes) {

            Locale obj = new Locale("", countryCode);
            Country country = countryService.findByCode(countryCode);

            if (country == null) {
                country = new Country();
                country.setCode(countryCode);
                country.setName(obj.getDisplayCountry());
                country.setActive(true);
                country.setCreated(new Date());

                try {
                    countryService.create(country);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }

    @Transactional
    public void addProductType() {
        createProductType("Gift Voucher");
        createProductType("Book");
        createProductType("Card");
        createProductType("Other");
    }

    private void createProductType(String name) {
        ProductTypeDto productType = new ProductTypeDto();
        if (productTypeService.findByName(name) == null) {
            productType.setTypeName(name);
            productType.setActive(true);
            try {
                productTypeService.create(productType);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Transactional
    void initializePageMetaData() {
        Page page = createPageIfNotFound("Info Graphics Gift", "infographics_gift");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Info Graphics Advertising", "infographics_advertising");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Info Graphics Dating", "infographics_dating");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Info Graphics Couples", "infographics_couples");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Info Graphics Music", "infographics_music");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Ambassador Landing", "ambassador_landing");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Icons", "icon-map");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Info Graphics Library", "infographics_library");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Dating Places Landing", "dating_places_landing");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Add Dating Places", "add_dating_places");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Sell your Music", "sell_your_music");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Musician help", "musician_help");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Find Love", "find_love");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Developer", "developer");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Developer Login", "developer_login");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Blog Help", "blog_help");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Video Blog", "vblog");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Blog Gallery", "blog_gallery");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Send Song", "f_send_song");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Send Gift", "f_send_gift");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Sitemap", "sitemap");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Patent", "patent");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Privacy", "privacy");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Privacy", "privacy");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Past Events", "past_events");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");


        page = createPageIfNotFound("Ambassador Registration", "ambassador_registration");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Brand/Retailer Registration", "brand_registration");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Author Registration", "author_registration");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Event Gallery", "event_gallery");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Terms", "terms");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");

        page = createPageIfNotFound("Careers", "careers");
        pageRepository.save(page);
        createPageMetaIfNotFound(page, "EN", "Lovappy");
        createPageMetaIfNotFound(page, "ES", "Lovappy");
    }

}