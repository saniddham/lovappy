package com.realroofers.lovappy.service.product.support;

/**
 * Created by Manoj on 01/02/2018.
 */
public enum ProductAvailability {
    IN_STOCK,OUT_OF_STOCK
}
