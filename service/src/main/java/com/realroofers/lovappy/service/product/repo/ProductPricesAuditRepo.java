package com.realroofers.lovappy.service.product.repo;

import com.realroofers.lovappy.service.product.model.ProductPricesAudit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductPricesAuditRepo extends JpaRepository<ProductPricesAudit, Integer> {
}
