package com.realroofers.lovappy.service.radio;

import com.realroofers.lovappy.service.radio.dto.RadioDto;
import com.realroofers.lovappy.service.radio.dto.RadioFilterDto;
import com.realroofers.lovappy.service.radio.dto.StartSoundDto;
import com.realroofers.lovappy.service.user.support.Gender;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

/**
 * Created by Daoud Shaheen on 12/17/2017.
 */
public interface RadioService {

    Page<RadioDto> getRadioItemsByGender(Gender gender, Integer page, Integer limit);

    Page<RadioDto> getAllRadioItems(Integer page, Integer limit);

    Page<RadioDto> getFilteredRadioItems(Integer userID, Boolean useFilter, RadioFilterDto filter, Integer page, Integer limit);

    Page<RadioDto> getHomePublicLovRadio(Pageable pageable);

    Page<RadioDto> getHomePrivateLovRadio(Pageable pageable);
}
