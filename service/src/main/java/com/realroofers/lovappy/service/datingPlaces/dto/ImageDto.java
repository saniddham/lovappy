package com.realroofers.lovappy.service.datingPlaces.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * Created by Darrel Rayen on 3/8/18.
 */
@Data
@EqualsAndHashCode
public class ImageDto implements Serializable{
    private String thumbnailUrl,name;
    private Integer size;

    public ImageDto(String thumbnailUrl, String name, Integer size) {
        this.thumbnailUrl = thumbnailUrl;
        this.name = name;
        this.size = size;
    }
}
