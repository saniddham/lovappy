package com.realroofers.lovappy.service.event.dto;

import com.realroofers.lovappy.service.event.model.EventUser;
import com.realroofers.lovappy.service.event.model.EventUserId;
import com.realroofers.lovappy.service.event.support.UserEventStatus;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Tejaswi Venupalli on 8/29/17
 */
@EqualsAndHashCode
public class EventUserDto {

    @NotNull
    private Integer eventId ;
    @NotNull
    private Integer userId;
    private UserEventStatus status;
    private Date registrationDate;
    private Date cancellationDate;
    private Boolean isPaymentMade;


    public EventUserDto(){

    }

    public EventUserDto(EventUser eventUser){
        this.eventId = eventUser.getId().getEvent().getEventId();
        this.userId = eventUser.getId().getUser().getUserId();
        this.status = eventUser.getStatus();
        this.registrationDate = eventUser.getRegistrationDate();
        this.cancellationDate = eventUser.getCancellationDate();
        this.isPaymentMade = eventUser.getPaymentMade();
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public UserEventStatus getStatus() {
        return status;
    }

    public void setStatus(UserEventStatus status) {
        this.status = status;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Date getCancellationDate() {
        return cancellationDate;
    }

    public void setCancellationDate(Date cancellationDate) {
        this.cancellationDate = cancellationDate;
    }

    public Boolean getPaymentMade() {
        return isPaymentMade;
    }

    public void setPaymentMade(Boolean paymentMade) {
        isPaymentMade = paymentMade;
    }
}
