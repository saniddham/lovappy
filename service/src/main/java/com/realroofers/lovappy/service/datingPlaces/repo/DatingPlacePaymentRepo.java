package com.realroofers.lovappy.service.datingPlaces.repo;

import com.realroofers.lovappy.service.datingPlaces.model.DatingPlacePayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Darrel Rayen on 10/19/17.
 */
@Repository
public interface DatingPlacePaymentRepo extends JpaRepository<DatingPlacePayment,Integer> {
}
