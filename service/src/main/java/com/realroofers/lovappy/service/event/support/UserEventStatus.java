package com.realroofers.lovappy.service.event.support;


/**
 * Created by Tejaswi Venupalli on 8/29/17
 */

public enum UserEventStatus {

    REGISTERED("Registered"), CANCELLED("Cancelled");

    private String status;

    UserEventStatus(String status){
        this.status = status;
    }
}
