package com.realroofers.lovappy.service.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Daoud Shaheen on 7/31/2017.
 */
public class CommonUtils {

    private static Map<String, String> countries = new HashMap<>();

    /**
     * Convert object to JSON bytes
     *
     * @param object object to be converted to json bytes
     * @return json bytes
     */
    public static String convertObjectToJsonString(Object object) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            return mapper.writeValueAsString(object);
        } catch (IOException e) {
            return "{}";
        }
    }

    /**
     * Convert  JSON bytes to object
     *

     */
    public static Object fromJSONString(final String jsonString, Class<?> domainClass) {
        try {
            return new ObjectMapper().readValue(jsonString, domainClass);
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Convert  JSON bytes to object
     *

     */
    public static Object fromJSONArrayString(final String jsonString, TypeReference domainClass) {
        try {
            return new ObjectMapper().readValue(jsonString, domainClass);
        } catch (IOException e) {
            return null;
        }
    }


    /**
     * get video id from its url
     *
     * @param videoUrl
     * @return
     */

    static final String youTubeUrlRegEx = "^(https?)?(://)?(www.)?(m.)?((youtube.com)|(youtu.be))/";
    static final String[] videoIdRegex = { "\\?vi?=([^&]*)","watch\\?.*v=([^&]*)", "(?:embed|vi?)/([^/?]*)", "^([A-Za-z0-9\\-]*)"};

    public static String findYoutubeVideoId(String url) {
        String youTubeLinkWithoutProtocolAndDomain = youTubeLinkWithoutProtocolAndDomain(url);

        for(String regex : videoIdRegex) {
            Pattern compiledPattern = Pattern.compile(regex);
            Matcher matcher = compiledPattern.matcher(youTubeLinkWithoutProtocolAndDomain);

            if(matcher.find()){
                return matcher.group(1);
            }
        }

        return null;
    }

    private static String youTubeLinkWithoutProtocolAndDomain(String url) {
        Pattern compiledPattern = Pattern.compile(youTubeUrlRegEx);
        Matcher matcher = compiledPattern.matcher(url);

        if(matcher.find()){
            return url.replace(matcher.group(), "");
        }
        return url;
    }

    public static void init() {
        for (String iso : Locale.getISOCountries()) {
            Locale l = new Locale("", iso);
            countries.put(l.getDisplayCountry(), iso);
        }
    }

    public static String getShortCountryName(String countryName) {
        init();
        return countries.get(countryName);
    }

}
