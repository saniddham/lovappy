package com.realroofers.lovappy.service.gift.dto;


import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class FilerObject {
    private String filterType, searchTerm;
}