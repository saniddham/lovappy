package com.realroofers.lovappy.service.cloud.model;

import com.realroofers.lovappy.service.cloud.support.ImageSize;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Eias Altawil
 */

@Entity
@Table(name="image_thumbnail")
@EqualsAndHashCode
public class ImageThumbnail  implements Serializable {
    @Id
    @GeneratedValue
    private Integer id;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    private CloudStorageFile file;

    @ManyToOne
    private CloudStorageFile originalFile;

    @Enumerated(EnumType.STRING)
    private ImageSize size;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CloudStorageFile getFile() {
        return file;
    }

    public void setFile(CloudStorageFile file) {
        this.file = file;
    }

    public CloudStorageFile getOriginalFile() {
        return originalFile;
    }

    public void setOriginalFile(CloudStorageFile originalFile) {
        this.originalFile = originalFile;
    }

    public ImageSize getSize() {
        return size;
    }

    public void setSize(ImageSize size) {
        this.size = size;
    }
}
