package com.realroofers.lovappy.service.product.dto;

import com.realroofers.lovappy.service.product.support.Upload;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode
public class ProductRegDto {

    private Integer id;
    @NotNull(message = "Product name cannot be empty.", groups = {Upload.class})
    private String productName;
    @NotNull(message = "Product code cannot be empty.", groups = {Upload.class})
    private String productCode;
    private String productDescription;
    @NotNull(message = "Product price cannot be empty.", groups = {Upload.class})
    private BigDecimal price;
    @NotNull(message = "Product category cannot be empty.", groups = {Upload.class})
    private Integer productType;
    private Integer brand;

    @NotNull(message = "Main image cannot be empty.", groups = {Upload.class})
    private Long image1;
    private String imageUrl1;
//    @NotNull(message = "Second image cannot be empty.", groups = {Upload.class})
    private Long image2;
    private String imageUrl2;
//    @NotNull(message = "Third image cannot be empty.", groups = {Upload.class})
    private Long image3;
    private String imageUrl3;
    @NotNull(message = "Product ean cannot be empty.", groups = {Upload.class})
    private String productEan;

    @Size(min = 1, message = "Location is required.", groups = {Upload.class})
    private List<Integer> vendorLocations = new ArrayList<>();

    private String vendorCode;

}
