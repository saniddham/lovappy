package com.realroofers.lovappy.service.couples.model;

import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.likes.model.LikePK;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.model.UserProfile;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.user.support.Gender;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by hasan on 9/10/2017.
 */
@Entity
@Table(name = "couple_profile")
@EqualsAndHashCode
@Data
@DynamicUpdate
public class CouplesProfile extends BaseEntity<Integer>{
    private Date anniversaryDate;
    private Date firstDate;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn
    private CloudStorageFile couplePhotoImage;

    private ApprovalStatus status;

    @OneToMany(
            mappedBy = "couplesProfile",
            cascade = CascadeType.PERSIST,
            orphanRemoval = true
    )
    private List<UserProfile> couples = new ArrayList<>();
}
