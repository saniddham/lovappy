package com.realroofers.lovappy.service.notification.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Daoud Shaheen on 6/24/2017.
 */
@Data
@Entity
@Table(name = "notification")
@EqualsAndHashCode
public class Notification  extends BaseEntity<Long>{

    private String content;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "id.notification")
    private Set<UserNotification> usersNotifications = new HashSet<>(0);

    @Transient
    private Boolean seen;
    @Transient
    private Date lastSeen;

    @Enumerated(EnumType.STRING)
    private NotificationTypes type;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd MMM, yyyy")
    @Transient
    private Date createdOn;

    @Column(name = "user_id")
    private Integer userId;

    private Long sourceId;

    public Notification() {
    }

    public Notification(Long id, String content, Date created, NotificationTypes type, Boolean seen, Date lastSeen) {
        this.content = content;
        this.seen = seen;
        this.lastSeen = lastSeen;
        this.type = type;
        setId(id);
        this.createdOn = created;
    }
}
