package com.realroofers.lovappy.service.cms.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.realroofers.lovappy.service.core.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Daoud Shaheen on 6/6/2017.
 */
@Entity
@Data
@Table(name = "pages")
@EqualsAndHashCode
public class Page extends BaseEntity<Integer> {

    private String name;
    @Column(name = "tag", unique = true)
    private String tag;

    @Column(name = "is_active", columnDefinition = "boolean default true", nullable = false)
    private Boolean active = true;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    @JoinTable(name = "pages_images", joinColumns = {@JoinColumn(name = "page_id",
            referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "image_id",
            referencedColumnName = "id")})
    @JsonManagedReference
    private Set<ImageWidget> images = new HashSet<>(0);

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    @JoinTable(name = "pages_texts", joinColumns = {@JoinColumn(name = "page_id",
            referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "text_id",
            referencedColumnName = "id")})
    @JsonManagedReference
    private Set<TextWidget> textContents = new HashSet<>(0);

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    @JoinTable(name = "pages_videos", joinColumns = {@JoinColumn(name = "page_id",
            referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "video_id",
            referencedColumnName = "id")})
    @JsonManagedReference
    private Set<VideoWidget> videos = new HashSet<>(0);


    @ElementCollection
    @MapKeyColumn(name="name")
    @Column(name="value")
    @CollectionTable(name="page_settings", joinColumns=@JoinColumn(name="page_id"))
    private Map<String, String> settings = new HashMap<>(0);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Page page = (Page) o;

        if (getId() != null ? !getId().equals(page.getId()) : page.getId() != null) return false;
        return (name != null ? !name.equals(page.name) : page.name != null);
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
