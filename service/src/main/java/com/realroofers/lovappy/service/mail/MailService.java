package com.realroofers.lovappy.service.mail;

import java.util.function.BiFunction;

import com.realroofers.lovappy.service.user.dto.AdminInvitationDto;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.PasswordResetRequest;
import com.realroofers.lovappy.service.user.model.User;


public interface MailService {
    void sendResetEmail(String email, String subject, BiFunction<User, PasswordResetRequest, String> contentGenerator);

    void sendMail(String recipient, String html, String subject);

    void sendMail(String recipient, String html, String subject, String fromEmail, String fromName);

    void sendInvitationEmail(String email, UserDto user, BiFunction<UserDto, AdminInvitationDto, String> contentGenerator);

//    void sendSendGridEmail(String recipient, String html, String subject, String fromEmail, String fromName);
}
