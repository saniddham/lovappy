package com.realroofers.lovappy.service.blog.support;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * 
 * @author mwiyono
 *
 */
public enum PostStatus {
	ACTIVE("active"), INACTIVE("inactive"), HIDDEN("pending"), DENIED("denied");
    String text;

    PostStatus(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
