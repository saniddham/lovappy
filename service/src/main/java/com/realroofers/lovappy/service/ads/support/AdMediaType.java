package com.realroofers.lovappy.service.ads.support;

public enum AdMediaType {
    TEXT, AUDIO, VIDEO
}
