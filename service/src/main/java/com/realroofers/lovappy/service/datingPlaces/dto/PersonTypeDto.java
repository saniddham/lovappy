package com.realroofers.lovappy.service.datingPlaces.dto;

import com.realroofers.lovappy.service.datingPlaces.model.PersonType;
import lombok.EqualsAndHashCode;

/**
 * Created by Darrel Rayen on 2/15/18.
 */
@EqualsAndHashCode
public class PersonTypeDto {

    private Integer id;
    private String personTypeName;
    private Boolean enabled;


    public PersonTypeDto() {
    }

    public PersonTypeDto(PersonType personType){
        this.id = personType.getId();
        this.personTypeName = personType.getPersonTypeName();
        this.enabled = personType.getEnabled();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPersonTypeName() {
        return personTypeName;
    }

    public void setPersonTypeName(String personTypeName) {
        this.personTypeName = personTypeName;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
