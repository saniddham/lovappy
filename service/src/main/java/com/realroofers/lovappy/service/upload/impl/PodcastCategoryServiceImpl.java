package com.realroofers.lovappy.service.upload.impl;

import com.realroofers.lovappy.service.upload.PodcastCategoryService;
import com.realroofers.lovappy.service.upload.dto.PodcastCategoryDto;
import com.realroofers.lovappy.service.upload.model.PodcastCategory;
import com.realroofers.lovappy.service.upload.repo.PodcastCategoryRepo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manoj
 */
@Service
public class PodcastCategoryServiceImpl implements PodcastCategoryService {

    private final PodcastCategoryRepo uploadCategoryRepo;

    public PodcastCategoryServiceImpl(PodcastCategoryRepo uploadCategoryRepo) {
        this.uploadCategoryRepo = uploadCategoryRepo;
    }

    @Override
    public Page<PodcastCategoryDto> findAll(Pageable pageable) {
        Page<PodcastCategoryDto> list = null;

        Page<PodcastCategory> all = uploadCategoryRepo.findAll(pageable);
        list = all.map(PodcastCategoryDto::new);
        return list;
    }

    @Override
    public List<PodcastCategoryDto> findAll() {
        List<PodcastCategoryDto> list = new ArrayList<>();
        uploadCategoryRepo.findAll().stream().forEach(uploadCategory -> list.add(new PodcastCategoryDto(uploadCategory)));
        return list;
    }

    @Override
    public List<PodcastCategoryDto> findAllActive() {
        List<PodcastCategoryDto> list = new ArrayList<>();
        uploadCategoryRepo.findAll().stream().forEach(uploadCategory -> list.add(new PodcastCategoryDto(uploadCategory)));
        return list;
    }

    @Override
    public PodcastCategoryDto create(PodcastCategoryDto uploadCategoryDto) throws Exception {
        return new PodcastCategoryDto(uploadCategoryRepo.saveAndFlush(new PodcastCategory(uploadCategoryDto)));
    }

    @Override
    public PodcastCategoryDto update(PodcastCategoryDto uploadCategoryDto) throws Exception {
        return new PodcastCategoryDto(uploadCategoryRepo.save(new PodcastCategory(uploadCategoryDto)));
    }

    @Override
    public void delete(Integer integer) throws Exception {

    }

    @Override
    public PodcastCategoryDto findById(Integer id) {
        PodcastCategory uploadCategory = uploadCategoryRepo.findOne(id);
        return uploadCategory == null ? null : new PodcastCategoryDto(uploadCategory);
    }
}
