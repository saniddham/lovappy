package com.realroofers.lovappy.service.gift.repo;

import com.realroofers.lovappy.service.gift.model.PurchasedGift;
import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Manoj on 02/02/2018.
 */
public interface PurchasedGiftRepo extends JpaRepository<PurchasedGift, Integer> {

    PurchasedGift findTopByUserOrderByPurchasedOnDesc(User user);

    PurchasedGift findTopByUserAndProductOrderByPurchasedOnDesc(@Param("user") User user, @Param("product") Product product);

    List<PurchasedGift> findAllByUserOrderByPurchasedOnDesc(User user);
}
