package com.realroofers.lovappy.service.radio.dto;

import com.realroofers.lovappy.service.ads.dto.AdDto;
import com.realroofers.lovappy.service.ads.support.AdMediaType;
import com.realroofers.lovappy.service.core.TextAlign;
import com.realroofers.lovappy.service.datingPlaces.dto.DatingPlaceDto;
import com.realroofers.lovappy.service.lovstamps.dto.LovstampDto;
import com.realroofers.lovappy.service.radio.support.AudioMediaType;
import com.realroofers.lovappy.service.radio.support.RadioType;
import com.realroofers.lovappy.service.tips.dto.LoveTipDto;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.UserUtils;
import com.realroofers.lovappy.service.util.AddressUtil;
import com.realroofers.lovappy.service.util.DateUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 12/17/2017.
 */
@Data
@ToString
@EqualsAndHashCode
public class RadioDto implements Serializable {

    private Integer id;
    private String fileUrl;
    private String coverImageUrl;
    private StartSoundDto startSound;
    private Integer recordSeconds = 0;
    private Integer userId;
    private Integer nextMemberId;
    private Integer prevMemberId;
    private Boolean attractive;
    private Boolean showHome;
    private AudioMediaType audioMediaType;
    private String backgroundColor;
    private RadioType type;
    private String language;
    private Gender gender;
    private Integer age;
    private boolean verified;
    private boolean updated;
    private boolean liked;
    private boolean blocked;
    private String address;
    private RadioExtraInfoDto extraInfo;

    public RadioDto() {
    }
    public RadioDto(LovstampDto lovstamp) {
        if(lovstamp != null) {
            this.id = lovstamp.getId();
            this.gender = lovstamp.getUser().getUserProfile().getGender();
            this.fileUrl = lovstamp.getAudioFileCloud() == null ? null : lovstamp.getAudioFileCloud().getUrl();
            this.recordSeconds = lovstamp.getRecordSeconds();
            this.userId = lovstamp.getUser().getID();
            this.language = lovstamp.getLanguage().getAbbreviation();
            this.attractive= "Y".equals(lovstamp.getUser().getUserProfile().getStatuses().get("attractmany"));
            this.showHome = lovstamp.getShowHome();
            if(lovstamp.getUser().getUserProfile().getAllowedProfilePic() != null && lovstamp.getUser().getUserProfile().getAllowedProfilePic()) {
                this.coverImageUrl = lovstamp.getUser().getUserProfile().getProfilePhotoCloudFile() == null ? "" : lovstamp.getUser().getUserProfile().getProfilePhotoCloudFile().getUrl();
            }
            this.audioMediaType = AudioMediaType.LOVSTAMP;
            this.verified = lovstamp.getUser().getEmailVerified() == null ? false : lovstamp.getUser().getEmailVerified();
            this.age = UserUtils.getAge(lovstamp.getUser().getUserProfile().getBirthDate());

            try {
                this.address = AddressUtil.getShortAddress(lovstamp.getUser().getUserProfile().getAddress().getStateShort() == null ?
                        lovstamp.getUser().getUserProfile().getAddress().getState() : lovstamp.getUser().getUserProfile().getAddress().getStateShort());

                if(this.address == null) {
                    this.address = AddressUtil.getShortAddress(lovstamp.getUser().getUserProfile().getAddress().getCountryCode() == null? lovstamp.getUser().getUserProfile().getAddress().getCountry() : lovstamp.getUser().getUserProfile().getAddress().getCountryCode());
                }
            } catch (Exception ex) {
                this.address = "";
            }
            this.updated = DateUtils.daysBetween(lovstamp.getCreatedOn(), new Date(), 7) ||
                    (lovstamp.getUpdatedOn() != null && DateUtils.daysBetween(lovstamp.getUpdatedOn(), new Date(), 7));
        }
    }
    public RadioDto(LovstampDto lovdstamp, StartSoundDto startSound) {
        this(lovdstamp);
        if(startSound != null) {
            this.startSound = startSound;
        }
    }

    public RadioDto(LovstampDto lovdstamp, StartSoundDto startSound, AdDto ad) {
     this(lovdstamp, startSound);
        setAd(ad);
    }


    //set featured ADs
    public RadioDto(AdDto ad, StartSoundDto startSound) {
        if(ad != null) {
            this.id = ad.getId();
            this.fileUrl = ad.getMediaFileCloud() == null ? null : ad.getMediaFileCloud().getUrl();
            setAd(ad);
            this.setType(ad.getMediaType().equals(AdMediaType.AUDIO) ? RadioType.FEATURED_AUDIO : RadioType.FEATURED_VIDEO);
            if(startSound != null) {
                this.startSound = startSound;
            }
        }
    }
    public void setAd(AdDto ad){
        if(ad.getFeatured()){
            this.coverImageUrl = ad.getFeaturedAdBackgroundFileCloudUrl();
        }else {
            this.coverImageUrl = ad.getBackgroundFileCloudUrl();
        }
        this.audioMediaType = AudioMediaType.valueOf(ad.getAdType().name());
       String mediaUrl = null;
       if(StringUtils.isEmpty(ad.getMediaFileUrl())){
           mediaUrl = ad.getMediaFileCloud() == null ? null : ad.getMediaFileCloud().getUrl();
       } else {
           mediaUrl = ad.getMediaFileUrl();
       }
        this.extraInfo = new RadioExtraInfoDto(ad.getMediaType(), ad.getUrl(), mediaUrl, ad.getText(), ad.getTextColor(), ad.getTextAlign());
        this.backgroundColor = ad.getBackgroundColor();
    }

    public void setLovTip(LoveTipDto tip, Integer index){
        this.extraInfo = new RadioExtraInfoDto(AdMediaType.TEXT,null, null, tip.getContent(), null, TextAlign.CENTER);
        this.backgroundColor = "#ff6666";
        this.extraInfo.setTitle("LOVAPPY TIP #" + index);
    }

    public void setDatingPlace(DatingPlaceDto datingPlace){
        this.extraInfo = new RadioExtraInfoDto(AdMediaType.TEXT, null, null, datingPlace.getPlaceName(), null, TextAlign.CENTER);
        this.coverImageUrl = datingPlace.getCoverPicture().getUrl();
    }

    public String getFormatedTime(){
        if(recordSeconds == null || recordSeconds == 0)
            return "00:00";
        int hours = (int)Math.floor(recordSeconds / 3600);
        int mins = (int)Math.floor(recordSeconds / 60 % 60);
        int secs = (int)Math.floor(recordSeconds % 60);
        if(hours>0) {
            return hours+":" + (mins>=9?""+mins : "0"+mins)
                    +":" + (secs>=9?""+secs : "0"+secs);
        } else if(mins > 0){
            return (mins>=9?""+mins : "0"+mins)
                    +":" + (secs>=9?""+secs : "0"+secs);
        }
        return "00:"+ (secs>=9?""+secs : "0"+secs);
    }

}
