package com.realroofers.lovappy.service.survey.support;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Created by hasan on 9/8/2017.
 */
public enum SurveyStatus {
    ANSWERED("ANSWERED"), SKIPPED("SKIPPED"), NOT_INTERESTED("NOT_INTERESTED");
    String text;

    SurveyStatus(String text) {
        this.text = text;
    }
}
