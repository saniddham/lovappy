package com.realroofers.lovappy.service.mail.dto;

import lombok.EqualsAndHashCode;

import java.io.Serializable;
/**
 * Created by Daoud Shaheen on 9/9/2017.
 */
@EqualsAndHashCode
public class EmailDto implements Serializable {
    private String email;
    private String fromEmail;
    private String subject;
    private String body;
    private String imageData;
    public EmailDto(String email, String subject, String body) {
        this.email = email;
        this.subject = subject;
        this.body = body;
    }

    public EmailDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getImageData() {
        return imageData;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }
}
