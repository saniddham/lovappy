package com.realroofers.lovappy.service.music.model;

import com.realroofers.lovappy.service.music.support.SendStatus;
import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Eias Altawil on 8/25/17
 */

@Entity
@EqualsAndHashCode
@DynamicUpdate
public class MusicExchange implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "music")
    private Music music;

    @ManyToOne
    @JoinColumn(name = "from_user")
    private User fromUser;

    @ManyToOne
    @JoinColumn(name = "to_user")
    private User toUser;

    private SendStatus sendStatus;

    private Date exchangeDate;

    @Column(name = "sender_mobile_downloads", nullable = false, columnDefinition = "int default 0")
    private int senderMobileDownloads;

    @Column(name = "sender_web_downloads", nullable = false, columnDefinition = "int default 0")
    private int senderWebDownloads;

    @Column(name = "receiver_mobile_downloads", nullable = false, columnDefinition = "int default 0")
    private int receiverMobileDownloads;

    @Column(name = "receiver_web_downloads", nullable = false, columnDefinition = "int default 0")
    private int receiverWebDownloads;

    @Type(type="text")
    private String lyrics;

    private Boolean userResponded;

    private Boolean seen;

    public MusicExchange() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Music getMusic() {
        return music;
    }

    public void setMusic(Music music) {
        this.music = music;
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    public Date getExchangeDate() {
        return exchangeDate;
    }

    public void setExchangeDate(Date exchangeDate) {
        this.exchangeDate = exchangeDate;
    }

    public SendStatus getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(SendStatus sendStatus) {
        this.sendStatus = sendStatus;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public Boolean getSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }

    public Boolean getUserResponded() {
        return userResponded;
    }

    public void setUserResponded(Boolean userResponded) {
        this.userResponded = userResponded;
    }

    public int getSenderMobileDownloads() {
        return senderMobileDownloads;
    }

    public void setSenderMobileDownloads(int senderMobileDownloads) {
        this.senderMobileDownloads = senderMobileDownloads;
    }

    public int getSenderWebDownloads() {
        return senderWebDownloads;
    }

    public void setSenderWebDownloads(int senderWebDownloads) {
        this.senderWebDownloads = senderWebDownloads;
    }

    public int getReceiverMobileDownloads() {
        return receiverMobileDownloads;
    }

    public void setReceiverMobileDownloads(int receiverMobileDownloads) {
        this.receiverMobileDownloads = receiverMobileDownloads;
    }

    public int getReceiverWebDownloads() {
        return receiverWebDownloads;
    }

    public void setReceiverWebDownloads(int receiverWebDownloads) {
        this.receiverWebDownloads = receiverWebDownloads;
    }
}
