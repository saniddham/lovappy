package com.realroofers.lovappy.service.ads;

import com.realroofers.lovappy.service.ads.dto.AdBackgroundColorDto;
import com.realroofers.lovappy.service.ads.dto.AdBackgroundImageDto;
import com.realroofers.lovappy.service.ads.dto.AdDto;
import com.realroofers.lovappy.service.ads.dto.AdPricingDto;
import com.realroofers.lovappy.service.ads.support.AdMediaType;
import com.realroofers.lovappy.service.ads.support.AdStatus;
import com.realroofers.lovappy.service.ads.support.AdType;
import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by Eias Altawil on 7/7/2017
 */
public interface AdsService {

    OrderDto addAd(AdDto ad);

    Page<AdDto> getAds(Integer userID, List<Integer> excludedAds, Pageable pageable);

    Page<AdDto> getAllAds(Integer page, Integer limit);

    Page<AdDto> findAll(Pageable pageable);

    AdDto getAd(Integer id);

    AdDto changeAdStatus(Integer id, AdStatus status);

    List<AdBackgroundColorDto> getAllBackgroundColors();

    List<AdBackgroundImageDto> getAllBackgroundImages();

    AdBackgroundColorDto addBackgroundColor(String color);

    AdBackgroundImageDto addBackgroundImage(CloudStorageFileDto file,CloudStorageFileDto file2);

    void deleteBackgroundColor(Integer id);

    void deleteBackgroundImage(Integer id);

    List<AdPricingDto> getAdPricing(AdType adType);

    AdPricingDto getAdPricingById(Integer id);

    AdPricingDto savePricing(AdPricingDto pricing);

    CalculatePriceDto calculatePrice(AdType adType, AdMediaType mediaType, Integer duration, String couponCode);

    Page<AdDto>  getFeaturedAdsByStatus(AdStatus status, Pageable pageable);

    Page<AdDto>  getRegularAdsByStatus(AdStatus status, Pageable pageable);

    Page<AdDto>  getRandomAds(Boolean featured, AdStatus status, Pageable pageable);
}
