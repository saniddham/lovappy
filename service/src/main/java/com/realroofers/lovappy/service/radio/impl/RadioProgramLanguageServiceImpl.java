package com.realroofers.lovappy.service.radio.impl;

import com.realroofers.lovappy.service.radio.RadioProgramLanguageService;
import com.realroofers.lovappy.service.radio.model.RadioProgramLanguage;
import com.realroofers.lovappy.service.radio.repo.RadioProgramLanguageRepo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Manoj on 17/12/2017.
 */
@Service
public class RadioProgramLanguageServiceImpl implements RadioProgramLanguageService {

    private final RadioProgramLanguageRepo radioProgramLanguageRepo;

    public RadioProgramLanguageServiceImpl(RadioProgramLanguageRepo radioProgramLanguageRepo) {
        this.radioProgramLanguageRepo = radioProgramLanguageRepo;
    }

    @Override
    public List<RadioProgramLanguage> findAll() {
        return radioProgramLanguageRepo.findAll();
    }

    @Override
    public List<RadioProgramLanguage> findAllActive() {
        return radioProgramLanguageRepo.findAllByActiveIsTrue();
    }

    @Override
    public RadioProgramLanguage create(RadioProgramLanguage radioProgramLanguage) {
        return radioProgramLanguageRepo.saveAndFlush(radioProgramLanguage);
    }

    @Override
    public RadioProgramLanguage update(RadioProgramLanguage radioProgramLanguage) {
        return radioProgramLanguageRepo.save(radioProgramLanguage);
    }

    @Override
    public void delete(Integer id) {
        radioProgramLanguageRepo.delete(id);
    }

    @Override
    public RadioProgramLanguage findById(Integer id) {
        return radioProgramLanguageRepo.findOne(id);
    }
}
