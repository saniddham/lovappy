package com.realroofers.lovappy.service.validator;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.GeocodingResult;
import com.realroofers.lovappy.service.validator.constraint.ZipCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.io.IOException;

/**
 * Created by Eias Altawil on 6/8/2017
 */
public class ZipCodeValidator implements ConstraintValidator<ZipCode, String> {

    private static Logger LOG = LoggerFactory.getLogger(ZipCodeValidator.class);

    @Value("${google.maps.api.key}")
    private String googleApiKey;

    @Override
    public void initialize(ZipCode zipCode) {}

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        GeoApiContext context = new GeoApiContext().setApiKey(googleApiKey);
        try {
            GeocodingResult[] results =  GeocodingApi.geocode(context, s).await();
            return results.length > 0;
            //results[0].addressComponents[0].types[0].compareTo(AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            return false;
        }
    }
}
