package com.realroofers.lovappy.service.lovstamps;

import com.realroofers.lovappy.service.lovstamps.dto.LovstampSampleDto;
import com.realroofers.lovappy.service.lovstamps.support.SampleTypes;
import com.realroofers.lovappy.service.user.support.Gender;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;

/**
 * Created by Daoud Shaheen on 12/31/2017.
 */
public interface LovstampSampleService {
    LovstampSampleDto saveRecordingSample(Integer id, Long audioFileCloudID, Integer languageID, Gender gender, SampleTypes sampleTypes);

    Page<LovstampSampleDto> getAllRecordingSamplesAndType(SampleTypes type, Pageable pageable);

    Page<LovstampSampleDto> getLovstampSamplesByLanguageAndGenderAndType(Integer languageID, Gender gender, SampleTypes type, org.springframework.data.domain.Pageable pageable);
    Page<LovstampSampleDto> getLovstampSamplesByLanguageAndGenderAndType(String languageCode, Gender gender, SampleTypes type, Pageable pageable);

    LovstampSampleDto getRecordingSample(Integer id);

    LovstampSampleDto saveDefaultSample(Integer duration, Long audioFileCloudID, Gender gender, String langAbbrev);
}