package com.realroofers.lovappy.service.cms.model;

/**
 * Created by Daoud Shaheen on 6/6/2017.
 */
public enum WidgetType {
    IMAGE, TEXT, VIDEO
}
