package com.realroofers.lovappy.service.music.model;

import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Daoud Shaheen on 9/23/2017.
 */
@Entity
@DynamicUpdate
@Table(name = "musics_users")
@AssociationOverrides({
        @AssociationOverride(name = "id.music",
                joinColumns = @JoinColumn(name = "music_id")),
        @AssociationOverride(name = "id.user",
                joinColumns = @JoinColumn(name = "user_id")) })
@EqualsAndHashCode
public class MusicUser implements Serializable {

    private MusicUserPK id;

    @CreationTimestamp
    @Column(name = "created", insertable = false, updatable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
    private Date created;

    @UpdateTimestamp
    private Date updated;

    @Column(name = "purchased", columnDefinition = "BIT(1) default 0")
    private Boolean purchased;

    @Column(name = "received", columnDefinition = "BIT(1) default 0")
    private Boolean received;

    @Column(name = "mobile_downloads", columnDefinition = "INT(11)")
    private Integer mobileDownloads;

    @Column(name = "web_downloads", columnDefinition = "INT(11)")
    private Integer webDownloads;

    @Column(name = "max_downloads", columnDefinition = "INT(11)")
    private Integer maxDownloads;

    private Date purchasedOn;

    private Integer rating;

    public MusicUser() {
        this.rating = -1;
        this.maxDownloads = 0;
        this.webDownloads = 0;
        this.mobileDownloads = 0;
        this.received = false;
        this.purchased = false;
    }

    @EmbeddedId
    public MusicUserPK getId() {
        return id;
    }

    public void setId(MusicUserPK id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Date getPurchasedOn() {
        return purchasedOn;
    }

    public void setPurchasedOn(Date purchasedOn) {
        this.purchasedOn = purchasedOn;
    }

    public Integer getMobileDownloads() {
        return mobileDownloads;
    }

    public void setMobileDownloads(Integer mobileDownloads) {
        this.mobileDownloads = mobileDownloads;
    }

    public Integer getWebDownloads() {
        return webDownloads;
    }

    public void setWebDownloads(Integer webDownloads) {
        this.webDownloads = webDownloads;
    }

    public Integer getMaxDownloads() {
        return maxDownloads;
    }

    public void setMaxDownloads(Integer maxDownloads) {
        this.maxDownloads = maxDownloads;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Boolean getPurchased() {
        return purchased;
    }

    public void setPurchased(Boolean purchased) {
        this.purchased = purchased;
    }

    public Boolean getReceived() {
        return received;
    }

    public void setReceived(Boolean received) {
        this.received = received;
    }
}
