package com.realroofers.lovappy.service.event.support;


/**
 * Created by Tejaswi Venupalli on 8/28/17
 */

public enum EventStatus {

    ACTIVE("Active"), CANCELLED("Cancelled"), INACTIVE("Inactive");

     private String status;

    EventStatus(String status){
        this.status = status;
    }

}
