package com.realroofers.lovappy.service.order.repo;

import com.realroofers.lovappy.service.order.model.Coupon;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;

public interface CouponRepo extends JpaRepository<Coupon, Integer> {
    Coupon findByCouponCodeAndCategoryAndExpiryDateAfter(String couponCode, CouponCategory category, Date today);
}
