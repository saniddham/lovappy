package com.realroofers.lovappy.service.order.support;

/**
 * Created by hasan on 15/11/2017.
 */
public enum RuleActionType {
    LOGIN("LOGIN"), BUY("BUY"), ADD_CONTENT("ADD_CONTENT");

    String text;

    RuleActionType(String text) {
        this.text = text;
    }
}
