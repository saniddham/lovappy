package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.util.DateUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode
public class ToUser implements Serializable {
        private Integer id;
        private String gender;
        private Integer age;
        private String state;
        private String language;
        private String userImage;
        public ToUser(User user) {
                this.id = user.getUserId();
                this.gender= user.getUserProfile().getGender().name();
                this.age = DateUtils.getAge(user.getUserProfile().getBirthDate());
                this.state =  user.getUserProfile().getAddress() != null? user.getUserProfile().getAddress().getStateShort():null;
                this.language = user.getUserProfile().getLanguage() != null ? user.getUserProfile().getLanguage().getAbbreviation(): null;
        }
        public ToUser() {
        }
}
