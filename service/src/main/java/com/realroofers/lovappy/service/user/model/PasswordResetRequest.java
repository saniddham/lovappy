package com.realroofers.lovappy.service.user.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Eias on 27-Apr-17
 */

@Entity
@Table(name="password_reset_request")
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode
public class PasswordResetRequest implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer ID;

    @Column(name = "user_id")
    private Integer userID;

    @Column(name = "token")
    private String token;

    @Column(name = "created_at")
    private Date createdAt;


    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
