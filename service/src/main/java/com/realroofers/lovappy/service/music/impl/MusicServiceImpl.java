package com.realroofers.lovappy.service.music.impl;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.repo.CloudStorageRepo;
import com.realroofers.lovappy.service.exception.ResourceNotFoundException;
import com.realroofers.lovappy.service.music.MusicService;
import com.realroofers.lovappy.service.music.dto.*;
import com.realroofers.lovappy.service.music.model.*;
import com.realroofers.lovappy.service.music.repo.GenreRepo;
import com.realroofers.lovappy.service.music.repo.MusicExchangeRepo;
import com.realroofers.lovappy.service.music.repo.MusicRepo;
import com.realroofers.lovappy.service.music.repo.MusicUserRepo;
import com.realroofers.lovappy.service.music.support.MusicDownload;
import com.realroofers.lovappy.service.music.support.MusicProvider;
import com.realroofers.lovappy.service.music.support.MusicStatus;
import com.realroofers.lovappy.service.music.support.SendStatus;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.CouponDto;
import com.realroofers.lovappy.service.order.dto.OrderDto;
import com.realroofers.lovappy.service.order.model.Price;
import com.realroofers.lovappy.service.order.repo.PriceRepo;
import com.realroofers.lovappy.service.order.support.OrderDetailType;
import com.realroofers.lovappy.service.order.support.PaymentMethodType;
import com.realroofers.lovappy.service.user.model.Role;
import com.realroofers.lovappy.service.user.model.Roles;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.repo.RoleRepo;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import com.realroofers.lovappy.service.user.support.Gender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by Eias Altawil on 8/25/17
 */

@Service("musicService")
public class MusicServiceImpl implements MusicService {

    private final MusicRepo musicRepo;
    private final GenreRepo genreRepo;
    private final UserRepo userRepo;
    private final CloudStorageRepo cloudStorageRepo;
    private final MusicExchangeRepo musicExchangeRepo;
    private final MusicUserRepo musicUserRepo;
    private final RoleRepo roleRepo;
    private final OrderService orderService;
    private final PriceRepo priceRepo;

    @Autowired
    public MusicServiceImpl(MusicRepo musicRepo, GenreRepo genreRepo, UserRepo userRepo,
                            CloudStorageRepo cloudStorageRepo, MusicExchangeRepo musicExchangeRepo, MusicUserRepo musicUserRepo, RoleRepo roleRepo, OrderService orderService, PriceRepo priceRepo) {
        this.musicRepo = musicRepo;
        this.genreRepo = genreRepo;
        this.userRepo = userRepo;
        this.cloudStorageRepo = cloudStorageRepo;
        this.musicExchangeRepo = musicExchangeRepo;
        this.musicUserRepo = musicUserRepo;
        this.roleRepo = roleRepo;
        this.orderService = orderService;
        this.priceRepo = priceRepo;
    }


    @Override
    @Transactional
    public void create(MusicDto musicDto, MusicProvider provider, Integer authorId) {
        Music music = null;
        if (musicDto.getProviderResourceId() != null) {
            music = musicRepo.findByProviderResourceId(musicDto.getProviderResourceId());
            if (music != null)
                return;
        }

        if (music == null) {
            music = new Music();
        }
        music.setAuthor(userRepo.findOne(authorId));
        if (musicDto.getGenre() != null)
            music.setGenre(genreRepo.findOne(musicDto.getGenre().getId()));
        music.setProviderResourceId(musicDto.getProviderResourceId());
        music.setLyrics(musicDto.getLyrics());
        music.setProvider(provider);
        music.setTitle(musicDto.getTitle());
        music.setStatus(MusicStatus.PENDING);
        music.setAgeRange(musicDto.getAgeRange());

        music.setCoverImageUrl(musicDto.getCoverImageUrl());
        music.setFileUrl(musicDto.getFileUrl());
        music.setPreviewFileUrl(musicDto.getPreviewFileUrl());
        music.setDownloadUrl(musicDto.getDownloadUrl());
        music.setCreated(new Date());
        music.setAdvertised(musicDto.getAdvertised());
        music.setArtistName(musicDto.getArtistName());
        music.setDeleted(false);
        musicRepo.save(music);
    }

    @Transactional
    @Override
    public MusicDto create(MusicSubmitDto musicSubmit, Integer authorId) {
        Music music = new Music();
        music.setAuthor(userRepo.findOne(authorId));
        music.setGenre(genreRepo.findOne(musicSubmit.getGenreId()));
        music.setLyrics(musicSubmit.getLyrics());
        music.setProvider(MusicProvider.LOVAPPY);
        music.setStatus(MusicStatus.PENDING);
        music.setTitle(musicSubmit.getTitle());
        music.setDecade(musicSubmit.getDecade());
        music.setCreated(new Date());
        music.setAdvertised(musicSubmit.isAdvertised());
        music.setArtistName(musicSubmit.getArtistName());
        music.setDeleted(false);
        return new MusicDto(musicRepo.save(music));
    }

    @Transactional
    @Override
    public MusicDto update(Integer musicId, MusicSubmitDto musicSubmit) {
        Music music = musicRepo.findOne(musicId);
        music.setGenre(genreRepo.findOne(musicSubmit.getGenreId()));
        music.setLyrics(musicSubmit.getLyrics());
        music.setProvider(MusicProvider.LOVAPPY);
        music.setStatus(MusicStatus.PENDING);
        music.setTitle(musicSubmit.getTitle());
        music.setAdvertised(musicSubmit.isAdvertised());
        return new MusicDto(musicRepo.save(music));
    }

    @Override
    public void delete(Integer musicId) {
        musicRepo.delete(musicId);
    }

    @Transactional
    @Override
    public MusicDto addFile(Integer musicId, CloudStorageFileDto file, CloudStorageFileDto demoFile) {
        Music music = musicRepo.findOne(musicId);
        if (music == null) {
            throw new ResourceNotFoundException("Music " + musicId + " is not found");
        }

        music.setFile(cloudStorageRepo.findOne(file.getId()));
        music.setPreviewFile(cloudStorageRepo.findOne(demoFile.getId()));
        music.setFileUrl(file.getUrl());
        music.setPreviewFileUrl(demoFile.getUrl());
        return new MusicDto(musicRepo.save(music));
    }

    @Transactional
    @Override
    public MusicDto addCoverPhoto(Integer musicId, CloudStorageFileDto file) {
        Music music = musicRepo.findOne(musicId);
        if (music == null) {
            throw new ResourceNotFoundException("Music " + musicId + " is not found");
        }

        music.setCoverImage(cloudStorageRepo.findOne(file.getId()));
        music.setCoverImageUrl(file.getUrl());
        return new MusicDto(musicRepo.save(music));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MusicDto> getAll(Pageable pageable) {
        Page<Music> all = musicRepo.findAll(pageable);
        return all.map(MusicDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MusicDto> getAllApproved(Pageable pageable) {
        Page<Music> all = musicRepo.findAllByStatus(MusicStatus.APPROVED, pageable);
        return all.map(MusicDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MusicDto> getAllByAuthor(Integer authorId, Pageable pageable) {
        return musicRepo.findByAuthor(userRepo.findOne(authorId), pageable).map(MusicDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MusicDto> getAllByKeywordAndAuthor(String keyword, String email, Pageable pageable) {
        QMusic qMusic = QMusic.music;
        return musicRepo.findAll(qMusic.author.email.eq(email).and(
                qMusic.title.containsIgnoreCase(keyword)
                        .or(qMusic.lyrics.containsIgnoreCase(keyword))
        ), pageable).map(MusicDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public MusicDto getMusic(Integer id) {
        Music music = musicRepo.findOne(id);
        return music == null ? null : new MusicDto(music);
    }

    @Override
    @Transactional(readOnly = true)
    public MusicDto getMusicWithRating(Integer id,Integer userID) {
        Music music = musicRepo.findOne(id);
        User user = userRepo.findOne(userID);
        MusicDto musicDto=null;

        if (music != null && user != null) {
            MusicUserPK pk = new MusicUserPK(user, music);
            MusicUser musicUser = musicUserRepo.findOne(pk);
            musicDto = new MusicDto(music);
            if(musicUser!=null && musicUser.getRating()!=null) {
                musicDto.setUserRating(new Double(musicUser.getRating()));
            }else{
                musicDto.setUserRating(new Double(-1));
            }
        }

        return musicDto;
    }

    @Transactional(readOnly = true)
    @Override
    public CloudStorageFileDto getMusicFile(Integer musicId) {
        Music music = musicRepo.findOne(musicId);
        return music == null ? null : new CloudStorageFileDto(music.getFile());
    }

    @Override
    @Transactional(readOnly = true)
    public MusicDto getApprovedMusic(Integer id) {
        Music music = musicRepo.findOne(id);
        if (MusicStatus.APPROVED.equals(music.getStatus()))
            return music == null ? null : new MusicDto(music);

        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MusicExchangeDto> getMusicExchange(Pageable pageable) {
        Page<MusicExchange> all = musicExchangeRepo.findAll(pageable);
        return all.map(MusicExchangeDto::new);
    }

    @Override
    public MusicExchange getMusicExchange(Integer musicExchangeId) {
        return musicExchangeRepo.findOne(musicExchangeId);
    }

    @Override
    @Transactional
    public MusicExchangeDto send(Integer senderId, Integer toUserId, Integer musicId, String lyrics) {

        Music music = musicRepo.findOne(musicId);
        User toUser = userRepo.findOne(toUserId);
        if(toUser == null) {
            throw new ResourceNotFoundException("User " + toUserId + " is not found !!");
        }
        User fromUser = userRepo.findOne(senderId);
        MusicExchange musicExchange = musicExchangeRepo.findTop1ByToUserAndFromUserAndMusicIdOrderByExchangeDateDesc(toUser, fromUser, musicId);
        if (musicExchange == null) {
            musicExchange = new MusicExchange();
            musicExchange.setFromUser(fromUser);
            musicExchange.setToUser(toUser);
            musicExchange.setMusic(music);
        }

        musicExchange.setExchangeDate(new Date());
        musicExchange.setLyrics(lyrics);
        musicExchange.setSendStatus(SendStatus.SENDING);
        musicExchange.setSeen(false);
        return new MusicExchangeDto(musicExchangeRepo.save(musicExchange));
    }

    @Override
    public CalculatePriceDto calculatePrice(Integer musicId, CouponDto coupon) {

        Price price1 = priceRepo.findByType(OrderDetailType.MUSIC);

        Double price = price1 != null ? price1.getPrice() : 0d;
        Double totalPrice = price;
        Double discountValue = 0d;
        if (coupon != null)
            discountValue = totalPrice * coupon.getDiscountPercent() * 0.01;

        totalPrice -= discountValue;

        return new CalculatePriceDto(price, coupon, totalPrice);
    }

    @Override
    @Transactional
    public OrderDto order(Integer musicId,  String couponCode, PaymentMethodType paymentMethodType) {
        Music music = musicRepo.findOne(musicId);
        if (music == null) {
            throw new ResourceNotFoundException("Music with id " + musicId + "is not found");
        }
        Price price =  priceRepo.findByType(OrderDetailType.MUSIC);
        return orderService.addOrder(music, OrderDetailType.MUSIC, price.getPrice(), 1, null,
                couponCode, paymentMethodType);
    }

    @Override
    @Transactional(readOnly = true)
    public List<GenreDto> getAllGenres() {
        List<GenreDto> genres = new ArrayList<>();
        for (Genre genre : genreRepo.findAll()) {
            genres.add(new GenreDto(genre));
        }
        return genres;
    }

    @Override
    @Transactional(readOnly = true)
    public List<GenreDto> getAllEnabledGenres() {
        List<GenreDto> genres = new ArrayList<>();
        for (Genre genre : genreRepo.findByEnabled(true)) {
            genres.add(new GenreDto(genre));
        }
        return genres;
    }

    @Override
    @Transactional(readOnly = true)
    public GenreDto getGenre(Integer id) {
        Genre genre = genreRepo.findOne(id);
        return genre == null ? null : new GenreDto(genre);
    }

    @Override
    public List<GenreDto> findByAddedToSurveyIsFalse() {
        List<GenreDto> genres = new ArrayList<>();
        for (Genre genre : genreRepo.findByAddedToSurveyIsFalseAndEnabledIsTrue()) {
            genres.add(new GenreDto(genre));
        }
        return genres;
    }

    @Override
    @Transactional
    public GenreDto addGenre(GenreDto genreDto) {
        Genre genre = new Genre();
        genre.setTitle(genreDto.getTitle());
        genre.setEnabled(true);

        Genre save = genreRepo.save(genre);
        return save == null ? null : new GenreDto(genre);
    }

    @Override
    @Transactional
    public GenreDto addGenreIfNotExist(GenreDto genreDto) {
        Genre genre = genreRepo.findByTitleIgnoreCase(genreDto.getTitle());
        if (genre == null) {
            genre.setTitle(genreDto.getTitle());
            genre.setEnabled(true);

            Genre save = genreRepo.save(genre);
            return save == null ? null : new GenreDto(genre);
        }
        return new GenreDto(genre);

    }

    @Override
    @Transactional
    public GenreDto updateGenre(GenreDto genreDto) {
        Genre genre = genreRepo.findOne(genreDto.getId());
        genre.setTitle(genreDto.getTitle());
        genre.setEnabled(genreDto.getEnabled());

        Genre save = genreRepo.save(genre);
        return save == null ? null : new GenreDto(genre);
    }

    @Override
    @Transactional
    public MusicDto updateStatus(Integer id, MusicStatus status) {
        Music music = musicRepo.findOne(id);
        music.setStatus(status);

        return new MusicDto(musicRepo.save(music));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MusicDto> getMostPopularPurchased(Pageable pageable) {
        Page<Music> musicPage = musicRepo.getMostPopularPurchased(pageable);
        return musicPage.map(MusicDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MusicDto> getRecentlyPurchased(Pageable pageable) {
        Page<Music> musicPage = musicRepo.getRecentlyPurchased(pageable);
        return musicPage.map(MusicDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MusicDto> findByFilterAndKeyword(String filter, String keyword, Pageable pageable) {
        QMusic qMusic = QMusic.music;
        if (filter.equals("artist")) {
            return musicRepo.findAll(qMusic.status.eq(MusicStatus.APPROVED).and(
                    qMusic.artistName.containsIgnoreCase(keyword
                    )), pageable).map(MusicDto::new);
        } else {
            return musicRepo.findAll(qMusic.status.eq(MusicStatus.APPROVED).and(
                    qMusic.title.containsIgnoreCase(keyword)
                            .or(qMusic.lyrics.containsIgnoreCase(keyword))
            ), pageable).map(MusicDto::new);

        }
    }

    @Override
    @Transactional
    public void rateMusic(Integer musicID, Integer userID, Integer rating) {
        Music music = musicRepo.findOne(musicID);
        User user = userRepo.findOne(userID);

        if (music != null && user != null) {
            MusicUserPK pk = new MusicUserPK(user, music);
            MusicUser musicUser = musicUserRepo.findOne(pk);
            if (musicUser == null) {
                musicUser = new MusicUser();
                musicUser.setId(pk);
                musicUser.setRating(rating);
                musicUser.setMobileDownloads(0);
                musicUser.setMaxDownloads(2);//??
                musicUser.setWebDownloads(0);
                musicUser.setReceived(false);
                musicUser.setPurchased(false);
                musicUser.setCreated(new Date());
            }
            musicUser.setRating(rating);
            musicUser.setUpdated(new Date());

            musicUserRepo.saveAndFlush(musicUser);

            //Update Music avarege Rating
            music.setAvgRating(musicUserRepo.findAverageRatingByMusicId(musicID));
            musicRepo.save(music);
        }
    }

    @Override
    @Transactional
    public void setPurchased(Integer musicID, Integer userID) {
        Music music = musicRepo.findOne(musicID);
        User user = userRepo.findOne(userID);

        if (music != null && user != null) {
            MusicUserPK pk = new MusicUserPK(user, music);
            MusicUser musicUser = musicUserRepo.findOne(pk);
            if (musicUser == null) {
                musicUser = new MusicUser();
                musicUser.setId(pk);
                musicUser.setRating(-1);
                musicUser.setMobileDownloads(0);
                musicUser.setMaxDownloads(2);//??
                musicUser.setWebDownloads(0);
                musicUser.setReceived(false);
                musicUser.setPurchased(true);
                musicUser.setCreated(new Date());
            }
            musicUser.setPurchased(true);
            musicUser.setUpdated(new Date());

            musicUserRepo.saveAndFlush(musicUser);

            musicRepo.save(music);
        }
    }

    @Override
    @Transactional
    public void setReceived(Integer musicID, Integer userID) {
        Music music = musicRepo.findOne(musicID);
        User user = userRepo.findOne(userID);

        if (music != null && user != null) {
            MusicUserPK pk = new MusicUserPK(user, music);
            MusicUser musicUser = musicUserRepo.findOne(pk);
            if (musicUser == null) {
                musicUser = new MusicUser();
                musicUser.setId(pk);
                musicUser.setRating(-1);
                musicUser.setMobileDownloads(0);
                musicUser.setMaxDownloads(2);//??
                musicUser.setWebDownloads(0);
                musicUser.setReceived(false);
                musicUser.setPurchased(false);
                musicUser.setCreated(new Date());
            }
            musicUser.setReceived(true);
            musicUser.setUpdated(new Date());

            musicUserRepo.saveAndFlush(musicUser);

            musicRepo.save(music);
        }
    }

    @Override
    public void setUserResponded(Integer musicID) {
        MusicExchange musicExchange = musicExchangeRepo.findOne(musicID);
        musicExchange.setUserResponded(true);
        musicExchangeRepo.save(musicExchange);
    }

    @Transactional(readOnly = true)
    @Override
    public Integer countMusicSent(Integer userId) {
        return musicExchangeRepo.countByFromUser(userRepo.findOne(userId));
    }

    @Transactional(readOnly = true)
    @Override
    public Integer countUnSeenMusicReceived(Integer userId) {
        if (userId == null)
            return 0;
        return musicExchangeRepo.countByToUserAndSeen(userRepo.findOne(userId), false);
    }

    @Transactional
    @Override
    public void setMusicNotificationSeen(Integer sendTo, Integer sendBy, Integer musicId) {
        MusicExchange musicExchange = musicExchangeRepo.findTop1ByToUserAndFromUserAndMusicIdOrderByExchangeDateDesc(userRepo.findOne(sendTo), userRepo.findOne(sendBy), musicId);
        if(musicExchange != null) {
            musicExchange.setSeen(true);
            musicExchangeRepo.save(musicExchange);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Page<MusicDto> getAllMusic(PageRequest pageRequest, String searchText, int genre, String decade) {

        Genre genre1 = genre != 0 ? genreRepo.findOne(genre) : null;

        decade = !decade.equals("0") && !decade.equals("") ? decade : null;

        Page<Music> all = musicRepo.search(MusicStatus.APPROVED, "%" + searchText + "%", genre1, decade, pageRequest);
        return all.map(MusicDto::new);
    }

    @Transactional
    @Override
    public void updateMusicExchange(MusicExchange musicExchange) {
        musicExchangeRepo.save(musicExchange);
    }

    @Transactional(readOnly = true)
    @Override
    public Integer countMusicReceived(Integer userId) {
        return musicExchangeRepo.countByToUser(userRepo.findOne(userId));
    }

    @Transactional(readOnly = true)
    @Override
    public Integer countMusicSentBetweenFromAndToUser(Integer fromUser, Integer toUser) {
        return musicExchangeRepo.countByFromUserAndToUser(userRepo.findOne(fromUser), userRepo.findOne(toUser));
    }

    @Override
    @Transactional(readOnly = true)
    public Integer countSongsByDateBetween(Date fromDate, Date toDate) {
        return musicRepo.countByCreatedBetween(fromDate, toDate);
    }

    @Override
    @Transactional(readOnly = true)
    public Integer countSendingMusicByFromUserAndGenderAndSendDateBetween(Integer userId, Gender gender, Date fromDate, Date toDate) {
        return musicExchangeRepo.countByFromUserAndToUserUserProfileGenderAndExchangeDateBetween(userRepo.findOne(userId), gender, fromDate, toDate);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<MusicExchangeDto> getMusicExchangeByFromUser(Integer fromUser, Pageable pageable) {

        return musicExchangeRepo.findByFromUser(userRepo.findOne(fromUser), pageable).map(MusicExchangeDto::new);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<MusicExchangeDto> getMusicExchangeByToUser(Integer toUser, Pageable pageable) {
        return musicExchangeRepo.findByToUser(userRepo.findOne(toUser), pageable).map(this::convertToObjectDto);
    }

    private MusicExchangeDto convertToObjectDto(MusicExchange o) {
        MusicExchangeDto dto = new MusicExchangeDto(o);
        return dto;
    }

    @Transactional(readOnly = true)
    @Override
    public Integer countMusicSentBetweenFromAndToUserAndMusic(Integer fromUser, Integer toUser, Integer musicId) {
        return musicExchangeRepo.countByFromUserAndToUserAndMusic(userRepo.findOne(fromUser), userRepo.findOne(toUser), musicRepo.findOne(musicId));
    }

    @Transactional
    @Override
    public void deleteAll() {
        musicRepo.deleteAll();
    }

    @Override
    @Transactional
    public void setDownloadStatus(Integer musicID, Integer userID, MusicDownload downloadBy) {
        Music music = musicRepo.findOne(musicID);
        User user = userRepo.findOne(userID);

        if (music != null && user != null) {
            MusicUserPK pk = new MusicUserPK(user, music);
            MusicUser musicUser = musicUserRepo.findOne(pk);
            if (musicUser == null) {
                musicUser = new MusicUser();
                musicUser.setId(pk);
                musicUser.setRating(-1);

                musicUser.setMaxDownloads(2);//??

                musicUser.setMobileDownloads(0);
                musicUser.setWebDownloads(0);
                musicUser.setReceived(false);
                musicUser.setPurchased(true);
                musicUser.setCreated(new Date());
            }

            if (MusicDownload.MOBILE.equals(downloadBy)) {
                musicUser.setMobileDownloads(musicUser.getMobileDownloads() + 1);
            } else {
                musicUser.setWebDownloads(musicUser.getWebDownloads() + 1);
            }
            musicUser.setUpdated(new Date());

            musicUserRepo.saveAndFlush(musicUser);

            musicRepo.save(music);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public boolean isPurchased(Integer musicID, Integer userID) {
        MusicUserDto musicUserDto = getMusicUserDetails(musicID, userID);
        return musicUserDto != null && musicUserDto.getPurchased();
    }

    @Transactional(readOnly = true)
    @Override
    public MusicUserDto getMusicUserDetails(Integer musicID, Integer userID) {
        User user = userRepo.findOne(userID);
        Music music = musicRepo.findOne(musicID);

        MusicUser musicUser = musicUserRepo.findOne(new MusicUserPK(user, music));
        return musicUser == null ? null : new MusicUserDto(musicUser);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<MusicSubDetailsDto> getTopByLovappyUploader(Pageable pageable) {
        List<Role> roles = roleRepo.findByNameIn(Arrays.asList(Roles.ADMIN.getValue(), Roles.SUPER_ADMIN.getValue()));
        Page<Music> musicPage = musicRepo.getMostPopularPurchasedByRole(new HashSet<>(roles), pageable);
        return musicPage.map(MusicSubDetailsDto::new);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<MusicSubDetailsDto> getTopByMusicianUploader(Pageable pageable) {
        List<Role> roles = roleRepo.findByNameIn(Arrays.asList(Roles.MUSICIAN.getValue()));
        Page<Music> musicPage = musicRepo.getMostPopularPurchasedByRole(new HashSet<>(roles), pageable);
        return musicPage.map(MusicSubDetailsDto::new);
    }

}
