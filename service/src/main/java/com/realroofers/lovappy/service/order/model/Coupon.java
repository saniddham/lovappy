package com.realroofers.lovappy.service.order.model;

import com.realroofers.lovappy.service.core.BaseEntity;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author Eias Altawil
 */

@Entity
@Table(name="coupon")
@Data
public class Coupon extends BaseEntity<Integer> {

    @Column(unique = true)
    private String couponCode;

    @Enumerated(EnumType.STRING)
    private CouponCategory category;

    private String description;
    private Integer discountPercent;
    private Date expiryDate;

    @OneToMany(mappedBy = "coupon")
    private List<ProductOrder> orders;
}
