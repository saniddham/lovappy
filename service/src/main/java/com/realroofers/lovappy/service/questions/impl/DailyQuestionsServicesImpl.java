package com.realroofers.lovappy.service.questions.impl;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.cloud.model.CloudStorageFile;
import com.realroofers.lovappy.service.cloud.repo.CloudStorageRepo;
import com.realroofers.lovappy.service.order.OrderService;
import com.realroofers.lovappy.service.order.dto.CalculatePriceDto;
import com.realroofers.lovappy.service.order.dto.CouponDto;
import com.realroofers.lovappy.service.order.support.CouponCategory;
import com.realroofers.lovappy.service.questions.DailyQuestionService;
import com.realroofers.lovappy.service.questions.dto.DailyQuestionDto;
import com.realroofers.lovappy.service.questions.dto.UserResponseDto;
import com.realroofers.lovappy.service.questions.model.BuyResponse;
import com.realroofers.lovappy.service.questions.model.BuyerId;
import com.realroofers.lovappy.service.questions.model.DailyQuestion;
import com.realroofers.lovappy.service.questions.model.UserResponse;
import com.realroofers.lovappy.service.questions.repo.BuyerResponseRepo;
import com.realroofers.lovappy.service.questions.repo.DailyQuestionRepo;
import com.realroofers.lovappy.service.questions.repo.UserResponseRepo;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.system.repo.LanguageRepo;
import com.realroofers.lovappy.service.user.model.User;
import com.realroofers.lovappy.service.user.repo.UserRepo;
import com.realroofers.lovappy.service.user.support.ApprovalStatus;
import com.realroofers.lovappy.service.util.ListMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Darrel Rayen on 9/18/18.
 */
@Service
@Slf4j
public class DailyQuestionsServicesImpl implements DailyQuestionService {

    private final DailyQuestionRepo questionRepo;
    private final UserResponseRepo userResponseRepo;
    private final UserRepo userRepo;
    private final LanguageRepo languageRepo;
    private final CloudStorageRepo cloudStorageRepo;
    private final BuyerResponseRepo buyerResponseRepo;
    private final OrderService orderService;

    @Autowired
    public DailyQuestionsServicesImpl(DailyQuestionRepo questionRepo, UserResponseRepo userResponseRepo, UserRepo userRepo, LanguageRepo languageRepo, CloudStorageRepo cloudStorageRepo, BuyerResponseRepo buyerResponseRepo, OrderService orderService) {
        this.questionRepo = questionRepo;
        this.userResponseRepo = userResponseRepo;
        this.userRepo = userRepo;
        this.languageRepo = languageRepo;
        this.cloudStorageRepo = cloudStorageRepo;
        this.buyerResponseRepo = buyerResponseRepo;
        this.orderService = orderService;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DailyQuestionDto> getAllQuestions(Pageable pageable) {
        Page<DailyQuestion> dailyQuestions = questionRepo.findAll(pageable);
        return dailyQuestions.map(DailyQuestionDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DailyQuestionDto> getAllQuestionsWithUserId(Integer userId, Pageable pageable) {
        Page<DailyQuestion> dailyQuestions = questionRepo.findAll(pageable);
        return dailyQuestions.map(objects1 -> new DailyQuestionDto(objects1, userId));
    }

    @Override
    @Transactional(readOnly = true)
    public DailyQuestionDto findQuestionById(Long id) {
        DailyQuestion dailyQuestion = questionRepo.findOne(id);
        return new DailyQuestionDto(dailyQuestion);
    }

    @Override
    @Transactional(readOnly = true)
    public DailyQuestionDto findLatestQuestion() {
        DailyQuestion question = questionRepo.findTopByOrderByIdDesc();
        if (question != null) {
            return new DailyQuestionDto(question);
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public DailyQuestionDto findQuestionByDate(Date date) {
        DailyQuestion question = questionRepo.findFirstByQuestionDate(date);
        return new DailyQuestionDto(question);
    }

    @Override
    @Transactional
    public DailyQuestionDto addQuestion(DailyQuestionDto question) {
        DailyQuestion dailyQuestion = new DailyQuestion(question.getQuestion(), new Date(question.getQuestionDate()));
        DailyQuestion savedQuestion = questionRepo.save(dailyQuestion);
        if (savedQuestion != null) {
            return new DailyQuestionDto(savedQuestion);
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public DailyQuestionDto updateQuestion(DailyQuestionDto question) {
        DailyQuestion dailyQuestion = questionRepo.findOne(question.getQuestionId());
        dailyQuestion.setQuestion(question.getQuestion());
        DailyQuestion updatedQuestion = questionRepo.save(dailyQuestion);
        if (updatedQuestion != null) {
            return new DailyQuestionDto(updatedQuestion);
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public DailyQuestionDto updateQuestion(Long id, String question) {
        DailyQuestion dailyQuestion = questionRepo.findOne(id);
        dailyQuestion.setQuestion(question);
        DailyQuestion updatedQuestion = questionRepo.save(dailyQuestion);
        if (updatedQuestion != null) {
            return new DailyQuestionDto(updatedQuestion);
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public Boolean deleteDailyQuestion(Long id) {
        questionRepo.delete(id);
        return !questionRepo.exists(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserResponseDto> findResponsesByQuestionId(Long id, Pageable pageable, ApprovalStatus approvalStatus) {
        DailyQuestion question = questionRepo.findOne(id);
        Page<UserResponse> userResponses = userResponseRepo.findAllByQuestionAndResponseApprovalStatus(
                question, approvalStatus, pageable);
        return userResponses.map(UserResponseDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserResponseDto> findResponsesByQuestionId(Long id, Pageable pageable) {
        DailyQuestion question = questionRepo.findOne(id);
        Page<UserResponse> userResponses = userResponseRepo.findAllByQuestion(question, pageable);
        return userResponses.map(UserResponseDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public UserResponseDto findByResponseId(Long id) {
        UserResponse response = userResponseRepo.findOne(id);
        return new UserResponseDto(response);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserResponseDto> findUserResponseByUserId(Integer userId) {
        List<UserResponse> userResponses = userResponseRepo.findAllByUser(userId);
        return new ListMapper<>(userResponses).map(UserResponseDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserResponseDto> findPaidUserResponseByUserId(Integer userId, Integer buyerId) {
        List<UserResponseDto> responseDtos = new ArrayList<>();
        List<UserResponse> userResponses = userResponseRepo.findAllByUserAndApprovalStatus(userId, ApprovalStatus.APPROVED);
        List<BuyResponse> buyResponses = buyerResponseRepo.findAllByUserId(buyerId);
        userResponses.forEach(userResponse -> {
            UserResponseDto dto = new UserResponseDto(userResponse);
            for (BuyResponse buyResponse : buyResponses) {
                if (userResponse.getId().equals(buyResponse.getUserResponse().getId())) {
                    dto.setIsPurchased(true);
                    break;
                } else {
                    dto.setIsPurchased(false);
                }
            }
            responseDtos.add(dto);
        });
        return responseDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserResponseDto> findUserResponseByUserId(Integer userId, Pageable pageable) {
        Page<UserResponse> userResponses = userResponseRepo.findAllByUser(userId, pageable);
        return userResponses.map(UserResponseDto::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserResponseDto> findApprovedUserResponseByUserId(Integer userId, Pageable pageable) {
        Page<UserResponse> userResponses = userResponseRepo.findAllApprovedByUser(userId, pageable);
        return userResponses.map(UserResponseDto::new);
    }

    @Override
    public Page<UserResponseDto> findByResponseApprovalStatus(ApprovalStatus status) {
        return null;
    }

    @Override
    @Transactional
    public Boolean changeResponseApprovalStatus(Long responseId, ApprovalStatus approvalStatus) {
        UserResponse userResponse = userResponseRepo.findOne(responseId);
        userResponse.setResponseApprovalStatus(approvalStatus);
        UserResponse updated = userResponseRepo.save(userResponse);
        return updated != null;
    }

    @Override
    @Transactional
    public Boolean deleteResponse(Long responseId) {
        UserResponse userResponse = userResponseRepo.findOne(responseId);
        if (userResponse != null) {
            userResponseRepo.delete(userResponse);
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public Boolean deleteResponseMobile(Long responseId, Long questionId, Integer userId) {
        UserResponse userResponse = userResponseRepo.findOne(responseId);
        if (userResponse != null) {
            if (questionId.equals(userResponse.getQuestion().getId()) && userId.equals(userResponse.getUser().getUserId())) {
                userResponseRepo.delete(userResponse);
                return !userResponseRepo.exists(responseId);
            }
        }
        return false;
    }

    @Override
    @Transactional
    public UserResponseDto addUserResponse(UserResponseDto userResponse) {
        DailyQuestion dailyQuestion = questionRepo.findOne(userResponse.getQuestionId());
        User user = userRepo.findOne(userResponse.getUserId());
        Language language = languageRepo.findOne(userResponse.getLanguageId());
        List<UserResponse> responseList = userResponseRepo.findAllByUserAndQuestion(user, dailyQuestion);
        UserResponse response;
        if (responseList.isEmpty()) {
            response = new UserResponse();
        } else {
            response = responseList.get(0);
        }
        response.setQuestion(dailyQuestion);
        response.setResponseApprovalStatus(ApprovalStatus.PENDING);
        response.setResponseDate(userResponse.getResponseDate());
        response.setUser(user);
        response.setRecordSeconds(userResponse.getRecordSeconds());
        response.setLanguage(language);
        response.setAudioResponse(getCloudRecording(userResponse.getAudioResponse()));
        UserResponse savedResponse = userResponseRepo.save(response);
        if (savedResponse != null) {
            return new UserResponseDto(savedResponse);
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public UserResponseDto updateUserResponse(UserResponseDto userResponse) {
        UserResponse response = userResponseRepo.findOne(userResponse.getResponseId());
        response.setResponseApprovalStatus(ApprovalStatus.PENDING);
        response.setUser(userRepo.findOne(userResponse.getUserId()));
        response.setAudioResponse(getCloudRecording(userResponse.getAudioResponse()));
        response.setRecordSeconds(userResponse.getRecordSeconds());
        UserResponse savedResponse = userResponseRepo.save(response);
        if (savedResponse != null) {
            return new UserResponseDto(savedResponse);
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public UserResponseDto updateUserResponseMobile(UserResponseDto userResponse, Integer userId) {
        UserResponse response = userResponseRepo.findOne(userResponse.getResponseId());
        if (response.getQuestion().getId().equals(userResponse.getQuestionId())
                && response.getUser().getUserId().equals(userId)) {
            response.setResponseApprovalStatus(ApprovalStatus.PENDING);
            response.setUser(userRepo.findOne(userId));
            response.setAudioResponse(getCloudRecording(userResponse.getAudioResponse()));
            response.setRecordSeconds(userResponse.getRecordSeconds());
            UserResponse savedResponse = userResponseRepo.save(response);
            if (savedResponse != null) {
                return new UserResponseDto(savedResponse);
            }
        }
        return null;
    }

    @Override
    @Transactional
    public Integer findUserResponseCountByUserId(Integer userId) {
        Integer count = userResponseRepo.findUserResponseCountByUserId(userId);
        return (count != null && count > 0) ? count : 0;
    }

    @Override
    @Transactional(readOnly = true)
    public CalculatePriceDto calculatePrice(Integer quantity, String couponCode) {
        Double totalPrice = 0d;
        Double price = 2.50;//take it form database form PRice table
        if (quantity != null && quantity > 0) {
            if (quantity == 3) {
                totalPrice = 4d;
            } else {
                totalPrice = quantity * 2.50;
            }
        }
        CouponDto coupon = orderService.getCoupon(couponCode, CouponCategory.DAILY_QUESTION);
        Double discountValue = 0d;
        if (coupon != null) {
            discountValue = totalPrice * coupon.getDiscountPercent() * 0.01;
            totalPrice = totalPrice - discountValue;
        }
        return new CalculatePriceDto(price, coupon, totalPrice);
    }

    @Override
    @Transactional
    public void addPurchaseRecord(List<Long> responseIds, Integer userId) {
        User user = userRepo.findOne(userId);
        List<UserResponse> userResponses = userResponseRepo.findByIdIn(responseIds);
        for (UserResponse userResponse : userResponses) {

            BuyerId buyerId = new BuyerId(userId, userResponse.getId());
            BuyResponse buyResponse = new BuyResponse(userResponse, user);
            buyResponse.setBuyerId(buyerId);
            buyerResponseRepo.save(buyResponse);
        }
    }

    private CloudStorageFile getCloudRecording(CloudStorageFileDto file) {
        CloudStorageFile cloudStorageFile = null;
        if (file.getId() != null)
            cloudStorageFile = cloudStorageRepo.findOne(file.getId());

        if (cloudStorageFile == null) {
            cloudStorageFile = new CloudStorageFile();
            cloudStorageFile.setBucket(file.getBucket());
            cloudStorageFile.setName(file.getName());
            cloudStorageFile.setUrl(file.getUrl());
        }
        return cloudStorageFile;
    }
}
