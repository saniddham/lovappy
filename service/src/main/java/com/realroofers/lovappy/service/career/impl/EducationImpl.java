package com.realroofers.lovappy.service.career.impl;

import com.realroofers.lovappy.service.career.EducationService;
import com.realroofers.lovappy.service.career.model.Education;
import com.realroofers.lovappy.service.career.model.Vacancy;
import com.realroofers.lovappy.service.career.repo.EducationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Set;

@Service
public class EducationImpl implements EducationService {

    @Autowired
    private EducationRepo educationRepo;

    @Override
    public String addEducations(Set<Education> educations) {

        String status;
        try {
            educationRepo.save(educations);
            status = "SuccessFully Saved";

        } catch (Exception e){
            System.out.println("error in inserting");
            System.out.println(e);;
            status = "Error Occured While Saving";
        }

        return status;
    }
}
