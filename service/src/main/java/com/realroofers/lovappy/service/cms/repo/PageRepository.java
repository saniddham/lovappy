package com.realroofers.lovappy.service.cms.repo;

import com.realroofers.lovappy.service.cms.model.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Daoud Shaheen on 6/6/2017.
 */
public interface PageRepository extends JpaRepository<Page, Integer> {

    Page findByTag(String tag);

    List<Page> findAllByActiveIsTrue();

    List<Page> findAllByOrderByActiveDescNameAsc();

    @Query("Select c from Page c where c.name LIKE  %?1%")
    org.springframework.data.domain.Page<Page> findAllByName(String name, Pageable pageable);

}
