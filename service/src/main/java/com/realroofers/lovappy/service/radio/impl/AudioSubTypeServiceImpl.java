package com.realroofers.lovappy.service.radio.impl;

import com.realroofers.lovappy.service.radio.AudioSubTypeService;
import com.realroofers.lovappy.service.radio.model.AudioSubType;
import com.realroofers.lovappy.service.radio.repo.AudioSubTypeRepo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Manoj on 17/12/2017.
 */
@Service
public class AudioSubTypeServiceImpl implements AudioSubTypeService {

    private final AudioSubTypeRepo audioSubTypeRepo;

    public AudioSubTypeServiceImpl(AudioSubTypeRepo audioSubTypeRepo) {
        this.audioSubTypeRepo = audioSubTypeRepo;
    }

    @Override
    public List<AudioSubType> findAll() {
        return audioSubTypeRepo.findAll();
    }

    @Override
    public List<AudioSubType> findAllActive() {
        return audioSubTypeRepo.findAllByActiveIsTrueOrderByName();
    }

    @Override
    public AudioSubType create(AudioSubType audioSubType) {
        return audioSubTypeRepo.saveAndFlush(audioSubType);
    }

    @Override
    public AudioSubType update(AudioSubType audioSubType) {
        return audioSubTypeRepo.save(audioSubType);
    }

    @Override
    public void delete(Integer id) {
        audioSubTypeRepo.delete(id);
    }

    @Override
    public AudioSubType findById(Integer id) {
        return audioSubTypeRepo.findOne(id);
    }

    @Override
    public AudioSubType findByName(String name) {
        return audioSubTypeRepo.findByName(name);
    }
}
