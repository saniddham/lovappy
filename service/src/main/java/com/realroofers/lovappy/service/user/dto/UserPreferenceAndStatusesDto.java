package com.realroofers.lovappy.service.user.dto;

import com.realroofers.lovappy.service.system.dto.LanguageDto;
import com.realroofers.lovappy.service.system.model.Language;
import com.realroofers.lovappy.service.user.model.UserPreference;
import com.realroofers.lovappy.service.user.support.Gender;
import com.realroofers.lovappy.service.user.support.PrefHeight;
import com.realroofers.lovappy.service.user.support.Size;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * @author mwiyono
 *
 */
@EqualsAndHashCode
public class UserPreferenceAndStatusesDto {

    private Gender gender;

    private Boolean penisSizeMatter;

    private Size bustSize;

    private Size buttSize;

    private Integer minAge;

    private Integer maxAge;

    private PrefHeight height;

    private Collection<LanguageDto> seekingLanguages;

    private String seekingLanguagesAbb;
    private String employed;
    private String risktaker;
    private String believeingod;
    private String wantkids;
    private String havekids;
    private String attractmany;
    public UserPreferenceAndStatusesDto() {
    }


    public UserPreferenceAndStatusesDto(UserPreferenceDto userPreference, Map<String, String> statuses) {
    	if (userPreference!=null){

			this.gender = userPreference.getGender();
			this.penisSizeMatter = userPreference.getPenisSizeMatter();
			this.bustSize = userPreference.getBustSize();
			this.buttSize = userPreference.getButtSize();
			this.minAge = userPreference.getMinAge();
			this.maxAge = userPreference.getMaxAge();
			this.height = userPreference.getHeight();

			this.believeingod = statuses.get("believeingod");
            this.wantkids = statuses.get("wantkids");
            this.havekids = statuses.get("havekids");
            this.risktaker = statuses.get("risktaker");
            this.employed = statuses.get("employed");
            this.attractmany = statuses.get("attractmany");
            if (userPreference.getSeekingLanguages() != null) {
                this.seekingLanguages = new ArrayList<>();

                this.seekingLanguages.addAll(userPreference.getSeekingLanguages());
            }

    	}
	}



	public UserPreferenceAndStatusesDto(Gender gender, Boolean penisSizeMatter, Size bustSize, Size buttSize, Integer minAge,
                                        Integer maxAge, PrefHeight height, String language) {
		super();
		this.gender = gender;
		this.penisSizeMatter = penisSizeMatter;
		this.bustSize = bustSize;
		this.buttSize = buttSize;
		this.minAge = minAge;
		this.maxAge = maxAge;
		this.height = height;
	}

    public String getAttractmany() {
        return attractmany;
    }

    public void setAttractmany(String attractmany) {
        this.attractmany = attractmany;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Boolean getPenisSizeMatter() {
        return penisSizeMatter;
    }

    public void setPenisSizeMatter(Boolean penisSizeMatter) {
        this.penisSizeMatter = penisSizeMatter;
    }

    public Size getBustSize() {
        return bustSize;
    }

    public void setBustSize(Size bustSize) {
        this.bustSize = bustSize;
    }

    public Size getButtSize() {
        return buttSize;
    }

    public void setButtSize(Size buttSize) {
        this.buttSize = buttSize;
    }

    public Integer getMinAge() {
        return minAge;
    }

    public void setMinAge(Integer minAge) {
        this.minAge = minAge;
    }

    public Integer getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(Integer maxAge) {
        this.maxAge = maxAge;
    }

    public PrefHeight getHeight() {
        return height;
    }

    public void setHeight(PrefHeight height) {
        this.height = height;
    }

    public Collection<LanguageDto> getSeekingLanguages() {
        return seekingLanguages;
    }

    public void setSeekingLanguages(Collection<LanguageDto> seekingLanguages) {
        this.seekingLanguages = seekingLanguages;
    }

    public String getEmployed() {
        return employed;
    }

    public void setEmployed(String employed) {
        this.employed = employed;
    }

    public String getRisktaker() {
        return risktaker;
    }

    public void setRisktaker(String risktaker) {
        this.risktaker = risktaker;
    }

    public String getBelieveingod() {
        return believeingod;
    }

    public void setBelieveingod(String believeingod) {
        this.believeingod = believeingod;
    }

    public String getWantkids() {
        return wantkids;
    }

    public void setWantkids(String wantkids) {
        this.wantkids = wantkids;
    }

    public String getHavekids() {
        return havekids;
    }

    public void setHavekids(String havekids) {
        this.havekids = havekids;
    }

    public Map<String, String> getStatuses() {
        Map<String, String> statuses = new HashMap<>();
        statuses.put("believeingod", this.believeingod);
        statuses.put("wantkids", this.wantkids );
        statuses.put("havekids", this.havekids);
        statuses.put("attractmany", this.attractmany);
        statuses.put("risktaker", this.risktaker);
        statuses.put("employed", this.employed );
        return statuses;

    }
    public String getSeekingLanguagesAbb() {
        String sl = "";
        if(this.seekingLanguages != null) {
            for (LanguageDto languageDto : this.seekingLanguages) {
                sl = sl.concat(languageDto.getAbbreviation() + ", ");
            }
        }

        return sl.length() > 2 ? sl.substring(0, sl.length() - 2) : "";
    }

    public String getGenderIcon() {
		if  (gender!=null && gender.equals(Gender.FEMALE)){
			return "new-female-icon.png";
		} else if  (gender!=null && gender.equals(Gender.MALE)){
			return "new-male-icon.png";
		} else if  (gender!=null && gender.equals(Gender.BOTH)){
			return "new-both-gender.png";
		}
		
		return "new-female-icon.png";
	}

}
