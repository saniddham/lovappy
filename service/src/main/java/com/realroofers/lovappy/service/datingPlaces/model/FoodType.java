package com.realroofers.lovappy.service.datingPlaces.model;

import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Darrel Rayen on 12/8/17.
 */
@Entity
@Table(name = "food_type")
@EqualsAndHashCode
public class FoodType implements Serializable{

    @Id
    @GeneratedValue
    @Column(name = "food_id")
    private Integer id;
    private String foodTypeName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "foodType", cascade = CascadeType.ALL)
    private List<DatingPlaceFoodTypes> foodTypes = new ArrayList<>();

    private Boolean enabled;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFoodTypeName() {
        return foodTypeName;
    }

    public void setFoodTypeName(String foodTypeName) {
        this.foodTypeName = foodTypeName;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
