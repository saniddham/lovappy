package com.realroofers.lovappy.service.music.dto;

import com.realroofers.lovappy.service.cloud.dto.CloudStorageFileDto;
import com.realroofers.lovappy.service.music.model.Music;
import com.realroofers.lovappy.service.user.dto.UserDto;
import com.realroofers.lovappy.service.user.model.User;
import lombok.EqualsAndHashCode;
import org.springframework.util.StringUtils;

/**
 * Created by Daoud Shaheen on 11/1/2017.
 */
@EqualsAndHashCode
public class MusicSubDetailsDto {

    private Integer id;
    private String title;
    private String fileUrl;
    private String coverImageUrl;
    private UserDto author;
    private String artistName;

    public MusicSubDetailsDto() {
    }

    public MusicSubDetailsDto(Music music) {
        this.id = music.getId();
        this.title = music.getTitle();
        this.fileUrl = music.getFileUrl();
        this.coverImageUrl = music.getCoverImageUrl();
        this.author = new UserDto(music.getAuthor());
        this.artistName = music.getArtistName();
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public void setAuthorUsername(User author) {
        if(StringUtils.isEmpty(author.getFirstName()) )
            this.artistName = author.getEmail();
        else {
            this.artistName = StringUtils.isEmpty(author.getLastName()) ? author.getFirstName() : author.getFirstName() + " " +
                    author.getLastName();
        }
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public UserDto getAuthor() {
        return author;
    }

    public void setAuthor(UserDto author) {
        this.author = author;
    }


}
