package com.realroofers.lovappy.service.gift.dto;

import com.realroofers.lovappy.service.gift.model.ViewedGift;
import com.realroofers.lovappy.service.product.model.Product;
import com.realroofers.lovappy.service.user.dto.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Manoj on 08/02/2018.
 */
@Data
@ToString
@EqualsAndHashCode
public class ViewedGiftDto implements Serializable {

    Integer id;
    Product product;
    private UserDto user;
    private Date viewedOn;

    public ViewedGiftDto() {
    }

    public ViewedGiftDto(ViewedGift viewedGift) {
        id = viewedGift.getId();
        product = viewedGift.getProduct();
        user = new UserDto(viewedGift.getUser());
        viewedOn = viewedGift.getViewedOn();
    }
}
