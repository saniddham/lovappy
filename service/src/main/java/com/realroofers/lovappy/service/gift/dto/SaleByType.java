package com.realroofers.lovappy.service.gift.dto;

public interface SaleByType {
    String getChartType();
    Long getSale();
}
