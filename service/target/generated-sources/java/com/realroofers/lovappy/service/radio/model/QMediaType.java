package com.realroofers.lovappy.service.radio.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMediaType is a Querydsl query type for MediaType
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMediaType extends EntityPathBase<MediaType> {

    private static final long serialVersionUID = -1300841757L;

    public static final QMediaType mediaType = new QMediaType("mediaType");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath description = createString("description");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath name = createString("name");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QMediaType(String variable) {
        super(MediaType.class, forVariable(variable));
    }

    public QMediaType(Path<? extends MediaType> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMediaType(PathMetadata metadata) {
        super(MediaType.class, metadata);
    }

}

