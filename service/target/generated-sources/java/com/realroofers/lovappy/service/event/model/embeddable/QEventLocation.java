package com.realroofers.lovappy.service.event.model.embeddable;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QEventLocation is a Querydsl query type for EventLocation
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QEventLocation extends BeanPath<EventLocation> {

    private static final long serialVersionUID = 110567362L;

    public static final QEventLocation eventLocation = new QEventLocation("eventLocation");

    public final StringPath city = createString("city");

    public final StringPath country = createString("country");

    public final StringPath floorRoom = createString("floorRoom");

    public final StringPath fullAddress = createString("fullAddress");

    public final NumberPath<Double> latitude = createNumber("latitude", Double.class);

    public final NumberPath<Double> longitude = createNumber("longitude", Double.class);

    public final StringPath premise = createString("premise");

    public final StringPath province = createString("province");

    public final StringPath route = createString("route");

    public final StringPath state = createString("state");

    public final StringPath stateShort = createString("stateShort");

    public final StringPath streetNumber = createString("streetNumber");

    public final StringPath zipCode = createString("zipCode");

    public QEventLocation(String variable) {
        super(EventLocation.class, forVariable(variable));
    }

    public QEventLocation(Path<? extends EventLocation> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEventLocation(PathMetadata metadata) {
        super(EventLocation.class, metadata);
    }

}

