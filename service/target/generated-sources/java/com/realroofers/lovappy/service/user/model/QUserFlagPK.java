package com.realroofers.lovappy.service.user.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUserFlagPK is a Querydsl query type for UserFlagPK
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QUserFlagPK extends BeanPath<UserFlagPK> {

    private static final long serialVersionUID = -684481513L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QUserFlagPK userFlagPK = new QUserFlagPK("userFlagPK");

    public final QUser flagBy;

    public final QUser flagTo;

    public QUserFlagPK(String variable) {
        this(UserFlagPK.class, forVariable(variable), INITS);
    }

    public QUserFlagPK(Path<? extends UserFlagPK> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QUserFlagPK(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QUserFlagPK(PathMetadata metadata, PathInits inits) {
        this(UserFlagPK.class, metadata, inits);
    }

    public QUserFlagPK(Class<? extends UserFlagPK> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.flagBy = inits.isInitialized("flagBy") ? new QUser(forProperty("flagBy"), inits.get("flagBy")) : null;
        this.flagTo = inits.isInitialized("flagTo") ? new QUser(forProperty("flagTo"), inits.get("flagTo")) : null;
    }

}

