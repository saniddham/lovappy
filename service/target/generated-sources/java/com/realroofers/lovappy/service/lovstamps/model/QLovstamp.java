package com.realroofers.lovappy.service.lovstamps.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QLovstamp is a Querydsl query type for Lovstamp
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLovstamp extends EntityPathBase<Lovstamp> {

    private static final long serialVersionUID = 2088403299L;

    private static final PathInits INITS = new PathInits("*", "user.*.*");

    public static final QLovstamp lovstamp = new QLovstamp("lovstamp");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile audioFileCloud;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.system.model.QLanguage language;

    public final SetPath<com.realroofers.lovappy.service.user.model.User, com.realroofers.lovappy.service.user.model.QUser> listioners = this.<com.realroofers.lovappy.service.user.model.User, com.realroofers.lovappy.service.user.model.QUser>createSet("listioners", com.realroofers.lovappy.service.user.model.User.class, com.realroofers.lovappy.service.user.model.QUser.class, PathInits.DIRECT2);

    public final NumberPath<Integer> numberOfFemaleListeners = createNumber("numberOfFemaleListeners", Integer.class);

    public final NumberPath<Integer> numberOfListeners = createNumber("numberOfListeners", Integer.class);

    public final NumberPath<Integer> numberOfMaleListeners = createNumber("numberOfMaleListeners", Integer.class);

    public final NumberPath<Integer> numberOfUpdates = createNumber("numberOfUpdates", Integer.class);

    public final StringPath recordFile = createString("recordFile");

    public final NumberPath<Integer> recordSeconds = createNumber("recordSeconds", Integer.class);

    public final BooleanPath showInHome = createBoolean("showInHome");

    public final BooleanPath skip = createBoolean("skip");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final com.realroofers.lovappy.service.user.model.QUser user;

    public QLovstamp(String variable) {
        this(Lovstamp.class, forVariable(variable), INITS);
    }

    public QLovstamp(Path<? extends Lovstamp> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QLovstamp(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QLovstamp(PathMetadata metadata, PathInits inits) {
        this(Lovstamp.class, metadata, inits);
    }

    public QLovstamp(Class<? extends Lovstamp> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.audioFileCloud = inits.isInitialized("audioFileCloud") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("audioFileCloud"), inits.get("audioFileCloud")) : null;
        this.language = inits.isInitialized("language") ? new com.realroofers.lovappy.service.system.model.QLanguage(forProperty("language")) : null;
        this.user = inits.isInitialized("user") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("user"), inits.get("user")) : null;
    }

}

