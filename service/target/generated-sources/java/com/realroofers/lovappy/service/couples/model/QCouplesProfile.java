package com.realroofers.lovappy.service.couples.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCouplesProfile is a Querydsl query type for CouplesProfile
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCouplesProfile extends EntityPathBase<CouplesProfile> {

    private static final long serialVersionUID = 478670969L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCouplesProfile couplesProfile = new QCouplesProfile("couplesProfile");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final DateTimePath<java.util.Date> anniversaryDate = createDateTime("anniversaryDate", java.util.Date.class);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile couplePhotoImage;

    public final ListPath<com.realroofers.lovappy.service.user.model.UserProfile, com.realroofers.lovappy.service.user.model.QUserProfile> couples = this.<com.realroofers.lovappy.service.user.model.UserProfile, com.realroofers.lovappy.service.user.model.QUserProfile>createList("couples", com.realroofers.lovappy.service.user.model.UserProfile.class, com.realroofers.lovappy.service.user.model.QUserProfile.class, PathInits.DIRECT2);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final DateTimePath<java.util.Date> firstDate = createDateTime("firstDate", java.util.Date.class);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final EnumPath<com.realroofers.lovappy.service.user.support.ApprovalStatus> status = createEnum("status", com.realroofers.lovappy.service.user.support.ApprovalStatus.class);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QCouplesProfile(String variable) {
        this(CouplesProfile.class, forVariable(variable), INITS);
    }

    public QCouplesProfile(Path<? extends CouplesProfile> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCouplesProfile(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCouplesProfile(PathMetadata metadata, PathInits inits) {
        this(CouplesProfile.class, metadata, inits);
    }

    public QCouplesProfile(Class<? extends CouplesProfile> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.couplePhotoImage = inits.isInitialized("couplePhotoImage") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("couplePhotoImage"), inits.get("couplePhotoImage")) : null;
    }

}

