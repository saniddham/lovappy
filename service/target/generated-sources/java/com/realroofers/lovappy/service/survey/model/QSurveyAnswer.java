package com.realroofers.lovappy.service.survey.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSurveyAnswer is a Querydsl query type for SurveyAnswer
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSurveyAnswer extends EntityPathBase<SurveyAnswer> {

    private static final long serialVersionUID = -1021016754L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSurveyAnswer surveyAnswer = new QSurveyAnswer("surveyAnswer");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final QSurveyQuestion question;

    public final StringPath title = createString("title");

    public QSurveyAnswer(String variable) {
        this(SurveyAnswer.class, forVariable(variable), INITS);
    }

    public QSurveyAnswer(Path<? extends SurveyAnswer> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSurveyAnswer(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSurveyAnswer(PathMetadata metadata, PathInits inits) {
        this(SurveyAnswer.class, metadata, inits);
    }

    public QSurveyAnswer(Class<? extends SurveyAnswer> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.question = inits.isInitialized("question") ? new QSurveyQuestion(forProperty("question")) : null;
    }

}

