package com.realroofers.lovappy.service.career.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QEducation is a Querydsl query type for Education
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEducation extends EntityPathBase<Education> {

    private static final long serialVersionUID = -1198671370L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QEducation education = new QEducation("education");

    public final NumberPath<Integer> endMonth = createNumber("endMonth", Integer.class);

    public final NumberPath<Integer> endYear = createNumber("endYear", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath schoolType = createString("schoolType");

    public final NumberPath<Integer> startMonth = createNumber("startMonth", Integer.class);

    public final NumberPath<Integer> startYear = createNumber("startYear", Integer.class);

    public final QCareerUser user;

    public QEducation(String variable) {
        this(Education.class, forVariable(variable), INITS);
    }

    public QEducation(Path<? extends Education> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QEducation(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QEducation(PathMetadata metadata, PathInits inits) {
        this(Education.class, metadata, inits);
    }

    public QEducation(Class<? extends Education> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.user = inits.isInitialized("user") ? new QCareerUser(forProperty("user")) : null;
    }

}

