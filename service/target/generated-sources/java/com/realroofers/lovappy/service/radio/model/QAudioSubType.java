package com.realroofers.lovappy.service.radio.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAudioSubType is a Querydsl query type for AudioSubType
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAudioSubType extends EntityPathBase<AudioSubType> {

    private static final long serialVersionUID = 1786857759L;

    public static final QAudioSubType audioSubType = new QAudioSubType("audioSubType");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath description = createString("description");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath name = createString("name");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QAudioSubType(String variable) {
        super(AudioSubType.class, forVariable(variable));
    }

    public QAudioSubType(Path<? extends AudioSubType> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAudioSubType(PathMetadata metadata) {
        super(AudioSubType.class, metadata);
    }

}

