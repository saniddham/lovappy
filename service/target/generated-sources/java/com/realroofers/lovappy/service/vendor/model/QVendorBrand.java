package com.realroofers.lovappy.service.vendor.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QVendorBrand is a Querydsl query type for VendorBrand
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QVendorBrand extends EntityPathBase<VendorBrand> {

    private static final long serialVersionUID = -1626195657L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QVendorBrand vendorBrand = new QVendorBrand("vendorBrand");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    public final BooleanPath approved = createBoolean("approved");

    public final com.realroofers.lovappy.service.user.model.QUser approvedBy;

    public final com.realroofers.lovappy.service.product.model.QBrand brand;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final com.realroofers.lovappy.service.user.model.QUser createdBy;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final QVendor vendor;

    public QVendorBrand(String variable) {
        this(VendorBrand.class, forVariable(variable), INITS);
    }

    public QVendorBrand(Path<? extends VendorBrand> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QVendorBrand(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QVendorBrand(PathMetadata metadata, PathInits inits) {
        this(VendorBrand.class, metadata, inits);
    }

    public QVendorBrand(Class<? extends VendorBrand> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.approvedBy = inits.isInitialized("approvedBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("approvedBy"), inits.get("approvedBy")) : null;
        this.brand = inits.isInitialized("brand") ? new com.realroofers.lovappy.service.product.model.QBrand(forProperty("brand"), inits.get("brand")) : null;
        this.createdBy = inits.isInitialized("createdBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("createdBy"), inits.get("createdBy")) : null;
        this.vendor = inits.isInitialized("vendor") ? new QVendor(forProperty("vendor"), inits.get("vendor")) : null;
    }

}

