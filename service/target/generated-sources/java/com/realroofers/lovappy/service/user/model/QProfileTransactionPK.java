package com.realroofers.lovappy.service.user.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProfileTransactionPK is a Querydsl query type for ProfileTransactionPK
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QProfileTransactionPK extends BeanPath<ProfileTransactionPK> {

    private static final long serialVersionUID = -739299851L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProfileTransactionPK profileTransactionPK = new QProfileTransactionPK("profileTransactionPK");

    public final QUser userFrom;

    public final QUser userTo;

    public QProfileTransactionPK(String variable) {
        this(ProfileTransactionPK.class, forVariable(variable), INITS);
    }

    public QProfileTransactionPK(Path<? extends ProfileTransactionPK> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QProfileTransactionPK(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QProfileTransactionPK(PathMetadata metadata, PathInits inits) {
        this(ProfileTransactionPK.class, metadata, inits);
    }

    public QProfileTransactionPK(Class<? extends ProfileTransactionPK> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.userFrom = inits.isInitialized("userFrom") ? new QUser(forProperty("userFrom"), inits.get("userFrom")) : null;
        this.userTo = inits.isInitialized("userTo") ? new QUser(forProperty("userTo"), inits.get("userTo")) : null;
    }

}

