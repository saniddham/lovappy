package com.realroofers.lovappy.service.mail.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QEmailTemplateContent is a Querydsl query type for EmailTemplateContent
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEmailTemplateContent extends EntityPathBase<EmailTemplateContent> {

    private static final long serialVersionUID = 1526752700L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QEmailTemplateContent emailTemplateContent = new QEmailTemplateContent("emailTemplateContent");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final StringPath body = createString("body");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath htmlBody = createString("htmlBody");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath language = createString("language");

    public final StringPath subject = createString("subject");

    public final QEmailTemplate template;

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QEmailTemplateContent(String variable) {
        this(EmailTemplateContent.class, forVariable(variable), INITS);
    }

    public QEmailTemplateContent(Path<? extends EmailTemplateContent> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QEmailTemplateContent(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QEmailTemplateContent(PathMetadata metadata, PathInits inits) {
        this(EmailTemplateContent.class, metadata, inits);
    }

    public QEmailTemplateContent(Class<? extends EmailTemplateContent> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.template = inits.isInitialized("template") ? new QEmailTemplate(forProperty("template")) : null;
    }

}

