package com.realroofers.lovappy.service.vendor.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QVendor is a Querydsl query type for Vendor
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QVendor extends EntityPathBase<Vendor> {

    private static final long serialVersionUID = 1011204144L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QVendor vendor = new QVendor("vendor");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    public final StringPath addressLine1 = createString("addressLine1");

    public final StringPath addressLine2 = createString("addressLine2");

    public final StringPath alternateContactEmail = createString("alternateContactEmail");

    public final StringPath alternateContactPerson = createString("alternateContactPerson");

    public final BooleanPath approved = createBoolean("approved");

    public final StringPath city = createString("city");

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile companyLogo;

    public final StringPath companyName = createString("companyName");

    public final StringPath contactNumber = createString("contactNumber");

    public final com.realroofers.lovappy.service.user.model.QCountry country;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath mobileVerificationCode = createString("mobileVerificationCode");

    public final StringPath primaryContactEmail = createString("primaryContactEmail");

    public final StringPath primaryContactNumber = createString("primaryContactNumber");

    public final StringPath primaryContactPerson = createString("primaryContactPerson");

    public final BooleanPath registrationStep1 = createBoolean("registrationStep1");

    public final BooleanPath registrationStep2 = createBoolean("registrationStep2");

    public final StringPath state = createString("state");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final com.realroofers.lovappy.service.user.model.QUser user;

    public final EnumPath<VendorType> vendorType = createEnum("vendorType", VendorType.class);

    public final EnumPath<VerificationType> verificationType = createEnum("verificationType", VerificationType.class);

    public final StringPath zipCode = createString("zipCode");

    public QVendor(String variable) {
        this(Vendor.class, forVariable(variable), INITS);
    }

    public QVendor(Path<? extends Vendor> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QVendor(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QVendor(PathMetadata metadata, PathInits inits) {
        this(Vendor.class, metadata, inits);
    }

    public QVendor(Class<? extends Vendor> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.companyLogo = inits.isInitialized("companyLogo") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("companyLogo"), inits.get("companyLogo")) : null;
        this.country = inits.isInitialized("country") ? new com.realroofers.lovappy.service.user.model.QCountry(forProperty("country"), inits.get("country")) : null;
        this.user = inits.isInitialized("user") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("user"), inits.get("user")) : null;
    }

}

