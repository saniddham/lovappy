package com.realroofers.lovappy.service.datingPlaces.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPersonType is a Querydsl query type for PersonType
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPersonType extends EntityPathBase<PersonType> {

    private static final long serialVersionUID = 855559528L;

    public static final QPersonType personType = new QPersonType("personType");

    public final BooleanPath enabled = createBoolean("enabled");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath personTypeName = createString("personTypeName");

    public final ListPath<DatingPlacePersonTypes, QDatingPlacePersonTypes> personTypes = this.<DatingPlacePersonTypes, QDatingPlacePersonTypes>createList("personTypes", DatingPlacePersonTypes.class, QDatingPlacePersonTypes.class, PathInits.DIRECT2);

    public QPersonType(String variable) {
        super(PersonType.class, forVariable(variable));
    }

    public QPersonType(Path<? extends PersonType> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPersonType(PathMetadata metadata) {
        super(PersonType.class, metadata);
    }

}

