package com.realroofers.lovappy.service.news.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QNewsStory is a Querydsl query type for NewsStory
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QNewsStory extends EntityPathBase<NewsStory> {

    private static final long serialVersionUID = 1505808965L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QNewsStory newsStory = new QNewsStory("newsStory");

    public final StringPath audioVideoUrl = createString("audioVideoUrl");

    public final StringPath contain = createString("contain");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath imageDescription = createString("imageDescription");

    public final StringPath imageKeywords = createString("imageKeywords");

    public final QNewsAdvancedOption newsAdvancedOption;

    public final QNewsCategory newsCategory;

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile newsImage;

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile newsUserIdentification;

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile newsVideo;

    public final BooleanPath posted = createBoolean("posted");

    public final DateTimePath<java.util.Date> postedDate = createDateTime("postedDate", java.util.Date.class);

    public final StringPath source = createString("source");

    public final DateTimePath<java.util.Date> submitDate = createDateTime("submitDate", java.util.Date.class);

    public final StringPath title = createString("title");

    public QNewsStory(String variable) {
        this(NewsStory.class, forVariable(variable), INITS);
    }

    public QNewsStory(Path<? extends NewsStory> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QNewsStory(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QNewsStory(PathMetadata metadata, PathInits inits) {
        this(NewsStory.class, metadata, inits);
    }

    public QNewsStory(Class<? extends NewsStory> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.newsAdvancedOption = inits.isInitialized("newsAdvancedOption") ? new QNewsAdvancedOption(forProperty("newsAdvancedOption"), inits.get("newsAdvancedOption")) : null;
        this.newsCategory = inits.isInitialized("newsCategory") ? new QNewsCategory(forProperty("newsCategory")) : null;
        this.newsImage = inits.isInitialized("newsImage") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("newsImage"), inits.get("newsImage")) : null;
        this.newsUserIdentification = inits.isInitialized("newsUserIdentification") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("newsUserIdentification"), inits.get("newsUserIdentification")) : null;
        this.newsVideo = inits.isInitialized("newsVideo") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("newsVideo"), inits.get("newsVideo")) : null;
    }

}

