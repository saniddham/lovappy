package com.realroofers.lovappy.service.cloud.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QImageThumbnail is a Querydsl query type for ImageThumbnail
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QImageThumbnail extends EntityPathBase<ImageThumbnail> {

    private static final long serialVersionUID = 465125714L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QImageThumbnail imageThumbnail = new QImageThumbnail("imageThumbnail");

    public final QCloudStorageFile file;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final QCloudStorageFile originalFile;

    public final EnumPath<com.realroofers.lovappy.service.cloud.support.ImageSize> size = createEnum("size", com.realroofers.lovappy.service.cloud.support.ImageSize.class);

    public QImageThumbnail(String variable) {
        this(ImageThumbnail.class, forVariable(variable), INITS);
    }

    public QImageThumbnail(Path<? extends ImageThumbnail> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QImageThumbnail(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QImageThumbnail(PathMetadata metadata, PathInits inits) {
        this(ImageThumbnail.class, metadata, inits);
    }

    public QImageThumbnail(Class<? extends ImageThumbnail> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.file = inits.isInitialized("file") ? new QCloudStorageFile(forProperty("file"), inits.get("file")) : null;
        this.originalFile = inits.isInitialized("originalFile") ? new QCloudStorageFile(forProperty("originalFile"), inits.get("originalFile")) : null;
    }

}

