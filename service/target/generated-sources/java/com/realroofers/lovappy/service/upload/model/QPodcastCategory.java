package com.realroofers.lovappy.service.upload.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPodcastCategory is a Querydsl query type for PodcastCategory
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPodcastCategory extends EntityPathBase<PodcastCategory> {

    private static final long serialVersionUID = 1637036595L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPodcastCategory podcastCategory = new QPodcastCategory("podcastCategory");

    public final StringPath catName = createString("catName");

    public final DateTimePath<java.util.Date> created = createDateTime("created", java.util.Date.class);

    public final com.realroofers.lovappy.service.user.model.QUser createdBy;

    public final StringPath description = createString("description");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public QPodcastCategory(String variable) {
        this(PodcastCategory.class, forVariable(variable), INITS);
    }

    public QPodcastCategory(Path<? extends PodcastCategory> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPodcastCategory(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPodcastCategory(PathMetadata metadata, PathInits inits) {
        this(PodcastCategory.class, metadata, inits);
    }

    public QPodcastCategory(Class<? extends PodcastCategory> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.createdBy = inits.isInitialized("createdBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("createdBy"), inits.get("createdBy")) : null;
    }

}

