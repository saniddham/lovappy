package com.realroofers.lovappy.service.event.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QEventUserId is a Querydsl query type for EventUserId
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QEventUserId extends BeanPath<EventUserId> {

    private static final long serialVersionUID = -418100540L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QEventUserId eventUserId = new QEventUserId("eventUserId");

    public final QEvent event;

    public final com.realroofers.lovappy.service.user.model.QUser user;

    public QEventUserId(String variable) {
        this(EventUserId.class, forVariable(variable), INITS);
    }

    public QEventUserId(Path<? extends EventUserId> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QEventUserId(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QEventUserId(PathMetadata metadata, PathInits inits) {
        this(EventUserId.class, metadata, inits);
    }

    public QEventUserId(Class<? extends EventUserId> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.event = inits.isInitialized("event") ? new QEvent(forProperty("event"), inits.get("event")) : null;
        this.user = inits.isInitialized("user") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("user"), inits.get("user")) : null;
    }

}

