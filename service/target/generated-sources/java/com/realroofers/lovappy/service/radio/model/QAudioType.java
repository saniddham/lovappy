package com.realroofers.lovappy.service.radio.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAudioType is a Querydsl query type for AudioType
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAudioType extends EntityPathBase<AudioType> {

    private static final long serialVersionUID = 1041866197L;

    public static final QAudioType audioType = new QAudioType("audioType");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath description = createString("description");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath name = createString("name");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QAudioType(String variable) {
        super(AudioType.class, forVariable(variable));
    }

    public QAudioType(Path<? extends AudioType> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAudioType(PathMetadata metadata) {
        super(AudioType.class, metadata);
    }

}

