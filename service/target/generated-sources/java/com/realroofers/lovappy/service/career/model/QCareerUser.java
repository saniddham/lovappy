package com.realroofers.lovappy.service.career.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCareerUser is a Querydsl query type for CareerUser
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCareerUser extends EntityPathBase<CareerUser> {

    private static final long serialVersionUID = -325029925L;

    public static final QCareerUser careerUser = new QCareerUser("careerUser");

    public final StringPath address1 = createString("address1");

    public final StringPath address2 = createString("address2");

    public final BooleanPath agree = createBoolean("agree");

    public final BooleanPath authorizedInUs = createBoolean("authorizedInUs");

    public final NumberPath<Integer> birthDate = createNumber("birthDate", Integer.class);

    public final NumberPath<Integer> birthMonth = createNumber("birthMonth", Integer.class);

    public final NumberPath<Integer> birthYear = createNumber("birthYear", Integer.class);

    public final StringPath city = createString("city");

    public final StringPath country = createString("country");

    public final StringPath description = createString("description");

    public final SetPath<Education, QEducation> educations = this.<Education, QEducation>createSet("educations", Education.class, QEducation.class, PathInits.DIRECT2);

    public final StringPath email = createString("email");

    public final SetPath<Employment, QEmployment> employments = this.<Employment, QEmployment>createSet("employments", Employment.class, QEmployment.class, PathInits.DIRECT2);

    public final NumberPath<Integer> ethnicity = createNumber("ethnicity", Integer.class);

    public final StringPath firstName = createString("firstName");

    public final NumberPath<Integer> gender = createNumber("gender", Integer.class);

    public final StringPath hobbies = createString("hobbies");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath lastName = createString("lastName");

    public final StringPath phoneNumber = createString("phoneNumber");

    public final StringPath state = createString("state");

    public final SetPath<Vacancy, QVacancy> vacancies = this.<Vacancy, QVacancy>createSet("vacancies", Vacancy.class, QVacancy.class, PathInits.DIRECT2);

    public final NumberPath<Integer> zipCode = createNumber("zipCode", Integer.class);

    public QCareerUser(String variable) {
        super(CareerUser.class, forVariable(variable));
    }

    public QCareerUser(Path<? extends CareerUser> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCareerUser(PathMetadata metadata) {
        super(CareerUser.class, metadata);
    }

}

