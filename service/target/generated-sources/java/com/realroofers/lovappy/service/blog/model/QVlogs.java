package com.realroofers.lovappy.service.blog.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QVlogs is a Querydsl query type for Vlogs
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QVlogs extends EntityPathBase<Vlogs> {

    private static final long serialVersionUID = -641617065L;

    public static final QVlogs vlogs = new QVlogs("vlogs");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final StringPath captions = createString("captions");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath keywords = createString("keywords");

    public final EnumPath<com.realroofers.lovappy.service.blog.support.PostStatus> state = createEnum("state", com.realroofers.lovappy.service.blog.support.PostStatus.class);

    public final StringPath title = createString("title");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final StringPath videoId = createString("videoId");

    public final StringPath videoLink = createString("videoLink");

    public final EnumPath<com.realroofers.lovappy.service.core.VideoProvider> videoProvider = createEnum("videoProvider", com.realroofers.lovappy.service.core.VideoProvider.class);

    public final NumberPath<Integer> viewCount = createNumber("viewCount", Integer.class);

    public QVlogs(String variable) {
        super(Vlogs.class, forVariable(variable));
    }

    public QVlogs(Path<? extends Vlogs> path) {
        super(path.getType(), path.getMetadata());
    }

    public QVlogs(PathMetadata metadata) {
        super(Vlogs.class, metadata);
    }

}

