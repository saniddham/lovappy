package com.realroofers.lovappy.service.notification.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUserNotificationPK is a Querydsl query type for UserNotificationPK
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QUserNotificationPK extends BeanPath<UserNotificationPK> {

    private static final long serialVersionUID = -1021840458L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QUserNotificationPK userNotificationPK = new QUserNotificationPK("userNotificationPK");

    public final QNotification notification;

    public final com.realroofers.lovappy.service.user.model.QUser user;

    public QUserNotificationPK(String variable) {
        this(UserNotificationPK.class, forVariable(variable), INITS);
    }

    public QUserNotificationPK(Path<? extends UserNotificationPK> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QUserNotificationPK(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QUserNotificationPK(PathMetadata metadata, PathInits inits) {
        this(UserNotificationPK.class, metadata, inits);
    }

    public QUserNotificationPK(Class<? extends UserNotificationPK> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.notification = inits.isInitialized("notification") ? new QNotification(forProperty("notification")) : null;
        this.user = inits.isInitialized("user") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("user"), inits.get("user")) : null;
    }

}

