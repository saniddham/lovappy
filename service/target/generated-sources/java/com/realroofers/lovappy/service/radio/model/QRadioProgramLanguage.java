package com.realroofers.lovappy.service.radio.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QRadioProgramLanguage is a Querydsl query type for RadioProgramLanguage
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRadioProgramLanguage extends EntityPathBase<RadioProgramLanguage> {

    private static final long serialVersionUID = 13516220L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QRadioProgramLanguage radioProgramLanguage = new QRadioProgramLanguage("radioProgramLanguage");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.system.model.QLanguage language;

    public final QRadioProgramControl radioProgramControl;

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QRadioProgramLanguage(String variable) {
        this(RadioProgramLanguage.class, forVariable(variable), INITS);
    }

    public QRadioProgramLanguage(Path<? extends RadioProgramLanguage> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QRadioProgramLanguage(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QRadioProgramLanguage(PathMetadata metadata, PathInits inits) {
        this(RadioProgramLanguage.class, metadata, inits);
    }

    public QRadioProgramLanguage(Class<? extends RadioProgramLanguage> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.language = inits.isInitialized("language") ? new com.realroofers.lovappy.service.system.model.QLanguage(forProperty("language")) : null;
        this.radioProgramControl = inits.isInitialized("radioProgramControl") ? new QRadioProgramControl(forProperty("radioProgramControl"), inits.get("radioProgramControl")) : null;
    }

}

