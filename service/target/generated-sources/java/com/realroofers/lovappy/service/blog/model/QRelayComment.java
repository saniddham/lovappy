package com.realroofers.lovappy.service.blog.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QRelayComment is a Querydsl query type for RelayComment
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRelayComment extends EntityPathBase<RelayComment> {

    private static final long serialVersionUID = 514611740L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QRelayComment relayComment = new QRelayComment("relayComment");

    public final QComment comment;

    public final StringPath content = createString("content");

    public final DateTimePath<java.util.Date> date = createDateTime("date", java.util.Date.class);

    public final NumberPath<Integer> ID = createNumber("ID", Integer.class);

    public QRelayComment(String variable) {
        this(RelayComment.class, forVariable(variable), INITS);
    }

    public QRelayComment(Path<? extends RelayComment> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QRelayComment(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QRelayComment(PathMetadata metadata, PathInits inits) {
        this(RelayComment.class, metadata, inits);
    }

    public QRelayComment(Class<? extends RelayComment> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.comment = inits.isInitialized("comment") ? new QComment(forProperty("comment"), inits.get("comment")) : null;
    }

}

