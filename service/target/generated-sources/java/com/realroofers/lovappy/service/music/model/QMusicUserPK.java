package com.realroofers.lovappy.service.music.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMusicUserPK is a Querydsl query type for MusicUserPK
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QMusicUserPK extends BeanPath<MusicUserPK> {

    private static final long serialVersionUID = 2011791770L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMusicUserPK musicUserPK = new QMusicUserPK("musicUserPK");

    public final QMusic music;

    public final com.realroofers.lovappy.service.user.model.QUser user;

    public QMusicUserPK(String variable) {
        this(MusicUserPK.class, forVariable(variable), INITS);
    }

    public QMusicUserPK(Path<? extends MusicUserPK> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QMusicUserPK(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QMusicUserPK(PathMetadata metadata, PathInits inits) {
        this(MusicUserPK.class, metadata, inits);
    }

    public QMusicUserPK(Class<? extends MusicUserPK> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.music = inits.isInitialized("music") ? new QMusic(forProperty("music"), inits.get("music")) : null;
        this.user = inits.isInitialized("user") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("user"), inits.get("user")) : null;
    }

}

