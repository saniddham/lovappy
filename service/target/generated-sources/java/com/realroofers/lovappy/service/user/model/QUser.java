package com.realroofers.lovappy.service.user.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUser is a Querydsl query type for User
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUser extends EntityPathBase<User> {

    private static final long serialVersionUID = -211335792L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QUser user = new QUser("user");

    public final BooleanPath activated = createBoolean("activated");

    public final BooleanPath banned = createBoolean("banned");

    public final DateTimePath<java.util.Date> createdOn = createDateTime("createdOn", java.util.Date.class);

    public final StringPath email = createString("email");

    public final BooleanPath emailNotifications = createBoolean("emailNotifications");

    public final StringPath emailVerificationKey = createString("emailVerificationKey");

    public final BooleanPath emailVerified = createBoolean("emailVerified");

    public final BooleanPath enabled = createBoolean("enabled");

    public final ListPath<com.realroofers.lovappy.service.event.model.EventUserPicture, com.realroofers.lovappy.service.event.model.QEventUserPicture> eventPictures = this.<com.realroofers.lovappy.service.event.model.EventUserPicture, com.realroofers.lovappy.service.event.model.QEventUserPicture>createList("eventPictures", com.realroofers.lovappy.service.event.model.EventUserPicture.class, com.realroofers.lovappy.service.event.model.QEventUserPicture.class, PathInits.DIRECT2);

    public final StringPath firstName = createString("firstName");

    public final StringPath invitedBy = createString("invitedBy");

    public final BooleanPath isTweeted = createBoolean("isTweeted");

    public final DateTimePath<java.util.Date> lastLogin = createDateTime("lastLogin", java.util.Date.class);

    public final StringPath lastName = createString("lastName");

    public final SetPath<com.realroofers.lovappy.service.likes.model.Like, com.realroofers.lovappy.service.likes.model.QLike> likeBySet = this.<com.realroofers.lovappy.service.likes.model.Like, com.realroofers.lovappy.service.likes.model.QLike>createSet("likeBySet", com.realroofers.lovappy.service.likes.model.Like.class, com.realroofers.lovappy.service.likes.model.QLike.class, PathInits.DIRECT2);

    public final NumberPath<Long> likeMeCount = createNumber("likeMeCount", Long.class);

    public final SetPath<com.realroofers.lovappy.service.likes.model.Like, com.realroofers.lovappy.service.likes.model.QLike> likeToSet = this.<com.realroofers.lovappy.service.likes.model.Like, com.realroofers.lovappy.service.likes.model.QLike>createSet("likeToSet", com.realroofers.lovappy.service.likes.model.Like.class, com.realroofers.lovappy.service.likes.model.QLike.class, PathInits.DIRECT2);

    public final SetPath<com.realroofers.lovappy.service.lovstamps.model.Lovstamp, com.realroofers.lovappy.service.lovstamps.model.QLovstamp> listenLovstampSet = this.<com.realroofers.lovappy.service.lovstamps.model.Lovstamp, com.realroofers.lovappy.service.lovstamps.model.QLovstamp>createSet("listenLovstampSet", com.realroofers.lovappy.service.lovstamps.model.Lovstamp.class, com.realroofers.lovappy.service.lovstamps.model.QLovstamp.class, PathInits.DIRECT2);

    public final NumberPath<Long> loginsCount = createNumber("loginsCount", Long.class);

    public final NumberPath<Long> matchesCount = createNumber("matchesCount", Long.class);

    public final BooleanPath mobileVerified = createBoolean("mobileVerified");

    public final DateTimePath<java.util.Date> modifiedOn = createDateTime("modifiedOn", java.util.Date.class);

    public final NumberPath<Long> myLikesCount = createNumber("myLikesCount", Long.class);

    public final ListPath<com.realroofers.lovappy.service.datingPlaces.model.DatingPlace, com.realroofers.lovappy.service.datingPlaces.model.QDatingPlace> ownedPlaces = this.<com.realroofers.lovappy.service.datingPlaces.model.DatingPlace, com.realroofers.lovappy.service.datingPlaces.model.QDatingPlace>createList("ownedPlaces", com.realroofers.lovappy.service.datingPlaces.model.DatingPlace.class, com.realroofers.lovappy.service.datingPlaces.model.QDatingPlace.class, PathInits.DIRECT2);

    public final StringPath password = createString("password");

    public final BooleanPath paused = createBoolean("paused");

    public final StringPath phoneNumber = createString("phoneNumber");

    public final ListPath<com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceClaim, com.realroofers.lovappy.service.datingPlaces.model.QDatingPlaceClaim> placeClaims = this.<com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceClaim, com.realroofers.lovappy.service.datingPlaces.model.QDatingPlaceClaim>createList("placeClaims", com.realroofers.lovappy.service.datingPlaces.model.DatingPlaceClaim.class, com.realroofers.lovappy.service.datingPlaces.model.QDatingPlaceClaim.class, PathInits.DIRECT2);

    public final SetPath<UserRoles, QUserRoles> roles = this.<UserRoles, QUserRoles>createSet("roles", UserRoles.class, QUserRoles.class, PathInits.DIRECT2);

    public final StringPath title = createString("title");

    public final BooleanPath tweeted = createBoolean("tweeted");

    public final NumberPath<Integer> userId = createNumber("userId", Integer.class);

    public final QUserPreference userPreference;

    public final QUserProfile userProfile;

    public final SetPath<com.realroofers.lovappy.service.notification.model.UserNotification, com.realroofers.lovappy.service.notification.model.QUserNotification> usersNotifications = this.<com.realroofers.lovappy.service.notification.model.UserNotification, com.realroofers.lovappy.service.notification.model.QUserNotification>createSet("usersNotifications", com.realroofers.lovappy.service.notification.model.UserNotification.class, com.realroofers.lovappy.service.notification.model.QUserNotification.class, PathInits.DIRECT2);

    public QUser(String variable) {
        this(User.class, forVariable(variable), INITS);
    }

    public QUser(Path<? extends User> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QUser(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QUser(PathMetadata metadata, PathInits inits) {
        this(User.class, metadata, inits);
    }

    public QUser(Class<? extends User> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.userPreference = inits.isInitialized("userPreference") ? new QUserPreference(forProperty("userPreference"), inits.get("userPreference")) : null;
        this.userProfile = inits.isInitialized("userProfile") ? new QUserProfile(forProperty("userProfile"), inits.get("userProfile")) : null;
    }

}

