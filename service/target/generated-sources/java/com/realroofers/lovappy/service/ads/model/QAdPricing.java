package com.realroofers.lovappy.service.ads.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdPricing is a Querydsl query type for AdPricing
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdPricing extends EntityPathBase<AdPricing> {

    private static final long serialVersionUID = 667315645L;

    public static final QAdPricing adPricing = new QAdPricing("adPricing");

    public final EnumPath<com.realroofers.lovappy.service.ads.support.AdType> adType = createEnum("adType", com.realroofers.lovappy.service.ads.support.AdType.class);

    public final NumberPath<Integer> duration = createNumber("duration", Integer.class);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final EnumPath<com.realroofers.lovappy.service.ads.support.AdMediaType> mediaType = createEnum("mediaType", com.realroofers.lovappy.service.ads.support.AdMediaType.class);

    public final NumberPath<Double> price = createNumber("price", Double.class);

    public QAdPricing(String variable) {
        super(AdPricing.class, forVariable(variable));
    }

    public QAdPricing(Path<? extends AdPricing> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdPricing(PathMetadata metadata) {
        super(AdPricing.class, metadata);
    }

}

