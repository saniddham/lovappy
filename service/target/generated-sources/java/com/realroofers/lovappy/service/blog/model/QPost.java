package com.realroofers.lovappy.service.blog.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPost is a Querydsl query type for Post
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPost extends EntityPathBase<Post> {

    private static final long serialVersionUID = 394768942L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPost post = new QPost("post");

    public final BooleanPath approved = createBoolean("approved");

    public final BooleanPath authorAnonymous = createBoolean("authorAnonymous");

    public final QCategory category;

    public final NumberPath<Integer> credit = createNumber("credit", Integer.class);

    public final StringPath description = createString("description");

    public final BooleanPath draft = createBoolean("draft");

    public final NumberPath<Integer> emailShares = createNumber("emailShares", Integer.class);

    public final NumberPath<Integer> facebookShares = createNumber("facebookShares", Integer.class);

    public final BooleanPath featured = createBoolean("featured");

    public final NumberPath<Integer> googleShares = createNumber("googleShares", Integer.class);

    public final StringPath htmlContent = createString("htmlContent");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath imagAlt = createString("imagAlt");

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile image;

    public final StringPath imageAlt = createString("imageAlt");

    public final BooleanPath isAuthorAnonymous = createBoolean("isAuthorAnonymous");

    public final StringPath keywords = createString("keywords");

    public final StringPath keywordsPhrases = createString("keywordsPhrases");

    public final NumberPath<Integer> linkedinShares = createNumber("linkedinShares", Integer.class);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile originalImage;

    public final NumberPath<Integer> pinterestShares = createNumber("pinterestShares", Integer.class);

    public final com.realroofers.lovappy.service.user.model.QUser postAuthor;

    public final StringPath postContent = createString("postContent");

    public final DateTimePath<java.util.Date> postDate = createDateTime("postDate", java.util.Date.class);

    public final StringPath postTitle = createString("postTitle");

    public final StringPath seoTitle = createString("seoTitle");

    public final StringPath slugUrl = createString("slugUrl");

    public final EnumPath<com.realroofers.lovappy.service.blog.support.PostStatus> state = createEnum("state", com.realroofers.lovappy.service.blog.support.PostStatus.class);

    public final NumberPath<Integer> twitterShares = createNumber("twitterShares", Integer.class);

    public final EnumPath<BlogTypes> type = createEnum("type", BlogTypes.class);

    public final StringPath videoLink = createString("videoLink");

    public final NumberPath<Integer> viewCount = createNumber("viewCount", Integer.class);

    public final BooleanPath writtenByAdmin = createBoolean("writtenByAdmin");

    public QPost(String variable) {
        this(Post.class, forVariable(variable), INITS);
    }

    public QPost(Path<? extends Post> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPost(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPost(PathMetadata metadata, PathInits inits) {
        this(Post.class, metadata, inits);
    }

    public QPost(Class<? extends Post> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.category = inits.isInitialized("category") ? new QCategory(forProperty("category"), inits.get("category")) : null;
        this.image = inits.isInitialized("image") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("image"), inits.get("image")) : null;
        this.originalImage = inits.isInitialized("originalImage") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("originalImage"), inits.get("originalImage")) : null;
        this.postAuthor = inits.isInitialized("postAuthor") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("postAuthor"), inits.get("postAuthor")) : null;
    }

}

