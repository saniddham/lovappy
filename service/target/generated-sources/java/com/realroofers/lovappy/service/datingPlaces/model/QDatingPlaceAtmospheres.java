package com.realroofers.lovappy.service.datingPlaces.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDatingPlaceAtmospheres is a Querydsl query type for DatingPlaceAtmospheres
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDatingPlaceAtmospheres extends EntityPathBase<DatingPlaceAtmospheres> {

    private static final long serialVersionUID = -782059826L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDatingPlaceAtmospheres datingPlaceAtmospheres = new QDatingPlaceAtmospheres("datingPlaceAtmospheres");

    public final QPlaceAtmosphere atmosphere;

    public final QDatingPlace placeId;

    public QDatingPlaceAtmospheres(String variable) {
        this(DatingPlaceAtmospheres.class, forVariable(variable), INITS);
    }

    public QDatingPlaceAtmospheres(Path<? extends DatingPlaceAtmospheres> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QDatingPlaceAtmospheres(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QDatingPlaceAtmospheres(PathMetadata metadata, PathInits inits) {
        this(DatingPlaceAtmospheres.class, metadata, inits);
    }

    public QDatingPlaceAtmospheres(Class<? extends DatingPlaceAtmospheres> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.atmosphere = inits.isInitialized("atmosphere") ? new QPlaceAtmosphere(forProperty("atmosphere")) : null;
        this.placeId = inits.isInitialized("placeId") ? new QDatingPlace(forProperty("placeId"), inits.get("placeId")) : null;
    }

}

