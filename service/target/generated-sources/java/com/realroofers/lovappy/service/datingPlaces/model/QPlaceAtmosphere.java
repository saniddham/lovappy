package com.realroofers.lovappy.service.datingPlaces.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPlaceAtmosphere is a Querydsl query type for PlaceAtmosphere
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPlaceAtmosphere extends EntityPathBase<PlaceAtmosphere> {

    private static final long serialVersionUID = 729067504L;

    public static final QPlaceAtmosphere placeAtmosphere = new QPlaceAtmosphere("placeAtmosphere");

    public final StringPath atmosphereName = createString("atmosphereName");

    public final BooleanPath enabled = createBoolean("enabled");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final ListPath<DatingPlaceAtmospheres, QDatingPlaceAtmospheres> placeAtmospheres = this.<DatingPlaceAtmospheres, QDatingPlaceAtmospheres>createList("placeAtmospheres", DatingPlaceAtmospheres.class, QDatingPlaceAtmospheres.class, PathInits.DIRECT2);

    public QPlaceAtmosphere(String variable) {
        super(PlaceAtmosphere.class, forVariable(variable));
    }

    public QPlaceAtmosphere(Path<? extends PlaceAtmosphere> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPlaceAtmosphere(PathMetadata metadata) {
        super(PlaceAtmosphere.class, metadata);
    }

}

