package com.realroofers.lovappy.service.career.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QEmployment is a Querydsl query type for Employment
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEmployment extends EntityPathBase<Employment> {

    private static final long serialVersionUID = 2087689054L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QEmployment employment = new QEmployment("employment");

    public final StringPath businessName = createString("businessName");

    public final NumberPath<Integer> endMonth = createNumber("endMonth", Integer.class);

    public final NumberPath<Integer> endYear = createNumber("endYear", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath jobTitle = createString("jobTitle");

    public final NumberPath<Integer> reasonForLeaving = createNumber("reasonForLeaving", Integer.class);

    public final StringPath resposibilities = createString("resposibilities");

    public final NumberPath<Integer> startMonth = createNumber("startMonth", Integer.class);

    public final NumberPath<Integer> startYear = createNumber("startYear", Integer.class);

    public final NumberPath<Integer> stillWorking = createNumber("stillWorking", Integer.class);

    public final QCareerUser user;

    public QEmployment(String variable) {
        this(Employment.class, forVariable(variable), INITS);
    }

    public QEmployment(Path<? extends Employment> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QEmployment(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QEmployment(PathMetadata metadata, PathInits inits) {
        this(Employment.class, metadata, inits);
    }

    public QEmployment(Class<? extends Employment> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.user = inits.isInitialized("user") ? new QCareerUser(forProperty("user")) : null;
    }

}

