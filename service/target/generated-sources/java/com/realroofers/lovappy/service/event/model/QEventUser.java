package com.realroofers.lovappy.service.event.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QEventUser is a Querydsl query type for EventUser
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEventUser extends EntityPathBase<EventUser> {

    private static final long serialVersionUID = -921104439L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QEventUser eventUser = new QEventUser("eventUser");

    public final DateTimePath<java.util.Date> cancellationDate = createDateTime("cancellationDate", java.util.Date.class);

    public final QEventUserId id;

    public final BooleanPath isPaymentMade = createBoolean("isPaymentMade");

    public final BooleanPath paymentMade = createBoolean("paymentMade");

    public final DateTimePath<java.util.Date> registrationDate = createDateTime("registrationDate", java.util.Date.class);

    public final EnumPath<com.realroofers.lovappy.service.event.support.UserEventStatus> status = createEnum("status", com.realroofers.lovappy.service.event.support.UserEventStatus.class);

    public QEventUser(String variable) {
        this(EventUser.class, forVariable(variable), INITS);
    }

    public QEventUser(Path<? extends EventUser> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QEventUser(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QEventUser(PathMetadata metadata, PathInits inits) {
        this(EventUser.class, metadata, inits);
    }

    public QEventUser(Class<? extends EventUser> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QEventUserId(forProperty("id"), inits.get("id")) : null;
    }

}

