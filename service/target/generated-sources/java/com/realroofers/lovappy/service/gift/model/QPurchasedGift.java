package com.realroofers.lovappy.service.gift.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPurchasedGift is a Querydsl query type for PurchasedGift
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPurchasedGift extends EntityPathBase<PurchasedGift> {

    private static final long serialVersionUID = -895243245L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPurchasedGift purchasedGift = new QPurchasedGift("purchasedGift");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.product.model.QProduct product;

    public final DateTimePath<java.util.Date> purchasedOn = createDateTime("purchasedOn", java.util.Date.class);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final com.realroofers.lovappy.service.user.model.QUser user;

    public QPurchasedGift(String variable) {
        this(PurchasedGift.class, forVariable(variable), INITS);
    }

    public QPurchasedGift(Path<? extends PurchasedGift> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPurchasedGift(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPurchasedGift(PathMetadata metadata, PathInits inits) {
        this(PurchasedGift.class, metadata, inits);
    }

    public QPurchasedGift(Class<? extends PurchasedGift> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.product = inits.isInitialized("product") ? new com.realroofers.lovappy.service.product.model.QProduct(forProperty("product"), inits.get("product")) : null;
        this.user = inits.isInitialized("user") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("user"), inits.get("user")) : null;
    }

}

