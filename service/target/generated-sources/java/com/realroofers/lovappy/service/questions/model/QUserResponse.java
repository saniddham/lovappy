package com.realroofers.lovappy.service.questions.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUserResponse is a Querydsl query type for UserResponse
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUserResponse extends EntityPathBase<UserResponse> {

    private static final long serialVersionUID = -561560235L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QUserResponse userResponse = new QUserResponse("userResponse");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile audioResponse;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final com.realroofers.lovappy.service.system.model.QLanguage language;

    public final QDailyQuestion question;

    public final NumberPath<Integer> recordSeconds = createNumber("recordSeconds", Integer.class);

    public final EnumPath<com.realroofers.lovappy.service.user.support.ApprovalStatus> responseApprovalStatus = createEnum("responseApprovalStatus", com.realroofers.lovappy.service.user.support.ApprovalStatus.class);

    public final DateTimePath<java.util.Date> responseDate = createDateTime("responseDate", java.util.Date.class);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final com.realroofers.lovappy.service.user.model.QUser user;

    public QUserResponse(String variable) {
        this(UserResponse.class, forVariable(variable), INITS);
    }

    public QUserResponse(Path<? extends UserResponse> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QUserResponse(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QUserResponse(PathMetadata metadata, PathInits inits) {
        this(UserResponse.class, metadata, inits);
    }

    public QUserResponse(Class<? extends UserResponse> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.audioResponse = inits.isInitialized("audioResponse") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("audioResponse"), inits.get("audioResponse")) : null;
        this.language = inits.isInitialized("language") ? new com.realroofers.lovappy.service.system.model.QLanguage(forProperty("language")) : null;
        this.question = inits.isInitialized("question") ? new QDailyQuestion(forProperty("question")) : null;
        this.user = inits.isInitialized("user") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("user"), inits.get("user")) : null;
    }

}

