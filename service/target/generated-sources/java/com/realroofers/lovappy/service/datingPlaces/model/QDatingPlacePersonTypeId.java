package com.realroofers.lovappy.service.datingPlaces.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDatingPlacePersonTypeId is a Querydsl query type for DatingPlacePersonTypeId
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QDatingPlacePersonTypeId extends BeanPath<DatingPlacePersonTypeId> {

    private static final long serialVersionUID = 18750989L;

    public static final QDatingPlacePersonTypeId datingPlacePersonTypeId = new QDatingPlacePersonTypeId("datingPlacePersonTypeId");

    public final NumberPath<Integer> datingPlaceId = createNumber("datingPlaceId", Integer.class);

    public final NumberPath<Integer> personTypeId = createNumber("personTypeId", Integer.class);

    public QDatingPlacePersonTypeId(String variable) {
        super(DatingPlacePersonTypeId.class, forVariable(variable));
    }

    public QDatingPlacePersonTypeId(Path<? extends DatingPlacePersonTypeId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDatingPlacePersonTypeId(PathMetadata metadata) {
        super(DatingPlacePersonTypeId.class, metadata);
    }

}

