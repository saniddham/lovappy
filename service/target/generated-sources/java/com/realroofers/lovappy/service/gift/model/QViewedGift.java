package com.realroofers.lovappy.service.gift.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QViewedGift is a Querydsl query type for ViewedGift
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QViewedGift extends EntityPathBase<ViewedGift> {

    private static final long serialVersionUID = -673711980L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QViewedGift viewedGift = new QViewedGift("viewedGift");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.product.model.QProduct product;

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final com.realroofers.lovappy.service.user.model.QUser user;

    public final DateTimePath<java.util.Date> viewedOn = createDateTime("viewedOn", java.util.Date.class);

    public QViewedGift(String variable) {
        this(ViewedGift.class, forVariable(variable), INITS);
    }

    public QViewedGift(Path<? extends ViewedGift> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QViewedGift(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QViewedGift(PathMetadata metadata, PathInits inits) {
        this(ViewedGift.class, metadata, inits);
    }

    public QViewedGift(Class<? extends ViewedGift> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.product = inits.isInitialized("product") ? new com.realroofers.lovappy.service.product.model.QProduct(forProperty("product"), inits.get("product")) : null;
        this.user = inits.isInitialized("user") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("user"), inits.get("user")) : null;
    }

}

