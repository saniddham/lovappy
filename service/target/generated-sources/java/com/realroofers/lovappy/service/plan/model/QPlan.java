package com.realroofers.lovappy.service.plan.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPlan is a Querydsl query type for Plan
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPlan extends EntityPathBase<Plan> {

    private static final long serialVersionUID = 741908176L;

    public static final QPlan plan = new QPlan("plan");

    public final StringPath colorScheme = createString("colorScheme");

    public final NumberPath<Integer> cost = createNumber("cost", Integer.class);

    public final StringPath costDescription = createString("costDescription");

    public final BooleanPath isValid = createBoolean("isValid");

    public final CollectionPath<Feature, QFeature> planFeatures = this.<Feature, QFeature>createCollection("planFeatures", Feature.class, QFeature.class, PathInits.DIRECT2);

    public final NumberPath<Integer> planID = createNumber("planID", Integer.class);

    public final StringPath planName = createString("planName");

    public final StringPath planSubtitle = createString("planSubtitle");

    public QPlan(String variable) {
        super(Plan.class, forVariable(variable));
    }

    public QPlan(Path<? extends Plan> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPlan(PathMetadata metadata) {
        super(Plan.class, metadata);
    }

}

