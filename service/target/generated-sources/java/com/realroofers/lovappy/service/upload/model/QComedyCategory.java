package com.realroofers.lovappy.service.upload.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QComedyCategory is a Querydsl query type for ComedyCategory
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QComedyCategory extends EntityPathBase<ComedyCategory> {

    private static final long serialVersionUID = 880388742L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QComedyCategory comedyCategory = new QComedyCategory("comedyCategory");

    public final StringPath catName = createString("catName");

    public final DateTimePath<java.util.Date> created = createDateTime("created", java.util.Date.class);

    public final com.realroofers.lovappy.service.user.model.QUser createdBy;

    public final StringPath description = createString("description");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public QComedyCategory(String variable) {
        this(ComedyCategory.class, forVariable(variable), INITS);
    }

    public QComedyCategory(Path<? extends ComedyCategory> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QComedyCategory(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QComedyCategory(PathMetadata metadata, PathInits inits) {
        this(ComedyCategory.class, metadata, inits);
    }

    public QComedyCategory(Class<? extends ComedyCategory> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.createdBy = inits.isInitialized("createdBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("createdBy"), inits.get("createdBy")) : null;
    }

}

