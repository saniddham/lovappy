package com.realroofers.lovappy.service.notification.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QNotification is a Querydsl query type for Notification
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QNotification extends EntityPathBase<Notification> {

    private static final long serialVersionUID = 1722486672L;

    public static final QNotification notification = new QNotification("notification");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final StringPath content = createString("content");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> sourceId = createNumber("sourceId", Long.class);

    public final EnumPath<NotificationTypes> type = createEnum("type", NotificationTypes.class);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final NumberPath<Integer> userId = createNumber("userId", Integer.class);

    public final SetPath<UserNotification, QUserNotification> usersNotifications = this.<UserNotification, QUserNotification>createSet("usersNotifications", UserNotification.class, QUserNotification.class, PathInits.DIRECT2);

    public QNotification(String variable) {
        super(Notification.class, forVariable(variable));
    }

    public QNotification(Path<? extends Notification> path) {
        super(path.getType(), path.getMetadata());
    }

    public QNotification(PathMetadata metadata) {
        super(Notification.class, metadata);
    }

}

