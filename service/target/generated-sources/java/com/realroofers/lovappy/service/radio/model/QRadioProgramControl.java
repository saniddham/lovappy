package com.realroofers.lovappy.service.radio.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QRadioProgramControl is a Querydsl query type for RadioProgramControl
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRadioProgramControl extends EntityPathBase<RadioProgramControl> {

    private static final long serialVersionUID = -2044010887L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QRadioProgramControl radioProgramControl = new QRadioProgramControl("radioProgramControl");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    public final BooleanPath ageAbove50 = createBoolean("ageAbove50");

    public final BooleanPath ageBetween18And24 = createBoolean("ageBetween18And24");

    public final BooleanPath ageBetween25And34 = createBoolean("ageBetween25And34");

    public final BooleanPath ageBetween35And49 = createBoolean("ageBetween35And49");

    public final EnumPath<com.realroofers.lovappy.service.radio.support.AudioAction> audioAction = createEnum("audioAction", com.realroofers.lovappy.service.radio.support.AudioAction.class);

    public final QAudioSubType audioSubType;

    public final QAudioType audioType;

    public final NumberPath<Integer> contentAfter = createNumber("contentAfter", Integer.class);

    public final NumberPath<Integer> contentBefore = createNumber("contentBefore", Integer.class);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> dailyLimit = createNumber("dailyLimit", Integer.class);

    public final StringPath gender = createString("gender");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final EnumPath<com.realroofers.lovappy.service.radio.support.AudioInsertWhere> insertWhere = createEnum("insertWhere", com.realroofers.lovappy.service.radio.support.AudioInsertWhere.class);

    public final NumberPath<Integer> lifetimeLimit = createNumber("lifetimeLimit", Integer.class);

    public final QMediaType mediaType;

    public final BooleanPath playNext = createBoolean("playNext");

    public final BooleanPath playToAll = createBoolean("playToAll");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QRadioProgramControl(String variable) {
        this(RadioProgramControl.class, forVariable(variable), INITS);
    }

    public QRadioProgramControl(Path<? extends RadioProgramControl> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QRadioProgramControl(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QRadioProgramControl(PathMetadata metadata, PathInits inits) {
        this(RadioProgramControl.class, metadata, inits);
    }

    public QRadioProgramControl(Class<? extends RadioProgramControl> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.audioSubType = inits.isInitialized("audioSubType") ? new QAudioSubType(forProperty("audioSubType")) : null;
        this.audioType = inits.isInitialized("audioType") ? new QAudioType(forProperty("audioType")) : null;
        this.mediaType = inits.isInitialized("mediaType") ? new QMediaType(forProperty("mediaType")) : null;
    }

}

