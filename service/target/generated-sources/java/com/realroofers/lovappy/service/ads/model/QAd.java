package com.realroofers.lovappy.service.ads.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QAd is a Querydsl query type for Ad
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAd extends EntityPathBase<Ad> {

    private static final long serialVersionUID = -1530174007L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QAd ad = new QAd("ad");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final EnumPath<com.realroofers.lovappy.service.ads.support.AdType> adType = createEnum("adType", com.realroofers.lovappy.service.ads.support.AdType.class);

    public final EnumPath<com.realroofers.lovappy.service.ads.support.AgeRange> ageRange = createEnum("ageRange", com.realroofers.lovappy.service.ads.support.AgeRange.class);

    public final EnumPath<com.realroofers.lovappy.service.ads.support.AudioType> audioType = createEnum("audioType", com.realroofers.lovappy.service.ads.support.AudioType.class);

    public final StringPath backgroundColor = createString("backgroundColor");

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile backgroundFileCloud;

    public final StringPath coupon = createString("coupon");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath email = createString("email");

    public final BooleanPath featured = createBoolean("featured");

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile featuredAdBackgroundFileCloud;

    public final EnumPath<com.realroofers.lovappy.service.user.support.Gender> gender = createEnum("gender", com.realroofers.lovappy.service.user.support.Gender.class);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.system.model.QLanguage language;

    public final NumberPath<Integer> length = createNumber("length", Integer.class);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile mediaFileCloud;

    public final StringPath mediaFileUrl = createString("mediaFileUrl");

    public final EnumPath<com.realroofers.lovappy.service.ads.support.AdMediaType> mediaType = createEnum("mediaType", com.realroofers.lovappy.service.ads.support.AdMediaType.class);

    public final MapPath<String, String, StringPath> preferences = this.<String, String, StringPath>createMap("preferences", String.class, String.class, StringPath.class);

    public final NumberPath<Double> price = createNumber("price", Double.class);

    public final BooleanPath promote = createBoolean("promote");

    public final EnumPath<com.realroofers.lovappy.service.ads.support.AdStatus> status = createEnum("status", com.realroofers.lovappy.service.ads.support.AdStatus.class);

    public final ListPath<AdTargetArea, QAdTargetArea> targetAreas = this.<AdTargetArea, QAdTargetArea>createList("targetAreas", AdTargetArea.class, QAdTargetArea.class, PathInits.DIRECT2);

    public final StringPath text = createString("text");

    public final EnumPath<com.realroofers.lovappy.service.core.TextAlign> textAlign = createEnum("textAlign", com.realroofers.lovappy.service.core.TextAlign.class);

    public final StringPath textColor = createString("textColor");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final StringPath url = createString("url");

    public QAd(String variable) {
        this(Ad.class, forVariable(variable), INITS);
    }

    public QAd(Path<? extends Ad> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QAd(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QAd(PathMetadata metadata, PathInits inits) {
        this(Ad.class, metadata, inits);
    }

    public QAd(Class<? extends Ad> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.backgroundFileCloud = inits.isInitialized("backgroundFileCloud") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("backgroundFileCloud"), inits.get("backgroundFileCloud")) : null;
        this.featuredAdBackgroundFileCloud = inits.isInitialized("featuredAdBackgroundFileCloud") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("featuredAdBackgroundFileCloud"), inits.get("featuredAdBackgroundFileCloud")) : null;
        this.language = inits.isInitialized("language") ? new com.realroofers.lovappy.service.system.model.QLanguage(forProperty("language")) : null;
        this.mediaFileCloud = inits.isInitialized("mediaFileCloud") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("mediaFileCloud"), inits.get("mediaFileCloud")) : null;
    }

}

