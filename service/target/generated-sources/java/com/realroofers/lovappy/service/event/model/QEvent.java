package com.realroofers.lovappy.service.event.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QEvent is a Querydsl query type for Event
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEvent extends EntityPathBase<Event> {

    private static final long serialVersionUID = -1861267746L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QEvent event = new QEvent("event");

    public final NumberPath<Double> admissionCost = createNumber("admissionCost", Double.class);

    public final NumberPath<Integer> ambassadorUserId = createNumber("ambassadorUserId", Integer.class);

    public final BooleanPath approved = createBoolean("approved");

    public final NumberPath<Integer> attendeesLimit = createNumber("attendeesLimit", Integer.class);

    public final CollectionPath<EventComment, QEventComment> comments = this.<EventComment, QEventComment>createCollection("comments", EventComment.class, QEventComment.class, PathInits.DIRECT2);

    public final NumberPath<Integer> emailShares = createNumber("emailShares", Integer.class);

    public final EnumPath<com.realroofers.lovappy.service.user.support.ApprovalStatus> eventApprovalStatus = createEnum("eventApprovalStatus", com.realroofers.lovappy.service.user.support.ApprovalStatus.class);

    public final CollectionPath<EventCategory, QEventCategory> eventCategories = this.<EventCategory, QEventCategory>createCollection("eventCategories", EventCategory.class, QEventCategory.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> eventDate = createDateTime("eventDate", java.util.Date.class);

    public final StringPath eventDescription = createString("eventDescription");

    public final NumberPath<Integer> eventId = createNumber("eventId", Integer.class);

    public final com.realroofers.lovappy.service.event.model.embeddable.QEventLocation eventLocation;

    public final StringPath eventLocationDescription = createString("eventLocationDescription");

    public final ListPath<EventUserPicture, QEventUserPicture> eventPictures = this.<EventUserPicture, QEventUserPicture>createList("eventPictures", EventUserPicture.class, QEventUserPicture.class, PathInits.DIRECT2);

    public final EnumPath<com.realroofers.lovappy.service.event.support.EventStatus> eventStatus = createEnum("eventStatus", com.realroofers.lovappy.service.event.support.EventStatus.class);

    public final StringPath eventTitle = createString("eventTitle");

    public final SetPath<EventUser, QEventUser> eventUsers = this.<EventUser, QEventUser>createSet("eventUsers", EventUser.class, QEventUser.class, PathInits.DIRECT2);

    public final NumberPath<Integer> facebookShares = createNumber("facebookShares", Integer.class);

    public final DateTimePath<java.util.Date> finishTime = createDateTime("finishTime", java.util.Date.class);

    public final NumberPath<Integer> googleShares = createNumber("googleShares", Integer.class);

    public final NumberPath<Integer> linkedinShares = createNumber("linkedinShares", Integer.class);

    public final ListPath<EventLocationPicture, QEventLocationPicture> locationPictures = this.<EventLocationPicture, QEventLocationPicture>createList("locationPictures", EventLocationPicture.class, QEventLocationPicture.class, PathInits.DIRECT2);

    public final NumberPath<Integer> outsiderCount = createNumber("outsiderCount", Integer.class);

    public final NumberPath<Integer> pinterestShares = createNumber("pinterestShares", Integer.class);

    public final DateTimePath<java.util.Date> startTime = createDateTime("startTime", java.util.Date.class);

    public final MapPath<String, String, StringPath> targetedAudience = this.<String, String, StringPath>createMap("targetedAudience", String.class, String.class, StringPath.class);

    public final StringPath timeZone = createString("timeZone");

    public final NumberPath<Integer> twitterShares = createNumber("twitterShares", Integer.class);

    public QEvent(String variable) {
        this(Event.class, forVariable(variable), INITS);
    }

    public QEvent(Path<? extends Event> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QEvent(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QEvent(PathMetadata metadata, PathInits inits) {
        this(Event.class, metadata, inits);
    }

    public QEvent(Class<? extends Event> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.eventLocation = inits.isInitialized("eventLocation") ? new com.realroofers.lovappy.service.event.model.embeddable.QEventLocation(forProperty("eventLocation")) : null;
    }

}

