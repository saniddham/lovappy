package com.realroofers.lovappy.service.user.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSocialProfile is a Querydsl query type for SocialProfile
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSocialProfile extends EntityPathBase<SocialProfile> {

    private static final long serialVersionUID = -1160832681L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSocialProfile socialProfile = new QSocialProfile("socialProfile");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath firstName = createString("firstName");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath imageUrl = createString("imageUrl");

    public final StringPath lastName = createString("lastName");

    public final StringPath name = createString("name");

    public final StringPath socialId = createString("socialId");

    public final EnumPath<com.realroofers.lovappy.service.core.SocialType> socialPlatform = createEnum("socialPlatform", com.realroofers.lovappy.service.core.SocialType.class);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final QUser user;

    public QSocialProfile(String variable) {
        this(SocialProfile.class, forVariable(variable), INITS);
    }

    public QSocialProfile(Path<? extends SocialProfile> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSocialProfile(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSocialProfile(PathMetadata metadata, PathInits inits) {
        this(SocialProfile.class, metadata, inits);
    }

    public QSocialProfile(Class<? extends SocialProfile> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.user = inits.isInitialized("user") ? new QUser(forProperty("user"), inits.get("user")) : null;
    }

}

