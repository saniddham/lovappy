package com.realroofers.lovappy.service.user.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUserRolePK is a Querydsl query type for UserRolePK
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QUserRolePK extends BeanPath<UserRolePK> {

    private static final long serialVersionUID = -337835359L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QUserRolePK userRolePK = new QUserRolePK("userRolePK");

    public final QRole role;

    public final QUser user;

    public QUserRolePK(String variable) {
        this(UserRolePK.class, forVariable(variable), INITS);
    }

    public QUserRolePK(Path<? extends UserRolePK> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QUserRolePK(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QUserRolePK(PathMetadata metadata, PathInits inits) {
        this(UserRolePK.class, metadata, inits);
    }

    public QUserRolePK(Class<? extends UserRolePK> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.role = inits.isInitialized("role") ? new QRole(forProperty("role")) : null;
        this.user = inits.isInitialized("user") ? new QUser(forProperty("user"), inits.get("user")) : null;
    }

}

