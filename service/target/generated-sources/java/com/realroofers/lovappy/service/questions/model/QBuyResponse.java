package com.realroofers.lovappy.service.questions.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QBuyResponse is a Querydsl query type for BuyResponse
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QBuyResponse extends EntityPathBase<BuyResponse> {

    private static final long serialVersionUID = -610235490L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QBuyResponse buyResponse = new QBuyResponse("buyResponse");

    public final QBuyerId buyerId;

    public final com.realroofers.lovappy.service.user.model.QUser user;

    public final QUserResponse userResponse;

    public QBuyResponse(String variable) {
        this(BuyResponse.class, forVariable(variable), INITS);
    }

    public QBuyResponse(Path<? extends BuyResponse> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QBuyResponse(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QBuyResponse(PathMetadata metadata, PathInits inits) {
        this(BuyResponse.class, metadata, inits);
    }

    public QBuyResponse(Class<? extends BuyResponse> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.buyerId = inits.isInitialized("buyerId") ? new QBuyerId(forProperty("buyerId")) : null;
        this.user = inits.isInitialized("user") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("user"), inits.get("user")) : null;
        this.userResponse = inits.isInitialized("userResponse") ? new QUserResponse(forProperty("userResponse"), inits.get("userResponse")) : null;
    }

}

