package com.realroofers.lovappy.service.cms.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPageMetaData is a Querydsl query type for PageMetaData
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPageMetaData extends EntityPathBase<PageMetaData> {

    private static final long serialVersionUID = 1775095787L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPageMetaData pageMetaData = new QPageMetaData("pageMetaData");

    public final StringPath abstractDescription = createString("abstractDescription");

    public final StringPath description = createString("description");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath keywords = createString("keywords");

    public final com.realroofers.lovappy.service.system.model.QLanguage language;

    public final QPage page;

    public final StringPath title = createString("title");

    public QPageMetaData(String variable) {
        this(PageMetaData.class, forVariable(variable), INITS);
    }

    public QPageMetaData(Path<? extends PageMetaData> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPageMetaData(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPageMetaData(PathMetadata metadata, PathInits inits) {
        this(PageMetaData.class, metadata, inits);
    }

    public QPageMetaData(Class<? extends PageMetaData> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.language = inits.isInitialized("language") ? new com.realroofers.lovappy.service.system.model.QLanguage(forProperty("language")) : null;
        this.page = inits.isInitialized("page") ? new QPage(forProperty("page")) : null;
    }

}

