package com.realroofers.lovappy.service.datingPlaces.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDatingPlace is a Querydsl query type for DatingPlace
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDatingPlace extends EntityPathBase<DatingPlace> {

    private static final long serialVersionUID = 966996163L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDatingPlace datingPlace = new QDatingPlace("datingPlace");

    public final StringPath addressLine = createString("addressLine");

    public final StringPath addressLineTwo = createString("addressLineTwo");

    public final EnumPath<com.realroofers.lovappy.service.datingPlaces.support.DatingAgeRange> ageRange = createEnum("ageRange", com.realroofers.lovappy.service.datingPlaces.support.DatingAgeRange.class);

    public final BooleanPath approved = createBoolean("approved");

    public final NumberPath<Double> averageOverAllRatingByUser = createNumber("averageOverAllRatingByUser", Double.class);

    public final NumberPath<Double> averageRating = createNumber("averageRating", Double.class);

    public final StringPath city = createString("city");

    public final StringPath country = createString("country");

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile coverPicture;

    public final DateTimePath<java.util.Date> createdDate = createDateTime("createdDate", java.util.Date.class);

    public final StringPath createEmail = createString("createEmail");

    public final ListPath<DatingPlaceReview, QDatingPlaceReview> datingPlaceReviews = this.<DatingPlaceReview, QDatingPlaceReview>createList("datingPlaceReviews", DatingPlaceReview.class, QDatingPlaceReview.class, PathInits.DIRECT2);

    public final NumberPath<Integer> datingPlacesId = createNumber("datingPlacesId", Integer.class);

    public final DateTimePath<java.util.Date> deletedDate = createDateTime("deletedDate", java.util.Date.class);

    public final StringPath externalPlaceId = createString("externalPlaceId");

    public final ListPath<DatingPlaceFoodTypes, QDatingPlaceFoodTypes> foodTypes = this.<DatingPlaceFoodTypes, QDatingPlaceFoodTypes>createList("foodTypes", DatingPlaceFoodTypes.class, QDatingPlaceFoodTypes.class, PathInits.DIRECT2);

    public final EnumPath<com.realroofers.lovappy.service.user.support.Gender> gender = createEnum("gender", com.realroofers.lovappy.service.user.support.Gender.class);

    public final NumberPath<Double> googleAverage = createNumber("googleAverage", Double.class);

    public final BooleanPath isClaimed = createBoolean("isClaimed");

    public final BooleanPath isFeatured = createBoolean("isFeatured");

    public final BooleanPath isGoodForMarriedCouple = createBoolean("isGoodForMarriedCouple");

    public final BooleanPath isPetsAllowed = createBoolean("isPetsAllowed");

    public final NumberPath<Double> latitude = createNumber("latitude", Double.class);

    public final NumberPath<Double> longitude = createNumber("longitude", Double.class);

    public final BooleanPath loveStampFeatured = createBoolean("loveStampFeatured");

    public final BooleanPath mapFeatured = createBoolean("mapFeatured");

    public final EnumPath<com.realroofers.lovappy.service.datingPlaces.support.NeighborHood> neighborHood = createEnum("neighborHood", com.realroofers.lovappy.service.datingPlaces.support.NeighborHood.class);

    public final com.realroofers.lovappy.service.user.model.QUser owner;

    public final StringPath ownerContactNumber = createString("ownerContactNumber");

    public final StringPath ownerEmail = createString("ownerEmail");

    public final NumberPath<Double> parkingAverage = createNumber("parkingAverage", Double.class);

    public final ListPath<DatingPlacePersonTypes, QDatingPlacePersonTypes> personTypes = this.<DatingPlacePersonTypes, QDatingPlacePersonTypes>createList("personTypes", DatingPlacePersonTypes.class, QDatingPlacePersonTypes.class, PathInits.DIRECT2);

    public final EnumPath<com.realroofers.lovappy.service.user.support.ApprovalStatus> placeApprovalStatus = createEnum("placeApprovalStatus", com.realroofers.lovappy.service.user.support.ApprovalStatus.class);

    public final ListPath<DatingPlaceAtmospheres, QDatingPlaceAtmospheres> placeAtmospheres = this.<DatingPlaceAtmospheres, QDatingPlaceAtmospheres>createList("placeAtmospheres", DatingPlaceAtmospheres.class, QDatingPlaceAtmospheres.class, PathInits.DIRECT2);

    public final ListPath<DatingPlaceClaim, QDatingPlaceClaim> placeClaimList = this.<DatingPlaceClaim, QDatingPlaceClaim>createList("placeClaimList", DatingPlaceClaim.class, QDatingPlaceClaim.class, PathInits.DIRECT2);

    public final ListPath<DatingPlaceDeals, QDatingPlaceDeals> placeDeals = this.<DatingPlaceDeals, QDatingPlaceDeals>createList("placeDeals", DatingPlaceDeals.class, QDatingPlaceDeals.class, PathInits.DIRECT2);

    public final StringPath placeDescription = createString("placeDescription");

    public final StringPath placeName = createString("placeName");

    public final ListPath<PlacesAttachment, QPlacesAttachment> placesAttachments = this.<PlacesAttachment, QPlacesAttachment>createList("placesAttachments", PlacesAttachment.class, QPlacesAttachment.class, PathInits.DIRECT2);

    public final EnumPath<com.realroofers.lovappy.service.datingPlaces.support.PriceRange> priceRange = createEnum("priceRange", com.realroofers.lovappy.service.datingPlaces.support.PriceRange.class);

    public final EnumPath<com.realroofers.lovappy.service.datingPlaces.support.DatingPlaceProvider> provider = createEnum("provider", com.realroofers.lovappy.service.datingPlaces.support.DatingPlaceProvider.class);

    public final NumberPath<Double> securityAverage = createNumber("securityAverage", Double.class);

    public final StringPath state = createString("state");

    public final NumberPath<Integer> suitableForBoth = createNumber("suitableForBoth", Integer.class);

    public final NumberPath<Integer> suitableForFemale = createNumber("suitableForFemale", Integer.class);

    public final NumberPath<Integer> suitableForMaleCount = createNumber("suitableForMaleCount", Integer.class);

    public final NumberPath<Double> transportAverage = createNumber("transportAverage", Double.class);

    public final StringPath zipCode = createString("zipCode");

    public QDatingPlace(String variable) {
        this(DatingPlace.class, forVariable(variable), INITS);
    }

    public QDatingPlace(Path<? extends DatingPlace> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QDatingPlace(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QDatingPlace(PathMetadata metadata, PathInits inits) {
        this(DatingPlace.class, metadata, inits);
    }

    public QDatingPlace(Class<? extends DatingPlace> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.coverPicture = inits.isInitialized("coverPicture") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("coverPicture"), inits.get("coverPicture")) : null;
        this.owner = inits.isInitialized("owner") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("owner"), inits.get("owner")) : null;
    }

}

