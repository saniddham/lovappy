package com.realroofers.lovappy.service.cms.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTextTranslation is a Querydsl query type for TextTranslation
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTextTranslation extends EntityPathBase<TextTranslation> {

    private static final long serialVersionUID = 1173809975L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QTextTranslation textTranslation = new QTextTranslation("textTranslation");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final StringPath content = createString("content");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath language = createString("language");

    public final QTextWidget textWidget;

    public final StringPath title = createString("title");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QTextTranslation(String variable) {
        this(TextTranslation.class, forVariable(variable), INITS);
    }

    public QTextTranslation(Path<? extends TextTranslation> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QTextTranslation(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QTextTranslation(PathMetadata metadata, PathInits inits) {
        this(TextTranslation.class, metadata, inits);
    }

    public QTextTranslation(Class<? extends TextTranslation> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.textWidget = inits.isInitialized("textWidget") ? new QTextWidget(forProperty("textWidget")) : null;
    }

}

