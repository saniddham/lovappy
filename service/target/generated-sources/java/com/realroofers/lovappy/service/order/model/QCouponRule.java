package com.realroofers.lovappy.service.order.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCouponRule is a Querydsl query type for CouponRule
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCouponRule extends EntityPathBase<CouponRule> {

    private static final long serialVersionUID = -1487800246L;

    public static final QCouponRule couponRule = new QCouponRule("couponRule");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final EnumPath<com.realroofers.lovappy.service.order.support.RuleActionType> actionType = createEnum("actionType", com.realroofers.lovappy.service.order.support.RuleActionType.class);

    public final EnumPath<com.realroofers.lovappy.service.order.support.CouponCategory> affectedCategory = createEnum("affectedCategory", com.realroofers.lovappy.service.order.support.CouponCategory.class);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final DateTimePath<java.util.Date> creationDate = createDateTime("creationDate", java.util.Date.class);

    public final NumberPath<Double> discountValue = createNumber("discountValue", Double.class);

    public final DateTimePath<java.util.Date> expiryDate = createDateTime("expiryDate", java.util.Date.class);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath ruleDescription = createString("ruleDescription");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QCouponRule(String variable) {
        super(CouponRule.class, forVariable(variable));
    }

    public QCouponRule(Path<? extends CouponRule> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCouponRule(PathMetadata metadata) {
        super(CouponRule.class, metadata);
    }

}

