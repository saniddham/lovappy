package com.realroofers.lovappy.service.user.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUserPreference is a Querydsl query type for UserPreference
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUserPreference extends EntityPathBase<UserPreference> {

    private static final long serialVersionUID = -1555551605L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QUserPreference userPreference = new QUserPreference("userPreference");

    public final EnumPath<com.realroofers.lovappy.service.user.support.Size> bustSize = createEnum("bustSize", com.realroofers.lovappy.service.user.support.Size.class);

    public final EnumPath<com.realroofers.lovappy.service.user.support.Size> buttSize = createEnum("buttSize", com.realroofers.lovappy.service.user.support.Size.class);

    public final EnumPath<com.realroofers.lovappy.service.user.support.Gender> gender = createEnum("gender", com.realroofers.lovappy.service.user.support.Gender.class);

    public final EnumPath<com.realroofers.lovappy.service.user.support.PrefHeight> height = createEnum("height", com.realroofers.lovappy.service.user.support.PrefHeight.class);

    public final NumberPath<Integer> maxAge = createNumber("maxAge", Integer.class);

    public final NumberPath<Integer> minAge = createNumber("minAge", Integer.class);

    public final BooleanPath newGiftsNotifications = createBoolean("newGiftsNotifications");

    public final BooleanPath newLikesNotifications = createBoolean("newLikesNotifications");

    public final BooleanPath newMatchesNotifications = createBoolean("newMatchesNotifications");

    public final BooleanPath newMessagesNotifications = createBoolean("newMessagesNotifications");

    public final BooleanPath newSongPurchase = createBoolean("newSongPurchase");

    public final BooleanPath newSongsNotifications = createBoolean("newSongsNotifications");

    public final BooleanPath newUnlockRequestNotifications = createBoolean("newUnlockRequestNotifications");

    public final BooleanPath penisSizeMatter = createBoolean("penisSizeMatter");

    public final CollectionPath<com.realroofers.lovappy.service.system.model.Language, com.realroofers.lovappy.service.system.model.QLanguage> seekingLanguage = this.<com.realroofers.lovappy.service.system.model.Language, com.realroofers.lovappy.service.system.model.QLanguage>createCollection("seekingLanguage", com.realroofers.lovappy.service.system.model.Language.class, com.realroofers.lovappy.service.system.model.QLanguage.class, PathInits.DIRECT2);

    public final BooleanPath songApproval = createBoolean("songApproval");

    public final BooleanPath songDenials = createBoolean("songDenials");

    public final QUser user;

    public final NumberPath<Integer> userId = createNumber("userId", Integer.class);

    public QUserPreference(String variable) {
        this(UserPreference.class, forVariable(variable), INITS);
    }

    public QUserPreference(Path<? extends UserPreference> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QUserPreference(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QUserPreference(PathMetadata metadata, PathInits inits) {
        this(UserPreference.class, metadata, inits);
    }

    public QUserPreference(Class<? extends UserPreference> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.user = inits.isInitialized("user") ? new QUser(forProperty("user"), inits.get("user")) : null;
    }

}

