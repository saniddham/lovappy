package com.realroofers.lovappy.service.radio.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QStartSound is a Querydsl query type for StartSound
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QStartSound extends EntityPathBase<StartSound> {

    private static final long serialVersionUID = -890643256L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QStartSound startSound = new QStartSound("startSound");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile audioFileCloud;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final EnumPath<com.realroofers.lovappy.service.radio.support.StartSoundType> soundType = createEnum("soundType", com.realroofers.lovappy.service.radio.support.StartSoundType.class);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QStartSound(String variable) {
        this(StartSound.class, forVariable(variable), INITS);
    }

    public QStartSound(Path<? extends StartSound> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QStartSound(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QStartSound(PathMetadata metadata, PathInits inits) {
        this(StartSound.class, metadata, inits);
    }

    public QStartSound(Class<? extends StartSound> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.audioFileCloud = inits.isInitialized("audioFileCloud") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("audioFileCloud"), inits.get("audioFileCloud")) : null;
    }

}

