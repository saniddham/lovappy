package com.realroofers.lovappy.service.lovstamps.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QLovstampSample is a Querydsl query type for LovstampSample
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLovstampSample extends EntityPathBase<LovstampSample> {

    private static final long serialVersionUID = -989131539L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QLovstampSample lovstampSample = new QLovstampSample("lovstampSample");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile audioFileCloud;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> duration = createNumber("duration", Integer.class);

    public final EnumPath<com.realroofers.lovappy.service.user.support.Gender> gender = createEnum("gender", com.realroofers.lovappy.service.user.support.Gender.class);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.system.model.QLanguage language;

    public final StringPath recordFile = createString("recordFile");

    public final EnumPath<com.realroofers.lovappy.service.lovstamps.support.SampleTypes> type = createEnum("type", com.realroofers.lovappy.service.lovstamps.support.SampleTypes.class);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QLovstampSample(String variable) {
        this(LovstampSample.class, forVariable(variable), INITS);
    }

    public QLovstampSample(Path<? extends LovstampSample> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QLovstampSample(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QLovstampSample(PathMetadata metadata, PathInits inits) {
        this(LovstampSample.class, metadata, inits);
    }

    public QLovstampSample(Class<? extends LovstampSample> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.audioFileCloud = inits.isInitialized("audioFileCloud") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("audioFileCloud"), inits.get("audioFileCloud")) : null;
        this.language = inits.isInitialized("language") ? new com.realroofers.lovappy.service.system.model.QLanguage(forProperty("language")) : null;
    }

}

