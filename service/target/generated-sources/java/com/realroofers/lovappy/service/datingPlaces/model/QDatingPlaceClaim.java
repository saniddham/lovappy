package com.realroofers.lovappy.service.datingPlaces.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDatingPlaceClaim is a Querydsl query type for DatingPlaceClaim
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDatingPlaceClaim extends EntityPathBase<DatingPlaceClaim> {

    private static final long serialVersionUID = -1921087175L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDatingPlaceClaim datingPlaceClaim = new QDatingPlaceClaim("datingPlaceClaim");

    public final com.realroofers.lovappy.service.user.model.QUser claimer;

    public final NumberPath<Integer> claimId = createNumber("claimId", Integer.class);

    public final StringPath content = createString("content");

    public final StringPath googlePlaceId = createString("googlePlaceId");

    public final EnumPath<com.realroofers.lovappy.service.user.support.ApprovalStatus> placeApprovalStatus = createEnum("placeApprovalStatus", com.realroofers.lovappy.service.user.support.ApprovalStatus.class);

    public final QDatingPlace placeId;

    public final StringPath sendFrom = createString("sendFrom");

    public final StringPath sendTo = createString("sendTo");

    public final StringPath verificationCode = createString("verificationCode");

    public final EnumPath<com.realroofers.lovappy.service.datingPlaces.support.VerificationType> verificationType = createEnum("verificationType", com.realroofers.lovappy.service.datingPlaces.support.VerificationType.class);

    public QDatingPlaceClaim(String variable) {
        this(DatingPlaceClaim.class, forVariable(variable), INITS);
    }

    public QDatingPlaceClaim(Path<? extends DatingPlaceClaim> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QDatingPlaceClaim(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QDatingPlaceClaim(PathMetadata metadata, PathInits inits) {
        this(DatingPlaceClaim.class, metadata, inits);
    }

    public QDatingPlaceClaim(Class<? extends DatingPlaceClaim> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.claimer = inits.isInitialized("claimer") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("claimer"), inits.get("claimer")) : null;
        this.placeId = inits.isInitialized("placeId") ? new QDatingPlace(forProperty("placeId"), inits.get("placeId")) : null;
    }

}

