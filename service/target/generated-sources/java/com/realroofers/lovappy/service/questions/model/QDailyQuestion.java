package com.realroofers.lovappy.service.questions.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDailyQuestion is a Querydsl query type for DailyQuestion
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDailyQuestion extends EntityPathBase<DailyQuestion> {

    private static final long serialVersionUID = -81658346L;

    public static final QDailyQuestion dailyQuestion = new QDailyQuestion("dailyQuestion");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath question = createString("question");

    public final DateTimePath<java.util.Date> questionDate = createDateTime("questionDate", java.util.Date.class);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final ListPath<UserResponse, QUserResponse> userResponses = this.<UserResponse, QUserResponse>createList("userResponses", UserResponse.class, QUserResponse.class, PathInits.DIRECT2);

    public QDailyQuestion(String variable) {
        super(DailyQuestion.class, forVariable(variable));
    }

    public QDailyQuestion(Path<? extends DailyQuestion> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDailyQuestion(PathMetadata metadata) {
        super(DailyQuestion.class, metadata);
    }

}

