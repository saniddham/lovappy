package com.realroofers.lovappy.service.cms.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QVideoWidget is a Querydsl query type for VideoWidget
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QVideoWidget extends EntityPathBase<VideoWidget> {

    private static final long serialVersionUID = 29779986L;

    public static final QVideoWidget videoWidget = new QVideoWidget("videoWidget");

    public final QWidget _super = new QWidget(this);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Integer> id = _super.id;

    //inherited
    public final StringPath name = _super.name;

    public final SetPath<Page, QPage> pages = this.<Page, QPage>createSet("pages", Page.class, QPage.class, PathInits.DIRECT2);

    public final EnumPath<com.realroofers.lovappy.service.core.VideoProvider> provider = createEnum("provider", com.realroofers.lovappy.service.core.VideoProvider.class);

    public final StringPath providerId = createString("providerId");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final StringPath url = createString("url");

    public QVideoWidget(String variable) {
        super(VideoWidget.class, forVariable(variable));
    }

    public QVideoWidget(Path<? extends VideoWidget> path) {
        super(path.getType(), path.getMetadata());
    }

    public QVideoWidget(PathMetadata metadata) {
        super(VideoWidget.class, metadata);
    }

}

