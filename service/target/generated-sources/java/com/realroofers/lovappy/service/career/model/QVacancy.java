package com.realroofers.lovappy.service.career.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QVacancy is a Querydsl query type for Vacancy
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QVacancy extends EntityPathBase<Vacancy> {

    private static final long serialVersionUID = 1888799305L;

    public static final QVacancy vacancy = new QVacancy("vacancy");

    public final SetPath<CareerUser, QCareerUser> applicants = this.<CareerUser, QCareerUser>createSet("applicants", CareerUser.class, QCareerUser.class, PathInits.DIRECT2);

    public final StringPath city = createString("city");

    public final DatePath<java.sql.Date> closingDate = createDate("closingDate", java.sql.Date.class);

    public final StringPath compensation = createString("compensation");

    public final StringPath country = createString("country");

    public final StringPath description = createString("description");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath jobTitle = createString("jobTitle");

    public final StringPath keywords = createString("keywords");

    public final DatePath<java.sql.Date> openDate = createDate("openDate", java.sql.Date.class);

    public final StringPath positionDescription = createString("positionDescription");

    public final StringPath seoTitle = createString("seoTitle");

    public final StringPath skillRequired = createString("skillRequired");

    public final StringPath state = createString("state");

    public final StringPath status = createString("status");

    public QVacancy(String variable) {
        super(Vacancy.class, forVariable(variable));
    }

    public QVacancy(Path<? extends Vacancy> path) {
        super(path.getType(), path.getMetadata());
    }

    public QVacancy(PathMetadata metadata) {
        super(Vacancy.class, metadata);
    }

}

