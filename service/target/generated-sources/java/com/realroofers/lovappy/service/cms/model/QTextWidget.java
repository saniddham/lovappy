package com.realroofers.lovappy.service.cms.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTextWidget is a Querydsl query type for TextWidget
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTextWidget extends EntityPathBase<TextWidget> {

    private static final long serialVersionUID = 395748862L;

    public static final QTextWidget textWidget = new QTextWidget("textWidget");

    public final QWidget _super = new QWidget(this);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Integer> id = _super.id;

    //inherited
    public final StringPath name = _super.name;

    public final SetPath<Page, QPage> pages = this.<Page, QPage>createSet("pages", Page.class, QPage.class, PathInits.DIRECT2);

    public final SetPath<TextTranslation, QTextTranslation> translations = this.<TextTranslation, QTextTranslation>createSet("translations", TextTranslation.class, QTextTranslation.class, PathInits.DIRECT2);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QTextWidget(String variable) {
        super(TextWidget.class, forVariable(variable));
    }

    public QTextWidget(Path<? extends TextWidget> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTextWidget(PathMetadata metadata) {
        super(TextWidget.class, metadata);
    }

}

