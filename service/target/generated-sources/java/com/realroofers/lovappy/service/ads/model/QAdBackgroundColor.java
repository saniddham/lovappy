package com.realroofers.lovappy.service.ads.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdBackgroundColor is a Querydsl query type for AdBackgroundColor
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdBackgroundColor extends EntityPathBase<AdBackgroundColor> {

    private static final long serialVersionUID = -2060460596L;

    public static final QAdBackgroundColor adBackgroundColor = new QAdBackgroundColor("adBackgroundColor");

    public final StringPath color = createString("color");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public QAdBackgroundColor(String variable) {
        super(AdBackgroundColor.class, forVariable(variable));
    }

    public QAdBackgroundColor(Path<? extends AdBackgroundColor> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdBackgroundColor(PathMetadata metadata) {
        super(AdBackgroundColor.class, metadata);
    }

}

