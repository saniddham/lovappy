package com.realroofers.lovappy.service.order.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProductOrder is a Querydsl query type for ProductOrder
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProductOrder extends EntityPathBase<ProductOrder> {

    private static final long serialVersionUID = -238207577L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProductOrder productOrder = new QProductOrder("productOrder");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final StringPath cardNo = createString("cardNo");

    public final StringPath cardType = createString("cardType");

    public final QCoupon coupon;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final DateTimePath<java.util.Date> date = createDateTime("date", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final CollectionPath<OrderDetail, QOrderDetail> ordersDetails = this.<OrderDetail, QOrderDetail>createCollection("ordersDetails", OrderDetail.class, QOrderDetail.class, PathInits.DIRECT2);

    public final EnumPath<com.realroofers.lovappy.service.order.support.PaymentMethodType> paymentMethod = createEnum("paymentMethod", com.realroofers.lovappy.service.order.support.PaymentMethodType.class);

    public final NumberPath<Double> totalPrice = createNumber("totalPrice", Double.class);

    public final StringPath transactionID = createString("transactionID");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final com.realroofers.lovappy.service.user.model.QUser user;

    public QProductOrder(String variable) {
        this(ProductOrder.class, forVariable(variable), INITS);
    }

    public QProductOrder(Path<? extends ProductOrder> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QProductOrder(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QProductOrder(PathMetadata metadata, PathInits inits) {
        this(ProductOrder.class, metadata, inits);
    }

    public QProductOrder(Class<? extends ProductOrder> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.coupon = inits.isInitialized("coupon") ? new QCoupon(forProperty("coupon")) : null;
        this.user = inits.isInitialized("user") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("user"), inits.get("user")) : null;
    }

}

