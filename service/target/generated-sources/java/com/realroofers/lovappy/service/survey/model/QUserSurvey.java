package com.realroofers.lovappy.service.survey.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUserSurvey is a Querydsl query type for UserSurvey
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUserSurvey extends EntityPathBase<UserSurvey> {

    private static final long serialVersionUID = -1197160933L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QUserSurvey userSurvey = new QUserSurvey("userSurvey");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final ListPath<SurveyQuestionAnswer, QSurveyQuestionAnswer> questionAnswers = this.<SurveyQuestionAnswer, QSurveyQuestionAnswer>createList("questionAnswers", SurveyQuestionAnswer.class, QSurveyQuestionAnswer.class, PathInits.DIRECT2);

    public final EnumPath<com.realroofers.lovappy.service.survey.support.SurveyStatus> surveyStatus = createEnum("surveyStatus", com.realroofers.lovappy.service.survey.support.SurveyStatus.class);

    public final com.realroofers.lovappy.service.user.model.QUser user;

    public QUserSurvey(String variable) {
        this(UserSurvey.class, forVariable(variable), INITS);
    }

    public QUserSurvey(Path<? extends UserSurvey> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QUserSurvey(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QUserSurvey(PathMetadata metadata, PathInits inits) {
        this(UserSurvey.class, metadata, inits);
    }

    public QUserSurvey(Class<? extends UserSurvey> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.user = inits.isInitialized("user") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("user"), inits.get("user")) : null;
    }

}

