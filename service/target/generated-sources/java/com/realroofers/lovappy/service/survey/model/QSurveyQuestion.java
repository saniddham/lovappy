package com.realroofers.lovappy.service.survey.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSurveyQuestion is a Querydsl query type for SurveyQuestion
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSurveyQuestion extends EntityPathBase<SurveyQuestion> {

    private static final long serialVersionUID = 1684102518L;

    public static final QSurveyQuestion surveyQuestion = new QSurveyQuestion("surveyQuestion");

    public final ListPath<SurveyAnswer, QSurveyAnswer> answers = this.<SurveyAnswer, QSurveyAnswer>createList("answers", SurveyAnswer.class, QSurveyAnswer.class, PathInits.DIRECT2);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath title = createString("title");

    public QSurveyQuestion(String variable) {
        super(SurveyQuestion.class, forVariable(variable));
    }

    public QSurveyQuestion(Path<? extends SurveyQuestion> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSurveyQuestion(PathMetadata metadata) {
        super(SurveyQuestion.class, metadata);
    }

}

