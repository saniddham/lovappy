package com.realroofers.lovappy.service.music.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMusicUser is a Querydsl query type for MusicUser
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMusicUser extends EntityPathBase<MusicUser> {

    private static final long serialVersionUID = 1758516063L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMusicUser musicUser = new QMusicUser("musicUser");

    public final DateTimePath<java.util.Date> created = createDateTime("created", java.util.Date.class);

    public final QMusicUserPK id;

    public final NumberPath<Integer> maxDownloads = createNumber("maxDownloads", Integer.class);

    public final NumberPath<Integer> mobileDownloads = createNumber("mobileDownloads", Integer.class);

    public final BooleanPath purchased = createBoolean("purchased");

    public final DateTimePath<java.util.Date> purchasedOn = createDateTime("purchasedOn", java.util.Date.class);

    public final NumberPath<Integer> rating = createNumber("rating", Integer.class);

    public final BooleanPath received = createBoolean("received");

    public final DateTimePath<java.util.Date> updated = createDateTime("updated", java.util.Date.class);

    public final NumberPath<Integer> webDownloads = createNumber("webDownloads", Integer.class);

    public QMusicUser(String variable) {
        this(MusicUser.class, forVariable(variable), INITS);
    }

    public QMusicUser(Path<? extends MusicUser> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QMusicUser(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QMusicUser(PathMetadata metadata, PathInits inits) {
        this(MusicUser.class, metadata, inits);
    }

    public QMusicUser(Class<? extends MusicUser> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QMusicUserPK(forProperty("id"), inits.get("id")) : null;
    }

}

