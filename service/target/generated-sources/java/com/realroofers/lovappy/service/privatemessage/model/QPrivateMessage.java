package com.realroofers.lovappy.service.privatemessage.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPrivateMessage is a Querydsl query type for PrivateMessage
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPrivateMessage extends EntityPathBase<PrivateMessage> {

    private static final long serialVersionUID = -1161272176L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPrivateMessage privateMessage = new QPrivateMessage("privateMessage");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile audioFileCloud;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath fileName = createString("fileName");

    public final com.realroofers.lovappy.service.user.model.QUser fromUser;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.system.model.QLanguage language;

    public final DateTimePath<java.util.Date> sentAt = createDateTime("sentAt", java.util.Date.class);

    public final EnumPath<com.realroofers.lovappy.service.privatemessage.support.Status> status = createEnum("status", com.realroofers.lovappy.service.privatemessage.support.Status.class);

    public final com.realroofers.lovappy.service.user.model.QUser toUser;

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QPrivateMessage(String variable) {
        this(PrivateMessage.class, forVariable(variable), INITS);
    }

    public QPrivateMessage(Path<? extends PrivateMessage> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPrivateMessage(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPrivateMessage(PathMetadata metadata, PathInits inits) {
        this(PrivateMessage.class, metadata, inits);
    }

    public QPrivateMessage(Class<? extends PrivateMessage> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.audioFileCloud = inits.isInitialized("audioFileCloud") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("audioFileCloud"), inits.get("audioFileCloud")) : null;
        this.fromUser = inits.isInitialized("fromUser") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("fromUser"), inits.get("fromUser")) : null;
        this.language = inits.isInitialized("language") ? new com.realroofers.lovappy.service.system.model.QLanguage(forProperty("language")) : null;
        this.toUser = inits.isInitialized("toUser") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("toUser"), inits.get("toUser")) : null;
    }

}

