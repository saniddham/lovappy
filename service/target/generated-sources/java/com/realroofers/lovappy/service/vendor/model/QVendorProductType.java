package com.realroofers.lovappy.service.vendor.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QVendorProductType is a Querydsl query type for VendorProductType
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QVendorProductType extends EntityPathBase<VendorProductType> {

    private static final long serialVersionUID = -1905087879L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QVendorProductType vendorProductType = new QVendorProductType("vendorProductType");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    public final BooleanPath approved = createBoolean("approved");

    public final com.realroofers.lovappy.service.user.model.QUser approvedBy;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final com.realroofers.lovappy.service.user.model.QUser createdBy;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.product.model.QProductType productType;

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final QVendor vendor;

    public QVendorProductType(String variable) {
        this(VendorProductType.class, forVariable(variable), INITS);
    }

    public QVendorProductType(Path<? extends VendorProductType> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QVendorProductType(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QVendorProductType(PathMetadata metadata, PathInits inits) {
        this(VendorProductType.class, metadata, inits);
    }

    public QVendorProductType(Class<? extends VendorProductType> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.approvedBy = inits.isInitialized("approvedBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("approvedBy"), inits.get("approvedBy")) : null;
        this.createdBy = inits.isInitialized("createdBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("createdBy"), inits.get("createdBy")) : null;
        this.productType = inits.isInitialized("productType") ? new com.realroofers.lovappy.service.product.model.QProductType(forProperty("productType"), inits.get("productType")) : null;
        this.vendor = inits.isInitialized("vendor") ? new QVendor(forProperty("vendor"), inits.get("vendor")) : null;
    }

}

