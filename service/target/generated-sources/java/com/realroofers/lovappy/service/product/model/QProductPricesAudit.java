package com.realroofers.lovappy.service.product.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProductPricesAudit is a Querydsl query type for ProductPricesAudit
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProductPricesAudit extends EntityPathBase<ProductPricesAudit> {

    private static final long serialVersionUID = 137115145L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProductPricesAudit productPricesAudit = new QProductPricesAudit("productPricesAudit");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final com.realroofers.lovappy.service.user.model.QUser createdBy;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final NumberPath<java.math.BigDecimal> price = createNumber("price", java.math.BigDecimal.class);

    public final QProduct product;

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QProductPricesAudit(String variable) {
        this(ProductPricesAudit.class, forVariable(variable), INITS);
    }

    public QProductPricesAudit(Path<? extends ProductPricesAudit> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QProductPricesAudit(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QProductPricesAudit(PathMetadata metadata, PathInits inits) {
        this(ProductPricesAudit.class, metadata, inits);
    }

    public QProductPricesAudit(Class<? extends ProductPricesAudit> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.createdBy = inits.isInitialized("createdBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("createdBy"), inits.get("createdBy")) : null;
        this.product = inits.isInitialized("product") ? new QProduct(forProperty("product"), inits.get("product")) : null;
    }

}

