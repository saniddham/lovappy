package com.realroofers.lovappy.service.music.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMusic is a Querydsl query type for Music
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMusic extends EntityPathBase<Music> {

    private static final long serialVersionUID = -550220428L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMusic music = new QMusic("music");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath advertised = createBoolean("advertised");

    public final EnumPath<com.realroofers.lovappy.service.ads.support.AgeRange> ageRange = createEnum("ageRange", com.realroofers.lovappy.service.ads.support.AgeRange.class);

    public final StringPath artistName = createString("artistName");

    public final com.realroofers.lovappy.service.user.model.QUser author;

    public final NumberPath<Double> avgRating = createNumber("avgRating", Double.class);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile coverImage;

    public final StringPath coverImageUrl = createString("coverImageUrl");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath decade = createString("decade");

    public final BooleanPath deleted = createBoolean("deleted");

    public final StringPath downloadUrl = createString("downloadUrl");

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile file;

    public final StringPath fileUrl = createString("fileUrl");

    public final EnumPath<com.realroofers.lovappy.service.user.support.Gender> gender = createEnum("gender", com.realroofers.lovappy.service.user.support.Gender.class);

    public final QGenre genre;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath lyrics = createString("lyrics");

    public final SetPath<MusicUser, QMusicUser> musicUsers = this.<MusicUser, QMusicUser>createSet("musicUsers", MusicUser.class, QMusicUser.class, PathInits.DIRECT2);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile previewFile;

    public final StringPath previewFileUrl = createString("previewFileUrl");

    public final EnumPath<com.realroofers.lovappy.service.music.support.MusicProvider> provider = createEnum("provider", com.realroofers.lovappy.service.music.support.MusicProvider.class);

    public final NumberPath<Long> providerResourceId = createNumber("providerResourceId", Long.class);

    public final EnumPath<com.realroofers.lovappy.service.music.support.MusicStatus> status = createEnum("status", com.realroofers.lovappy.service.music.support.MusicStatus.class);

    public final StringPath title = createString("title");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QMusic(String variable) {
        this(Music.class, forVariable(variable), INITS);
    }

    public QMusic(Path<? extends Music> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QMusic(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QMusic(PathMetadata metadata, PathInits inits) {
        this(Music.class, metadata, inits);
    }

    public QMusic(Class<? extends Music> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.author = inits.isInitialized("author") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("author"), inits.get("author")) : null;
        this.coverImage = inits.isInitialized("coverImage") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("coverImage"), inits.get("coverImage")) : null;
        this.file = inits.isInitialized("file") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("file"), inits.get("file")) : null;
        this.genre = inits.isInitialized("genre") ? new QGenre(forProperty("genre")) : null;
        this.previewFile = inits.isInitialized("previewFile") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("previewFile"), inits.get("previewFile")) : null;
    }

}

