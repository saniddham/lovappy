package com.realroofers.lovappy.service.user.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProfileTransaction is a Querydsl query type for ProfileTransaction
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileTransaction extends EntityPathBase<ProfileTransaction> {

    private static final long serialVersionUID = -1015293318L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProfileTransaction profileTransaction = new QProfileTransaction("profileTransaction");

    public final DateTimePath<java.util.Date> created = createDateTime("created", java.util.Date.class);

    public final QProfileTransactionPK id;

    public final DateTimePath<java.util.Date> lastTransaction = createDateTime("lastTransaction", java.util.Date.class);

    public final BooleanPath seen = createBoolean("seen");

    public final NumberPath<Integer> transactionCount = createNumber("transactionCount", Integer.class);

    public QProfileTransaction(String variable) {
        this(ProfileTransaction.class, forVariable(variable), INITS);
    }

    public QProfileTransaction(Path<? extends ProfileTransaction> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QProfileTransaction(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QProfileTransaction(PathMetadata metadata, PathInits inits) {
        this(ProfileTransaction.class, metadata, inits);
    }

    public QProfileTransaction(Class<? extends ProfileTransaction> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QProfileTransactionPK(forProperty("id"), inits.get("id")) : null;
    }

}

