package com.realroofers.lovappy.service.datingPlaces.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDatingPlacePayment is a Querydsl query type for DatingPlacePayment
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDatingPlacePayment extends EntityPathBase<DatingPlacePayment> {

    private static final long serialVersionUID = -968833085L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDatingPlacePayment datingPlacePayment = new QDatingPlacePayment("datingPlacePayment");

    public final NumberPath<Integer> datingPlacePaymentId = createNumber("datingPlacePaymentId", Integer.class);

    public final NumberPath<Double> paidAmount = createNumber("paidAmount", Double.class);

    public final DateTimePath<java.util.Date> paymentDate = createDateTime("paymentDate", java.util.Date.class);

    public final QDatingPlace placeId;

    public QDatingPlacePayment(String variable) {
        this(DatingPlacePayment.class, forVariable(variable), INITS);
    }

    public QDatingPlacePayment(Path<? extends DatingPlacePayment> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QDatingPlacePayment(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QDatingPlacePayment(PathMetadata metadata, PathInits inits) {
        this(DatingPlacePayment.class, metadata, inits);
    }

    public QDatingPlacePayment(Class<? extends DatingPlacePayment> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.placeId = inits.isInitialized("placeId") ? new QDatingPlace(forProperty("placeId"), inits.get("placeId")) : null;
    }

}

