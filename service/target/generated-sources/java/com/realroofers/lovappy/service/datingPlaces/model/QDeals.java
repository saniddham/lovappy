package com.realroofers.lovappy.service.datingPlaces.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDeals is a Querydsl query type for Deals
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDeals extends EntityPathBase<Deals> {

    private static final long serialVersionUID = 2093617326L;

    public static final QDeals deals = new QDeals("deals");

    public final NumberPath<Integer> dealId = createNumber("dealId", Integer.class);

    public final NumberPath<Double> endPrice = createNumber("endPrice", Double.class);

    public final NumberPath<Double> feePercentage = createNumber("feePercentage", Double.class);

    public final ListPath<DatingPlaceDeals, QDatingPlaceDeals> placeDeals = this.<DatingPlaceDeals, QDatingPlaceDeals>createList("placeDeals", DatingPlaceDeals.class, QDatingPlaceDeals.class, PathInits.DIRECT2);

    public final NumberPath<Double> startPrice = createNumber("startPrice", Double.class);

    public QDeals(String variable) {
        super(Deals.class, forVariable(variable));
    }

    public QDeals(Path<? extends Deals> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDeals(PathMetadata metadata) {
        super(Deals.class, metadata);
    }

}

