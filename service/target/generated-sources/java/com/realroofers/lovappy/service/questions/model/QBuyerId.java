package com.realroofers.lovappy.service.questions.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QBuyerId is a Querydsl query type for BuyerId
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QBuyerId extends BeanPath<BuyerId> {

    private static final long serialVersionUID = -727984027L;

    public static final QBuyerId buyerId = new QBuyerId("buyerId");

    public final NumberPath<Long> responseId = createNumber("responseId", Long.class);

    public final NumberPath<Integer> userId = createNumber("userId", Integer.class);

    public QBuyerId(String variable) {
        super(BuyerId.class, forVariable(variable));
    }

    public QBuyerId(Path<? extends BuyerId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBuyerId(PathMetadata metadata) {
        super(BuyerId.class, metadata);
    }

}

