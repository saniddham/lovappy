package com.realroofers.lovappy.service.radio.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QRadioProgramPlayTime is a Querydsl query type for RadioProgramPlayTime
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRadioProgramPlayTime extends EntityPathBase<RadioProgramPlayTime> {

    private static final long serialVersionUID = -789101947L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QRadioProgramPlayTime radioProgramPlayTime = new QRadioProgramPlayTime("radioProgramPlayTime");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final DateTimePath<java.util.Date> endTime = createDateTime("endTime", java.util.Date.class);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final QRadioProgramControl radioProgramControl;

    public final DateTimePath<java.util.Date> startTime = createDateTime("startTime", java.util.Date.class);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QRadioProgramPlayTime(String variable) {
        this(RadioProgramPlayTime.class, forVariable(variable), INITS);
    }

    public QRadioProgramPlayTime(Path<? extends RadioProgramPlayTime> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QRadioProgramPlayTime(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QRadioProgramPlayTime(PathMetadata metadata, PathInits inits) {
        this(RadioProgramPlayTime.class, metadata, inits);
    }

    public QRadioProgramPlayTime(Class<? extends RadioProgramPlayTime> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.radioProgramControl = inits.isInitialized("radioProgramControl") ? new QRadioProgramControl(forProperty("radioProgramControl"), inits.get("radioProgramControl")) : null;
    }

}

