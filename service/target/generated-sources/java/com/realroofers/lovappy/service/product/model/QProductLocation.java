package com.realroofers.lovappy.service.product.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProductLocation is a Querydsl query type for ProductLocation
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProductLocation extends EntityPathBase<ProductLocation> {

    private static final long serialVersionUID = -1058842019L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProductLocation productLocation = new QProductLocation("productLocation");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final com.realroofers.lovappy.service.user.model.QUser createdBy;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.vendor.model.QVendorLocation location;

    public final QProduct product;

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QProductLocation(String variable) {
        this(ProductLocation.class, forVariable(variable), INITS);
    }

    public QProductLocation(Path<? extends ProductLocation> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QProductLocation(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QProductLocation(PathMetadata metadata, PathInits inits) {
        this(ProductLocation.class, metadata, inits);
    }

    public QProductLocation(Class<? extends ProductLocation> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.createdBy = inits.isInitialized("createdBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("createdBy"), inits.get("createdBy")) : null;
        this.location = inits.isInitialized("location") ? new com.realroofers.lovappy.service.vendor.model.QVendorLocation(forProperty("location"), inits.get("location")) : null;
        this.product = inits.isInitialized("product") ? new QProduct(forProperty("product"), inits.get("product")) : null;
    }

}

