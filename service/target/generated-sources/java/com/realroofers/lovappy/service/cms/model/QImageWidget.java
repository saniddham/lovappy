package com.realroofers.lovappy.service.cms.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QImageWidget is a Querydsl query type for ImageWidget
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QImageWidget extends EntityPathBase<ImageWidget> {

    private static final long serialVersionUID = 1162204402L;

    public static final QImageWidget imageWidget = new QImageWidget("imageWidget");

    public final QWidget _super = new QWidget(this);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    //inherited
    public final NumberPath<Integer> id = _super.id;

    //inherited
    public final StringPath name = _super.name;

    public final SetPath<Page, QPage> pages = this.<Page, QPage>createSet("pages", Page.class, QPage.class, PathInits.DIRECT2);

    public final StringPath type = createString("type");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final StringPath url = createString("url");

    public QImageWidget(String variable) {
        super(ImageWidget.class, forVariable(variable));
    }

    public QImageWidget(Path<? extends ImageWidget> path) {
        super(path.getType(), path.getMetadata());
    }

    public QImageWidget(PathMetadata metadata) {
        super(ImageWidget.class, metadata);
    }

}

