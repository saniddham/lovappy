package com.realroofers.lovappy.service.event.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QEventCategory is a Querydsl query type for EventCategory
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEventCategory extends EntityPathBase<EventCategory> {

    private static final long serialVersionUID = -2045332996L;

    public static final QEventCategory eventCategory = new QEventCategory("eventCategory");

    public final StringPath categoryName = createString("categoryName");

    public final BooleanPath enabled = createBoolean("enabled");

    public final CollectionPath<Event, QEvent> events = this.<Event, QEvent>createCollection("events", Event.class, QEvent.class, PathInits.DIRECT2);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public QEventCategory(String variable) {
        super(EventCategory.class, forVariable(variable));
    }

    public QEventCategory(Path<? extends EventCategory> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEventCategory(PathMetadata metadata) {
        super(EventCategory.class, metadata);
    }

}

