package com.realroofers.lovappy.service.event.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QEventUserPicture is a Querydsl query type for EventUserPicture
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEventUserPicture extends EntityPathBase<EventUserPicture> {

    private static final long serialVersionUID = 2063417589L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QEventUserPicture eventUserPicture = new QEventUserPicture("eventUserPicture");

    public final QEvent eventId;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile picture;

    public final com.realroofers.lovappy.service.user.model.QUser userId;

    public QEventUserPicture(String variable) {
        this(EventUserPicture.class, forVariable(variable), INITS);
    }

    public QEventUserPicture(Path<? extends EventUserPicture> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QEventUserPicture(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QEventUserPicture(PathMetadata metadata, PathInits inits) {
        this(EventUserPicture.class, metadata, inits);
    }

    public QEventUserPicture(Class<? extends EventUserPicture> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.eventId = inits.isInitialized("eventId") ? new QEvent(forProperty("eventId"), inits.get("eventId")) : null;
        this.picture = inits.isInitialized("picture") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("picture"), inits.get("picture")) : null;
        this.userId = inits.isInitialized("userId") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("userId"), inits.get("userId")) : null;
    }

}

