package com.realroofers.lovappy.service.cms.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPage is a Querydsl query type for Page
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPage extends EntityPathBase<Page> {

    private static final long serialVersionUID = -974735236L;

    public static final QPage page = new QPage("page");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final SetPath<ImageWidget, QImageWidget> images = this.<ImageWidget, QImageWidget>createSet("images", ImageWidget.class, QImageWidget.class, PathInits.DIRECT2);

    public final StringPath name = createString("name");

    public final MapPath<String, String, StringPath> settings = this.<String, String, StringPath>createMap("settings", String.class, String.class, StringPath.class);

    public final StringPath tag = createString("tag");

    public final SetPath<TextWidget, QTextWidget> textContents = this.<TextWidget, QTextWidget>createSet("textContents", TextWidget.class, QTextWidget.class, PathInits.DIRECT2);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final SetPath<VideoWidget, QVideoWidget> videos = this.<VideoWidget, QVideoWidget>createSet("videos", VideoWidget.class, QVideoWidget.class, PathInits.DIRECT2);

    public QPage(String variable) {
        super(Page.class, forVariable(variable));
    }

    public QPage(Path<? extends Page> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPage(PathMetadata metadata) {
        super(Page.class, metadata);
    }

}

