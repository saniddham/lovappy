package com.realroofers.lovappy.service.blog.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCategory is a Querydsl query type for Category
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCategory extends EntityPathBase<Category> {

    private static final long serialVersionUID = -1594728564L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCategory category = new QCategory("category");

    public final StringPath catState = createString("catState");

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final com.realroofers.lovappy.service.user.model.QUser createdBy;

    public final com.realroofers.lovappy.service.user.model.QUser deletedBy;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final ListPath<com.realroofers.lovappy.service.cloud.model.CloudStorageFile, com.realroofers.lovappy.service.cloud.model.QCloudStorageFile> images = this.<com.realroofers.lovappy.service.cloud.model.CloudStorageFile, com.realroofers.lovappy.service.cloud.model.QCloudStorageFile>createList("images", com.realroofers.lovappy.service.cloud.model.CloudStorageFile.class, com.realroofers.lovappy.service.cloud.model.QCloudStorageFile.class, PathInits.DIRECT2);

    public final StringPath name = createString("name");

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final com.realroofers.lovappy.service.user.model.QUser updatedBy;

    public QCategory(String variable) {
        this(Category.class, forVariable(variable), INITS);
    }

    public QCategory(Path<? extends Category> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCategory(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCategory(PathMetadata metadata, PathInits inits) {
        this(Category.class, metadata, inits);
    }

    public QCategory(Class<? extends Category> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.createdBy = inits.isInitialized("createdBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("createdBy"), inits.get("createdBy")) : null;
        this.deletedBy = inits.isInitialized("deletedBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("deletedBy"), inits.get("deletedBy")) : null;
        this.updatedBy = inits.isInitialized("updatedBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("updatedBy"), inits.get("updatedBy")) : null;
    }

}

