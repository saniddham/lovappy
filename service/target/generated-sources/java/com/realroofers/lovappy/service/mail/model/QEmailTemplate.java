package com.realroofers.lovappy.service.mail.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QEmailTemplate is a Querydsl query type for EmailTemplate
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEmailTemplate extends EntityPathBase<EmailTemplate> {

    private static final long serialVersionUID = 498065405L;

    public static final QEmailTemplate emailTemplate = new QEmailTemplate("emailTemplate");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final BooleanPath enabled = createBoolean("enabled");

    public final StringPath fromEmail = createString("fromEmail");

    public final StringPath fromName = createString("fromName");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath imageUrl = createString("imageUrl");

    public final StringPath name = createString("name");

    public final StringPath templateUrl = createString("templateUrl");

    public final EnumPath<com.realroofers.lovappy.service.mail.support.EmailTypes> type = createEnum("type", com.realroofers.lovappy.service.mail.support.EmailTypes.class);

    public final DateTimePath<java.util.Date> updated = createDateTime("updated", java.util.Date.class);

    public QEmailTemplate(String variable) {
        super(EmailTemplate.class, forVariable(variable));
    }

    public QEmailTemplate(Path<? extends EmailTemplate> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEmailTemplate(PathMetadata metadata) {
        super(EmailTemplate.class, metadata);
    }

}

