package com.realroofers.lovappy.service.event.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QEventComment is a Querydsl query type for EventComment
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEventComment extends EntityPathBase<EventComment> {

    private static final long serialVersionUID = 1021338017L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QEventComment eventComment = new QEventComment("eventComment");

    public final StringPath commentDescription = createString("commentDescription");

    public final DateTimePath<java.util.Date> commentPostedDate = createDateTime("commentPostedDate", java.util.Date.class);

    public final QEvent eventId;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.user.model.QUser userId;

    public QEventComment(String variable) {
        this(EventComment.class, forVariable(variable), INITS);
    }

    public QEventComment(Path<? extends EventComment> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QEventComment(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QEventComment(PathMetadata metadata, PathInits inits) {
        this(EventComment.class, metadata, inits);
    }

    public QEventComment(Class<? extends EventComment> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.eventId = inits.isInitialized("eventId") ? new QEvent(forProperty("eventId"), inits.get("eventId")) : null;
        this.userId = inits.isInitialized("userId") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("userId"), inits.get("userId")) : null;
    }

}

