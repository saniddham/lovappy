package com.realroofers.lovappy.service.tips.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QLoveTip is a Querydsl query type for LoveTip
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLoveTip extends EntityPathBase<LoveTip> {

    private static final long serialVersionUID = 1556278417L;

    public static final QLoveTip loveTip = new QLoveTip("loveTip");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    public final StringPath backgroundColor = createString("backgroundColor");

    public final StringPath backgroundImage = createString("backgroundImage");

    public final StringPath content = createString("content");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath textColor = createString("textColor");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QLoveTip(String variable) {
        super(LoveTip.class, forVariable(variable));
    }

    public QLoveTip(Path<? extends LoveTip> path) {
        super(path.getType(), path.getMetadata());
    }

    public QLoveTip(PathMetadata metadata) {
        super(LoveTip.class, metadata);
    }

}

