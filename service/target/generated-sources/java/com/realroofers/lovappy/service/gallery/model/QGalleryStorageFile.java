package com.realroofers.lovappy.service.gallery.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QGalleryStorageFile is a Querydsl query type for GalleryStorageFile
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QGalleryStorageFile extends EntityPathBase<GalleryStorageFile> {

    private static final long serialVersionUID = -1269686359L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QGalleryStorageFile galleryStorageFile = new QGalleryStorageFile("galleryStorageFile");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final com.realroofers.lovappy.service.user.model.embeddable.QAddress address;

    public final StringPath bucket = createString("bucket");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath name = createString("name");

    public final StringPath photoCaption = createString("photoCaption");

    public final EnumPath<com.realroofers.lovappy.service.user.support.ApprovalStatus> status = createEnum("status", com.realroofers.lovappy.service.user.support.ApprovalStatus.class);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final StringPath url = createString("url");

    public QGalleryStorageFile(String variable) {
        this(GalleryStorageFile.class, forVariable(variable), INITS);
    }

    public QGalleryStorageFile(Path<? extends GalleryStorageFile> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QGalleryStorageFile(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QGalleryStorageFile(PathMetadata metadata, PathInits inits) {
        this(GalleryStorageFile.class, metadata, inits);
    }

    public QGalleryStorageFile(Class<? extends GalleryStorageFile> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.address = inits.isInitialized("address") ? new com.realroofers.lovappy.service.user.model.embeddable.QAddress(forProperty("address")) : null;
    }

}

