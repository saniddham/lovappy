package com.realroofers.lovappy.service.datingPlaces.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDatingPlaceReview is a Querydsl query type for DatingPlaceReview
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDatingPlaceReview extends EntityPathBase<DatingPlaceReview> {

    private static final long serialVersionUID = 999437819L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDatingPlaceReview datingPlaceReview = new QDatingPlaceReview("datingPlaceReview");

    public final BooleanPath approved = createBoolean("approved");

    public final NumberPath<Double> averageRating = createNumber("averageRating", Double.class);

    public final StringPath googlePlaceId = createString("googlePlaceId");

    public final BooleanPath isFeatured = createBoolean("isFeatured");

    public final BooleanPath isGoodForCouple = createBoolean("isGoodForCouple");

    public final NumberPath<Double> overallRating = createNumber("overallRating", Double.class);

    public final NumberPath<Double> parkingAccessRating = createNumber("parkingAccessRating", Double.class);

    public final NumberPath<Double> parkingRating = createNumber("parkingRating", Double.class);

    public final QDatingPlace placeId;

    public final StringPath placeName = createString("placeName");

    public final NumberPath<Integer> placesReviewId = createNumber("placesReviewId", Integer.class);

    public final EnumPath<com.realroofers.lovappy.service.user.support.ApprovalStatus> reviewApprovalStatus = createEnum("reviewApprovalStatus", com.realroofers.lovappy.service.user.support.ApprovalStatus.class);

    public final DateTimePath<java.util.Date> reviewDate = createDateTime("reviewDate", java.util.Date.class);

    public final StringPath reviewerComment = createString("reviewerComment");

    public final StringPath reviewerEmail = createString("reviewerEmail");

    public final StringPath reviewerId = createString("reviewerId");

    public final StringPath reviewerType = createString("reviewerType");

    public final NumberPath<Double> securityRating = createNumber("securityRating", Double.class);

    public final EnumPath<com.realroofers.lovappy.service.datingPlaces.support.DatingAgeRange> suitableAgeRange = createEnum("suitableAgeRange", com.realroofers.lovappy.service.datingPlaces.support.DatingAgeRange.class);

    public final EnumPath<com.realroofers.lovappy.service.user.support.Gender> suitableGender = createEnum("suitableGender", com.realroofers.lovappy.service.user.support.Gender.class);

    public final EnumPath<com.realroofers.lovappy.service.datingPlaces.support.PriceRange> suitablePriceRange = createEnum("suitablePriceRange", com.realroofers.lovappy.service.datingPlaces.support.PriceRange.class);

    public QDatingPlaceReview(String variable) {
        this(DatingPlaceReview.class, forVariable(variable), INITS);
    }

    public QDatingPlaceReview(Path<? extends DatingPlaceReview> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QDatingPlaceReview(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QDatingPlaceReview(PathMetadata metadata, PathInits inits) {
        this(DatingPlaceReview.class, metadata, inits);
    }

    public QDatingPlaceReview(Class<? extends DatingPlaceReview> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.placeId = inits.isInitialized("placeId") ? new QDatingPlace(forProperty("placeId"), inits.get("placeId")) : null;
    }

}

