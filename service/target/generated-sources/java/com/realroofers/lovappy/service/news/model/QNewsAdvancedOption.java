package com.realroofers.lovappy.service.news.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QNewsAdvancedOption is a Querydsl query type for NewsAdvancedOption
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QNewsAdvancedOption extends EntityPathBase<NewsAdvancedOption> {

    private static final long serialVersionUID = 1207852007L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QNewsAdvancedOption newsAdvancedOption = new QNewsAdvancedOption("newsAdvancedOption");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath keywordPhrases = createString("keywordPhrases");

    public final StringPath metaDescription = createString("metaDescription");

    public final QNewsStory newsStory;

    public final StringPath seoTitle = createString("seoTitle");

    public QNewsAdvancedOption(String variable) {
        this(NewsAdvancedOption.class, forVariable(variable), INITS);
    }

    public QNewsAdvancedOption(Path<? extends NewsAdvancedOption> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QNewsAdvancedOption(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QNewsAdvancedOption(PathMetadata metadata, PathInits inits) {
        this(NewsAdvancedOption.class, metadata, inits);
    }

    public QNewsAdvancedOption(Class<? extends NewsAdvancedOption> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.newsStory = inits.isInitialized("newsStory") ? new QNewsStory(forProperty("newsStory"), inits.get("newsStory")) : null;
    }

}

