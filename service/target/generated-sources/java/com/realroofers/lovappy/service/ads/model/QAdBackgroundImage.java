package com.realroofers.lovappy.service.ads.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QAdBackgroundImage is a Querydsl query type for AdBackgroundImage
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdBackgroundImage extends EntityPathBase<AdBackgroundImage> {

    private static final long serialVersionUID = -2054989884L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QAdBackgroundImage adBackgroundImage = new QAdBackgroundImage("adBackgroundImage");

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile featuredAdBackgroundFileCloud;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile imageFileCloud;

    public QAdBackgroundImage(String variable) {
        this(AdBackgroundImage.class, forVariable(variable), INITS);
    }

    public QAdBackgroundImage(Path<? extends AdBackgroundImage> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QAdBackgroundImage(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QAdBackgroundImage(PathMetadata metadata, PathInits inits) {
        this(AdBackgroundImage.class, metadata, inits);
    }

    public QAdBackgroundImage(Class<? extends AdBackgroundImage> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.featuredAdBackgroundFileCloud = inits.isInitialized("featuredAdBackgroundFileCloud") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("featuredAdBackgroundFileCloud"), inits.get("featuredAdBackgroundFileCloud")) : null;
        this.imageFileCloud = inits.isInitialized("imageFileCloud") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("imageFileCloud"), inits.get("imageFileCloud")) : null;
    }

}

