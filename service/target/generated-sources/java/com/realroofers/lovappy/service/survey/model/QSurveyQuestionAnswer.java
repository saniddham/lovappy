package com.realroofers.lovappy.service.survey.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSurveyQuestionAnswer is a Querydsl query type for SurveyQuestionAnswer
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSurveyQuestionAnswer extends EntityPathBase<SurveyQuestionAnswer> {

    private static final long serialVersionUID = 1383471316L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSurveyQuestionAnswer surveyQuestionAnswer = new QSurveyQuestionAnswer("surveyQuestionAnswer");

    public final QSurveyAnswer answer;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final QSurveyQuestion question;

    public final QUserSurvey userSurvey;

    public QSurveyQuestionAnswer(String variable) {
        this(SurveyQuestionAnswer.class, forVariable(variable), INITS);
    }

    public QSurveyQuestionAnswer(Path<? extends SurveyQuestionAnswer> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSurveyQuestionAnswer(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSurveyQuestionAnswer(PathMetadata metadata, PathInits inits) {
        this(SurveyQuestionAnswer.class, metadata, inits);
    }

    public QSurveyQuestionAnswer(Class<? extends SurveyQuestionAnswer> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.answer = inits.isInitialized("answer") ? new QSurveyAnswer(forProperty("answer"), inits.get("answer")) : null;
        this.question = inits.isInitialized("question") ? new QSurveyQuestion(forProperty("question")) : null;
        this.userSurvey = inits.isInitialized("userSurvey") ? new QUserSurvey(forProperty("userSurvey"), inits.get("userSurvey")) : null;
    }

}

