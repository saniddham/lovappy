package com.realroofers.lovappy.service.credits.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUserCredits is a Querydsl query type for UserCredits
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUserCredits extends EntityPathBase<UserCredits> {

    private static final long serialVersionUID = 765535827L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QUserCredits userCredits = new QUserCredits("userCredits");

    public final NumberPath<Integer> balance = createNumber("balance", Integer.class);

    public final DateTimePath<java.util.Date> created = createDateTime("created", java.util.Date.class);

    public final QUserCreditPK id;

    public final DateTimePath<java.util.Date> updated = createDateTime("updated", java.util.Date.class);

    public QUserCredits(String variable) {
        this(UserCredits.class, forVariable(variable), INITS);
    }

    public QUserCredits(Path<? extends UserCredits> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QUserCredits(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QUserCredits(PathMetadata metadata, PathInits inits) {
        this(UserCredits.class, metadata, inits);
    }

    public QUserCredits(Class<? extends UserCredits> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QUserCreditPK(forProperty("id"), inits.get("id")) : null;
    }

}

