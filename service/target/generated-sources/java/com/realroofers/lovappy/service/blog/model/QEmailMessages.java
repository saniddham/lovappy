package com.realroofers.lovappy.service.blog.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QEmailMessages is a Querydsl query type for EmailMessages
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEmailMessages extends EntityPathBase<EmailMessages> {

    private static final long serialVersionUID = -1793057990L;

    public static final QEmailMessages emailMessages = new QEmailMessages("emailMessages");

    public final StringPath action = createString("action");

    public final StringPath body = createString("body");

    public final DateTimePath<java.util.Date> createdOn = createDateTime("createdOn", java.util.Date.class);

    public final NumberPath<Integer> emailMessagesId = createNumber("emailMessagesId", Integer.class);

    public final StringPath title = createString("title");

    public QEmailMessages(String variable) {
        super(EmailMessages.class, forVariable(variable));
    }

    public QEmailMessages(Path<? extends EmailMessages> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEmailMessages(PathMetadata metadata) {
        super(EmailMessages.class, metadata);
    }

}

