package com.realroofers.lovappy.service.music.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMusicExchange is a Querydsl query type for MusicExchange
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMusicExchange extends EntityPathBase<MusicExchange> {

    private static final long serialVersionUID = -1441952841L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMusicExchange musicExchange = new QMusicExchange("musicExchange");

    public final DateTimePath<java.util.Date> exchangeDate = createDateTime("exchangeDate", java.util.Date.class);

    public final com.realroofers.lovappy.service.user.model.QUser fromUser;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath lyrics = createString("lyrics");

    public final QMusic music;

    public final NumberPath<Integer> receiverMobileDownloads = createNumber("receiverMobileDownloads", Integer.class);

    public final NumberPath<Integer> receiverWebDownloads = createNumber("receiverWebDownloads", Integer.class);

    public final BooleanPath seen = createBoolean("seen");

    public final NumberPath<Integer> senderMobileDownloads = createNumber("senderMobileDownloads", Integer.class);

    public final NumberPath<Integer> senderWebDownloads = createNumber("senderWebDownloads", Integer.class);

    public final EnumPath<com.realroofers.lovappy.service.music.support.SendStatus> sendStatus = createEnum("sendStatus", com.realroofers.lovappy.service.music.support.SendStatus.class);

    public final com.realroofers.lovappy.service.user.model.QUser toUser;

    public final BooleanPath userResponded = createBoolean("userResponded");

    public QMusicExchange(String variable) {
        this(MusicExchange.class, forVariable(variable), INITS);
    }

    public QMusicExchange(Path<? extends MusicExchange> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QMusicExchange(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QMusicExchange(PathMetadata metadata, PathInits inits) {
        this(MusicExchange.class, metadata, inits);
    }

    public QMusicExchange(Class<? extends MusicExchange> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.fromUser = inits.isInitialized("fromUser") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("fromUser"), inits.get("fromUser")) : null;
        this.music = inits.isInitialized("music") ? new QMusic(forProperty("music"), inits.get("music")) : null;
        this.toUser = inits.isInitialized("toUser") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("toUser"), inits.get("toUser")) : null;
    }

}

