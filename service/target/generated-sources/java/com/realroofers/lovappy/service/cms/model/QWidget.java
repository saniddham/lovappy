package com.realroofers.lovappy.service.cms.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QWidget is a Querydsl query type for Widget
 */
@Generated("com.querydsl.codegen.SupertypeSerializer")
public class QWidget extends EntityPathBase<Widget> {

    private static final long serialVersionUID = -209983247L;

    public static final QWidget widget = new QWidget("widget");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath name = createString("name");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QWidget(String variable) {
        super(Widget.class, forVariable(variable));
    }

    public QWidget(Path<? extends Widget> path) {
        super(path.getType(), path.getMetadata());
    }

    public QWidget(PathMetadata metadata) {
        super(Widget.class, metadata);
    }

}

