package com.realroofers.lovappy.service.datingPlaces.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPlacesAttachment is a Querydsl query type for PlacesAttachment
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPlacesAttachment extends EntityPathBase<PlacesAttachment> {

    private static final long serialVersionUID = -200023800L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPlacesAttachment placesAttachment = new QPlacesAttachment("placesAttachment");

    public final NumberPath<Integer> attachmentId = createNumber("attachmentId", Integer.class);

    public final DateTimePath<java.util.Date> createdDate = createDateTime("createdDate", java.util.Date.class);

    public final StringPath imageDescription = createString("imageDescription");

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile picture;

    public final QDatingPlace placeId;

    public final StringPath url = createString("url");

    public QPlacesAttachment(String variable) {
        this(PlacesAttachment.class, forVariable(variable), INITS);
    }

    public QPlacesAttachment(Path<? extends PlacesAttachment> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPlacesAttachment(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPlacesAttachment(PathMetadata metadata, PathInits inits) {
        this(PlacesAttachment.class, metadata, inits);
    }

    public QPlacesAttachment(Class<? extends PlacesAttachment> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.picture = inits.isInitialized("picture") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("picture"), inits.get("picture")) : null;
        this.placeId = inits.isInitialized("placeId") ? new QDatingPlace(forProperty("placeId"), inits.get("placeId")) : null;
    }

}

