package com.realroofers.lovappy.service.datingPlaces.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDatingPlaceFoodTypes is a Querydsl query type for DatingPlaceFoodTypes
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDatingPlaceFoodTypes extends EntityPathBase<DatingPlaceFoodTypes> {

    private static final long serialVersionUID = -74612424L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDatingPlaceFoodTypes datingPlaceFoodTypes = new QDatingPlaceFoodTypes("datingPlaceFoodTypes");

    public final QFoodType foodType;

    public final QDatingPlace placeId;

    public QDatingPlaceFoodTypes(String variable) {
        this(DatingPlaceFoodTypes.class, forVariable(variable), INITS);
    }

    public QDatingPlaceFoodTypes(Path<? extends DatingPlaceFoodTypes> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QDatingPlaceFoodTypes(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QDatingPlaceFoodTypes(PathMetadata metadata, PathInits inits) {
        this(DatingPlaceFoodTypes.class, metadata, inits);
    }

    public QDatingPlaceFoodTypes(Class<? extends DatingPlaceFoodTypes> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.foodType = inits.isInitialized("foodType") ? new QFoodType(forProperty("foodType")) : null;
        this.placeId = inits.isInitialized("placeId") ? new QDatingPlace(forProperty("placeId"), inits.get("placeId")) : null;
    }

}

