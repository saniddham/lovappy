package com.realroofers.lovappy.service.user.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPasswordResetRequest is a Querydsl query type for PasswordResetRequest
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPasswordResetRequest extends EntityPathBase<PasswordResetRequest> {

    private static final long serialVersionUID = 987550784L;

    public static final QPasswordResetRequest passwordResetRequest = new QPasswordResetRequest("passwordResetRequest");

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final NumberPath<Integer> ID = createNumber("ID", Integer.class);

    public final StringPath token = createString("token");

    public final NumberPath<Integer> userID = createNumber("userID", Integer.class);

    public QPasswordResetRequest(String variable) {
        super(PasswordResetRequest.class, forVariable(variable));
    }

    public QPasswordResetRequest(Path<? extends PasswordResetRequest> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPasswordResetRequest(PathMetadata metadata) {
        super(PasswordResetRequest.class, metadata);
    }

}

