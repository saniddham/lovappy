package com.realroofers.lovappy.service.user.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUserProfile is a Querydsl query type for UserProfile
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUserProfile extends EntityPathBase<UserProfile> {

    private static final long serialVersionUID = 725711353L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QUserProfile userProfile = new QUserProfile("userProfile");

    public final com.realroofers.lovappy.service.user.model.embeddable.QAddress address;

    public final SetPath<User, QUser> allowedProfileUsers = this.<User, QUser>createSet("allowedProfileUsers", User.class, QUser.class, PathInits.DIRECT2);

    public final NumberPath<Double> avgDistanceOfViewers = createNumber("avgDistanceOfViewers", Double.class);

    public final DateTimePath<java.util.Date> birthDate = createDateTime("birthDate", java.util.Date.class);

    public final ListPath<User, QUser> blockedUsers = this.<User, QUser>createList("blockedUsers", User.class, QUser.class, PathInits.DIRECT2);

    public final BooleanPath completed = createBoolean("completed");

    public final com.realroofers.lovappy.service.couples.model.QCouplesProfile couplesProfile;

    public final NumberPath<Integer> creditBalance = createNumber("creditBalance", Integer.class);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile feetCloudFile;

    public final BooleanPath feetFileSkipped = createBoolean("feetFileSkipped");

    public final EnumPath<com.realroofers.lovappy.service.user.support.Gender> gender = createEnum("gender", com.realroofers.lovappy.service.user.support.Gender.class);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile handsCloudFile;

    public final BooleanPath handsFileSkipped = createBoolean("handsFileSkipped");

    public final NumberPath<Double> heightCm = createNumber("heightCm", Double.class);

    public final BooleanPath isAllowedProfilePic = createBoolean("isAllowedProfilePic");

    public final com.realroofers.lovappy.service.system.model.QLanguage language;

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile legsCloudFile;

    public final BooleanPath legsFileSkipped = createBoolean("legsFileSkipped");

    public final MapPath<String, String, StringPath> lifestyles = this.<String, String, StringPath>createMap("lifestyles", String.class, String.class, StringPath.class);

    public final NumberPath<Integer> listenedToCount = createNumber("listenedToCount", Integer.class);

    public final NumberPath<Integer> listenedToFemaleCount = createNumber("listenedToFemaleCount", Integer.class);

    public final NumberPath<Integer> listenedToMaleCount = createNumber("listenedToMaleCount", Integer.class);

    public final BooleanPath lovstampSkipped = createBoolean("lovstampSkipped");

    public final NumberPath<Integer> maxTransactions = createNumber("maxTransactions", Integer.class);

    public final MapPath<String, String, StringPath> personalities = this.<String, String, StringPath>createMap("personalities", String.class, String.class, StringPath.class);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile photoIdentificationCloudFile;

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile profilePhotoCloudFile;

    public final StringPath skills = createString("skills");

    public final CollectionPath<com.realroofers.lovappy.service.system.model.Language, com.realroofers.lovappy.service.system.model.QLanguage> speakingLanguage = this.<com.realroofers.lovappy.service.system.model.Language, com.realroofers.lovappy.service.system.model.QLanguage>createCollection("speakingLanguage", com.realroofers.lovappy.service.system.model.Language.class, com.realroofers.lovappy.service.system.model.QLanguage.class, PathInits.DIRECT2);

    public final MapPath<String, String, StringPath> statuses = this.<String, String, StringPath>createMap("statuses", String.class, String.class, StringPath.class);

    public final StringPath stripeCustomerId = createString("stripeCustomerId");

    public final EnumPath<com.realroofers.lovappy.service.user.support.SubscriptionType> subscriptionType = createEnum("subscriptionType", com.realroofers.lovappy.service.user.support.SubscriptionType.class);

    public final QUser user;

    public final NumberPath<Integer> userId = createNumber("userId", Integer.class);

    public final NumberPath<Integer> views = createNumber("views", Integer.class);

    public final EnumPath<com.realroofers.lovappy.service.user.support.ZodiacSigns> zodiacSigns = createEnum("zodiacSigns", com.realroofers.lovappy.service.user.support.ZodiacSigns.class);

    public QUserProfile(String variable) {
        this(UserProfile.class, forVariable(variable), INITS);
    }

    public QUserProfile(Path<? extends UserProfile> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QUserProfile(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QUserProfile(PathMetadata metadata, PathInits inits) {
        this(UserProfile.class, metadata, inits);
    }

    public QUserProfile(Class<? extends UserProfile> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.address = inits.isInitialized("address") ? new com.realroofers.lovappy.service.user.model.embeddable.QAddress(forProperty("address")) : null;
        this.couplesProfile = inits.isInitialized("couplesProfile") ? new com.realroofers.lovappy.service.couples.model.QCouplesProfile(forProperty("couplesProfile"), inits.get("couplesProfile")) : null;
        this.feetCloudFile = inits.isInitialized("feetCloudFile") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("feetCloudFile"), inits.get("feetCloudFile")) : null;
        this.handsCloudFile = inits.isInitialized("handsCloudFile") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("handsCloudFile"), inits.get("handsCloudFile")) : null;
        this.language = inits.isInitialized("language") ? new com.realroofers.lovappy.service.system.model.QLanguage(forProperty("language")) : null;
        this.legsCloudFile = inits.isInitialized("legsCloudFile") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("legsCloudFile"), inits.get("legsCloudFile")) : null;
        this.photoIdentificationCloudFile = inits.isInitialized("photoIdentificationCloudFile") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("photoIdentificationCloudFile"), inits.get("photoIdentificationCloudFile")) : null;
        this.profilePhotoCloudFile = inits.isInitialized("profilePhotoCloudFile") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("profilePhotoCloudFile"), inits.get("profilePhotoCloudFile")) : null;
        this.user = inits.isInitialized("user") ? new QUser(forProperty("user"), inits.get("user")) : null;
    }

}

