package com.realroofers.lovappy.service.music.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMusicResponseTemplate is a Querydsl query type for MusicResponseTemplate
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMusicResponseTemplate extends EntityPathBase<MusicResponseTemplate> {

    private static final long serialVersionUID = 1636801903L;

    public static final QMusicResponseTemplate musicResponseTemplate = new QMusicResponseTemplate("musicResponseTemplate");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath template = createString("template");

    public final EnumPath<MusicResponseType> type = createEnum("type", MusicResponseType.class);

    public QMusicResponseTemplate(String variable) {
        super(MusicResponseTemplate.class, forVariable(variable));
    }

    public QMusicResponseTemplate(Path<? extends MusicResponseTemplate> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMusicResponseTemplate(PathMetadata metadata) {
        super(MusicResponseTemplate.class, metadata);
    }

}

