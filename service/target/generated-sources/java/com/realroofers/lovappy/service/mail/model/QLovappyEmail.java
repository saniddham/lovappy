package com.realroofers.lovappy.service.mail.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QLovappyEmail is a Querydsl query type for LovappyEmail
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLovappyEmail extends EntityPathBase<LovappyEmail> {

    private static final long serialVersionUID = -1548717718L;

    public static final QLovappyEmail lovappyEmail = new QLovappyEmail("lovappyEmail");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath email = createString("email");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QLovappyEmail(String variable) {
        super(LovappyEmail.class, forVariable(variable));
    }

    public QLovappyEmail(Path<? extends LovappyEmail> path) {
        super(path.getType(), path.getMetadata());
    }

    public QLovappyEmail(PathMetadata metadata) {
        super(LovappyEmail.class, metadata);
    }

}

