package com.realroofers.lovappy.service.gift.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QGiftExchange is a Querydsl query type for GiftExchange
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QGiftExchange extends EntityPathBase<GiftExchange> {

    private static final long serialVersionUID = 1532880563L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QGiftExchange giftExchange = new QGiftExchange("giftExchange");

    public final StringPath cardNumber = createString("cardNumber");

    public final NumberPath<Double> commission = createNumber("commission", Double.class);

    public final NumberPath<Double> commissionPercentage = createNumber("commissionPercentage", Double.class);

    public final NumberPath<Double> earned = createNumber("earned", Double.class);

    public final com.realroofers.lovappy.service.user.model.QUser fromUser;

    public final com.realroofers.lovappy.service.product.model.QProduct gift;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath paymentMethod = createString("paymentMethod");

    public final com.realroofers.lovappy.service.vendor.model.QVendorLocation redeemAt;

    public final StringPath redeemCode = createString("redeemCode");

    public final BooleanPath redeemed = createBoolean("redeemed");

    public final DateTimePath<java.util.Date> redeemOn = createDateTime("redeemOn", java.util.Date.class);

    public final DateTimePath<java.util.Date> sentAt = createDateTime("sentAt", java.util.Date.class);

    public final NumberPath<Double> soldPrice = createNumber("soldPrice", Double.class);

    public final com.realroofers.lovappy.service.user.model.QUser toUser;

    public final StringPath transactionID = createString("transactionID");

    public QGiftExchange(String variable) {
        this(GiftExchange.class, forVariable(variable), INITS);
    }

    public QGiftExchange(Path<? extends GiftExchange> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QGiftExchange(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QGiftExchange(PathMetadata metadata, PathInits inits) {
        this(GiftExchange.class, metadata, inits);
    }

    public QGiftExchange(Class<? extends GiftExchange> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.fromUser = inits.isInitialized("fromUser") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("fromUser"), inits.get("fromUser")) : null;
        this.gift = inits.isInitialized("gift") ? new com.realroofers.lovappy.service.product.model.QProduct(forProperty("gift"), inits.get("gift")) : null;
        this.redeemAt = inits.isInitialized("redeemAt") ? new com.realroofers.lovappy.service.vendor.model.QVendorLocation(forProperty("redeemAt"), inits.get("redeemAt")) : null;
        this.toUser = inits.isInitialized("toUser") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("toUser"), inits.get("toUser")) : null;
    }

}

