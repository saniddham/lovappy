package com.realroofers.lovappy.service.vendor.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QVendorLocation is a Querydsl query type for VendorLocation
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QVendorLocation extends EntityPathBase<VendorLocation> {

    private static final long serialVersionUID = -1132162043L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QVendorLocation vendorLocation = new QVendorLocation("vendorLocation");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    public final StringPath addressLine1 = createString("addressLine1");

    public final StringPath addressLine2 = createString("addressLine2");

    public final StringPath assistantManager = createString("assistantManager");

    public final StringPath assistantManagerContact = createString("assistantManagerContact");

    public final StringPath city = createString("city");

    public final StringPath companyName = createString("companyName");

    public final StringPath country = createString("country");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final com.realroofers.lovappy.service.user.model.QUser createdBy;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final NumberPath<Double> latitude = createNumber("latitude", Double.class);

    public final StringPath locationContact = createString("locationContact");

    public final NumberPath<Double> longitude = createNumber("longitude", Double.class);

    public final StringPath openFrom = createString("openFrom");

    public final StringPath openTo = createString("openTo");

    public final StringPath state = createString("state");

    public final StringPath storeId = createString("storeId");

    public final StringPath storeManager = createString("storeManager");

    public final StringPath storeManagerContact = createString("storeManagerContact");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final StringPath zipCode = createString("zipCode");

    public QVendorLocation(String variable) {
        this(VendorLocation.class, forVariable(variable), INITS);
    }

    public QVendorLocation(Path<? extends VendorLocation> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QVendorLocation(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QVendorLocation(PathMetadata metadata, PathInits inits) {
        this(VendorLocation.class, metadata, inits);
    }

    public QVendorLocation(Class<? extends VendorLocation> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.createdBy = inits.isInitialized("createdBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("createdBy"), inits.get("createdBy")) : null;
    }

}

