package com.realroofers.lovappy.service.order.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCoupon is a Querydsl query type for Coupon
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCoupon extends EntityPathBase<Coupon> {

    private static final long serialVersionUID = 706524206L;

    public static final QCoupon coupon = new QCoupon("coupon");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final EnumPath<com.realroofers.lovappy.service.order.support.CouponCategory> category = createEnum("category", com.realroofers.lovappy.service.order.support.CouponCategory.class);

    public final StringPath couponCode = createString("couponCode");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final StringPath description = createString("description");

    public final NumberPath<Integer> discountPercent = createNumber("discountPercent", Integer.class);

    public final DateTimePath<java.util.Date> expiryDate = createDateTime("expiryDate", java.util.Date.class);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final ListPath<ProductOrder, QProductOrder> orders = this.<ProductOrder, QProductOrder>createList("orders", ProductOrder.class, QProductOrder.class, PathInits.DIRECT2);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QCoupon(String variable) {
        super(Coupon.class, forVariable(variable));
    }

    public QCoupon(Path<? extends Coupon> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCoupon(PathMetadata metadata) {
        super(Coupon.class, metadata);
    }

}

