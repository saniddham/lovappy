package com.realroofers.lovappy.service.datingPlaces.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDatingPlacePersonTypes is a Querydsl query type for DatingPlacePersonTypes
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDatingPlacePersonTypes extends EntityPathBase<DatingPlacePersonTypes> {

    private static final long serialVersionUID = -415037087L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDatingPlacePersonTypes datingPlacePersonTypes = new QDatingPlacePersonTypes("datingPlacePersonTypes");

    public final QDatingPlacePersonTypeId id;

    public final QPersonType personType;

    public final QDatingPlace placeId;

    public QDatingPlacePersonTypes(String variable) {
        this(DatingPlacePersonTypes.class, forVariable(variable), INITS);
    }

    public QDatingPlacePersonTypes(Path<? extends DatingPlacePersonTypes> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QDatingPlacePersonTypes(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QDatingPlacePersonTypes(PathMetadata metadata, PathInits inits) {
        this(DatingPlacePersonTypes.class, metadata, inits);
    }

    public QDatingPlacePersonTypes(Class<? extends DatingPlacePersonTypes> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QDatingPlacePersonTypeId(forProperty("id")) : null;
        this.personType = inits.isInitialized("personType") ? new QPersonType(forProperty("personType")) : null;
        this.placeId = inits.isInitialized("placeId") ? new QDatingPlace(forProperty("placeId"), inits.get("placeId")) : null;
    }

}

