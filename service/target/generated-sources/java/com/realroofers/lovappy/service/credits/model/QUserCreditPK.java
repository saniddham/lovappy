package com.realroofers.lovappy.service.credits.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUserCreditPK is a Querydsl query type for UserCreditPK
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QUserCreditPK extends BeanPath<UserCreditPK> {

    private static final long serialVersionUID = -2038194149L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QUserCreditPK userCreditPK = new QUserCreditPK("userCreditPK");

    public final com.realroofers.lovappy.service.user.model.QUser fromUser;

    public final com.realroofers.lovappy.service.user.model.QUser toUser;

    public QUserCreditPK(String variable) {
        this(UserCreditPK.class, forVariable(variable), INITS);
    }

    public QUserCreditPK(Path<? extends UserCreditPK> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QUserCreditPK(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QUserCreditPK(PathMetadata metadata, PathInits inits) {
        this(UserCreditPK.class, metadata, inits);
    }

    public QUserCreditPK(Class<? extends UserCreditPK> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.fromUser = inits.isInitialized("fromUser") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("fromUser"), inits.get("fromUser")) : null;
        this.toUser = inits.isInitialized("toUser") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("toUser"), inits.get("toUser")) : null;
    }

}

