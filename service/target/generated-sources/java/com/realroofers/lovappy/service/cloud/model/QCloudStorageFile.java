package com.realroofers.lovappy.service.cloud.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCloudStorageFile is a Querydsl query type for CloudStorageFile
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCloudStorageFile extends EntityPathBase<CloudStorageFile> {

    private static final long serialVersionUID = -127302557L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCloudStorageFile cloudStorageFile = new QCloudStorageFile("cloudStorageFile");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath approved = createBoolean("approved");

    public final com.realroofers.lovappy.service.user.model.QUser approvedBy;

    public final DateTimePath<java.util.Date> approvedDate = createDateTime("approvedDate", java.util.Date.class);

    public final StringPath bucket = createString("bucket");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    public final BooleanPath rejected = createBoolean("rejected");

    public final com.realroofers.lovappy.service.user.model.QUser rejectedBy;

    public final DateTimePath<java.util.Date> rejectedDate = createDateTime("rejectedDate", java.util.Date.class);

    public final ListPath<ImageThumbnail, QImageThumbnail> thumbnails = this.<ImageThumbnail, QImageThumbnail>createList("thumbnails", ImageThumbnail.class, QImageThumbnail.class, PathInits.DIRECT2);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final StringPath url = createString("url");

    public QCloudStorageFile(String variable) {
        this(CloudStorageFile.class, forVariable(variable), INITS);
    }

    public QCloudStorageFile(Path<? extends CloudStorageFile> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCloudStorageFile(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCloudStorageFile(PathMetadata metadata, PathInits inits) {
        this(CloudStorageFile.class, metadata, inits);
    }

    public QCloudStorageFile(Class<? extends CloudStorageFile> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.approvedBy = inits.isInitialized("approvedBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("approvedBy"), inits.get("approvedBy")) : null;
        this.rejectedBy = inits.isInitialized("rejectedBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("rejectedBy"), inits.get("rejectedBy")) : null;
    }

}

