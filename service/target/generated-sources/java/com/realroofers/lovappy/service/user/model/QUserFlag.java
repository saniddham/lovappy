package com.realroofers.lovappy.service.user.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUserFlag is a Querydsl query type for UserFlag
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUserFlag extends EntityPathBase<UserFlag> {

    private static final long serialVersionUID = -1135906532L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QUserFlag userFlag = new QUserFlag("userFlag");

    public final DateTimePath<java.util.Date> created = createDateTime("created", java.util.Date.class);

    public final BooleanPath discriminatory = createBoolean("discriminatory");

    public final BooleanPath harrassment = createBoolean("harrassment");

    public final QUserFlagPK id;

    public final BooleanPath notRelevant = createBoolean("notRelevant");

    public final BooleanPath offensive = createBoolean("offensive");

    public final StringPath others = createString("others");

    public final BooleanPath uncivil = createBoolean("uncivil");

    public QUserFlag(String variable) {
        this(UserFlag.class, forVariable(variable), INITS);
    }

    public QUserFlag(Path<? extends UserFlag> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QUserFlag(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QUserFlag(PathMetadata metadata, PathInits inits) {
        this(UserFlag.class, metadata, inits);
    }

    public QUserFlag(Class<? extends UserFlag> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QUserFlagPK(forProperty("id"), inits.get("id")) : null;
    }

}

