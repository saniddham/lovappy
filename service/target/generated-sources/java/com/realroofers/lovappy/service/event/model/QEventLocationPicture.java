package com.realroofers.lovappy.service.event.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QEventLocationPicture is a Querydsl query type for EventLocationPicture
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEventLocationPicture extends EntityPathBase<EventLocationPicture> {

    private static final long serialVersionUID = -144097077L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QEventLocationPicture eventLocationPicture = new QEventLocationPicture("eventLocationPicture");

    public final QEvent eventId;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile picture;

    public QEventLocationPicture(String variable) {
        this(EventLocationPicture.class, forVariable(variable), INITS);
    }

    public QEventLocationPicture(Path<? extends EventLocationPicture> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QEventLocationPicture(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QEventLocationPicture(PathMetadata metadata, PathInits inits) {
        this(EventLocationPicture.class, metadata, inits);
    }

    public QEventLocationPicture(Class<? extends EventLocationPicture> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.eventId = inits.isInitialized("eventId") ? new QEvent(forProperty("eventId"), inits.get("eventId")) : null;
        this.picture = inits.isInitialized("picture") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("picture"), inits.get("picture")) : null;
    }

}

