package com.realroofers.lovappy.service.datingPlaces.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDatingPlaceDeals is a Querydsl query type for DatingPlaceDeals
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDatingPlaceDeals extends EntityPathBase<DatingPlaceDeals> {

    private static final long serialVersionUID = -1920372092L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDatingPlaceDeals datingPlaceDeals = new QDatingPlaceDeals("datingPlaceDeals");

    public final EnumPath<com.realroofers.lovappy.service.user.support.ApprovalStatus> approvalStatus = createEnum("approvalStatus", com.realroofers.lovappy.service.user.support.ApprovalStatus.class);

    public final BooleanPath approved = createBoolean("approved");

    public final QDatingPlace datingPlace;

    public final StringPath dealDescription = createString("dealDescription");

    public final NumberPath<Double> dealPrice = createNumber("dealPrice", Double.class);

    public final QDeals deals;

    public final NumberPath<Double> discountValue = createNumber("discountValue", Double.class);

    public final DateTimePath<java.util.Date> expiryDate = createDateTime("expiryDate", java.util.Date.class);

    public final NumberPath<Double> fullAmount = createNumber("fullAmount", Double.class);

    public final BooleanPath isActive = createBoolean("isActive");

    public final NumberPath<Integer> placeDealId = createNumber("placeDealId", Integer.class);

    public final DateTimePath<java.util.Date> startDate = createDateTime("startDate", java.util.Date.class);

    public QDatingPlaceDeals(String variable) {
        this(DatingPlaceDeals.class, forVariable(variable), INITS);
    }

    public QDatingPlaceDeals(Path<? extends DatingPlaceDeals> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QDatingPlaceDeals(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QDatingPlaceDeals(PathMetadata metadata, PathInits inits) {
        this(DatingPlaceDeals.class, metadata, inits);
    }

    public QDatingPlaceDeals(Class<? extends DatingPlaceDeals> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.datingPlace = inits.isInitialized("datingPlace") ? new QDatingPlace(forProperty("datingPlace"), inits.get("datingPlace")) : null;
        this.deals = inits.isInitialized("deals") ? new QDeals(forProperty("deals")) : null;
    }

}

