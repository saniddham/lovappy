package com.realroofers.lovappy.service.product.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProduct is a Querydsl query type for Product
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProduct extends EntityPathBase<Product> {

    private static final long serialVersionUID = -1029939576L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProduct product = new QProduct("product");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    public final BooleanPath approved = createBoolean("approved");

    public final com.realroofers.lovappy.service.user.model.QUser approvedBy;

    public final BooleanPath blocked = createBoolean("blocked");

    public final com.realroofers.lovappy.service.user.model.QUser blockedBy;

    public final QBrand brand;

    public final NumberPath<java.math.BigDecimal> commissionEarned = createNumber("commissionEarned", java.math.BigDecimal.class);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final com.realroofers.lovappy.service.user.model.QUser createdBy;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile image1;

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile image2;

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile image3;

    public final NumberPath<java.math.BigDecimal> price = createNumber("price", java.math.BigDecimal.class);

    public final EnumPath<com.realroofers.lovappy.service.product.support.ProductAvailability> productAvailability = createEnum("productAvailability", com.realroofers.lovappy.service.product.support.ProductAvailability.class);

    public final StringPath productCode = createString("productCode");

    public final StringPath productDescription = createString("productDescription");

    public final StringPath productEan = createString("productEan");

    public final StringPath productName = createString("productName");

    public final QProductType productType;

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final com.realroofers.lovappy.service.user.model.QUser updatedBy;

    public QProduct(String variable) {
        this(Product.class, forVariable(variable), INITS);
    }

    public QProduct(Path<? extends Product> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QProduct(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QProduct(PathMetadata metadata, PathInits inits) {
        this(Product.class, metadata, inits);
    }

    public QProduct(Class<? extends Product> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.approvedBy = inits.isInitialized("approvedBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("approvedBy"), inits.get("approvedBy")) : null;
        this.blockedBy = inits.isInitialized("blockedBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("blockedBy"), inits.get("blockedBy")) : null;
        this.brand = inits.isInitialized("brand") ? new QBrand(forProperty("brand"), inits.get("brand")) : null;
        this.createdBy = inits.isInitialized("createdBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("createdBy"), inits.get("createdBy")) : null;
        this.image1 = inits.isInitialized("image1") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("image1"), inits.get("image1")) : null;
        this.image2 = inits.isInitialized("image2") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("image2"), inits.get("image2")) : null;
        this.image3 = inits.isInitialized("image3") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("image3"), inits.get("image3")) : null;
        this.productType = inits.isInitialized("productType") ? new QProductType(forProperty("productType"), inits.get("productType")) : null;
        this.updatedBy = inits.isInitialized("updatedBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("updatedBy"), inits.get("updatedBy")) : null;
    }

}

