package com.realroofers.lovappy.service.product.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QBrandCommission is a Querydsl query type for BrandCommission
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QBrandCommission extends EntityPathBase<BrandCommission> {

    private static final long serialVersionUID = -865464213L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QBrandCommission brandCommission = new QBrandCommission("brandCommission");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    public final DateTimePath<java.util.Date> affectiveDate = createDateTime("affectiveDate", java.util.Date.class);

    public final NumberPath<Double> commissionPercentage = createNumber("commissionPercentage", Double.class);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final com.realroofers.lovappy.service.user.model.QUser createdBy;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.user.model.QUser modifiedBy;

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QBrandCommission(String variable) {
        this(BrandCommission.class, forVariable(variable), INITS);
    }

    public QBrandCommission(Path<? extends BrandCommission> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QBrandCommission(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QBrandCommission(PathMetadata metadata, PathInits inits) {
        this(BrandCommission.class, metadata, inits);
    }

    public QBrandCommission(Class<? extends BrandCommission> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.createdBy = inits.isInitialized("createdBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("createdBy"), inits.get("createdBy")) : null;
        this.modifiedBy = inits.isInitialized("modifiedBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("modifiedBy"), inits.get("modifiedBy")) : null;
    }

}

