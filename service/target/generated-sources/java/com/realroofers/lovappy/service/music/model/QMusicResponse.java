package com.realroofers.lovappy.service.music.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMusicResponse is a Querydsl query type for MusicResponse
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMusicResponse extends EntityPathBase<MusicResponse> {

    private static final long serialVersionUID = 522916309L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMusicResponse musicResponse = new QMusicResponse("musicResponse");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final QMusicExchange musicExchange;

    public final StringPath response = createString("response");

    public final DateTimePath<java.util.Date> responseDate = createDateTime("responseDate", java.util.Date.class);

    public final BooleanPath seen = createBoolean("seen");

    public final EnumPath<MusicResponseType> type = createEnum("type", MusicResponseType.class);

    public QMusicResponse(String variable) {
        this(MusicResponse.class, forVariable(variable), INITS);
    }

    public QMusicResponse(Path<? extends MusicResponse> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QMusicResponse(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QMusicResponse(PathMetadata metadata, PathInits inits) {
        this(MusicResponse.class, metadata, inits);
    }

    public QMusicResponse(Class<? extends MusicResponse> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.musicExchange = inits.isInitialized("musicExchange") ? new QMusicExchange(forProperty("musicExchange"), inits.get("musicExchange")) : null;
    }

}

