package com.realroofers.lovappy.service.order.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPrice is a Querydsl query type for Price
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPrice extends EntityPathBase<Price> {

    private static final long serialVersionUID = 173421633L;

    public static final QPrice price1 = new QPrice("price1");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final NumberPath<Double> price = createNumber("price", Double.class);

    public final EnumPath<com.realroofers.lovappy.service.order.support.OrderDetailType> type = createEnum("type", com.realroofers.lovappy.service.order.support.OrderDetailType.class);

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QPrice(String variable) {
        super(Price.class, forVariable(variable));
    }

    public QPrice(Path<? extends Price> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPrice(PathMetadata metadata) {
        super(Price.class, metadata);
    }

}

