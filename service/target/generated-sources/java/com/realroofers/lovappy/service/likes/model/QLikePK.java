package com.realroofers.lovappy.service.likes.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QLikePK is a Querydsl query type for LikePK
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QLikePK extends BeanPath<LikePK> {

    private static final long serialVersionUID = 575369772L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QLikePK likePK = new QLikePK("likePK");

    public final com.realroofers.lovappy.service.user.model.QUser likeBy;

    public final com.realroofers.lovappy.service.user.model.QUser likeTo;

    public QLikePK(String variable) {
        this(LikePK.class, forVariable(variable), INITS);
    }

    public QLikePK(Path<? extends LikePK> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QLikePK(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QLikePK(PathMetadata metadata, PathInits inits) {
        this(LikePK.class, metadata, inits);
    }

    public QLikePK(Class<? extends LikePK> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.likeBy = inits.isInitialized("likeBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("likeBy"), inits.get("likeBy")) : null;
        this.likeTo = inits.isInitialized("likeTo") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("likeTo"), inits.get("likeTo")) : null;
    }

}

