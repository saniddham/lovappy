package com.realroofers.lovappy.service.ads.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QAdTargetArea is a Querydsl query type for AdTargetArea
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdTargetArea extends EntityPathBase<AdTargetArea> {

    private static final long serialVersionUID = 418111591L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QAdTargetArea adTargetArea = new QAdTargetArea("adTargetArea");

    public final QAd ad;

    public final com.realroofers.lovappy.service.user.model.embeddable.QAddress address;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public QAdTargetArea(String variable) {
        this(AdTargetArea.class, forVariable(variable), INITS);
    }

    public QAdTargetArea(Path<? extends AdTargetArea> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QAdTargetArea(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QAdTargetArea(PathMetadata metadata, PathInits inits) {
        this(AdTargetArea.class, metadata, inits);
    }

    public QAdTargetArea(Class<? extends AdTargetArea> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.ad = inits.isInitialized("ad") ? new QAd(forProperty("ad"), inits.get("ad")) : null;
        this.address = inits.isInitialized("address") ? new com.realroofers.lovappy.service.user.model.embeddable.QAddress(forProperty("address")) : null;
    }

}

