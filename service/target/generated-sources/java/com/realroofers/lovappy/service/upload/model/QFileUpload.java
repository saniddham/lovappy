package com.realroofers.lovappy.service.upload.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QFileUpload is a Querydsl query type for FileUpload
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QFileUpload extends EntityPathBase<FileUpload> {

    private static final long serialVersionUID = -999994900L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QFileUpload fileUpload = new QFileUpload("fileUpload");

    public final BooleanPath active = createBoolean("active");

    public final BooleanPath approved = createBoolean("approved");

    public final com.realroofers.lovappy.service.user.model.QUser approvedBy;

    public final EnumPath<com.realroofers.lovappy.service.ads.support.AudioType> audioType = createEnum("audioType", com.realroofers.lovappy.service.ads.support.AudioType.class);

    public final QComedyCategory comedyCategory;

    public final DateTimePath<java.util.Date> created = createDateTime("created", java.util.Date.class);

    public final com.realroofers.lovappy.service.user.model.QUser createdBy;

    public final StringPath description = createString("description");

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile file;

    public final com.realroofers.lovappy.service.music.model.QGenre genre;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile imageFile;

    public final BooleanPath isAdvertisement = createBoolean("isAdvertisement");

    public final StringPath lyrics = createString("lyrics");

    public final EnumPath<com.realroofers.lovappy.service.ads.support.AdMediaType> mediaType = createEnum("mediaType", com.realroofers.lovappy.service.ads.support.AdMediaType.class);

    public final QPodcastCategory podcastCategory;

    public final NumberPath<Double> price = createNumber("price", Double.class);

    public final BooleanPath rejected = createBoolean("rejected");

    public final com.realroofers.lovappy.service.user.model.QUser rejectedBy;

    public final StringPath title = createString("title");

    public final EnumPath<com.realroofers.lovappy.service.ads.support.AdType> type = createEnum("type", com.realroofers.lovappy.service.ads.support.AdType.class);

    public final BooleanPath userAgreement = createBoolean("userAgreement");

    public QFileUpload(String variable) {
        this(FileUpload.class, forVariable(variable), INITS);
    }

    public QFileUpload(Path<? extends FileUpload> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QFileUpload(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QFileUpload(PathMetadata metadata, PathInits inits) {
        this(FileUpload.class, metadata, inits);
    }

    public QFileUpload(Class<? extends FileUpload> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.approvedBy = inits.isInitialized("approvedBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("approvedBy"), inits.get("approvedBy")) : null;
        this.comedyCategory = inits.isInitialized("comedyCategory") ? new QComedyCategory(forProperty("comedyCategory"), inits.get("comedyCategory")) : null;
        this.createdBy = inits.isInitialized("createdBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("createdBy"), inits.get("createdBy")) : null;
        this.file = inits.isInitialized("file") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("file"), inits.get("file")) : null;
        this.genre = inits.isInitialized("genre") ? new com.realroofers.lovappy.service.music.model.QGenre(forProperty("genre")) : null;
        this.imageFile = inits.isInitialized("imageFile") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("imageFile"), inits.get("imageFile")) : null;
        this.podcastCategory = inits.isInitialized("podcastCategory") ? new QPodcastCategory(forProperty("podcastCategory"), inits.get("podcastCategory")) : null;
        this.rejectedBy = inits.isInitialized("rejectedBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("rejectedBy"), inits.get("rejectedBy")) : null;
    }

}

