package com.realroofers.lovappy.service.event.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QEventUserEmail is a Querydsl query type for EventUserEmail
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEventUserEmail extends EntityPathBase<EventUserEmail> {

    private static final long serialVersionUID = -231358125L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QEventUserEmail eventUserEmail = new QEventUserEmail("eventUserEmail");

    public final StringPath emailBody = createString("emailBody");

    public final StringPath emailId = createString("emailId");

    public final NumberPath<Integer> eventEmailId = createNumber("eventEmailId", Integer.class);

    public final DateTimePath<java.util.Date> sentDate = createDateTime("sentDate", java.util.Date.class);

    public final StringPath title = createString("title");

    public final com.realroofers.lovappy.service.user.model.QUser userId;

    public QEventUserEmail(String variable) {
        this(EventUserEmail.class, forVariable(variable), INITS);
    }

    public QEventUserEmail(Path<? extends EventUserEmail> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QEventUserEmail(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QEventUserEmail(PathMetadata metadata, PathInits inits) {
        this(EventUserEmail.class, metadata, inits);
    }

    public QEventUserEmail(Class<? extends EventUserEmail> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.userId = inits.isInitialized("userId") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("userId"), inits.get("userId")) : null;
    }

}

