package com.realroofers.lovappy.service.news.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QNewsCategory is a Querydsl query type for NewsCategory
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QNewsCategory extends EntityPathBase<NewsCategory> {

    private static final long serialVersionUID = 1249288622L;

    public static final QNewsCategory newsCategory = new QNewsCategory("newsCategory");

    public final StringPath categoryDescription = createString("categoryDescription");

    public final NumberPath<Long> categoryId = createNumber("categoryId", Long.class);

    public final StringPath categoryName = createString("categoryName");

    public final NumberPath<Integer> status = createNumber("status", Integer.class);

    public QNewsCategory(String variable) {
        super(NewsCategory.class, forVariable(variable));
    }

    public QNewsCategory(Path<? extends NewsCategory> path) {
        super(path.getType(), path.getMetadata());
    }

    public QNewsCategory(PathMetadata metadata) {
        super(NewsCategory.class, metadata);
    }

}

