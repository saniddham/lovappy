package com.realroofers.lovappy.service.radio.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QRadioProgramLocation is a Querydsl query type for RadioProgramLocation
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRadioProgramLocation extends EntityPathBase<RadioProgramLocation> {

    private static final long serialVersionUID = -766817767L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QRadioProgramLocation radioProgramLocation = new QRadioProgramLocation("radioProgramLocation");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath location = createString("location");

    public final QRadioProgramControl radioProgramControl;

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QRadioProgramLocation(String variable) {
        this(RadioProgramLocation.class, forVariable(variable), INITS);
    }

    public QRadioProgramLocation(Path<? extends RadioProgramLocation> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QRadioProgramLocation(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QRadioProgramLocation(PathMetadata metadata, PathInits inits) {
        this(RadioProgramLocation.class, metadata, inits);
    }

    public QRadioProgramLocation(Class<? extends RadioProgramLocation> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.radioProgramControl = inits.isInitialized("radioProgramControl") ? new QRadioProgramControl(forProperty("radioProgramControl"), inits.get("radioProgramControl")) : null;
    }

}

