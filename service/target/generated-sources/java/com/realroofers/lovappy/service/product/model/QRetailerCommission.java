package com.realroofers.lovappy.service.product.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QRetailerCommission is a Querydsl query type for RetailerCommission
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRetailerCommission extends EntityPathBase<RetailerCommission> {

    private static final long serialVersionUID = 971987650L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QRetailerCommission retailerCommission = new QRetailerCommission("retailerCommission");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final BooleanPath active = createBoolean("active");

    public final DateTimePath<java.util.Date> affectiveDate = createDateTime("affectiveDate", java.util.Date.class);

    public final NumberPath<Double> commissionPercentage = createNumber("commissionPercentage", Double.class);

    public final com.realroofers.lovappy.service.user.model.QCountry country;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final com.realroofers.lovappy.service.user.model.QUser createdBy;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final com.realroofers.lovappy.service.user.model.QUser modifiedBy;

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public QRetailerCommission(String variable) {
        this(RetailerCommission.class, forVariable(variable), INITS);
    }

    public QRetailerCommission(Path<? extends RetailerCommission> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QRetailerCommission(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QRetailerCommission(PathMetadata metadata, PathInits inits) {
        this(RetailerCommission.class, metadata, inits);
    }

    public QRetailerCommission(Class<? extends RetailerCommission> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.country = inits.isInitialized("country") ? new com.realroofers.lovappy.service.user.model.QCountry(forProperty("country"), inits.get("country")) : null;
        this.createdBy = inits.isInitialized("createdBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("createdBy"), inits.get("createdBy")) : null;
        this.modifiedBy = inits.isInitialized("modifiedBy") ? new com.realroofers.lovappy.service.user.model.QUser(forProperty("modifiedBy"), inits.get("modifiedBy")) : null;
    }

}

