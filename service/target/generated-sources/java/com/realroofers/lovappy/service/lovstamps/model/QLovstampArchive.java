package com.realroofers.lovappy.service.lovstamps.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QLovstampArchive is a Querydsl query type for LovstampArchive
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLovstampArchive extends EntityPathBase<LovstampArchive> {

    private static final long serialVersionUID = 1083716063L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QLovstampArchive lovstampArchive = new QLovstampArchive("lovstampArchive");

    public final com.realroofers.lovappy.service.core.QBaseEntity _super = new com.realroofers.lovappy.service.core.QBaseEntity(this);

    public final com.realroofers.lovappy.service.cloud.model.QCloudStorageFile audioFileCloud;

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath recordFile = createString("recordFile");

    //inherited
    public final DateTimePath<java.util.Date> updated = _super.updated;

    public final NumberPath<Integer> userId = createNumber("userId", Integer.class);

    public QLovstampArchive(String variable) {
        this(LovstampArchive.class, forVariable(variable), INITS);
    }

    public QLovstampArchive(Path<? extends LovstampArchive> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QLovstampArchive(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QLovstampArchive(PathMetadata metadata, PathInits inits) {
        this(LovstampArchive.class, metadata, inits);
    }

    public QLovstampArchive(Class<? extends LovstampArchive> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.audioFileCloud = inits.isInitialized("audioFileCloud") ? new com.realroofers.lovappy.service.cloud.model.QCloudStorageFile(forProperty("audioFileCloud"), inits.get("audioFileCloud")) : null;
    }

}

