# Lovappy #

*Lovappy** is a courting app that tries to slow down the dating process.

## **For developers of this service**

### Prerequisites ###

* Install [Java 8 or higher](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* Install [Maven 3](https://maven.apache.org/download.cgi)
* Install [Mysql 5.x](https://dev.mysql.com/downloads/mysql/)
* Create 'lovappy' database in mysql

### Building and Running the application ###
* clone this repo
* Build the project
```
cd lovappy-app
mvn clean install
```
* Run the server:
```
cd web
mvn spring-boot:run
```
* Open browser then go to http://localhost:8080/user/genderinfo

### Notes ##
* Not all environments are the same. Eg, production credentials must be hidden from developers. To be able to override the default configurations which are set in application.properties, go to ```src/main/resources``` and copy ```application-overrides.properties.sample``` to ```application-overrides.properties```. Set application-overrides.properties to your desired configuration the run or re-run the server.